#!/bin/bash
echo $(date +'%Y-%m-%d %H:%M:%S') "Starte import" >> /var/log/cron_alditool.log
/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/production/current/public/index.php pcshipment/import_cron init
echo $(date +'%Y-%m-%d %H:%M:%S') "beende import" >> /var/log/cron_alditool.log
