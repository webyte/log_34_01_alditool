#!/bin/bash
echo $(date +'%Y-%m-%d %H:%M:%S') "Starte Aldi KPI Report" >> /var/log/cron_alditool.log
/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/production/current/public/index.php report/report_clp_cron excel_report_mail 3 401
echo $(date +'%Y-%m-%d %H:%M:%S') "Beende Aldi KPI Report" >> /var/log/cron_alditool.log
echo $(date +'%Y-%m-%d %H:%M:%S') "Starte Aldi Container Status Report" >> /var/log/cron_alditool.log
/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/production/current/public/index.php report/report_clp_cron excel_report_mail 1 401
echo $(date +'%Y-%m-%d %H:%M:%S') "Beende Aldi Container Status Report" >> /var/log/cron_alditool.log
