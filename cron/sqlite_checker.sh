#!/bin/bash
echo $(date +'%Y-%m-%d %H:%M:%S') "Starte sqlite_checker" >> /var/log/cron_alditool.log
/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/production/current/public/index.php manage/sqlite_checker init
echo $(date +'%Y-%m-%d %H:%M:%S') "beende sqlite_checker" >> /var/log/cron_alditool.log
