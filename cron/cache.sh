#!/bin/bash
echo $(date +'%Y-%m-%d %H:%M:%S') "Starte cache" >> /var/log/cron_alditool.log
#/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/staging/current/public/index.php dashboard/dashboard_cache_cron init
/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/production/current/public/index.php dashboard/dashboard_cache_cron init
/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/production/current/public/index.php report/report_clp_cache_cron init
echo $(date +'%Y-%m-%d %H:%M:%S') "Beende cache" >> /var/log/cron_alditool.log

