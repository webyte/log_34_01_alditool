#!/bin/sh
own_pid=$$

# check if an other aldi-permissions.sh process is already running
echo " " >>/tmp/aldi-permissions.sh.log
echo "`date` - starting $0" >>/tmp/aldi-permissions.sh.log


ps -ef  | grep -v "/bin/bash" | grep -v "/bin/sh" | grep "/usr/local/scripts/cronjobs/aldi/aldi-permissions.sh" | grep -v "${own_pid}" | grep -v grep >>/tmp/aldi-permissions.sh.log

ps -ef  | grep -v "/bin/bash" | grep -v "/bin/sh" | grep "/usr/local/scripts/cronjobs/aldi/aldi-permissions.sh" | grep -v "${own_pid}" | grep -v grep >/dev/null 2>&1
ret=$?
echo "`date` - RET: $ret" >>/tmp/aldi-permissions.sh.log

# if no other aldi-permissions.sh is running, go ahead
if [ $ret -ne 0 ]
then
    # find /clu/srv/www/alditool.thiel-logistik.loc -type d -mtime -1 -exec chmod 2775 {} \;
    # find /clu/srv/www/alditool.thiel-logistik.loc -type f -mtime -1 -exec chmod 666 {} \;
    find /clu/srv/www/alditool.thiel-logistik.loc -type d -name uploads -exec chmod 2777 {} \; 
    find /clu/srv/www/alditool.thiel-logistik.loc/production/shared -type d -mtime -1 -exec chmod 2777 {} \;
    find /clu/srv/www/alditool.thiel-logistik.loc/production/shared -type f -mtime -1 -exec chmod 777 {} \;
    find /clu/srv/www/alditool.thiel-logistik.loc/staging -type d -mtime -1 -exec chmod 2777 {} \;
    find /clu/srv/www/alditool.thiel-logistik.loc/staging -type f -mtime -1 -exec chmod 777 {} \;
    chmod 777 /clu/srv/www/alditool.thiel-logistik.loc/web
    chmod -R 777 /clu/srv/www/alditool.thiel-logistik.loc/web/export
else
    echo "`date`- another aldi-permissions.sh process is already running - exiting" >>/tmp/aldi-permissions.sh.log
fi
echo "`date` - finishing $0" >>/tmp/aldi-permissions.sh.log


exit 0

