#!/bin/bash
echo $(date +'%Y-%m-%d %H:%M:%S') "Starte Aldi PO Report" >> /var/log/cron_alditool.log
/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/production/current/public/index.php report/report_clp_cron excel_report_mail 5 401
echo $(date +'%Y-%m-%d %H:%M:%S') "Beende Aldi PO Report" >> /var/log/cron_alditool.log
echo $(date +'%Y-%m-%d %H:%M:%S') "Starte Aldi Shipment Report" >> /var/log/cron_alditool.log
/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/production/current/public/index.php report/report_clp_cron excel_report_mail 6 401
echo $(date +'%Y-%m-%d %H:%M:%S') "Beende Aldi Shipment Report" >> /var/log/cron_alditool.log
