#!/bin/bash
echo $(date +'%Y-%m-%d %H:%M:%S') "Starte reminder" >> /var/log/cron_alditool.log
/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/production/current/public/index.php shipment/reminder_cron init
echo $(date +'%Y-%m-%d %H:%M:%S') "Beende reminder" >> /var/log/cron_alditool.log
