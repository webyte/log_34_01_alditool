#!/bin/sh
###
# Loesche alditool files aelter 60 Tage
# author : x.team@logwin-logistics.com
# 08-07-2020
###
#
### finde files in shared
find /srv/ftp/alditool/production/shared/* -mtime +45 -exec ls {} + > /tmp/todelete_alditool.txt
#

### finde files in prod   
find /srv/www/alditool.thiel-logistik.loc/www/archive/* -mtime +45 -exec ls {} + >> /tmp/todelete_alditool.txt
#

### finde files in import
find /srv/www/alditool.thiel-logistik.loc/www/import/ -mtime +45 -exec ls {} +  >> /tmp/todelete_alditool.txt

### finde files im stage
find /srv/www/alditool.thiel-logistik.loc/www/import_stage/ -mtime +30 -exec ls {} + >> /tmp/todelete_alditool.txt
#
### loesche files
cat /tmp/todelete_alditool.txt | while read FILENAME; do    rm "${FILENAME}"; done
#
