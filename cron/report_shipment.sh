#!/bin/bash
echo $(date +'%Y-%m-%d %H:%M:%S') "Starte Aldi Shipment Report" >> /var/log/cron_alditool.log
/usr/bin/php5.6 /var/www/alditool.thiel-logistik.loc/production/current/public/index.php report/report_clp_cron excel_report_file 7
echo $(date +'%Y-%m-%d %H:%M:%S') "Beende Aldi Shipment Report" >> /var/log/cron_alditool.log
