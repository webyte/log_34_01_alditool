
$(document).ready(function() {
	initDfuButtons();
	initTableSorter(); // must be last function to call!
	$('.dfu-search-form').validationEngine();

});

/**
 * initDfuButtons
 */
function initDfuButtons(){
	$('input#dfu-select-all').on( 'click', function(e) {
		var isChecked = $(this).prop('checked');
		if( isChecked ) {
			$('table.dfuresulttable td input[type="checkbox"]').prop('checked', true);
		} else {
			$('table.dfuresulttable td input[type="checkbox"]').prop('checked', false);
		}
	})

	$('button#send-dfu').click( function() {

		if( ! $('.dfu-search-form').validationEngine('validate') ) return false;

		//if( $('input[name="shipmentid[]"]:checked').length <= 0 || $('input[name="documents[]"]:checked').length <= 0 ) return false;

		var docs = '';
		var docTypes = [];
		var body = $('textarea#dfu-body').text();
		$('input[name="documents[]"]:checked').each(function() {
			docs += '- '+$(this).val()+"\n";
			docTypes.push($(this).val());
		});
		body = body.replace('[DOCUMENTS]', docs);
		$('textarea#dfu-body').text(body);

		var advertisementWeek = $('input#dfu-advertisement-week').val();

		$('#modal-dfu').modal('show');
		$('#modal-dfu a.submit').click( function() {
			var recipients = [];
			var contractNumbers = [];
			var shipmentIds = [];
			$('.dfu-recipients input:checked').each(function() {
				recipients.push($(this).attr('value'));
			});
			$('input[name="shipmentid[]"]:checked').each(function() {
				contractNumbers.push($(this).attr('data-contract-number'));
				shipmentIds.push($(this).val());
			});
			var url = 'shipment/dfu/sendMail';
			$.post(
				url,
				{
					recipients: recipients,
					contractNumbers: contractNumbers,
					shipmentIds: shipmentIds,
					docTypes: docTypes,
					subject: $('#modal-dfu h3').text(),
					body: escape( $('#modal-dfu textarea').text() ),
					advertisementWeek: advertisementWeek
				}
			);
			$(this).unbind('click');
			$('#modal-dfu').modal('hide');
		});
		return false;
	});
}

function initTableSorter() {
	$.tablesorter.addParser({
		id: 'germandate',
		is: function(s) {
			return false;
		},
		format: function(s) {
			if( s != "" ) {
				var a = s.split('/');
				a[1] = a[1].replace(/^[0]+/g,"");
				return new Date(a.reverse().join("/")).getTime();
			}
		},
		type: 'numeric'
	});

	if( $("table.dfuresulttable tr").length > 1 ){
		$("table.dfuresulttable").addClass('tablesorter').tablesorter();
	}
}
