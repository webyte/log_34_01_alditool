$(document).ready(function() {

	$('.customs-excel-export').click( function(e) {
		$search = $('form.search-customs-excel-export');
		$inputs = $('form.search input[type="text"]').clone();
		$inputs.attr({type:"hidden"});
		$search.find('input[type="hidden"]').remove();
		$search.prepend($inputs);
		$search.submit();
		return false;
	});

	$('[data-toggle="tooltip"]').tooltip({html:true});

});
