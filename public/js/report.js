
$(document).ready(function() {
	initReportingButtons();
	initReportTableSorter(); // must be last function to call!
});

/**
 * Report
 */
function initReportingButtons(){

	$('a.generate-report, a.generate-clp, a.generate-cda, a.generate-order, a.generate-crd-vs-atd, a.generate-telex-1, a.generate-telex-2').show();

	$('a.generate-report').click( function(e) {
		$('form.search:first').attr('target','_blank');
		$('form.search:first').submit();
		return false;
	});
	$('a.generate-crd-vs-atd').click( function(e) {
		$('form.search:first').attr('target','_blank');
		$('form.search:first').submit();
		return false;
	});
	$('a.generate-clp').click( function(e) {
		$('form.search:first').attr('target','_blank');
		$('form.search:first').submit();
		return false;
	});
	$('a.search-cda').click( function(e) {
		$('form.search:first').submit();
		return false;
	});
	$('a.generate-cda').click( function(e) {
		$('form.search:eq(1)').attr('target','_blank');
		$('form.search:eq(1)').submit();
		return false;
	});
	$('a.generate-order').click( function(e) {
		$('form.search:first').attr('target','_blank');
		$('form.search:first').submit();
		return false;
	});
	$('a.generate-clp-delivery').click( function(e) {
		$('form.search:first').attr('target','_blank');
		$('form.search:first').submit();
		return false;
	});
	$('a.generate-telex-1').click( function(e) {
		$('form.search:first').attr('target','_blank');
		$('form.search:first').submit();
		return false;
	});
	$('a.generate-telex-2').click( function(e) {
		$('form.search:first').attr('target','_blank');
		$('form.search:first').submit();
		return false;
	});
	$('a.generate-ff').click( function(e) {

		$('form.search:first').attr('target','_blank');
		$('form.search:first').submit();
		//$('input[name="ff_states[]"]:checked').each( function() {
		//	var $form 	= $('form.search:first');
		//	var id 		= $(this).val();
		//	var action 	= $form.attr('action')+'/'+id;
		//	$form.attr('target','');
		//	$form.attr('action',action);
		//	$form.submit();
		//})
		return false;
	});

	$('a.generate-and-send-crd-vs-atd').click( function(e) {
		var contractType = $('select#contracts_contract_type').val()
		var url = $(this).attr('href');
		$(this).attr('href', url + '/' + contractType );
	});

	$('input#cda-select-all').on( 'click', function(e) {
		var isChecked = $(this).prop('checked');
		if( isChecked ) {
			$('table.reportresulttable td input[type="checkbox"]').prop('checked', true);
		} else {
			$('table.reportresulttable td input[type="checkbox"]').prop('checked', false);
		}
	})
}

function initReportTableSorter() {
	$.tablesorter.addParser({
		id: 'germandate',
		is: function(s) {
			return false;
		},
		format: function(s) {
			if( s != "" ) {
				var a = s.split('/');
				a[1] = a[1].replace(/^[0]+/g,"");
				return new Date(a.reverse().join("/")).getTime();
			}
		},
		type: 'numeric'
	});

	if( $("table.reportresulttable tr").length > 1 ){
		$("table.reportresulttable").addClass('tablesorter').tablesorter();
		$("table.reportresulttable").tablesorterPager({container: $(".pager"), positionFixed: false, size: 20});
	}
}
