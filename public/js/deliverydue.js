
$(document).ready(function() {
	initUploadButtons();
});

/**
 * initUploadButtons
 */
function initUploadButtons(){
	$('#delivery-due-upload-file').change(function() {
		$('#delivery-due-upload-holder').val($(this).val());
	});
}
