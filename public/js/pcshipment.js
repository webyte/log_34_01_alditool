
$(document).ready(function() {
	initModals();
	initPCShipmentButtons();
});

/**
 * initPCShipmentButtons
 */
function initPCShipmentButtons(){

	addHandlers();

	$('.link-clp tbody tr.row-clickable').mouseover(function() {
		$(this).css('background-color','#f9f9f9');
	});
	$('.link-clp tbody tr.row-clickable').mouseout(function() {
		$(this).css('background-color','#fff');
	});
	$('.link-clp tbody tr.row-clickable').toggle(function() {
		$(this).next().css('display','table-row');
		//	addHandlers()
	}, function() {
		$(this).next().css('display','none');
	});

}

function addHandlers() {

	$('button.remove-delivery').off('click');
	$('button.add-delivery').off('click');

	$('button.remove-delivery').on('click', function(e) {
		var $ref 			= $(this);
		var deliveryId 		= $ref.attr('data-delivery-id');
		var shipmentId 		= $ref.attr('data-shipment-id');
		var pcshipmentId 	= $('input[name="pcshipments_id[]"]').val();
		var url 			= "pcshipment/pcshipment/removeDelivery";
		if( deliveryId != '' ) {
			$ref.addClass('disabled');
			$ref.find('i').removeClass('icon-trash').addClass('icon-time');
			$ref.off('click');
			$.post(
				url,
				{deliveryId: deliveryId, pcshipmentId: pcshipmentId, shipmentId: shipmentId},
				function( data ) {
					if( data.success ) {
						$ref.removeClass('disabled');
						$ref.removeClass('remove-delivery').addClass('add-delivery');
						$ref.find('i').removeClass('icon-time').addClass('icon-plus');
						addHandlers();
					}
				},
				'json'
			);
		}
	});


	$('button.add-delivery').on('click', function(e) {
		var $ref 			= $(this);
		var deliveryId 		= $ref.attr('data-delivery-id');
		var shipmentId 		= $ref.attr('data-shipment-id');
		var pcshipmentId 	= $('input[name="pcshipments_id[]"]').val();
		var url 			= "pcshipment/pcshipment/addDelivery";

		if( deliveryId != '' && !isOtherCLPRelation( shipmentId ) ) {
			$ref.addClass('disabled');
			$ref.find('i').removeClass('icon-plus').addClass('icon-time');
			$ref.off('click');
			$.post(
				url,
				{deliveryId: deliveryId, pcshipmentId: pcshipmentId, shipmentId: shipmentId},
				function( data ) {
					if( data.success ) {
						$ref.removeClass('disabled');
						$ref.removeClass('add-delivery').addClass('remove-delivery');
						$ref.find('i').removeClass('icon-time').addClass('icon-trash');
						$('input[name="delivery-added-initially"]').val(1 );
						addHandlers()
					}
				},
				'json'
			);
		} else {
			$('#linkCLPError').modal('show');
		}
	})

}

function isOtherCLPRelation( shipmentId ) {
	var isFound = false;
	$('table.link-clp button.remove-delivery').each( function() {
		var $ref = $(this);
		var newShipmentId = $ref.attr('data-shipment-id');
		if( shipmentId != newShipmentId ) {
			isFound =  true;
		}
	});
	return isFound;
}

function initModals(){
	$('#linkCLPError').modal('hide');
}
