$(document).ready(function() {
	$('.comment-show').live('click', function () {
		var $modal = $('#comment-modal');
		var id = $(this).data('id');
		$modal.find('.modal-body div').empty();
		$modal.modal('show');
		$('input#comment-msg').attr('data-id', id);
		$.post(
			'dashboard/dashboard_supplier/getComments',
			{id: id},
			function (data) {
				$.each( data, function( k, v ) {
					$modal.find('.modal-body div').append('<p><small>'+v.crdate+' '+v.username+'</small><br>'+v.msg+'</p><hr>');
				});
			},
			'json'
		);
		return false;
	});
	$('.comment-add').live('click', function () {
		var $modal  = $('#comment-modal');
		var id      = $('input#comment-msg').data('id');
		var msg     = $('input#comment-msg').val();
		$('input#comment-msg').val('');
		$.post(
			'dashboard/dashboard_supplier/addComment',
			{id: id, msg: msg},
			function (data) {
				$modal.find('.modal-body div').empty();
				$.each( data, function( k, v ) {
					$modal.find('.modal-body div').append('<p><small>'+v.crdate+' '+v.username+'</small><br>'+v.msg+'</p><hr>');
				});
			},
			'json'
		);
		return false;
	});

	$('.export-xls').live('click', function () {
		var defs = [
			{field: "contract_number", label: "Contract No"},
			{field: "sales_week", label: "Sales Week"},
			{field: "traffic_type", label: "Traffic Type"},
			{field: "departure_port", label: "Departure"},
			{field: "dc", label: "DC"},
			{field: "ofu_real", label: "Booking Received Date"},
			{field: "actual_contract_ready_date", label: "Actual Contract Ready Date"},
			{field: "received_in_cfs", label: "Received in CFS"},
			{field: "container_gate_in", label: "CY Gate In"},
			{field: "etd", label: "ETD"},
			{field: "eta", label: "ETA"},
			{field: "atd", label: "ATD"},
			{field: "ata", label: "ATA"},
			{field: "cus", label: "CUS"},
			{field: "delivery_window_start", label: "Delivery Window Start"},
			{field: "delivery_window_end", label: "Delivery Window End"},
			{field: "status_delivery", label: "Status Delivery"}
		];
		var data = [];
		var rows = table.$children[1].newData;
		var labels = [];
		$.each(defs, function(i, def) {
			labels.push(def.label);
		});
		data.push(labels);

		$.each(rows, function(i, row) {
			var tmp = [];
			$.each(defs, function(i, def) {
				tmp.push(row[def.field]);
			});
			data.push(tmp);
		});

		var ws = XLSX.utils.aoa_to_sheet(data);
		var wb = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, "Entire Transportation");
		XLSX.writeFile(wb, "dashboard_entire_transportation.xlsb");

		return false;
	});
});

function delete_row(ws, row_index){
	var variable = XLSX.utils.decode_range(ws["!ref"])
	for(var R = row_index; R < variable.e.r; ++R){
		for(var C = variable.s.c; C <= variable.e.c; ++C){
			ws[ec(R,C)] = ws[ec(R+1,C)];
		}
	}
	variable.e.r--
	ws['!ref'] = XLSX.utils.encode_range(variable.s, variable.e);
}
function ec(r, c){
	return XLSX.utils.encode_cell({r:r,c:c});
}