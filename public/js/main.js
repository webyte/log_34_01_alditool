$(document).ready(function () {

    $('.form').validationEngine();

    initDatepicker();
    initTimepicker();
    initDeleteWarning();
    initDeleteButtons();
    initFieldHistoryIcons();
    initSearchButtons();
    initTableSorter(); // must be last function to call!
});



function initDatepicker() {
    $('input.datepicker').datepicker({
        dateFormat: 'dd/mm/yy',
        timeFormat: 'hh:mm:ss'
    });
    $('input.datetimepicker').datetimepicker({
        dateFormat: 'dd/mm/yy',
        timeFormat: 'hh:mm:ss'
    });
}

function initTimepicker() {
    $('input.timepicker').timepicker({
        timeFormat: 'hh:mm'
    });
}

function initTableSorter() {
    $.tablesorter.addParser({
        id: 'germandate',
        is: function (s) {
            return false;
        },
        format: function (s) {
            if (s != "") {
                var a = s.split('/');
                a[1] = a[1].replace(/^[0]+/g, "");
                return new Date(a.reverse().join("/")).getTime();
            }
        },
        type: 'numeric'
    });

    if ($("table.resulttable tr").length > 1) {
        $("table.resulttable").addClass('tablesorter').tablesorter();
        $("table.resulttable").tablesorterPager({container: $(".pager"), positionFixed: false, size: 20});
    }
}

function initDeleteButtons() {
    $('.delete a').click(function () {
        $('#deleteWarning').modal('show');
        var deleteLink = $(this).attr('href');
        $('#deleteWarning a.delete').click(function () {
            window.location.href = deleteLink;
            $('#deleteWarning a.delete').unbind('click');
            $('#deleteWarning').modal('hide');
        });
        return false;
    });
}

function initDeleteWarning() {
    $('#deleteWarning').modal('hide');
}

function initFieldHistoryIcons() {
    $('.history_icon').each(function () {
        var self = $(this);
        var idParts = self.attr('id').split('-');
        var recordId = idParts[0];
        var fieldName = idParts[1];
        var fieldValue = $('input', $(this).parent()).val();
        var label = $('label', $(this).parent()).text();
        var dataUrl = 'utils/fieldhistory/getHistory/' + recordId + '/' + fieldName + '/' + fieldValue;
        self.hide();

        $.get(dataUrl, function (d) {
            if (d != "") {
                self.show();
                self.popover({content: d, title: '<strong>History: ' + label + '</strong>', html: true, placement: 'top'});
            }
        });
    })

    $('.history_icon').mouseenter(function () {
        $(this).popover('show');
    }).mouseleave(function () {
        $(this).popover('hide');
    });
}

/**
 * initSearchButtons
 */
function initSearchButtons() {
    $('input[type="reset"]').click(function () {
        $('form.search').find('input:text, input:password, input:file, select, textarea').val('');
        $('form.search').find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
        $('form.search').submit();
    });
}
/////////////////////////////////////////

//Custom Formfield Validation

/////////////////////////////////////////

function isEqual(field, rules, i, options) {
    var fieldname = rules[i + 2];

//var compareField = $('input[name='+fieldname+']');
    var compareField = $('input#' + fieldname);

    var d1 = parseDate(field.attr('value'));
    var d2 = parseDate(compareField.attr('value'));

//console.log("isEqual :: "+fieldname);
//console.log( field );
//console.log( fieldname );
//console.log( rules );

    if (d1 && d2) {
        if (d1.getTime() == d2.getTime()) return;
        else {
            return "Date differs from " + getTableHeadline(compareField).text();
        }
    } else {

    }
}

/////////////////////////////////////////

//MISC

/////////////////////////////////////////

/**
 * parseDate
 * parses string in the form DD.MM.YYYY HH:mm:ss
 * into a date object.
 * if string has no valid format, null is returned.
 */
function parseDate(str) {
    if (!str) return null;
    var valid = str.match(/^\d{2}[\.]\d{2}[\.]\d{4} \d{2}[:]\d{2}[:]\d{2}$/);
    if (valid) {
        var date = str.split(" ")[0];
        var time = str.split(" ")[1];
        var dateparts = date.split(".");
        var timeparts = time.split(":");
        return new Date(dateparts[2], dateparts[1] - 1, dateparts[0], timeparts[0], timeparts[1], timeparts[2]);
    } else {
        //alert("given string not valid "+str);
        return null;
    }
}


/////////////////////////////////////////

//FORM VALIDATION

/////////////////////////////////////////
function setFormFieldError(field, promptText, type, ajaxed, options) {
    removeFormFieldError(field);
//alert("Prompt: "+promptText);
    if (type != 'pass') {
//	field.addClass('error');
//	field.parent().append('<p class="errorInfo">'+promptText+'</p>');
        var th = getTableHeadline(field);
        th.addClass('error');
        th.append('<a href="#" rel="tooltip" data-original-title="' + promptText + '">&nbsp;<i class="icon-warning-sign"></i></a>');
    }
    $('a[rel="tooltip"]').tooltip();
}

function removeFormFieldError(field) {
    var th = getTableHeadline(field);
    th.removeClass('error');
    $('a', th).remove();
    field.parent().find('.errorInfo').remove();
}

function getTableHeadline(field) {
    var tdIdx = field.parent().index();
    var tableObj = field.parents("table");
    return $('thead tr th', tableObj).eq(tdIdx);
}


/////////////////////////////////////////

//MISC

/////////////////////////////////////////

$.fn.clearForm = function () {
    return this.each(function () {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
            return $(':input', this).clearForm();
        if (type == 'text' || type == 'password' || tag == 'textarea')
            this.value = '';
        else if (type == 'checkbox' || type == 'radio')
            this.checked = false;
        else if (tag == 'select')
            this.selectedIndex = -1;
    });
};

//Funktion gibt es erst seit späteren JS Implementierungen
//für alte Browser muss sie so verfügbar gemacht werden!

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt /*, from*/) {
        var len = this.length;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}
