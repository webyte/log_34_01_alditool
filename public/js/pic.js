$(document).ready(function() {
	initPicButtons();
});

/**
 * Booking
 */
function initPicButtons(){
	$('select#pics_user_id').live( 'change', function(e) {
		var id = $(this).val();
		var url = 'manage/pic/getUserData/'+id;
		$.post(
			url,
			{},
			function( data ) {
				$('input#pics_email').val(data.email);
				$('input[name="pics_username[]"]').val(data.username);
			},
			'json'
		);
		return false;
	});

}
