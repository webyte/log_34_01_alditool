
$(document).ready(function() {
	initContainerButtons();
});

function initContainerButtons() {
	var consolidatedContainerIds = [];

	$('input.container-checkbox').live( 'click', function(e,d) {
		consolidateContainers( $(this) );
	});

	$('input#container-select-all').on( 'click', function(e) {
		var isChecked 	= $(this).prop('checked');
		$('input.container-checkbox').each( function() {
			$(this).prop('checked',isChecked);
			consolidateContainers( $(this) );
		});
	});

	$('button.transmit-container-button').live( 'click', function() {
		console.log(consolidatedContainerIds);
		if( consolidatedContainerIds.length > 0 ) {
			$('input#container-ids').val( $.toJSON( consolidatedContainerIds ) );
			$('form.transmit-container').submit();
		}
		return false;
	});

	consolidateContainers = function( $ref ) {
		var id = $ref.val();
		if( $ref.prop('checked') ) {
			consolidatedContainerIds.push( id );
		} else {
			consolidatedContainerIds = jQuery.grep( consolidatedContainerIds, function(value) { return value != id; });
		}
		//console.log(consolidatedContainerIds);
	};


	$('input.edo-checkbox').live( 'click', function() {
		var isChecked 	= $(this).prop('checked');
		var id 			= $(this).val();
		$.post(
			'container/container/setEDOSent',
			{edoSent: isChecked, id: id},
			function( data ) {}
		);

	});

}