
$(document).ready(function() {
	initContractButtons();
	initDeliveries();
	initModals();
	//initTableSorter(); // must be last function to call!
});

/**
 * Contract
 */
function initContractButtons(){
	$('a.add-delivery').click( function(e) {
		var url = "contract/contract/getNewDeliveryForm";
		$.post(
			url,
			{},
			function( data ) {
				$('.delivery-records').prepend( data );
				initDatepicker();
				initDeliveries();
			},
			'html'
		);
		return false;
	});
	$('a.delete-delivery').live( 'click', function(e){
		e.preventDefault();
		$(this).parent().fadeOut( 'fast', function() { $(this).remove() } );
		var url = "delivery/delivery/delete/"+$(this).attr('rel');
		$.post(
			url,
			{},
			function( data ) {},
			'html'
		);
		return false;
	});

    // Supplier Mail
	$('a.inform-supplier').click( function() {
		$('#mailToSupplier').modal('show');
		$('#mailToSupplier a.submit').click( function() {
            var recipients = [];
            $('.supplier-recipients input:checked').each(function() {
				recipients.push($(this).attr('value'));
            });

			var url = 'contract/contract/sendMail';
			$.post(
				url,
				{ 	id: $(this).attr('rel'),
					type: 1,
                    recipients: recipients,
					subject: $('#mailToSupplier h3').text(),
					body: escape( $('#mailToSupplier textarea').val() )
				}
			);
			$(this).unbind('click');
			$('#mailToSupplier').modal('hide');
		});
		return false;
	});
	// logwin origin office mail
	$('a.inform-loo').click( function() {
		$('#mailToLoo').modal('show');
		$('#mailToLoo a.submit').click( function() {
			var url = 'contract/contract/sendMail';
			$.post(
				url,
				{ 	id: $(this).attr('rel'),
					type: 2,
					subject: $('#mailToLoo h3').text(),
					body: escape( $('#mailToLoo textarea').val() )
				}
			);
			$(this).unbind('click');
			$('#mailToLoo').modal('hide');
		});
		return false;
	});
	//cube QA check
	$('input[name="contracts_cube_qa_check"]').click( function() {
		$('#cubeQACheck').modal('show');
		$('#cubeQACheck a.cube-qa-check-confirmed').click( function() {
			$('#cubeQACheck').modal('hide');
			$('input[name="contracts_cube_qa_check"]').attr('checked', true);
			return false;
		});
		$('#cubeQACheck a.cube-qa-check-unconfirmed').click( function() {
			$('#cubeQACheck').modal('hide');
			$('input[name="contracts_cube_qa_check"]').attr('checked', false);
			return false;
		});
		return false;
	});

	$('a.show-clp-unconfirmed-info').live( 'mouseover', function() {
		$(this).validationEngine('showPrompt', 'This CLP is currently not confirmed.', 'error', 'topRight', true);
		return false;
	});
	$('a.show-clp-unconfirmed-info').live( 'mouseout', function() {
		$(this).validationEngine('hide');
		return false;
	});
	$('a.show-clp-unconfirmed-info').live( 'click', function() {
		return false;
	});

	$('td.delete a').click(function () {
		$('#deleteWarning').modal('show');
		var deleteLink = $(this).attr('href');
		$('#deleteWarning a.delete').click(function () {
			window.location.href = deleteLink;
			$('#deleteWarning a.delete').unbind('click');
			$('#deleteWarning').modal('hide');
		});
		return false;
	});
}

/**
 * Delivery
 */
function initDeliveries(){
	// load product description with product code
	$('input[name="deliveries_product_code[]"]').change( function() {
		var val = $(this).val();
		var ref = $(this);
		if( val.length >= 4 ) {
			var url = 'contract/contract/getProductDescription/'+val;
			$.post(
				url,
				{},
				function( data ) {
					console.log( data.desc );
					if( data.desc != '' ) $( 'input[name="deliveries_product_description[]"]', ref.parent().parent() ).val( data.desc );
				},
				'json'
			);
		}
	});
	/*$('.article').each( function() {
		var tmp = $('.row input:first',this).attr('id');
		var id = tmp.substr(tmp.length - 1);
		$('#articles_actual_quantity_'+id).addClass('readonly').attr('readonly','readonly');
		$('#articles_cbm_'+id).addClass('readonly').attr('readonly','readonly');
		$('#articles_gross_weight_'+id).addClass('readonly').attr('readonly','readonly');
		$('#articles_carton_'+id).addClass('readonly').attr('readonly','readonly');
	})*/
}

function initModals(){
	$('#mailToSupplier').modal('hide');
	$('#mailToLogwin').modal('hide');
	$('#cubeQACheck').modal('hide');
}

function initTableSorter() {
	$.tablesorter.addParser({
		id: 'germandate',
		is: function (s) {
			return false;
		},
		format: function (s) {
			if (s != "") {
				var a = s.split('/');
				a[1] = a[1].replace(/^[0]+/g, "");
				return new Date(a.reverse().join("/")).getTime();
			}
		},
		type: 'numeric'
	});

	if ($("table.resulttable tr").length > 1) {
		$("table.resulttable").addClass('tablesorter').tablesorter();
		$("table.resulttable").tablesorterPager({container: $(".pager"), positionFixed: false, size: 100000});
	}
}
