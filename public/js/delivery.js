$(document).ready(function() {
	initDeliveryButtons();
});

/**
 * Booking
 */
function initDeliveryButtons(){
	var consolidatedDeliveryIds = [];

	$('input.delivery-checkbox').live( 'click', function(e,d) {
		consolidateDeliveries( $(this) );
	});

	$('button.consolidate-delivery-button').live( 'click', function() {
		var destinations = new Array();
		for( var i=0; i<consolidatedDeliveryIds.length; i ++ ) {
			destinations.push( consolidatedDeliveryIds[i].destination );
		}
		$('input#consolidate-delivery-ids').val( $.toJSON( consolidatedDeliveryIds ) );

		destinations = array_unique( destinations );
		if( destinations.length <= 1 ){
			$('form.consolidate-delivery').submit();
		} else {
			//$('.message').append('<div class="alert alert-error"><strong>Error!</strong> Different destinations selected.<button data-dismiss="alert" class="close" type="button">×</button></div>');
			var dcs = [];
			for( var i=0; i<consolidatedDeliveryIds.length; i++ ) {
				dcs.push(consolidatedDeliveryIds[i].destination);
			}
			dcs = jQuery.unique(dcs);
			var str = $('#consolidateDifferentDCs .modal-body p').text();
			str = str.replace( '[DC]', dcs.join(', '));

			$('#consolidateDifferentDCs .modal-body p').text( str );
			$('#consolidateDifferentDCs').modal('show');
			$('#consolidateDifferentDCs a.submit').click( function() {
				$('#consolidateDifferentDCs a.submit').unbind('click');
				$('#consolidateDifferentDCs').modal('hide');
				$('form.consolidate-delivery').submit();
			});
		}
		return false;
	});

	$('button.report-delivery-button').live( 'click', function() {
		if( consolidatedDeliveryIds.length > 0 ) {
			$('input#report-delivery-ids').val( $.toJSON( consolidatedDeliveryIds ) );
			$('form.report-delivery').submit();
		}
		return false;
	});

	$('input#delivery-select-all').on( 'click', function(e) {
		var isChecked 	= $(this).prop('checked');
		$('input.delivery-checkbox').each( function() {
			$(this).prop('checked',isChecked);
			consolidateDeliveries( $(this) );
		});
	});

	consolidateDeliveries = function( $ref ) {
		var val			= $ref.val();
		var items 		= val.split('_');
		var id 			= items[0];
		var destination	= items[1];
		var tmp 		= { id: id, destination: destination };
		if( $ref.prop('checked') ) {
			consolidatedDeliveryIds.push( tmp );
		} else {
			consolidatedDeliveryIds = jQuery.grep( consolidatedDeliveryIds, function(value) { return value.id != tmp.id; });
		}
	};

}

function array_unique( arrayName ) {
	var u = {}, a = [];
	for(var i = 0, l = arrayName.length; i < l; ++i){
		if(u.hasOwnProperty(arrayName[i])) {
			continue;
		}
		a.push(arrayName[i]);
		u[arrayName[i]] = 1;
	}
	return a;
}