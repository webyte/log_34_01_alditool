
$(document).ready(function() {

	$('.csv-report-export').click( function(e) {
		$searchCsv = $('form.search-csv');
		$inputs = $('form.search input[type="text"]').clone();
		$inputs.attr({type:"hidden"});
		$searchCsv.find('input[type="hidden"]').remove();
		$searchCsv.prepend($inputs);
		$searchCsv.submit();
		return false;
	});

});
