
$(document).ready(function() {
	initSuppliePortalButtons();
	initSupplierPortalUploadButtons();
});

/**
 * initSuppliePortalButtons
 */
function initSuppliePortalButtons(){
	$('input.supplier-document-confirmed').click( function(e) {
		var isChecked = $(this).prop('checked') ? 1 : 0;
		var recordId = $(this).data( 'record-id' );
		var documentId = $(this).data( 'document-id' );
		location.href = 'supplier/supplier_portal/confirmDocument/'+recordId+'/'+documentId+'/'+isChecked;
	});

	$('select[name="mandatory"]').change( function(e) {
		var mandatory = $(this).val();
		var recordId = $(this).data( 'record-id' );
		var documentId = $(this).data( 'document-id' );
		location.href = 'supplier/supplier_portal/setMandatory/'+recordId+'/'+documentId+'/'+mandatory;
	});

}

/**
 * initSupplierUploadButtons
 */
function initSupplierPortalUploadButtons(){
	var formData = new FormData();

	$('html').on('dragover', function(e) {
		e.preventDefault();
		e.stopPropagation();
	});

	$('html').on('drop', function(e) {
		e.preventDefault();
		e.stopPropagation();
	});

	$('#supplier-upload-holder').on('click', function() {
		$('input#supplier-upload-file').click();
	});

	$('input#supplier-upload-file').change( function(){
		var file = $('input#supplier-upload-file')[0].files[0];
		formData.append('supplier-upload-file', file );
		$('#supplier-upload-holder p').empty().append(file.name);
	});

	$('#supplier-upload-holder').on('drop', function (e) {
		e.stopPropagation();
		e.preventDefault();
		var file = e.originalEvent.dataTransfer.files[0];
		formData.append('supplier-upload-file', file );
		$('#supplier-upload-holder p').empty().append(file.name);
	});

	$('form.supplier-upload button[type="submit"]').on('click', function() {
		var url 			= $('form.supplier-upload').attr('action');
		var documentType 	= $('form.supplier-upload select[name="document-type"] option:selected').val();
		formData.append('document-type', documentType );
		$.ajax({
			url: url,
			type: 'post',
			data: formData,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function(response){
				window.location.reload();
			}
		});

		return false;
	});
	//$('#supplier-upload-file').change(function() {
	//	$('#supplier-upload-holder').val($(this).val());
	//});
}
