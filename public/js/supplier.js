
$(document).ready(function() {
	initContractButtons();
	initDeliveries();
	initModals();
});

/**
 * Contract
 */
function initContractButtons(){
	$('a.add-contactperson').click( function(e) {
		var url = "manage/supplier/getNewContactPersonForm";
		$.post(
			url,
			{},
			function( data ) {
				$('.contactperson-records').prepend( data );
			},
			'html'
		);
		return false;
	});
	$('a.delete-contactperson').live( 'click', function(e){
		e.preventDefault();
		$(this).parent().fadeOut( 'fast', function() { $(this).remove() } );
		var url = "manage/supplier/removeContactPerson/"+$(this).attr('rel');
		$.post(
			url,
			{},
			function( data ) {},
			'html'
		);
		return false;
	});
	$('a.inform-supplier').click( function() {
		$('#mailToSupplier').modal('show');
		$('#mailToSupplier a.submit').click( function() {
			var url = 'contract/contract/sendMail';
			$.post(
				url,
				{ 	id: $(this).attr('rel'),
					type: 1,
					subject: $('#mailToSupplier h3').text(),
					body: escape( $('#mailToSupplier textarea').val() )
				}
			);
			$(this).unbind('click');
			$('#mailToSupplier').modal('hide');
		});
		return false;
	});
	$('a.inform-loo').click( function() {
		$('#mailToLoo').modal('show');
		$('#mailToLoo a.submit').click( function() {
			var url = 'contract/contract/sendMail';
			$.post(
				url,
				{ 	id: $(this).attr('rel'),
					type: 2,
					subject: $('#mailToLoo h3').text(),
					body: escape( $('#mailToLoo textarea').val() )
				}
			);
			$(this).unbind('click');
			$('#mailToLoo').modal('hide');
		});
		return false;
	});
}

/**
 * Delivery
 */
function initDeliveries(){
	/*$('.article').each( function() {
		var tmp = $('.row input:first',this).attr('id');
		var id = tmp.substr(tmp.length - 1);
		$('#articles_actual_quantity_'+id).addClass('readonly').attr('readonly','readonly');
		$('#articles_cbm_'+id).addClass('readonly').attr('readonly','readonly');
		$('#articles_gross_weight_'+id).addClass('readonly').attr('readonly','readonly');
		$('#articles_carton_'+id).addClass('readonly').attr('readonly','readonly');
	})*/
}

function initModals(){
	$('#mailToSupplier').modal('hide');
	$('#mailToLogwin').modal('hide');
}
