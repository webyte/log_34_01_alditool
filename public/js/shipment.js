
$(document).ready(function() {
	initShipmentButtons();
	initDeliveries();
	initHBLHAWBButtons();
	initTypeAhead();
	initHBLHAWB();
	initDC();
	initDG();
	initDelayReason();
});

/**
 * Contract
 */
function initShipmentButtons(){
	$('a.add-contract').click( function(e) {
		var contractNumber 	= $('.add-contract-number').val();
		var destination 	= ( $('.delivery').length > 0 ) ? $('.delivery:eq(0) input[name="deliveries_destination[]"]').val() : '';
		var url 			= "shipment/shipment/getDeliveries/"+contractNumber+"/"+destination;
		//var exists 			= false;
		//$('input[name="deliveries_contract_number[]"]').each( function() {
		//		if( $(this).val() == contractNumber ) {
		//		$('.add-contract-number').validationEngine('showPrompt', 'This contract # already exists.');
		//		exists = true;
		//	}
		//});
		//if(exists) return false;

		$.post(
			url,
			{},
			function( data ) {
				if( data == 'error' ) {
					$('.delivery-holder').html( '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>Contract/PO number does not exist!</div>' );
				} else {
					$('.delivery-holder').html( data );
				}
			},
			'html'
		);
		return false;
	});
	$('button.add-delivery').live( 'click', function(e) {
		var shipmentId = $('input[name="shipments_id[]"]').val();
		var deliveryId = $(this).attr('rel');
		var url 	= "shipment/shipment/addDelivery/"+deliveryId+"/"+shipmentId;
		var dc 		= ( $('.delivery').length > 0 ) ? $('.delivery:eq(0) input[name="deliveries_dc[]"]').val() : '';
		var newDc 	= $('td:eq(1)', $(this).parent().parent()).text();
		if( dc != '' && dc != newDc ) {
			var str = $('#consolidateDifferentDCs .modal-body p').text();
			str = str.replace( '[DC]', newDc );

			$('#consolidateDifferentDCs .modal-body p').text( str );
			$('#consolidateDifferentDCs').modal('show');
			$('#consolidateDifferentDCs a.submit').click( function() {
				$('#consolidateDifferentDCs a.submit').unbind('click');
				$('#consolidateDifferentDCs').modal('hide');
				addDelivery( url );
			});
			$('#consolidateDifferentDCs a.cancel').click( function() {
				$('#consolidateDifferentDCs a.submit').unbind('click');
			});
		} else {
			addDelivery( url );
		}

		return false;
	});
	$('a.delete-delivery').live( 'click', function(e){
		e.preventDefault();
		$(this).parent().fadeOut( 'fast', function() { $(this).remove() } );
		var shipmentId = $('input[name="shipments_id[]"]').val();
		var deliveryId = $(this).attr('rel');
		var url = "shipment/shipment/removeDelivery/"+shipmentId+"/"+deliveryId;
		$.post(
			url,
			{},
			function( data ) {},
			'html'
		);
		return false;
	});
	$('#shipment-tab a').click( function(e) {
		e.preventDefault();
		$(this).tab('show');
	});
	if( $('#export #shipments_container_number').val() == '' ) $('#export #shipments_container_number').val('dummy');

	if( $('select[name="shipments_approval[]"]').val() == '1' ) {
		$('#export input, #export textarea, #export select, #import input, #import textarea, #import select').each( function() {
			var excludes = ['shipments_so_released_date[]','shipments_dc[]','shipments_online_rate[]','shipments_delay_reason_group[]','shipments_cy_cut_off[]','shipments_container_gate_in[]','shipments_export_customs_cleared[]','shipments_container_loaded[]','shipments_container_stuffed[]','shipments_delivered_to_dc[]','shipments_pick_up_at_wharf[]','shipments_delay_reason[]','deliveries_telex_received_mm[]','shipments_edo_sent','shipments_approval[]','shipments_etd[]','shipments_eta[]','shipments_cus[]','shipments_atd[]','shipments_ata[]','shipments_aqc[]','shipments_edor[]','deliveries_edl[]','deliveries_adl[]'];
			var n = $(this).attr('name');
			var t = $(this).attr('type');
			var d = $(this).attr('data-disable');
			if( jQuery.inArray( n, excludes ) == -1 && t != 'hidden' && d != 'false' ) $(this).addClass('readonly').attr('readonly','readonly').attr('disabled','disabled');
			if( t== 'checkbox' ) $(this).parent().find('input[type="hidden"][name="'+n+'"]').remove();
		});
	}

	$('input[type="submit"]').click(function(e) {
		e.preventDefault();
		//var port 	= $('input#shipments_departure_port').val();
		//var url 	= 'shipment/shipment/checkDeparturePort/'+port;
		//$.post(
		//	url,
		//	{},
		//	function( data ) {
		//		if( data.result ) {
		//			if( $('form.form').validationEngine('validate') ) {
		//				$('form.form').submit();
		//			}
		//		} else {
		//			$('input#shipments_departure_port').validationEngine('showPrompt', 'Invalid departure port', 'error', 'topRight', true);
		//			$('body').scrollTop(300);
		//		}
		//	},
		//	'json'
		//);
		if( $('form.form').validationEngine('validate') ) {
			$('form.form').submit();
		}

	});
	$('button.reactivate-hbl-hawb').click(function(e) {
		e.preventDefault();
		$('input[name="deliveries_hbl_hawb_mm[]"]',$(this).parent()).removeClass('readonly').removeAttr('readonly').removeAttr('disabled');
	});

	if( $('#shipments_container_tare_weight').val() == '' ) {
		changeContainerTareWeight();
	}

	$('#shipments_container_type, #shipments_container_size').change( function() {
		changeContainerTareWeight();
	});


}

function changeContainerTareWeight() {
	if( $('#shipments_container_type').attr('disabled') != 'disabled' ) {
		var containerType = $('option:selected', $('#shipments_container_type')).text();
		var containerSize = $('option:selected', $('#shipments_container_size')).text();
		var $inputContainerTareWeight = $('#shipments_container_tare_weight');
		var url = "shipment/shipment/getContainerTareWeight";
		$.post(
			url,
			{containerType:containerType,containerSize:containerSize},
			function( data ) { $inputContainerTareWeight.val(data.tare_weight);},
			'json'
		);
	}
}

function addDelivery( url ) {
	$('.delivery-holder table').fadeOut( 'fast', function() { $(this).remove() } );
	$.post(
		url,
		{},
		function( data ) {
			$('.delivery-records').prepend( data );
			initDatepicker();
			initDeliveries();
		},
		'html'
	);
}

/**
 * Delivery
 */
function initDeliveries(){
	/*$('.article').each( function() {
	 var tmp = $('.row input:first',this).attr('id');
	 var id = tmp.substr(tmp.length - 1);
	 $('#articles_actual_quantity_'+id).addClass('readonly').attr('readonly','readonly');
	 $('#articles_cbm_'+id).addClass('readonly').attr('readonly','readonly');
	 $('#articles_gross_weight_'+id).addClass('readonly').attr('readonly','readonly');
	 $('#articles_carton_'+id).addClass('readonly').attr('readonly','readonly');
	 })*/
}


/**
 * Delivery
 */
function initHBLHAWBButtons(){

	$('button.add-hblhawb').live('click', function(e) {
		var $ref 				= $(this);
		var $inputHBLHAWB 		= $ref.parent().find('.input-hblhawb');
		var $inputDelHBLHAWB 	= $ref.parent().parent().find('input[name="deliveries_hbl_hawb_mm[]"]')
		var hblHawb 			= $inputHBLHAWB.val();
		var delHblHawb 			= $inputDelHBLHAWB.val();

		if( hblHawb != '' ) {

			// validate HBL/HAWB
			if( delHblHawb != '' ) {
				var str1 = delHblHawb.substr(0, 3);
				var str2 = hblHawb.substr(0, 3);

				if( str1 != str2 ) {
					$inputHBLHAWB.validationEngine('showPrompt', 'Invalid HBL/HAWB<br>Must start with: '+str1, 'error');
					return false;
				}
			}

			var deliveryId 	= $(this).attr('data-delivery-id');
			var shipmentId 	= $('input[name="shipments_id[]"]').val();
			var url 		= "shipment/shipment/addHBLHAWB";
			var html 		= '<div class="input-append" class="span3"><span class="input uneditable-input">'+hblHawb+'</span><button class="btn remove-hblhawb" type="button" data-delivery-id="'+deliveryId+'"><i class="icon-remove"></i></button></div>';

			$.post(
				url,
				{deliveryId: deliveryId, shipmentId: shipmentId, hblHawb: hblHawb},
				function( data ) {
					if( data.success ) {
						$inputHBLHAWB.val('');
						$ref.parent().before(html);
						res = ( $inputDelHBLHAWB.val() != '' ) ? $inputDelHBLHAWB.val()+','+hblHawb : hblHawb;
						$inputDelHBLHAWB.val( res );
						//updateShipmentFields( data.data );
					} else {
						$ref.validationEngine('showPrompt', 'Invalid HBL/HAWB', 'error', 'topRight', true);
					}
				},
				'json'
			);

		}
	})

	$('button.remove-hblhawb').live('click', function(e) {
		var $ref 				= $(this);
		var $inputHBLHAWB 		= $ref.parent().find('span.uneditable-input');
		var $inputDelHBLHAWB 	= $ref.parent().parent().find('input[name="deliveries_hbl_hawb_mm[]"]')
		var hblHawb 			= $inputHBLHAWB.html();
		if( hblHawb != '' ) {
			var deliveryId 	= $(this).attr('data-delivery-id');
			var shipmentId 	= $('input[name="shipments_id[]"]').val();
			var url 		= "shipment/shipment/removeHBLHAWB";
			$.post(
				url,
				{deliveryId: deliveryId, shipmentId: shipmentId, hblHawb: hblHawb},
				function( data ) {
					$ref.parent().remove();
					var items = $inputDelHBLHAWB.val().split(',');
					var tmp = new Array();
					for( var i= 0; i<items.length; i++ ) {
						if( items[i] != hblHawb ) tmp.push( items[i] );
					}
					$inputDelHBLHAWB.val( tmp.join(',') );
				},
				'json'
			);
		} else {
			$ref.parent().remove();
		}
	})

}

/**
 * updateShipmentFields
 */
function updateShipmentFields( data ){

	$('select[name="shipments_incoterm_code[]"] option').removeAttr('selected');
	$('select[name="shipments_incoterm_code[]"] option[value="'+data.incoterm_code+'"]').attr('selected',true);

	$('input[name="shipments_carrier[]"]').val(data.carrier);
	$('input[name="shipments_mother_vessel_name[]"]').val(data.mother_vessel_name);
	$('input[name="shipments_departure_port[]"]').val(data.departure_port);

	$('select[name="shipments_destination_port[]"] option').removeAttr('selected');
	$('select[name="shipments_destination_port[]"]').val(data.destination_port);

	$('input[name="shipments_etd[]"]').val(data.etd);
	$('input[name="shipments_atd[]"]').val(data.atd);
	$('input[name="shipments_eta[]"]').val(data.eta);
	$('input[name="shipments_ata[]"]').val(data.ata);
	$('input[name="shipments_cus[]"]').val(data.cus);
	$('input[name="shipments_container_number[]"]').val(data.container_number);
	$('input[name="shipments_container_tare_weight[]"]').val(data.container_tare_weight);

	$('select[name="shipments_container_size[]"] option').removeAttr('selected');
	$('select[name="shipments_container_size[]"]').val(data.container_size);

	$('input[name="shipments_seal_number[]"]').val(data.seal_number);
}


/**
 * initTypeAhead
 * inits bootstrap type head feature
 */
function initTypeAhead(){
	$('.contract-number-typeahead').typeahead({
		source: function( query, process ) { $.post(
			'shipment/shipment/getContractNumbers',
			{ query: query },
			function ( data ) {
				return process( data.options );
			},
			'json'
		)}
	});
	$('.departure-typeahead').typeahead({
		minLength: 2,
		source: function( query, process ) { $.post(
			'shipment/shipment/getDeparturePorts',
			{ query: query },
			function ( data ) {
				return process( data.options );
			},
			'json'
		)}
	});

	/*$('.hblhawb-typeahead').typeahead({
		minLength: 2,
		source: function( query, process ) { $.post(
			'shipment/shipment/getHBLHAWB',
			{ query: query },
			function ( data ) {
				return process( data.options );
			},
			'json'
		)}
	});*/
}

/**
 * initHBLHAWB
 */
function initHBLHAWB(){
	var html = '';
	var hbls = [];

	$('div.delivery').each( function(){
		var $ref 	= $(this)
		var hblHawb = $('input[name="deliveries_hbl_hawb_mm[]"]', $ref ).val();
		var $telex	= $('span#telex_received', $ref );
		var css		= ( jQuery.inArray( hblHawb, hbls ) != -1 ) ? 'hide' : '';
		if( hblHawb != '' ) {
			$('select', $telex).attr('data-hbl-hawb', hblHawb).attr('data-hidden', css);
			html += '<tr class="'+css+'"><td>'+hblHawb.replace(/,/g, ', ')+'</td><td>'+$telex.html()+'</td></tr>';
			$telex.remove();
			hbls.push(hblHawb);
		}
	});

	$('table.hblhawb-table').append( html );

	$('select[name="deliveries_telex_received_mm[]"]').each( function(){
		var $select = $(this);
		var hblHawb = $select.attr('data-hbl-hawb');
		var selVal 	= $('option:selected', $select).val();

		// init all hidden selects
		changeHBLHAWBSelect( hblHawb, selVal );

		// set change handler
		$select.change(function() {
			var selVal 	= $('option:selected', $select).val();
			changeHBLHAWBSelect( hblHawb, selVal );
		});
	});
}

function changeHBLHAWBSelect( hblHawb, selVal ){

	$('select[data-hbl-hawb="'+hblHawb+'"][data-hidden="hide"]').each(function() {
		var $select = $(this);
		$('option', $select).removeAttr('selected');
		$('option[value="'+selVal+'"]', $select).attr('selected', true);
	});
}

function sendTelexReleaseMail( hblHawb ){
	var shipmentId = $('input[name="shipments_id[]"]').val();
	var url = "shipment/shipment/sendTelexReleaseMail/"+shipmentId;
	$.post(
		url,
		{shipmentId: shipmentId, hblHawb: hblHawb},
		function( data ) {},
		'json'
	);
}

/**
 * initDC
 */
function initDC(){
	$('#shipments_dc').change( function() {
		var $ref = $(this);
		var selDC = $ref.find('option:selected').text();
		var isDCFound = false;
		var delDCs = new Array();;
		var vols = new Object();
		$('.delivery-records .delivery').each( function() {
			var dc = $(this).find('input[name="deliveries_dc[]"]').val();
			if( selDC == dc ) isDCFound = true;
			delDCs.push(dc);

			var vol = $(this).find('input[name="deliveries_volume_mm[]"]').val();
			vol = parseFloat( vol )
			if( vols[dc] == undefined ) vols[dc] = 0;
			vols[dc] += vol;
		});
		if( !isDCFound ) {
			delDCs = unique( delDCs );
			console.log(delDCs);
			var str = 'Do you really want to select [HEADERDC] for this CLP? It only contains deliveries to [DELIVERYDCS].';
			str = str.replace( '[HEADERDC]', selDC );
			str = str.replace( '[DELIVERYDCS]', delDCs.join(', ') );
			$('#notMatchingDCs .modal-body p').text( str );
			$('#notMatchingDCs').modal('show');
			$('#notMatchingDCs a.submit').click( function() {
				$('#notMatchingDCs').modal('hide');
			});
			$('#notMatchingDCs a.cancel').click( function() {
				$ref.find('option').removeAttr('selected');
				$ref.find('option[value=""]').attr('selected',true);
				$('#notMatchingDCs').modal('hide');
			});
		} else {
			var isHigherVolAvail 	= false;
			var selVol 				= vols[selDC];
			var highestVolume 		= 0;
			var highestDC 			= '';
			for(var key in vols) {
				if (vols.hasOwnProperty(key)) {
					if( vols[key] > selVol ) isHigherVolAvail = true;
					if( vols[key] > highestVolume ) {
						highestVolume = vols[key];
						highestDC = key;
					}
				}
			}
			if( isHigherVolAvail ) {
				var str = '[HIGHESTDC] ([HIGHESTVOL] cbm) has a higher volume than [HEADERDC] ([SELVOL] cbm). Do you really want to select [HEADERDC] for this CLP?';
				str = str.replace( '[HEADERDC]', selDC );
				str = str.replace( '[HEADERDC]', selDC );
				str = str.replace( '[SELVOL]', selVol.toFixed(3) );
				str = str.replace( '[HIGHESTDC]', highestDC );
				str = str.replace( '[HIGHESTVOL]', highestVolume.toFixed(3) );
				$('#higherVolumeDCs .modal-body p').text( str );
				$('#higherVolumeDCs').modal('show');
				$('#higherVolumeDCs a.submit').click( function() {
					$('#higherVolumeDCs').modal('hide');
				});
				$('#higherVolumeDCs a.cancel').click( function() {
					$ref.find('option').removeAttr('selected');
					$ref.find('option[value=""]').attr('selected',true);
					$('#higherVolumeDCs').modal('hide');
				});
			}
		}
	});
}

/**
* initDG
*/
function initDG(){

	if ( $('input[name="shipments_dg"]').not( ':checked' ) ) {
		$('input[name="shipments_dg_class[]"]').attr('readonly','readonly').addClass('readonly');
	}
	$('input[name="shipments_dg"]').click( function(){
		if ( $(this).is( ':checked' ) ) {
			$('input[name="shipments_dg_class[]"]').removeAttr('readonly','readonly').removeClass('readonly');
		} else {
			$('input[name="shipments_dg_class[]"]').attr('readonly','readonly').addClass('readonly');
		}
	});
}

/**
 * initDelayReason
 */
function initDelayReason(){
	var $delayReasonGroup = $('select[name="shipments_delay_reason_group[]"]');
	var $delayReason = $('select[name="shipments_delay_reason[]"]')
	if ( $delayReasonGroup.val() == 0 ) {
		$delayReason.attr('readonly','readonly').addClass('readonly').attr('disabled','disabled');
	}
	$delayReasonGroup.change( function() {
		var group = $(this).find('option:selected').val();
		var url = "shipment/shipment/getDelayReasons";
		$.post(
			url,
			{group: group},
			function( data ) {
				$delayReason.find('option').remove();
				$(data).each( function() {
					var item = $(this);
					$delayReason.append('<option value="'+item.attr('id')+'">'+item.attr('title')+'</option>');
				});
				$delayReason.removeAttr('readonly').removeClass('readonly').removeAttr('disabled');
			},
			'json'
		);
	});
}


function unique(arr) {
	var hash = {}, result = [];
	for ( var i = 0, l = arr.length; i < l; ++i ) {
		if ( !hash.hasOwnProperty(arr[i]) ) { //it works with objects! in FF, at least
			hash[ arr[i] ] = true;
			result.push(arr[i]);
		}
	}
	return result;
}