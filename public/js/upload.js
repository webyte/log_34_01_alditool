
$(document).ready(function() {
	initUploadButtons();
	initCustomsUploadButtons();
});

/**
 * initUploadButtons
 */
function initUploadButtons(){
	$('#contract-upload-file').change(function() {
		$('#contract-upload-holder').val($(this).val());
	});

	$('select[name="upload-type"]').change(function() {
		var val = $(this).find('option:selected').val();
		if( val == "ecommerce" ) {
			$('select[name="contract-type"] option[value="6"]').attr('selected',true);
		}
	});

}

/**
 * initCustomsUploadButtons
 */
function initCustomsUploadButtons(){
	$('#customs-upload-file').change(function() {
		$('#customs-upload-holder').val($(this).val());
	});

}

