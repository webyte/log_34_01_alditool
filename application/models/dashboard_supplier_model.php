<?php
require_once( APPPATH.'models/abstract_model.php' );

class Dashboard_supplier_model extends Abstract_model {

	public static $STATUS_CONTRACT_DELAY 			= 'Delay';
	public static $STATUS_CONTRACT_ON_TIME 			= 'On time';

	function __construct() {
		parent::__construct();
		$this->load->model('Supplier_model', 'supplier_model');
		$this->load->model('Discrepancy_model', 'discrepancy_model');
		$this->load->driver('cache', array('adapter' => 'file'));
	}

	/**
	 * getRecords
	 * returns the db-records with the given search params
	 *
	 * @return array - db record as array.
	 */
	public function getRecords( $filter=array() ) {
		if( !$res = $this->cache->get('dashboard_supplier_getrecords') ) {
			$res = array();
			$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
			$this->db->from( 'contracts' );
			$this->db->join( 'deliveries', 'deliveries.contract_id = contracts.id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
			$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
			$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
			$this->db->where( array( 'contracts.hidden' => 0, 'contracts.deleted' => 0 ) );
			$this->db->where( array( 'contracts.fob_date >=' => date( 'Y-m-d', strtotime( '-25 days', time() ) ).' 00:00:00' ) );
			$this->db->where( array( 'contracts.fob_date <=' => date( 'Y-m-d', strtotime( '+25 days', time() ) ).' 00:00:00' ) );
			$this->db->group_by( 'contracts.id' );
			$this->db->order_by( 'contracts.id' );
			$query = $this->db->get();

			foreach ( $query->result_array() as $row ) {
				$res[] = $row;
			}
			$res = $this->prepareResultQueryForOutput( $res );
		}
		if( count( $filter ) > 0 ) $res = $this->filterResult( $res, $filter );
		return $res;
	}

	/**
	 * getDelayedRecordsBySupplier
	 * returns the db-records with the given search params
	 *
	 * @return array - db record as array.
	 */
	public function getDelayedRecordsBySupplier() {
		$rows = $this->getRecords();
		$delayedRows = array();
		foreach( $rows as $row ) {
			if( $row['status_contract'] == $this::$STATUS_CONTRACT_DELAY ) {
				$delayedRows[$row['contracts_supplier']] = ( isset( $delayedRows[$row['contracts_supplier']] ) ) ? intval($delayedRows[$row['contracts_supplier']]) + 1 : 1;
			}
		}
		arsort($delayedRows);
		$delayedRows = array_slice($delayedRows, 0, 5);

		return array(
			'labels' => array_keys( $delayedRows ),
			'data' => array_values( $delayedRows ),
		);
	}

	/**
	 * getRecordsByStatusContract
	 * returns the db-records with the given search params
	 *
	 * @return array - db record as array.
	 */
	public function getRecordsByStatusContract() {
		$rows = $this->getRecords();
		$delayedRows = array();
		foreach( $rows as $row ) {
			$delayedRows[$row['status_contract']] = ( isset( $delayedRows[$row['status_contract']] ) ) ? intval($delayedRows[$row['status_contract']]) + 1 : 1;
		}
		return array(
			'labels' => array(
				$this::$STATUS_CONTRACT_ON_TIME,
				$this::$STATUS_CONTRACT_DELAY
			),
			'data' => array(
				isset( $delayedRows[$this::$STATUS_CONTRACT_ON_TIME] ) ? $delayedRows[$this::$STATUS_CONTRACT_ON_TIME] : array(),
				isset( $delayedRows[$this::$STATUS_CONTRACT_DELAY] ) ? $delayedRows[$this::$STATUS_CONTRACT_DELAY] : array(),
			),
		);
	}

	/**
	 * generateCache
	 *
	 * @return
	 */
	public function generateCache() {
		$res = $this->getRecords();
		$this->cache->file->save('dashboard_supplier_getrecords', $res, 300);
	}

	/**
	 * isCached
	 *
	 * @return
	 */
	public function isCached() {
		return file_exists( $this->config->item('app_path').'cache/dashboard_ocean_freight_getrecords' );
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function prepareResultQueryForOutput( $query_result_array ){
		$res = array();

		foreach ( $query_result_array as $row) {

			$delayOptions = $this->getTCAgetMultiselectionOptions( 'contracts_delay_reason' );

			$row['status_contract'] = '';
			$row['status_contract_css'] = 'label';
			if( $row['contracts_actual_contract_ready_date'] != '0000-00-00 00:00:00' && strtotime( $row['contracts_fob_date'] ) >= strtotime( $row['contracts_actual_contract_ready_date'] ) ) {
				$row['status_contract'] = $this::$STATUS_CONTRACT_ON_TIME;
				$row['status_contract_css'] = 'label label-success';
			} else if( $row['contracts_actual_contract_ready_date'] == '0000-00-00 00:00:00' && strtotime( $row['contracts_fob_date'] ) >= time() ) {
				$row['status_contract'] = $this::$STATUS_CONTRACT_ON_TIME;
				$row['status_contract_css'] = 'label label-success';
			} else {
				$row['status_contract'] = $this::$STATUS_CONTRACT_DELAY;
				$row['status_contract_css'] = 'label label-important';
			}

			$dates = array(
				'contracts_ofu_real',
				'contracts_fob_date',
				'contracts_actual_contract_ready_date',
			);

			foreach( $dates as $date ) {
				$row[$date.'_timestamp'] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? strtotime( $row[$date] ) : 0;
				$row[$date] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? date( $this->config->item('date_format'), strtotime( $row[$date] ) ) : "";
			}

			if( isset( $row['contracts_supplier'] ) && $row['contracts_supplier'] > 0 ) {
				$supplier = $this->supplier_model->getRecord( $row['contracts_supplier'] );
				$row['contracts_supplier'] = $supplier['name'];
			}
			$row['comments_count'] =  $this->discrepancy_model->getCommentsCount( $row['contracts_id'] );
			$row['discrepancy'] = $row['comments_count'] > 0 ? 'yes' : 'no';
			$row['contracts_delay_reason'] = ( $row['contracts_delay_reason'] != 0 && array_key_exists($row['contracts_delay_reason'], $delayOptions) ) ? $delayOptions[$row['contracts_delay_reason']] : '';

			$traOptions = $this->getTCAgetMultiselectionOptions( 'contracts_traffic_type' );
			$row['contracts_traffic_type'] = array_key_exists($row['contracts_traffic_type'], $traOptions) ? $traOptions[$row['contracts_traffic_type']] : $row['contracts_traffic_type'];

			$res[] = $row;
		}
		return $res;
	}

	private function filterResult( $res, $filter )
	{
		$out = array();
		if($filter['type'] == 'status') {
			foreach( $res as $row ) {
				if ( $filter['value'] == 'ontime' ) {
					if( $row['status_contract'] == $this::$STATUS_CONTRACT_ON_TIME ) $out[] = $row;
				} else {
					if( $row['status_contract'] == $this::$STATUS_CONTRACT_DELAY ) $out[] = $row;
				}
			}
		}
		return $out;
	}

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}

}
