<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Document_model extends Abstract_model {

	public static $STATUS_DOCUMENT_MISSING 		= 0;
	public static $STATUS_DOCUMENT_UPLOADED 	= 1;
	public static $STATUS_DOCUMENT_CONFIRMED 	= 2;
	public static $DOCUMENT_TYPES 				= array(
		1 => array( 'id' => 1, 'title' => 'Commercial Invoice', 		'mandatory' => 1, 'code' => 'CIV' ),
		2 => array( 'id' => 2, 'title' => 'Packing List', 				'mandatory' => 1, 'code' => 'PKL' ),
		3 => array( 'id' => 3, 'title' => 'Packing Declaration', 		'mandatory' => 0, 'code' => 'PKD' ),
		4 => array( 'id' => 4, 'title' => 'Certificate of origin', 		'mandatory' => 0, 'code' => 'COO' ),
		5 => array( 'id' => 5, 'title' => 'Manufactures declaration', 	'mandatory' => 0, 'code' => 'MDC' ),
		6 => array( 'id' => 6, 'title' => 'Treatment certificate', 		'mandatory' => 0, 'code' => 'TRT' ),
		7 => array( 'id' => 7, 'title' => 'Health certificate', 		'mandatory' => 0, 'code' => 'HCC' ),
		8 => array( 'id' => 8, 'title' => 'Additional document', 		'mandatory' => 0, 'code' => 'MSC' ),
	);

	function __construct() {
		parent::__construct();
		$this->tablename = 'documents';
	}

	public function getDocumentTypeTitles() {
		$data = array();
		foreach( SELF::$DOCUMENT_TYPES as $key=>$documentType ) {
			$data[$key] = $documentType['title'];
		}
		return $data;
	}

	public function getMandatoryDocumentTypes( $id ) {
		$mandatoryDocumentTypes = array();

		if( $this->getCustomsDutyRate( $id ) == 'free' ) SELF::$DOCUMENT_TYPES[6]['mandatory'] = 1;

		foreach( SELF::$DOCUMENT_TYPES as $documentType ) {
			if( $documentType['mandatory'] ) $mandatoryDocumentTypes[] = $documentType['id'];
		}
		return $mandatoryDocumentTypes;
	}

	public function getMandatoryByDocumentType( $documentType, $id=0 ) {
		if( $this->getCustomsDutyRate( $id ) == 'free' ) SELF::$DOCUMENT_TYPES[6]['mandatory'] = 1;
		return SELF::$DOCUMENT_TYPES[$documentType]['mandatory'];
	}

	public function getDocumentsByPid( $pid ) {
		$data = array();
		$rows = $this->document_model->getRecordsWhere( array( 'shipments_deliveries_mm_id' => $pid, 'hidden' => 0, 'deleted' => 0 ) );

		if( $this->getCustomsDutyRate( $pid ) == 'free' ) SELF::$DOCUMENT_TYPES[6]['mandatory'] = 1;

		foreach( SELF::$DOCUMENT_TYPES as $documentType ) {
			if( $documentType['mandatory'] ) $rows = $this->addMandatoryDocumentIfMissing( $documentType['id'], $pid, $rows );
		}

		foreach( $rows as $row ) {
			$row['document_type_title'] = SELF::$DOCUMENT_TYPES[$row['document_type']]['title'];
			$row['mandatory_title'] 	= ( SELF::$DOCUMENT_TYPES[$row['document_type']]['mandatory'] == 1 ) ? 'Yes' : 'No';
			$data[] = $row;
		}
		return $data;
	}

	public function export( $recordId, $documentId )
	{
		$this->load->model('Supplier_portal_model', 'supplier_portal_model');
		$row 			= $this->getRecord( $documentId );
		$documentType 	= SELF::$DOCUMENT_TYPES[$row['document_type']];
		$file 			= $row['file'];
		$uploadPath 	= $this->config->item('documents_upload_path');
		$exportPath 	= $this->config->item('export_path') . 'supplier/';
		$fileInfo 		= pathinfo($uploadPath.$file );
		$fileExt 		= $fileInfo['extension'];

		$this->updateCounter( $documentId );
		$counter 		= $this->getTotalCounter();
		$code 			= $documentType['code'];
		$title 			= str_replace(' ', '', $documentType['title']);
		$shipmentNumber = $this->supplier_portal_model->getShipmentNumberById( $recordId );
		$newFile 		= '[SHP'.$code.$title.$shipmentNumber.'C@0]_['.$counter.'].'.$fileExt;

		copy( $uploadPath.$file, $exportPath.$newFile );
	}

	private function getCustomsDutyRate( $id ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->model('Customs_model', 'customs_model');

		$row = $this->shipment_delivery_model->getRecord( $id );
		$deliveryId = $row['delivery_id'];
		$row = $this->delivery_model->getRecord( $deliveryId );
		$contractId = $row['contract_id'];
		$rows = $this->customs_model->getRecordsWhere( array( 'contract_id' => $contractId, 'hidden' => 0, 'deleted'  => 0 ) );

		$out = ( count( $rows ) > 0 ) ? strtolower( $rows[0]['customs_duty_rate_incl_fta'] ) : '';
		return $out;
	}

	private function addMandatoryDocumentIfMissing( $documentType, $pid, $rows ) {
		$isThere = false;
		foreach( $rows as $row ) {
			if( $row['document_type'] == $documentType ) $isThere = true;
		}

		if( !$isThere ) {
			$data = array();
			$data['shipments_deliveries_mm_id'] = $pid;
			$data['document_type'] = $documentType;
			$data['mandatory'] = 1;
			$data['uploaded'] = '';
			$data['status'] = 0;
			$data['confirmed'] = 0;
			$data['file'] = '';
			$rows[] = $data;
		}
		return $rows;
	}

	private function updateCounter( $id ) {
		$row = $this->getRecord( $id );
		$this->updateRecord( $id, ['export_counter' => $row['export_counter'] + 1 ] );
	}

	private function getTotalCounter() {
		$this->db->select('SUM(export_counter) AS export_counter');
		$this->db->from('documents');
		$query = $this->db->get();
		$res = $query->result_array();
		$counter = $res[0]['export_counter'];
		return sprintf("%05d", $counter);
	}

}

/* End of file document_model.php */
/* Location: ./app/models/document_model.php */
