<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Container_type_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'container_types';
	}

}

/* End of file office.php */
/* Location: ./app/models/office.php */
