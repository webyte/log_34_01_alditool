<?php
require_once ( APPPATH.'models/abstract_model'.EXT);

class Delivery_due_model extends Abstract_model {

    function __construct() {
        parent::__construct();
        $this->tablename = 'delivery_dues';
    }

	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select('*');
		$this->db->from( $this->tablename );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * getDeliveryDueDate
	 */
	public function getDeliveryDueDate( $week, $year, $isEndDate = FALSE ) {
		$d = '';
		$rows = $this->getRecordsWhere( array(
			'sales_week' => $week,
			'sales_year' => $year,
			'hidden' => 0,
			'deleted' => 0,
		));
		if( count( $rows ) > 0 ) {
			$d = ( $isEndDate ) ? $rows[0]['delivery_due_end_date'] : $rows[0]['delivery_due_date'];
		}
		return $d;
	}

	private function prepareResultQueryForOverviewOutput( $query_result_array ){
		$res 		= array();
		foreach ( $query_result_array as $row) {
			$row['delivery_due_date'] = ( $row['delivery_due_date'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['delivery_due_date'] ) ) : "";
			$row['delivery_due_end_date'] = ( $row['delivery_due_end_date'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['delivery_due_end_date'] ) ) : "";
			$res[] = $row;
		}
		return $res;
	}

}
