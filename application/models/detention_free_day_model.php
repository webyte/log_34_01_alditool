<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Detention_free_day_model extends Abstract_model {

    function __construct() {
        parent::__construct();
        $this->tablename = 'detention_free_days';
    }

	/**
	 * isUnique
	 *
	 * @param string $contractNumber
	 * @param int $containerType
	 * @return boolean
	 */
	public function isUnique( $contractNumber, $containerType, $id ) {
		$records = $this->getRecordsWhere( array( 'contract_number' => $contractNumber, 'container_type' => $containerType, 'id !=' => $id, 'deleted' => 0, 'hidden' => 0 ) );
		return ( count( $records ) == 0 );
	}

}

/* End of file supplier.php */
/* Location: ./app/models/supplier.php */
