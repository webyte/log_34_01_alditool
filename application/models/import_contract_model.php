<?php
require_once( APPPATH.'models/abstract_import_model'.EXT );

class Import_contract_model extends Abstract_import_model {

	public static $CONTRACT_TYPE_CORE_RANGE = 1;
	public static $CONTRACT_TYPE_STORE_EQUIPMENT = 2;
	public static $CONTRACT_TYPE_SPECIAL_BUYS = 3;
	public static $CONTRACT_TYPE_AUI = 8;


	function __construct() {
		parent::__construct();
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->helper( 'str_helper' );
		$this->lockSuffix = '_CONTRACT';
	}

	/**
	 * setImportSubPath
	 * @param $path
	 * @return
	 */
	public function setImportSubPath( $path ) {
		$this->importSubPath = $path;
	}

	/**
	 * setContractType
	 * @param $contractType
	 * @return
	 */
	public function setContractType( $contractType ) {
		$this->contractType = $contractType;
	}

	/**
	 * readEventFile
	 * @param $file
	 * @return boolean
	 */
	public function readEventFile( $file ) {
		$this->welogger->log( "readEventFile called with file ".$file, WELogger::$LOG_LEVEL_INFO, "Import.readEventFile" );
		$this->log('readEventFile called with file '.$file);

		$success	= TRUE;
		$path 		= $this->config->item('import_path').$this->importSubPath;

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$src 			= $path.$file;
		$objPHPExcel 	= IOFactory::load( $src );
		$objWorksheet 	= $objPHPExcel->getActiveSheet();
		$contracts 		= $this->getContracts( $objWorksheet );

		$this->saveToDB( $contracts );
		$this->sendFilePerMail( $path.$file);

		// move file to archive
		if( ENVIRONMENT == 'production' || ENVIRONMENT == 'staging' ) $this->moveFileToArchive( $file );
		return $success;
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	/**
	 * getRegularContracts
	 * @param $objWorksheet
	 * @return array
	 */
	private function getContracts( $objWorksheet ) {

		$this->load->model('Delivery_due_model', 'delivery_due_model');
		$i			= 1;
		$contracts	= array();

		foreach ($objWorksheet->getRowIterator() as $row) {
			if( $i > 1 ) {
				$contractNumber = (string) $objWorksheet->getCellByColumnAndRow(2, $i)->getValue();
				if( !empty( $contractNumber ) ) {

					if( !isset( $orders[$contractNumber] ) ) {
						$advertisementWeek 	= '';
						$advertisementYear 	= '';
						$contractType 		= $this->contractType;
						$productCode 		= (string) $objWorksheet->getCellByColumnAndRow(0, $i)->getValue();
						$productDescription = (string) $objWorksheet->getCellByColumnAndRow(1, $i)->getValue();
						$supplier 			= $this->getSupplierId( (string) $objWorksheet->getCellByColumnAndRow(15, $i)->getValue(), (string) $objWorksheet->getCellByColumnAndRow(14, $i)->getValue(), $contractNumber );
						$incotermCode		= 2; // FOB
						$nearestPort		= (string) $objWorksheet->getCellByColumnAndRow(12, $i)->getValue();
						$loo				= $this->getLogwinOriginOffice( $nearestPort );
						$freightOrderNumber = (string) $objWorksheet->getCellByColumnAndRow(55, $i)->getValue();
						$tt					= (string) $objWorksheet->getCellByColumnAndRow(13, $i)->getValue();
						$trafficType		= $this->getTrafficType( $tt );
						$orderStatus 		= (string) $objWorksheet->getCellByColumnAndRow(56, $i)->getValue();

						$caseLength		= (float) $objWorksheet->getCellByColumnAndRow(4, $i)->getValue();
						$caseWidth 		= (float) $objWorksheet->getCellByColumnAndRow(5, $i)->getValue();
						$caseHeight		= (float) $objWorksheet->getCellByColumnAndRow(6, $i)->getValue();
						$caseWeight		= (float) $objWorksheet->getCellByColumnAndRow(3, $i)->getValue();
						$caseQuantity1 	= (float) $objWorksheet->getCellByColumnAndRow(20, $i)->getCalculatedValue();
						$caseQuantity2 	= (float) $objWorksheet->getCellByColumnAndRow(22, $i)->getCalculatedValue();
						$caseQuantity3 	= (float) $objWorksheet->getCellByColumnAndRow(24, $i)->getCalculatedValue();
						$caseQuantity4 	= (float) $objWorksheet->getCellByColumnAndRow(25, $i)->getCalculatedValue();
						$caseQuantity5 	= (float) $objWorksheet->getCellByColumnAndRow(26, $i)->getCalculatedValue();
						$caseQuantity6 	= (float) $objWorksheet->getCellByColumnAndRow(27, $i)->getCalculatedValue();
						$caseQuantity7 	= (float) $objWorksheet->getCellByColumnAndRow(28, $i)->getCalculatedValue();
						$caseQuantity8 	= (float) $objWorksheet->getCellByColumnAndRow(29, $i)->getCalculatedValue();
						$caseQuantity9 	= (float) $objWorksheet->getCellByColumnAndRow(30, $i)->getCalculatedValue();
						$caseQuantity10	= (float) $objWorksheet->getCellByColumnAndRow(31, $i)->getCalculatedValue();
						$caseQuantity11	= (float) $objWorksheet->getCellByColumnAndRow(32, $i)->getCalculatedValue();
						$caseQuantity12	= (float) $objWorksheet->getCellByColumnAndRow(33, $i)->getCalculatedValue();
						$caseQuantity13	= (float) $objWorksheet->getCellByColumnAndRow(34, $i)->getCalculatedValue();
						$caseQuantity14 = (float) $objWorksheet->getCellByColumnAndRow(35, $i)->getCalculatedValue();
						$caseQuantity15 = (float) $objWorksheet->getCellByColumnAndRow(23, $i)->getCalculatedValue();
						$caseQuantity16 = (float) $objWorksheet->getCellByColumnAndRow(21, $i)->getCalculatedValue();
						$totalCases 	= $caseQuantity1 + $caseQuantity2 + $caseQuantity3 + $caseQuantity4 + $caseQuantity5 + $caseQuantity6 + $caseQuantity7 + $caseQuantity8 + $caseQuantity9 + $caseQuantity10 + $caseQuantity11 + $caseQuantity12 + $caseQuantity13 + $caseQuantity14 + $caseQuantity15;
						$unitsPerCase	= (string) $objWorksheet->getCellByColumnAndRow(8, $i)->getValue();
						$caseVolume		= ( $caseLength / 1000 ) * ( $caseWidth / 1000 ) * ( $caseHeight / 1000 ); // convert millimeter in meter

						$volume 		= $caseVolume * $totalCases;
						$grossWeight 	= $caseWeight * $totalCases;

						switch( $this->contractType ) {
							case Import_contract_model::$CONTRACT_TYPE_CORE_RANGE:
								$deliveryDueDate 	= $this->formatDate( $objWorksheet->getCellByColumnAndRow(10, $i)->getValue() );
								$deliveryDueEndDate = '0000-00-00 00:00:00';
								$cubeQACheck 		= 1;
								break;
							case Import_contract_model::$CONTRACT_TYPE_AUI:
								$deliveryDueDate 	= $this->formatDate( $objWorksheet->getCellByColumnAndRow(10, $i)->getValue() );
								$deliveryDueEndDate = '0000-00-00 00:00:00';
								$cubeQACheck 		= 1;
								break;
							default:
								$deliveryDueDate = $this->delivery_due_model->getDeliveryDueDate( $advertisementWeek, $advertisementYear );
								$deliveryDueEndDate = $this->delivery_due_model->getDeliveryDueDate( $advertisementWeek, $advertisementYear, true );
								$cubeQACheck = 1;
						}

						$contracts[$contractNumber] = array(
							'contract_number' 	=> $contractNumber,
							'departure' 		=> (string) $objWorksheet->getCellByColumnAndRow(12, $i)->getValue(),
							'product_code' 			=> $productCode,
							'product_description' 	=> $productDescription,
							'volume'			=> $volume,
							'gross_weight'		=> $grossWeight,
							'total_cases'		=> $totalCases,
							'ofu_scheduled' 	=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(9, $i)->getValue(), ' -21 days' ),
							'on_sale_date' 		=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(19, $i)->getValue() ),
							'fob_date' 			=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(9, $i)->getValue() ),
							'initial_crd' 		=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(9, $i)->getValue() ),
							'initial_sales_date'=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(19, $i)->getValue() ),
							'aldi_au_wh_delivery_due_date' => $deliveryDueDate,
							'aldi_au_wh_delivery_due_end_date' => $deliveryDueEndDate,
							'advertisement_week' => $advertisementWeek,
							'advertisement_year' => $advertisementYear,
							'supplier' 			=> $supplier,
							'units_per_case' 	=> $unitsPerCase,
							'case_length' 		=> $caseLength,
							'case_width' 		=> $caseWidth,
							'case_height' 		=> $caseHeight,
							'case_weight' 		=> $caseWeight,
							'case_cubage'		=> $caseVolume,
							'contract_type'		=> $contractType,
							'incoterm_code'		=> $incotermCode,
							'logwin_origin_office' => $loo,
							'freight_order_number' => $freightOrderNumber,
							'traffic_type' 		=> $trafficType,
							'cube_qa_check'		=> $cubeQACheck,
							'order_status'		=> $orderStatus,
							'deliveries' 		=> array(),
						);
					}

					if( $contractType == Import_contract_model::$CONTRACT_TYPE_CORE_RANGE ) {
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' => 'MIN',
							'destination' => 'SYD',
							'case_quantity' => $caseQuantity1,
							'unit_quantity' => $caseQuantity1 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity1,
							'unit_quantity_remain' => $caseQuantity1 * $unitsPerCase,
							'volume' => $caseQuantity1 * $caseVolume,
							'gross_weight' => $caseQuantity1 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' => $productCode,
							'product_description' => $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' => 'DER',
							'destination' => 'MEL',
							'case_quantity' => $caseQuantity2,
							'unit_quantity' => $caseQuantity2 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity2,
							'unit_quantity_remain' => $caseQuantity2 * $unitsPerCase,
							'volume' => $caseQuantity2 * $caseVolume,
							'gross_weight' => $caseQuantity2 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' => $productCode,
							'product_description' => $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' => 'STP',
							'destination' => 'BNE',
							'case_quantity' => $caseQuantity3,
							'unit_quantity' => $caseQuantity3 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity3,
							'unit_quantity_remain' => $caseQuantity3 * $unitsPerCase,
							'volume' => $caseQuantity3 * $caseVolume,
							'gross_weight' => $caseQuantity3 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' => $productCode,
							'product_description' => $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' => 'PRE',
							'destination' => 'SYD',
							'case_quantity' => $caseQuantity4,
							'unit_quantity' => $caseQuantity4 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity4,
							'unit_quantity_remain' => $caseQuantity4 * $unitsPerCase,
							'volume' => $caseQuantity4 * $caseVolume,
							'gross_weight' => $caseQuantity4 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' => $productCode,
							'product_description' => $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' => 'DAN',
							'destination' => 'MEL',
							'case_quantity' => $caseQuantity5,
							'unit_quantity' => $caseQuantity5 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity5,
							'unit_quantity_remain' => $caseQuantity5 * $unitsPerCase,
							'volume' => $caseQuantity5 * $caseVolume,
							'gross_weight' => $caseQuantity5 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' => $productCode,
							'product_description' => $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' => 'BRE',
							'destination' => 'BNE',
							'case_quantity' => $caseQuantity6,
							'unit_quantity' => $caseQuantity6 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity6,
							'unit_quantity_remain' => $caseQuantity6 * $unitsPerCase,
							'volume' => $caseQuantity6 * $caseVolume,
							'gross_weight' => $caseQuantity6 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' => $productCode,
							'product_description' => $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' => 'JKT',
							'destination' => 'FRE',
							'case_quantity' => $caseQuantity7,
							'unit_quantity' => $caseQuantity7 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity7,
							'unit_quantity_remain' => $caseQuantity7 * $unitsPerCase,
							'volume' => $caseQuantity7 * $caseVolume,
							'gross_weight' => $caseQuantity7 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' => $productCode,
							'product_description' => $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' => 'RGY',
							'destination' => 'ADL',
							'case_quantity' => $caseQuantity8,
							'unit_quantity' => $caseQuantity8 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity8,
							'unit_quantity_remain' => $caseQuantity8 * $unitsPerCase,
							'volume' => $caseQuantity8 * $caseVolume,
							'gross_weight' => $caseQuantity8 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' => $productCode,
							'product_description' => $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' => 'TRU',
							'destination' => 'AUMEL',
							'case_quantity' => $caseQuantity15,
							'unit_quantity' => $caseQuantity15 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity15,
							'unit_quantity_remain' => $caseQuantity15 * $unitsPerCase,
							'volume' => $caseQuantity15 * $caseVolume,
							'gross_weight' => $caseQuantity15 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' => $productCode,
							'product_description' => $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' => 'ECK',
							'destination' => 'AUSYD',
							'case_quantity' => $caseQuantity16,
							'unit_quantity' => $caseQuantity16 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity16,
							'unit_quantity_remain' => $caseQuantity16 * $unitsPerCase,
							'volume' => $caseQuantity16 * $caseVolume,
							'gross_weight' => $caseQuantity16 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' => $productCode,
							'product_description' => $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
					}
					if( $contractType == Import_contract_model::$CONTRACT_TYPE_AUI ) {
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' 			=> 'AUIBNAM',
							'destination' 	=> 'AUBNE',
							'case_quantity' => $caseQuantity9,
							'unit_quantity' => $caseQuantity9 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity9,
							'unit_quantity_remain' => $caseQuantity9 * $unitsPerCase,
							'volume'		=> $caseQuantity9 * $caseVolume,
							'gross_weight'	=> $caseQuantity9 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' 			=> $productCode,
							'product_description' 	=> $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' 			=> 'AUIBNAC',
							'destination' 	=> 'AUBNE',
							'case_quantity' => $caseQuantity10,
							'unit_quantity' => $caseQuantity10 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity10,
							'unit_quantity_remain' => $caseQuantity10 * $unitsPerCase,
							'volume'		=> $caseQuantity10 * $caseVolume,
							'gross_weight'	=> $caseQuantity10 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' 			=> $productCode,
							'product_description' 	=> $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' 			=> 'AUISYAM',
							'destination' 	=> 'AUSYD',
							'case_quantity' => $caseQuantity11,
							'unit_quantity' => $caseQuantity11 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity11,
							'unit_quantity_remain' => $caseQuantity11 * $unitsPerCase,
							'volume'		=> $caseQuantity11 * $caseVolume,
							'gross_weight'	=> $caseQuantity11 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' 			=> $productCode,
							'product_description' 	=> $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' 			=> 'AUISYAC',
							'destination' 	=> 'AUSYD',
							'case_quantity' => $caseQuantity12,
							'unit_quantity' => $caseQuantity12 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity12,
							'unit_quantity_remain' => $caseQuantity12 * $unitsPerCase,
							'volume'		=> $caseQuantity12 * $caseVolume,
							'gross_weight'	=> $caseQuantity12 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' 			=> $productCode,
							'product_description' 	=> $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' 			=> 'AUIMEAM',
							'destination' 	=> 'AUMEL',
							'case_quantity' => $caseQuantity13,
							'unit_quantity' => $caseQuantity13 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity13,
							'unit_quantity_remain' => $caseQuantity13 * $unitsPerCase,
							'volume'		=> $caseQuantity13 * $caseVolume,
							'gross_weight'	=> $caseQuantity13 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' 			=> $productCode,
							'product_description' 	=> $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
						$contracts[$contractNumber]['deliveries'][] = array(
							'dc' 			=> 'AUIMEAC',
							'destination' 	=> 'AUMEL',
							'case_quantity' => $caseQuantity14,
							'unit_quantity' => $caseQuantity14 * $unitsPerCase,
							'case_quantity_remain' => $caseQuantity14,
							'unit_quantity_remain' => $caseQuantity14 * $unitsPerCase,
							'volume'		=> $caseQuantity14 * $caseVolume,
							'gross_weight'	=> $caseQuantity14 * $caseWeight,
							'contract_number' => $contractNumber,
							'product_code' 			=> $productCode,
							'product_description' 	=> $productDescription,
							'advertisement_week' => $advertisementWeek,
						);
					}
				}
			}
			$i++;
		}
		return $contracts;
	}

	/**
	 * saveToDB
	 * @param
	 * @return array
	 */
	private function saveToDB( $data ){

		foreach( $data as $value ) {
			// save contract
			$contractId = $this->contract_model->getIdByContractNumber( $value['contract_number'] );
			$isUpdate = ( $contractId > 0 ) ? true : false;
			$previousRow = ( $contractId > 0 ) ? $this->contract_model->getRecord( $contractId ) : array();

			// delete contract if order status == Y
			if( $value['order_status'] == 'Y' ) {
				$this->contract_model->updateRecord( $contractId, array( 'deleted' => 1 ) );
				continue;
			}

			$contract = $value;
			unset( $contract['deliveries'] );
			unset( $contract['order_status'] );
			if( $isUpdate ) {
				unset( $contract['contract_number'] );
				unset( $contract['product_code'] );
				unset( $contract['product_description'] );
				unset( $contract['supplier'] );
				unset( $contract['departure'] );
				unset( $contract['freight_order_number'] );

				if( $previousRow['initial_crd'] != '0000-00-00 00:00:00' ) unset( $contract['initial_crd'] );
				if( $previousRow['initial_sales_date'] != '0000-00-00 00:00:00' ) unset( $contract['initial_sales_date'] );
			}

			if( $contractId > 0 ) {
				$this->contract_model->updateRecord( $contractId, $contract );
			} else {
				$contractId = $this->contract_model->createNew( $contract );
			}

			// send inform mails
			if( !$isUpdate ) $this->sendInformMails( $contract, $contractId );

			// save deliveries
			$deliveries = $value['deliveries'];
			foreach( $deliveries as $delivery ) {
				$deliveryId = $this->delivery_model->getIdByDCAndContractId( $delivery['dc'], $contractId );
				$delivery['contract_id'] = $contractId;

				if( $deliveryId > 0 ) {
					unset( $delivery['contract_number'] );
					unset( $delivery['product_code'] );
					unset( $delivery['product_description'] );
					$this->delivery_model->updateRecord( $deliveryId, $delivery );
				} else {
					$articleId = $this->delivery_model->createNew( $delivery );
				}
			}

			//process event dates
			$latestRow = $this->contract_model->getRecord( $contractId );
			$this->contract_model->processEventDates( $latestRow, $previousRow );
		}
	}

	/**
	 * formatDate
	 * @param $date
	 * @return string
	 */
	private function formatDate( $date, $offset='' ) {
		if( !empty( $date ) && stristr( $date, '.' ) === FALSE ) {
			$date = str_replace('/', '.', $date);
			$out = date( 'Y-m-d H:i:s', strtotime( date( 'Y-m-d H:i:s', strtotime( $date ) ).$offset ) );
		} else {
			$out = '0000-00-00 00:00:00';
		}
		return $out;
	}

	/**
	 * getSupplierId
	 * @param string
	 * @return string
	 */
	private function getSupplierId( $supplierCode, $supplierName, $contractNumber ){
		$this->load->model('Supplier_model', 'supplier_model');
		$out = '';
		$suppliers = $this->supplier_model->getRecordsWhere( array( 'code' => $supplierCode, 'hidden' => 0, 'deleted' => 0 ) );
		//$suppliers = $this->supplier_model->getRecordsWhere( array( 'name' => $code, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $suppliers ) > 0 ) {
			$out = $suppliers[0]['id'];
		} else {
			$this->sendSupplierMissingMail( $supplierCode, $supplierName, $contractNumber );
		}
		return $out;
	}

	/**
	 * getLogwinOriginOffice
	 * @param $port
	 * @return string
	 */
	private function getLogwinOriginOffice( $port )
	{
		$out = '';
		if( empty( $port ) ) return $out;
		$this->load->model('Office_model', 'office_model');
		$offices = $this->office_model->getRecordsWhere(array('ports LIKE' => '%' . $port . '%', 'hidden' => 0, 'deleted' => 0));
		if (count($offices) > 0) {
			$out = $offices[0]['id'];
		}
		return $out;
	}

	/**
	 * getTrafficType
	 * @param $str
	 * @return string
	 */
	private function getTrafficType( $str )
	{
		$out = '';
		switch( $str ) {
			case 'Factory Load':
				$out = 1;
				break;
		}
		return $out;
	}

	/**
	 * sendSupplierMissingMail
	 * @return boolean
	 */
	private function sendSupplierMissingMail( $supplierCode, $supplierName, $contractNumber ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= ( ENVIRONMENT == 'production' ) ? array('ALDICSV@logwin-logistics.com','stefanie.stahl@logwin-logistics.com') : array('stefanie.stahl@logwin-logistics.com');
		$cc			= ( ENVIRONMENT == 'production' ) ? '' : array('or@webyte.org');
		$subject 	= '[CONTRACT_NUMBER] - New supplier';
		$body 		= 'Dear User,

Please note that we received a new supplier for contract [CONTRACT_NUMBER]

Supplier Code: [SUPPLIER_CODE]
Supplier Name: [SUPPLIER_NAME]

Please register the supplier in the Aldi tool.
After that please select the supplier in the contract and sent out the pre-alert.';

		$mailData['SUPPLIER_CODE'] = $supplierCode;
		$mailData['SUPPLIER_NAME'] = $supplierName;
		$mailData['CONTRACT_NUMBER'] = $contractNumber;
		$subject = replacePattern( $subject, $mailData );
		$body = replacePattern( $body, $mailData );

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		$this->email->cc( $cc );

		$sendSuccess = $this->email->send();
		$this->email->clear( true );

		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Reminder_cron.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Reminder_cron.sendMail' );
			return true;
		}
	}

	/**
	 * sendFilePerMail
	 * @return boolean
	 */
	private function sendFilePerMail( $file ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= ( ENVIRONMENT == 'production' ) ? array('ALDICSV@logwin-logistics.com','stefanie.stahl@logwin-logistics.com') : array('stefanie.stahl@logwin-logistics.com');
		$cc			= ( ENVIRONMENT == 'production' ) ? '' : array('or@webyte.org');
		$subject 	= ( $this->contractType == Import_contract_model::$CONTRACT_TYPE_CORE_RANGE ) ? 'Core Range Contracts Uploaded' : 'AUI Contracts Uploaded';
		$body 		= 'Please find attached a list of contracts that have been just uploaded to the Aldi tool.';

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		$this->email->attach( $file );
		$this->email->cc( $cc );

		$sendSuccess = $this->email->send();
		$this->email->clear( true );

		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Reminder_cron.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Reminder_cron.sendMail' );
			return true;
		}
	}

	/**
	 * sendInformMails
	 * @param $contract
	 * @return
	 */
	private function sendInformMails( $contract, $contractId ) {
		// send logwin origin office mail missing
		if( empty( $contract['logwin_origin_office'] ) ) {
			$data['subject'] = 'Export office missing for contract '.$contract['contract_number'];
			$data['body'] = 'Dear User,

Please note that for contract '.$contract['contract_number'].' no Logwin export office could have been assigned.
Therefore no e-mail was sent out.

Please enter the correct export office manually in the Aldi tool and trigger the e-mail.
Please sent the departure port and the corresponding export brancht to lots@logwin-logistics.com, so that the export office will be correctly assigned in the future.';
			$data['recipient'] 	= ( ENVIRONMENT == 'production' ) ? array( 'ALDICSV@logwin-logistics.com', 'stefanie.stahl@logwin-logistics.com' ) : array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
			$this->doSendMail( $data );
		}

		// send supplier mail
		if( !empty( $contract['supplier'] ) ) {
			$this->load->model('Contactperson_model', 'contactperson_model');
			$this->load->model('Office_model', 'office_model');
			$recipients = $this->contactperson_model->getDefaultEMailsForSupplier( $contract['supplier'] );
			$office 	= $this->office_model->getRecord( $contract['logwin_origin_office'] );
			$looMail 	= $office['contact_mail'];

			$data = $this->getSupplierInformMailData( $contract );
			$data['recipient'] = ( ENVIRONMENT == 'production' ) ? $recipients : array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
			$data['cc'] 		= ( ENVIRONMENT == 'production' ) ? $looMail : '';
			$this->doSendMail( $data );
			$this->contract_model->updateRecord( $contractId, array( 'supplier_mail_sent' => 1 ) );
		}
	}

	private function getLooInformMailData( $recorddata ) {
		$mailData['CONTRACT_NUMBER'] 				= $recorddata['contract_number'];
		$mailData['ADVERTISEMENT_WEEK'] 			= $recorddata['advertisement_week'];
		$mailData['ALDI_AU_WH_DELIVERY_DUE_DATE'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['aldi_au_wh_delivery_due_date'] ) );
		$mailData['ALDI_AU_WH_DELIVERY_DUE_DATE_MINUS_SEVEN_DAYS'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['aldi_au_wh_delivery_due_date'].' -7 Days' ) );
		$mailData['ALDI_AU_WH_DELIVERY_DUE_DATE_MINUS_TEN_DAYS'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['aldi_au_wh_delivery_due_date'].' -10 Days' ) );
		$subject 					= replacePattern( $this->lang->line( 'loo.inform.mail.subject' ), $mailData );
		if($recorddata['contract_type'] == Contract_model::$CONTRACT_TYPE_CORE_RANGE ){
			$body						= replacePattern( $this->lang->line( 'loo.inform.mail.body_core_range' ), $mailData );
		}else{
			$body						= replacePattern( $this->lang->line( 'loo.inform.mail.body' ), $mailData );
		}
		return array( 'subject' => $subject, 'body' => $body );
	}

	/**
	 * @param $recorddata
	 * @return array
	 */
	private function getSupplierInformMailData( $recorddata ) {
		$loo                        = $this->getLooAddress( $recorddata['logwin_origin_office'] );
		$mailData['RECIPIENT']		= 'User';
		$mailData['FOB_DATE'] 		= date( $this->config->item('date_format'), strtotime( $recorddata['fob_date'] ) );
		$mailData['OFU_SCHEDULED'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['ofu_scheduled'] ) );
		$mailData['DEPARTURE'] 		= $recorddata['departure'];
		$mailData['LOO_NAME'] 		= $loo['name'];
		$mailData['LOO_STREET'] 	= $loo['street'];
		$mailData['LOO_CITY'] 	    = $loo['city'];
		$mailData['LOO_PHONE'] 	    = $loo['contact_phone'];
		$mailData['LOO_MAIL'] 	    = $loo['contact_mail'];
		$mailData['INFO_DATE']      = date( 'd/m/Y', strtotime( date( 'Y-m-d H:i:s', strtotime( $recorddata['fob_date'] ) ).' -35 days' ) );
		$mailData['CONTACT_NAME'] 	= $this->getSupplierContactName( $recorddata['supplier'] );
		$mailData['CONTRACT_NUMBER']= $recorddata['contract_number'];
		$mailData['DESCRIPTION'] 	= $recorddata['product_description'];
		$subject 					= replacePattern( $this->lang->line( 'supplier.inform.mail.subject' ), $mailData );
		$body						= replacePattern( $this->lang->line( 'supplier.inform.mail.body' ), $mailData );
		return array( 'subject' => $subject, 'body' => $body );
	}

	private function getLooName( $id = 0 ) {
		$out = '';
		$this->load->model('Office_model', 'office_model');
		$record = $this->office_model->getRecord( $id );
		if( count( $record ) > 0 ) {
			if( !empty( $record['name'] ) ) $out = $record['name'];
		}
		return $out;
	}

	private function getLooAddress( $id = 0 ) {
		$this->load->model('Office_model', 'office_model');
		$record = $this->office_model->getRecord( $id );
		return $record;
	}

	private function getSupplierContactName( $id = 0 ) {
		$out = '';
		$this->load->model('Supplier_model', 'supplier_model');
		$record = $this->supplier_model->getRecord( $id );
		if( count( $record ) > 0 ) $out = $record['contact_name'];
		return $out;
	}

	/**
	 * doSendMail
	 * @return boolean
	 */
	private function doSendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$cc			= isset( $data['cc'] ) ? $data['cc'] : '';
		$subject 	= $data['subject'];
		$body 		= nl2br( $data['body'] );

		$this->email->set_mailtype( 'html' );
		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->cc( $cc );
		$this->email->subject( $subject );
		$this->email->message( $body );

		$sendSuccess = $this->email->send();
		$this->email->clear( true );

		if( !$sendSuccess ) {
			$this->welogger->log( "Sending Mail to ".$toMail." failed, error: ".$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, "Contract.doSendMail" );
			return false;
		} else {
			$this->welogger->log( "Mail was sent to ".$toMail." \nwith subject: ".$subject." \nand message: ".$body, WELogger::$LOG_LEVEL_INFO, "Contract.doSendMail" );
			return true;
		}
	}
}
/* End of file import_model.php */
/* Location: ./app/models/import_model.php */
