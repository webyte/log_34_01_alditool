<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Pcshipment_delivery_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'pcshipments_deliveries_mm';
	}

//	/**
//	 * updateRecordMM
//	 * updates the record with the given id
//	 *
//	 * @param int $id - row id
//	 * @param int $id - row id
//	 * @param array $insertData - updated data
//	 */
//	public function updateRecordMM( $shipmentId, $deliveryId, $insertData ) {
//		$this->welogger->log( "updateRecordMM called with shipmentId: ".$shipmentId." and deliveryId: ".$deliveryId." and date: ".serialize( $insertData ), WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.updateRecordMM" );
//		$records = $this->pcshipments_deliveries_mm->getRecordsWhere( array( 'delivery_id' => $deliveryId, 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
//		if( count( $records ) > 0 ) {
//			$id = $records[0]['id'];
//			$this->updateRecord( $id, $insertData );
//		}
//	}

	/**
	 * addRelation
	 * updates the record with the given id
	 *
	 * @param int $id - row id
	 * @param int $id - row id
	 * @param string $hblHawb
	 * @return int - id
	 */
	public function addRelation( $shipmentId, $pcshipmentId, $deliveryId ) {
		$data = array(
			'shipment_id' => $shipmentId,
			'pcshipment_id' => $pcshipmentId,
			'delivery_id' => $deliveryId,
		);
		$id = $this->createNew($data);
		return $id;
	}

	/**
	 * removeRelation
	 * updates the record with the given id
	 *
	 * @param int $id - row id
	 * @param int $id - row id
	 * @param array $insertData - updated data
	 */
	public function removeRelation( $shipmentId, $pcshipmentId, $deliveryId ) {
		$id = 0;
		$records = $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'pcshipment_id' => $pcshipmentId, 'delivery_id' => $deliveryId, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $records ) > 0 ) {
			$id = $records[0]['id'];
		}
		$this->deleteRecord( $id );
	}

	/**
	 * getPcshipmentIdfromHBLHAWB
	 * returns correspondending id
	 *
	 * @param string $hblHawb
	 * @param string $containerNumber
	 * @return int - id
	 */
	public function getPcshipmentIdfromHBLHAWBAndContainerNumber( $hblHawb, $containerNumber ) {
		$id = 0;
		$this->load->model('Pcshipment_model', 'pcshipment_model');
		$records = $this->pcshipment_model->getRecordsWhere( array( 'hbl_hawb' => $hblHawb, 'container_number' => $containerNumber, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $records ) > 0 ) {
			$id = $records[0]['id'];
		}
		return $id;
	}

	/**
	 * getRecordsForShipment
	 * returns all related hawb records for the given shipment id
	 *
	 * @param int $shipmentId - id of shipmentrecord we want related events for
	 * @return array query result as array
	 */
	public function getRecordsForShipment( $shipmentId ){
		$records = $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
		return $records;
	}

	/**
	 * getRecordByPcshipmentId
	 * returns all related hawb records for the given shipment id
	 *
	 * @param int $shipmentId - id of shipmentrecord we want related events for
	 * @return array query result as array
	 */
	public function getRecordsByPcshipmentId( $pcshipmentId ){
		$records = $this->getRecordsWhere( array( 'pcshipment_id' => $pcshipmentId, 'hidden' => 0, 'deleted' => 0 ) );
		return $records;
	}

	/**
	 * getRecordsByPON
	 * returns all related records for the given po number
	 *
	 * @param string $pon
	 * @return array query result as array
	 */
	public function getRecordsByPON( $pon ){
		$out = array();
		$this->db->where( array('hbl_hawb' => $pon, 'hidden' => 0, 'deleted' => 0) );
		$query = $this->db->get( $this->tablename );
		foreach( $query->result_array() as $row ) {
			$out[] = $row;
		}
		return $out;
	}

	/**
	 * getDeliveriesByShipmentId
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getDeliveriesByShipmentId( $pcshipmentId ) {
		$this->load->model('Delivery_model', 'delivery_model');
		$out = array();
		if( $pcshipmentId > 0 ) {
			$results = $this->getRecordsWhere( array( 'pcshipment_id' => $pcshipmentId, 'hidden' => 0, 'deleted' => 0 ) );
			foreach( $results as $row ) {
				$tmp = $this->delivery_model->getRecord( $row['delivery_id'] );
				$out[] = $tmp;
			}
		}
		return $out;
	}
}

/* End of file pcshipment_delivery_model.php */
/* Location: ./app/models/pcshipment_delivery_model.php */
