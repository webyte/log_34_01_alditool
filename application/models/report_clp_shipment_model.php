<?php
require_once( APPPATH.'models/abstract_report_clp_model.php' );

class Report_clp_shipment_model extends Abstract_report_clp_model {

	public $cacheName = 'report_clp_shipment_getrecords';

	function __construct() {
		parent::__construct();
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
	}

	/**
	 * getRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @return array - db record as array.
	 */
	public function getRecords( $recordListSearchParams ) {

		if( !$res = $this->cache->get($this->cacheName) ) {
			$res = array();
			$this->db->select(
				$this->getAllFieldsFromTable( 'deliveries' ).', '.
				$this->getAllFieldsFromTable( 'contracts' ).', '.
				$this->getAllFieldsFromTable( 'shipments' ).', '.
				$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
				$this->getAllFieldsFromTable( 'pcshipments' ).', '.
				'(SELECT SUM(shipments_deliveries_mm.gross_weight) FROM pcshipments_deliveries_mm, shipments_deliveries_mm WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS weight_total,'.
				'(SELECT SUM(shipments_deliveries_mm.volume) FROM pcshipments_deliveries_mm, shipments_deliveries_mm WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS volume_total,'.
				'(SELECT SUM(shipments_deliveries_mm.case_quantity) FROM pcshipments_deliveries_mm, shipments_deliveries_mm WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS case_quantity_total,'.
				'(SELECT GROUP_CONCAT(deliveries.contract_number) FROM pcshipments_deliveries_mm, deliveries WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND deliveries.hidden = 0 AND deliveries.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND deliveries.id = pcshipments_deliveries_mm.delivery_id) AS contract_number_total,'.
				'(SELECT GROUP_CONCAT(deliveries.product_code) FROM pcshipments_deliveries_mm, deliveries WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND deliveries.hidden = 0 AND deliveries.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND deliveries.id = pcshipments_deliveries_mm.delivery_id) AS product_code_total,'.
				'(SELECT GROUP_CONCAT(deliveries.product_description) FROM pcshipments_deliveries_mm, deliveries WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND deliveries.hidden = 0 AND deliveries.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND deliveries.id = pcshipments_deliveries_mm.delivery_id) AS product_description_total'
			);
			$this->db->from( 'deliveries' );
			$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
			$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
			$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
			$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.delivery_id = deliveries.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0');
			$this->db->join( 'pcshipments', 'pcshipments.id = pcshipments_deliveries_mm.pcshipment_id AND pcshipments.hidden = 0 AND pcshipments.deleted = 0');
			$this->setWhereClauseForSearchparams( $recordListSearchParams );

			$this->db->where( "(shipments.ata = '0000-00-00 00:00:00' OR shipments.ata >= '".date('Y-m-d H:i:s', strtotime('-12 months'))."')" );
			$this->db->where( array( 'deliveries.hidden' => 0, 'deliveries.deleted' => 0 ) );
			$this->db->group_by( 'shipments_deliveries_mm.id' );
			$query = $this->db->get();

			foreach ( $query->result_array() as $row ) {

				$row['aldi_country'] = 'AUSTRALIA';
				//$row['contracts_contract_number']   = $row['contract_number_total'];
				//$row['deliveries_product_code'] 	= $row['product_code_total'];
				$row['osd'] 						= ( $row['contracts_contract_type'] == '1' ) ? $row['contracts_aldi_au_wh_delivery_due_end_date'] : $row['contracts_on_sale_date'];
				$row['equipment_type'] 				= ( $row['shipments_container_type'] == '7' ) ? 'Reefer' : 'Dry Cargo';
				$row['dangerous_goods'] 			= 'N';
				$row['no_of_package']				= $row['case_quantity_total'];
				$row['weight'] 						= $row['weight_total'];
				$row['volume'] 						= sprintf($this->config->item('floatvalues_format'), $row['volume_total']);
				$row['cmff']						= 'Logwin';
				$row['shipping_mode']				= ( $row['contracts_contract_type'] == '1' ) ? 'CR' : 'SB';
				$departures 						= $this->getDepartures( $row['shipments_departure_port'] );
				$row['pol'] 						= $departures['port_name'];
				$row['pol_country'] 				= $departures['port_country'];
				$destinations 						= $this->getDepartures( $this->getDestinationPort( $row['shipments_destination_port'] ) );
				$row['pod'] 						= $destinations['port_name'];
				$row['pod_country'] 				= $destinations['port_country'];
				$row['ship_mode_ocean']				= 'Ocean';
				$transShipments 					= $this->getDepartures( $row['pcshipments_transshipment_port'] );
				$row['transshipment_port']			= $transShipments['port_name'];

				if( $row['shipments_ata'] != '0000-00-00 00:00:00' ) {
					$row['latest_shipment_status2'] = 'Arrived at discharge port';
				} else if ( $row['shipments_ata'] == '0000-00-00 00:00:00' && $row['pcshipments_last_leg_atd'] != '0000-00-00 00:00:00' ) {
					$row['latest_shipment_status2'] = 'Transit port departed';
				} else if ( $row['shipments_ata'] == '0000-00-00 00:00:00' && $row['pcshipments_last_leg_atd'] == '0000-00-00 00:00:00'  && $row['pcshipments_first_leg_ata'] != '0000-00-00 00:00:00' ) {
					$row['latest_shipment_status2'] = 'Transit port arrived';
				} else if ( $row['shipments_ata'] == '0000-00-00 00:00:00' && $row['pcshipments_first_leg_ata'] == '0000-00-00 00:00:00' && $row['shipments_atd'] != '0000-00-00 00:00:00' ) {
					$row['latest_shipment_status2'] = 'Shipped';
				} else if ( $row['shipments_ata'] == '0000-00-00 00:00:00' && $row['shipments_atd'] == '0000-00-00 00:00:00' && $row['shipments_container_gate_in'] != '0000-00-00 00:00:00' ) {
					$row['latest_shipment_status2'] = 'POL gate in';
				} else if ( $row['shipments_atd'] == '0000-00-00 00:00:00' && $row['shipments_container_gate_in'] == '0000-00-00 00:00:00' && $row['shipments_container_loaded'] != '0000-00-00 00:00:00' ) {
					$row['latest_shipment_status2'] = 'Empty container pick up';
				} else if ( $row['shipments_container_gate_in'] == '0000-00-00 00:00:00' && $row['shipments_container_loaded'] == '0000-00-00 00:00:00' && $row['shipments_so_released_date'] != '0000-00-00 00:00:00' ) {
					$row['latest_shipment_status2'] = 'Carrier SO released';
				} else if ( $row['shipments_so_released_date'] == '0000-00-00 00:00:00' && $row['shipments_carrier_booking_number'] != '' ) {
					$row['latest_shipment_status2'] = 'PO booked, Carrier SO not released';
				} else if ( $row['shipments_carrier_booking_number'] == '' && $row['contracts_ofu_real'] != '0000-00-00 00:00:00' ) {
					$row['latest_shipment_status2'] = 'PO booked, Carrier not booked';
				} else if ( $row['contracts_ofu_real'] == '0000-00-00 00:00:00' ) {
					$row['latest_shipment_status2'] = 'PO unbooked';
				} else {
					$row['latest_shipment_status2'] = '';
				}

				$contractType = $row['contracts_contract_type'];
				if ( $contractType == 1 ) {
					$row['shipping_mode'] = 'CR';
				} else if ( $contractType == 3 ) {
					$row['shipping_mode'] = 'SB';
				} else {
					$row['shipping_mode'] = $this->getContractType( $contractType );
				}

				$res[] = $row;
			}
			$res = $this->prepareResultQueryForOutput( $res, 'd-M-Y' );
		}
		return $res;
	}

	/**
	 * getCellDefintions
	 * returns cell defintion
	 *
	 * @return array
	 */
	public function getCellDefintions() {
		$cells = array(
			array('field' => 'aldi_country', 'label' => 'ALDI Country'),
			array('field' => 'deliveries_product_code', 'label' => 'Product Code'),
			array('field' => 'contracts_contract_number', 'label' => 'ALDI Contract#'),
			array('field' => '', 'label' => 'Priority'),
			array('field' => 'contracts_initial_crd', 'label' => 'Original CRD'),
			array('field' => 'contracts_fob_date', 'label' => 'CRD'),
			array('field' => 'contracts_initial_sales_date', 'label' => 'Original OSD'),
			array('field' => 'osd', 'label' => 'OSD'),
			array('field' => 'booking_deadline', 'label' => 'Booking Deadline'),
			array('field' => 'contracts_ofu_real', 'label' => 'Booked Date'),
			array('field' => 'contracts_actual_contract_ready_date', 'label' => 'Factory CRD'),
			array('field' => 'pol', 'label' => 'Port of Loading (Shipped)'),
			array('field' => 'pod', 'label' => 'Port Of Discharge (Shipped)'),
			array('field' => 'pcshipments_hbl_hawb', 'label' => 'CMFF reference no.'),
			array('field' => 'nominated_off', 'label' => 'OFF'),
			array('field' => 'shipments_contract_number', 'label' => 'OFC Contract no'),
			array('field' => 'shipments_carrier_booking_number', 'label' => 'Carrier Booking no#'),
			array('field' => 'pcshipments_mbl_mawb', 'label' => 'MBL#'),
			array('field' => 'pcshipments_hbl_hawb', 'label' => 'HBL#'),
			array('field' => 'shipments_container_number', 'label' => 'Container Number'),
			array('field' => 'container_type', 'label' => 'Container Type'),
			array('field' => 'movement', 'label' => 'Movement (CYCY, CFSCY)'),
			array('field' => 'equipment_type', 'label' => 'Equipment Type'),
			array('field' => 'dangerous_goods', 'label' => 'Dangerous Goods'),
			array('field' => '', 'label' => 'mix-load shipment'),
			array('field' => 'shipments_deliveries_mm_case_quantity', 'label' => 'Packages (shipped)'),
			array('field' => 'volume', 'label' => 'Shipped Volume (CBM)'),
			array('field' => 'shipments_deliveries_mm_gross_weight', 'label' => 'Shipped Gross weight(kg)'),
			array('field' => 'shipments_mother_vessel_name', 'label' => '1st Leg Vessel Name'),
			array('field' => 'shipments_voyage_number', 'label' => '1st Leg Vessel Voyage'),
			array('field' => 'shipments_so_released_date', 'label' => 'SO released Date'),
			array('field' => 'shipments_cy_cut_off', 'label' => 'CY Closing '),
			array('field' => 'shipments_container_gate_in', 'label' => 'Gate In'),
			array('field' => 'pcshipments_ietd', 'label' => 'Initial 1st Leg ETD'),
			array('field' => 'pcshipments_ieta', 'label' => 'Initial last Leg ETA'),
			array('field' => 'shipments_etd', 'label' => '1st Leg ETD (dynamic)'),
			array('field' => 'first_leg_eta', 'label' => '1st Leg ETA (dynamic)'),
			array('field' => 'shipments_atd', 'label' => '1st Leg ATD'),
			array('field' => 'first_leg_ata', 'label' => '1st Leg ATA'),
			array('field' => 'transshipment_port', 'label' => 'Transhipment Port'),
			array('field' => 'shipments_vessel_2', 'label' => '2nd Leg Vessel Name'),
			array('field' => 'shipments_voyage_2', 'label' => '2nd Leg Vessel Voyage'),
			array('field' => 'last_leg_etd', 'label' => '2nd Leg ETD (dynamic)'),
			array('field' => 'shipments_eta', 'label' => '2nd Leg ETA (dynamic)'),
			array('field' => 'last_leg_atd', 'label' => '2nd Leg ATD'),
			array('field' => 'shipments_ata', 'label' => '2nd Leg ATA'),
			array('field' => '', 'label' => 'Shipment Confirmed at Destination'),
			array('field' => '', 'label' => 'Cargo released by CMFF EDO release date'),
			array('field' => '', 'label' => 'Customs Declaration Sent'),
			array('field' => '', 'label' => 'ATB received date'),
			array('field' => 'shipments_cus', 'label' => 'Customs Cleared'),
			array('field' => '', 'label' => 'Date of Container Discharges from vessel'),
			array('field' => '', 'label' => 'Destination Gate out date'),
			array('field' => '', 'label' => 'Final Delivery Date'),
			array('field' => '', 'label' => 'Empty container return'),
			array('field' => '', 'label' => 'Drop off location applicable for EU'),
			array('field' => 'shipments_dc', 'label' => 'Final Destination Region / DC'),
			array('field' => 'deliveries_product_code', 'label' => 'Purchase Order'),
			array('field' => 'deliveries_product_description', 'label' => 'Product Description'),
			array('field' => 'contracts_supplier', 'label' => 'Supplier Name'),
			array('field' => 'cmff', 'label' => 'CMFF'),
			array('field' => 'domestic_freight_forwarder', 'label' => 'Domestic Freight forwarder'),
			array('field' => 'ship_mode_ocean', 'label' => 'Ship mode'),
			array('field' => 'pol', 'label' => 'Port of Loading (Planned)'),
			array('field' => 'pod', 'label' => 'Port Of Discharge (Planned)'),
			array('field' => 'shipments_dc', 'label' => 'Region'),
			array('field' => 'shipments_deliveries_mm_case_quantity', 'label' => 'PO Packages'),
			array('field' => 'volume', 'label' => 'PO Volume (CBM)'),
			array('field' => '', 'label' => 'Delay Responsible Parties'),
			array('field' => 'delay_reason_group', 'label' => 'Delay Reason Group'),
			array('field' => 'delay_reason', 'label' => 'Delay Reason details'),
			array('field' => 'latest_shipment_status2', 'label' => 'Latest shipment status'),
			array('field' => 'shipping_mode', 'label' => 'shipping Mode Core Range / Special Buys'),
			array('field' => 'contracts_received_in_cfs', 'label' => 'Cargo Delivery to Origin Warehouse (CFS/ CY or CFS/ CFS only)'),
		);
		return $cells;
	}

	////////////////////////////////////////////////////////////////////////////

    //		PRIVATE

    ////////////////////////////////////////////////////////////////////////////

	/**
	 * getDepartures
	 * returns departure port
	 *
	 * @param string $code - port code
	 * @return array
	 */
	private function getDepartures( $code='' )
	{
		$out = array(
			'port_name' => '',
			'port_country' => '',
		);

		if( count( $this->departures ) <= 0 ) {
			$this->load->model('Departure_model', 'departure_model');
			$this->departures = $this->departure_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		}

		foreach( $this->departures as $row ) {
			if( $row['name'] == $code ) {
				$out['port_name'] = $row['port_name'];
				$out['port_country'] = $row['port_country'];
				return $out;
			}
		}

		return $out;
	}

	/**
	 * getDestinationPort
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getDestinationPort( $id=0 ) {
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$destinationPortOptions 		= $this->tca->getMultiselectionOptions( $tcaNode );
		return $destinationPortOptions[$id];
	}
}

/* End of file orders.php */
/* Location: ./app/models/orders.php */
