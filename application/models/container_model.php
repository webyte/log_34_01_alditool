<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Container_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('dc_model', 'dc_model');
		$this->tablename = 'shipments';
	}

	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select('shipments.id AS id,
							shipments.container_number AS container_number,
							shipments.destination_port AS destination_port,
							shipments.mother_vessel_name AS mother_vessel_name,
							shipments.vessel_2 AS vessel_2,
							shipments.carrier AS carrier,
							shipments.voyage_number AS voyage_number,
							shipments.voyage_2 AS voyage_2,
							shipments.feeder_vessel_name AS feeder_vessel_name,
							shipments.eta AS eta,
							contracts.advertisement_week AS advertisement_week,
							contracts.aldi_au_wh_delivery_due_date AS aldi_au_wh_delivery_due_date,
							shipments.cus AS cus,
							shipments.aqc AS aqc,
							shipments.contract_numbers AS contract_numbers,
							shipments.temperature AS temperature,
							shipments.dg AS dg,
							shipments.dg_class AS dg_class,
							shipments.edo_sent AS edo_sent,
							shipments.last_transmission AS last_transmission,
							shipments.num_of_transmission AS num_of_transmission,
							shipments.file_transmission AS file_transmission');
		$this->db->from( $this->tablename );
		$this->db->join('shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join('deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0', 'left');
		$this->db->join('contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0', 'left');
		$loo = $this->session->userdata('logwin_origin_office');
		if( !empty( $loo ) ) $this->db->where( array( 'contracts.logwin_origin_office' => $loo ) );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$deliveries = $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $row['id'], 'hidden' => 0, 'deleted' => 0 ) );
			$row['deliveries'] = $this->prepareResultQueryForDeliveryOutput( $deliveries );

			if( empty( $row['vessel_2'] ) ) $row['vessel_2'] = $row['mother_vessel_name'];
			if( empty( $row['voyage_2'] ) ) $row['voyage_2'] = $row['voyage_number'];

			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * getFreightTrackerRecord
	 */
	public function getFreightTrackerRecord( $id ) {
		$res = array();
		$this->db->select(
			$this->getAllFieldsFromTable( 'shipments' ).', '.
			$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
			$this->getAllFieldsFromTable( 'deliveries' ).', '.
			$this->getAllFieldsFromTable( 'contracts' ).', '.
			$this->getAllFieldsFromTable( 'pcshipments' )
		);
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0', 'left');
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0', 'left');
		$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.delivery_id = deliveries.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join( 'pcshipments', 'pcshipments.id = pcshipments_deliveries_mm.pcshipment_id AND pcshipments.hidden = 0 AND pcshipments.deleted = 0', 'left');
		$this->db->where( array( 'shipments.id' => $id, 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
		$this->db->group_by( 'shipments.id' );
		$query = $this->db->get();

		$row = $query->row_array();
		$row['shipments_gross_weight'] = $row['shipments_container_tare_weight'];
		$row['shipments_carrier'] = $this->getCarrierCode($row['shipments_carrier']);

		if( empty( $row['shipments_vessel_2'] ) ) $row['shipments_vessel_2'] = $row['shipments_mother_vessel_name'];
		if( empty( $row['shipments_voyage_2'] ) ) $row['shipments_voyage_2'] = $row['shipments_voyage_number'];

		if( $row['shipments_dg'] == 1 ) {
			$row['shipments_cargo_type'] = empty( $row['shipments_temperature'] ) ? 'HAZ' : 'REF HAZ';
		} else {
			$row['shipments_cargo_type'] = empty( $row['shipments_temperature'] ) ? 'GEN' : 'REF';
		}

		$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId( $row['shipments_id'] );
		foreach( $deliveries as $delivery ) {
			$row['shipments_gross_weight'] += $delivery['gross_weight_mm'];
			$delivery['supplier'] = $this->getSupplierName( $row['contracts_supplier'] );
			$delivery['case_quantity'] = $delivery['case_quantity_mm'];
			$delivery['truckercode'] = $this->getTruckerCode( $delivery['dc'] );

			switch( $row['contracts_contract_type'] ) {
				case 3:
					$delivery['contract_type'] = 'SPECIAL';
					break;
				case 1:
					$delivery['contract_type'] = 'CORE';
					break;
				case 8:
					$delivery['contract_type'] = 'AUI';
					break;
			}

			$res[] = $delivery;
		}
		$row['deliveries'] = $res;
		$row['consignee'] = $this->getDCData( $row['shipments_dc'] );

		return $this->prepareResultQueryForFreightTrackerOutput( $row );
	}

	/**
	 * getContainerNumber
	 * returns array with associated hbl/hawb
	 * in the frontend dynamically.
	 *
	 * @param str $hblHawb - hbl/hawb
	 * @return array - db record as array.
	 */
	public function getContainerNumbersByHblHawb( $hblHawb ) {
		$result = $this->getRecordsWhere( array( 'hbl_hawb' => $hblHawb, 'hidden' => 0, 'deleted' => 0 ) );
		$tmp = array();
		foreach( $result as $row ) {
			$tmp[] = $row['container_number'];
		}

		return $tmp;
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function prepareResultQueryForOverviewOutput( $query_result_array ){
		$res 		= array();
		$desOptions = $this->getTCAgetMultiselectionOptions( 'shipments_destination_port' );
		foreach ( $query_result_array as $row) {
			$row['eta'] 							= ( $row['eta'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['eta'] ) ) : "";
			$row['aldi_au_wh_delivery_due_date'] 	= ( $row['aldi_au_wh_delivery_due_date'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['aldi_au_wh_delivery_due_date'] ) ) : "";
			$row['destination_port'] 				= ( $row['destination_port'] != 0 && array_key_exists($row['destination_port'], $desOptions) ) ? $desOptions[$row['destination_port']] : '';
			$row['last_transmission'] 				= ( $row['last_transmission'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['last_transmission'] ) ) : "";
			$row['dg'] 								= ( $row['dg'] != 0 ) ? "Yes" : "No";
			$row['cus'] 							= ( $row['cus'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['cus'] ) ) : "";
			$row['aqc'] 							= ( $row['aqc'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['aqc'] ) ) : "";
			$res[] = $row;
		}
		return $res;
	}

	private function prepareResultQueryForDeliveryOutput( $query_result_array ){
		$res 		= array();
		foreach ( $query_result_array as $row) {
			$row['telex_received'] 			= ( $row['telex_received'] != 0 ) ? "Yes" : "No";
			$res[] = $row;
		}
		return $res;
	}

	private function prepareResultQueryForFreightTrackerOutput( $row ){
		$desOptions = $this->getTCAgetMultiselectionOptions( 'shipments_destination_port' );
		$consOptions = $this->getTCAgetMultiselectionOptions( 'shipments_container_size' );
		$contOptions = $this->getTCAgetMultiselectionOptions( 'shipments_container_type' );
		$conrOptions = $this->getTCAgetMultiselectionOptions( 'contracts_contract_type' );

		$row['contract_type_aufre'] = '';
		switch( $row['contracts_contract_type'] ) {
			case 3:
			case 5:
				$row['contract_type_aufre'] = 'ALDI-SPECIAL';
				break;
			case 1:
				$row['contract_type_aufre'] = 'ALDI-CORE';
				break;
			case 7:
				$row['contract_type_aufre'] = 'ALDI-SEASONAL';
				break;
			case 8:
				$row['contract_type_aufre'] = 'ALDI-AUI';
				break;
		}


		$row['shipments_destination_port'] 	= ( $row['shipments_destination_port'] != 0 && array_key_exists($row['shipments_destination_port'], $desOptions) ) ? $desOptions[$row['shipments_destination_port']] : '';
		$row['shipments_dg'] 				= ( $row['shipments_dg'] != 0 ) ? "Y" : "N";
		$row['shipments_edo_sent'] 			= ( $row['shipments_edo_sent'] != 0 ) ? "Y" : "N";
		$row['shipments_customs_status'] 	= ( $row['shipments_cus'] != "0000-00-00 00:00:00" ) ? "Y" : "N";
		$row['shipments_aquis_status'] 		= ( $row['shipments_aqc'] != "0000-00-00 00:00:00" ) ? "Y" : "N";
		$row['contracts_aldi_au_wh_delivery_due_date'] 	= ( $row['contracts_aldi_au_wh_delivery_due_date'] != "0000-00-00 00:00:00" ) ? date( 'Y-m-d', strtotime( $row['contracts_aldi_au_wh_delivery_due_date'] ) ).'T'.date( 'H:i:s', strtotime( $row['contracts_aldi_au_wh_delivery_due_date'] ) ) : "";
		$row['shipments_container_size'] 	= ( $row['shipments_container_size'] != 0 && array_key_exists($row['shipments_container_size'], $consOptions) ) ? $consOptions[$row['shipments_container_size']] : '';
		$row['shipments_container_type'] 	= ( $row['shipments_container_type'] != 0 && array_key_exists($row['shipments_container_type'], $contOptions) ) ? $contOptions[$row['shipments_container_type']] : '';
		$row['contracts_on_sale_date'] 		= ( $row['contracts_on_sale_date'] != "0000-00-00 00:00:00" ) ? date( 'Ymd', strtotime( $row['contracts_on_sale_date'] ) ) : date( 'Ymd' );
		$row['contracts_contract_type'] 	= ( $row['contracts_contract_type'] != 0 && array_key_exists($row['contracts_contract_type'], $conrOptions) ) ? $conrOptions[$row['contracts_contract_type']] : '';
		$row['shipments_eta'] 				= ( $row['shipments_eta'] != "0000-00-00 00:00:00" ) ? date( 'Y-m-d', strtotime( $row['shipments_eta'] ) ).'T'.date( 'H:i:s', strtotime( $row['shipments_eta'] ) ) : "";
		return $row;
	}

	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	/**
	 * getSupplierName
	 * returns supplier name
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getSupplierName( $id=0 ) {
		$this->load->model('Supplier_model', 'supplier_model');
		$supplier = $this->supplier_model->getRecord( $id );
		return ( count( $supplier ) > 0 ) ? $supplier['name'] : '';
	}

	/**
	 * getDCData
	 * returns supplier name
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getDCData( $id='' ) {
		$dc = $this->dc_model->getRecordsWhere( array( 'id' => $id, 'hidden' => 0, 'deleted' => 0 ) );
		return ( count( $dc ) > 0 ) ? $dc[0] : array();
	}

	/**
	 * getTruckerCode
	 * returns supplier name
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getTruckerCode( $dc='' ) {
		$dc = $this->dc_model->getRecordsWhere( array( 'dc' => $dc, 'hidden' => 0, 'deleted' => 0 ) );
		return ( count( $dc ) > 0 ) ? $dc[0]['truckercode'] : '';
	}

	/**
	 * getCarrierCode
	 * returns carrier code
	 *
	 * @param string $carrier - name of carrier
	 * @return string - carrier code
	 */
	private function getCarrierCode( $carrier='' ) {
		$carrier = strtoupper( $carrier );
		$out = '';
		$map = $this->config->item('carrier_map');
		if( array_key_exists( $carrier, $map ) ) $out = $map[$carrier];
		return $out;
	}

}

/* End of file shipments.php */
/* Location: ./app/models/shipments.php */
