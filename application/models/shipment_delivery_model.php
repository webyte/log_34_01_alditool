<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Shipment_delivery_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'shipments_deliveries_mm';
	}

	/**
	 * updateRecordMM
	 * updates the record with the given id
	 *
	 * @param int $id - row id
	 * @param int $id - row id
	 * @param array $insertData - updated data
	 */
	public function updateRecordMM( $shipmentId, $deliveryId, $insertData ) {
		$this->welogger->log( "updateRecordMM called with shipmentId: ".$shipmentId." and deliveryId: ".$deliveryId." and date: ".serialize( $insertData ), WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.updateRecordMM" );
		$records = $this->shipment_delivery_model->getRecordsWhere( array( 'delivery_id' => $deliveryId, 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $records ) > 0 ) {
			$id = $records[0]['id'];
			$this->updateRecord( $id, $insertData );
		}
	}

	/**
	 * getDeliveriesByShipmentId
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getDeliveriesByShipmentId( $shipmentId ) {
		$this->load->model('Delivery_model', 'delivery_model');
		$out = array();
		if( $shipmentId > 0 ) {
			$results = $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
			foreach( $results as $row ) {
				$tmp = $this->delivery_model->getRecord( $row['delivery_id'] );
				$tmp['unit_quantity_mm'] 		= $row['unit_quantity'];
				$tmp['case_quantity_mm'] 		= $row['case_quantity'];
				$tmp['gross_weight_mm'] 		= $row['gross_weight'];
				$tmp['volume_mm'] 				= $row['volume'];
				$tmp['telex_received_mm'] 		= $row['telex_received'];
				$tmp['hbl_hawb_mm'] 			= $this->getHBLHAWBFromOtherShipmentDeliveryRelationsIfEmpty( $row['hbl_hawb'], $row['delivery_id'] );
				$tmp['hbl_hawb_container_number_mm'] = $this->getHBLHAWBContainerNumber( $shipmentId, $row['delivery_id'], $tmp['hbl_hawb_mm'] );
				$tmp['edl'] 					= $row['edl'];
				$tmp['adl'] 					= $row['adl'];
				$tmp['unit_quantity_remain'] 	= $this->getRemainingUnitQuantityByDeliveryId( $row['delivery_id'] );
				$out[] = $tmp;
			}
		}
		return $out;
	}

	/**
	 * getContainersByOrderId
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getHBLHAWBContainerNumber( $shipmentId, $deliveryId, $oldHblHawbs ) {
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');
		$results = $this->pcshipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId, 'hidden' => 0, 'deleted' => 0 ) );

		$oldHblHawbs = explode(',',$oldHblHawbs);
		$newHblHawbs = array();
		foreach( $results as $result ) {
			$newHblHawbs[] = $result['hbl_hawb'];
		}
		$diff = array_diff( $oldHblHawbs, $newHblHawbs );


		return $results;
	}

	/**
	 * getContainersByOrderId
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getShipmentsByDeliveryId( $deliveryId ) {
		$this->load->model('Shipment_model', 'shipment_model');
		$out = array();
		if( $deliveryId > 0 ) {
			$results = $this->getRecordsWhere( array( 'delivery_id' => $deliveryId, 'hidden' => 0, 'deleted' => 0 ) );
			foreach( $results as $row ) {
				$tmp = $this->shipment_model->getRecord( $row['shipment_id'] );
				$tmp['unit_quantity_mm'] 	= $row['unit_quantity'];
				$tmp['case_quantity_mm'] 	= $row['case_quantity'];
				$tmp['gross_weight_mm'] 	= $row['gross_weight'];
				$tmp['volume_mm'] 			= $row['volume'];
				$tmp['telex_received_mm'] 	= $row['telex_received'];
				$tmp['hbl_hawb_mm'] 		= $row['hbl_hawb'];
				$tmp['edl'] 				= $row['edl'];
				$tmp['adl'] 				= $row['adl'];
				$tmp['unit_quantity_remain'] = $this->getRemainingUnitQuantityByDeliveryId( $row['delivery_id'] );
				$out[] = $tmp;
			}
		}
		return $out;
	}

	/**
	 * getHBLHAWBFromOtherShipmentDeliveryRelationsIfEmpty
	 * returns correspondending hbl/hawb
	 *
	 * @param string $hblHawb
	 * @return string - hbl/hawb
	 */
	public function getHBLHAWBFromOtherShipmentDeliveryRelationsIfEmpty( $hblHawb, $deliveryId ) {
		if( empty( $hblHawb ) ) {
			$results = $this->getRecordsWhere( array( 'delivery_id' => $deliveryId, 'hidden' => 0, 'deleted' => 0 ) );
			foreach( $results as $row ) {
				if( !empty( $row['hbl_hawb'] ) ) $hblHawb = $row['hbl_hawb'];
			}
		}
		return $hblHawb;
	}

	/**
	 * getActualUnitQuantityByDeliveryId
	 * returns the unit quantity
	 *
	 * @param int $id - id of record to retrieve
	 * @return int - unit quantity
	 */
	public function getActualUnitQuantityByDeliveryId( $deliveryId ) {
		$out = 0;
		if( $deliveryId > 0 ) {
			$results = $this->getRecordsWhere( array( 'delivery_id' => $deliveryId, 'hidden' => 0, 'deleted' => 0 ) );
			foreach( $results as $row ) {
				$out += $row['unit_quantity'];
			}
		}
		return $out;
	}

	/**
	 * getActualCaseQuantityByDeliveryId
	 * returns the case quantity
	 *
	 * @param int $id - id of record to retrieve
	 * @return int - db record as array.
	 */
	public function getActualCaseQuantityByDeliveryId( $deliveryId ) {
		$out = 0;
		if( $deliveryId > 0 ) {
			$results = $this->getRecordsWhere( array( 'delivery_id' => $deliveryId, 'hidden' => 0, 'deleted' => 0 ) );
			foreach( $results as $row ) {
				$out += $row['case_quantity'];
			}
		}
		return $out;
	}

	/**
	 * getTotalUnitQuantityByDeliveryId
	 * returns the unit quantity
	 *
	 * @param int $id - id of record to retrieve
	 * @return int - TotalUnitQuantity
	 */
	public function getTotalUnitQuantityByDeliveryId( $deliveryId ) {
		$out = 0;
		if( $deliveryId > 0 ) {
			$this->load->model('Delivery_model', 'delivery_model');
			$row = $this->delivery_model->getRecord( $deliveryId );
			if( count( $row ) > 0 ) $out = $row['unit_quantity'];
		}
		return $out;
	}

	/**
	 * getTotalCaseQuantityByDeliveryId
	 * returns the case quantity
	 *
	 * @param int $id - id of record to retrieve
	 * @return int - db record as array.
	 */
	public function getTotalCaseQuantityByDeliveryId( $deliveryId ) {
		$out = 0;
		if( $deliveryId > 0 ) {
			$this->load->model('Delivery_model', 'delivery_model');
			$row = $this->delivery_model->getRecord( $deliveryId );
			if( count( $row ) > 0 ) $out = $row['case_quantity'];
		}
		return $out;
	}

	/**
	 * getRemainingUnitQuantityByDeliveryId
	 * returns the unit quantity
	 *
	 * @param int $id - id of record to retrieve
	 * @return int - RemainingUnitQuantity
	 */
	public function getRemainingUnitQuantityByDeliveryId( $deliveryId ) {
		$out = 0;
		if( $deliveryId > 0 ) {
			$totalQuantity	= $this->getTotalUnitQuantityByDeliveryId( $deliveryId );
			$actualQuantiy	= $this->getActualUnitQuantityByDeliveryId( $deliveryId );
			$remainQuantity	= $totalQuantity - $actualQuantiy;
			$out 			= $remainQuantity;
		}
		//$this->welogger->log( "getRemainingUnitQuantityByDeliveryId called with totalQuantity: ".$totalQuantity." and actualQuantiy: ".$actualQuantiy." and remainQuantity: ".$remainQuantity, WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.deleteRecordsByShipmentId" );
		return $out;
	}

	/**
	 * getRemainingCaseQuantityByDeliveryId
	 * returns the unit quantity
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getRemainingCaseQuantityByDeliveryId( $deliveryId ) {
		$out = 0;
		if( $deliveryId > 0 ) {
			$totalQuantity	= $this->getTotalCaseQuantityByDeliveryId( $deliveryId );
			$actualQuantiy	= $this->getActualCaseQuantityByDeliveryId( $deliveryId );
			$remainQuantity	= $totalQuantity - $actualQuantiy;
			$out 			= $remainQuantity;
		}
		//$this->welogger->log( "getRemainingUnitQuantityByDeliveryId called with totalQuantity: ".$totalQuantity." and actualQuantiy: ".$actualQuantiy." and remainQuantity: ".$remainQuantity, WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.deleteRecordsByShipmentId" );
		return $out;
	}

	/**
	 * deleteRecordsByShipmentId
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function deleteRecordsByShipmentId( $shipmentId ) {
		$this->welogger->log( "deleteRecordsByShipmentId called with shipmentId: ".$shipmentId, WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.deleteRecordsByShipmentId" );
		$this->load->model('Delivery_model', 'delivery_model');
		if( $shipmentId > 0 ) {
			$results = $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
			foreach( $results as $row ) {
				$this->updateRecord( $row['id'], array( 'deleted' => 1 ) );
				$this->welogger->log( "deleteRecordsByShipmentId: record deleted with id: ".$row['id'], WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.deleteRecordsByShipmentId" );

				$remainingUnitQuantity = $this->getRemainingUnitQuantityByDeliveryId( $row['delivery_id'] );
				$this->delivery_model->updateRecord( $row['delivery_id'], array( 'unit_quantity_remain' => $remainingUnitQuantity ) );
				$this->welogger->log( "deleteRecordsByShipmentId: delivery updated with id: ".$row['id']." and remainingUnitQuantity: ".$remainingUnitQuantity, WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.deleteRecordsByShipmentId" );
			}
		}
	}

	/**
	 * deleteRecord
	 * deletes record with given id from table
	 *
	 * @param int $containerId - row id
	 * @param int $articleId - row id
	 *
	 */
	function deleteRecord( $shipmentId, $deliveryId ) {
		$this->welogger->log( "deleteRecord called with shipmentId: ".$shipmentId." and deliveryId: ".$deliveryId, WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.deleteRecord" );
		$this->load->model('Delivery_model', 'delivery_model');
		$records = $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId ) );
		foreach( $records as $record ) {
			$this->updateRecord($record['id'], array( 'deleted' => 1 ) );
			$this->welogger->log( "deleteRecord: record deleted with id: ".$record['id'], WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.deleteRecord" );

			$remainingUnitQuantity = $this->getRemainingUnitQuantityByDeliveryId( $record['delivery_id'] );
			$remainingCaseQuantity = $this->getRemainingCaseQuantityByDeliveryId( $record['delivery_id'] );
			$this->delivery_model->updateRecord( $record['delivery_id'], array( 'unit_quantity_remain' => $remainingUnitQuantity, 'case_quantity_remain' => $remainingCaseQuantity ) );
			$this->welogger->log( "deleteRecord: delivery updated with id: ".$record['id']." and remainingUnitQuantity: ".$remainingUnitQuantity." and remainingCaseQuantity: ".$remainingCaseQuantity, WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.deleteRecord" );
		}
	}

	/**
	 * updateUnitQuantity
	 *
	 * @param $data
	 * @return
	 */
	public function updateUnitQuantity( $shipmentId=0, $deliveryId=0, $pieces=0 ) {
		$this->welogger->log( "updateUnitQuantity called with shipmentId: ".$shipmentId." and deliveryId: ".$deliveryId." and pieces: ".$pieces, WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.updateUnitQuantity" );
		$records = $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $records ) > 0 ) {
			foreach( $records as $record ) {
				$this->updateRecord( $record['id'], array( 'unit_quantity' => $pieces ) );
			}
		} else {
//			$this->createNew( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId, 'unit_quantity' => $pieces ) );
		}

		$caseQuantity = $this->updateCaseQuantity( $shipmentId, $deliveryId, $pieces );
		$this->updateWeight( $shipmentId, $deliveryId, $caseQuantity );
		$this->updateVolume( $shipmentId, $deliveryId, $caseQuantity );

		$this->load->model('Delivery_model', 'delivery_model');
		$remainUnitQuantity = $this->getRemainingUnitQuantityByDeliveryId( $deliveryId );
		$this->delivery_model->updateRecord( $deliveryId, array( 'unit_quantity_remain' => $remainUnitQuantity ) );

		return $pieces;
	}

	/**
	 * updateUnitQuantity
	 *
	 * @param $data
	 * @return
	 */
	public function updateCaseQuantity( $shipmentId=0, $deliveryId=0, $pieces=0 ) {
		$this->welogger->log( "updateCaseQuantity called with shipmentId: ".$shipmentId." and deliveryId: ".$deliveryId." and pieces: ".$pieces, WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.updateCaseQuantity" );
		$this->load->model('Delivery_model', 'delivery_model');
		$delivery 		= $this->delivery_model->getRecord( $deliveryId );

		$this->load->model('Contract_model', 'contract_model');
		$contract 		= $this->contract_model->getRecord( $delivery['contract_id'] );

		$caseQuantity	= ( $contract['units_per_case'] > 0 ) ? ceil($pieces / $contract['units_per_case'] ) : 0;
		$records 		= $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $records ) > 0 ) {
			foreach( $records as $record ) {
				$this->updateRecord( $record['id'], array( 'case_quantity' => $caseQuantity ) );
			}
		} else {
//			$this->createNew( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId, 'case_quantity' => $caseQuantity ) );
		}

		$this->load->model('Delivery_model', 'delivery_model');
		$remainCaseQuantity = $this->getRemainingCaseQuantityByDeliveryId( $deliveryId );
		$this->delivery_model->updateRecord( $deliveryId, array( 'case_quantity_remain' => $remainCaseQuantity ) );

		return $caseQuantity;
	}

	/**
	 * updateWeight
	 *
	 * @param $data
	 * @return
	 */
	public function updateWeight( $shipmentId=0, $deliveryId=0, $caseQuantity=0 ) {
		$this->welogger->log( "updateWeight called with shipmentId: ".$shipmentId." and deliveryId: ".$deliveryId." and caseQuantity: ".$caseQuantity, WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.updateWeight" );
		$this->load->model('Delivery_model', 'delivery_model');
		$delivery 		= $this->delivery_model->getRecord( $deliveryId );

		$this->load->model('Contract_model', 'contract_model');
		$contract 		= $this->contract_model->getRecord( $delivery['contract_id'] );

		$weight			= $caseQuantity * $contract['case_weight'];
		$records 		= $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $records ) > 0 ) {
			foreach( $records as $record ) {
				$this->updateRecord( $record['id'], array( 'gross_weight' => $weight ) );
			}
		} else {
//			$this->createNew( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId, 'gross_weight' => $weight ) );
		}
		return $caseQuantity;
	}

	/**
	 * updateVolume
	 *
	 * @param $data
	 * @return
	 */
	public function updateVolume( $shipmentId=0, $deliveryId=0, $caseQuantity=0 ) {
		$this->welogger->log( "updateVolume called with shipmentId: ".$shipmentId." and deliveryId: ".$deliveryId." and caseQuantity: ".$caseQuantity, WELogger::$LOG_LEVEL_INFO, "Shipment_delivery_model.updateVolume" );
		$this->load->model('Delivery_model', 'delivery_model');
		$delivery 		= $this->delivery_model->getRecord( $deliveryId );

		$this->load->model('Contract_model', 'contract_model');
		$contract 		= $this->contract_model->getRecord( $delivery['contract_id'] );

		$volume			= $caseQuantity * $contract['case_cubage'];
		$records 		= $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $records ) > 0 ) {
			foreach( $records as $record ) {
				$this->updateRecord( $record['id'], array( 'volume' => $volume ) );
			}
		} else {
//			$this->createNew( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId, 'volume' => $volume ) );
		}
		return $caseQuantity;
	}

	/**
	 * getRecordsForShipment
	 * returns all related hawb records for the given shipment id
	 *
	 * @param int $shipmentId - id of shipmentrecord we want related events for
	 * @return array query result as array
	 */
	public function getRecordsForShipment( $shipmentId ){
		$records = $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
		return $records;
	}
}

/* End of file container_order_mm.php */
/* Location: ./app/models/container_order_mm.php */
