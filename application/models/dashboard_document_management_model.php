<?php
require_once( APPPATH.'models/abstract_model.php' );

class Dashboard_document_management_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->load->model('Supplier_model', 'supplier_model');
		$this->load->driver('cache', array('adapter' => 'file'));
	}

	/**
	 * getRecords
	 * returns the db-records with the given search params
	 *
	 * @return array - db record as array.
	 */
	public function getRecords( $filter=array() ) {
		if( !$res = $this->cache->get('dashboard_document_management_getrecords') ) {
			$res = array();
			$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
			$this->db->from( 'contracts' );
			$this->db->join( 'deliveries', 'deliveries.contract_id = contracts.id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
			$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
			$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );

			$this->db->where( array( 'contracts.hidden' => 0, 'contracts.deleted' => 0 ) );
			$this->db->where( array( 'shipments.eta >=' => date( 'Y-m-d', strtotime( '-5 days', time() ) ).' 00:00:00' ) );
			$this->db->where( array( 'shipments.eta <=' => date( 'Y-m-d', strtotime( '+10 days', time() ) ).' 00:00:00' ) );
			$this->db->group_by( 'deliveries.id' );
			$this->db->order_by( 'contracts.id' );
			$query = $this->db->get();

			foreach ( $query->result_array() as $row ) {
				$res[] = $row;
			}
			$res = $this->prepareResultQueryForOutput( $res );
		}
		return $res;
	}

	/**
	 * generateCache
	 *
	 * @return
	 */
	public function generateCache() {
		$res = $this->getRecords();
		$this->cache->file->save('dashboard_document_management_getrecords', $res, 600);
	}

	/**
	 * isCached
	 *
	 * @return
	 */
	public function isCached() {
		return file_exists( $this->config->item('app_path').'cache/dashboard_ocean_freight_getrecords' );
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function prepareResultQueryForOutput( $query_result_array ){
		$res = array();

		foreach ( $query_result_array as $row) {

			if( isset( $row['contracts_supplier'] ) && $row['contracts_supplier'] > 0 ) {
				$supplier = $this->supplier_model->getRecord( $row['contracts_supplier'] );
				$row['contracts_supplier'] = $supplier['name'];
			}
			if( isset( $row['shipments_destination_port'] ) ) 	$row['shipments_destination_port'] 	= $this->getDestinationPort( $row['shipments_destination_port'] );

			$row['shipments_hbl_hawb'] = str_replace(',', ', ', $row['shipments_hbl_hawb']);

			$dates = array(
				'shipments_eta',
			);

			foreach( $dates as $date ) {
				$row[$date.'_timestamp'] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? strtotime( $row[$date] ) : 0;
				$row[$date] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? date( $this->config->item('date_format'), strtotime( $row[$date] ) ) : "";
			}

			if( $row['shipments_invoice_click_date'] != '0000-00-00 00:00:00' && $row['shipments_ata'] == '0000-00-00 00:00:00' ) {
				$row['commercial_invoice'] = 'Not Ready';
				$row['commercial_invoice_css'] = 'label label-important';
			} else {
				$row['commercial_invoice'] = 'Ready';
				$row['commercial_invoice_css'] = 'label label-success';
			}

			if( $row['shipments_packing_list_click_date'] != '0000-00-00 00:00:00' && $row['shipments_ata'] == '0000-00-00 00:00:00' ) {
				$row['packing_list'] = 'Not Ready';
				$row['packing_list_css'] = 'label label-important';
			} else {
				$row['packing_list'] = 'Ready';
				$row['packing_list_css'] = 'label label-success';
			}

			if( $row['certificate_of_origin_click_date'] != '0000-00-00 00:00:00' && $row['shipments_ata'] == '0000-00-00 00:00:00' ) {
				$row['coo'] = 'Not Ready';
				$row['coo_css'] = 'label label-important';
			} else {
				$row['coo'] = 'Ready';
				$row['coo_css'] = 'label label-success';
			}

			if( $row['packing_declaration_click_date'] != '0000-00-00 00:00:00' && $row['shipments_ata'] == '0000-00-00 00:00:00' ) {
				$row['packing_declaration'] = 'Not Ready';
				$row['packing_declaration_css'] = 'label label-important';
			} else {
				$row['packing_declaration'] = 'Ready';
				$row['packing_declaration_css'] = 'label label-success';
			}

			if( $row['shipments_telex_received'] == '0' && $row['shipments_ata'] == '0000-00-00 00:00:00' ) {
				$row['telex_release'] = 'Not Ready';
				$row['telex_release_css'] = 'label label-important';
			} else {
				$row['telex_release'] = 'Ready';
				$row['telex_release_css'] = 'label label-success';
			}

			if( $row['shipments_edo_sent'] == '0' && $row['shipments_ata'] == '0000-00-00 00:00:00' ) {
				$row['edo_sent'] = 'Not Ready';
				$row['edo_sent_css'] = 'label label-important';
			} else {
				$row['edo_sent'] = 'Ready';
				$row['edo_sent_css'] = 'label label-success';
			}

			$res[] = $row;
		}
		return $res;
	}

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	/**
	 * getDestinationPort
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getDestinationPort( $id=0 ) {
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$destinationPortOptions 		= $this->tca->getMultiselectionOptions( $tcaNode );
		return $destinationPortOptions[$id];
	}

}
