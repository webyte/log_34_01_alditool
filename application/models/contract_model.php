<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Contract_model extends Abstract_model {

	public static $CONTRACT_TYPE_CORE_RANGE 		= 1;
	public static $CONTRACT_TYPE_STORE_EQUIPMENT 	= 2;
	public static $CONTRACT_TYPE_SPECIAL_BUYS 		= 3;
	public static $CONTRACT_TYPE_ONE_OFF_SPECIALS 	= 5;
	public static $CONTRACT_TYPE_ECOMMERCE 			= 6;
	public static $CONTRACT_TYPE_SEASONAL 			= 7;
	public static $CONTRACT_TYPE_AUI 				= 8;

	function __construct() {
		parent::__construct();
		$this->load->model('Delivery_model', 'delivery_model');
		$this->tablename = 'contracts';
	}

	/**
	 * @override
	 */
	public function getRecord( $id ) {
		$res = array();
		if( $id > 0 ) {
			$res = parent::getRecord( $id );
			$deliveries = $this->delivery_model->getRecordsForContract( $id );
			$res['product_code'] 			= ( count( $deliveries ) > 0 ) ? $deliveries[0]['product_code'] : '';
			$res['product_description'] 	= ( count( $deliveries ) > 0 ) ? $deliveries[0]['product_description'] : '';
		}
		return $res;
	}

	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select('contracts.id AS id,
							contracts.traffic_type AS traffic_type,
							contracts.contract_number AS contract_number,
							deliveries.product_code AS product_code,
							contracts.supplier AS supplier,
							contracts.logwin_origin_office AS logwin_origin_office,
							contracts.departure AS departure,
							contracts.advertisement_week AS advertisement_week,
							contracts.fob_date AS fob_date,
							contracts.aldi_au_wh_delivery_due_date AS aldi_au_wh_delivery_due_date,
							contracts.aldi_au_wh_delivery_due_end_date AS aldi_au_wh_delivery_due_end_date,
							contracts.cube_qa_check AS cube_qa_check,
							contracts.freight_order_number AS freight_order_number,
							contracts.cube_qa_check AS cube_qa_check,
							contracts.pickup_point AS pickup_point,
							contracts.contract_type AS contract_type');
		$this->db->from( $this->tablename );
		$this->db->join('deliveries', 'deliveries.contract_id = contracts.id AND deliveries.hidden = 0 AND deliveries.deleted = 0', 'left');
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$loo = $this->session->userdata('logwin_origin_office');
		if( !empty( $loo ) ) $this->db->where( array( $this->tablename.'.logwin_origin_office' => $loo ) );
		$this->db->where( array( $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * configures where clause for db query, depending on values in searchform.
	 */
	public function setWhereClauseForSearchparams( $recordListSearchParams ) {
		$fieldnames = array(
							'contracts_contract_number',
							'deliveries_product_code',
							'contracts_departure',
							'contracts_traffic_type',
							'contracts_advertisement_week',
							'contracts_from',
							'contracts_to',
							'contracts_logwin_origin_office',
							'contracts_supplier',
							'contracts_freight_order_number',
							'contracts_pickup_point',
							'contracts_contract_type',
							'contracts_crdate',
							'contracts_modified',
						);
		foreach ( $fieldnames as $fname ) {
			$tmpValue = array_key_exists($fname, $recordListSearchParams) ? $recordListSearchParams[$fname][0] : '';
			if( !empty($tmpValue) ){
				switch($fname){
					case 'contracts_contract_number':
						$this->db->where( 'contracts.contract_number', $tmpValue );
						break;
					case 'deliveries_product_code':
						$this->db->where( 'deliveries.product_code', $tmpValue );
						break;
					case 'contracts_departure':
						$this->db->where( 'contracts.departure', $tmpValue );
						break;
					case 'contracts_traffic_type':
						$this->db->where( 'contracts.traffic_type', $tmpValue );
						break;
					case 'contracts_advertisement_week':
						$this->db->where( 'contracts.advertisement_week', $tmpValue );
						break;
					case 'contracts_from':
					case 'contracts_to':
						$fromDate 	= array_key_exists('contracts_from', $recordListSearchParams) ? $recordListSearchParams['contracts_from'][0] : '';
						$toDate 	= array_key_exists('contracts_to', $recordListSearchParams) ? $recordListSearchParams['contracts_to'][0] : '';
						if(!empty($fromDate)) {
							$comDate = date("Y-m-d H:i:s", strtotime($fromDate) );
							$where = "contracts.fob_date >= '$comDate' ";
							$this->db->where($where);
						}
						if(!empty($toDate)) {
							$comDate = date("Y-m-d H:i:s", strtotime($toDate) );
							$where = "contracts.fob_date <= '$comDate' ";
							$this->db->where($where);
						}
						break;
					case 'contracts_logwin_origin_office':
						$this->db->where( 'contracts.logwin_origin_office', $tmpValue );
						break;
					case 'contracts_supplier':
						$this->db->where( 'contracts.supplier', $tmpValue );
						break;
					case 'contracts_freight_order_number':
						$this->db->where( 'contracts.freight_order_number', $tmpValue );
						break;
					case 'contracts_pickup_point':
						$this->db->where( 'contracts.pickup_point', $tmpValue );
						break;
					case 'contracts_contract_type':
						$this->db->where( 'contracts.contract_type', $tmpValue );
						break;
					case 'contracts_crdate':
						$where = "contracts.crdate >= '$tmpValue' ";
						$this->db->where($where);
						break;
					case 'contracts_modified':
						$where = "contracts.modified >= '$tmpValue' ";
						$this->db->where($where);
						break;
				}
			}
		}
	}

	/**
	 * getIdByContractNumber
	 * returns id of wanted contract number
	 *
	 * @param str $contractNumber - Contract number of record
	 * @return int - id of record
	 */
	public function getIdByContractNumber( $contractNumber ) {
		$out = 0;
		$query = $this->db->get_where( $this->tablename, array( 'contract_number' => $contractNumber, 'hidden' => 0, 'deleted' => 0 ) );
		if( $query->num_rows() > 0 ) {
			$row = $query->row_array();
			$out = $row['id'];
		}
		return $out;
	}

	/**
	 * getContractNumbers
	 * returns a json encoded array with associated po numbers
	 * in the frontend dynamically.
	 *
	 * @param str $query - part of the wanted po
	 * @return array - db record as array.
	 */
	public function getContractNumbers( $query ) {
		$this->db->like( 'contract_number', $query );
		$this->db->where( array( 'cube_qa_check' => 1, 'hidden' => 0, 'deleted' => 0 ) );
		$query = $this->db->get( $this->tablename );
		return $query->result_array();
	}

	/**
	 * processEventDates
	 */
	public function processEventDates( $latestRow, $previousRow ) {
		// check event dates
		//$events = array('ofu_real', 'actual_contract_ready_date', 'crdate');
		$events = array('ofu_real', 'actual_contract_ready_date');
		foreach( $events as $event ) {
			if(
				strtotime( $previousRow[$event] ) != strtotime( $latestRow[$event] )
				&& $latestRow[$event] != '0000-00-00 00:00:00'
				&& $latestRow[$event] != ''
			)
				$this->processStatusXMLExport( $latestRow['id'], $event );
		}
	}

	/**
	 * processStatusXMLExport
	 * @return string
	 */
	public function processStatusXMLExport( $id, $event ) {
		$archivePath 	= $this->config->item('archive_path');
		$data 			= $this->getStatusXMLExportRecord( $id, $event );

		// update counter
		$this->updateCounter( $id );
		$counter 		= $this->getTotalCounter();
		$contractType	= str_replace(' ', '', $data[0]['contracts_contract_type']);

		$file 			= $contractType.'_'.date('YmdHis').'_'.$counter.'_Contract.xml';
		$xml 			= $this->load->view('contract/contract_status_xml', $data[0], true);

		//print($xml);
		$this->load->helper('file');
		write_file($archivePath.$file, $xml);
		$this->welogger->log( "status xml generated - contractId: ".$id.", file: ".$file, WELogger::$LOG_LEVEL_INFO, "contract_model.processStatusXMLExport" );

		$exportPath = $this->config->item('export_path').'status/';

		copy( $archivePath.$file, $exportPath.$file );

		if( ENVIRONMENT != 'production' ) $this->sendMail( $data[0], $archivePath.$file );

		return $file;
	}

	/**
	 * getStatusXMLExportRecord
	 *
	 * @param number $id
	 * @return array - db record as array.
	 */
	public function getStatusXMLExportRecord( $id, $event ) {
		$res = array();

		$this->db->select(
			$this->getAllFieldsFromTable( 'contracts' )
		);
		$this->db->from( 'contracts' );
		$this->db->where( array( 'contracts.id' => $id, 'contracts.hidden' => 0, 'contracts.deleted' => 0 ) );
		$this->db->group_by( 'contracts.id' );
		$query = $this->db->get();

		$eventCodes = array(
			'ofu_real' => 'BRD',
			'actual_contract_ready_date' => 'CRD',
			'crdate' => 'INI',
			'no_booking_received' => 'NOB',
		);
		$conOptions = $this->getTCAgetMultiselectionOptions( 'contracts_contract_type' );

		foreach ( $query->result_array() as $row ) {
			$row['event_code'] = $eventCodes[$event];
			if( $event == 'no_booking_received' ) {
				$row['event_date'] = date('Y-m-d' );
			} else {
				$row['event_date'] = date('Y-m-d', strtotime( $row['contracts_'.$event] ) );
			}
			$row['supplier_code'] = $this->getSupplierCode( $row['contracts_supplier'] );
			$row['contracts_contract_type'] = array_key_exists($row['contracts_contract_type'], $conOptions) ? $conOptions[$row['contracts_contract_type']] : $row['contracts_contract_type'];
			$res[] = $row;
		}
		return $res;
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function prepareResultQueryForOverviewOutput( $query_result_array ){
		$res 		= array();
		$traOptions = $this->getTCAgetMultiselectionOptions( 'contracts_traffic_type' );
		$looOptions = $this->getTCAgetMultiselectionOptions( 'contracts_logwin_origin_office' );
		$supOptions = $this->getTCAgetMultiselectionOptions( 'contracts_supplier' );
		$conOptions = $this->getTCAgetMultiselectionOptions( 'contracts_contract_type' );

		foreach ( $query_result_array as $row) {
			$row['fob_date'] 						= ( $row['fob_date'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['fob_date'] ) ) : "";
			$row['aldi_au_wh_delivery_due_date'] 	= ( $row['aldi_au_wh_delivery_due_date'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['aldi_au_wh_delivery_due_date'] ) ) : "";
			$row['aldi_au_wh_delivery_due_end_date'] = ( $row['aldi_au_wh_delivery_due_end_date'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['aldi_au_wh_delivery_due_end_date'] ) ) : "";
			$row['traffic_type'] 					= array_key_exists($row['traffic_type'], $traOptions) ? $traOptions[$row['traffic_type']] : $row['traffic_type'];
			$row['contract_type'] 					= array_key_exists($row['contract_type'], $conOptions) ? $conOptions[$row['contract_type']] : $row['contract_type'];
			$row['logwin_origin_office'] 			= ( $row['logwin_origin_office'] != 0 && array_key_exists($row['logwin_origin_office'], $looOptions) ) ? $looOptions[$row['logwin_origin_office']] : '';
			$row['supplier'] 						= ( $row['supplier'] != 0 && array_key_exists($row['supplier'], $supOptions) ) ? $supOptions[$row['supplier']] : '';
			$row['cube_qa_check'] 					= ( $row['cube_qa_check'] == "1" ) ? "Yes" : "No";
			$res[] 									= $row;
		}
		return $res;
	}

	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	/**
	 * getSupplierCode
	 * @param int
	 * @return string
	 */
	private function getSupplierCode( $id ){
		$this->load->model('Supplier_model', 'supplier_model');
		$out = '';
		$suppliers = $this->supplier_model->getRecordsWhere( array( 'id' => $id, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $suppliers ) > 0 ) {
			$out = $suppliers[0]['code'];
		}
		return $out;
	}

	/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data, $file ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
		$subject 	= 'Contract '.$data['contracts_contract_number'].': Event '.$data['event_code'].' was updated to '.$data['event_date'];
		$body 		= '';

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		$this->email->attach( $file );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Contract_model.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Contract_model.sendMail' );
			return true;
		}
		$this->email->clear(TRUE);
	}

	private function updateCounter( $id ) {
		$contract = $this->getRecord( $id );
		$this->updateRecord( $id, ['status_export_counter' => $contract['status_export_counter'] + 1 ] );
	}

	private function getTotalCounter() {
		$this->db->select('SUM(status_export_counter) AS status_export_counter');
		$this->db->from('contracts');
		$query = $this->db->get();
		$res = $query->result_array();
		$counter = $res[0]['status_export_counter'];
		return sprintf("%05d", $counter);
	}

}

/* End of file contracts.php */
/* Location: ./app/models/contracts.php */
