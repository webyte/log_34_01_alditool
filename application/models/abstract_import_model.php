<?php
require_once( APPPATH.'models/abstract_model.php' );

class Abstract_import_model extends Abstract_model {

	public $lockSuffix;
	public $importSubPath;

	function __construct() {
		parent::__construct();
		$this->tablename = 'pcshipments';
		$this->load->model('Pcshipment_model', 'pcshipment_model');
		$this->load->model('Shipment_model', 'shipment_model');
	}

	/**
	 * import
	 * @param
	 * @return boolean
	 */
	public function import() {
		$this->log("import called, environment ".ENVIRONMENT);
		$success = TRUE;
		// check if cron job is locked
		if( !$this->checkIfCronJobIsLocked() ) {

			try{
				$success = $this->checkForNewEventFiles();
			} catch (Exception $e) {
				$this->welogger->log( "CronJob is locked!", WELogger::$LOG_LEVEL_WARNING, "Import.init" );
				$this->log("CronJob exception: ".$e->getMessage());

				$data['subject'] 	= 'Logwin Aldi Tool - Import exception';
				$data['body'] 		= 'Logwin Aldi Tool Import exception: '.$e->getMessage();
				$data['recipients'] = 'or@webyte.org';
				$this->sendMail( $data );

				$this->unlockCronJob();
			}

		} else {
			$this->welogger->log( "CronJob is locked!", WELogger::$LOG_LEVEL_WARNING, "Import.init" );
			$this->log("CronJob is locked!");

			$data['subject'] 	= 'Logwin Aldi Tool - Import locked';
			$data['body'] 		= 'Logwin Aldi Tool Import is locked';
			$data['recipients'] = 'or@webyte.org';

			$file = $this->config->item( 'tmp_path' ).$this->config->item( 'lock_file' ).$this->lockSuffix;
			if ( time() - filemtime( $file ) > 2 * 3600 ) $this->sendMail( $data );
		}
		return $success;
	}

	/**
	 * checkForNewEventFiles
	 * @param
	 * @return boolean
	 */
	public function checkForNewEventFiles() {
		$this->welogger->log( "checkForNewEventFiles called, environment ".ENVIRONMENT, WELogger::$LOG_LEVEL_INFO, "Import.checkForNewEventFiles" );
		$this->log("checkForNewEventFiles called, environment ".ENVIRONMENT);

		$success 	= TRUE;
		$path 		= $this->config->item('import_path').$this->importSubPath;
		$counter 	= 0;
		$excludes 	= array( ".", "..", ".DS_Store", ".svn", "procars", "add_data", "contract", ".gitkeep");

		// lock cron job
		$this->lockCronJob();

		if ( is_dir( $path ) ) {
			if ( $handle = opendir( $path ) ) {
				while ( ( $file = readdir( $handle ) ) !== false ) {
					if ( !in_array( $file, $excludes ) ) {
						$tmp = $this->readEventFile( $file );
						if( !$tmp ) $success = FALSE;
						$counter++;
					}
				}

				if( $counter == 0 ) $this->welogger->log( "Nothing to do", WELogger::$LOG_LEVEL_INFO, "Import.checkForNewEventFiles" );

				closedir( $handle );
			} else {
				$this->welogger->log( "Directory ".$path." can not be opened", WELogger::$LOG_LEVEL_ERROR, "Import.checkForNewEventFiles" );
			}
		} else {
			$this->welogger->log( "Directory ".$path." does not exist", WELogger::$LOG_LEVEL_ERROR, "Import.checkForNewEventFiles" );
		}

		// unlock cron job
		$this->unlockCronJob();
		return $success;
	}

	/**
	 * moveFileToArchive
	 * @param $file
	 * @return
	 */
	public function moveFileToArchive( $file ) {
		$this->welogger->log( "moveFileToArchive called with file ".$file, WELogger::$LOG_LEVEL_INFO, "Event_import.moveFileToArchive" );
		$this->log('moveFileToArchive called with file '.$file);

		$uploadPath 	= $this->config->item('import_path').$this->importSubPath;
		$archivePath 	= $this->config->item('archive_path').date('Y-m-d').'/';

		if( !file_exists( $archivePath ) ) mkdir( $archivePath, 0777 );

		// get new filename if alreday exist in archive
		$fileName = $this->getFileName( $file, $archivePath );
		// change file rights
		chmod ( $uploadPath.$fileName, 0777);
		// copy to archive folder
		copy( $uploadPath.$file, $archivePath.$fileName ) or $this->welogger->log( "Couldn't copy file ".$file." to archive ".$archivePath, WELogger::$LOG_LEVEL_ERROR, "Event_import.moveFileToArchive" );
		// change file rights
		chmod ( $archivePath.$fileName, 0777);
		// delete original file
		unlink( $uploadPath.$file ) or $this->welogger->log( "Couldn't delete file ".$file." from upload ".$uploadPath, WELogger::$LOG_LEVEL_ERROR, "Event_import.moveFileToArchive" );
	}

	/**
	 * checkIfCronJobIsLocked
	 * @param
	 * @return boolean
	 */
	public function checkIfCronJobIsLocked() {
		$file = $this->config->item( 'tmp_path' ).$this->config->item( 'lock_file' ).$this->lockSuffix;
		return ( ENVIRONMENT == 'production' ) ? file_exists( $file ) : false;
	}

	/**
	 * lockCronJob
	 * @param
	 * @return
	 */
	public function lockCronJob() {
		$file = $this->config->item( 'tmp_path' ).$this->config->item( 'lock_file' ).$this->lockSuffix;
		$this->welogger->log( "Cron Job locked, file: ".$file, WELogger::$LOG_LEVEL_INFO, "Import.lockCronJob" );
		if( ENVIRONMENT == 'production' ) touch( $file ) or $this->welogger->log( "Couldn't create file ".$file, WELogger::$LOG_LEVEL_ERROR, "Import.lockCronJob" );
	}

	/**
	 * unlockCronJob
	 * @param
	 * @return
	 */
	public function unlockCronJob() {
		$file = $this->config->item( 'tmp_path' ).$this->config->item( 'lock_file' ).$this->lockSuffix;
		$this->welogger->log( "Cron Job unlocked, file: ".$file, WELogger::$LOG_LEVEL_INFO, "Import.unlockCronJob" );
		if( ENVIRONMENT == 'production' ) unlink( $file ) or $this->welogger->log( "Couldn't delete file ".$file, WELogger::$LOG_LEVEL_ERROR, "Import.unlockCronJob" );
	}

	/**
	 * log
	 * @param $msg
	 * @return
	 */
	public function log( $msg ) {
		print( date('Y-m-d H:i:s').': '.$msg."\n");
	}

	/**
	 * sendMail
	 * @param $bookingId
	 * @return boolean
	 */
	public function sendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		$fromMail 					= $this->config->item('from_mail');
		$fromName 					= $this->config->item('from_name' );
		$subject 					= $data['subject'];
		$mailText 					= $data['body'];
		$recipient 					= $data['recipients'];
		$body						= $mailText;

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $recipient );
		$this->email->subject( $subject );
		$this->email->message( $body );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( "Sending Mail to ".$recipient." failed, error: ".$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, "Import_model.sendMail" );
			return false;
		} else {
			$this->welogger->log( "Mail was sent to ".$recipient." \nwith subject: ".$subject." \nand message: ".$body, WELogger::$LOG_LEVEL_INFO, "Import_model.sendMail" );
			return true;
		}
		$this->email->clear(TRUE);
	}

	/**
	 * getFileName
	 * @param $file
	 * @return string
	 */
	public function getFileName( $file, $archivePath ) {
		// check if file already exists in archive
		if( file_exists( $archivePath.$file ) ) {
			$str = basename( $file,".xml" ).'_'.time().'.xml';
			$this->welogger->log( "file already exists new name is ".$str, WELogger::$LOG_LEVEL_INFO, "Event_import.getFileName" );
			return $str;
		} else {
			return $file;
		}
	}

	/**
	 * getValueFromXML
	 * @param $file
	 * @return
	 */
	public function getValueFromXML( $xml, $path ) {
		$data = $xml->xpath( $path );
		return  isset( $data[0] ) ? (string) $data[0] : '';
	}

	/**
	 * getShipmentId
	 * @param containerNumber
	 * @return int
	 */
	public function getShipmentId( $containerNumber, $hblHawb )
	{
		$out = 0;
		$shipments = $this->pcshipment_model->getRecordsWhere( array( 'container_number'=> $containerNumber, 'hbl_hawb' => $hblHawb,'hidden' => 0, 'deleted' => 0 ) );
		if( count( $shipments ) ) {
			$out = $shipments[0]['id'];
		}
		return $out;
	}


}

/* End of file orders.php */
/* Location: ./app/models/orders.php */
