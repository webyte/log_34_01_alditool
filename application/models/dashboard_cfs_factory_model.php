<?php
require_once( APPPATH.'models/abstract_model.php' );

class Dashboard_cfs_factory_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->load->driver('cache', array('adapter' => 'file'));
	}

	/**
	 * getRecords
	 * returns the db-records with the given search params
	 *
	 * @return array - db record as array.
	 */
	public function getRecords() {
		if( !$res = $this->cache->get('dashboard_cfs_factory_getrecords') ) {
			$res = array();
			$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
			$this->db->from( 'contracts' );
			$this->db->join( 'deliveries', 'deliveries.contract_id = contracts.id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
			$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
			$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
			$this->db->where( array( 'contracts.hidden' => 0, 'contracts.deleted' => 0 ) );
			$this->db->where( array( 'contracts.fob_date >=' => date( 'Y-m-d', strtotime( '-14 days', time() ) ).' 00:00:00', 'contracts.fob_date <=' => date( 'Y-m-d', strtotime( '+7 days', time() ) ).' 00:00:00' ) );
			$this->db->group_by( 'deliveries.id' );
			$this->db->order_by( 'contracts.id' );
			$query = $this->db->get();

			foreach ( $query->result_array() as $row ) {
				$res[] = $row;
			}
			$res = $this->prepareResultQueryForOutput( $res );
		}

		return $res;
	}

	/**
	 * generateCache
	 *
	 * @return
	 */
	public function generateCache() {
		$res = $this->getRecords();
		$this->cache->file->save('dashboard_cfs_factory_getrecords', $res, 600);
	}

	/**
	 * isCached
	 *
	 * @return
	 */
	public function isCached() {
		return file_exists( $this->config->item('app_path').'cache/dashboard_ocean_freight_getrecords' );
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function prepareResultQueryForOutput( $query_result_array ){
		$res = array();

		foreach ( $query_result_array as $row) {

			$etd 			= strtotime( $row['shipments_etd'] );
			$cutOff 		= $this->getCutOff( $row['shipments_departure_port'] );
			$etdMinusCutOff = date( 'Y-m-d H:i:s', strtotime( '-'.$cutOff.' days', $etd ) );

			$row['shipments_intended_cfs_cut_off'] = ( $row['shipments_etd'] != "0000-00-00 00:00:00" && !empty( $row['shipments_etd'] ) ) ? $etdMinusCutOff : '';

			$dates = array(
				'shipments_crdate',
				'shipments_approval_date',
				'shipments_container_loaded',
				'shipments_export_customs_cleared',
				'shipments_container_gate_in',
				'contracts_received_in_cfs',
				'shipments_intended_cfs_cut_off',
				'shipments_cy_cut_off',
			);

			foreach( $dates as $date ) {
				$row[$date.'_timestamp'] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? strtotime( $row[$date] ) : 0;
				$row[$date] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? date( $this->config->item('date_format'), strtotime( $row[$date] ) ) : "";
			}

			$traOptions = $this->getTCAgetMultiselectionOptions( 'contracts_traffic_type' );
			$row['contracts_traffic_type'] = array_key_exists($row['contracts_traffic_type'], $traOptions) ? $traOptions[$row['contracts_traffic_type']] : $row['contracts_traffic_type'];

			$row['deliveries_volume'] = sprintf($this->config->item('doublevalues_format'), $row['deliveries_volume']);
			$res[] = $row;
		}
		return $res;
	}

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}

	private function getCutOff( $departurePort ){
		$this->load->model('Departure_model', 'departure_model');
		$rows = $this->departure_model->getRecordsWhere( array( 'name' => $departurePort, 'hidden' => 0, 'deleted' => 0 ) );

		return count( $rows ) > 0 ? $rows[0]['cut_off_time'] : 0;
	}
}
