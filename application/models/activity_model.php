<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Activity_model extends Abstract_model {

	public static $DATA_TYPE_CONTRACT 	= 'contracts';
	public static $DATA_TYPE_DELIVERY 	= 'deliveries';
	public static $DATA_TYPE_SHIPMENT 	= 'shipments';

	public static $ACTION_CONTRACT_CREATED 	= 'Contract created';
	public static $ACTION_CONTRACT_UPDATED 	= 'Contract updated';
	public static $ACTION_CONTRACT_DELETED 	= 'Contract deleted';
	function __construct() {
		parent::__construct();
		$this->tablename = 'activities';
	}

	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams, $overviewFields ) {

		$start 				= ( !empty( $recordListSearchParams['activities_from'][0] ) ) ? date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '.', $recordListSearchParams['activities_from'][0].' 00:00:00' ) ) ) : '';
		$end 				= ( !empty( $recordListSearchParams['activities_to'][0] ) ) ? date( 'Y-m-d H:i:s', strtotime( str_replace( '/', '.', $recordListSearchParams['activities_to'][0].' 23:59:59' ) ) ) : '';
		$contractNumber 	= $recordListSearchParams['contracts_contract_number'][0];
		$containerNumber 	= $recordListSearchParams['shipments_container_number'][0];
		$res				= array();

		$this->db->select( 'activities.id AS id,
							activities.record_type AS record_type,
							activities.record_type_label AS record_type_label,
							activities.record_field AS record_field,
							activities.record_field_label AS record_field_label,
							activities.record_data_old AS record_data_old,
							activities.record_data_new AS record_data_new,
							activities.action_type AS action_type,
							activities.contract_number AS contract_number,
							activities.container_number AS container_number,
							activities.crdate AS crdate,
							users.username AS username' );
		$this->db->from( 'activities' );
		$this->db->join( 'users', 'activities.user_id = users.id AND users.hidden = 0 AND users.deleted = 0', 'left' );
		if( !empty( $contractNumber ) ) $this->db->where( array( 'activities.contract_number' => $contractNumber ) );
		if( !empty( $containerNumber ) ) $this->db->where( array( 'activities.container_number' => $containerNumber ) );
		if( !empty( $start ) ) $this->db->where( array( 'activities.crdate >=' => $start ) );
		if( !empty( $end ) ) $this->db->where( array( 'activities.crdate <=' => $end ) );
		$this->db->order_by( 'activities.contract_number, activities.container_number, activities.crdate' );

		$query 	= $this->db->get();
		$res	= $query->result_array();
		$out	= array();
		foreach( $res as $row ) {
            if( $row['record_type'] == Activity_model::$DATA_TYPE_CONTRACT || $row['record_type'] == Activity_model::$DATA_TYPE_DELIVERY ) {
				$out[$row['contract_number']][] = $this->prepareResultQueryForOverviewOutput( $row );
			} else if( $row['record_type'] == Activity_model::$DATA_TYPE_SHIPMENT) {
				$out[$row['container_number']][] = $this->prepareResultQueryForOverviewOutput( $row );
			}
		}
		return $out;
	}

	/**
	 * logChanges
	 * calculates diff and save to db
	 *
	 * @param array $latestData - latest date
	 * @param array $previousData - previous date
	 * @param string $type - data type
	 * @param string $number - number (contract/container)
	 * @return
	 */
	public function logChanges( $latestData, $previousData, $number='', $type, $action='' ) {
		$id						= $latestData['id'];
		$latestData 			= $this->cleanRecord( $latestData );
		$previousData 			= $this->cleanRecord( $previousData );
		$diff					= $this->generateDiff( $previousData, $latestData );

		foreach( $diff as $key=>$value ) {
			$data						= array();
			$data['record_id']			= $id;
			$data['record_type'] 		= $type;
            $data['record_type_label'] 	= $type;
            $data['record_field'] 		= $key;
			$data['record_field_label']	= $this->tca->getFormLabelById( $type.'_'.$key );
			$data['user_id'] 			= $this->session->userdata('userId');
			$data['record_data_old'] 	= $value['old'];
			$data['record_data_new'] 	= $value['new'];
			$data['action_type']		= $action;

			if( empty( $data['action_type'] ) ) {
				if ($type == Activity_model::$DATA_TYPE_CONTRACT || $type == Activity_model::$DATA_TYPE_DELIVERY) {
					if (empty($previousData)) {
						$data['action_type'] = Activity_model::$ACTION_CONTRACT_CREATED;
					} else {
						$data['action_type'] = Activity_model::$ACTION_CONTRACT_UPDATED;
					}
				}
			}

			switch( $type ) {
				case Activity_model::$DATA_TYPE_CONTRACT:
					$data['contract_number'] = $number;
					break;
				case Activity_model::$DATA_TYPE_DELIVERY:
					$data['contract_number'] = $number;
					break;
				case Activity_model::$DATA_TYPE_SHIPMENT:
					$data['container_number'] = $number;
					break;
			}

			$this->createNew( $data );
		}
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	/**
	 * cleanRecord
	 *
	 * @param array $arr - record
	 * @return array
	 */
	private function cleanRecord( $arr ) {
		unset( $arr['id'] );
		unset( $arr['crdate'] );
		unset( $arr['hidden'] );
		unset( $arr['deleted'] );
		unset( $arr['modified'] );
		unset( $arr['contract_id'] );
		unset( $arr['status'] );
		return $arr;
	}

	private function getLatestVersion( $data ) {
		$this->db->select_max('version');
		$query 		= $this->db->get_where( $this->tablename, array( 'record_id' => $data['record_id'], 'record_type' => $data['record_type'] ) );
		$row 		= $query->row_array();
		$version 	= $row['version'] != NULL ? $row['version'] : 0;
		return $version+1;
	}

	private function generateDiff( $previousData, $latestData ) {
		$out = array();
		foreach( $latestData as $key=>$latest ) {
			$previous = ( count( $previousData ) > 0 ) ? $previousData[$key] : '';
			if( $previous != $latest ) {
				$tmp = array( 'old' => $previous, 'new' => $latest );
				$out[$key] = $tmp;
			}
		}
		return $out;
	}


	private function prepareResultQueryForOverviewOutput( $row ){
		$row 			= $this->getSelectValueFromTCA( $row );
		$row 			= $this->getOtherValues( $row );
		$row['crdate'] 	= ( $row['crdate'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('datetime_format'), strtotime( $row['crdate'] ) ) : "";
		return $row;
	}

	private function getSelectValueFromTCA( $row ){
		$id						= $row['record_type'].'_'.$row['record_field'];
		$tcaNode 				= $this->tca->getColumnNodeById( $id );
		$options 				= $this->tca->getMultiselectionOptions( $tcaNode );
		if( count( $options ) > 0 ) {
			$row['record_data_old'] = array_key_exists($row['record_data_old'], $options) ? $options[$row['record_data_old']] : $row['record_data_old'];
			$row['record_data_new'] = array_key_exists($row['record_data_new'], $options) ? $options[$row['record_data_new']] : $row['record_data_new'];
			$row['record_data_old'] = ( !empty( $row['record_data_old'] ) && $row['record_data_old'] != 'please select ...' ) ? $row['record_data_old'] : '';
			$row['record_data_new'] = ( !empty( $row['record_data_new'] ) && $row['record_data_old'] != 'please select ...' ) ? $row['record_data_new'] : '';
		}
		return $row;
	}

	private function getOtherValues( $row ) {
		$id	= $row['record_field'];
		if( $id == 'cube_qa_check' ) {
			$row['record_data_old'] = ( $row['record_data_old'] == "1" ) ? "Yes" : "No";
			$row['record_data_new'] = ( $row['record_data_new'] == "1" ) ? "Yes" : "No";
		}
		if( $id == 'telex_received' ) {
			$row['record_data_old'] = ( $row['record_data_old'] == "1" ) ? "Yes" : "No";
			$row['record_data_new'] = ( $row['record_data_new'] == "1" ) ? "Yes" : "No";
		}

		$dateIds = array( 'fob_date', 'etd', 'eta', 'atd', 'ata', 'cus', 'edl', 'adl', 'import_date', 'ofu_scheduled', 'ofu_real', 'on_sale_date', 'fob_date', 'aldi_au_wh_delivery_due_date' );
		if( in_array( $id, $dateIds ) ){
			$row['record_data_old'] = ( !empty( $row['record_data_old'] ) && $row['record_data_old'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['record_data_old'] ) ) : "";
			$row['record_data_new'] = ( !empty( $row['record_data_new'] ) && $row['record_data_new'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['record_data_new'] ) ) : "";
		}
		return $row;
	}
}

/* End of file activity_model.php */
/* Location: ./app/models/activity_model.php */
