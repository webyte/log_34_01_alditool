<?php
require_once ( APPPATH.'models/abstract_model'.EXT);

class Dfu_model extends Abstract_model {

    function __construct() {
        parent::__construct();
		$this->load->model('Pic_model', 'pic_model');
		$this->load->model('User_model', 'user_model');
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Supplier_model', 'supplier_model');
    }

	/**
	 * getRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getRecords( $recordListSearchParams, $overviewFields ) {
		$res 	= array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
		$this->db->join( 'contracts', 'contracts.id = deliveries.contract_id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );

		$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
		$this->db->group_by( 'shipments.id' );
		$query 	= $this->db->get();
		$res 	= array();
		foreach ( $query->result_array() as $row ) {
			$suppliers = array();
			$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId( $row['shipments_id'] );
			foreach( $deliveries as $delivery ) {
				$contract = $this->contract_model->getRecord( $delivery['contract_id'] );
				if( !empty( $contract['supplier'] ) ) {
					$supplier = $this->supplier_model->getRecord( $contract['supplier'] );
					$suppliers[] = $supplier;
				}
			}
			$row['suppliers'] = $suppliers;
			$res[] = $row;
		}

		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * getRecipients
	 * returns array with PIC recipients
	 *
	 * @param string $onSaleWeek - search params
	 * @return array - recipient data
	 */
	public function getRecipients( $data ) {
		$out = array();
		foreach( $data as $row ) {
			foreach( $row['suppliers'] as $supplier ) {
				$contactpersons = $this->contactperson_model->getRecordsForSupplier( $supplier['id'] );
				foreach( $contactpersons as $contactperson ) {
					$contactperson['supplier_name'] = $supplier['name'];
					if( !in_array( $contactperson, $out ) ){
						$out[] = $contactperson;
					}
				}
			}
		}
		return $out;
	}

	/**
	 * getCCRecipientsByOnSaleWeek
	 * returns array with PIC recipients
	 *
	 * @param string $onSaleWeek - search params
	 * @return array - recipient data
	 */
	public function getCCRecipientsByOnSaleWeek( $onSaleWeek ) {
		$out 		= array();
		if( !empty( $onSaleWeek ) ) {
			$records 	= $this->pic_model->getRecordsWhere( array( 'on_sale_week' => $onSaleWeek, 'hidden' => 0, 'deleted' => 0 ) );
			foreach( $records as $record ) {
				$row 	= $this->user_model->getRecord($record['user_id']);
				$out[]	= $row['email'];
			}
		}
		$out[] = 'aldicustoms@logwin-logistics.com';
		//if( ENVIRONMENT == 'production' ) $out[] = 'Logistics@aldi.com.au';
		return implode( ',', $out );
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function prepareResultQueryForOverviewOutput( $query_result_array ){
		$res = array();
		foreach ( $query_result_array as $row) {
			$res[] = $row;
		}
		return $res;
	}

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

}

/* End of file dfu_model.php */
/* Location: ./app/models/dfu_model.php */
