<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Supplier_portal_model extends Abstract_model {


	function __construct() {
		parent::__construct();
		$this->load->model('Document_model', 'document_model');
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('User_model', 'user_model');
	}

	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams ) {
		$supplier = $this->user_model->getSupplier();
		$res = array();
		$this->db->select(
			'shipments_deliveries_mm.id AS id, '.
			$this->getAllFieldsFromTable( 'deliveries' ).', '.
			$this->getAllFieldsFromTable( 'contracts' ).', '.
			$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
			$this->getAllFieldsFromTable( 'customs' ).', '.
			$this->getAllFieldsFromTable( 'pcshipments' ).', '.
			$this->getAllFieldsFromTable( 'shipments' )
		);
		$this->db->from( 'shipments_deliveries_mm' );
		$this->db->join( 'deliveries', 'shipments_deliveries_mm.delivery_id = deliveries.id AND deliveries.hidden = 0 AND deliveries.deleted = 0');
		$this->db->join( 'contracts', 'contracts.id = deliveries.contract_id AND contracts.hidden = 0 AND contracts.deleted = 0');
		$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0');
		$this->db->join( 'customs', 'contracts.id = customs.contract_id AND customs.hidden = 0 AND customs.deleted = 0', 'left');
		$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.delivery_id = deliveries.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join( 'pcshipments', 'pcshipments.id = pcshipments_deliveries_mm.pcshipment_id AND pcshipments.hidden = 0 AND pcshipments.deleted = 0', 'left');
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		if( !empty( $supplier ) ) $this->db->where( array( 'contracts.supplier' => $supplier ) );
		$this->db->where( '(deliveries.contract_type != 1 AND deliveries.contract_type != 8)' );
		$this->db->where( array( 'shipments_deliveries_mm.hidden' => 0, 'shipments_deliveries_mm.deleted' => 0 ) );
		$this->db->group_by( 'shipments_deliveries_mm.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	public function updateMandatoryDocumentsStatus( $id ) {
		$status = array();
		$mandatoryDocumentTypes = $this->document_model->getMandatoryDocumentTypes( $id );
		$rows = $this->document_model->getDocumentsByPid( $id );
		foreach( $rows as $row ) {
			if( in_array( $row['document_type'], $mandatoryDocumentTypes ) ) $status[] = $row['status'];
		}

		$status = array_unique( $status );
		sort( $status );
		$this->shipment_delivery_model->updateRecord( $id, array( 'documents_status' => $status[0] ) );
	}

	public function getShipmentNumberById( $id ) {
		$this->load->model('Shipment_model', 'shipment_model');
		$row 		= $this->shipment_delivery_model->getRecord( $id );

		$shipmentId = $row['shipment_id'];
		$shipment 	= $this->shipment_model->getRecord( $shipmentId );
		return $shipment['shipment_number'];
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	/**
	 * configures where clause for db query, depending on values in searchform.
	 */
	public function setWhereClauseForSearchparams( $recordListSearchParams ) {
		$fieldnames = array(
			'shipments_deliveries_mm_id',
			'deliveries_contract_number',
			'deliveries_product_code',
			'deliveries_dc',
			'deliveries_advertisement_week',
			'shipments_container_number',
			'shipments_destination_port',
			'event',
			'shipments_from',
			'shipments_to',
			'documents_available',
		);
		foreach ( $fieldnames as $fname ) {
			$tmpValue = array_key_exists($fname, $recordListSearchParams) ? $recordListSearchParams[$fname][0] : '';
			if( $tmpValue != '' ){
				switch($fname){
					case 'shipments_deliveries_mm_id':
						$this->db->where( 'shipments_deliveries_mm.id', $tmpValue );
						break;
					case 'deliveries_contract_number':
						$this->db->where( 'deliveries.contract_number', $tmpValue );
						break;
					case 'deliveries_product_code':
						$this->db->where( 'deliveries.product_code', $tmpValue );
						break;
					case 'deliveries_dc':
						$this->db->where( 'deliveries.dc', $tmpValue );
						break;
					case 'deliveries_advertisement_week':
						$this->db->where( 'deliveries.advertisement_week', $tmpValue );
						break;
					case 'shipments_container_number':
						$this->db->where( 'shipments.container_number', $tmpValue );
						break;
					case 'shipments_destination_port':
						if($tmpValue != 0) $this->db->where( 'shipments.destination_port', $tmpValue );
						break;
					case 'event':
						$fromDate 	= array_key_exists('shipments_from', $recordListSearchParams) ? $recordListSearchParams['shipments_from'][0] : '';
						$toDate 	= array_key_exists('shipments_to', $recordListSearchParams) ? $recordListSearchParams['shipments_to'][0] : '';
						$table 		= $tmpValue != 'fob_date' ? 'shipments' : 'contracts';
						if(!empty($fromDate)) {
							$fromDate 	= str_replace( '/', '.', $fromDate );
							$comDate 	= date("Y-m-d H:i:s", strtotime($fromDate.' 00:00:00') );
							$where 		= $table.".".$tmpValue." >= '$comDate' ";
							$this->db->where($where);
						}

						if(!empty($toDate)) {
							$toDate 	= str_replace( '/', '.', $toDate );
							$comDate 	= date("Y-m-d H:i:s", strtotime($toDate.' 23:59:59') );
							$where 		= $table.".".$tmpValue." <= '$comDate' ";
							$this->db->where($where);
						}
						break;
					case 'documents_available':
						$this->db->where( 'shipments_deliveries_mm.documents_status', $tmpValue );
						break;
				}
			}
		}
	}

	/**
	 * getTCAgetMultiselectionOptions
	 *
	 * @return array
	 */
	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}

	/**
	 * prepareResultQueryForOverviewOutput
	 *
	 * @return array
	 */
	private function prepareResultQueryForOverviewOutput( $query_result_array ){
		$res = array();
		$dateFormat = empty( $date_format ) ? $this->config->item('date_format') : $date_format;
		$desOptions = $this->getTCAgetMultiselectionOptions( 'shipments_destination_port' );
		foreach ( $query_result_array as $row) {
			$row['shipments_destination_port'] = ( $row['shipments_destination_port'] != 0 && array_key_exists($row['shipments_destination_port'], $desOptions) ) ? $desOptions[$row['shipments_destination_port']] : '';
			$dates = array(
				'shipments_etd',
				'shipments_eta',
				'shipments_atd',
				'shipments_ata',
			);
			foreach( $dates as $date ) {
				if( isset( $row[$date] ) ) {
					$row[$date] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? date( $dateFormat, strtotime( $row[$date] ) ) : '';
				} else {
					$row[$date] = '';
				}
			}
			$res[] = $row;
		}
		return $res;
	}

	/**
	 * getAllFieldsFromTable
	 *
	 * @return string
	 */
	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}
}

/* End of file activity_model.php */
/* Location: ./app/models/activity_model.php */
