<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Dc_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'dcs';
	}

	/**
	 * createNew
	 * creates new record in table with the given insertdata
	 * retrns the id of the new entry or -1 if something went wrong
	 * @param array $insertData - associative array with data to insert into new table row
	 * @return int new table-row id
	 */
	function createNew( $insertData ) {
		return  parent::createNew( $insertData );
	}

	/**
	 * updateRecord
	 * updates the record with the given id
	 *
	 * @param int $id - row id
	 * @param array $insertData - updated data
	 */
	function updateRecord($id, $insertData ) {
		parent::updateRecord($id, $insertData );
	}

	/**
	 * getRecord
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	function getRecord( $id ) {
		return parent::getRecord( $id );
	}

	/**
	 * deleteRecord
	 * deletes record with given id from table
	 *
	 * @param int $id - row id
	 *
	 */
	function deleteRecord( $id ) {
		parent::deleteRecord( $id );
	}

}

/* End of file dc_model.php */
/* Location: ./app/models/dc_model.php */
