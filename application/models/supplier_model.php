<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Supplier_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'suppliers';
		$this->load->model('Contactperson_model', 'contactperson_model');
	}

	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select('suppliers.id AS id,
							suppliers.name AS name,
							suppliers.street AS street,
							suppliers.city AS city,
							suppliers.contact_phone AS contact_phone,
							suppliers.code AS code,
							contactpersons.name AS contact_name,
							contactpersons.email AS contact_mail');
		$this->db->from( $this->tablename );
		$this->db->join('contactpersons', 'suppliers.id = contactpersons.supplier_id AND contactpersons.hidden = 0 AND contactpersons.deleted = 0', 'left');
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$row['contact_name'] = $this->getContactNamesBySupplierId( $row['id'] );
			$row['contact_mail'] = $this->getContactMailsBySupplierId( $row['id'] );
			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * getRecipientsByContactpersonIds
	 * returns a comma separated list of email adresses.
	 *
	 * @param array $ids - contactperson ids
	 * @return string - comma separated list of email adresses
	 */
	public function getRecipientsByContactpersonIds( $ids ) {
		$out = array();
		foreach( $ids as $id ) {
			$contactPerson 	= $this->contactperson_model->getRecord( $id );
			$tmp 			= array();
			$tmp['email'] 	= $contactPerson['email'];
			$tmp['name'] 	= $contactPerson['name'];
			$out[] 			= $tmp;
		}
		return $out;
	}

	/**
	 * getContactMailsBySupplierId
	 * returns a comma separated list of email adresses.
	 *
	 * @param int $id - supplier id
	 * @return string - comma separated list of email adresses
	 */
	public function getContactMailsBySupplierId( $supplierId ) {
		$out = array();
		$records = $this->contactperson_model->getRecordsWhere( array( 'supplier_id' => $supplierId, 'hidden' => 0, 'deleted' => 0 ));
		foreach( $records as $row ) {
			$out[] = $row['email'];
		}
		return implode(', ',$out);
	}

	/**
	 * configures where clause for db query, depending on values in searchform.
	 */
	public function setWhereClauseForSearchparams( $recordListSearchParams ) {
		$fieldnames = array(
			'suppliers_name',
		);
		foreach ( $fieldnames as $fname ) {
			$tmpValue = array_key_exists($fname, $recordListSearchParams) ? $recordListSearchParams[$fname][0] : '';
			if( !empty($tmpValue) ){
				switch($fname){
					case 'suppliers_name':
						$this->db->where( 'suppliers.name LIKE', '%'.$tmpValue.'%' );
						break;
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function getContactNamesBySupplierId( $supplierId ) {
		$out = array();
		$records = $this->contactperson_model->getRecordsWhere( array( 'supplier_id' => $supplierId, 'hidden' => 0, 'deleted' => 0 ));
		foreach( $records as $row ) {
			$out[] = $row['name'];
		}
		return implode(', ',$out);
	}


	private function prepareResultQueryForOverviewOutput( $query_result_array ){
		$res 		= array();
		foreach ( $query_result_array as $row) {
			$res[] 								= $row;
		}
		return $res;
	}


}

/* End of file supplier.php */
/* Location: ./app/models/supplier.php */
