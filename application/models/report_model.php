<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Report_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->load->model('Supplier_model', 'supplier_model');
		$this->load->model('Office_model', 'office_model');
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Dc_model', 'dc_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->tablename = 'deliveries';
	}

	/**
	 * getReportRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getReportRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select(
			$this->getAllFieldsFromTable( 'deliveries' ).', '.
			$this->getAllFieldsFromTable( 'contracts' ).', '.
			$this->getAllFieldsFromTable( 'shipments' ).', '.
			$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
			$this->getAllFieldsFromTable( 'pcshipments' )
		);
		$this->db->from( 'deliveries' );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND ((contracts.hidden = 0 AND contracts.deleted = 0) OR (contracts.hidden = 1 AND contracts.deleted = 1))' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND ((shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0) OR (shipments_deliveries_mm.hidden = 1 AND shipments_deliveries_mm.deleted = 1))', 'left' );
		$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND ((shipments.hidden = 0 AND shipments.deleted = 0) OR (shipments.hidden = 1 AND shipments.deleted = 1))', 'left' );
		$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.delivery_id = deliveries.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0');
		$this->db->join( 'pcshipments', 'pcshipments.id = pcshipments_deliveries_mm.pcshipment_id AND pcshipments.hidden = 0 AND pcshipments.deleted = 0');
		$this->setWhereClauseForSearchparams( $recordListSearchParams );

		$loo = $this->session->userdata('logwin_origin_office');
		if( !empty( $loo ) ) $this->db->where( array( 'contracts.logwin_origin_office' => $loo ) );

		$this->db->where( '((deliveries.hidden = 0 AND deliveries.deleted = 0) OR (deliveries.hidden = 1 AND deliveries.deleted = 1))' );
		$this->db->group_by( 'deliveries.id,shipments.id' );
		$this->db->order_by( 'shipments.hbl_hawb,deliveries.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {

			if( $row['shipments_id'] > 0 ) {
				$row['deliveries_case_quantity'] 	= $row['shipments_deliveries_mm_case_quantity'];
				$row['deliveries_case_quantity'] 	= $row['shipments_deliveries_mm_case_quantity'];
				$row['deliveries_volume'] 			= $row['shipments_deliveries_mm_volume'];
				$row['deliveries_gross_weight'] 	= $row['shipments_deliveries_mm_gross_weight'];
			}

			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res, TRUE );
	}

	/**
	 * getBookingReportRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getBookingReportRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( $this->tablename );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
		$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );

		$this->db->where( array( 'contracts.ofu_real !=' => '0000-00-00 00:00:00', 'shipments.atd' => '0000-00-00 00:00:00' ) );
		$this->db->where( array( $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( 'deliveries.id,shipments.id' );
		$this->db->order_by( 'shipments.hbl_hawb,deliveries.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {

			if( $row['shipments_id'] > 0 ) {
				$row['deliveries_case_quantity'] 	= $row['shipments_deliveries_mm_case_quantity'];
				$row['deliveries_case_quantity'] 	= $row['shipments_deliveries_mm_case_quantity'];
				$row['deliveries_volume'] 			= $row['shipments_deliveries_mm_volume'];
				$row['deliveries_gross_weight'] 	= $row['shipments_deliveries_mm_gross_weight'];
			}

			$row['hbl_hawb_dc'] 	= $row['shipments_deliveries_mm_hbl_hawb'].$row['deliveries_dc'];
			$row['line_item_id'] 	= 'AU'.$row['contracts_product_code'].date('ym', strtotime( $row['contracts_on_sale_date'] ) ).'SYD';
			$row['mode'] 			= 'Sea';
			$containerType = $this->getContainerTypeFromTCA( $row['shipments_container_type'] );
			$containerSize = $this->getContainerSize( $row['shipments_container_size'] );
			$row['container_booked'] = $containerSize.$containerType;//$this->getContainerType( $row['shipments_container_type'] );
			$row['forwarder'] 		= 'Logwin';
			$row['cancel_booking'] 	= 'N';
			$row['contracts_contract_number'] = '';
			$row['deliveries_product_code'] = '';
			$row['shipments_carrier'] = '';
			$row['excel_etd'] = $row['shipments_etd'];
			$row['excel_eta'] = $row['shipments_eta'];

			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res, false );
	}

	/**
	 * getShipmentReportRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getShipmentReportRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( $this->tablename );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
		$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );

		$this->db->where( array( 'shipments.atd !=' => '0000-00-00 00:00:00' ) );
		$this->db->where( array( $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( 'deliveries.id,shipments.id' );
		$this->db->order_by( 'shipments.hbl_hawb,deliveries.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {

			if( $row['shipments_id'] > 0 ) {
				$row['deliveries_case_quantity'] 	= $row['shipments_deliveries_mm_case_quantity'];
				$row['deliveries_unit_quantity'] 	= $row['shipments_deliveries_mm_unit_quantity'];
				$row['deliveries_volume'] 			= $row['shipments_deliveries_mm_volume'];
				$row['deliveries_gross_weight'] 	= $row['shipments_deliveries_mm_gross_weight'];
			}
			$row['hbl_hawb_dc'] 	= $row['shipments_deliveries_mm_hbl_hawb'].$row['deliveries_dc'];
			$row['line_item_id'] 	= 'AU'.$row['contracts_product_code'].date('ym', strtotime( $row['contracts_on_sale_date'] ) ).'SYD';
			$row['container_type'] 	= $this->getContainerSize( $row['shipments_container_size'] ).$this->getContainerTypeFromTCA( $row['shipments_container_type'] );
			$row['cy_delivery'] 	= ' ';
			$row['mode'] 			= 'Sea';
			$row['contracts_contract_number'] = '';
			$row['deliveries_product_code'] = '';
			$row['shipments_carrier'] = '';
			$row['excel_etd'] = $row['shipments_etd'];
			$row['excel_eta'] = $row['shipments_eta'];

			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res, false );
	}

	/**
	 * getExcRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getExcRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'contracts' );
//		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
//		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0', 'left');
//		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->db->join( 'deliveries', 'deliveries.contract_id = contracts.id AND deliveries.hidden = 0 AND deliveries.deleted = 0');
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
		$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );

		$this->db->where( array( 'contracts.hidden' => 0, 'contracts.deleted' => 0 ) );
		$this->db->group_by( 'contracts.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$row['shipments_eta_minus_seven'] = ( $row['shipments_eta'] != '0000-00-00 00:00:00' ) ? date( 'Y-m-d', strtotime( '-7 days', strtotime( $row['shipments_eta'] ) ) ) : '0000-00-00 00:00:00';
			if( strtotime( $row['shipments_atd'] ) < strtotime( '+2 days', strtotime( $row['shipments_etd'] ) ) ) {
				$row['shipments_atd'] = '0000-00-00 00:00:00';
			}
			if( strtotime( $row['shipments_ata'] ) < strtotime( '+2 days', strtotime( $row['shipments_eta'] ) ) ) {
				$row['shipments_ata'] = '0000-00-00 00:00:00';
			}

			$dayVariances = '';
			if( $row['contracts_actual_contract_ready_date'] != '0000-00-00 00:00:00' && $row['contracts_fob_date'] != '0000-00-00 00:00:00' ) {
				$dayVariances = round(( strtotime( $row['contracts_actual_contract_ready_date'] ) - strtotime( $row['contracts_fob_date'] ) ) / ( 60 * 60 * 24 ) );
			}
			$row['contracts_day_variances'] =  $dayVariances;

			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res, TRUE );
	}


	/**
	 * getCLPDeliveryRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getCLPDeliveryRecords( $recordListSearchParams, $overviewFields ) {

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'pcshipments' ).', '.$this->getAllFieldsFromTable( 'shipments' ) );
		$this->db->from( 'pcshipments' );
		$this->db->join( 'shipments', 'shipments.id = pcshipments.shipment_id AND shipments.hidden = 0 AND shipments.deleted = 0' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0', 'left');
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );

		$this->db->where( array( 'pcshipments.container_number !=' => '', 'pcshipments.hidden' => 0, 'pcshipments.deleted' => 0 ) );
		$this->db->group_by( 'pcshipments.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			// related deliveries
			$deliveries 		= $this->shipment_delivery_model->getDeliveriesByShipmentId( $row['shipments_id'] );
			$contractNumbers 	= array();
			$productCodes 		= array();
			$dcs 				= array();
			$advertisementWeeks = array();
			foreach( $deliveries as $delivery ) {
				if( !empty( $delivery['contract_number'] ) ) 	$contractNumbers[] 		= $delivery['contract_number'];
				if( !empty( $delivery['product_code'] ) ) 		$productCodes[] 		= $delivery['product_code'];
				if( !empty( $delivery['dc'] ) ) 				$dcs[] 					= $delivery['dc'];
				if( !empty( $delivery['advertisement_week'] ) ) $advertisementWeeks[] 	= $delivery['advertisement_week'];
			}
			$dcs 									= array_unique( $dcs );
			$row['contracts_contract_number'] 		= implode( ', ', array_unique( $contractNumbers ) );
			$row['deliveries_product_code'] 		= implode( ', ', array_unique( $productCodes ) );
			$row['deliveries_dcs'] 					= implode( ', ', array_unique( $dcs ) );
			$row['deliveries_advertisement_weeks'] 	= implode( ', ', array_unique( $advertisementWeeks ) );
			$row['shipments_dc'] 					= $this->getShipmentDCById( $row['shipments_dc'] );

			$row['information'] = ( count( $dcs ) > 1 ) ? 'transfer' : '';
			if( ( $row['shipments_dc'] != 'RGY' && $row['shipments_dc'] != 'JKT' ) && ( in_array( 'RGY', $dcs ) || in_array( 'JKT', $dcs ) ) ) {
				$row['information'] = 'transfer RGY/JKT';
			}

			$res[] = $row;
		}
		return $res;
	}

	/**
	 * getKPIRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getKPIRecords( $recordListSearchParams, $overviewFields ) {

		$tcaNode 				= $this->tca->getColumnNodeById( 'contracts_incoterm_code' );
		$incotermCodeOptions 	= $this->tca->getMultiselectionOptions( $tcaNode );
		$tcaNode 				= $this->tca->getColumnNodeById( 'shipments_loading_type' );
		$loadingTypeOptions 	= $this->tca->getMultiselectionOptions( $tcaNode );

		$res = array();
		$this->db->select(
			$this->getAllFieldsFromTable( 'deliveries' ).', '.
			$this->getAllFieldsFromTable( 'contracts' ).', '.
			$this->getAllFieldsFromTable( 'shipments' ).', '.
			$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
			$this->getAllFieldsFromTable( 'pcshipments' )
		);
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0', 'left');
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.delivery_id = deliveries.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0');
		$this->db->join( 'pcshipments', 'pcshipments.id = pcshipments_deliveries_mm.pcshipment_id AND pcshipments.hidden = 0 AND pcshipments.deleted = 0');
		$this->setWhereClauseForSearchparams( $recordListSearchParams );

		$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
		$this->db->group_by( 'shipments_deliveries_mm.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$shipmentsId = $row['shipments_id'];

			$res[$shipmentsId]['deliveries_product_code'][] 	= $row['deliveries_product_code'];
			$res[$shipmentsId]['contracts_supplier'][] 			= $this->getSupplierName( $row['contracts_supplier'] );
			$res[$shipmentsId]['contracts_contract_number'][] 	= $row['contracts_contract_number'];
			$res[$shipmentsId]['contracts_traffic_type']		= $this->getTrafficType( $row['contracts_traffic_type'] );
			$res[$shipmentsId]['contracts_contract_type'][]		= $this->getContractType( $row['contracts_contract_type'] );
			$res[$shipmentsId]['deliveries_gross_weight'][]		= $row['shipments_deliveries_mm_gross_weight'];
			$res[$shipmentsId]['deliveries_volume'][]			= $row['shipments_deliveries_mm_volume'];
			$res[$shipmentsId]['shipments_dc'] 					= $this->getShipmentDCById( $row['shipments_dc'] );
			$res[$shipmentsId]['shipments_container_size'] 		= $this->getContainerSize( $row['shipments_container_size'] );
			$res[$shipmentsId]['shipments_destination_port']	= $this->getDestinationPort( $row['shipments_destination_port'] );
			$res[$shipmentsId]['pcshipments_ietd']				= $this->getFormattedDate( $row['pcshipments_ietd'] );
			$res[$shipmentsId]['shipments_etd']					= $this->getFormattedDate( $row['shipments_etd'] );
			$res[$shipmentsId]['shipments_atd'] 				= $this->getFormattedDate( $row['shipments_atd'] );
			$res[$shipmentsId]['pcshipments_ieta']				= $this->getFormattedDate( $row['pcshipments_ieta'] );
			$res[$shipmentsId]['shipments_eta']					= $this->getFormattedDate( $row['shipments_eta'] );
			$res[$shipmentsId]['shipments_ata']					= $this->getFormattedDate( $row['shipments_ata'] );
			$res[$shipmentsId]['shipments_edor']				= $this->getFormattedDate( $row['shipments_edor'] );
			$res[$shipmentsId]['shipments_container_number'] 	= $row['shipments_container_number'];
			//$res[$shipmentsId]['shipments_container_type'] 		= $this->getContainerType( $row['shipments_container_type'] );

			$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_container_type' );
			$containerTypeOptions 			= $this->tca->getMultiselectionOptions( $tcaNode );
			$res[$shipmentsId]['shipments_container_type'] 		= $containerTypeOptions[$row['shipments_container_type']];

			$res[$shipmentsId]['shipments_departure_port'] 		= $row['shipments_departure_port'];
			$res[$shipmentsId]['shipments_mother_vessel_name'] 	= $row['shipments_mother_vessel_name'];
			$res[$shipmentsId]['shipments_carrier'] 			= $row['shipments_carrier'];
			$res[$shipmentsId]['contracts_on_sale_date']		= $this->getFormattedDate( $row['contracts_on_sale_date'] );
			$res[$shipmentsId]['shipments_incoterm_code'] 		= $incotermCodeOptions[$row['shipments_incoterm_code']];
			$res[$shipmentsId]['shipments_loading_type'] 		= $loadingTypeOptions[$row['shipments_loading_type']];


			//			// related deliveries
//			$deliveries 		= $this->shipment_delivery_model->getDeliveriesByShipmentId( $row['shipments_id'] );
//			$shipmentDeliveries = $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $row['shipments_id'], 'hidden' => 0, 'deleted' => 0 ) );
//			$productCodes 		= array();
//			$contractNumbers 	= array();
//			$suppliers			= array();
//			$contracts 			= array();
//			$grossWeight 		= 0;
//			$volume 			= 0;
//			$contractTypes 		= array();
//			$deliveryDueDates 	= array();
//
//			foreach( $deliveries as $delivery ) {
//				$contracts[] = $this->contract_model->getRecord( $delivery['contract_id'] );
//				if( !empty( $delivery['product_code'] ) ) 	$productCodes[] 	= $delivery['product_code'];
//			}
//
//			foreach( $shipmentDeliveries as $shipmentDelivery ) {
//				if( !empty( $shipmentDelivery['gross_weight'] ) ) 	$grossWeight 		+= $shipmentDelivery['gross_weight'];
//				if( !empty( $shipmentDelivery['volume'] ) ) 		$volume 			+= $shipmentDelivery['volume'];
//			}
//
//			foreach( $contracts as $contract ) {
//				if( !empty( $contract['supplier'] ) ) 						$suppliers[] 		= $this->getSupplierName( $contract['supplier'] );
//				if( !empty( $contract['contract_number'] ) ) 				$contractNumbers[] 	= $contract['contract_number'];
//				if( !empty( $contract['contract_type'] ) ) 					$contractTypes[] 	= $this->getContractType( $contract['contract_type'] );
//				if( !empty( $contract['aldi_au_wh_delivery_due_date'] ) ) 	$deliveryDueDates[] = $this->getFormattedDate( $contract['aldi_au_wh_delivery_due_date'] );
//			}
//
//
//			$row['deliveries_product_code'] 	= implode( ', ', array_unique( $productCodes ) );
//			$row['contracts_supplier'] 			= implode( ', ', array_unique( $suppliers ) );
//			$row['contracts_contract_number']	= implode( ', ', array_unique( $contractNumbers ) );
//			$row['deliveries_gross_weight']		= sprintf($this->config->item('floatvalues_format'), $grossWeight );
//			$row['deliveries_volume']			= sprintf($this->config->item('doublevalues_format'), $volume );
//			$row['contracts_contract_type']		= implode( ', ', array_unique( $contractTypes ) );
//			$row['contracts_delivery_due_date']	= implode( ', ', array_unique( $deliveryDueDates ) );
//			$row['shipments_dc'] 				= $this->getShipmentDCById( $row['shipments_dc'] );
//			$row['shipments_container_size'] 	= $this->getContainerSize( $row['shipments_container_size'] );
//			$row['shipments_destination_port'] 	= $this->getDestinationPort( $row['shipments_destination_port'] );
//			$row['shipments_etd'] 				= $this->getFormattedDate( $row['shipments_etd'] );
//			$row['shipments_atd'] 				= $this->getFormattedDate( $row['shipments_atd'] );
//			$row['shipments_eta'] 				= $this->getFormattedDate( $row['shipments_eta'] );
//			$row['shipments_ata'] 				= $this->getFormattedDate( $row['shipments_ata'] );
//			$row['shipments_edor'] 				= $this->getFormattedDate( $row['shipments_edor'] );
//
//			$res[] = $row;
		}
		return $res;
	}

	/**
	 * getCOORecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getCOORecords( $recordListSearchParams, $overviewFields ) {

		$this->load->model('Delivery_model', 'delivery_model');

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'pcshipments' ) );
		$this->db->from( 'contracts, pcshipments, deliveries' );
		$this->db->where( 'pcshipments.po_number LIKE CONCAT("%", contracts.contract_number, "%")' );
		$this->db->where( 'deliveries.contract_id = contracts.id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
		$this->db->where( array( 'contracts.hidden' => 0, 'contracts.deleted' => 0, 'contracts.contract_number !=' => '' ) );
		$this->db->where( array( 'pcshipments.shipment_id !=' => 0, 'pcshipments.hidden' => 0, 'pcshipments.deleted' => 0 ) );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->group_by( 'contracts.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			if( $row['contracts_contract_type'] ==  Contract_model::$CONTRACT_TYPE_CORE_RANGE || $row['contracts_contract_type'] ==  Contract_model::$CONTRACT_TYPE_STORE_EQUIPMENT ) {
				$deliveries = $this->delivery_model->getRecordsWhere( array( 'contract_id' => $row['contracts_id'], 'hidden' => 0, 'deleted' => 0 ) );
				$productCodes = array();
				$productDescriptions = array();
				foreach( $deliveries as $delivery ) {
					$productCodes[] = $delivery['product_code'];
					$productDescriptions[] = $delivery['product_description'];
				}
				$row['contracts_product_code'] = implode( ',', $productCodes );
				$row['contracts_product_description'] = implode( ',', $productDescriptions );
			}
			$row['contracts_supplier'] 							= $this->getSupplierName( $row['contracts_supplier'] );
			$row['pcshipments_certificate_of_origin_received'] 	= ( $row['pcshipments_certificate_of_origin_received'] > 0 ) ? 'YES' : 'NO';
			$row['pcshipments_duty_refund_completed'] 			= ( $row['pcshipments_duty_refund_completed'] > 0 ) ? 'YES' : 'NO';
			$res[] = $row;
		}
		return $res;
	}

	/**
	 * getCLPRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getCLPRecords( $recordListSearchParams, $overviewFields, $unconfirmed = FALSE ) {
		$this->load->model('Delivery_model', 'delivery_model');

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );

		$loo = $this->session->userdata('logwin_origin_office');
		if( !empty( $loo ) ) $this->db->where( array( 'contracts.logwin_origin_office' => $loo ) );

		if( !$unconfirmed ) {
			$this->db->where( array( 'shipments.approval' => 1 ) );
		} else {
			$this->db->where( array( 'shipments.approval !=' => 1 ) );
		}
		$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
		$this->db->group_by( 'shipments.id, ' );
		$this->db->order_by( 'shipments.destination_port' );
		$query = $this->db->get();

		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$destinationPortOptions 		= $this->tca->getMultiselectionOptions( $tcaNode );

		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_container_type' );
		$containerTypeOptions 			= $this->tca->getMultiselectionOptions( $tcaNode );

		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_container_size' );
		$containerSizeOptions 			= $this->tca->getMultiselectionOptions( $tcaNode );

		foreach ( $query->result_array() as $row ) {
			$s = array();
			$s['destination_port'] 		= $destinationPortOptions[$row['shipments_destination_port']];
			$s['container_size'] 		= $containerSizeOptions[$row['shipments_container_size']];
			$s['container_type'] 		= $containerTypeOptions[$row['shipments_container_type']];
			$s['deliveries']			= array();

			$caseQuantityTotal 			= 0;
			$volumeTotal 				= 0;
			$grossWeightTotal 			= 0;

			//$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId( $row['shipments_id'] );
			$deliveries = $this->getDeliveriesByShipmentId( $row['shipments_id'] );

			foreach( $deliveries as $delivery ) {
				$d = array();
				$d['contract_number'] 	= $delivery['deliveries_contract_number'];
				$d['product_code'] 		= $delivery['deliveries_product_code'];
				$d['dc'] 				= $delivery['deliveries_dc'];
				$d['case_quantity'] 	= $delivery['deliveries_case_quantity'];
				$d['volume'] 			= sprintf($this->config->item('doublevalues_format'), $delivery['deliveries_volume']);
				$d['gross_weight'] 		= $delivery['deliveries_gross_weight'];
				$s['deliveries'][]		= $d;

				$caseQuantityTotal 		+= $delivery['deliveries_case_quantity'];
				$volumeTotal 			+= $delivery['deliveries_volume'];
				$grossWeightTotal 		+= $delivery['deliveries_gross_weight'];
			}

			$s['case_quantity_total'] 	= $caseQuantityTotal;
			$s['volume_total'] 			= sprintf($this->config->item('doublevalues_format'), $volumeTotal);
			$s['gross_weight_total'] 	= $grossWeightTotal;

			$res[$row['shipments_destination_port']][] = $s;
		}
		return $res;
	}

	/**
	 * getCDARecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getCDARecords( $recordListSearchParams, $overviewFields, $unconfirmed = FALSE  ) {
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->model('Shipment_model', 'shipment_model');

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
		$this->db->join( 'contracts', 'contracts.id = deliveries.contract_id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );

//		$loo = $this->session->userdata('logwin_origin_office');
//		if( !empty( $loo ) ) $this->db->where( array( 'contracts.logwin_origin_office' => $loo ) );

		if( !$unconfirmed ) {
			$this->db->where( array( 'shipments.approval' => 1 ) );
		} else {
			$this->db->where( array( 'shipments.approval !=' => 1 ) );
		}
		$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
		$this->db->group_by( 'shipments.id' );
		$query = $this->db->get();

		$tmp['total_cartons'] = 0;
		foreach ( $query->result_array() as $row ) {
			$tmp 				= $row;

			// deliveries
			$deliveries			= $this->getDeliveriesForCDAReport( $row['shipments_id'] );
			$tmp['deliveries'] 	= $deliveries;

			// total number of cartons
			$totalCartons = 0;
			foreach( $deliveries as $delivery ) {
				$totalCartons += $delivery['shipments_deliveries_mm_case_quantity'];
			}
			$tmp['total_cartons'] = $totalCartons;


			// contract numbers
			$contracts = array();
			foreach( $deliveries as $delivery ) {
				$contracts[] = $delivery['contracts_contract_number'];
			}
			$tmp['contracts_contract_number'] = implode( '/', array_unique( $contracts ) );

			// sum gross weight
			$sumGrossWeight = 0;
			foreach( $deliveries as $delivery ) {
				$sumGrossWeight += $delivery['shipments_deliveries_mm_gross_weight'];
			}
			$tmp['deliveries_sum_gross_weight'] = $sumGrossWeight;

			// region
			$this->load->model('Dc_model', 'dc_model');
			$dc					= ( isset( $tmp['shipments_dc'] ) ) ? $this->dc_model->getRecord( $tmp['shipments_dc'] ) : array('dc' => '');
			$tmp['shipments_region']		= $dc['dc'];

			// dc address
			$tmp['shipments_dc_address'] = $this->getDCAddress( $tmp['shipments_dc'] );
			$tmp['shipments_dc'] = $this->getDCAddress( $tmp['shipments_dc'] );

			// check if all hawb are telex received
			$tmp['telex_received'] = $this->shipment_model->isTelexReceived( $row['shipments_id'] );

			// container size and weight
			$tmp['shipments_container_size_weight'] = '';

			$res[] = $tmp;
		}

		return $this->prepareResultQueryForOverviewOutput( $res );
	}


	/**
	 * getOrderRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getOrderRecords( $recordListSearchParams, $overviewFields ) {

		$fromDate 		= array_key_exists('contracts_eta_from', $recordListSearchParams) ? $recordListSearchParams['contracts_eta_from'][0] : '';
		$toDate 		= array_key_exists('contracts_eta_to', $recordListSearchParams) ? $recordListSearchParams['contracts_eta_to'][0] : '';
		$contractType 	= array_key_exists('contracts_contract_type', $recordListSearchParams) ? $recordListSearchParams['contracts_contract_type'][0] : 0;
		$supplier 		= array_key_exists('contracts_supplier', $recordListSearchParams) ? $recordListSearchParams['contracts_supplier'][0] : 0;

		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->model('Shipment_model', 'shipment_model');

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'contracts' );
		$this->db->join( 'deliveries', 'deliveries.contract_id = contracts.id AND deliveries.hidden = 0 AND deliveries.deleted = 0');
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
		$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
		if(!empty($fromDate)) {
			$fromDate = str_replace( '/', '.', $fromDate );
			$comDate = date("Y-m-d H:i:s", strtotime($fromDate.' 00:00:00') );
			$where = array( 'deliveries.eta >=' => $comDate );
			$this->db->where( $where );
		}
		if(!empty($toDate)) {
			$toDate = str_replace( '/', '.', $toDate );
			$comDate = date("Y-m-d H:i:s", strtotime($toDate.' 23:59:59') );
			$where = array( 'deliveries.eta <=' => $comDate );
			$this->db->where( $where );
		}
		$this->db->where( array( 'contracts.hidden' => 0, 'contracts.deleted' => 0, 'contracts.contract_type' => $contractType, 'contracts.supplier' => $supplier ) );
		$this->db->group_by( 'deliveries.id' );
		$query = $this->db->get();

		foreach( $query->result_array() as $row ) {
			// PRE, MIN, STP, DER, DAN, BRE, JKT, RGY
			$dc = $row['deliveries_dc'];
			$res[$dc][] = $row;
		}
		return $res;
	}

	/**
	 * getDimensionRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getDimensionRecords( $recordListSearchParams, $overviewFields ) {

		$fromDate 		= array_key_exists('crdate_from', $recordListSearchParams) ? $recordListSearchParams['crdate_from'][0] : '';
		$toDate 		= array_key_exists('crdate_to', $recordListSearchParams) ? $recordListSearchParams['crdate_to'][0] : '';
		$dimensionType 	= array_key_exists('dimension_type', $recordListSearchParams) ? $recordListSearchParams['dimension_type'][0] : 0;

		$this->load->model('Activity_model', 'activity_model');

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'activities' ) );
		$this->db->from( 'activities' );
		if(!empty($fromDate)) {
			$fromDate = str_replace( '/', '.', $fromDate );
			$comDate = date("Y-m-d H:i:s", strtotime($fromDate.' 00:00:00') );
			$where = array( 'activities.crdate >=' => $comDate );
			$this->db->where( $where );
		}
		if(!empty($toDate)) {
			$toDate = str_replace( '/', '.', $toDate );
			$comDate = date("Y-m-d H:i:s", strtotime($toDate.' 23:59:59') );
			$where = array( 'activities.crdate <=' => $comDate );
			$this->db->where( $where );
		}
		if(empty($dimensionType)) {
			$where = '(activities.record_field ="case_length" or activities.record_field="case_width" or activities.record_field="case_height")';
			$this->db->where( $where );
		} else {
			$where = array( 'activities.record_field' => $dimensionType );
			$this->db->where( $where );
		}
		$this->db->where( array( 'activities.hidden' => 0, 'activities.deleted' => 0 ) );
		$query = $this->db->get();

		foreach( $query->result_array() as $row ) {
			$row['activities_action_type'] = 'Contract changed';
			$res[] = $row;
		}
		return $res;
	}

	/**
	 * getStatusRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getStatusRecords( $recordListSearchParams, $overviewFields ) {

		$ataFrom 	= array_key_exists('shipments_ata_from', $recordListSearchParams) ? $recordListSearchParams['shipments_ata_from'][0] : '';
		$ataTo 		= array_key_exists('shipments_ata_to', $recordListSearchParams) ? $recordListSearchParams['shipments_ata_to'][0] : '';
		$crdateFrom = array_key_exists('shipments_crdate_from', $recordListSearchParams) ? $recordListSearchParams['shipments_crdate_from'][0] : '';
		$deliveryDueFrom = array_key_exists('contracts_aldi_au_wh_delivery_due_date_from', $recordListSearchParams) ? $recordListSearchParams['contracts_aldi_au_wh_delivery_due_date_from'][0] : '';
		$shipmentId = array_key_exists('shipments_id', $recordListSearchParams) ? $recordListSearchParams['shipments_id'][0] : '';
		$emptyQuantity 	= array_key_exists('deliveries_empty_case_quantity', $recordListSearchParams) ? $recordListSearchParams['deliveries_empty_case_quantity'][0] : false;

		$res = array();
		$this->db->select(
			$this->getAllFieldsFromTable( 'deliveries' ).', '.
			$this->getAllFieldsFromTable( 'contracts' ).', '.
			$this->getAllFieldsFromTable( 'shipments' ).', '.
			$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
			$this->getAllFieldsFromTable( 'pcshipments' )
		);
		$this->db->from( 'deliveries' );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0');
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
		$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
		$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.delivery_id = deliveries.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0', 'left' );
		$this->db->join( 'pcshipments', 'pcshipments.id = pcshipments_deliveries_mm.pcshipment_id AND pcshipments.hidden = 0 AND pcshipments.deleted = 0', 'left' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		if(!empty($ataFrom)) {
			$where 		= array( 'shipments.ata >=' => $ataFrom );
			$this->db->where( $where );
		}
		if(!empty($ataTo)) {
			$where 		= array( 'shipments.ata <=' => $ataTo );
			$this->db->where( $where );
		}
		if(!empty($crdateFrom)) {
			$where 		= array( 'shipments.crdate >=' => $crdateFrom );
			$this->db->where( $where );
		}
		if(!empty($deliveryDueFrom)) {
			$where 		= array( 'contracts.aldi_au_wh_delivery_due_date >=' => $deliveryDueFrom );
			$this->db->where( $where );
		}
		if( is_null( $shipmentId ) ) {
			$where 		= array( 'shipments.id' => NULL );
			$this->db->where( $where );
		}
		if( $emptyQuantity ) {
			$where 		= array( 'deliveries.case_quantity >' => '0' );
			$this->db->where( $where );
		}
		$this->db->where( array( 'deliveries.hidden' => 0, 'deliveries.deleted' => 0 ) );
		$this->db->group_by( 'deliveries.id' );
		$this->db->order_by( 'contracts.id' );
		$query = $this->db->get();

		foreach( $query->result_array() as $row ) {

			$row['contracts_ris_date'] = '';
			if(
				!empty($row['contracts_aldi_au_wh_delivery_due_date']) &&
				$row['contracts_aldi_au_wh_delivery_due_date'] != '0000-00-00 00:00:00'
			) {
				$row['contracts_ris_date'] = date( $this->config->item('date_format'), strtotime( '+1 days', strtotime( $row['contracts_aldi_au_wh_delivery_due_date'] ) ) );
			}

			$row['contracts_cfso_opening_date'] = '';
			if(
				!empty($row['contracts_actual_contract_ready_date']) &&
				$row['contracts_actual_contract_ready_date'] != '0000-00-00 00:00:00'
			) {
				$row['contracts_cfso_opening_date'] = date( $this->config->item('date_format'), strtotime( '+7 days', strtotime( $row['contracts_actual_contract_ready_date'] ) ) );
			}

			$row['contracts_supplier_kpi'] = '';
			if(
				!empty($row['contracts_actual_contract_ready_date']) &&
				$row['contracts_actual_contract_ready_date'] != '0000-00-00 00:00:00' &&
				!empty($row['contracts_fob_date']) &&
				$row['contracts_fob_date'] != '0000-00-00 00:00:00'
			) {
				$readyTime 	= strtotime( $row['contracts_actual_contract_ready_date'] );
				$fobTime 	= strtotime( $row['contracts_fob_date'] );
				$diff 		= round( ( $readyTime - $fobTime ) / 86400 );
				$row['contracts_supplier_kpi'] = $diff;
			}

			$row['shipments_delivery_date'] = '';
			if(
				!empty($row['shipments_eta']) &&
				$row['shipments_eta'] != '0000-00-00 00:00:00'
			) {
				$row['shipments_delivery_date'] = date( $this->config->item('date_format'), strtotime( '+10 days', strtotime( $row['shipments_eta'] ) ) );
			}

			$row['shipments_early_late'] = '';
			if(
				!empty( $row['shipments_delivery_date'] ) &&
				!empty( $row['contracts_ris_date'] )
			) {
				$deliveryDate 	= str_replace( '/', '.', $row['shipments_delivery_date'] );
				$risDate 		= str_replace( '/', '.', $row['contracts_ris_date'] );

				$deliveryTime 	= strtotime( $deliveryDate );
				$risTime 		= strtotime( $risDate );
				$diff 			= round( ( $deliveryTime - $risTime ) / 86400 );
				$row['shipments_early_late'] = $diff;
			}

			$delayReasons = array();
			$reason = $this->getContractDelayReason( $row['contracts_delay_reason'] );
			if( !empty($reason) )$delayReasons[] = $reason;
			$reason = $this->getShipmentDelayReason( $row['shipments_delay_reason'] );
			if( !empty($reason) )$delayReasons[] = $reason;
			$row['shipments_remarks'] = implode( ', ', $delayReasons );

			$res[] = $row;
		}

		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * getCRDvsATDRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getCRDvsATDRecords( $recordListSearchParams, $overviewFields ) {

		$ataFrom 	= array_key_exists('shipments_ata_from', $recordListSearchParams) ? $recordListSearchParams['shipments_ata_from'][0] : '';
		$ataTo 		= array_key_exists('shipments_ata_to', $recordListSearchParams) ? $recordListSearchParams['shipments_ata_to'][0] : '';
		$crdateFrom = array_key_exists('shipments_crdate_from', $recordListSearchParams) ? $recordListSearchParams['shipments_crdate_from'][0] : '';
		$deliveryDueFrom = array_key_exists('contracts_aldi_au_wh_delivery_due_date_from', $recordListSearchParams) ? $recordListSearchParams['contracts_aldi_au_wh_delivery_due_date_from'][0] : '';
		$shipmentId = array_key_exists('shipments_id', $recordListSearchParams) ? $recordListSearchParams['shipments_id'][0] : '';
		$emptyQuantity 	= array_key_exists('deliveries_empty_case_quantity', $recordListSearchParams) ? $recordListSearchParams['deliveries_empty_case_quantity'][0] : false;

		$res = array();
		$this->db->select(
			$this->getAllFieldsFromTable( 'deliveries' ).', '.
			$this->getAllFieldsFromTable( 'contracts' ).', '.
			$this->getAllFieldsFromTable( 'shipments' ).', '.
			$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
			$this->getAllFieldsFromTable( 'pcshipments' )
		);
		$this->db->from( 'shipments_deliveries_mm' );
		$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
		$this->db->join( 'deliveries', 'shipments_deliveries_mm.delivery_id = deliveries.id AND deliveries.hidden = 0 AND deliveries.deleted = 0', 'left' );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0', 'left');
		$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.delivery_id = deliveries.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0', 'left' );
		$this->db->join( 'pcshipments', 'pcshipments.id = pcshipments_deliveries_mm.pcshipment_id AND pcshipments.hidden = 0 AND pcshipments.deleted = 0', 'left' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		if(!empty($ataFrom)) {
			$where 		= array( 'shipments.ata >=' => $ataFrom );
			$this->db->where( $where );
		}
		if(!empty($ataTo)) {
			$where 		= array( 'shipments.ata <=' => $ataTo );
			$this->db->where( $where );
		}
		if(!empty($crdateFrom)) {
			$where 		= array( 'shipments.crdate >=' => $crdateFrom );
			$this->db->where( $where );
		}
		if(!empty($deliveryDueFrom)) {
			$where 		= array( 'contracts.aldi_au_wh_delivery_due_date >=' => $deliveryDueFrom );
			$this->db->where( $where );
		}
		if( is_null( $shipmentId ) ) {
			$where 		= array( 'shipments.id' => NULL );
			$this->db->where( $where );
		}
		if( $emptyQuantity ) {
			$where 		= array( 'deliveries.case_quantity >' => '0' );
			$this->db->where( $where );
		}
		$this->db->where( array( 'shipments_deliveries_mm.hidden' => 0, 'shipments_deliveries_mm.deleted' => 0 ) );
		$this->db->group_by( 'shipments_deliveries_mm.id' );
		$this->db->order_by( 'contracts.id' );
		$query = $this->db->get();

		foreach( $query->result_array() as $row ) {

			$row['contracts_ris_date'] = '';
			if(
				!empty($row['contracts_aldi_au_wh_delivery_due_date']) &&
				$row['contracts_aldi_au_wh_delivery_due_date'] != '0000-00-00 00:00:00'
			) {
				$row['contracts_ris_date'] = date( $this->config->item('date_format'), strtotime( '+1 days', strtotime( $row['contracts_aldi_au_wh_delivery_due_date'] ) ) );
			}

			$row['contracts_cfso_opening_date'] = '';
			if(
				!empty($row['contracts_actual_contract_ready_date']) &&
				$row['contracts_actual_contract_ready_date'] != '0000-00-00 00:00:00'
			) {
				$row['contracts_cfso_opening_date'] = date( $this->config->item('date_format'), strtotime( '+7 days', strtotime( $row['contracts_actual_contract_ready_date'] ) ) );
			}

			$row['contracts_supplier_kpi'] = '';
			if(
				!empty($row['contracts_actual_contract_ready_date']) &&
				$row['contracts_actual_contract_ready_date'] != '0000-00-00 00:00:00' &&
				!empty($row['contracts_fob_date']) &&
				$row['contracts_fob_date'] != '0000-00-00 00:00:00'
			) {
				$readyTime 	= strtotime( $row['contracts_actual_contract_ready_date'] );
				$fobTime 	= strtotime( $row['contracts_fob_date'] );
				$diff 		= round( ( $readyTime - $fobTime ) / 86400 );
				$row['contracts_supplier_kpi'] = $diff;
			}

			$row['shipments_delivery_date'] = '';
			if(
				!empty($row['shipments_eta']) &&
				$row['shipments_eta'] != '0000-00-00 00:00:00'
			) {
				$row['shipments_delivery_date'] = date( $this->config->item('date_format'), strtotime( '+10 days', strtotime( $row['shipments_eta'] ) ) );
			}

			$row['shipments_early_late'] = '';
			if(
				!empty( $row['shipments_delivery_date'] ) &&
				!empty( $row['contracts_ris_date'] )
			) {
				$deliveryDate 	= str_replace( '/', '.', $row['shipments_delivery_date'] );
				$risDate 		= str_replace( '/', '.', $row['contracts_ris_date'] );

				$deliveryTime 	= strtotime( $deliveryDate );
				$risTime 		= strtotime( $risDate );
				$diff 			= round( ( $deliveryTime - $risTime ) / 86400 );
				$row['shipments_early_late'] = $diff;
			}

			$delayReasons = array();
			$reason = $this->getContractDelayReason( $row['contracts_delay_reason'] );
			if( !empty($reason) )$delayReasons[] = $reason;
			$reason = $this->getShipmentDelayReason( $row['shipments_delay_reason'] );
			if( !empty($reason) )$delayReasons[] = $reason;
			$row['shipments_remarks'] = implode( ', ', $delayReasons );

			$res[] = $row;
		}

		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * getOrderRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getSupplierContractReadyRecords( $recordListSearchParams, $overviewFields ) {

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'contracts' ) );
		$this->db->from( 'contracts' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( 'contracts.hidden' => 0, 'contracts.deleted' => 0 ) );
		$this->db->group_by( 'contracts.id' );
		$query = $this->db->get();

		foreach( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}


	/**
	 * getOrderRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getContractDepartureRecords( $recordListSearchParams, $overviewFields ) {

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
		$this->db->join( 'contracts', 'contracts.id = deliveries.contract_id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
		$this->db->group_by( 'shipments.id' );
		$query = $this->db->get();

		foreach( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * getContractOSWRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getContractOSWRecords( $recordListSearchParams, $overviewFields ) {

		$res = array();
		$this->db->select(
			$this->getAllFieldsFromTable( 'deliveries' ).', '.
			$this->getAllFieldsFromTable( 'contracts' )
		);
		$this->db->from( 'deliveries' );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0');
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( 'deliveries.hidden' => 0, 'deliveries.deleted' => 0 ) );
		$this->db->group_by( 'deliveries.id' );
		$query = $this->db->get();
		$contracts = array();
		$suppliers = array();
		foreach( $query->result_array() as $row ) {
			$contracts[] = $row['contracts_contract_number'];
			$suppliers[] = $row['contracts_supplier'];
			$row['volume'] = $row['deliveries_case_quantity'] * $row['contracts_case_cubage'];
			$res[] = $row;
		}
		$res[0]['total_contracts'] = count( array_unique( $contracts ) );
		$res[0]['total_suppliers'] = count( array_unique( $suppliers ) );
		return $this->prepareResultQueryForOverviewOutput( $res );
	}


	/**
	 * getTelex1Records
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getTelex1Records( $recordListSearchParams, $overviewFields ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Supplier_model', 'supplier_model');

		$fromDate 	= array_key_exists('telex_from', $recordListSearchParams) ? $recordListSearchParams['telex_from'][0] : '';
		$toDate 	= array_key_exists('telex_to', $recordListSearchParams) ? $recordListSearchParams['telex_to'][0] : '';

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
		$this->db->join( 'contracts', 'contracts.id = deliveries.contract_id AND contracts.hidden = 0 AND contracts.deleted = 0' );

		if(!empty($fromDate)) {
			$fromDate = str_replace( '/', '.', $fromDate );
			$comDate = date("Y-m-d H:i:s", strtotime($fromDate.' 00:00:00') );
			$where = array( 'shipments.eta >=' => $comDate );
			$this->db->where( $where );
		}
		if(!empty($toDate)) {
			$toDate = str_replace( '/', '.', $toDate );
			$comDate = date("Y-m-d H:i:s", strtotime($toDate.' 23:59:59') );
			$where = array( 'shipments.eta <=' => $comDate );
			$this->db->where( $where );
		}
		$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0, 'shipments_deliveries_mm.telex_second_reminder_sent !=' => '0000-00-00 00:00:00' ) );
		$this->db->group_by( 'shipments.id' );
		$this->db->order_by( 'contracts.supplier' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$supplier 				= $this->supplier_model->getRecord( $row['contracts_supplier'] );
			$tmp 					= array();
			$tmp['supplier'] 		= isset( $supplier['name'] ) ? $supplier['name'] : '';
			$tmp['contract_number'] = $row['contracts_contract_number'];
			$tmp['hbl_hawb'] 		= $row['shipments_deliveries_mm_hbl_hawb'];
			$tmp['crdate'] 			= $row['shipments_deliveries_mm_telex_second_reminder_sent'];
			$res[] 					= $tmp;
		}
		return $res;

	}

	/**
	 * getTelex2Records
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getTelex2Records( $recordListSearchParams, $overviewFields ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Supplier_model', 'supplier_model');

		$fromDate 	= array_key_exists('telex_from', $recordListSearchParams) ? $recordListSearchParams['telex_from'][0] : '';
		$toDate 	= array_key_exists('telex_to', $recordListSearchParams) ? $recordListSearchParams['telex_to'][0] : '';

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
		$this->db->join( 'contracts', 'contracts.id = deliveries.contract_id AND contracts.hidden = 0 AND contracts.deleted = 0' );

		if(!empty($fromDate)) {
			$fromDate = str_replace( '/', '.', $fromDate );
			$comDate = date("Y-m-d H:i:s", strtotime($fromDate.' 00:00:00') );
			$where = array( 'shipments.eta >=' => $comDate );
			$this->db->where( $where );
		}
		if(!empty($toDate)) {
			$toDate = str_replace( '/', '.', $toDate );
			$comDate = date("Y-m-d H:i:s", strtotime($toDate.' 23:59:59') );
			$where = array( 'shipments.eta <=' => $comDate );
			$this->db->where( $where );
		}
		$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
		$this->db->where( 'shipments.eta < shipments_deliveries_mm.telex_set_date' );
		$this->db->group_by( 'shipments.id' );
		$this->db->order_by( 'contracts.supplier' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$supplier 				= $this->supplier_model->getRecord( $row['contracts_supplier'] );
			$tmp 					= array();
			$tmp['supplier'] 		= isset( $supplier['name'] ) ? $supplier['name'] : '';
			$tmp['contract_number'] = $row['contracts_contract_number'];
			$tmp['hbl_hawb'] 		= $row['shipments_deliveries_mm_hbl_hawb'];
			$tmp['eta'] 			= $row['shipments_eta'];
			$tmp['crdate'] 			= $row['shipments_deliveries_mm_telex_set_date'];
			$res[] 					= $tmp;
		}
		return $res;

	}

	/**
	 * getFFRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getFFRecords( $recordListSearchParams, $overviewFields ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Supplier_model', 'supplier_model');

		$fromDate 	= array_key_exists('ff_from', $recordListSearchParams) ? $recordListSearchParams['ff_from'][0] : '';
		$toDate 	= array_key_exists('ff_to', $recordListSearchParams) ? $recordListSearchParams['ff_to'][0] : '';

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
		$this->db->join( 'contracts', 'contracts.id = deliveries.contract_id AND contracts.hidden = 0 AND contracts.deleted = 0' );

		if(!empty($fromDate)) {
			$fromDate = str_replace( '/', '.', $fromDate );
			$comDate = date("Y-m-d H:i:s", strtotime($fromDate.' 00:00:00') );
			$where = array( 'shipments.eta >=' => $comDate );
			$this->db->where( $where );
		}
		if(!empty($toDate)) {
			$toDate = str_replace( '/', '.', $toDate );
			$comDate = date("Y-m-d H:i:s", strtotime($toDate.' 23:59:59') );
			$where = array( 'shipments.eta <=' => $comDate );
			$this->db->where( $where );
		}
		$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
		$this->db->group_by( 'shipments.id' );
		$this->db->order_by( 'shipments.eta DESC' );
		$query = $this->db->get();

		foreach( $query->result_array() as $row ) {
			$tmp 					= $row;
			// PRE, MIN, STP, DER, DAN, BRE, JKT, RGY
			$dcRecord				= $this->dc_model->getRecord($row['shipments_dc']);
			$dc 					= $dcRecord['dc'];
			// deliveries
			$deliveries				= $this->getDeliveriesByShipmentId( $row['shipments_id'] );
			$contract_numbers 		= array();
			$suppliers 				= array();
			$product_codes 			= array();
			$product_descriptions 	= array();
			$sumGrossWeight 		= 0;
			$sumCaseQuantity 		= 0;
			$dateAldi				= '';
			foreach( $deliveries as $delivery ) {
				$contract_numbers[] 	= $delivery['contracts_contract_number'];
				$suppliers[] 			= $this->getSupplierName( $delivery['contracts_supplier'] );
				$product_codes[] 		= $delivery['deliveries_product_code'];
				$product_descriptions[] = $delivery['deliveries_product_description'];
				$sumGrossWeight 		+= $delivery['shipments_deliveries_mm_gross_weight'];
				$sumCaseQuantity 		+= $delivery['shipments_deliveries_mm_case_quantity'];
				if( $dc != $delivery['deliveries_dc'] ) $dateAldi = 'Transfer';
				if( $dc != 'JKT' && $dc != 'RGY' ) {
					if( $delivery['deliveries_dc'] == 'JKT' || $delivery['deliveries_dc'] == 'RGY' ) $dateAldi = 'Transfer RGY/JKT';
				}
			}
			$tmp['contracts_sum_contract_number'] 		= implode( ', ', array_unique( $contract_numbers ) );
			$tmp['contracts_sum_supplier'] 				= implode( ', ', array_unique( $suppliers ) );
			$tmp['deliveries_sum_product_code'] 		= implode( ', ', array_unique( $product_codes ) );
			$tmp['deliveries_sum_product_description'] 	= implode( ', ', array_unique( $product_descriptions ) );
			$tmp['deliveries_sum_gross_weight'] 		= $sumGrossWeight;
			$tmp['deliveries_sum_case_quantity'] 		= $sumCaseQuantity;
			$tmp['shipments_yn_edor']					= ( $row['shipments_edor'] != "0000-00-00 00:00:00" && !empty( $row['shipments_edor'] ) ) ? 'Y' : 'N';
			$tmp['shipments_yn_telex_received']			= ( $row['shipments_telex_received'] > 0 ) ? 'Y' : 'N';
			$tmp['shipments_yn_cus']					= ( $row['shipments_cus'] != "0000-00-00 00:00:00" && !empty( $row['shipments_cus'] ) ) ? 'Y' : 'N';
			$tmp['shipments_yn_aqc']					= ( $row['shipments_aqc'] != "0000-00-00 00:00:00" && !empty( $row['shipments_aqc'] ) ) ? 'Y' : 'N';
			$tmp['date_aldi']							= $dateAldi;
			$tmp['region'] 								= $dc;

			$res[$dc][] = $tmp;
		}

		$out = array();
		foreach( $res as $key=>$value ) {
			$out[$key] = $this->prepareResultQueryForOverviewOutput( $value );
		}

		return $out;
	}

	/**
	 * getVesselScheduleRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @param array $overviewFields
	 * @return array - db record as array.
	 */
	public function getVesselScheduleRecords( $recordListSearchParams, $overviewFields ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Supplier_model', 'supplier_model');

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
		$this->db->join( 'contracts', 'contracts.id = deliveries.contract_id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );

		$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
		$this->db->group_by( 'shipments.id' );
		$query = $this->db->get();

		foreach( $query->result_array() as $row ) {
			$tmp 					= $row;
			// PRE, MIN, STP, DER, DAN, BRE, JKT, RGY
			$dcRecord				= $this->dc_model->getRecord($row['shipments_dc']);
			$dc 					= $dcRecord['dc'];
			// deliveries
			$deliveries				= $this->getDeliveriesByShipmentId( $row['shipments_id'] );
			$suppliers 				= array();
			$product_codes 			= array();
			$product_descriptions 	= array();
			$onsale_dates 			= array();
			$sumCaseQuantity 		= 0;
			$sumCaseQuantityOther	= 0;
			$sumCaseQuantityMIN		= 0;
			$sumCaseQuantityDER		= 0;
			$sumCaseQuantitySTP		= 0;
			$sumCaseQuantityPRE		= 0;
			$sumCaseQuantityDAN		= 0;
			$sumCaseQuantityBRE		= 0;
			$sumCaseQuantityRGY		= 0;
			$sumCaseQuantityJKT		= 0;
			$comments 				= array();

			foreach( $deliveries as $delivery ) {
				$suppliers[] 			= $this->getSupplierName( $delivery['contracts_supplier'] );
				$product_codes[] 		= $delivery['deliveries_product_code'];
				$product_descriptions[] = $delivery['deliveries_product_description'];
				$onsale_dates[]			= ( $delivery['contracts_on_sale_date'] != "0000-00-00 00:00:00" && !empty( $delivery['contracts_on_sale_date'] ) ) ? date( $this->config->item('date_format'), strtotime( $delivery['contracts_on_sale_date'] ) ) : '';
				$sumCaseQuantity 		+= $delivery['shipments_deliveries_mm_case_quantity'];
				$sumCaseQuantityOther	+= ( $delivery['deliveries_dc'] != $dc ) ? $delivery['shipments_deliveries_mm_case_quantity'] : 0;
				$sumCaseQuantityMIN		+= ( $delivery['deliveries_dc'] == 'MIN' ) ? $delivery['shipments_deliveries_mm_case_quantity'] : 0;
				$sumCaseQuantityDER		+= ( $delivery['deliveries_dc'] == 'DER' ) ? $delivery['shipments_deliveries_mm_case_quantity'] : 0;
				$sumCaseQuantitySTP		+= ( $delivery['deliveries_dc'] == 'STP' ) ? $delivery['shipments_deliveries_mm_case_quantity'] : 0;
				$sumCaseQuantityPRE		+= ( $delivery['deliveries_dc'] == 'PRE' ) ? $delivery['shipments_deliveries_mm_case_quantity'] : 0;
				$sumCaseQuantityDAN		+= ( $delivery['deliveries_dc'] == 'DAN' ) ? $delivery['shipments_deliveries_mm_case_quantity'] : 0;
				$sumCaseQuantityBRE		+= ( $delivery['deliveries_dc'] == 'BRE' ) ? $delivery['shipments_deliveries_mm_case_quantity'] : 0;
				$sumCaseQuantityRGY		+= ( $delivery['deliveries_dc'] == 'RGY' ) ? $delivery['shipments_deliveries_mm_case_quantity'] : 0;
				$sumCaseQuantityJKT		+= ( $delivery['deliveries_dc'] == 'JKT' ) ? $delivery['shipments_deliveries_mm_case_quantity'] : 0;

				$comment				= $delivery['deliveries_dc'].': '.$delivery['deliveries_product_code'].'/'.$delivery['deliveries_contract_number'].' - '.$delivery['shipments_deliveries_mm_case_quantity'].' cases';
				$comments[]				= $comment;
			}

			$tmp['contracts_sum_supplier'] 				= implode( ', ', array_unique( $suppliers ) );
			$tmp['deliveries_sum_product_code'] 		= implode( ', ', array_unique( $product_codes ) );
			$tmp['deliveries_sum_product_description'] 	= implode( ', ', array_unique( $product_descriptions ) );
			$tmp['contracts_sum_on_sale_date'] 			= implode( ', ', array_unique( $onsale_dates ) );
			$tmp['deliveries_sum_case_quantity'] 		= $sumCaseQuantity;
			$tmp['deliveries_sum_case_quantity_other'] 	= $sumCaseQuantityOther;
			$tmp['deliveries_sum_case_quantity_MIN'] 	= $sumCaseQuantityMIN;
			$tmp['deliveries_sum_case_quantity_DER'] 	= $sumCaseQuantityDER;
			$tmp['deliveries_sum_case_quantity_STP'] 	= $sumCaseQuantitySTP;
			$tmp['deliveries_sum_case_quantity_PRE'] 	= $sumCaseQuantityPRE;
			$tmp['deliveries_sum_case_quantity_DAN'] 	= $sumCaseQuantityDAN;
			$tmp['deliveries_sum_case_quantity_BRE'] 	= $sumCaseQuantityBRE;
			$tmp['deliveries_sum_case_quantity_RGY'] 	= $sumCaseQuantityRGY;
			$tmp['deliveries_sum_case_quantity_JKT'] 	= $sumCaseQuantityJKT;
			//$tmp['deliveries_sum_comment'] 				= implode( ','."\n", $comments );
			$tmp['deliveries_sum_comment'] 				= $comments;

			$res[$dc][] = $tmp;
		}

		$out = array();
		foreach( $res as $key=>$value ) {
			$out[$key] = $this->prepareResultQueryForOverviewOutput( $value );
		}
		$out = $this->sortArrayByDCs( $out );

		return $out;
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function prepareResultQueryForOverviewOutput( $query_result_array, $csv = FALSE ){
		$res 							= array();
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_container_type' );
		$containerTypeOptions 			= $this->tca->getMultiselectionOptions( $tcaNode );
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_container_size' );
		$containerSizeOptions 			= $this->tca->getMultiselectionOptions( $tcaNode );
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_traffic_type' );
		$trafficTypeOptions 			= $this->tca->getMultiselectionOptions( $tcaNode );
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_incoterm_code' );
		$incotermCodeOptions 			= $this->tca->getMultiselectionOptions( $tcaNode );
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_loading_type' );
		$loadingTypeOptions 			= $this->tca->getMultiselectionOptions( $tcaNode );
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$destinationPortOptions 		= $this->tca->getMultiselectionOptions( $tcaNode );
		$tcaNode 						= $this->tca->getColumnNodeById( 'contracts_contract_type' );
		$contractTypeOptions 			= $this->tca->getMultiselectionOptions( $tcaNode );

		foreach ( $query_result_array as $row) {
			$dates = array(
						'contracts_fob_date',
						'contracts_aldi_au_wh_delivery_due_date',
						'contracts_aldi_au_wh_delivery_due_end_date',
						'contracts_actual_contract_ready_date',
						'contracts_on_sale_date',
						'contracts_ofu_scheduled',
						'contracts_ofu_real',
						'contracts_received_in_cfs',
						'contracts_crdate',
						'contracts_modified',
						'contracts_actual_cargo_ready_date',
						'shipments_etd',
						'shipments_eta',
						'pcshipments_ietd',
						'pcshipments_ieta',
						'shipments_atd',
						'shipments_ata',
						'shipments_cus',
						'shipments_deliveries_mm_edl',
						'shipments_deliveries_mm_adl',
						'shipments_approval_date',
						'shipments_print_date',
						'shipments_import_date',
						'shipments_container_gate_in',
						'shipments_invoice_click_date',
						'shipments_bl_click_date',
						'shipments_packing_list_click_date',
						'shipments_treatment_certificate_click_date',
						'shipments_packing_declaration_click_date',
						'shipments_certificate_of_origin_click_date',
						'shipments_eta_minus_seven',
						'shipments_deliveries_mm_telex_first_reminder_sent',
						'shipments_deliveries_mm_telex_second_reminder_sent',
						'shipments_deliveries_mm_telex_set_date',
						);

			foreach( $dates as $date ) {
				if( isset( $row[$date] ) ) $row[$date] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? date( $this->config->item('date_format'), strtotime( $row[$date] ) ) : "";
			}
			if( isset( $row['shipments_container_size'] ) ) 	$row['shipments_container_size'] 	= $containerSizeOptions[$row['shipments_container_size']];
			if( isset( $row['shipments_container_type'] ) ) 	$row['shipments_container_type'] 	= $containerTypeOptions[$row['shipments_container_type']];
			if( isset( $row['contracts_traffic_type'] ) ) 		$row['contracts_traffic_type'] 		= $trafficTypeOptions[$row['contracts_traffic_type']];
			if( isset( $row['shipments_traffic_type'] ) ) 		$row['shipments_traffic_type'] 		= $trafficTypeOptions[$row['shipments_traffic_type']];
			if( isset( $row['shipments_incoterm_code'] ) ) 		$row['shipments_incoterm_code'] 	= $incotermCodeOptions[$row['shipments_incoterm_code']];
			if( isset( $row['shipments_loading_type'] ) ) 		$row['shipments_loading_type'] 		= $loadingTypeOptions[$row['shipments_loading_type']];
			if( isset( $row['shipments_destination_port'] ) ) 	$row['shipments_destination_port'] 	= $destinationPortOptions[$row['shipments_destination_port']];
			if( isset( $row['contracts_contract_type'] ) ) 		$row['contracts_contract_type'] 	= $contractTypeOptions[$row['contracts_contract_type']];
			if( isset( $row['contracts_supplier'] ) && $row['contracts_supplier'] > 0 ) {
				//$supplier = $this->supplier_model->getRecord( $row['contracts_supplier'] );
				$row['contracts_supplier'] = $this->getSupplierName( $row['contracts_supplier'] );
			}
			if( isset( $row['contracts_logwin_origin_office'] ) && $row['contracts_logwin_origin_office'] > 0 ) {
				//$office = $this->office_model->getRecord( $row['contracts_logwin_origin_office'] );
				$row['contracts_logwin_origin_office'] = $this->getOfficeName( $row['contracts_logwin_origin_office'] );
			}
			if( isset( $row['shipments_hbl_hawb'] ) ) $row['shipments_hbl_hawb'] = str_replace( array("\r\n", "\n", "\r"), array(",", ",", ","), $row['shipments_hbl_hawb'] );
			if( isset( $row['shipments_deliveries_mm_volume'] ) ) $row['shipments_deliveries_mm_volume'] = sprintf($this->config->item('doublevalues_format'), $row['shipments_deliveries_mm_volume']);
			if( isset( $row['deliveries_volume'] ) ) $row['deliveries_volume'] = sprintf($this->config->item('doublevalues_format'), $row['deliveries_volume']);
			if( isset( $row['shipments_contents_secured'] ) ) $row['shipments_contents_secured'] = ( $row['shipments_contents_secured'] == 1 ) ? 'Y' : 'N';

			if( $csv ) $row['shipments_deliveries_mm_volume'] = '="'.$row['shipments_deliveries_mm_volume'].'"';
			if( $csv ) $row['deliveries_volume'] 			= '="'.$row['deliveries_volume'].'"';
			if( $csv ) $row['deliveries_case_quantity'] 	= '="'.$row['deliveries_case_quantity'].'"';
			if( $csv ) $row['deliveries_gross_weight'] 		= '="'.$row['deliveries_gross_weight'].'"';
			if( $csv ) $row['deliveries_unit_quantity'] 	= '="'.$row['deliveries_unit_quantity'].'"';
			if( $csv ) $row['shipments_hbl_hawb'] 			= '"'.$row['shipments_hbl_hawb'].'"';

			if( isset( $row['shipments_container_size'] ) && isset( $row['shipments_container_type'] ) ) {
				$containerSizeWeights = array(
					'20DC' => '2500',
					'40DC' => '3800',
					'40HC' => '4000',
					'20REEFER' => '3400',
					'40REEFER' => '4800',
					'40HC REEFER' => '4800'
				);
				$key = $row['shipments_container_size'].$row['shipments_container_type'];
				if( isset( $containerSizeWeights[$key] ) ) {
					$row['shipments_container_size_weight'] = $row['shipments_container_size'].$row['shipments_container_type'];
				} else {
					$row['shipments_container_size_weight'] = $row['shipments_container_size'].$row['shipments_container_type'];
				}
				if( isset( $row['shipments_dg'] ) )  $row['shipments_dg'] = ( $row['shipments_dg'] != 0 ) ? "Yes" : "No";
			}

			$res[] = $row;
		}
		return $res;
	}

	private function getDCAddress( $id = 0 ){
		$out = '';
		if( $id > 0 ){
			$this->load->model('Dc_model', 'dc_model');
			$record = $this->dc_model->getRecord( $id );
			if( count( $record ) > 0 ) {
				$out = $record['dc_long'].' - '.$record['street'].', '.$record['suburb'].' '.$record['state'].' '.$record['postcode'];
			}
		}
		return $out;
	}

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	/**
	 * getDeliveriesForCDA
	 * returns the db-records with the given search params
	 *
	 * @param int $id
	 * @return array - db record as array.
	 */
	private function getDeliveriesForCDAReport( $id ) {
		$this->load->model('Delivery_model', 'delivery_model');

		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( $this->tablename );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->where( array( 'shipments_deliveries_mm.shipment_id' => $id, $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$this->db->_protect_identifiers = FALSE;
		$this->db->order_by('FIELD(deliveries_dc, "MIN", "DER", "STP", "PRE", "DAN", "BRE", "RGY", "JKT")');
		$this->db->_protect_identifiers = TRUE;
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * getDeliveriesByShipmentId
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	private function getDeliveriesByShipmentId( $shipmentId ) {
		$out = array();
		if( $shipmentId > 0 ) {
			$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
			$this->load->model('Delivery_model', 'delivery_model');

			$this->db->select( $this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.$this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ) );
			$this->db->from( 'shipments_deliveries_mm' );
			$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
			$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
			$this->db->where( array( 'shipments_deliveries_mm.shipment_id' => $shipmentId, 'shipments_deliveries_mm.hidden' => 0, 'shipments_deliveries_mm.deleted' => 0 ) );
			$this->db->group_by( 'shipments_deliveries_mm.id' );
			$query = $this->db->get();
			$out = $query->result_array();
		}
		return $out;
	}

	/**
	 * getDeliveriesByShipmentId
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	private function getShipmentDCById( $id ) {
		$out = '';
		if( $id > 0 ) {
			$this->load->model('Dc_model', 'dc_model');
			$dc = $this->dc_model->getRecord( $id );
			if( count( $dc ) > 0 ) $out = $dc['dc'];
		}
		return $out;
	}

	/**
	 * getSupplierName
	 * returns supplier name
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getSupplierName( $id=0 ) {
		$out = '';
		if( count( $this->suppliers ) <= 0 ) {
			$this->load->model('Supplier_model', 'supplier_model');
			$this->suppliers = $this->supplier_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		}

		foreach( $this->suppliers as $row ) {
			if( $row['id'] == $id ) return $row['name'];
		}

		return $out;
	}

	/**
	 * getOfficeName
	 * returns supplier name
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getOfficeName( $id=0 ) {
		$out = '';
		if( count( $this->offices ) <= 0 ) {
			$this->load->model('Office_model', 'office_model');
			$this->offices = $this->office_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		}

		foreach( $this->offices as $row ) {
			if( $row['id'] == $id ) return $row['name'];
		}

		return $out;
	}

	/**
	 * getContractType
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getContractType( $id=0 ) {
		$tcaNode 				= $this->tca->getColumnNodeById( 'contracts_contract_type' );
		$contractTypeOptions 	= $this->tca->getMultiselectionOptions( $tcaNode );
		return $contractTypeOptions[$id];
	}

	/**
	 * getTrafficType
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - contract type
	 */
	private function getTrafficType( $id=0 ) {
		$out = '';
		if( $id > 0 ) {
			$tcaNode 	= $this->tca->getColumnNodeById( 'contracts_traffic_type' );
			$options 	= $this->tca->getMultiselectionOptions( $tcaNode );
			$out		= $options[$id];
		}
		return $out;
	}

	/**
	 * getContainerTypeFromTCA
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getContainerTypeFromTCA( $id=0 ) {
		$containerSize = '';
		if( $id > 0 ) {
			$tcaNode 				= $this->tca->getColumnNodeById( 'shipments_container_type' );
			$containerTypeOptions 	= $this->tca->getMultiselectionOptions( $tcaNode );
			$containerType			= $containerTypeOptions[$id];
		}
		return $containerType;
	}

	/**
	 * getContainerSize
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getContainerSize( $id=0 ) {
		$containerSize = '';
		if( $id > 0 ) {
			$tcaNode 				= $this->tca->getColumnNodeById( 'shipments_container_size' );
			$containerSizeOptions 	= $this->tca->getMultiselectionOptions( $tcaNode );
			$containerSize			= $containerSizeOptions[$id];
		}
		return $containerSize;
	}

	/**
	 * getContainerType
	 * @param id
	 * @return int
	 */
	private function getContainerType( $id )
	{
		$this->load->model('Container_type_model', 'container_type_model');
		$out = '';
		$rows = $this->container_type_model->getRecordsWhere( array( 'id'=> $id, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $rows ) ) {
			$out = $rows[0]['container_type_procars'];
		}
		return $out;
	}

	/**
	 * getDestinationPort
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getDestinationPort( $id=0 ) {
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$destinationPortOptions 		= $this->tca->getMultiselectionOptions( $tcaNode );
		return $destinationPortOptions[$id];
	}

	/**
	 * getFormattedDate
	 * returns date
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getFormattedDate( $date=0 ) {
		return ( $date != "0000-00-00 00:00:00" && !empty( $date ) ) ? date( $this->config->item('date_format'), strtotime( $date ) ) : "";
	}


	/**
	 * sortArrayByDCs
	 * returns date
	 *
	 * @param array
	 * @return array
	 */
	private function sortArrayByDCs( $data ) {
		$tmp = array();
		$dcs = array('MIN','DER','STP','PRE','DAN','BRE','RGY','JKT');

		foreach( $dcs as $dc ) {
			if( isset( $data[$dc] ) ) $tmp[$dc] = $data[$dc];
		}
		return $tmp;
	}

	/**
	 * getContractDelayReason
	 * returns delay reason
	 *
	 * @param int $id - id of record to retrieve
	 * @return string
	 */
	private function getContractDelayReason( $id ) {
		$delayReason = '';

		if( $id > 0 ) {
			$tcaNode 				= $this->tca->getColumnNodeById( 'contracts_delay_reason' );
			$delayReasonOptions 	= $this->tca->getMultiselectionOptions( $tcaNode );
			$delayReason			= $delayReasonOptions[$id];
		}

		return $delayReason;
	}

	/**
	 * getShipmentDelayReason
	 * returns delay reason
	 *
	 * @param int $id - id of record to retrieve
	 * @return string
	 */
	private function getShipmentDelayReason( $id ) {
		$out = '';

		if( count( $this->delayReasons ) <= 0 ) {
			$this->load->model('Delay_reason_model', 'delay_reason_model');
			$this->delayReasons = $this->delay_reason_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		}

		foreach( $this->delayReasons as $row ) {
			if( $row['id'] == $id ) return $row['title'];
		}

		return $out;
	}

}

/* End of file orders.php */
/* Location: ./app/models/orders.php */
