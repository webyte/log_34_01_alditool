<?php

class Abstract_aldi_model extends CI_Model {
	protected $tablename;
	
    function __construct() {
        parent::__construct();
    }

	/**
	 * configures where clause for db query, depending on values in searchform.
	 */
	public function setWhereClauseForSearchparams( $recordListSearchParams ) {
		if( !$recordListSearchParams ) return;
		$fieldnames = array(
						'shipments_id',
						'shipments_hbl_hawb',
						'pcshipments_hbl_hawb',
						'contracts_contract_number',
						'contracts_contract_type',
						'shipments_container_number',
						'pcshipments_container_number',
						'pcshipments_departure_port',
						'pcshipments_destination_port',
						'pcshipments_po_number',
						'contracts_product_code',
						'deliveries_product_code',
						'contracts_departure',
						'deliveries_destination',
						'contracts_traffic_type',
						'event',
						'pcevent',
						'shipments_from',
						'shipments_to',
						'contracts_advertisement_week',
						'deliveries_advertisement_week',
						'contracts_logwin_origin_office',
						'contracts_supplier',
						'shipments_approval',
						'deliveries_dc',
						'shipments_incoterm_code',
						'shipments_telex_received',
						'shipments_dc',
						'shipments_departure_port',
						'shipments_destination_port',
						'shipments_mother_vessel_name',
						'shipments_vessel_2',
						'shipments_voyage_2',
						'shipments_etd',
						'shipments_ata',
						'shipments_voyage_number',
						'shipments_carrier',
						'shipments_num_of_transmission',
						'containers_cus',
						'containers_aqc',
						'containers_event',
						'containers_from',
						'containers_to',
						'deliveries_crdate',
						'shipments_crdate',
						'pcshipments_crdate',
						'contracts_crdate',
						'contracts_modified',
						'shipments_modified',
						'deliveries_modified',
						);
		foreach ( $fieldnames as $fname ) {
			$tmpValue = array_key_exists($fname, $recordListSearchParams) ? $recordListSearchParams[$fname][0] : '';
			if( $tmpValue !== '' ){
				switch($fname){
					case 'shipments_id':
						$this->db->where( 'shipments.id', $tmpValue );
						break;
					case 'shipments_hbl_hawb':
						$this->db->like( 'shipments.hbl_hawb', $tmpValue );
						break;
					case 'pcshipments_hbl_hawb':
						$this->db->like( 'pcshipments.hbl_hawb', $tmpValue );
						break;
					case 'contracts_contract_number':
						$this->db->where( 'contracts.contract_number', $tmpValue );
						break;
					case 'contracts_contract_type':
						$this->db->where( 'contracts.contract_type', $tmpValue );
						break;
					case 'shipments_container_number':
						$this->db->where( 'shipments.container_number', $tmpValue );
						break;
					case 'shipments_departure_port':
						$this->db->where( 'shipments.departure_port', $tmpValue );
						break;
					case 'shipments_destination_port':
						if($tmpValue != 0) $this->db->where( 'shipments.destination_port', $tmpValue );
						break;
					case 'pcshipments_container_number':
						$this->db->where( 'pcshipments.container_number', $tmpValue );
						break;
					case 'deliveries_product_code':
						$this->db->where( 'deliveries.product_code', $tmpValue );
						break;
					case 'contracts_product_code':
						$this->db->where( 'contracts.product_code', $tmpValue );
						break;
					case 'contracts_departure':
						$this->db->where( 'contracts.departure', $tmpValue );
						break;
					case 'deliveries_destination':
						$this->db->where( 'deliveries.destination', $tmpValue );
						break;
					case 'pcshipments_departure_port':
						$this->db->where( 'pcshipments.departure_port', $tmpValue );
						break;
					case 'pcshipments_destination_port':
						$this->db->where( 'pcshipments.destination_port', $tmpValue );
						break;
					case 'pcshipments_po_number':
						$this->db->like( 'pcshipments.po_number', $tmpValue );
						break;
					case 'deliveries_dc':
						$this->db->where( 'deliveries.dc', $tmpValue );
						break;
					case 'contracts_traffic_type':
						$this->db->where( 'contracts.traffic_type', $tmpValue );
						break;
					case 'event':
						$fromDate 	= array_key_exists('shipments_from', $recordListSearchParams) ? $recordListSearchParams['shipments_from'][0] : '';
						$toDate 	= array_key_exists('shipments_to', $recordListSearchParams) ? $recordListSearchParams['shipments_to'][0] : '';
						$table 		= $tmpValue != 'fob_date' ? 'shipments' : 'contracts';
						if(!empty($fromDate)) {
							$fromDate 	= str_replace( '/', '.', $fromDate );
							$comDate 	= date("Y-m-d H:i:s", strtotime($fromDate.' 00:00:00') );
							$where 		= $table.".".$tmpValue." >= '$comDate' ";
							$this->db->where($where);
						}

						if(!empty($toDate)) {
							$toDate 	= str_replace( '/', '.', $toDate );
							$comDate 	= date("Y-m-d H:i:s", strtotime($toDate.' 23:59:59') );
							$where 		= $table.".".$tmpValue." <= '$comDate' ";
							$this->db->where($where);
						}
						break;
					case 'pcevent':
						$fromDate 	= array_key_exists('pcshipments_from', $recordListSearchParams) ? $recordListSearchParams['pcshipments_from'][0] : '';
						$toDate 	= array_key_exists('pcshipments_to', $recordListSearchParams) ? $recordListSearchParams['pcshipments_to'][0] : '';
						if(!empty($fromDate)) {
							$fromDate = str_replace( '/', '.', $fromDate );
							$comDate = date("Y-m-d H:i:s", strtotime($fromDate.' 00:00:00') );
							$where = "pcshipments.".$tmpValue." >= '$comDate' ";
							$this->db->where($where);
						}

						if(!empty($toDate)) {
							$toDate = str_replace( '/', '.', $toDate );
							$comDate = date("Y-m-d H:i:s", strtotime($toDate.' 23:59:59') );
							$where = "pcshipments.".$tmpValue." <= '$comDate' ";
							$this->db->where($where);
						}
						break;
					case 'shipments_etd':
						$fromDate 	= str_replace( '/', '.', $tmpValue );
						$comDate 	= date("Y-m-d H:i:s", strtotime($fromDate.' 00:00:00') );
						$where 		= "shipments.etd >= '".$comDate."' ";
						$this->db->where($where);

						$toDate 	= str_replace( '/', '.', $tmpValue );
						$comDate 	= date("Y-m-d H:i:s", strtotime($toDate.' 23:59:59') );
						$where 	= "shipments.etd <= '".$comDate."' ";
						$this->db->where($where);

						break;
					case 'shipments_ata':
						$this->db->where( 'shipments.ata', $tmpValue );
						break;
					case 'contracts_advertisement_week':
						$this->db->where( 'contracts.advertisement_week', $tmpValue );
						break;
					case 'deliveries_advertisement_week':
						$this->db->where( 'deliveries.advertisement_week', $tmpValue );
						break;
					case 'contracts_logwin_origin_office':
						$this->db->where( 'contracts.logwin_origin_office', $tmpValue );
						break;
					case 'contracts_supplier':
						$this->db->where( 'contracts.supplier', $tmpValue );
						break;
					case 'shipments_telex_received':
						if( $tmpValue != 3 ) $this->db->where( 'shipments.telex_received', $tmpValue );
						break;
					case 'shipments_dc':
						$this->db->where( 'shipments.dc', $tmpValue );
						break;
					case 'shipments_incoterm_code':
						$this->db->where( 'shipments.incoterm_code', $tmpValue );
						break;
					case 'shipments_mother_vessel_name':
						$this->db->like( 'shipments.mother_vessel_name', $tmpValue );
						break;
					case 'shipments_voyage_number':
						$this->db->like( 'shipments.voyage_number', $tmpValue );
						break;
					case 'shipments_voyage_2':
						$this->db->like( 'shipments.voyage_2', $tmpValue );
						break;
					case 'shipments_vessel_2':
						$this->db->like( 'shipments.vessel_2', $tmpValue );
						break;
					case 'shipments_carrier':
						$this->db->like( 'shipments.carrier', $tmpValue );
						break;
					case 'shipments_num_of_transmission':
						if( $tmpValue == 'no' ) $this->db->where( 'shipments.num_of_transmission', 0 );
						break;
					case 'containers_cus':
						if( $tmpValue == 1 ) $this->db->where( 'shipments.cus !=', '0000-00-00 00:00:00' );
						else $this->db->where( 'shipments.cus', '0000-00-00 00:00:00' );
						break;
					case 'containers_aqc':
						if( $tmpValue == 1 ) $this->db->where( 'shipments.aqc !=', '0000-00-00 00:00:00' );
						else $this->db->where( 'shipments.aqc', '0000-00-00 00:00:00' );
						break;
					case 'containers_event':
						$fromDate 	= array_key_exists('containers_from', $recordListSearchParams) ? $recordListSearchParams['containers_from'][0] : '';
						$toDate 	= array_key_exists('containers_to', $recordListSearchParams) ? $recordListSearchParams['containers_to'][0] : '';
						if(!empty($fromDate)) {
							$fromDate = str_replace( '/', '.', $fromDate );
							$comDate = date("Y-m-d H:i:s", strtotime($fromDate.' 00:00:00') );
							$where = $tmpValue." >= '$comDate' ";
							$this->db->where($where);
						}

						if(!empty($toDate)) {
							$toDate = str_replace( '/', '.', $toDate );
							$comDate = date("Y-m-d H:i:s", strtotime($toDate.' 23:59:59') );
							$where = $tmpValue." <= '$comDate' ";
							$this->db->where($where);
						}
						break;
					case 'deliveries_crdate':
						$where = "deliveries.crdate >= '$tmpValue' ";
						$this->db->where($where);
						break;
					case 'shipments_crdate':
						$where = "shipments.crdate >= '$tmpValue' ";
						$this->db->where($where);
						break;
					case 'pcshipments_crdate':
						$where = "pcshipments.crdate >= '$tmpValue' ";
						$this->db->where($where);
						break;
					case 'contracts_crdate':
						$where = "contracts.crdate >= '$tmpValue' ";
						$this->db->where($where);
						break;
					case 'contracts_modified':
						$where = "contracts.modified >= '$tmpValue' ";
						$this->db->where($where);
						break;
					case 'shipments_modified':
						$where = "shipments.modified >= '$tmpValue' ";
						$this->db->where($where);
						break;
					case 'deliveries_modified':
						$where = "deliveries.modified >= '$tmpValue' ";
						$this->db->where($where);
						break;
				}
			}
		}
	}


}

/* End of file abstract_aldi_model.php */
/* Location: ./app/models/abstract_aldi_model.php */
