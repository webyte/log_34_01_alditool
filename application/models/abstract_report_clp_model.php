<?php
require_once( APPPATH.'models/abstract_model.php' );

class Abstract_report_clp_model extends Abstract_model {

	public static $REPORT_TYPE_CONTAINER_STATUS 	= 1;
	public static $REPORT_TYPE_DELAY 				= 2;
	public static $REPORT_TYPE_KPI 					= 3;
	public static $REPORT_TYPE_SHIPMENT_STATUS 		= 4;
	public static $REPORT_TYPE_PO 					= 5;
	public static $REPORT_TYPE_SHIPMENT		 		= 6;
	public static $REPORT_TYPE_SHIPMENT_INTERFACE	= 7;

	function __construct() {
		parent::__construct();
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->driver('cache', array('adapter' => 'file'));
	}

	/**
	 * generateCache
	 *
	 * @return
	 */
	public function generateCache() {
		$res = $this->getRecords();
		$this->cache->file->save($this->cacheName, $res, 600);
	}

	/**
	 * prepareResultQueryForOutput
	 *
	 * @return
	 */
	public function prepareResultQueryForOutput( $query_result_array, $date_format = '' ){
        $res = array();
		$dateFormat = empty( $date_format ) ? $this->config->item('date_format') : $date_format;

		foreach ( $query_result_array as $row) {

			if( isset( $row['shipments_hbl_hawb'] ) ) $row['shipments_hbl_hawb'] = str_replace( array("\r\n", "\n", "\r"), array(",", ",", ","), $row['shipments_hbl_hawb'] );

			if( $row['contracts_traffic_type'] == 1 ) {
				$row['service_type'] = 'CY/CY';
			}  else {
				$row['service_type'] = 'CFS/CY';
			}

			if( !empty( $row['shipments_id'] ) ) {
				$row['nominated_off'] = $this->getCarrierName( $row['shipments_carrier'] );
			} else {
				$row['nominated_off'] = '';
			}

			if( $row['shipments_traffic_type'] == 1 ) {
				$row['movement'] = 'CY/CY';
			}  else {
				$row['movement'] = 'CFS/CY';
			}

			if( $row['shipments_container_size'] == 1 ) {
				$row['no_of_teu'] = '1';
			}  else {
				$row['no_of_teu'] = '2';
			}

			if( $row['shipments_ata'] != '0000-00-00 00:00:00' ) {
				$row['latest_shipment_status'] = 'Vessel arrived';
			} else if ( $row['shipments_atd'] != '0000-00-00 00:00:00' ) {
				$row['latest_shipment_status'] = 'Vessel departed';
			} else if ( $row['shipments_container_gate_in'] != '0000-00-00 00:00:00' || $row['shipments_received_in_cfs'] != '0000-00-00 00:00:00' ) {
				$row['latest_shipment_status'] = 'Cargo Received at Origin';
			} else if ( $row['contracts_ofu_real'] != '0000-00-00 00:00:00' ) {
				$row['latest_shipment_status'] = 'Booked Date';
			} else {
				$row['latest_shipment_status'] = 'Open Order';
			}

			$row['shipments_destination_port'] = $this->getDestinationPort( $row['shipments_destination_port'] );
			if( $row['shipments_destination_port'] == 'AUFRE' ) {
				$row['domestic_freight_forwarder'] = 'Sadleirs';
			}  else {
				$row['domestic_freight_forwarder'] = 'Newline';
			}

			$row['booking_deadline'] 		= ( $row['contracts_fob_date'] != '0000-00-00 00:00:00' ) ? date( $dateFormat, strtotime( '-21 days', strtotime( $row['contracts_fob_date'] ) ) ) : '';
			$row['ship_mode']				= 'SEA';
			$row['dgr_flag']				= 'N';

			$row['contracts_supplier']		= $this->getSupplierName( $row['contracts_supplier'] );

			$row['delay_reason_group'] 		= $this->getDelayReasonGroup( $row['shipments_delay_reason_group'] );
			$row['delay_reason'] 			= $this->getDelayReason( $row['shipments_delay_reason'] );

			$containerType = $this->getContainerType( $row['shipments_container_type'] );
			$containerSize = $this->getContainerSize( $row['shipments_container_size'] );
			$row['container_type'] = $containerSize.$containerType;

			$row['20dc'] = ( $row['container_type'] == '20DC' || $row['container_type'] == '20GP' ) ? '1' : '';
			$row['40dc'] = ( $row['container_type'] == '40DC' || $row['container_type'] == '40GP' ) ? '1' : '';
			$row['40hc_substitution_for_40'] = '';
			$row['40hc'] = ( $row['container_type'] == '40HC' ) ? '1' : '';
			$row['20rf'] = ( $row['container_type'] == '20RF' ) ? '1' : '';
			$row['40rf'] = ( $row['container_type'] == '40RF' ) ? '1' : '';
			$row['20no'] = ( $row['container_type'] == '20NO' ) ? '1' : '';
			$row['40no'] = ( $row['container_type'] == '40NO' ) ? '1' : '';

			$row['kpi_20ge'] = ( $row['container_type'] == '20DC' || $row['container_type'] == '20GP' || $row['container_type'] == '20RF' || $row['container_type'] == '20HC' ) ? '1' : '';
			$row['kpi_40ge'] = ( $row['container_type'] == '40DC' || $row['container_type'] == '40GP' ) ? '1' : '';;
			$row['kpi_40hc'] = ( $row['container_type'] == '40HC' || $row['container_type'] == '40RF' || $row['container_type'] == '40NO' || $row['container_type'] == '40HR' ) ? '1' : '';;

			$row['cbm_utilization'] 	= $this->getCBMUtilization( $row['volume'], $row['container_type'] );
			$row['weight_utilization'] 	= $this->getWeightUtilization( $row['weight'], $containerType );
			$row['shipments_dc'] 		= $this->getShipmentDCById( $row['shipments_dc'] );


			if( ( strtotime( $row['shipments_container_gate_in'] ) - strtotime( $row['contracts_fob_date'] ) ) <= 0 ) {
				$row['crd_on_time'] = 'On Time';
			} else {
				$row['crd_on_time'] = 'Late';
			}

			$row['first_leg_eta']				= $row['pcshipments_first_leg_eta'];
			$row['first_leg_ata']				= $row['pcshipments_first_leg_ata'];
			$row['last_leg_etd']				= $row['pcshipments_last_leg_etd'];
			$row['last_leg_atd']				= $row['pcshipments_last_leg_atd'];

			$row['crd_vs_factory_cargo_ready_date'] = ( $row['contracts_fob_date'] != '0000-00-00 00:00:00' && $row['contracts_actual_contract_ready_date'] != '0000-00-00 00:00:00' ) ? round( ( strtotime( $row['contracts_fob_date'] ) - strtotime( $row['contracts_actual_contract_ready_date'] ) ) / ( 24 * 60 * 60 ) ) : '';
			$row['first_leg_etd_vs_first_leg_atd'] 	= ( $row['shipments_etd'] != '0000-00-00 00:00:00' && $row['shipments_atd'] != '0000-00-00 00:00:00' ) ? round(( strtotime( $row['shipments_etd'] ) - strtotime( $row['shipments_atd'] ) ) / ( 24 * 60 * 60 ) ) : '';
			$row['last_leg_eta_vs_last_leg_ata'] 	= ( $row['shipments_eta'] != '0000-00-00 00:00:00' && $row['shipments_ata'] != '0000-00-00 00:00:00' ) ? round(( strtotime( $row['shipments_eta'] ) - strtotime( $row['shipments_ata'] ) ) / ( 24 * 60 * 60 ) ) : '';
			$row['last_leg_eta_vs_osd'] 			= ( $row['shipments_eta'] != '0000-00-00 00:00:00' && $row['contracts_on_sale_date'] != '0000-00-00 00:00:00' ) ? round(( strtotime( $row['shipments_eta'] ) - strtotime( $row['contracts_on_sale_date'] ) ) / ( 24 * 60 * 60 ) ) : '';
			$row['last_leg_ata_vs_osd'] 			= ( $row['shipments_ata'] != '0000-00-00 00:00:00' && $row['contracts_on_sale_date'] != '0000-00-00 00:00:00' ) ? round(( strtotime( $row['shipments_ata'] ) - strtotime( $row['contracts_on_sale_date'] ) ) / ( 24 * 60 * 60 ) ) : '';
			$row['first_leg_etd_vs_last_leg_eta'] 	= ( $row['shipments_etd'] != '0000-00-00 00:00:00' && $row['shipments_eta'] != '0000-00-00 00:00:00' ) ? round(( strtotime( $row['shipments_eta'] ) - strtotime( $row['shipments_etd'] ) ) / ( 24 * 60 * 60 ) ) : '';

			$row['crd_vs_factory_cargo_ready_date'] = $row['crd_vs_factory_cargo_ready_date'];
			$row['first_leg_etd_vs_first_leg_atd'] 	= $row['first_leg_etd_vs_first_leg_atd'];
			$row['last_leg_eta_vs_last_leg_ata']	= $row['last_leg_eta_vs_last_leg_ata'];
			$row['last_leg_eta_vs_osd']				= $row['last_leg_eta_vs_osd'];
			$row['last_leg_ata_vs_osd'] 			= $row['last_leg_ata_vs_osd'];
			$row['first_leg_etd_vs_last_leg_eta']	= $row['first_leg_etd_vs_last_leg_eta'];

			$dates = array(
				'shipments_etd',
				'shipments_eta',
				'shipments_atd',
				'shipments_ata',
				'contracts_fob_date',
				'contracts_on_sale_date',
				'contracts_ofu_scheduled',
				'contracts_ofu_real',
				'contracts_actual_contract_ready_date',
				'contracts_actual_cargo_ready_date',
				'contracts_aldi_au_wh_delivery_due_end_date',
				'contracts_initial_crd',
				'contracts_initial_sales_date',
				'shipments_received_in_cfs',
				'contracts_received_in_cfs',
				'shipments_container_gate_in',
				'shipments_so_released_date',
				'shipments_cy_cut_off',
				'shipments_cus',
				'pcshipments_first_leg_eta',
				'pcshipments_first_leg_ata',
				'pcshipments_last_leg_etd',
				'pcshipments_last_leg_atd',
				'pcshipments_ietd',
				'pcshipments_ieta',
				'first_leg_eta',
				'first_leg_ata',
				'last_leg_etd',
				'last_leg_atd',
				'osd',
				'delayed',
			);

			foreach( $dates as $date ) {
				if( isset( $row[$date] ) ) {
					$row[$date] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? date( $dateFormat, strtotime( $row[$date] ) ) : '';
				} else {
					$row[$date] = '';
				}
			}

			$res[] = $row;
        }
        return $res;
    }

	/**
	 * getAllFieldsFromTable
	 *
	 * @return
	 */
	public function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

    /**
     * getSupplierName
     * returns supplier name
     *
     * @param int $id - id of record to retrieve
     * @return string - supplier name
     */
    private function getSupplierName( $id=0 )
    {
    	$out = '';
        if( count( $this->suppliers ) <= 0 ) {
			$this->load->model('Supplier_model', 'supplier_model');
			$this->suppliers = $this->supplier_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		}

        foreach( $this->suppliers as $row ) {
			if( $row['id'] == $id ) return $row['name'];
		}

        return $out;
    }

    /**
     * getContainerSize
     * returns contract type
     *
     * @param int $id - id of record to retrieve
     * @return string - supplier name
     */
    public function getContainerSize( $id=0 ) {
        $containerSize = '';
        if( $id > 0 ) {
            $tcaNode 				= $this->tca->getColumnNodeById( 'shipments_container_size' );
            $containerSizeOptions 	= $this->tca->getMultiselectionOptions( $tcaNode );
            $containerSize			= $containerSizeOptions[$id];
        }
        return $containerSize;
    }

    /**
     * getContainerType
     * @param id
     * @return int
     */
	public function getContainerType( $id )
    {
		$containerType = '';
		if( $id > 0 ) {
			$tcaNode 				= $this->tca->getColumnNodeById( 'shipments_container_type' );
			$containerTypeOptions 	= $this->tca->getMultiselectionOptions( $tcaNode );
			$containerType			= $containerTypeOptions[$id];
		}
		return $containerType;
	}

	/**
	 * getContractType
	 * @param id
	 * @return string
	 */
	public function getContractType( $id )
	{
		$contractType = '';
		if( $id > 0 ) {
			$tcaNode 				= $this->tca->getColumnNodeById( 'contracts_contract_type' );
			$contractTypeOptions 	= $this->tca->getMultiselectionOptions( $tcaNode );
			$contractType			= $contractTypeOptions[$id];
		}
		return $contractType;
	}

	/**
	 * getCarrierName
	 * @param $str
	 * @return string
	 */
	public function getCarrierName( $str ) {
		$out = '';
		if (stristr($str, 'HAPAG') !== false) {
			$out = 'Hapag Lloyd';
		} else if (stristr($str, 'MAERSK') !== false) {
			$out = 'Maersk';
		} else if (stristr($str, 'Hamburg') !== false) {
			$out = 'Hamburg Sud';
		} else if (stristr($str, 'Evergreen') !== false) {
			$out = 'Evergreen';
		} else if (stristr($str, 'ANL') !== false) {
			$out = 'CMA-CGM';
		} else if (stristr($str, 'Cosco') !== false) {
			$out = 'Cosco';
		} else if (stristr($str, 'OOCL') !== false) {
			$out = 'OOCL';
		} else if (stristr($str, 'MSC') !== false) {
			$out = 'MSC';
		} else {
			$out = 'Logwin';
		}
		return $out;
	}

    /**
     * getCBMUtilization
     * returns cbm utilization
     *
     * @param string $volume
     * @param string $containerType
     * @return string - cbm utilization
     */
    private function getCBMUtilization( $volume, $containerType )
    {
        switch( $containerType ) {
            case '20GP':
            case '20DC':
            case '20RF':
            case '20NO':
                $a = 28;
                break;
            case '40GP':
            case '40DC':
            case '40RF':
            case '40NO':
                $a = 58;
                break;
            case '40HC':
            case '40HCRF':
                $a = 68;
                break;
        }
         return sprintf( $this->config->item('floatvalues_format'), ( $volume / $a ) * 100 );
    }

    /**
     * getWeightUtilization
     * returns weight utilization
     *
     * @param string $weight
     * @param string $containerType
     * @return string - weight utilization
     */
    private function getWeightUtilization( $weight, $containerType )
    {
        switch( $containerType ) {
            case 'HC':
            case 'HCRF':
                $a = 21000;
                break;
            default:
                $a = 21500;
        }
        return sprintf( $this->config->item('floatvalues_format'), ( $weight / $a ) * 100 );
    }

	/**
	 * getDestinationPort
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getDestinationPort( $id=0 ) {
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$destinationPortOptions 		= $this->tca->getMultiselectionOptions( $tcaNode );
		return $destinationPortOptions[$id];
	}

	/**
	 * getShipmentDCById
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return string
	 */
	private function getShipmentDCById( $id ) {
		$out = '';

		if( count( $this->dcs ) <= 0 ) {
			$this->load->model('Dc_model', 'dc_model');
			$this->dcs = $this->dc_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		}

		foreach( $this->dcs as $row ) {
			if( $row['id'] == $id ) return $row['dc'];
		}

		return $out;
	}

	/**
	 * getDelayReason
	 * returns delay reason
	 *
	 * @param int $id - id of record to retrieve
	 * @return string
	 */
	public function getDelayReason( $id ) {
		$out = '';

		if( count( $this->delayReasons ) <= 0 ) {
			$this->load->model('Delay_reason_model', 'delay_reason_model');
			$this->delayReasons = $this->delay_reason_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		}

		foreach( $this->delayReasons as $row ) {
			if( $row['id'] == $id ) return $row['title'];
		}

		return $out;
	}

	/**
	 * getDelayReasonGroup
	 * returns delay reason group
	 *
	 * @param int $id - id of record to retrieve
	 * @return string
	 */
	public function getDelayReasonGroup( $id ) {
		$tcaNode 	= $this->tca->getColumnNodeById( 'delay_reasons_group' );
		$options 	= $this->tca->getMultiselectionOptions( $tcaNode );
		return ( $id > 0 && isset( $options[$id] ) ) ? $options[$id] : '';
	}

}

/* End of file orders.php */
/* Location: ./app/models/orders.php */
