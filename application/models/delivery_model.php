<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Delivery_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->tablename = 'deliveries';
	}

	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select('deliveries.id AS id,
							contracts.contract_number AS contract_number,
							deliveries.product_code AS product_code,
							deliveries.dc AS dc,
							contracts.advertisement_week AS advertisement_week,
							contracts.fob_date AS fob_date,
							contracts.aldi_au_wh_delivery_due_date AS aldi_au_wh_delivery_due_date,
							contracts.departure AS departure,
							deliveries.destination AS destination,
							deliveries.unit_quantity_remain AS unit_quantity_remain,
							deliveries.case_quantity_remain AS case_quantity_remain,
							deliveries.case_quantity AS case_quantity,
							deliveries.unit_quantity AS unit_quantity');
		$this->db->from( $this->tablename );
		$this->db->join('contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0');
		$this->db->join('shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join('shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left');
		$loo = $this->session->userdata('logwin_origin_office');
		if( !empty( $loo ) ) $this->db->where( array( 'contracts.logwin_origin_office' => $loo ) );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( 'contracts.cube_qa_check' => 1, 'deliveries.unit_quantity_remain >' => 0, $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			//$actualUnitQuantiy = $this->shipment_delivery_model->getActualUnitQuantityByDeliveryId( $row['id'] );
			//$row['unit_quantity_remain'] = $row['unit_quantity'] - $actualUnitQuantiy;
			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * getApiRecords
	 * @return array - db record as array.
	 */
	public function getApiRecords() {
		$res = array();
		$this->db->select('deliveries.id AS id,
							contracts.advertisement_week AS advertisement_week,
							contracts.contract_number AS contract_number,
							deliveries.product_code AS product_code,
							deliveries.dc AS dc,
							contracts.departure AS departure,
							deliveries.destination AS destination,
							deliveries.case_quantity AS case_quantity,
							deliveries.gross_weight AS gross_weight,
							deliveries.volume AS volume,
							contracts.case_length AS case_length,
							contracts.case_width AS case_width,
							contracts.case_height AS case_height,
							contracts.traffic_type AS traffic_type,
							offices.id AS office_id,
							offices.name AS office_name,
							offices.contact_mail AS office_email');
		$this->db->from( 'deliveries' );
		$this->db->join('contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0');
		$this->db->join('shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join('offices', 'offices.id = contracts.logwin_origin_office AND offices.hidden = 0 AND offices.deleted = 0', 'left');
		$this->db->where(
			array(
				'contracts.cube_qa_check' => 1,
				'deliveries.unit_quantity_remain >' => 0,
				'shipments_deliveries_mm.id' => NULL,
				'deliveries.hidden' => 0,
				'deliveries.deleted' => 0
			)
		);
		$this->db->group_by( 'deliveries.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $res;
	}


	/**
	 * getRecordsForContract
	 * returns all related hawb records for the given shipment id
	 *
	 * @param int $shipmentId - id of shipmentrecord we want related events for
	 * @return array query result as array
	 */
	public function getRecordsForContract( $contractId ){
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$out = array();
		$this->db->where( array('contract_id' => $contractId, 'hidden' => 0, 'deleted' => 0) );
		$query = $this->db->get( $this->tablename );
		foreach( $query->result_array() as $row ) {
			$row['hbl_hawb'] = '';
			$row['hbl_hawb_mm'] = '';
			$row['unit_quantity_remain'] = $row['unit_quantity'];
			$shipments = $this->shipment_delivery_model->getShipmentsByDeliveryId( $row['id'] );
			if( count( $shipments ) > 0 ) {
				$tmp = array();
				foreach( $shipments as $shipment ) {
					$tmp[] = array( 'id' => $shipment['id'], 'hbl_hawb' => $shipment['hbl_hawb'], 'hbl_hawb_mm' => $shipment['hbl_hawb_mm'], 'approval' => $shipment['approval'] );
				}
				$row['hbl_hawb'] = $tmp;
				$row['hbl_hawb_mm'] = $tmp;
				$row['unit_quantity_remain'] = $row['unit_quantity'] - $this->shipment_delivery_model->getActualUnitQuantityByDeliveryId( $row['id'] );
			}
			$out[] = $row;
		}
		return $out;
	}

	/**
	 * getRecordsByPON
	 * returns all related records for the given po number
	 *
	 * @param string $pon
	 * @return array query result as array
	 */
	public function getRecordsByPON( $pon ){
		$out = array();
		$this->db->where( array('contract_number' => $pon, 'hidden' => 0, 'deleted' => 0) );
		$query = $this->db->get( $this->tablename );
		foreach( $query->result_array() as $row ) {
			$out[] = $row;
		}
		return $out;
	}

	/**
	 * getRecordsForShipment
	 * returns all related hawb records for the given shipment id
	 *
	 * @param int $shipmentId - id of shipmentrecord we want related events for
	 * @return array query result as array
	 */
	public function getRecordsForShipment( $shipmentId ){
		$this->load->model('Shipment_model', 'shipment_model');
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');

		$shipment = $this->shipment_model->getRecord( $shipmentId );

		$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId( $shipmentId );
		$res = array();
		foreach( $deliveries as $row ) {
			$row['approval'] = $shipment['approval'];
			$res[] = $row;
		}
		return $res;
	}

	/**
	 * getRecordForActivityLog
	 * returns related record for the given delivery id and shipment id
	 *
	 * @param int $deliveryId - id of record
	 * @param int $shipmentId - id of
	 * @return array query result as array
	 */
	public function getRecordForActivityLog( $deliveryId, $shipmentId ){
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$records 			= $this->shipment_delivery_model->getRecordsWhere( array( 'delivery_id' => $deliveryId, 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
		$unitQuantity		= 0;
		$caseQuantity		= 0;
		$volume				= 0;
		$weight				= 0;
		$edl				= '';
		$adl				= '';
		$telexReceived		= 0;
		if( count( $records ) > 0 ) {
			$unitQuantity 	= $records[0]['unit_quantity'];
			$caseQuantity 	= $records[0]['case_quantity'];
			$volume			= $records[0]['volume'];
			$weight 		= $records[0]['gross_weight'];
			$edl 			= $records[0]['edl'];
			$adl 			= $records[0]['adl'];
			$telexReceived	= $records[0]['telex_received'];
		}
		$delivery 					= $this->getRecord( $deliveryId );
		$delivery['unit_quantity'] 	= $unitQuantity;
		$delivery['case_quantity'] 	= $caseQuantity;
		$delivery['volume'] 		= $volume;
		$delivery['gross_weight'] 	= $weight;
		$delivery['edl'] 			= $edl;
		$delivery['adl'] 			= $adl;
		$delivery['telex_received'] = $telexReceived;

		return $delivery;
	}

	/**
	 * deleteRecordsByParentId
	 * deletes record with given id from table
	 *
	 * @param int $id - row id
	 *
	 */
	public function deleteRecordsByParentId( $id, $field ) {
		$this->db->update( $this->tablename, array( 'deleted' => 1 ), array( $field => $id ) );
	}

	/**
	 * getIdByDCAndContractId
	 * returns idr
	 *
	 * @param str $dc - sku of record
	 * @param int $contractId - id of record
	 * @return int - id of record
	 */
	public function getIdByDCAndContractId( $dc, $contractId ) {
		$out = 0;
		$query = $this->db->get_where( $this->tablename, array( 'dc' => $dc, 'contract_id' => $contractId, 'hidden' => 0, 'deleted' => 0 ) );
		if( $query->num_rows() > 0 ) {
			$row = $query->row_array();
			$out = $row['id'];
		}
		return $out;
	}

	/**
	 * getReportRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $deliveries - list of delivery ids
	 * @return array - db record as array.
	 */
	public function getReportRecords( $deliveries ) {

		$res = array();
		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ) );
		$this->db->from( 'deliveries' );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );

		$this->db->where( array( 'deliveries.hidden' => 0, 'deliveries.deleted' => 0 ) );
		$this->db->where_in( 'deliveries.id', $deliveries );
		$this->db->group_by( 'deliveries.id, ' );
		$this->db->order_by( 'deliveries.destination' );
		$query = $this->db->get();

		$sortedDeliveries = array();
		foreach ( $query->result_array() as $row ) {
			$sortedDeliveries[$row['deliveries_destination']]['deliveries'][] = $row;
		}

		foreach ( $sortedDeliveries as $key=>$val ) {
			$unitQuantityTotal 			= 0;
			$caseQuantityTotal 			= 0;
			$unitQuantityRemainTotal 	= 0;
			$caseQuantityRemainTotal 	= 0;
			$volumeTotal 				= 0;
			$grossWeightTotal 			= 0;
			foreach( $val['deliveries'] as $delivery ) {
				$delivery['deliveries_volume'] 	= sprintf($this->config->item('doublevalues_format'), $delivery['deliveries_volume']);

				$unitQuantityTotal 		+= $delivery['deliveries_unit_quantity'];
				$caseQuantityTotal 		+= $delivery['deliveries_case_quantity'];
				$unitQuantityRemainTotal += $delivery['deliveries_unit_quantity_remain'];
				$caseQuantityRemainTotal += $delivery['deliveries_case_quantity_remain'];
				$volumeTotal 			+= $delivery['deliveries_volume'];
				$grossWeightTotal 		+= $delivery['deliveries_gross_weight'];
				$res[$key]['deliveries'][] = $delivery;
			}

			$res[$key]['unit_quantity_total'] 	= $unitQuantityTotal;
			$res[$key]['case_quantity_total'] 	= $caseQuantityTotal;
			$res[$key]['unit_quantity_remain_total'] 	= $unitQuantityRemainTotal;
			$res[$key]['case_quantity_remain_total'] 	= $caseQuantityRemainTotal;
			$res[$key]['volume_total'] 			= sprintf($this->config->item('doublevalues_format'), $volumeTotal);
			$res[$key]['gross_weight_total'] 	= $grossWeightTotal;
		}

		return $res;

//		foreach ( $query->result_array() as $row ) {
//			$s = array();
//			$s['destination_port'] 		= '';
//			$s['container_size'] 		= '';
//			$s['container_type'] 		= '';
//			$s['deliveries']			= array();
//
//			$unitQuantityTotal 			= 0;
//			$volumeTotal 				= 0;
//			$grossWeightTotal 			= 0;
//
//			$deliveries = $this->getDeliveriesByShipmentId( $row['shipments_id'] );
//
//			foreach( $deliveries as $delivery ) {
//				$d = array();
//				$d['contract_number'] 	= $delivery['deliveries_contract_number'];
//				$d['product_code'] 		= $delivery['deliveries_product_code'];
//				$d['dc'] 				= $delivery['deliveries_dc'];
//				$d['unit_quantity'] 	= $delivery['deliveries_unit_quantity'];
//				$d['volume'] 			= sprintf($this->config->item('doublevalues_format'), $delivery['deliveries_volume']);
//				$d['gross_weight'] 		= $delivery['deliveries_gross_weight'];
//				$s['deliveries'][]		= $d;
//
//				$unitQuantityTotal 		+= $delivery['deliveries_unit_quantity'];
//				$volumeTotal 			+= $delivery['deliveries_volume'];
//				$grossWeightTotal 		+= $delivery['deliveries_gross_weight'];
//			}
//
//			$s['unit_quantity_total'] 	= $unitQuantityTotal;
//			$s['volume_total'] 			= sprintf($this->config->item('doublevalues_format'), $volumeTotal);
//			$s['gross_weight_total'] 	= $grossWeightTotal;
//
//			$res[$row['contracts_departure']][] = $s;
//		}
//		return $res;
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	private function prepareResultQueryForOverviewOutput( $query_result_array ){
		$res = array();
		foreach ( $query_result_array as $row) {
			$row['fob_date'] = ( $row['fob_date'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['fob_date'] ) ) : "";
			$row['aldi_au_wh_delivery_due_date'] = ( $row['aldi_au_wh_delivery_due_date'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['aldi_au_wh_delivery_due_date'] ) ) : "";
			$res[] = $row;
		}
		return $res;
	}
}

/* End of file deliveries.php */
/* Location: ./app/models/deliveries.php */
