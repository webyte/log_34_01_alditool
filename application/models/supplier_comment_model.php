<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Supplier_comment_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'supplier_comments';
	}

	public function createNew( $insertData ) {
		$insertData['user_id'] = $this->session->userdata('userId');
    	return parent::createNew( $insertData );
    }
    
    public function getCommentsByPid( $pid ) {
    	$this->db->select('supplier_comments.*, users.username, supplier_comments.id AS id');
		$this->db->from( $this->tablename );
		$this->db->join('users', 'supplier_comments.user_id = users.id', 'left');
		$this->db->where( array( $this->tablename.'.pid' => $pid, $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$this->db->order_by( $this->tablename.'.crdate ASC');
		$query = $this->db->get();
    	$comments = $query->result_array();
    	return $comments;
    }
}

/* End of file comment_model.php */
/* Location: ./app/models/comment_model.php */
