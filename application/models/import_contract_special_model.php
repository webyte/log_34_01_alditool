<?php
require_once( APPPATH.'models/abstract_import_model'.EXT );

class Import_contract_special_model extends Abstract_import_model {

	public static $CONTRACT_TYPE_SPECIAL_BUYS = 3;

	function __construct() {
		parent::__construct();
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->helper( 'str_helper' );
		$this->lockSuffix = '_CONTRACT_SPECIAL';
	}

	/**
	 * setImportSubPath
	 * @param $path
	 * @return
	 */
	public function setImportSubPath( $path ) {
		$this->importSubPath = $path;
	}

	/**
	 * setContractType
	 * @param $contractType
	 * @return
	 */
	public function setContractType( $contractType ) {
		$this->contractType = $contractType;
	}

	/**
	 * readEventFile
	 * @param $file
	 * @return boolean
	 */
	public function readEventFile( $file ) {
		$this->welogger->log( "readEventFile called with file ".$file, WELogger::$LOG_LEVEL_INFO, "Import_contract_special_model.readEventFile" );
		$this->log('readEventFile called with file '.$file);

		$success	= TRUE;
		$path 		= $this->config->item('import_path').$this->importSubPath;

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$src 			= $path.$file;

		$objReader = IOFactory::createReader('CSV')->setDelimiter(',');
		$objPHPExcel = $objReader->load($src);
		$objWorksheet 	= $objPHPExcel->getActiveSheet();
		$contracts 		= $this->getContracts( $objWorksheet );
		$this->saveToDB( $contracts );
		$this->sendFilePerMail( $path.$file);

		// move file to archive
		if( ENVIRONMENT == 'production' || ENVIRONMENT == 'staging' ) $this->moveFileToArchive( $file );
		return $success;
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	/**
	 * getRegularContracts
	 * @param $objWorksheet
	 * @return array
	 */
	private function getContracts( $objWorksheet ) {

		$this->load->model('Delivery_due_model', 'delivery_due_model');
		$contracts	= array();
		$i			= 1;

		foreach ($objWorksheet->getRowIterator() as $row) {
			if( $i > 1 ) {
				$contractNumber = (string) $objWorksheet->getCellByColumnAndRow(31, $i)->getValue();
				if( !empty( $contractNumber ) ) {
					// Mapping
					// 1	Supplier (Contract Header)
					// 11	Product Code (Contract Information)
					// 12	Product Description (Contract Information)
					// 14	Departure Port (Contract Header)
					// 20	Incoterm Code (Contract Header)
					// 27	neues Feld "ShipmentType" (Contract Ebene)
					// 31	Contract Number (eindeutige Referenz)
					// 32	Case length (Contract information)
					// 33	Case width (Contract information)
					// 34	Case height (Contract information)
					// 36	Case weight (Contract information)
					// 40	Cargo Ready Date (Dynamic) (Contract Header)
					// 41	On sale date (Contract Header)
					// 45	neues Feld "Contact Details" (Contract Header)
					// 47	Traffic Type (Contract Header)
					$supplierName	= (string) $objWorksheet->getCellByColumnAndRow(1, $i)->getValue();
					$supplier 		= $this->getSupplierId( $supplierName );
					$tt				= (string) $objWorksheet->getCellByColumnAndRow(47, $i)->getValue();
					$trafficType	= $this->getTrafficType( $tt );

					$caseLength		= (float) $objWorksheet->getCellByColumnAndRow(32, $i)->getValue();
					$caseWidth 		= (float) $objWorksheet->getCellByColumnAndRow(33, $i)->getValue();
					$caseHeight		= (float) $objWorksheet->getCellByColumnAndRow(34, $i)->getValue();
					$weight			= (float) $objWorksheet->getCellByColumnAndRow(36, $i)->getValue();

					$orderedQuantity = (float) $objWorksheet->getCellByColumnAndRow(22, $i)->getCalculatedValue();
					$orderedQuantity2 = (float) $objWorksheet->getCellByColumnAndRow(24, $i)->getCalculatedValue();
					$caseQuantity1 	= $orderedQuantity2;
					$unitsPerCase	= $orderedQuantity / $orderedQuantity2;
					$caseVolume		= ( $caseLength / 1000 ) * ( $caseWidth / 1000 ) * ( $caseHeight / 1000 ); // convert millimeter in meter
					$caseWeight		= $weight / $orderedQuantity2;
					$productCode 		= (string) $objWorksheet->getCellByColumnAndRow(11, $i)->getValue();
					$productDescription = (string) $objWorksheet->getCellByColumnAndRow(12, $i)->getValue();
					$dc				= substr( (string) $objWorksheet->getCellByColumnAndRow(6, $i)->getValue(), -3 );
					$advertisementWeek 	= date( 'W', strtotime( $this->formatDate( $objWorksheet->getCellByColumnAndRow(41, $i)->getValue() ) ) );
					$advertisementYear 	= date( 'Y', strtotime( $this->formatDate( $objWorksheet->getCellByColumnAndRow(41, $i)->getValue() ) ) );
					$deliveryDueDate = $this->delivery_due_model->getDeliveryDueDate( intval( $advertisementWeek ), $advertisementYear );
					$deliveryDueEndDate = $this->delivery_due_model->getDeliveryDueDate( intval( $advertisementWeek ), $advertisementYear, true );
					$nearestPort		= (string) $objWorksheet->getCellByColumnAndRow(14, $i)->getValue();
					$loo				= $this->getLogwinOriginOffice( $nearestPort );
					$incotermId 		= $this->getIncotermId( (string) $objWorksheet->getCellByColumnAndRow(20, $i)->getValue() );

					if( !isset( $contracts[$contractNumber] ) ) {
						$contracts[$contractNumber] = array(
							'contract_type'			=> $this->contractType,
							'contract_number' 		=> $contractNumber,
							'supplier' 				=> $supplier,
							'supplier_name'			=> $supplierName,
							'units_per_case' 		=> $unitsPerCase,
							'product_code' 			=> $productCode,
							'product_description' 	=> $productDescription,
							'departure' 			=> (string) $objWorksheet->getCellByColumnAndRow(14, $i)->getValue(),
							'incoterm_code'			=> $incotermId,
							'shipment_type'			=> (string) $objWorksheet->getCellByColumnAndRow(27, $i)->getValue(),
							'case_length' 			=> (string) $objWorksheet->getCellByColumnAndRow(32, $i)->getValue(),
							'case_width' 			=> (string) $objWorksheet->getCellByColumnAndRow(33, $i)->getValue(),
							'case_height' 			=> (string) $objWorksheet->getCellByColumnAndRow(34, $i)->getValue(),
							'case_weight' 			=> $caseWeight,
							'case_cubage'			=> $caseVolume,
							'fob_date' 				=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(40, $i)->getValue() ),
							'initial_crd' 			=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(40, $i)->getValue() ),
							'on_sale_date' 			=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(41, $i)->getValue() ),
							'initial_sales_date'	=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(41, $i)->getValue() ),
							'advertisement_week' 	=> $advertisementWeek,
							'advertisement_year' 	=> $advertisementYear,
							'ofu_scheduled' 		=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(40, $i)->getValue(), ' -21 days' ),
							'contact_details' 		=> (string) $objWorksheet->getCellByColumnAndRow(45, $i)->getValue(),
							'traffic_type' 			=> $trafficType,
							'aldi_au_wh_delivery_due_date' => $deliveryDueDate,
							'aldi_au_wh_delivery_due_end_date' => $deliveryDueEndDate,
							'logwin_origin_office' 	=> $loo,
						);
					}

					// Mapping
					// 0	neues Feld "Shipment Number" (Delivery)
					// 6	neues Feld "ShiptoSite" (Delivery)
					// 7	neues Feld "OrderNumber" (Delivery)
					// 17	Destination (Delivery)
					// 22	Unit Quantity (Delivery)
					// 24	Case Quantity (Delivery)

					$contracts[$contractNumber]['deliveries'][] = array(
						'shipment_number' 	=> (string) $objWorksheet->getCellByColumnAndRow(0, $i)->getValue(),
						'shiptosite' 		=> (string) $objWorksheet->getCellByColumnAndRow(6, $i)->getValue(),
						'order_number' 		=> (string) $objWorksheet->getCellByColumnAndRow(7, $i)->getValue(),
						'destination' 		=> (string) $objWorksheet->getCellByColumnAndRow(17, $i)->getValue(),
						'contract_number' 	=> $contractNumber,
						'product_code' 		=> $productCode,
						'product_description' => $productDescription,
						'dc'				=> $dc,
						'case_quantity' 		=> $caseQuantity1,
						'unit_quantity' 		=> $caseQuantity1 * $unitsPerCase,
						'case_quantity_remain' 	=> $caseQuantity1,
						'unit_quantity_remain' 	=> $caseQuantity1 * $unitsPerCase,
					);
				}
			}
			$i++;
		}
		return $contracts;
	}

	/**
	 * saveToDB
	 * @param
	 * @return array
	 */
	private function saveToDB( $data ){

		foreach( $data as $value ) {
			// save contract
			$contractId = $this->contract_model->getIdByContractNumber( $value['contract_number'] );
			$isUpdate = ( $contractId > 0 ) ? true : false;
			$previousRow = ( $contractId > 0 ) ? $this->contract_model->getRecord( $contractId ) : array();

			$contract = $value;
			$supplierName = $contract['supplier_name'];
			unset( $contract['deliveries'] );
			unset( $contract['supplier_name'] );

			if( $isUpdate ) {
				if( $previousRow['initial_crd'] != '0000-00-00 00:00:00' ) unset( $contract['initial_crd'] );
				if( $previousRow['initial_sales_date'] != '0000-00-00 00:00:00' ) unset( $contract['initial_sales_date'] );
                if( $previousRow['cube_qa_check'] == '1' ) {
                    unset( $contract['case_length'] );
                    unset( $contract['case_width'] );
                    unset( $contract['case_height'] );
                    unset( $contract['case_weight'] );
                }
            }

			if( $contractId > 0 ) {
				$this->contract_model->updateRecord( $contractId, $contract );
			} else {
				$contractId = $this->contract_model->createNew( $contract );
			}

			// send supplier missing mail
			if( $contract['supplier'] == '' ) $this->sendSupplierMissingMail( $supplierName, $contract['contract_number'] );

			// send update mail
			//if( $isUpdate ) $this->sendUpdateMail( $contract, $contractId );

			// send inform mails
			if( !$isUpdate ) $this->sendInformMails( $contract, $contractId );

			// save deliveries
			$totalCases = 0;
			$deliveries = $value['deliveries'];
			foreach( $deliveries as $delivery ) {
				$deliveryId = $this->delivery_model->getIdByDCAndContractId( $delivery['dc'], $contractId );
				$delivery['contract_id'] = $contractId;
				if( $deliveryId > 0 ) {
					$this->delivery_model->updateRecord( $deliveryId, $delivery );
				} else {
					$deliveryId = $this->delivery_model->createNew( $delivery );
				}

				$totalCases += $delivery['case_quantity'];
			}

			// update total_cases volume and gross_weight
			$volume 		= $value['case_cubage'] * $totalCases;
			$grossWeight 	= $value['case_weight'] * $totalCases;
			$tmp = array(
				'total_cases' 	=> $totalCases,
				'volume' 		=> $volume,
				'gross_weight' 	=> $grossWeight,
			);
			$this->contract_model->updateRecord( $contractId, $tmp );
		}
	}

	/**
	 * formatDate
	 * @param $date
	 * @return string
	 */
	private function formatDate( $date, $offset='' ) {
		if( !empty( $date ) && stristr( $date, '.' ) === FALSE ) {
			$date = str_replace('/', '.', $date);
			$date = substr( $date, 0, 10);
			$out = date( 'Y-m-d H:i:s', strtotime( date( 'Y-m-d H:i:s', strtotime( $date ) ).$offset ) );
		} else {
			$out = '0000-00-00 00:00:00';
		}
		return $out;
	}

	/**
	 * getSupplierId
	 * @param string
	 * @return string
	 */
	private function getSupplierId( $supplierName ){
		$this->load->model('Supplier_model', 'supplier_model');
		$out = '';
		$suppliers = $this->supplier_model->getRecordsWhere( array( 'name' => $supplierName, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $suppliers ) > 0 ) {
			$out = $suppliers[0]['id'];
		}
		return $out;
	}

	/**
	 * getTrafficType
	 * @param $str
	 * @return string
	 */
	private function getTrafficType( $str )
	{
		switch( $str ) {
			case '':
				$out = 5; // Factory/CFS
				break;
			case 'CY/CY':
				$out = 1; // Factory
				break;
			default:
				$out = 2; // CFS
		}
		return $out;
	}

	/**
	 * sendSupplierMissingMail
	 * @return boolean
	 */
	private function sendSupplierMissingMail( $supplierName, $contractNumber ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= ( ENVIRONMENT == 'production' ) ? array('ALDICSV@logwin-logistics.com','hkg.aldict@logwin-logistics.com','stefanie.stahl@logwin-logistics.com') : array('stefanie.stahl@logwin-logistics.com');
		$cc			= ( ENVIRONMENT == 'production' ) ? '' : array('or@webyte.org');
		$subject 	= '[CONTRACT_NUMBER] - New supplier';
		$body 		= 'Dear User,

Please note that we received a new supplier for contract [CONTRACT_NUMBER]

Supplier Name: [SUPPLIER_NAME]

Please register the supplier in the Aldi tool.
After that please select the supplier in the contract and sent out the pre-alert.';

		$mailData['SUPPLIER_NAME'] = $supplierName;
		$mailData['CONTRACT_NUMBER'] = $contractNumber;
		$subject = replacePattern( $subject, $mailData );
		$body = replacePattern( $body, $mailData );

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		$this->email->cc( $cc );

		$sendSuccess = $this->email->send();
		$this->email->clear( true );

		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Import_contract_special_model.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Import_contract_special_model.sendMail' );
			return true;
		}
	}

	/**
	 * sendFilePerMail
	 * @return boolean
	 */
	private function sendFilePerMail( $file ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= ( ENVIRONMENT == 'production' ) ? array('ALDICSV@logwin-logistics.com','hkg.aldict@logwin-logistics.com','stefanie.stahl@logwin-logistics.com') : array('stefanie.stahl@logwin-logistics.com');
		$cc			= ( ENVIRONMENT == 'production' ) ? '' : array('or@webyte.org');
		$subject 	= 'Special Buys Contracts Uploaded';
		$body 		= 'Please find attached a list of contracts that have been just uploaded to the Aldi tool.';

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		$this->email->attach( $file );
		$this->email->cc( $cc );

		$sendSuccess = $this->email->send();
		$this->email->clear( true );

		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Import_contract_special_model.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Import_contract_special_model.sendMail' );
			return true;
		}
	}

	/**
	 * sendUpdateMail
	 * @param $contract
	 * @return
	 */
	private function sendUpdateMail( $contract, $contractId ) {
		// send update mail
		if( !empty( $contract['logwin_origin_office'] ) ) {
			$this->load->model('Office_model', 'office_model');
			$office 			= $this->office_model->getRecord( $contract['logwin_origin_office'] );
			$data['subject'] 	= 'Update contract '.$contract['contract_number'].' in ALDI Tool';
			$data['body'] = 'Dear colleagues,

Please note that we received an update for contract '.$contract['contract_number'].' in the ALDI tool.
Please log in to the Aldi tool for further details.

This is an automatic generated e-mail. Please do not reply!';
			$data['recipient'] 	= ( ENVIRONMENT == 'production' ) ? $office['contact_mail'] : array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
			$this->doSendMail( $data );
		}
	}

	/**
	 * sendInformMails
	 * @param $contract
	 * @return
	 */
	private function sendInformMails( $contract, $contractId ) {
		// send logwin origin office mail missing
		if( empty( $contract['logwin_origin_office'] ) ) {
			$data['subject'] = 'Export office missing for contract '.$contract['contract_number'];
			$data['body'] = 'Dear User,

Please note that for contract '.$contract['contract_number'].' no Logwin export office could have been assigned.
Therefore no e-mail was sent out.

Please enter the correct export office manually in the Aldi tool and trigger the e-mail.
Please sent the departure port and the corresponding export branch to lots@logwin-logistics.com, so that the export office will be correctly assigned in the future.';
			$data['recipient'] 	= ( ENVIRONMENT == 'production' ) ? array( 'ALDICSV@logwin-logistics.com', 'stefanie.stahl@logwin-logistics.com' ) : array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
			$this->doSendMail( $data );
		}

		// send supplier mail
		if( !empty( $contract['supplier'] ) ) {
			$this->load->model('Contactperson_model', 'contactperson_model');
			$this->load->model('Office_model', 'office_model');
			$recipients = $this->contactperson_model->getDefaultEMailsForSupplier( $contract['supplier'] );
			$office 	= $this->office_model->getRecord( $contract['logwin_origin_office'] );
			$looMail 	= $office['contact_mail'];
			$data = $this->getSupplierInformMailData( $contract );
			$data['cc'] 		= ( ENVIRONMENT == 'production' ) ? $looMail : '';
			$data['recipient'] 	= ( ENVIRONMENT == 'production' ) ? $recipients : array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
			$this->doSendMail( $data );
			$this->contract_model->updateRecord( $contractId, array( 'supplier_mail_sent' => 1 ) );
		}
	}

	/**
	 * getLooInformMailData
	 * @param $recorddata
	 * @return array
	 */
	private function getLooInformMailData( $recorddata ) {
		$mailData['CONTRACT_NUMBER'] 				= $recorddata['contract_number'];
		$mailData['ADVERTISEMENT_WEEK'] 			= $recorddata['advertisement_week'];
		$mailData['ALDI_AU_WH_DELIVERY_DUE_DATE'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['aldi_au_wh_delivery_due_date'] ) );
		$mailData['ALDI_AU_WH_DELIVERY_DUE_DATE_MINUS_SEVEN_DAYS'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['aldi_au_wh_delivery_due_date'].' -7 Days' ) );
		$mailData['ALDI_AU_WH_DELIVERY_DUE_DATE_MINUS_TEN_DAYS'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['aldi_au_wh_delivery_due_date'].' -10 Days' ) );
		$subject 					= replacePattern( $this->lang->line( 'loo.inform.mail.subject' ), $mailData );
		if($recorddata['contract_type'] == Contract_model::$CONTRACT_TYPE_CORE_RANGE ){
			$body						= replacePattern( $this->lang->line( 'loo.inform.mail.body_core_range' ), $mailData );
		}else{
			$body						= replacePattern( $this->lang->line( 'loo.inform.mail.body' ), $mailData );
		}
		return array( 'subject' => $subject, 'body' => $body );
	}

	/**
	 * @param $recorddata
	 * @return array
	 */
	private function getSupplierInformMailData( $recorddata ) {
		$loo                        = $this->getLooAddress( $recorddata['logwin_origin_office'] );
		$mailData['RECIPIENT']		= 'User';
		$mailData['FOB_DATE'] 		= date( $this->config->item('date_format'), strtotime( $recorddata['fob_date'] ) );
		$mailData['OFU_SCHEDULED'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['ofu_scheduled'] ) );
		$mailData['DEPARTURE'] 		= $recorddata['departure'];
		$mailData['LOO_NAME'] 		= $loo['name'];
		$mailData['LOO_STREET'] 	= $loo['street'];
		$mailData['LOO_CITY'] 	    = $loo['city'];
		$mailData['LOO_PHONE'] 	    = $loo['contact_phone'];
		$mailData['LOO_MAIL'] 	    = $loo['contact_mail'];
		$mailData['INFO_DATE']      = date( 'd/m/Y', strtotime( date( 'Y-m-d H:i:s', strtotime( $recorddata['fob_date'] ) ).' -35 days' ) );
		$mailData['CONTACT_NAME'] 	= $this->getSupplierContactName( $recorddata['supplier'] );
		$mailData['CONTRACT_NUMBER']= $recorddata['contract_number'];
		$mailData['DESCRIPTION'] 	= $recorddata['product_description'];
		$subject 					= replacePattern( $this->lang->line( 'supplier.inform.mail.subject' ), $mailData );
		$body						= replacePattern( $this->lang->line( 'supplier.inform.mail.body' ), $mailData );
		return array( 'subject' => $subject, 'body' => $body );
	}

	private function getLooAddress( $id = 0 ) {
		$this->load->model('Office_model', 'office_model');
		$record = $this->office_model->getRecord( $id );
		return $record;
	}

	private function getSupplierContactName( $id = 0 ) {
		$out = '';
		$this->load->model('Supplier_model', 'supplier_model');
		$record = $this->supplier_model->getRecord( $id );
		if( count( $record ) > 0 ) $out = $record['contact_name'];
		return $out;
	}

	/**
	 * getLogwinOriginOffice
	 * @param $port
	 * @return string
	 */
	private function getLogwinOriginOffice( $port )
	{
		$out = '';
		if( empty( $port ) ) return $out;
		$this->load->model('Office_model', 'office_model');
		$offices = $this->office_model->getRecordsWhere(array('ports LIKE' => '%' . $port . '%', 'hidden' => 0, 'deleted' => 0));
		if (count($offices) > 0) {
			$out = $offices[0]['id'];
		}
		return $out;
	}

	/**
	 * getIncotermId
	 * returns incoterm id
	 *
	 * @param string $code
	 * @return int
	 */
	private function getIncotermId( $code='' ) {
		$tcaNode = $this->tca->getColumnNodeById( 'shipments_incoterm_code' );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		$key = array_search( $code, $options );
		return $key!==false ? $key : 0;
	}
	/**
	 * doSendMail
	 * @return boolean
	 */
	private function doSendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$cc			= isset( $data['cc'] ) ? $data['cc'] : '';
		$subject 	= $data['subject'];
		$body 		= nl2br( $data['body'] );

		$this->email->set_mailtype( 'html' );
		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->cc( $cc );
		$this->email->subject( $subject );
		$this->email->message( $body );

		$sendSuccess = $this->email->send();
		$this->email->clear( true );
		if( !$sendSuccess ) {
			$this->welogger->log( "Sending Mail to ".$toMail." failed, error: ".$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, "Import_contract_special_model.doSendMail" );
			return false;
		} else {
			$this->welogger->log( "Mail was sent to ".$toMail." \nwith subject: ".$subject." \nand message: ".$body, WELogger::$LOG_LEVEL_INFO, "Import_contract_special_model.doSendMail" );
			return true;
		}
	}
}
/* End of file import_model.php */
/* Location: ./app/models/import_model.php */
