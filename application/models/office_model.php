<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Office_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'offices';
	}

}

/* End of file office.php */
/* Location: ./app/models/office.php */
