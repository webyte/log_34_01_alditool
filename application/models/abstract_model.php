<?php
require_once( APPPATH.'models/abstract_aldi_model'.EXT );

class Abstract_model extends Abstract_aldi_model {
	protected $tablename;
	private $sqlite;
	
    function __construct() {
        parent::__construct();
        $this->load->library('tca');
        $this->load->library('session');
		$this->initSQLite();
    }

    /**
     * createNew
     * creates new record in table with the given insertdata
     * retrns the id of the new entry or -1 if something went wrong
     * @param array $insertData - associative array with data to insert into new table row
     * @return int new table-row id
     */
    function createNew( $insertData, $saveHistory=TRUE ) {
    	if( $this->isValidData($insertData) ){
			$insertData['modified'] = date( 'Y-m-d H:i:s' );
    		$this->db->insert($this->tablename, $insertData);
			$id = $this->db->insert_id();
			// save full version to history
			if( $saveHistory ) $this->saveRecordVersion( $id );
    		$this->clearDbCache();
    		return $id;
    	} else {
    		return -1;
    	}
    }

    /**
     * updateRecord
     * updates the record with the given id
     * 
     * @param int $id - row id
     * @param array $insertData - updated data
     */
    function updateRecord($id, $insertData, $saveHistory=TRUE ) {
    	if( $this->isValidData($insertData) ) {
    		if( $saveHistory ) $this->saveRecordVersion( $id );
			$insertData['modified'] = date( 'Y-m-d H:i:s' );
    		$this->db->update( $this->tablename, $insertData, array( 'id' => $id ) );
    		$this->clearDbCache();
    	} 
    }

    /**
     * getRecord
     * returns the db-record with the given id as array
     * 
     * @param int $id - id of record to retrieve
     * @return array - db record as array.
     */
    function getRecord( $id ) {
   		$query = $this->db->get_where( $this->tablename, array( 'id' => $id, 'hidden' => 0, 'deleted' => 0 ) );
   		return $query->first_row('array');
    }

	/**
	 * getAllRecords
	 * returns all records of this table
	 *
	 * @param boolean $includeDeleted - also return deleted/hidden records? (default is false)
	 * @return array found records
	 **/
	function getAllRecords( $includeDeleted = FALSE ) {
		if( $includeDeleted ) $this->db->where( array('deleted' => 0, 'hidden' => 0) );
		$query = $this->db->get($this->tablename);
		return $query->result_array();
	}


    /**
     * getRecordsWhere
     * returns the db-record
     * @param $where_arr
     * @param string $orderby
     * @return mixed
     */
    function getRecordsWhere( $where_arr, $orderby = '' ) {
    	$this->db->where( $where_arr );
    	if( $orderby != '' ) $this->db->order_by( $orderby );
    	$query = $this->db->get( $this->tablename );

   		return $query->result_array();
    }

    /**
     * deleteRecord
     * deletes record with given id from table
     * 
     * @param int $id - row id
     * 
     */
    function deleteRecord( $id ) {
    	$this->updateRecord($id, array( 'deleted' => 1 ) );
		//$this->db->update( $this->tablename, array( 'deleted' => 1 ), array( 'id' => $id ) );
   		//$this->db->delete($this->tablename, array('id' => $id));
   		//$this->clearDbCache();
    }

	/**
	 * saveRecordVersion
	 * saves actual record to history
	 *
	 * @param int $id - row id
	 * @param boolean $empty - if set an empty record is saved
	 *
	 */
	function saveRecordVersion( $id, $empty=FALSE ) {
		$original_data 		= $this->getRecord( $id );
		$field_identifier 	= $this->tablename.'_'.$id;
		$insertData 		= array(
			'record_identifier_hash' 	=> $field_identifier,
			'record_data' 				=> json_encode( $original_data ),
			'crdate' 					=> date('Y-m-d H:i:s', strtotime('now')),
			'version' 					=> 0,
			'user_id' 					=> $this->session->userdata('userId')
		);

		if( !$this->isSQLiteFileValid() ) return;

		try{
			$this->sqlite->insert('recorddata_history', $insertData);
		} catch (Exception $e) {
			$this->welogger->log( "saveRecordVersion Exception", WELogger::$LOG_LEVEL_WARNING, "Abstract_model.saveRecordVersion" );
			$data['subject'] 	= 'Logwin Aldi Tool - saveRecordVersion exception';
			$data['body'] 		= 'Logwin Aldi Tool saveRecordVersion exception: '.$e->getMessage();
			$data['recipients'] = 'or@webyte.org';
			$this->sendMail( $data );
			//show_error('can not write to log db', $status_code = 500);
		}
	}

	/**
	 * initSQLite
	 * opens a SQLite DB connection
	 *
	 *
	 */
	function initSQLite() {
		$sqLitePath = $this->config->item('sqlite_path');
		$sqLiteFile = $sqLitePath.'record_histories_'.ENVIRONMENT.'_'.date('Y-m').'.sqlite';

        if( !file_exists( $sqLiteFile ) ) {
            copy( $sqLitePath.'record_histories.sqlite', $sqLiteFile );
            chmod ( $sqLiteFile, 0777);
        }

		if( !$this->isSQLiteFileValid() ) return;

        $config 			= array();
		$config['hostname'] = 'sqlite:'.$sqLiteFile;
		$config['username'] = '';
		$config['password'] = '';
		$config['database'] = '';
		$config['dbdriver'] = 'pdo';
		$config['dbprefix'] = '';
		$config['pconnect'] = TRUE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = '';
		$config['char_set'] = 'utf8';
		$config['dbcollat'] = 'utf8_general_ci';
		$config['swap_pre'] = '';
		$config['autoinit'] = TRUE;
		$config['stricton'] = FALSE;

		$this->sqlite = $this->load->database($config, TRUE);

	}

	private function isSQLiteFileValid() {
		$sqLitePath = $this->config->item('sqlite_path');
		$sqLiteFile = $sqLitePath.'record_histories_'.ENVIRONMENT.'_'.date('Y-m').'.sqlite';

		// check if source file does exist
		if( !file_exists( $sqLitePath.'record_histories.sqlite' ) ) {
			return false;
		}

		// check if latest file is bigger than 1kb
		if( filesize( $sqLiteFile ) <= 1000 ) {
			return false;
		}

		return true;
	}

	/**
     * isValidData
     * generic method for validating data before inserting / updating in db
     * 
     * @param array $data - the date to insert/update
     * @return boolean isValid
     */
    protected function isValidData($data){
    	return is_array( $data );
    }

    protected function clearDbCache(){
    	$this->db->cache_delete_all();
    }

	/**
	 * sendMail
	 * @param $bookingId
	 * @return boolean
	 */
	private function sendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		$fromMail 					= $this->config->item('from_mail');
		$fromName 					= $this->config->item('from_name' );
		$subject 					= $data['subject'];
		$mailText 					= $data['body'];
		$recipient 					= $data['recipients'];
		$body						= $mailText;

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $recipient );
		$this->email->subject( $subject );
		$this->email->message( $body );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( "Sending Mail to ".$recipient." failed, error: ".$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, "Abstract_model.sendMail" );
			return false;
		} else {
			$this->welogger->log( "Mail was sent to ".$recipient." \nwith subject: ".$subject." \nand message: ".$body, WELogger::$LOG_LEVEL_INFO, "Abstract_model.sendMail" );
			return true;
		}
		$this->email->clear(TRUE);
	}
}

/* End of file abstract_model.php */
/* Location: ./app/models/abstract_model.php */
