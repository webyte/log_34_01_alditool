<?php
require_once( APPPATH.'models/abstract_model.php' );

class Dashboard_destination_management_model extends Abstract_model {

	public static $STATUS_DELIVERY_DELAY 			= 'Delay';
	public static $STATUS_DELIVERY_POTENTIAL_DELAY 	= 'Potential delay';
	public static $STATUS_DELIVERY_ON_TIME 			= 'On time';

	function __construct() {
		parent::__construct();
		$this->load->model('Supplier_model', 'supplier_model');
		$this->load->driver('cache', array('adapter' => 'file'));
	}

	/**
	 * getRecords
	 * returns the db-records with the given search params
	 *
	 * @return array - db record as array.
	 */
	public function getRecords( $filter=array() ) {
		if( !$res = $this->cache->get('dashboard_destination_management_getrecords') ) {
			$res = array();
			$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'contracts' ).', '.$this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
			$this->db->from( 'contracts' );
			$this->db->join( 'deliveries', 'deliveries.contract_id = contracts.id AND deliveries.hidden = 0 AND deliveries.deleted = 0' );
			$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
			$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );

			$this->db->where( array( 'contracts.hidden' => 0, 'contracts.deleted' => 0 ) );
			$this->db->where( array( 'contracts.aldi_au_wh_delivery_due_date >=' => date( 'Y-m-d', strtotime( '-20 days', time() ) ).' 00:00:00' ) );
			$this->db->where( array( 'contracts.aldi_au_wh_delivery_due_date <=' => date( 'Y-m-d', strtotime( '+10 days', time() ) ).' 00:00:00' ) );
			$this->db->where( array( 'deliveries.case_quantity !=' =>  0 ) );
			$this->db->group_by( 'deliveries.id' );
			$this->db->order_by( 'contracts.id' );
			$query = $this->db->get();

			foreach ( $query->result_array() as $row ) {
				$res[] = $row;
			}
			$res = $this->prepareResultQueryForOutput( $res );
		}
		if( count( $filter ) > 0 ) $res = $this->filterResult( $res, $filter );
		return $res;
	}

	/**
	 * getRecordsByStatusDelivery
	 * returns the db-records with the given search params
	 *
	 * @return array - db record as array.
	 */
	public function getRecordsByStatusDelivery() {
		$rows = $this->getRecords();
		$delayedRows = array();
		foreach( $rows as $row ) {
			$delayedRows[$row['status_delivery']] = ( isset( $delayedRows[$row['status_delivery']] ) ) ? intval($delayedRows[$row['status_delivery']]) + 1 : 1;
		}

		return array(
			'labels' => array(
				$this::$STATUS_DELIVERY_ON_TIME,
				$this::$STATUS_DELIVERY_POTENTIAL_DELAY,
				$this::$STATUS_DELIVERY_DELAY
			),
			'data' => array(
				isset( $delayedRows[$this::$STATUS_DELIVERY_ON_TIME] ) ? $delayedRows[$this::$STATUS_DELIVERY_ON_TIME] : array(),
				isset( $delayedRows[$this::$STATUS_DELIVERY_POTENTIAL_DELAY] ) ? $delayedRows[$this::$STATUS_DELIVERY_POTENTIAL_DELAY] : array(),
				isset( $delayedRows[$this::$STATUS_DELIVERY_DELAY] ) ? $delayedRows[$this::$STATUS_DELIVERY_DELAY] : array(),
			),
		);
	}

	/**
	 * generateCache
	 *
	 * @return
	 */
	public function generateCache() {
		$res = $this->getRecords();
		$this->cache->file->save('dashboard_destination_management_getrecords', $res, 600);
	}

	/**
	 * isCached
	 *
	 * @return
	 */
	public function isCached() {
		return file_exists( $this->config->item('app_path').'cache/dashboard_ocean_freight_getrecords' );
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function prepareResultQueryForOutput( $query_result_array ){
		$res = array();

		foreach ( $query_result_array as $row) {

			$row['status_delivery'] = '';
			$row['status_delivery_css'] = '';
			$deliveryWindow = strtotime( $row['contracts_aldi_au_wh_delivery_due_date'] );
			$ata = strtotime( $row['shipments_ata'] );
			$sec = 24 * 60 * 60;
			$deliveryWindowMinus3Days = date( 'U', strtotime( '-3 days', $deliveryWindow ) );

//			in case ATA is not populated:
//			If Delivery window Start > today + 5, green,
//			If Delivery window Start >= today + 3 und Delivery window Start <= today + 5, orange
//			If Delivery window Start < today + 3, rot
//
//			in case ATA is populated:
//			ATA AFTER deliveryWindow -3 days, RED else GREEN


			if( $row['shipments_ata'] == '0000-00-00 00:00:00' || empty( $row['shipments_ata'] ) ) {
				if( $deliveryWindow > time() + 5 * $sec ) {
					$row['status_delivery'] = $this::$STATUS_DELIVERY_ON_TIME;
					$row['status_delivery_css'] = 'label label-success';
				} else if( $deliveryWindow >= time() + 3 * $sec && $deliveryWindow <= time() + 5 * $sec ) {
					$row['status_delivery'] = $this::$STATUS_DELIVERY_POTENTIAL_DELAY;
					$row['status_delivery_css'] = 'label label-warning';
				} else if( $deliveryWindow < time() + 3 * $sec ) {
					$row['status_delivery'] = $this::$STATUS_DELIVERY_DELAY;
					$row['status_delivery_css'] = 'label label-important';
				} else {
					$row['status_delivery'] = $this::$STATUS_DELIVERY_ON_TIME;
					$row['status_delivery_css'] = 'label label-success';
				}
			} else {
				if( $ata > $deliveryWindowMinus3Days ) {
					$row['status_delivery'] = $this::$STATUS_DELIVERY_DELAY;
					$row['status_delivery_css'] = 'label label-important';
				} else {
					$row['status_delivery'] = $this::$STATUS_DELIVERY_ON_TIME;
					$row['status_delivery_css'] = 'label label-success';
				}
			}

			$dates = array(
				'shipments_ata',
				'shipments_eta',
				'shipments_edo_sent_click_date',
				'contracts_aldi_au_wh_delivery_due_date',
				'contracts_aldi_au_wh_delivery_due_end_date',
				'shipments_cus',
				'shipments_pick_up_at_wharf',
				'shipments_delivered_to_dc'
			);

			foreach( $dates as $date ) {
				$row[$date.'_timestamp'] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? strtotime( $row[$date] ) : 0;
				$row[$date] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? date( $this->config->item('date_format'), strtotime( $row[$date] ) ) : "";
			}

			if( isset( $row['shipments_destination_port'] ) ) 	$row['shipments_destination_port'] 	= $this->getDestinationPort( $row['shipments_destination_port'] );

			$traOptions = $this->getTCAgetMultiselectionOptions( 'contracts_traffic_type' );
			$row['contracts_traffic_type'] = array_key_exists($row['contracts_traffic_type'], $traOptions) ? $traOptions[$row['contracts_traffic_type']] : $row['contracts_traffic_type'];

			$res[] = $row;
		}
		return $res;
	}

	private function filterResult( $res, $filter )
	{
		$out = array();
		if($filter['type'] == 'status') {
			foreach( $res as $row ) {
				if ( $filter['value'] == 'ontime' ) {
					if( $row['status_delivery'] == $this::$STATUS_DELIVERY_ON_TIME ) $out[] = $row;
				} else if ( $filter['value'] == 'potentialdelay' ) {
					if( $row['status_delivery'] == $this::$STATUS_DELIVERY_POTENTIAL_DELAY ) $out[] = $row;
				} else if ( $filter['value'] == 'delay' ) {
					if( $row['status_delivery'] == $this::$STATUS_DELIVERY_DELAY ) $out[] = $row;
				}
			}
		}
		return $out;
	}

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}

	/**
	 * getDestinationPort
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getDestinationPort( $id=0 ) {
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$destinationPortOptions 		= $this->tca->getMultiselectionOptions( $tcaNode );
		return $destinationPortOptions[$id];
	}


}
