<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Carrier_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'carriers';
	}


	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////


}

/* End of file carrier.php */
/* Location: ./app/models/carrier.php */
