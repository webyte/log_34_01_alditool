<?php
require_once( APPPATH.'models/abstract_report_clp_model.php' );

class Report_clp_shipment_interface_model extends Abstract_report_clp_model {

	public $cacheName = 'report_clp_shipment_interface_getrecords';

	function __construct() {
		parent::__construct();
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
	}

	/**
	 * getRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @return array - db record as array.
	 */
	public function getRecords( $recordListSearchParams ) {

		if( !$res = $this->cache->get($this->cacheName) ) {
			$res = array();

			$this->db->select(
				$this->getAllFieldsFromTable( 'deliveries' ).', '.
				$this->getAllFieldsFromTable( 'contracts' ).', '.
				$this->getAllFieldsFromTable( 'shipments' ).', '.
				$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
				$this->getAllFieldsFromTable( 'pcshipments' ).', '.
				'(SELECT SUM(shipments_deliveries_mm.gross_weight) FROM pcshipments_deliveries_mm, shipments_deliveries_mm WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS weight_total,'.
				'(SELECT SUM(shipments_deliveries_mm.volume) FROM pcshipments_deliveries_mm, shipments_deliveries_mm WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS volume_total'
			);
			$this->db->from( 'deliveries' );
			$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
			$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left' );
			$this->db->join( 'shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
			$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.shipment_id = shipments.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0', 'left');
			$this->db->join( 'pcshipments', 'pcshipments.id = pcshipments_deliveries_mm.pcshipment_id AND pcshipments.hidden = 0 AND pcshipments.deleted = 0', 'left');
			$this->setWhereClauseForSearchparams( $recordListSearchParams );
			$this->db->where( array( 'deliveries.shipment_number !='  => '', 'deliveries.hidden' => 0, 'deliveries.deleted' => 0 ) );
			$this->db->where( array( 'deliveries.shipment_number !='  => '', 'deliveries.hidden' => 0, 'deliveries.deleted' => 0 ) );
			$this->db->where( '((shipments_deliveries_mm.id IS NULL AND shipments.eta IS NULL) OR (shipments_deliveries_mm.id IS NOT NULL AND shipments.eta >= "'.date('Y-m-d H:i:s', strtotime('-1 year' ) ).'"))');
			$this->db->group_by( 'CASE WHEN shipments_deliveries_mm.id IS NULL THEN deliveries.id ELSE shipments_deliveries_mm.id END', true );
			$query = $this->db->get();


			foreach ( $query->result_array() as $row ) {

				$row['service_level_name'] 			= $row['shipments_contract_number'];
				$row['pol_organization_enterprise_name'] = 'ONE';
				$row['pol_organization_name'] 		= 'ONE';
				$row['pod_organization_enterprise_name'] = 'ONE';
				$row['pod_organization_name'] 		= 'ONE';
				$row['equipment_type'] 				= 'ALL';
				$row['quantity_unit_of_measure_2'] 	= 'CARTON';
				$row['weight_unit_of_measure'] 		= 'KILOGRAM';
				$row['volume_unit_of_measure'] 		= 'CUMT';
				$row['ship_to_enterprise_name'] 	= 'ALDI SUED KG';
				$row['ship_to_organization_name'] 	= 'ALDI SUED KG';
				$row['order_line_number'] 			= '1';
				$row['lead_logistics_provider_name'] = 'Logwin';
				$row['osd'] 						= ( $row['contracts_contract_type'] == '1' ) ? $row['contracts_aldi_au_wh_delivery_due_end_date'] : $row['contracts_on_sale_date'];
				$row['dangerous_goods'] 			= ( $row['shipments_dg'] == '1' ) ? '1' : '0';
				$row['weight'] 						= $row['weight_total'];
				$row['volume'] 						= sprintf( $this->config->item('floatvalues_format'), $row['deliveries_volume'] );
				$row['cmff']						= 'Logwin';
				if( empty( $row['shipments_id'] ) ) {
					$departures 						= $this->getDepartures( $row['contracts_departure'] );
					$destinations 						= $this->getDepartures( $row['deliveries_destination'] );
				} else {
					$departures 						= $this->getDepartures( $row['shipments_departure_port'] );
					$destinations 						= $this->getDepartures( $this->getDestinationPort( $row['shipments_destination_port'] ) );
				}
				$row['pol'] 						= $departures['name'];
				$row['pod'] 						= $destinations['name'];
				$transShipments 					= $this->getDepartures( $row['pcshipments_transshipment_port'] );
				$row['transshipment_port']			= $transShipments['port_name'];
				$row['special_instructions']		= 'Nominated DFF: '.( in_array( $row['deliveries_dc'], array('STP','MIN','PRE','BRE','DAN') ) ? 'Newline' : 'ACFS' );
				$row['delay_reason_group'] 			= $this->getDelayReasonGroup( $row['shipments_delay_reason_group'] );
				$row['delay_reason'] 				= $this->getDelayReason( $row['shipments_delay_reason'] );
				$row['reason_code']					= !empty( $row['delay_reason'] ) ? $row['delay_reason_group'].': '.$row['delay_reason'] : '';
				$row['order_request_schedule_number'] = 'RS'.$row['deliveries_order_number'];
				$row['order_schedule_number'] 		= 'DS'.$row['deliveries_order_number'];
				$containerType 						= $this->getContainerType( $row['shipments_container_type'] );
				$containerSize 						= $this->getContainerSize( $row['shipments_container_size'] );
				$row['aldi_container_type'] 		= $this->getAldiContainerType( $containerSize.$containerType );
				if( empty( $row['shipments_id'] ) ) {
					$row['movement'] = ( $row['contracts_traffic_type'] == 1 ) ? 'CY/CY' : 'CFS/CY';
				} else {
					$row['movement'] = ( $row['shipments_traffic_type'] == 1 ) ? 'CY/CY' : 'CFS/CY';
				}
				$row['incoterm_code'] 				= $this->getIncotermCode( $row['shipments_incoterm_code'] );
				$row['action']						= 'Update';
				$row['delayed']						= !empty( $row['delay_reason'] ) ? $row['contracts_actual_contract_ready_date'] : '';

				if( !empty( $row['shipments_id'] ) ) {
					$row['carrier_partner_name'] = $this->getCarrierName( $row['shipments_carrier'] );
				} else {
					$row['carrier_partner_name'] = '';
				}
				$row['contract_number'] 			= $row['carrier_partner_name'];
				$res[] = $row;
			}
			$res = $this->prepareResultQueryForOutput( $res, "Y-m-d" );
		}
		return $res;
	}

	/**
	 * getCellDefintions
	 * returns cell defintion
	 *
	 * @return array
	 */
	public function getCellDefintions() {
		$cells = array(

			array('field' => 'action', 'label' => '#* Action (Cancel, Update)'),
			array('field' => 'deliveries_shipment_number', 'label' => '* Shipment Number'),
			array('field' => 'deliveries_product_code', 'label' => 'Non Catalogued Item Name'),
			array('field' => 'contracts_contract_number', 'label' => 'BPO Number'),
			array('field' => 'service_level_name', 'label' => 'Service Level Name'),
			array('field' => 'contracts_actual_cargo_ready_date', 'label' => 'Pickup Date Range Start'),
			array('field' => 'contracts_actual_contract_ready_date', 'label' => 'Pickup Date Range End'),
			array('field' => 'contracts_fob_date', 'label' => 'Cargo Ready Date'),
			array('field' => 'osd', 'label' => 'Delivery Date Range Start'),
			array('field' => 'osd', 'label' => 'Delivery Date Range End'),
			array('field' => 'contracts_initial_crd', 'label' => 'Earliest Ship Target Date'),
			array('field' => 'pol', 'label' => 'Port Of Loading Name'),
			array('field' => 'pol_organization_enterprise_name', 'label' => 'Port Of Loading Organization Enterprise Name'),
			array('field' => 'pol_organization_name', 'label' => 'Port Of Loading Organization Name'),
			array('field' => 'pod', 'label' => 'Port Of Discharge Name'),
			array('field' => 'pod_organization_enterprise_name', 'label' => 'Port Of Discharge Organization Enterprise Name'),
			array('field' => 'pod_organization_name', 'label' => 'Port Of Discharge Organization Name'),
			array('field' => 'shipments_deliveries_mm_hbl_hawb', 'label' => 'Order Ref Number'),
			array('field' => 'carrier_partner_name', 'label' => 'Carrier Partner Name'),
			array('field' => 'contract_number', 'label' => 'Contract Number'),
			array('field' => 'shipments_carrier_booking_number', 'label' => '* Booking Number'),
			array('field' => 'pcshipments_mbl_mawb', 'label' => 'Bill Of Lading Number'),
			array('field' => 'shipments_deliveries_mm_hbl_hawb', 'label' => 'Pro Number'),
			array('field' => 'shipments_container_number', 'label' => 'Container Number'),
			array('field' => 'aldi_container_type', 'label' => 'Container Type'),
			array('field' => 'movement', 'label' => 'Routing Group'),
			array('field' => 'equipment_type', 'label' => 'Equipment Type'),
			array('field' => 'dangerous_goods', 'label' => 'Hazardous'),
			array('field' => 'deliveries_case_quantity', 'label' => 'Shipped Quantity 2'),
			array('field' => 'quantity_unit_of_measure_2', 'label' => 'Quantity Unit Of Measure 2'),
			array('field' => 'deliveries_gross_weight', 'label' => 'Weight'),
			array('field' => 'weight_unit_of_measure', 'label' => 'Weight Unit Of Measure'),
			array('field' => 'volume', 'label' => 'Volume'),
			array('field' => 'volume_unit_of_measure', 'label' => 'Volume Unit Of Measure'),
			array('field' => 'shipments_mother_vessel_name', 'label' => 'Vehicle No'),
			array('field' => 'shipments_voyage_number', 'label' => 'Offnetwork Schedule'),
			array('field' => 'shipments_cy_cut_off', 'label' => 'Cargo Cut Off Time'),
			array('field' => '', 'label' => 'Customs Filling Cut Off'),
			array('field' => 'shipments_container_gate_in', 'label' => 'In Gate'),
			array('field' => 'shipments_etd', 'label' => 'ETD Port Of Loading'),
			array('field' => 'shipments_eta', 'label' => 'ETA Port Of Discharge'),
			array('field' => 'shipments_atd', 'label' => 'ATD Port Of Loading'),
			array('field' => 'shipments_ata', 'label' => 'ATA Port Of Discharge'),
			array('field' => 'pcshipments_transshipment_port', 'label' => 'Transshipment Port 1 Site Name'),
			array('field' => 'pcshipments_vessel_2', 'label' => 'Transshipment Port 1 Vessel Name'),
			array('field' => 'pcshipments_voyage_2', 'label' => 'Transshipment Port 1 Voyage Number'),
			array('field' => 'first_leg_eta', 'label' => 'Transshipment Port 1 ETA'),
			array('field' => 'last_leg_etd', 'label' => 'Transshipment Port 1 ETD'),
			array('field' => 'first_leg_ata', 'label' => 'Transshipment Port 1 ATA'),
			array('field' => 'last_leg_atd', 'label' => 'Transshipment Port 1 ATD'),
			array('field' => '', 'label' => 'Transshipment Port 2 Site Name'),
			array('field' => '', 'label' => 'Transshipment Port 2 Vessel Name'),
			array('field' => '', 'label' => 'Transshipment Port 2 Voyage Number'),
			array('field' => '', 'label' => 'Transshipment Port 2 ETA'),
			array('field' => '', 'label' => 'Transshipment Port 2 ETD'),
			array('field' => '', 'label' => 'Transshipment Port 2 ATA'),
			array('field' => '', 'label' => 'Transshipment Port 2 ATD'),
			array('field' => '', 'label' => 'Transshipment Port 3 Site Name'),
			array('field' => '', 'label' => 'Transshipment Port 3 Vessel Name'),
			array('field' => '', 'label' => 'Transshipment Port 3 Voyage Number'),
			array('field' => '', 'label' => 'Transshipment Port 3 ETA'),
			array('field' => '', 'label' => 'Transshipment Port 3 ETD'),
			array('field' => '', 'label' => 'Transshipment Port 3 ATA'),
			array('field' => '', 'label' => 'Transshipment Port 3 ATD'),
			array('field' => '', 'label' => 'Transshipment Port 4 Site Name'),
			array('field' => '', 'label' => 'Transshipment Port 4 Vessel Name'),
			array('field' => '', 'label' => 'Transshipment Port 4 Voyage Number'),
			array('field' => '', 'label' => 'Transshipment Port 4 ETA'),
			array('field' => '', 'label' => 'Transshipment Port 4 ETD'),
			array('field' => '', 'label' => 'Transshipment Port 4 ATA'),
			array('field' => '', 'label' => 'Transshipment Port 4 ATD'),
			array('field' => '', 'label' => 'Transshipment Port 5 Site Name'),
			array('field' => '', 'label' => 'Transshipment Port 5 Vessel Name'),
			array('field' => '', 'label' => 'Transshipment Port 5 Voyage Number'),
			array('field' => '', 'label' => 'Transshipment Port 5 ETA'),
			array('field' => '', 'label' => 'Transshipment Port 5 ETD'),
			array('field' => '', 'label' => 'Transshipment Port 5 ATA'),
			array('field' => '', 'label' => 'Transshipment Port 5 ATD'),
			array('field' => '', 'label' => 'Arrived At Delivery Location'),
			array('field' => '', 'label' => 'Original Bill Of Lading Received'),
			array('field' => '', 'label' => 'PIN Released'),
			array('field' => '', 'label' => 'Customs Submitted'),
			array('field' => '', 'label' => 'ATB Received'),
			array('field' => '', 'label' => 'Unloaded'),
			array('field' => 'shipments_cus', 'label' => 'Customs Cleared'),
			array('field' => '', 'label' => 'Out Gate'),
			array('field' => '', 'label' => 'Delivered'),
			array('field' => '', 'label' => 'Equipment Returned'),
			array('field' => '', 'label' => 'Empty Return Site Name'),
			array('field' => 'ship_to_enterprise_name', 'label' => 'Ship To Enterprise Name'),
			array('field' => 'ship_to_organization_name', 'label' => 'Ship To Organization Name'),
			array('field' => 'deliveries_shiptosite', 'label' => 'Ship To Site Name'),
			array('field' => 'deliveries_order_number', 'label' => 'Order Number'),
			array('field' => 'order_line_number', 'label' => 'Order Line Number'),
			array('field' => 'order_request_schedule_number', 'label' => 'Order Request Schedule Number'),
			array('field' => 'order_schedule_number', 'label' => 'Order Schedule Number'),
			array('field' => 'deliveries_product_description', 'label' => 'Item Description'),
			array('field' => 'contracts_supplier', 'label' => 'Ship From Enterprise Name'),
			array('field' => 'contracts_supplier', 'label' => 'Ship From Organization Name'),
			array('field' => 'contracts_supplier', 'label' => 'Ship From Site Name'),
			array('field' => 'cmff', 'label' => 'Lead Logistics Provider Enterprise Name'),
			array('field' => 'lead_logistics_provider_name', 'label' => 'Lead Logistics Provider Name'),
			array('field' => 'contracts_shipment_type', 'label' => 'Shipment Type'),
			array('field' => 'incoterm_code', 'label' => 'Inco Terms Enum'),
			array('field' => 'shipments_departure_port', 'label' => 'Inco Terms Location'),
			array('field' => 'special_instructions', 'label' => 'Special Instructions'),
			array('field' => 'delayed', 'label' => 'Delayed'),
			array('field' => 'reason_code', 'label' => 'Reason Code'),
			array('field' => 'contracts_ofu_real', 'label' => 'Booked'),
			array('field' => 'shipments_so_released_date', 'label' => 'Carrier SO Released'),
			array('field' => '', 'label' => 'CMFFSO Released'),
			array('field' => 'shipments_received_in_cfs', 'label' => 'Cargo Delivered To Origin Warehouse'),
			array('field' => '', 'label' => 'Loaded On Rail'),
			array('field' => '', 'label' => 'Out Gate From Rail Yard'),
			array('field' => '', 'label' => 'ETA Final Delivery'),
			array('field' => '', 'label' => 'VGM Submission Date'),
			array('field' => '', 'label' => 'Destination Rail ETD'),
			array('field' => '', 'label' => 'Destination Rail ATD'),

		);
		return $cells;
	}

	////////////////////////////////////////////////////////////////////////////

    //		PRIVATE

    ////////////////////////////////////////////////////////////////////////////

	/**
	 * getDepartures
	 * returns departure port
	 *
	 * @param string $code - port code
	 * @return array
	 */
	private function getDepartures( $code='' )
	{
		$out = array(
			'name' => '',
			'port_name' => '',
			'port_country' => '',
		);

		if( count( $this->departures ) <= 0 ) {
			$this->load->model('Departure_model', 'departure_model');
			$this->departures = $this->departure_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		}

		foreach( $this->departures as $row ) {
			if( $row['name'] == $code ) {
				$out['name'] = $row['name'];
				$out['port_name'] = $row['port_name'];
				$out['port_country'] = $row['port_country'];
				return $out;
			}
		}

		return $out;
	}

	/**
	 * getDestinationPort
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getDestinationPort( $id=0 ) {
		$tcaNode = $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options[$id];
	}

	/**
	 * getDestinationPort
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getIncotermCode( $id=0 ) {
		$tcaNode = $this->tca->getColumnNodeById( 'shipments_incoterm_code' );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options[$id];
	}

	/**
	 * getContainerType
	 */
	private function getAldiContainerType( $code ) {

		$map = array(
			'20DC' => '20Ft Dry',
			'20GP' => '20Ft Dry',
			'20HC' => '20Ft Dry High Cube',
			'40DC' => '40Ft Dry',
			'40GP' => '40Ft Dry',
			'40HC' => '40Ft Dry High Cube',
			'45DC' => '45Ft Dry',
			'45GP' => '45Ft Dry',
			'45HV' => '45Ft Dry High Cube',
			'20RF' => '20Ft Reefer',
			'20HCRF' => '20Ft Reefer High Cube',
			'40RF' => '40Ft Reefer',
			'40HCRF' => '40Ft Reefer High Cube',
			'20NO' => '',
			'40NO' => '',
			'45RF' => '45Ft Reefer',
			'45HCRF' => '45Ft Reefer High Cube',
		);
		return isset( $map[$code] ) ? $map[$code] : '';
	}
}

/* End of file orders.php */
/* Location: ./app/models/orders.php */
