<?php
require_once( APPPATH.'models/abstract_report_clp_model.php' );

class Report_clp_kpi_model extends Abstract_report_clp_model {

	public $cacheName = 'report_clp_kpi_getrecords';

	function __construct() {
		parent::__construct();
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
	}

    /**
     * getRecords
     * returns the db-records with the given search params
     *
     * @param array $recordListSearchParams - search params
     * @return array - db record as array.
     */
    public function getRecords( $recordListSearchParams ) {

    	if( !$res = $this->cache->get($this->cacheName) ) {
			$res = array();
			$this->db->select(
				$this->getAllFieldsFromTable( 'deliveries' ).', '.
				$this->getAllFieldsFromTable( 'contracts' ).', '.
				$this->getAllFieldsFromTable( 'shipments' ).', '.
				$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
				$this->getAllFieldsFromTable( 'pcshipments' ).', '.
				//'(SELECT SUM(shipments_deliveries_mm.gross_weight) FROM shipments_deliveries_mm WHERE shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS weight_total,'.
				//'(SELECT SUM(shipments_deliveries_mm.volume) FROM shipments_deliveries_mm WHERE shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS volume_total,'.
				'(SELECT SUM(shipments_deliveries_mm.gross_weight) FROM pcshipments_deliveries_mm, shipments_deliveries_mm WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS weight_total,'.
				'(SELECT SUM(shipments_deliveries_mm.volume) FROM pcshipments_deliveries_mm, shipments_deliveries_mm WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS volume_total,'.
				'(SELECT GROUP_CONCAT(deliveries.contract_number) FROM pcshipments_deliveries_mm, deliveries WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND deliveries.hidden = 0 AND deliveries.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND deliveries.id = pcshipments_deliveries_mm.delivery_id) AS contract_number_total,'.
				'(SELECT GROUP_CONCAT(deliveries.product_code) FROM pcshipments_deliveries_mm, deliveries WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND deliveries.hidden = 0 AND deliveries.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND deliveries.id = pcshipments_deliveries_mm.delivery_id) AS product_code_total,'.
				'(SELECT GROUP_CONCAT(deliveries.product_description) FROM pcshipments_deliveries_mm, deliveries WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND deliveries.hidden = 0 AND deliveries.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND deliveries.id = pcshipments_deliveries_mm.delivery_id) AS product_description_total,'.
				'(SELECT COUNT(pc.id) FROM pcshipments AS pc WHERE pc.hbl_hawb = pcshipments.hbl_hawb AND pc.deleted = 0 AND pc.hidden = 0) AS shipment_total,'.
				'(SELECT COUNT(pc.id) FROM pcshipments AS pc WHERE (pc.ata = "0000-00-00 00:00:00" OR shipments.ata >= "'.date('Y-m-d H:i:s', strtotime('-12 months')).'") AND pc.container_number = pcshipments.container_number AND pc.deleted = 0 AND pc.hidden = 0) AS container_total'
			);
			$this->db->from( 'pcshipments' );
			$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0');
			$this->db->join( 'deliveries', 'deliveries.id = pcshipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0');
			$this->db->join( 'shipments', 'shipments.id = pcshipments_deliveries_mm.shipment_id AND shipments.hidden = 0 AND shipments.deleted = 0');
			$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0');
			$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0');
			$this->setWhereClauseForSearchparams( $recordListSearchParams );

			$this->db->where( "(shipments.ata = '0000-00-00 00:00:00' OR shipments.ata >= '".date('Y-m-d H:i:s', strtotime('-12 months'))."')" );
			$this->db->where( array( 'pcshipments.hidden' => 0, 'pcshipments.deleted' => 0 ) );
			$this->db->group_by( 'pcshipments.id' );
			$query = $this->db->get();

			foreach ( $query->result_array() as $row ) {

				$row['contracts_contract_number']   	= $row['contract_number_total'];
				$row['deliveries_product_code'] 		= $row['product_code_total'];
				$row['shipments_atd_year_month']    	= date('Y', strtotime( $row['shipments_atd'] ) ).'/'.date('m', strtotime( $row['shipments_atd'] ) );
				$row['shipments_atd_month']         	= sprintf('%02d', date('m', strtotime( $row['shipments_atd'] ) ) );
				$row['aldi_country']                	= 'AU';
				$row['weight'] 							= $row['weight_total'];
				$row['volume'] 							= sprintf($this->config->item('doublevalues_format'), $row['volume_total']);
				$row['ocean_transit_time'] 				= ( $row['shipments_ata'] != '0000-00-00 00:00:00' && $row['shipments_atd'] != '0000-00-00 00:00:00' ) ? round( ( strtotime( $row['shipments_ata'] ) - strtotime( $row['shipments_atd'] ) ) / ( 24 * 60 * 60 ) ) : '';
				$row['cmff'] 							= 'Logwin';
				$row['unique_shipment'] 				= ( $row['shipment_total'] == 1 ) ? 'Select' : '';
				$row['unique_container'] 				= ( $row['container_total'] == 1 ) ? 'Select' : '';

				$row['commiduty'] = '';
				$row['order_type'] = '';
				$row['drop_off_location'] = '';
				$row['gate_out_at_destination_date'] = '';
				$row['empty_gate_in_at_destinatino_date'] = '';
				$row['used_of_demurrage'] = '';
				$row['user_of_detention'] = '';

				$departures 		= $this->getDepartures( $row['shipments_departure_port'] );
				$row['pol'] 		= $departures['port_name'];
				$row['pol_country'] = $departures['port_country'];
				$destinations 		= $this->getDepartures( $this->getDestinationPort( $row['shipments_destination_port'] ) );
				$row['pod'] 		= $destinations['port_name'];
				$row['pod_country'] = $destinations['port_country'];

				$res[] = $row;
			}
			$res = $this->prepareResultQueryForOutput( $res, false );
		}
		return $res;
    }

	/**
	 * getCellDefintions
	 * returns cell defintion
	 *
	 * @return array
	 */
	public function getCellDefintions() {
		$cells = array(
			array('field' => 'shipments_atd_year_month', 'label' => 'ATD Year/ Month'),
			array('field' => 'shipments_atd_month', 'label' => 'ATD Month'),
			array('field' => 'contracts_contract_number', 'label' => 'Freight Contract Number'),
			array('field' => 'deliveries_product_code', 'label' => 'Shipment Key'),
			array('field' => 'aldi_country', 'label' => 'ALDI Country'),
			array('field' => 'deliveries_product_code', 'label' => 'ALDI Product code'),
			array('field' => 'contracts_contract_number', 'label' => 'ALDI Contract Number'),
			array('field' => 'deliveries_product_description', 'label' => 'Product description'),
			array('field' => 'commiduty', 'label' => 'Commiduty'),
			array('field' => 'order_type', 'label' => 'Order type'),
			array('field' => 'contracts_supplier', 'label' => 'Supplier'),
			array('field' => 'contracts_fob_date', 'label' => 'ALDI Cargo ready date (CRD)'),
			array('field' => 'contracts_on_sale_date', 'label' => 'On Sales Date (OSD)'),
			array('field' => 'nominated_off', 'label' => 'Nominated OFF'),
			array('field' => 'domestic_freight_forwarder', 'label' => 'Domestic Freight Forwarder'),
			array('field' => 'drop_off_location', 'label' => 'Drop off location'),
			array('field' => 'pcshipments_mbl_mawb', 'label' => 'Master ref#'),
			array('field' => 'pcshipments_hbl_hawb', 'label' => 'HBL#'),
			array('field' => 'movement', 'label' => 'Movement'),
			array('field' => 'no_of_teu', 'label' => 'No. of TEU'),
			array('field' => 'kpi_20ge', 'label' => '20GE'),
			array('field' => 'kpi_40ge', 'label' => '40GE'),
			array('field' => 'kpi_40hc', 'label' => '40HC'),
			array('field' => 'volume', 'label' => 'Volume (CBM)'),
			array('field' => 'weight', 'label' => 'Weight (kg)'),
			array('field' => 'pol', 'label' => 'POL'),
			array('field' => 'pol_country', 'label' => 'POL country'),
			array('field' => 'pod', 'label' => 'POD'),
			array('field' => 'pod_country', 'label' => 'POD country'),
			array('field' => 'contracts_fob_date', 'label' => 'ALDI Cargo ready date (CRD)'),
			array('field' => 'contracts_ofu_real', 'label' => 'Booked by Shipper Date'),
			array('field' => 'shipments_container_gate_in', 'label' => 'Cargo gate in date'),
			array('field' => 'shipments_mother_vessel_name', 'label' => 'Vessel Name'),
			array('field' => 'shipments_voyage_number', 'label' => 'Vessel Voyage'),
			array('field' => 'shipments_carrier', 'label' => 'Ocean carrier'),
			array('field' => 'shipments_etd', 'label' => 'ETD'),
			array('field' => 'shipments_eta', 'label' => 'ETA'),
			array('field' => 'shipments_atd', 'label' => 'ATD'),
			array('field' => 'shipments_ata', 'label' => 'ATA'),
			array('field' => 'shipments_ata', 'label' => 'Date of discharge'),
			array('field' => 'gate_out_at_destination_date', 'label' => 'Gate out at destination date'),
			array('field' => 'empty_gate_in_at_destinatino_date', 'label' => 'Empty gate in at destinatino date'),
			array('field' => 'delay_reason_group', 'label' => 'Delay reason group'),
			array('field' => 'delay_reason', 'label' => 'Delay reason'),
			array('field' => 'shipments_container_number', 'label' => 'Container #'),
			array('field' => 'container_type', 'label' => 'Container type'),
			array('field' => 'cbm_utilization', 'label' => 'CBM utilization (%)'),
			array('field' => 'weight_utilization', 'label' => 'Weight utiliztion (%)'),
			array('field' => 'used_of_demurrage', 'label' => 'Used of Demurrage'),
			array('field' => 'user_of_detention', 'label' => 'User of Detention'),
			array('field' => 'ocean_transit_time', 'label' => 'Ocean transit time (day)'),
			array('field' => 'cmff', 'label' => 'CMFF'),
			array('field' => 'unique_shipment', 'label' => 'Unique shipment'),
			array('field' => 'unique_container', 'label' => 'Unique Container'),
			array('field' => 'pcshipments_ietd', 'label' => 'Initial ETD'),
			array('field' => 'pcshipments_ieta', 'label' => 'Initial ETA'),
		);
		return $cells;
	}

	////////////////////////////////////////////////////////////////////////////

    //		PRIVATE

    ////////////////////////////////////////////////////////////////////////////

 	/**
	 * getDepartures
	 * returns departure port
	 *
	 * @param string $code - port code
	 * @return array
	 */
	private function getDepartures( $code='' )
	{
		$out = array(
			'port_name' => '',
			'port_country' => '',
		);

		if( count( $this->departures ) <= 0 ) {
			$this->load->model('Departure_model', 'departure_model');
			$this->departures = $this->departure_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		}

		foreach( $this->departures as $row ) {
			if( $row['name'] == $code ) {
				$out['port_name'] = $row['port_name'];
				$out['port_country'] = $row['port_country'];
				return $out;
			}
		}

		return $out;
	}

	/**
	 * getDestinationPort
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getDestinationPort( $id=0 ) {
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$destinationPortOptions 		= $this->tca->getMultiselectionOptions( $tcaNode );
		return $destinationPortOptions[$id];
	}

}

/* End of file orders.php */
/* Location: ./app/models/orders.php */
