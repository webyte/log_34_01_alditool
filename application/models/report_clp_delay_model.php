<?php
require_once( APPPATH.'models/abstract_report_clp_model.php' );

class Report_clp_delay_model extends Abstract_report_clp_model {

	public $cacheName = 'report_clp_delay_getrecords';

	function __construct() {
		parent::__construct();
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
	}

 	/**
	 * getRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @return array - db record as array.
	 */
	public function getRecords( $recordListSearchParams ) {

		if( !$res = $this->cache->get($this->cacheName) ) {
			$res = array();
			$this->db->select(
				$this->getAllFieldsFromTable( 'deliveries' ).', '.
				$this->getAllFieldsFromTable( 'contracts' ).', '.
				$this->getAllFieldsFromTable( 'shipments' ).', '.
				$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
				$this->getAllFieldsFromTable( 'pcshipments' ).', '.
				'(SELECT GROUP_CONCAT(d1.contract_number) FROM shipments_deliveries_mm AS sdmm4 JOIN deliveries AS d1 ON d1.id = sdmm4.delivery_id AND d1.hidden = 0 AND d1.deleted = 0 WHERE sdmm4.hidden = 0 AND sdmm4.deleted = 0 AND sdmm4.shipment_id = shipments.id) AS contract_number_total,'.
				'(SELECT GROUP_CONCAT(d2.product_code) FROM shipments_deliveries_mm AS sdmm5 JOIN deliveries AS d2 ON d2.id = sdmm5.delivery_id AND d2.hidden = 0 AND d2.deleted = 0 WHERE sdmm5.hidden = 0 AND sdmm5.deleted = 0 AND sdmm5.shipment_id = shipments.id) AS product_code_total,'.
				'(SELECT GROUP_CONCAT(d3.product_description) FROM shipments_deliveries_mm AS sdmm6 JOIN deliveries AS d3 ON d3.id = sdmm6.delivery_id AND d3.hidden = 0 AND d3.deleted = 0 WHERE sdmm6.hidden = 0 AND sdmm6.deleted = 0 AND sdmm6.shipment_id = shipments.id) AS product_description_total'
			);
			$this->db->from( 'shipments' );
			$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0');
			$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0');
			$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0');
			$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.delivery_id = deliveries.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0');
			$this->db->join( 'pcshipments', 'pcshipments.id = pcshipments_deliveries_mm.pcshipment_id AND pcshipments.hidden = 0 AND pcshipments.deleted = 0');
			$this->setWhereClauseForSearchparams( $recordListSearchParams );

			$this->db->where( "shipments.ata = '0000-00-00 00:00:00' AND TIMESTAMPDIFF(DAY,pcshipments.ieta,contracts.aldi_au_wh_delivery_due_date) <= 25" );
			$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
			$this->db->group_by( 'shipments.id' );
			$query = $this->db->get();

			foreach ( $query->result_array() as $row ) {

				$row['contracts_contract_number']   	= $row['contract_number_total'];
				$row['deliveries_product_code'] 		= $row['product_code_total'];
				$row['deliveries_product_description'] 	= $row['product_description_total'];
				$row['aldi_country'] 				= 'AUSTRALIA';
				$row['transhipment_port']			= $row['pcshipments_transshipment_port'];
				$row['booking_deadline'] 			= date( 'd/m/Y', strtotime( '-21 days', strtotime( $row['contracts_fob_date'] ) ) );
				$row['ship_mode']					= 'SEA';
				$row['carrier_booking_no']			= $row['pcshipments_carrier_booking_no'];

				$res[] = $row;
			}
			$res = $this->prepareResultQueryForOutput( $res, false );
		}
		return $res;
	}

	/**
	 * getCellDefintions
	 * returns cell defintion
	 *
	 * @return array
	 */
	public function getCellDefintions() {
		$cells = array(
			array('field' => 'aldi_country', 'label' => 'ALDI Country'),
			array('field' => 'deliveries_product_code', 'label' => 'ALDI Purchase Order'),
			array('field' => 'deliveries_product_code', 'label' => 'ALDI Product Code'),
			array('field' => 'contracts_contract_number', 'label' => 'ALDI Contract Number'),
			array('field' => 'deliveries_product_description', 'label' => 'Product Description'),
			array('field' => 'contracts_supplier', 'label' => 'Supplier Name'),
			array('field' => 'shipments_departure_port', 'label' => 'Port of Loading'),
			array('field' => 'transhipment_port', 'label' => 'Transhipment Port'),
			array('field' => 'shipments_destination_port', 'label' => 'Port Of Discharge'),
			array('field' => 'shipments_dc', 'label' => 'Region'),
			array('field' => 'contracts_actual_contract_ready_date', 'label' => 'Factory Cargo Ready Date'),
			array('field' => 'contracts_fob_date', 'label' => 'CRD'),
			array('field' => 'contracts_on_sale_date', 'label' => 'OSD'),
			array('field' => 'contracts_received_in_cfs', 'label' => 'Cargo Delivery to Origin Warehouse (CFS/ CY or CFS/ CFS only)'),
			array('field' => 'shipments_container_gate_in', 'label' => 'CY Gate In'),
			array('field' => 'ship_mode', 'label' => 'Ship mode'),
			array('field' => 'service_type', 'label' => 'Service Type'),
			array('field' => 'nominated_off', 'label' => 'Nominated OFF'),
			array('field' => 'shipments_carrier', 'label' => 'Ocean Carrier'),
			array('field' => 'carrier_booking_no', 'label' => 'Carrier Booking no#'),
			array('field' => 'pcshipments_mbl_mawb', 'label' => 'MBL#'),
			array('field' => 'shipments_hbl_hawb', 'label' => 'HBL#'),
			array('field' => 'shipments_mother_vessel_name', 'label' => '1st Leg Vessel Name '),
			array('field' => 'shipments_voyage_number', 'label' => '1st Leg Vessel Voyage '),
			array('field' => 'pcshipments_ietd', 'label' => 'Initial 1st Leg ETD'),
			array('field' => 'shipments_etd', 'label' => '1st Leg ETD'),
			array('field' => 'first_leg_eta', 'label' => '1st Leg ETA'),
			array('field' => 'shipments_atd', 'label' => '1st Leg ATD'),
			array('field' => 'first_leg_ata', 'label' => '1st Leg ATA'),
			array('field' => 'shipments_vessel_2', 'label' => 'last Leg Vessel Name'),
			array('field' => 'shipments_voyage_2', 'label' => 'last Leg Vessel Voyage'),
			array('field' => 'last_leg_etd', 'label' => 'last Leg ETD'),
			array('field' => 'last_leg_atd', 'label' => 'last Leg ATD'),
			array('field' => 'pcshipments_ieta', 'label' => 'Initial last Leg ETA'),
			array('field' => 'shipments_eta', 'label' => 'last Leg ETA'),
			array('field' => '20dc', 'label' => '20\' Standard Dry'),
			array('field' => '40dc', 'label' => '40\' Standard Dry'),
			array('field' => '40hc_substitution_for_40', 'label' => '40\' HC Substitution for 40'),
			array('field' => '40hc', 'label' => '40\' High Cube Dry '),
			array('field' => '20rf', 'label' => '20\' Reefer'),
			array('field' => '40rf', 'label' => '40\'H Reefer'),
			array('field' => '20no', 'label' => '20\' NOR'),
			array('field' => '40no', 'label' => '40\' NOR'),
			array('field' => 'domestic_freight_forwarder', 'label' => 'Domestic Freight forwarder'),
			array('field' => 'crd_vs_factory_cargo_ready_date', 'label' => 'CRD vs Factory Cargo Ready Date'),
			array('field' => 'crd_on_time', 'label' => 'CRD On Time '),
			array('field' => 'first_leg_etd_vs_first_leg_atd', 'label' => '1st Leg ETD vs 1st Leg ATD'),
			array('field' => 'last_leg_eta_vs_osd', 'label' => 'last Leg ETA vs OSD'),
			array('field' => 'delay_reason', 'label' => 'Delay Reason'),
			array('field' => 'latest_shipment_status', 'label' => 'Latest shipment status'),
			array('field' => 'shipments_container_number', 'label' => 'Container Number'),
			array('field' => 'contracts_advertisement_week', 'label' => 'Sales Week'),
		);
		return $cells;
	}


	////////////////////////////////////////////////////////////////////////////

    //		PRIVATE

    ////////////////////////////////////////////////////////////////////////////

}

/* End of file orders.php */
/* Location: ./app/models/orders.php */
