<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Departure_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'departures';
	}


	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select('*');
		$this->db->from( $this->tablename );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $res;
	}

	/**
	 * getDeparturePorts
	 * returns a json encoded array with associated departure ports
	 * in the frontend dynamically.
	 *
	 * @param str $query - part of the wanted departure port
	 * @return array - db record as array.
	 */
	public function getDeparturePorts( $query ) {
		$this->db->like( 'name', $query );
		$this->db->where( array( 'hidden' => 0, 'deleted' => 0 ) );
		$query = $this->db->get( $this->tablename );
		return $query->result_array();
	}

	/**
	 * configures where clause for db query, depending on values in searchform.
	 */
	public function setWhereClauseForSearchparams( $recordListSearchParams ) {
		$fieldnames = array(
			'departures_name',
		);
		foreach ( $fieldnames as $fname ) {
			$tmpValue = array_key_exists($fname, $recordListSearchParams) ? $recordListSearchParams[$fname][0] : '';
			if( !empty($tmpValue) ){
				switch($fname){
					case 'departures_name':
						$this->db->where( 'departures.name', $tmpValue );
						break;
				}
			}
		}
	}
}

/* End of file office.php */
/* Location: ./app/models/departure.php */
