<?php
require_once( APPPATH.'models/abstract_report_clp_model.php' );

class Report_clp_container_status_model extends Abstract_report_clp_model {

	public $cacheName = 'report_clp_container_status_getrecords';

	function __construct() {
		parent::__construct();
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
	}

 	/**
	 * getRecords
	 * returns the db-records with the given search params
	 *
	 * @param array $recordListSearchParams - search params
	 * @return array - db record as array.
	 */
	public function getRecords( $recordListSearchParams ) {

		if( !$res = $this->cache->get($this->cacheName) ) {
			$res = array();
			$this->db->select(
				$this->getAllFieldsFromTable( 'deliveries' ).', '.
				$this->getAllFieldsFromTable( 'contracts' ).', '.
				$this->getAllFieldsFromTable( 'shipments' ).', '.
				$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.
				$this->getAllFieldsFromTable( 'pcshipments' ).', '.
				'(SELECT SUM(shipments_deliveries_mm.gross_weight) FROM pcshipments_deliveries_mm, shipments_deliveries_mm WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS weight_total,'.
				'(SELECT SUM(shipments_deliveries_mm.volume) FROM pcshipments_deliveries_mm, shipments_deliveries_mm WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS volume_total,'.
				'(SELECT GROUP_CONCAT(deliveries.contract_number) FROM pcshipments_deliveries_mm, deliveries WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND deliveries.hidden = 0 AND deliveries.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND deliveries.id = pcshipments_deliveries_mm.delivery_id) AS contract_number_total,'.
				'(SELECT GROUP_CONCAT(deliveries.product_code) FROM pcshipments_deliveries_mm, deliveries WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND deliveries.hidden = 0 AND deliveries.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND deliveries.id = pcshipments_deliveries_mm.delivery_id) AS product_code_total,'.
				'(SELECT GROUP_CONCAT(deliveries.product_description) FROM pcshipments_deliveries_mm, deliveries WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND deliveries.hidden = 0 AND deliveries.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND deliveries.id = pcshipments_deliveries_mm.delivery_id) AS product_description_total,'.
				'(SELECT SUM(shipments_deliveries_mm.case_quantity) FROM pcshipments_deliveries_mm, shipments_deliveries_mm WHERE pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.deleted = 0 AND pcshipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.delivery_id = pcshipments_deliveries_mm.delivery_id AND shipments_deliveries_mm.shipment_id = pcshipments_deliveries_mm.shipment_id) AS case_quantity_total,'.
				'(SELECT SUM(deliveries.gross_weight) FROM shipments_deliveries_mm JOIN deliveries ON deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0 WHERE shipments_deliveries_mm.shipment_id = shipments.id) AS gross_weight_total'
			);
			$this->db->from( 'pcshipments' );
			$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0');
			$this->db->join( 'shipments', 'shipments.id = pcshipments_deliveries_mm.shipment_id AND shipments.hidden = 0 AND shipments.deleted = 0');
			$this->db->join( 'deliveries', 'deliveries.id = pcshipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0');
			$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0');
			$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0');
			$this->setWhereClauseForSearchparams( $recordListSearchParams );

			$this->db->where( "(shipments.ata = '0000-00-00 00:00:00' OR shipments.ata >= '".date('Y-m-d H:i:s', strtotime('-12 months'))."')" );
			$this->db->where( array( 'pcshipments.hidden' => 0, 'pcshipments.deleted' => 0 ) );
			$this->db->group_by( 'pcshipments.id' );
			$query = $this->db->get();

			foreach ( $query->result_array() as $row ) {

				$row['contracts_contract_number']   = $row['contract_number_total'];
				$row['deliveries_product_code'] 	= $row['product_code_total'];
				$row['deliveries_product_description'] 	= $row['product_description_total'];
				$row['aldi_country'] 				= 'AUSTRALIA';
				$row['transhipment_port']			= $row['pcshipments_transshipment_port'];
				$row['carrier_booking_no']			= $row['pcshipments_carrier_booking_no'];
				$row['delivered']					= '';
				$row['laden_gate_out_date']			= '';
				$row['empty_gate_in']				= '';
				$row['no_of_package']				= $row['case_quantity_total'];
				$row['weight'] 						= $row['weight_total'];
				$row['volume'] 						= sprintf($this->config->item('doublevalues_format'), $row['volume_total']);
				$grossWeight						= $row['weight_total'] + $row['shipments_container_tare_weight'];
				$row['gross_weight'] 				= sprintf($this->config->item('floatvalues_format'), $grossWeight);

				$res[] = $row;
			}
			$res = $this->prepareResultQueryForOutput( $res, false );
		}
		return $res;
	}

	/**
	 * getCellDefintions
	 * returns cell defintion
	 *
	 * @return array
	 */
	public function getCellDefintions() {
		$cells = array(
			array('field' => 'aldi_country', 'label' => 'ALDI Country'),
			array('field' => 'deliveries_product_code', 'label' => 'ALDI Purchase Order'),
			array('field' => 'deliveries_product_code', 'label' => 'ALDI Product Code'),
			array('field' => 'contracts_contract_number', 'label' => 'ALDI Contract Number'),
			array('field' => 'deliveries_product_description', 'label' => 'Product Description'),
			array('field' => 'contracts_supplier', 'label' => 'Supplier Name'),
			array('field' => 'shipments_departure_port', 'label' => 'Port of Loading'),
			array('field' => 'transhipment_port', 'label' => 'Transhipment Port'),
			array('field' => 'shipments_destination_port', 'label' => 'Port Of Discharge'),
			array('field' => 'shipments_dc', 'label' => 'Region'),
			array('field' => 'contracts_actual_contract_ready_date', 'label' => 'Factory Cargo Ready Date'),
			array('field' => 'contracts_fob_date', 'label' => 'CRD'),
			array('field' => 'contracts_on_sale_date', 'label' => 'OSD'),
			array('field' => 'booking_deadline', 'label' => 'Booking Deadline'),
			array('field' => 'contracts_ofu_real', 'label' => 'Booked Date'),
			array('field' => 'contracts_received_in_cfs', 'label' => 'Cargo Delivery to Origin Warehouse (CFS/ CY or CFS/ CFS only)'),
			array('field' => 'shipments_container_gate_in', 'label' => 'CY Gate In'),
			array('field' => 'ship_mode', 'label' => 'Ship mode'),
			array('field' => 'service_type', 'label' => 'Service Type'),
			array('field' => 'nominated_off', 'label' => 'Nominated OFF'),
			array('field' => 'shipments_carrier', 'label' => 'Ocean Carrier'),
			array('field' => 'carrier_booking_no', 'label' => 'Carrier Booking no#'),
			array('field' => 'pcshipments_mbl_mawb', 'label' => 'MBL#'),
			array('field' => 'pcshipments_hbl_hawb', 'label' => 'HBL#'),
			array('field' => 'shipments_mother_vessel_name', 'label' => '1st Leg Vessel Name '),
			array('field' => 'shipments_voyage_number', 'label' => '1st Leg Vessel Voyage '),
			array('field' => 'pcshipments_ietd', 'label' => 'Initial 1st Leg ETD'),
			array('field' => 'shipments_etd', 'label' => '1st Leg ETD'),
			array('field' => 'first_leg_eta', 'label' => '1st Leg ETA'),
			array('field' => 'shipments_atd', 'label' => '1st Leg ATD'),
			array('field' => 'first_leg_ata', 'label' => '1st Leg ATA'),
			array('field' => 'shipments_vessel_2', 'label' => 'last Leg Vessel Name'),
			array('field' => 'shipments_voyage_2', 'label' => 'last Leg Vessel Voyage'),
			array('field' => 'last_leg_etd', 'label' => 'last Leg ETD'),
			array('field' => 'last_leg_atd', 'label' => 'last Leg ATD'),
			array('field' => 'pcshipments_ieta', 'label' => 'Initial last Leg ETA'),
			array('field' => 'shipments_eta', 'label' => 'last Leg ETA'),
			array('field' => 'shipments_ata', 'label' => 'last Leg ATA'),
			array('field' => 'delivered', 'label' => 'Delivered (Arrival after discharge from Vessel)'),
			array('field' => 'laden_gate_out_date', 'label' => 'Laden Gate out date'),
			array('field' => 'empty_gate_in', 'label' => 'Empty Gate In'),
			array('field' => 'no_of_package', 'label' => 'No of Package '),
			array('field' => 'volume', 'label' => 'Shipment Volume (CBM)'),
			array('field' => 'weight', 'label' => 'Gross Weight '),
			array('field' => 'shipments_container_tare_weight', 'label' => 'Container Weight '),
			array('field' => 'gross_weight', 'label' => 'Gross Weight (including container)'),
			array('field' => 'dgr_flag', 'label' => 'DGR Flag'),
			array('field' => 'latest_shipment_status', 'label' => 'Latest shipment status'),
			array('field' => 'domestic_freight_forwarder', 'label' => 'Domestic Freight forwarder'),
			array('field' => 'crd_vs_factory_cargo_ready_date', 'label' => 'CRD vs Factory Cargo Ready Date'),
			array('field' => 'crd_on_time', 'label' => 'CRD On Time '),
			array('field' => 'first_leg_etd_vs_first_leg_atd', 'label' => '1st Leg ETD vs 1st Leg ATD'),
			array('field' => 'last_leg_eta_vs_last_leg_ata', 'label' => 'last Leg ETA vs last Leg ATA'),
			array('field' => 'last_leg_eta_vs_osd', 'label' => 'last Leg ETA vs OSD'),
			array('field' => 'last_leg_ata_vs_osd', 'label' => 'last Leg ATA vs OSD'),
			array('field' => 'first_leg_etd_vs_last_leg_eta', 'label' => 'Transit Time (1st Leg ETD vs last Leg ETA)'),
			array('field' => 'pcshipments_container_number', 'label' => 'Container Number'),
			array('field' => 'contracts_advertisement_week', 'label' => 'Sales Week'),
			array('field' => 'container_type', 'label' => 'Container Type'),
		);
		return $cells;
	}

	////////////////////////////////////////////////////////////////////////////

    //		PRIVATE

    ////////////////////////////////////////////////////////////////////////////


 }

/* End of file orders.php */
/* Location: ./app/models/orders.php */
