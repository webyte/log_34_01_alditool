<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Contactperson_model extends Abstract_model {

    function __construct() {
        parent::__construct();
        $this->tablename = 'contactpersons';
    }

    /**
     * getRecordsForContract
     * returns all related hawb records for the given shipment id
     *
     * @param int $shipmentId - id of shipmentrecord we want related events for
     * @return array query result as array
     */
    public function getRecordsForSupplier( $supplierId ){
        $this->db->where( array('supplier_id' => $supplierId, 'hidden' => 0, 'deleted' => 0) );
        $query = $this->db->get( $this->tablename );
        return $query->result_array();
    }

	/**
	 * getDefaultEMailsForSupplier
	 *
	 * @param int $shipmentId - id of shipmentrecord we want related events for
	 * @return array query result as array
	 */
	public function getDefaultEMailsForSupplier( $supplierId ){
		$emails = array();
		$this->db->where( array('supplier_id' => $supplierId, 'default_contact' => 1, 'hidden' => 0, 'deleted' => 0) );
		$query = $this->db->get( $this->tablename );
		foreach( $query->result_array() as $row ){
			$emails[] = $row['email'];
		}
		return $emails;
	}

}

/* End of file supplier.php */
/* Location: ./app/models/supplier.php */
