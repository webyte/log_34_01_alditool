<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Product_model extends Abstract_model {

    function __construct() {
        parent::__construct();
        $this->tablename = 'products';
    }

	/**
	 * getProductDescriptionByProductCode
	 * returns a the associated product description as string
	 *
	 * @param str $productCode - product code
	 * @return string - product description
	 */
	public function getProductDescriptionByProductCode( $productCode ) {
		$out = '';
		$this->db->like( 'article_number', $productCode );
		$this->db->where( array( 'hidden' => 0, 'deleted' => 0 ) );
		$query = $this->db->get( $this->tablename );
		if( $query->num_rows() > 0 ) {
			$row = $query->row_array();
			$out = $row['article_description'];
		}
		return $out;
	}


//    /**
//     * return just the product descripton
//     *
//     * @param string $product_number
//     * @return mixed
//     */
//    public function getProductDescriptionById($product_number){
//        $where_arr = "article_number = '$product_number' AND deleted='0' AND hidden='0'";
//        $this->db->where( $where_arr );
////        if( $orderby != '' ) $this->db->order_by( $orderby );
//        $query = $this->db->get( $this->tablename );
//        $result = $query->result_array();
//        if(!empty($result)){
//            if(empty($result['1'])){
//                return $result['0']['article_description'];
//            }else{
//                return $result;
//            }
//        }
//    }

}

/* End of file comment_model.php */
/* Location: ./app/models/comment_model.php */
