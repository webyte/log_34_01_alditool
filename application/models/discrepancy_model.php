<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Discrepancy_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'discrepancies';
	}

	public function createNew( $insertData ) {
		$insertData['user_id'] = $this->session->userdata('userId');
    	return parent::createNew( $insertData );
    }
    
    public function getComments( $pid ) {
    	$this->db->select('discrepancies.*, users.username, discrepancies.id AS id');
		$this->db->from( $this->tablename );
		$this->db->join('users', 'discrepancies.user_id = users.id', 'left');
		$this->db->where( array( $this->tablename.'.pid' => $pid, $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$this->db->order_by( $this->tablename.'.crdate DESC');
		$query = $this->db->get();
    	$comments = $query->result_array();
    	return $comments;
    }

	public function getCommentsCount( $pid ) {
		$this->db->select('*');
		$this->db->from( $this->tablename );
		$this->db->where( array( $this->tablename.'.pid' => $pid, $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$query = $this->db->get();
		return $query->num_rows();
	}
}

/* End of file comment_model.php */
/* Location: ./app/models/comment_model.php */
