<?php
require_once( APPPATH.'models/abstract_import_model'.EXT );

class Import_tms_model extends Abstract_import_model {


	function __construct() {
		parent::__construct();
		$this->lockSuffix = '_TMS';
		$this->importSubPath = 'add_data/';
	}


	/**
	 * readEventFile
	 * @param $file
	 * @return boolean
	 */
	public function readEventFile( $file ) {
		$this->welogger->log( "readEventFile called with file ".$file, WELogger::$LOG_LEVEL_INFO, "Import_tms_model.readEventFile" );
		$this->log('readEventFile called with file '.$file);

		$success	= TRUE;
		$path 		= $this->config->item('import_path').$this->importSubPath;

		$content 	= file_get_contents( $path.$file );
		$xml 		= new SimpleXMLElement( $content );

		$shipments = array();
		foreach( $xml->xpath('SubShipment') as $shipmentXml ) {

			foreach( $shipmentXml->xpath('Container') as  $containerXml ) {
				$item						= array();
				$item['hbl_hawb'] 			= $this->getValueFromXML( $shipmentXml, 'WayBillNumber' );
				$item['container_number'] 	= $this->getValueFromXML( $containerXml, 'ContainerNumber' );
				$item['id'] 				= $this->getShipmentId( $item['container_number'], $item['hbl_hawb'] );
				$shipments[]				= $item;
			}

		}

		$numOfTransportLegs = count( $xml->xpath( 'TransportLeg' ) );
		$data = array();

		if( $numOfTransportLegs > 1 ) {
			$data['transshipment_port'] 	= $this->getValueFromXML( $xml, 'TransportLeg[last()]/PortOfLoading/Code' );
			$data['first_leg_eta'] 			= $this->getValueFromXML( $xml, 'TransportLeg[1]/EstimatedArrival' );
			$data['first_leg_ata'] 			= $this->getValueFromXML( $xml, 'TransportLeg[1]/ActualArrival' );
			$data['last_leg_etd'] 			= $this->getValueFromXML( $xml, 'TransportLeg[last()]/EstimatedDeparture' );
			$data['last_leg_atd'] 			= $this->getValueFromXML( $xml, 'TransportLeg[last()]/ActualDeparture' );
			$data['lloyds_number_export'] 	= $this->getValueFromXML( $xml, 'TransportLeg[1]/VesselLloydsIMO' );
			$data['lloyds_number_import'] 	= $this->getValueFromXML( $xml, 'TransportLeg[last()]/VesselLloydsIMO' );
			$data['mother_vessel_name'] 	= $this->getValueFromXML( $xml, 'TransportLeg[1]/VesselName' );
			$data['voyage_number'] 			= $this->getValueFromXML( $xml, 'TransportLeg[1]/VoyageFlightNo' );
			$data['vessel_2'] 				= $this->getValueFromXML( $xml, 'TransportLeg[last()]/VesselName' );
			$data['voyage_2'] 				= $this->getValueFromXML( $xml, 'TransportLeg[last()]/VoyageFlightNo' );
		} else {
			$data['lloyds_number_export'] = $this->getValueFromXML($xml, 'TransportLeg[1]/VesselLloydsIMO');
			$data['lloyds_number_import'] = $data['lloyds_number_export'];
			$data['mother_vessel_name'] = $this->getValueFromXML($xml, 'TransportLeg[1]/VesselName');
			$data['voyage_number'] = $this->getValueFromXML($xml, 'TransportLeg[1]/VoyageFlightNo');
			$data['vessel_2'] = $data['mother_vessel_name'];
			$data['voyage_2'] = $data['voyage_number'];
		}

		$contractNumber = $this->getValueFromXML( $xml, 'TransportLeg[1]/Carrier/Contract' );
		if( !empty( $contractNumber ) ) $data['contract_number'] = $contractNumber;

		$contractType = $this->getValueFromXML( $xml, 'TransportLeg[1]/Carrier/ContractType' );
		if( !empty( $contractType ) ) $data['contract_type'] = $contractType;

		$data['carrier_booking_no'] 	= $this->getValueFromXML( $xml, 'BookingConfirmationReference' );
		//$data['cy_cut_off'] 			= $this->getValueFromXML( $xml, 'TransportLeg[1]/CutOff' );

		foreach( $shipments as $shipment ) {
			if( $shipment['id'] > 0 ) {
				$tmp = $data;
				$tmp['hbl_hawb'] 			= $shipment['hbl_hawb'];
				$tmp['container_number'] 	= $shipment['container_number'];

				$this->welogger->log( "write shipment to db ".var_export($tmp,true), WELogger::$LOG_LEVEL_INFO, "Import_tms_model.readEventFile" );
				$this->pcshipment_model->updateRecord( $shipment['id'], $tmp, false );

				$pcshipment = $this->pcshipment_model->getRecord( $shipment['id'], false );

				// set detention free days
				$this->setDetentionFreeDays( $pcshipment );

				// update corresponding shipment
				$this->pcshipment_model->updateRelatedShipment( $shipment['id'] );
			}
		}

		// move file to archive
		if( ENVIRONMENT == 'production' || ENVIRONMENT == 'staging' ) $this->moveFileToArchive( $file );
		return $success;
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	/**
	 * setDetentionFreeDays
	 * @param $pcshipment
	 * @return void
	 */
	private function setDetentionFreeDays( $pcshipment )
	{
		$id 				= $pcshipment['id'];
		$contractNumber 	= $pcshipment['contract_number'];
		$pcContainerType 	= $pcshipment['container_type'];
		$dfdContainerType 	= 0;

		// pcshipment container types:
		// 1|DC,2|HC,7|RF,8|GP,13|FL,14|HR,15|NO,16|HCRF
		//
		// detention_free_days container types:
		// 1|GP,2|NO,3|RF
		//
		// Mapping pcshipment : detention_free_days
		// 8|GP, 1|DC, 2|HC, 14|HR : 1|GP
		// 7|RF, 16|HCRF : 3|RF
		// 15|NO : 2|NO

		switch( $pcContainerType ) {
			case 8:
			case 1:
			case 2:
			case 14:
				$dfdContainerType = 1;
				break;
			case 7:
			case 16:
				$dfdContainerType = 3;
				break;
			case 15:
				$dfdContainerType = 2;
				break;
		}

		$this->welogger->log( "setDetentionFreeDays id:".$id." contractNumber:".$contractNumber." pcContainerType:".$pcContainerType." dfdContainerType:".$dfdContainerType, WELogger::$LOG_LEVEL_INFO, "Import_tms_model.setDetentionFreeDays" );

		if( !empty( $contractNumber ) && $dfdContainerType > 0 ) {
			$this->load->model('Detention_free_day_model', 'detention_free_day_model');
			$rows = $this->detention_free_day_model->getRecordsWhere( array( 'contract_number'=> $contractNumber, 'container_type'=> $dfdContainerType, 'hidden' => 0, 'deleted' => 0 ) );

			if( count( $rows ) > 0 ) {
				$data['detention_free_days'] = $rows[0]['free_days'];
				$this->welogger->log( "setDetentionFreeDays id:".$id." detention_free_days:".$data['detention_free_days'], WELogger::$LOG_LEVEL_INFO, "Import_tms_model.setDetentionFreeDays" );
				$this->pcshipment_model->updateRecord( $id, $data );
			}
		}
	}



}
/* End of file import_model.php */
/* Location: ./app/models/import_model.php */
