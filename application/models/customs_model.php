<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Customs_model extends Abstract_model {


	function __construct() {
		parent::__construct();
		$this->tablename = 'customs';
	}

	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select(
			$this->getAllFieldsFromTable( 'customs' ).', '.
			$this->getAllFieldsFromTable( 'contracts' )
		);
		$this->db->from( $this->tablename );
		$this->db->join('contracts', 'customs.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0', 'left');
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$query = $this->db->get();

		$supOptions = $this->getTCAgetMultiselectionOptions( 'contracts_supplier' );

		foreach ( $query->result_array() as $row ) {
			$row['contracts_supplier'] 					= ( $row['contracts_supplier'] != 0 && array_key_exists($row['contracts_supplier'], $supOptions) ) ? $supOptions[$row['contracts_supplier']] : '';
			$row['customs_customs_duty_rate_excl_fta'] 	= is_numeric( $row['customs_customs_duty_rate_excl_fta'] ) ?  ( $row['customs_customs_duty_rate_excl_fta'] * 100 ). '%' : $row['customs_customs_duty_rate_excl_fta'];
			$res[] = $row;
		}
		return $res;
	}

	/**
	 * getIdByContractNumber
	 * returns id of wanted contract number
	 *
	 * @param str $contractNumber - Contract number of record
	 * @return int - id of record
	 */
	public function getIdByContractNumber( $contractNumber ) {
		$out = 0;
		$query = $this->db->get_where( $this->tablename, array( 'contract_number' => $contractNumber, 'hidden' => 0, 'deleted' => 0 ) );
		if( $query->num_rows() > 0 ) {
			$row = $query->row_array();
			$out = $row['id'];
		}
		return $out;
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	/**
	 * configures where clause for db query, depending on values in searchform.
	 */
	public function setWhereClauseForSearchparams( $recordListSearchParams ) {
		$fieldnames = array(
			'customs_contract_number',
			'customs_product_code',
			'contracts_advertisement_week',
			'customs_tariff_classification',
		);
		foreach ( $fieldnames as $fname ) {
			$tmpValue = array_key_exists($fname, $recordListSearchParams) ? $recordListSearchParams[$fname][0] : '';
			if( !empty($tmpValue) ){
				switch($fname){
					case 'customs_contract_number':
						$this->db->where( 'customs.contract_number', $tmpValue );
						break;
					case 'customs_product_code':
						$this->db->where( 'customs.product_code', $tmpValue );
						break;
					case 'contracts_advertisement_week':
						$this->db->where( 'contracts.advertisement_week', $tmpValue );
						break;
					case 'customs_tariff_classification':
						$this->db->where( 'customs.tariff_classification', $tmpValue );
						break;
				}
			}
		}
	}

	/**
	 * getTCAgetMultiselectionOptions
	 *
	 * @return array
	 */
	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}

	/**
	 * getAllFieldsFromTable
	 *
	 * @return string
	 */
	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}
}

/* End of file activity_model.php */
/* Location: ./app/models/activity_model.php */
