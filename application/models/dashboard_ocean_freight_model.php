<?php
require_once( APPPATH.'models/abstract_model.php' );

class Dashboard_ocean_freight_model extends Abstract_model {

	public static $STATUS_DELAY 	= 'Delay';
	public static $STATUS_ON_TIME 	= 'On time';

	function __construct() {
		parent::__construct();
		$this->load->model('Supplier_model', 'supplier_model');
		$this->load->model('Discrepancy_model', 'discrepancy_model');
		$this->load->driver('cache', array('adapter' => 'file'));
	}


	/**
	 * getRecords
	 * returns the db-records with the given search params
	 *
	 * @return array - db record as array.
	 */
	public function getRecords( $filter=array() ) {
		if( !$res = $this->cache->get('dashboard_ocean_freight_getrecords') ) {
			$res = array();
			$this->db->select($this->getAllFieldsFromTable('deliveries') . ', ' . $this->getAllFieldsFromTable('contracts') . ', ' . $this->getAllFieldsFromTable('shipments') . ', ' . $this->getAllFieldsFromTable('shipments_deliveries_mm'));
			$this->db->from('contracts');
			$this->db->join('deliveries', 'deliveries.contract_id = contracts.id AND deliveries.hidden = 0 AND deliveries.deleted = 0');
			$this->db->join('shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
			$this->db->join('shipments', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left');

			$this->db->where(array('contracts.hidden' => 0, 'contracts.deleted' => 0));

			$this->db->where('( shipments.ata >= "' . date('Y-m-d', strtotime('-20 days', time())) . ' 00:00:00" OR shipments.ata = "0000-00-00 00:00:00" )');
			$this->db->where('shipments.eta >= "' . date('Y-m-d', strtotime('-1 year', time())) . ' 00:00:00"');
			$this->db->group_by('deliveries.id');
			$this->db->order_by('contracts.id');
			$query = $this->db->get();

			foreach ($query->result_array() as $row) {
				$res[] = $row;
			}
			$res = $this->prepareResultQueryForOutput($res);
		}

		if( count( $filter ) > 0 ) $res = $this->filterResult( $res, $filter );
		return $res;
	}

	/**
	 * getDeparturePieData
	 * returns the db-records with the given search params
	 *
	 * @return array - db record as array.
	 */
	public function getDeparturePieData() {
		$rows = $this->getRecords();

		$total 	= count( $rows );
		$delay 	= 0;
		$ontime = 0;
		foreach( $rows as $row ) {
			if( $row['status_departure'] == $this::$STATUS_DELAY ) $delay++;
			if( $row['status_departure'] == $this::$STATUS_ON_TIME ) $ontime++;
		}

		$ontimePercent = round( ($ontime*100)/$total, 2 );
		$delayPercent = round( ($delay*100)/$total, 2 );

		return array(
			'labels' => array( $this::$STATUS_ON_TIME, $this::$STATUS_DELAY ),
			'data' => array( $ontimePercent, $delayPercent ),
		);
	}

	/**
	 * getArrivalPieData
	 * returns the db-records with the given search params
	 *
	 * @return array - db record as array.
	 */
	public function getArrivalPieData() {
		$rows = $this->getRecords();

		$total 	= count( $rows );
		$delay 	= 0;
		$ontime = 0;
		foreach( $rows as $row ) {
			if( $row['status_arrival'] == $this::$STATUS_DELAY ) $delay++;
			if( $row['status_arrival'] == $this::$STATUS_ON_TIME ) $ontime++;
		}

		$ontimePercent = round( ($ontime*100)/$total, 2 );
		$delayPercent = round( ($delay*100)/$total, 2 );

		return array(
			'labels' => array( $this::$STATUS_ON_TIME, $this::$STATUS_DELAY ),
			'data' => array( $ontimePercent, $delayPercent ),
		);
	}

	/**
	 * generateCache
	 *
	 * @return
	 */
	public function generateCache() {
		$res = $this->getRecords();
		$this->cache->file->save('dashboard_ocean_freight_getrecords', $res, 600);
	}

	/**
	 * isCached
	 *
	 * @return
	 */
	public function isCached() {
		return file_exists( $this->config->item('app_path').'cache/dashboard_ocean_freight_getrecords' );
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function prepareResultQueryForOutput( $query_result_array ){
		$res = array();

		foreach ( $query_result_array as $row) {

			$row['status_arrival'] = '';
			$row['status_arrival_css'] = '';
			if( $row['shipments_ata'] == '0000-00-00 00:00:00' && strtotime( $row['shipments_eta'] ) < time() ) {
				$row['status_arrival'] = $this::$STATUS_DELAY;;
				$row['status_arrival_css'] = 'label label-important';
			} else if( strtotime( $row['shipments_ata'] ) > strtotime( $row['shipments_eta'] ) ) {
				$row['status_arrival'] = $this::$STATUS_DELAY;;
				$row['status_arrival_css'] = 'label label-important';
			} else {
				$row['status_arrival'] = $this::$STATUS_ON_TIME;;
				$row['status_arrival_css'] = 'label label-success';
			}

			$row['status_departure'] = '';
			$row['status_departure_css'] = 'label';
			if( $row['shipments_atd'] == '0000-00-00 00:00:00' && strtotime( $row['shipments_etd'] ) < time() ) {
				$row['status_departure'] = $this::$STATUS_DELAY;;
				$row['status_departure_css'] = 'label label-important';
			} else if( strtotime( $row['shipments_atd'] ) > strtotime( $row['shipments_etd'] ) ) {
				$row['status_departure'] = $this::$STATUS_DELAY;;
				$row['status_departure_css'] = 'label label-important';
			} else {
				$row['status_departure'] = $this::$STATUS_ON_TIME;;
				$row['status_departure_css'] = 'label label-success';
			}


			$dates = array(
				'contracts_actual_contract_ready_date',
				'shipments_approval_date',
				'shipments_etd',
				'shipments_eta',
				'shipments_atd',
				'shipments_ata',
				'contracts_aldi_au_wh_delivery_due_date',
			);

			foreach( $dates as $date ) {
				$row[$date.'_timestamp'] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? strtotime( $row[$date] ) : 0;
				$row[$date] = ( $row[$date] != "0000-00-00 00:00:00" && !empty( $row[$date] ) ) ? date( $this->config->item('date_format'), strtotime( $row[$date] ) ) : "";
			}

			if( isset( $row['shipments_destination_port'] ) ) 	$row['shipments_destination_port'] 	= $this->getDestinationPort( $row['shipments_destination_port'] );

			$row['comments_count'] =  $this->discrepancy_model->getCommentsCount( $row['contracts_id'] );
			$row['discrepancy'] = $row['comments_count'] > 0 ? 'yes' : 'no';
			$res[] = $row;
		}
		return $res;
	}

	private function filterResult( $res, $filter )
	{
		$out = array();
		if($filter['type'] == 'departure') {
			foreach( $res as $row ) {
				if ( $filter['value'] == 'ontime' ) {
					if( $row['status_departure'] == $this::$STATUS_ON_TIME ) $out[] = $row;
				} else {
					if( $row['status_departure'] == $this::$STATUS_DELAY ) $out[] = $row;
				}
			}
		} else if($filter['type'] == 'arrival') {
			foreach( $res as $row ) {
				if ( $filter['value'] == 'ontime' ) {
					if( $row['status_arrival'] == $this::$STATUS_ON_TIME ) $out[] = $row;
				} else {
					if( $row['status_arrival'] == $this::$STATUS_DELAY ) $out[] = $row;
				}
			}
		}
		return $out;
	}

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	/**
	 * getSupplierName
	 * returns supplier name
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getSupplierName( $id=0 ) {
		$supplier = $this->supplier_model->getRecord( $id );
		return ( count( $supplier ) > 0 ) ? $supplier['name'] : '';
	}

	/**
	 * getDestinationPort
	 * returns contract type
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getDestinationPort( $id=0 ) {
		$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$destinationPortOptions 		= $this->tca->getMultiselectionOptions( $tcaNode );
		return $destinationPortOptions[$id];
	}

}
