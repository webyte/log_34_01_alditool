<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Groupperm_model extends abstract_model {

	function __construct()
	{
		parent::__construct();
		$this->tablename = 'group_perms';
	}

	public function savePermission($perms_name, $permValue, $groupId) {
		$inserArray = array( 'name' => $perms_name, 'value' => $permValue, 'group_id' => $groupId );
		$recordId = $this->getPermsValue( $perms_name, $groupId );
		if( $recordId > 0 ) {
			$this->updateRecord($recordId, $inserArray, FALSE );
		} else {
			$this->createNew( $inserArray, FALSE );
		}
	}

	public function getPermissionValue($perm_name, $groupId) {
		$val = $this->getPermsValue( $perm_name, $groupId, 'value' );
		return $val;
	}

	/**
	 * getFieldPermissionsForTable
	 * returns array with all fields of the given table and the appropriate permissions that
	 * are set for the given usergroup.
	 *
	 * @param string tablename - Name of table
	 * @param int groupId - id of user group
	 * @return array - multidimensional array with entries for every single tablefield: array( 'perm_name' => $perm_name, 'perm_value' => $perm_value, 'label' => $label )
	 */
	public function getFieldPermissionsForTable( $tablename, $groupId ) {
		$field_perms = array();
		$fieldnames = $this->tca->getAllFieldnamesOfTable( $tablename );
		foreach ( $fieldnames as $field ) {
//			if( $field != 'hidden' && $field != 'deleted' && $field != 'crdate' && $field != 'modified' ) {
			if( $field != 'hidden' && $field != 'deleted' ) {
				$node_id = $this->tca->getIdByFieldName($field, $tablename);
				$fieldDisplaySettings = $this->tca->getParamValueWithFieldId($node_id, 'display');
				if( $fieldDisplaySettings == "1" ) {
					$perm_name 	= Acl::$tablefield_prefix.$tablename.'_'.$field;
					$label 		= (string)$this->tca->getColumnNodeById($node_id)->label;
					$perm_value	= $this->getPermissionValue($perm_name, $groupId);
					$field_perms[] = array( 'perm_name' => $perm_name, 'perm_value' => $perm_value, 'label' => $label );
				}
			}
		}
		return $field_perms;
	}

	/**
	 * getModulePermissions
	 * returns permissions for all modules of the application
	 *
	 * @param int groupId - id of usergroup
	 * @return array
	 */
	public function getModulePermissions( $groupId ){
		$modulepermissions = array();
		$menuConf = $this->config->item('main_menu');
		foreach ( $menuConf as $mainMenuItem ) {
			if( !array_key_exists('submenu', $mainMenuItem) ) {
				$modulepermissions[] = $this->gatherModulePermission($mainMenuItem, $groupId);
			} else {
				$modulepermissions[] = $this->gatherModulePermission($mainMenuItem, $groupId);
				foreach ( $mainMenuItem['submenu'] as $subMenuItem ) {
					$modulepermissions[] = $this->gatherModulePermission($subMenuItem, $groupId);
				}
			}
		}
		return $modulepermissions;
	}

	private function gatherModulePermission( $item, $groupId ) {
		$res = array();
		$res['title'] 		= $item['title'];
		$res['controller'] 	= Acl::$controller_prefix.$item['controller'];
		$res['perm_value'] 	= $this->getPermissionValue($res['controller'], $groupId);
		return $res;
	}

	public function getActionPermissionsForController( $controller, $actions, $groupId ) {

		$controllers_path = APPPATH.'controllers/';
		$class = $controllers_path.$controller.'.php';
		require_once( $class );
		$tmp = explode( '/', $controller );
		$controllerClass = end( $tmp );
		$rc = new ReflectionClass( $controllerClass );
		$public_methods = $rc->getMethods( ReflectionMethod::IS_PUBLIC );

		$action_perms = array();
		foreach ( $public_methods as $m_obj ) {
			if( $m_obj->name != "__construct" && $m_obj->class != "CI_Controller" ){
				if( in_array( $controller.'_'.$m_obj->name, $actions ) ) {
//       			$perm_name 	= "action_".$m_obj->class.'_'.$m_obj->name;
					$perm_name 	= Acl::$action_prefix.$controller.'_'.$m_obj->name;
					$label 		= $m_obj->name;
					$perm_value	= $this->getPermissionValue($perm_name, $groupId);
					$action_perms[] = array( 'perm_name' => $perm_name, 'perm_value' => $perm_value, 'label' => $label );
				}
			}
		}
		return $action_perms;
	}

	public function getCrudPermissionsForTable( $tablename, $groupId ) {
		$field_perms = array();
		$fieldnames = array('new', 'edit', 'delete');
		foreach ( $fieldnames as $field ) {
			$perm_name 	= Acl::$crud_prefix.$tablename.'_'.$field;
			$label 		= $field;
			$perm_value	= $this->getPermissionValue($perm_name, $groupId);
			$field_perms[] = array( 'perm_name' => $perm_name, 'perm_value' => $perm_value, 'label' => $label );
		}
		return $field_perms;
	}

	/**
	 * getPermsValue
	 * search for given group permissions.
	 * if they exist, the id is returned. otherwise 0.
	 * @param string perms_name - name of permission
	 * @param string groupId - group id.
	 * @param string field - name of db-field to return (default is id)
	 * @return int record id
	 */
	private function getPermsValue( $perms_name, $groupId, $field = 'id' ){
		$query = $this->db->get_where($this->tablename, array('name' => $perms_name, 'group_id' => $groupId ) );
		if( $query->num_rows() > 0 ) {
			$recorddata = $query->first_row('array');
			return $recorddata[$field];
		} else {
			return 0;
		}
	}

}

/* End of file groupperm_model.php */
/* Location: ./app/models/groupperm_model.php */