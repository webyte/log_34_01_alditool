<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Pcshipment_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'pcshipments';
	}

	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select('pcshipments.id AS id,
							pcshipments.hbl_hawb AS hbl_hawb,
							pcshipments.container_number AS container_number,
							pcshipments.po_number AS po_number,
							pcshipments.departure_port AS departure_port,
							pcshipments.destination_port AS destination_port');
		$this->db->from( $this->tablename );
//		$this->db->join('pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join('shipments', 'shipments.id = pcshipments.shipment_id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left');
		$this->db->join('shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join('deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0', 'left');
		$this->db->join('contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0', 'left');
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * @override
	 */
	public function getRecord( $id, $withShipments = TRUE ) {
		$this->load->model('Shipment_model', 'shipment_model');
		$res = parent::getRecord( $id );
		if( count( $res ) > 0 && !empty( $res['po_number'] ) && $withShipments ) {
//			$shipments 				= $this->shipment_model->getRecordsWhere( array( 'contract_numbers LIKE' => '%'.$res['po_number'].'%', 'hidden' => 0, 'deleted' => 0 ) );
//			$shipments 				= $this->prepareResultQueryForOverviewOutput( $shipments );
			$res['shipments'] 		= $this->getNotLinkedShipments( $res );
			$res['linkedShipments'] = $this->getLinkedShipments( $res );
		}
		return $res;
	}

	/**
	 * getHBLHAWB
	 * returns a json encoded array with associated hbl/hawb
	 * in the frontend dynamically.
	 *
	 * @param str $query - part of the wanted hbl/hawb
	 * @return array - db record as array.
	 */
	public function getHBLHAWB( $query ) {
		$this->db->like( 'hbl_hawb', $query );
		$this->db->where( array( 'hidden' => 0, 'deleted' => 0 ) );
		$query = $this->db->get( $this->tablename );
		return $query->result_array();
	}

	/**
	 * getContainerNumber
	 * returns a json encoded array with associated hbl/hawb
	 * in the frontend dynamically.
	 *
	 * @param str $hblHawb - hbl/hawb
	 * @return array - db record as array.
	 */
	public function getContainerNumber( $hblHawb ) {
		$this->db->where( 'hbl_hawb', $hblHawb );
		$this->db->where( array( 'hidden' => 0, 'deleted' => 0 ) );
		$query = $this->db->get( $this->tablename );
		return $query->result_array();
	}

	/**
	 * getRecord
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getPreparedRecord( $id ) {
		$record = $this->getRecord( $id );
		if( count( $record ) > 0 ) {
			unset( $record['id'] );
			unset( $record['hbl_hawb'] );
			unset( $record['mbl_mawb'] );
			unset( $record['obl'] );
			unset( $record['transshipment_port'] );
			unset( $record['carrier_booking_no'] );
			unset( $record['first_leg_eta'] );
			unset( $record['first_leg_ata'] );
			unset( $record['last_leg_etd'] );
			unset( $record['last_leg_atd'] );
			unset( $record['crdate'] );
			unset( $record['modified'] );
			unset( $record['hidden'] );
			unset( $record['deleted'] );

			$record['etd'] = ( $record['etd'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $record['etd'] ) ) : "";
			$record['eta'] = ( $record['eta'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $record['eta'] ) ) : "";
			$record['atd'] = ( $record['atd'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $record['atd'] ) ) : "";
			$record['ata'] = ( $record['ata'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $record['ata'] ) ) : "";
			$record['cus'] = ( $record['cus'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $record['cus'] ) ) : "";
			$record['container_loaded'] = ( $record['container_loaded'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $record['container_loaded'] ) ) : "";
			$record['container_unloaded'] = ( $record['container_unloaded'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $record['container_unloaded'] ) ) : "";
		}
		return $record;
	}

	/**
	 * getRecord
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function addHBLHAWBToShipment( $shipmentId, $hblHawb ) {
		$this->load->model('Shipment_model', 'shipment_model');

		$shipment = $this->shipment_model->getRecord( $shipmentId );

		if( !empty( $shipment['hbl_hawb'] ) ) {
			$items = explode( ',', $shipment['hbl_hawb'] );
			$found = FALSE;
			foreach( $items as $item ) {
				if( $item  == $hblHawb ) $found = TRUE;
			}
			if( !$found ) {
				$hblHawb = $shipment['hbl_hawb'].','.$hblHawb;
			} else {
				$hblHawb = $shipment['hbl_hawb'];
			}
		}

		$this->welogger->log( "addHBLHAWBToShipment - shipmentId: ".$shipmentId.", hblHawb: ".$hblHawb, WELogger::$LOG_LEVEL_WARNING, "pcshipment_model.addHBLHAWBToShipment" );

		$this->shipment_model->updateRecord( $shipmentId, array( 'hbl_hawb' => $hblHawb ) );
	}

	/**
	 * removeHBLHAWBFromShipment
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function removeHBLHAWBFromShipment( $pcshipmentId, $shipmentId ) {
		$this->load->model('Shipment_model', 'shipment_model');
		
		$pcshipment = $this->getRecord( $pcshipmentId );
		$shipment 	= $this->shipment_model->getRecord( $shipmentId );

		$tmp 		= array();
		$items = explode( ',', $shipment['hbl_hawb'] );
		foreach( $items as $item ) {
			if( $item != $pcshipment['hbl_hawb'] ) $tmp[] = $item;
		}
		$hblHawb = implode(',', $tmp);

		$pcshipments = $this->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
		$containerNumber = ( count( $pcshipments ) > 0 ) ? $pcshipment['container_number'] : 'dummy';

		$this->shipment_model->updateRecord( $shipmentId, array( 'hbl_hawb' => $hblHawb, 'container_number' => $containerNumber, 'seal_number' => '' ) );
	}

	/**
	 * addHBLHAWBToShipmentDelivery
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function addHBLHAWBToShipmentDelivery( $poNumber, $shipmentId, $hblHawb ) {
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');

		$deliveries = array();
		$poNumbers = explode(',',$poNumber);
		$poNumbers = array_map('trim',$poNumbers);

		foreach( $poNumbers as $contractNumber ) {
			$tmp = $this->delivery_model->getRecordsWhere( array( 'contract_number' => $contractNumber, 'hidden' => 0, 'deleted' => 0 ) );
			$deliveries = array_merge($deliveries, $tmp);
		}

		foreach( $deliveries as $delivery ) {
			$shipmentDeliveries = $this->shipment_delivery_model->getRecordsWhere( array( 'delivery_id' => $delivery['id'], 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
			if( count($shipmentDeliveries) > 0 ) {
				$shipmentDelivery 	= $shipmentDeliveries[0];
				if (!empty($shipmentDelivery['hbl_hawb'])) {
					$items = explode(',', $shipmentDelivery['hbl_hawb']);
					$items 		= array_merge( $items, explode( ',', $hblHawb ) );
					$items 		= array_unique( $items );
					$hblHawb 	= implode( ',', $items);
				}
				if (!empty($shipmentDelivery['id'])) {
					$this->welogger->log("addHBLHAWBToShipmentDelivery - shipmentDeliveryId: " . $shipmentDelivery['id'] . ", shipmentId: " . $shipmentId . ", hblHawb: " . $hblHawb . ", poNumber: " . $poNumber, WELogger::$LOG_LEVEL_WARNING, "pcshipment_model.addHBLHAWBToShipmentDelivery");
					$this->shipment_delivery_model->updateRecord($shipmentDelivery['id'], array('hbl_hawb' => $hblHawb), false );
				}
			}
		}
	}

	/**
	 * addHBLHAWBToShipmentDelivery
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function addHBLHAWBToShipmentDeliveryByDeliveryId( $deliveryId, $shipmentId, $hblHawb ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');

		$shipmentDeliveries = $this->shipment_delivery_model->getRecordsWhere( array( 'delivery_id' => $deliveryId, 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
		if( count($shipmentDeliveries) > 0 ) {
			$shipmentDelivery 	= $shipmentDeliveries[0];
			if (!empty($shipmentDelivery['hbl_hawb'])) {
				$items 		= explode(',', $shipmentDelivery['hbl_hawb']);
				$items 		= array_merge( $items, explode( ',', $hblHawb ) );
				$items 		= array_unique( $items );
				$hblHawb 	= implode( ',', $items);
			}
			$this->shipment_delivery_model->updateRecord( $shipmentDelivery['id'], array( 'hbl_hawb' => $hblHawb ) );
		}
	}

	/**
	 * removeHBLHAWBFromShipmentDelivery
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function removeHBLHAWBFromShipmentDelivery( $shipmentId, $hblHawb ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');

		$shipmentDeliveries = $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'hbl_hawb LIKE' => '%'.$hblHawb.'%', 'hidden' => 0, 'deleted' => 0 ) );

		foreach( $shipmentDeliveries as $shipmentDelivery ) {
			$tmp 		= array();
			$newHblHawb	= '';

			$items = explode( ',', $shipmentDelivery['hbl_hawb'] );
			foreach( $items as $item ) {
				if( $item != $hblHawb ) $tmp[] = $item;
			}
			$newHblHawb = implode(',', $tmp);
			$this->shipment_delivery_model->updateRecord( $shipmentDelivery['id'], array( 'hbl_hawb' => $newHblHawb ) );
		}
	}

	/**
	 * getRelatedShipmentsByPoNumber
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getRelatedShipmentsByPoNumber( $poNumber ) {
		$this->db->select( $this->getAllFieldsFromTable( 'shipments' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ).', '.$this->getAllFieldsFromTable( 'deliveries' ) );
		$this->db->from( 'shipments' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join( 'deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0', 'left');
		$this->db->where_in( 'deliveries.contract_number', explode( ',',$poNumber ) );
		$this->db->where( array( 'shipments.hidden' => 0, 'shipments.deleted' => 0 ) );
		$this->db->group_by( 'shipments.id' );
		$query = $this->db->get();
		return $query->result_array();
	}


	/**
	 * updateRelatedShipment
	 */
	public function updateRelatedShipment( $id, $deliveryId=0 ) {
		$pcShipment = $this->getRecord( $id );
		$poNumber	= $pcShipment['po_number'];
		$hblHawb	= $pcShipment['hbl_hawb'];
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');
		$shipmentDeliveries = $this->pcshipment_delivery_model->getRecordsWhere( array( 'pcshipment_id' => $id, 'hidden' => 0, 'deleted' => 0 ) );

		if( count( $shipmentDeliveries ) > 0 ) {
			$shipmentId = $shipmentDeliveries[0]['shipment_id'];

			if ($shipmentId > 0) {
				$data 						= array();
				$data['incoterm_code'] 		= $pcShipment['incoterm_code'];
				$data['carrier'] 			= $pcShipment['carrier'];
				$data['mother_vessel_name'] = $pcShipment['mother_vessel_name'];
				$data['departure_port'] 	= $pcShipment['departure_port'];
				$data['destination_port'] 	= $pcShipment['destination_port'];
				$data['etd'] 				= $pcShipment['etd'];
				$data['eta'] 				= $pcShipment['eta'];
				$data['container_type'] 	= $pcShipment['container_type'];
				$data['container_size'] 	= $pcShipment['container_size'];
				$data['container_number'] 	= $pcShipment['container_number'];
				$data['shipment_number'] 	= $pcShipment['shipment_number'];
				$data['seal_number'] 		= $pcShipment['seal_number'];
				$data['voyage_number'] 		= $pcShipment['voyage_number'];
				$data['vessel_2'] 			= $pcShipment['vessel_2'];
				$data['voyage_2'] 			= $pcShipment['voyage_2'];
				$data['container_tare_weight'] = $this->getContainerTareWeight($pcShipment['container_type'], $pcShipment['container_size']);
				$data['contract_number'] 	= $pcShipment['contract_number'];
				$data['contract_type'] 		= $pcShipment['contract_type'];
				$data['detention_free_days'] = $pcShipment['detention_free_days'];
				$data['carrier_booking_number'] = $pcShipment['carrier_booking_no'];
				//$data['cy_cut_off'] 		= $pcShipment['cy_cut_off'];

				if ($pcShipment['atd'] != '0000-00-00 00:00:00' && !empty($pcShipment['atd'])) $data['atd'] = $pcShipment['atd'];
				if ($pcShipment['ata'] != '0000-00-00 00:00:00' && !empty($pcShipment['ata'])) $data['ata'] = $pcShipment['ata'];
				if ($pcShipment['cus'] != '0000-00-00 00:00:00' && !empty($pcShipment['cus'])) $data['cus'] = $pcShipment['cus'];
				if ($pcShipment['aqc'] != '0000-00-00 00:00:00' && !empty($pcShipment['aqc'])) $data['aqc'] = $pcShipment['aqc'];
				if ($pcShipment['container_loaded'] != '0000-00-00 00:00:00' && !empty($pcShipment['container_loaded'])) $data['container_loaded'] = $pcShipment['container_loaded'];
				if ($pcShipment['container_unloaded'] != '0000-00-00 00:00:00' && !empty($pcShipment['container_unloaded'])) $data['container_unloaded'] = $pcShipment['container_unloaded'];

				$this->load->model('Shipment_model', 'shipment_model');
				$this->shipment_model->updateRecord($shipmentId, $data);

				// set approval
				if( $data['atd'] != '0000-00-00 00:00:00' ) $this->shipment_model->setApproval( $shipmentId );

//				if( $deliveryId == 0 ) {
//					$this->addHBLHAWBToShipmentDelivery($poNumber, $shipmentId, $hblHawb);
//				} else {
//					$this->addHBLHAWBToShipmentDeliveryByDeliveryId($deliveryId, $shipmentId, $hblHawb);
//				}
			}
		}
	}

	/**
	 * getStatusXMLExportRecord
	 *
	 * @param number $id
	 * @return array - db record as array.
	 */
	public function getStatusXMLExportRecord( $id, $event ) {
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');
		$res = array();

		$this->db->select(
			$this->getAllFieldsFromTable( 'pcshipments' ).', '.
			$this->getAllFieldsFromTable( 'deliveries' ).', '.
			$this->getAllFieldsFromTable( 'contracts' ).', '.
			$this->getAllFieldsFromTable( 'shipments' ).', '.
			$this->getAllFieldsFromTable( 'shipments_deliveries_mm' )
		);
		$this->db->from( 'pcshipments' );
		$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.pcshipment_id = pcshipments.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0');
		$this->db->join( 'shipments', 'shipments.id = pcshipments_deliveries_mm.shipment_id AND shipments.hidden = 0 AND shipments.deleted = 0');
		$this->db->join( 'deliveries', 'deliveries.id = pcshipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0');
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0');
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0');
		$this->db->where( array( 'pcshipments.id' => $id, 'pcshipments.hidden' => 0, 'pcshipments.deleted' => 0 ) );
		$this->db->group_by( 'pcshipments.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$row['pcShipmentDeliveries'] = $this->getDeliveriesByPCShipmentId( $id );
			$row['shipmentDeliveries'] = $this->getDeliveriesByShipmentId( $row['shipments_id'] );
			$row['gross_weight'] = 0;
			foreach( $row['shipmentDeliveries'] as $delivery ) {
				$row['gross_weight'] += $delivery['shipments_deliveries_mm_gross_weight'];
			}
			$row['gross_weight'] += $row['pcshipments_container_tare_weight'];
			$row['event_code'] = $event;
			$row['event_date'] = date('Y-m-d', strtotime( $row['pcshipments_'.$event] ) );

			$res[] = $row;
		}
		return $this->prepareResultQueryForStatusXMLOutput( $res );
	}


	/**
	 * processEventDatesInitially
	 */
	public function processEventDatesInitially( $latestRow )
	{
		$previousRow = array(
			'etd' => '0000-00-00 00:00:00',
			'eta' => '0000-00-00 00:00:00',
			'atd' => '0000-00-00 00:00:00',
			'ata' => '0000-00-00 00:00:00',
			'cus' => '0000-00-00 00:00:00',
		);
		$this->processEventDates( $latestRow, $previousRow );
	}

	/**
	 * processEventDates
	 */
	public function processEventDates( $latestRow, $previousRow ) {
		// check if relation to delivery exists
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');
		$id 	= $latestRow['id'];
		$rows 	= $this->pcshipment_delivery_model->getRecordsByPcshipmentId( $id );
		if( count( $rows ) <= 0 ) return;

		// check event dates
		$events = array('etd', 'eta', 'atd', 'ata', 'cus');
		foreach( $events as $event ) {
			if( strtotime( $previousRow[$event] ) != strtotime( $latestRow[$event] ) && $latestRow[$event] != '0000-00-00 00:00:00' ) $this->processStatusXMLExport( $latestRow['id'], $event );
		}
	}

	/**
	 * processStatusXMLExport
	 * @return string
	 */
	public function processStatusXMLExport( $id, $event ) {
		$archivePath 	= $this->config->item('archive_path');
		$data 			= $this->getStatusXMLExportRecord( $id, $event );

		// update counter
		$this->updateCounter( $data[0]['contracts_id'] );
		$counter 		= $this->getTotalCounter();
		$contractType	= str_replace(' ', '', $data[0]['contracts_contract_type']);

		$file 			= $contractType.'_'.date('YmdHis').'_'.$counter.'_Shipment.xml';
		$xml 			= $this->load->view('pcshipment/pcshipment_xml', $data[0], true);

		$this->load->helper('file');
		write_file($archivePath.$file, $xml);
		$this->welogger->log( "status xml generated - shipmentId: ".$id.", file: ".$file, WELogger::$LOG_LEVEL_INFO, "pcshipment.processStatusXMLExport" );

		$exportPath = $this->config->item('export_path').'status/';

		copy( $archivePath.$file, $exportPath.$file );

		if( ENVIRONMENT != 'production' ) $this->sendMail( $data[0], $archivePath.$file );

		return $file;
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	/**
	 * getDeliveriesByPCShipmentId
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getDeliveriesByPCShipmentId( $id ) {
		$this->load->model('Delivery_model', 'delivery_model');

		$this->db->select(
			$this->getAllFieldsFromTable( 'deliveries' ).', '.
			$this->getAllFieldsFromTable( 'contracts' ).', '.
			$this->getAllFieldsFromTable( 'shipments_deliveries_mm' )
		);
		$this->db->from( 'deliveries' );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->db->join( 'pcshipments_deliveries_mm', 'pcshipments_deliveries_mm.delivery_id = deliveries.id AND pcshipments_deliveries_mm.hidden = 0 AND pcshipments_deliveries_mm.deleted = 0' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->where( array( 'pcshipments_deliveries_mm.pcshipment_id' => $id, 'deliveries.hidden' => 0, 'deliveries.deleted' => 0 ) );
		$this->db->group_by( 'deliveries.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $this->prepareResultQueryForStatusXMLOutput( $res );
	}

	/**
	 * getDeliveriesByShipmentId
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getDeliveriesByShipmentId( $id ) {
		$this->load->model('Delivery_model', 'delivery_model');

		$this->db->select(
			$this->getAllFieldsFromTable( 'deliveries' ).', '.
			$this->getAllFieldsFromTable( 'contracts' ).', '.
			$this->getAllFieldsFromTable( 'shipments_deliveries_mm' )
		);
		$this->db->from( 'deliveries' );
		$this->db->join( 'contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0' );
		$this->db->where( array( 'shipments_deliveries_mm.shipment_id' => $id, 'deliveries.hidden' => 0, 'deliveries.deleted' => 0 ) );
		$this->db->group_by( 'deliveries.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$res[] = $row;
		}
		return $this->prepareResultQueryForStatusXMLOutput( $res );
	}

	private function prepareResultQueryForOverviewOutput( $query_result_array ){
		$this->load->model('Dc_model', 'dc_model');
		$res 		= array();
		$desOptions = $this->getTCAgetMultiselectionOptions( 'shipments_destination_port' );
		foreach ( $query_result_array as $row) {
			$row['destination_port'] 			= ( isset( $row['destination_port'] ) && $row['destination_port'] != 0 && array_key_exists($row['destination_port'], $desOptions) ) ? $desOptions[$row['destination_port']] : '';
			$dc									= ( isset( $row['dc'] ) ) ? $this->dc_model->getRecord( $row['dc'] ) : array('dc' => '');
			$row['dc']							= $dc['dc'];
			$res[] 								= $row;
		}
		return $res;
	}

	private function prepareResultQueryForStatusXMLOutput( $query_result_array ){
		$res = array();
		foreach ( $query_result_array as $row) {
			$tcaNode 						= $this->tca->getColumnNodeById( 'pcshipments_incoterm_code' );
			$options 						= $this->tca->getMultiselectionOptions( $tcaNode );
			$row['pcshipments_incoterm_code'] = ( isset( $row['pcshipments_incoterm_code'] ) && $row['pcshipments_incoterm_code'] != 0 && array_key_exists($row['pcshipments_incoterm_code'], $options) ) ? $options[$row['pcshipments_incoterm_code']] : '';

			$tcaNode 						= $this->tca->getColumnNodeById( 'shipments_traffic_type' );
			$options 						= $this->tca->getMultiselectionOptions( $tcaNode );
			$row['shipments_traffic_type'] 	= ( isset( $row['shipments_traffic_type'] ) && $row['shipments_traffic_type'] != 0 && array_key_exists($row['shipments_traffic_type'], $options) ) ? $options[$row['shipments_traffic_type']] : '';

			$tcaNode 						= $this->tca->getColumnNodeById( 'pcshipments_destination_port' );
			$options 						= $this->tca->getMultiselectionOptions( $tcaNode );
			$row['pcshipments_destination_port'] = ( isset( $row['pcshipments_destination_port'] ) && $row['pcshipments_destination_port'] != 0 && array_key_exists($row['pcshipments_destination_port'], $options) ) ? $options[$row['pcshipments_destination_port']] : '';

			$tcaNode 						= $this->tca->getColumnNodeById( 'contracts_contract_type' );
			$options 						= $this->tca->getMultiselectionOptions( $tcaNode );
			$row['contracts_contract_type'] = ( isset( $row['contracts_contract_type'] ) && $row['contracts_contract_type'] != 0 && array_key_exists($row['contracts_contract_type'], $options) ) ? $options[$row['contracts_contract_type']] : '';

			$row['shipments_dc'] 			= $this->getShipmentDC( $row['shipments_dc'] );
			$row['contracts_supplier'] 		= $this->getSupplierName( $row['contracts_supplier'] );
			$row['contracts_supplier'] 		= htmlspecialchars( $row['contracts_supplier'], ENT_XML1);

			$tcaNode 				= $this->tca->getColumnNodeById( 'pcshipments_container_size' );
			$options 				= $this->tca->getMultiselectionOptions( $tcaNode );
			$containerSize			= ( isset( $row['pcshipments_container_size'] ) && $row['pcshipments_container_size'] != 0 && array_key_exists($row['pcshipments_container_size'], $options) ) ? $options[$row['pcshipments_container_size']] : '';

			$tcaNode 				= $this->tca->getColumnNodeById( 'pcshipments_container_type' );
			$options 				= $this->tca->getMultiselectionOptions( $tcaNode );
			$containerType			= ( isset( $row['pcshipments_container_type'] ) && $row['pcshipments_container_type'] != 0 && array_key_exists($row['pcshipments_container_type'], $options) ) ? $options[$row['pcshipments_container_type']] : '';
			$row['container_type'] = $containerSize.$containerType;

			$row['shipments_dg'] 				= ( $row['shipments_dg'] != 0 ) ? "Y" : "N";
			$row['shipments_edo_sent'] 			= ( $row['shipments_edo_sent'] != 0 ) ? "Y" : "N";
			$row['shipments_cus'] 				= ( $row['shipments_cus'] != '0000-00-00 00:00:00' ) ? "Y" : "N";
			$res[] = $row;
		}

		return $res;
	}

	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}

	private function getNotLinkedShipments( $pcshipment ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');
		$out = array();

		if( !empty( $pcshipment['po_number'] ) ) {

			$shipments = $this->getRelatedShipmentsByPoNumber( $pcshipment['po_number'] );

			foreach ( $shipments as $row ) {

				$shipmentId 	= $row['shipments_id'];
				$pcShipmentId 	= $pcshipment['id'];
				$shipment		= array();
				$delivery 						= $this->getDataByShipmentId( $row['shipments_id'] );
				$shipment['id'] 				= $row['shipments_id'];
				$shipment['hbl_hawb'] 			= $row['shipments_hbl_hawb'];
				$shipment['dc'] 				= $row['shipments_dc'];
				$shipment['dcs'] 				= $delivery['dcs'];
				$shipment['unit_quantities'] 	= $delivery['unit_quantities'];
				$shipment['case_quantities'] 	= $delivery['case_quantities'];
				$shipment['contract_numbers'] 	= $delivery['contract_numbers'];

				$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId($shipmentId);
				foreach( $deliveries as $delivery ) {
					$rows 	= $this->pcshipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'pcshipment_id' => $pcShipmentId, 'delivery_id' => $delivery['id'], 'hidden' => 0, 'deleted' => 0 ) );
					$tmp 	= array();
					$tmp 	= $delivery;
					$tmp['isLinked'] = ( count( $rows  ) > 0 ) ? TRUE : FALSE;
					$shipment['deliveries'][]	= $tmp;
				}
				if( !$this->doesShipmentHasUnusedDeliveries( $shipmentId, $pcShipmentId ) ) $out[] = $shipment;
			}
		}
		$out = $this->prepareResultQueryForOverviewOutput( $out );
		return $out;
	}

	private function doesShipmentHasUnusedDeliveries( $shipmentId, $pcShipmentId ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');
		$pcShipmentDeliveryIds 	= array();
		$shipmentDeliveryIds 	= array();
		$diff					= array();
		$pcShipments 			= $this->pcshipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'pcshipment_id' => $pcShipmentId, 'hidden' => 0, 'deleted' => 0 ) );
		$shipments 				= $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $pcShipments as $pcShipment) {
			$pcShipmentDeliveryIds[] = $pcShipment['delivery_id'];
		}
		foreach( $shipments as $shipment) {
			$shipmentDeliveryIds[] = $shipment['delivery_id'];
		}
		$diff = array_diff( $pcShipmentDeliveryIds, $shipmentDeliveryIds );
		return count( $diff ) > 0 ? TRUE : FALSE;
	}

	private function getLinkedShipments( $pcshipment ) {
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');
		$this->load->model('Shipment_model', 'shipment_model');
		$this->load->model('Delivery_model', 'delivery_model');
		$out 				= array();
		$records 			= $this->pcshipment_delivery_model->getRecordsByPcshipmentId( $pcshipment['id'] );
		foreach( $records as $record ) {
			if( isset($record['shipment_id']) && $record['shipment_id'] > 0 ){
				$shipmentId						= $record['shipment_id'];
				$deliveryId						= $record['delivery_id'];
				$shipment 						= $this->shipment_model->getRecord( $shipmentId );
				$data 							= $this->getDataByShipmentId( $shipmentId );
				$delivery 						= $this->delivery_model->getRecord( $deliveryId );
				$shipment['delivery_id'] 		= $deliveryId;
				$shipment['shipment_id'] 		= $shipmentId;
				$shipment['unit_quantity'] 		= $data['unit_quantities'];
				$shipment['case_quantity'] 		= $data['case_quantities'];
				$shipment['contract_number'] 	= $data['contract_numbers'];
				$shipment['deliveries_dc'] 		= $delivery['dc'];
				$out[]							= $shipment;
			}
		}

		$out = $this->prepareResultQueryForOverviewOutput( $out );
		return $out;
	}

	private function getDataByShipmentId( $id ) {

		$out 				= array();
		$dcs 				= array();
		$unitQuantities 	= array();
		$caseQuantities 	= array();
		$contractNumbers 	= array();
		$deliveryId 		= 0;

		$this->db->select( $this->getAllFieldsFromTable( 'deliveries' ).', '.$this->getAllFieldsFromTable( 'shipments_deliveries_mm' ) );
		$this->db->from( 'deliveries' );
		$this->db->join( 'shipments_deliveries_mm', 'shipments_deliveries_mm.delivery_id = deliveries.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
		$this->db->where( array( 'deliveries.hidden' => 0, 'deliveries.deleted' => 0,  'shipments_deliveries_mm.shipment_id' => $id ) );
		$this->db->group_by( 'deliveries.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			if( !empty( $row['deliveries_contract_number'] ) ) 					$contractNumbers[] 	= $row['deliveries_contract_number'];
			if( !empty( $row['deliveries_dc'] ) ) 								$dcs[] 				= $row['deliveries_dc'];
			if( !empty( $row['shipments_deliveries_mm_unit_quantity'] ) ) 		$unitQuantities[] 	= $row['shipments_deliveries_mm_unit_quantity'];
			if( !empty( $row['shipments_deliveries_mm_case_quantity'] ) ) 		$caseQuantities[] 	= $row['shipments_deliveries_mm_case_quantity'];
			if( !empty( $row['deliveries_id'] ) ) 								$deliveryId 		= $row['deliveries_id'];
		}

		$out['delivery_id'] 		= $deliveryId;
		$out['dcs'] 				= implode( ', ', array_unique( $dcs ) );
		$out['unit_quantities'] 	= implode( ', ', array_unique( $unitQuantities ) );
		$out['case_quantities'] 	= implode( ', ', array_unique( $caseQuantities ) );
		$out['contract_numbers'] 	= implode( ', ', array_unique( $contractNumbers ) );

		return $out;
	}

	private function getAllFieldsFromTable( $table ){
		$tmp = array();
		$fields = $this->db->list_fields( $table );
		foreach ( $fields as $field ) {
			$tmp[] =  $table.'.'.$field.' AS '.$table.'_'.$field;
		}
		return implode( ', ', $tmp );
	}

	private function getContainerTareWeight( $containerType, $containerSize ) {
		$this->load->model('Container_type_model', 'container_type_model');
		$out		= '';
		$options 	= $this->getTCAgetMultiselectionOptions('pcshipments_container_type');
		$cType 		= ( array_key_exists($containerType, $options) ) ? $options[$containerType] : '';
		$options 	= $this->getTCAgetMultiselectionOptions('pcshipments_container_size');
		$cSize		= ( array_key_exists($containerSize, $options) ) ? $options[$containerSize] : '';

		$rows = $this->container_type_model->getRecordsWhere( array( 'container_type'=> $cType, 'container_size'=> $cSize, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $rows ) ) {
			$out = $rows[0]['tare_weight'];
		}

		return $out;
	}

	/**
	 * getShipmentDCById
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return string
	 */
	private function getShipmentDC( $id ) {
		$this->load->model('Dc_model', 'dc_model');
		$dc = $this->dc_model->getRecord( $id );
		return ( count( $dc ) > 0 ) ? $dc['dc'] : '';
	}

	/**
	 * getSupplierName
	 * returns supplier name
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - supplier name
	 */
	private function getSupplierName( $id=0 ) {
		$this->load->model('Supplier_model', 'supplier_model');
		$supplier = $this->supplier_model->getRecord( $id );
		return ( count( $supplier ) > 0 ) ? $supplier['name'] : '';
	}

	/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data, $file ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
		$subject 	= 'Contract '.$data['contracts_contract_number'].': Event '.$data['event_code'].' was updated to '.$data['event_date'];
		$body 		= '';

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		$this->email->attach( $file );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Pcshipment_model.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Pcshipment_model.sendMail' );
			return true;
		}
		$this->email->clear(TRUE);
	}

	private function updateCounter( $id ) {
		$this->load->model('Contract_model', 'contract_model');
		$contract = $this->contract_model->getRecord( $id );
		$this->contract_model->updateRecord( $id, ['status_export_counter' => $contract['status_export_counter'] + 1 ] );
	}

	private function getTotalCounter() {
		$this->db->select('SUM(status_export_counter) AS status_export_counter');
		$this->db->from('contracts');
		$query = $this->db->get();
		$res = $query->result_array();
		$counter = $res[0]['status_export_counter'];
		return sprintf("%05d", $counter);
	}

}

/* End of file contracts.php */
/* Location: ./app/models/pcshipment_model.php */
