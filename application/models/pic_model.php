<?php
require_once ( APPPATH.'models/abstract_model'.EXT);

class Pic_model extends Abstract_model {

    function __construct() {
        parent::__construct();
        $this->tablename = 'pics';
    }

    /**
     * getMailByOnSaleWeek
     *
     * @param str $onSaleWeek - on sale week
     * @return string - mail
     */
    public function getMailByOnSaleWeek( $onSaleWeek ) {
        $out = array();
        $this->db->where( array( 'on_sale_week' => $onSaleWeek, 'hidden' => 0, 'deleted' => 0 ) );
        $query = $this->db->get( $this->tablename );
        if( $query->num_rows() > 0 ) {
			foreach ( $query->result_array() as $row ) {
				$out[] = $row['email'];
			}
        }
        return implode( ', ', $out );
    }
}

/* End of file pic_model.php */
/* Location: ./app/models/pic_model.php */
