<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Shipment_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->load->model('Comment_model', 'comment_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->tablename = 'shipments';
	}

	/**
	 * getOverviewRecords
	 * returns the db-record with the given id as array
	 *
	 * @param int $id - id of record to retrieve
	 * @return array - db record as array.
	 */
	public function getOverviewRecords( $recordListSearchParams, $overviewFields ) {
		$res = array();
		$this->db->select('shipments.id AS id,
							shipments.hbl_hawb AS hbl_hawb,
							shipments.container_number AS container_number,
							contracts.logwin_origin_office AS logwin_origin_office,
							shipments.contract_numbers AS contract_number,
							shipments.dcs AS dc,
							contracts.advertisement_week AS advertisement_week,
							contracts.fob_date AS fob_date,
							contracts.aldi_au_wh_delivery_due_date AS aldi_au_wh_delivery_due_date,
							shipments.departure_port AS departure,
							shipments.destination_port AS destination');
		$this->db->from( $this->tablename );
		$this->db->join('shipments_deliveries_mm', 'shipments_deliveries_mm.shipment_id = shipments.id AND shipments_deliveries_mm.hidden = 0 AND shipments_deliveries_mm.deleted = 0', 'left');
		$this->db->join('deliveries', 'deliveries.id = shipments_deliveries_mm.delivery_id AND deliveries.hidden = 0 AND deliveries.deleted = 0', 'left');
		$this->db->join('contracts', 'deliveries.contract_id = contracts.id AND contracts.hidden = 0 AND contracts.deleted = 0', 'left');
		$loo = $this->session->userdata('logwin_origin_office');
		if( !empty( $loo ) ) $this->db->where( array( 'contracts.logwin_origin_office' => $loo ) );
		$this->setWhereClauseForSearchparams( $recordListSearchParams );
		$this->db->where( array( $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
//			// related deliveries
//			$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId( $row['id'] );
//			$contractNumbers = array();
//			$dcs = array();
//			foreach( $deliveries as $delivery ) {
//				$contractNumbers[] = $delivery['contract_number'];
//				$dcs[] =  $delivery['dc'];
//			}
//			$row['contract_number'] = implode( ', ', array_unique( $contractNumbers ) );
//			$row['dc'] = implode( ', ', array_unique( $dcs ) );
			$row['hbl_hawb'] = str_replace( ',',', ',$row['hbl_hawb']);
			$res[] = $row;
		}
		return $this->prepareResultQueryForOverviewOutput( $res );
	}

	/**
	 * @override
	 */
	public function getRecord( $id ) {
		$res = parent::getRecord( $id );
		if( count( $res ) > 0 ) $res['comment'] = $this->comment_model->getCommentHistory( $id );
		return $res;
	}

	/**
	 * getAdvertisementWeek
	 */
	public function getAdvertisementWeek( $id ) {
		$out = '';
		$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId( $id );
		if( count( $deliveries ) > 0 ) $out = $deliveries[0]['advertisement_week'];
		return $out;
	}

	/**
	 * getLOOContactMail
	 */
	public function getLOOContactMail( $shipmentId ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Office_model', 'office_model');

		$out 		= '';
		$tmp 		= array();
		$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId( $shipmentId );
		foreach( $deliveries as $delivery ) {
			$contract 	= $this->contract_model->getRecord( $delivery['contract_id'] );
			$office 	= $this->office_model->getRecord( $contract['logwin_origin_office'] );
			$tmp[] 		= $office['contact_mail'];
		}
		$out = implode( ',', array_unique( $tmp ) );
		return $out;
	}

	/**
	 * getLOOTelexMail
	 */
	public function getLOOTelexMail( $shipmentId ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Office_model', 'office_model');

		$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId( $shipmentId );
		$contract 	= $this->contract_model->getRecord( $deliveries[0]['contract_id'] );
		$office 	= $this->office_model->getRecord( $contract['logwin_origin_office'] );

		return $office['telex_mail'];
	}

	/**
	 * getContainerNumber
	 * returns array with associated hbl/hawb
	 * in the frontend dynamically.
	 *
	 * @param str $hblHawb - hbl/hawb
	 * @return array - db record as array.
	 */
	public function getContainerNumbersByHblHawb( $hblHawb ) {

		//$result = $this->getRecordsWhere( array( 'hbl_hawb LIKE' => '%'.$hblHawb.'%', 'hidden' => 0, 'deleted' => 0 ) );

		$tmp = array();
		$this->db->select( 'shipments.container_number AS container_number' );
		$this->db->from( 'shipments_deliveries_mm' );
		$this->db->join( 'shipments', 'shipments.id = shipments_deliveries_mm.shipment_id AND shipments.hidden = 0 AND shipments.deleted = 0', 'left' );
		$this->db->where( array( 'shipments_deliveries_mm.hbl_hawb LIKE' => '%'.$hblHawb.'%', 'shipments_deliveries_mm.hidden' => 0, 'shipments_deliveries_mm.deleted' => 0 ) );
		$this->db->group_by( 'shipments_deliveries_mm.id' );
		$query = $this->db->get();

		foreach ( $query->result_array() as $row ) {
			$tmp[] = $row['container_number'];
		}

		return $tmp;
	}

	/**
	 * configures where clause for db query, depending on values in searchform.
	 */
	public function setWhereClauseForSearchparams( $recordListSearchParams ) {
		parent::setWhereClauseForSearchparams( $recordListSearchParams );

		if( !$recordListSearchParams ) return;
		if( !isset( $recordListSearchParams['shipments_approval'] ) ) return;
		$approval = $recordListSearchParams['shipments_approval'][0];

		if( $approval === 0 || $approval === 2 ) {
			$this->db->where( '(shipments.approval=0 OR shipments.approval=2)' );
		} else {
			$this->db->where( 'shipments.approval', $approval );
		}
	}

	/**
	 * updateContractNumbersAndDC
	 */
	public function updateContractNumbersAndDCs( $id ) {
		$deliveries 		= $this->shipment_delivery_model->getDeliveriesByShipmentId( $id );
		$contractNumbers 	= array();
		$dcs 				= array();
		$data				= array();
		foreach( $deliveries as $delivery ) {
			$contractNumbers[] = $delivery['contract_number'];
			$dcs[] =  $delivery['dc'];
		}
		$data['contract_numbers'] = implode( ', ', array_unique( $contractNumbers ) );
		$data['dcs'] = implode( ', ', array_unique( $dcs ) );
		$this->updateRecord( $id, $data );
	}

	/**
	 * setApproval
	 * @param $shipmentId
	 * @return
	 */
	public function setApproval( $shipmentId )
	{
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$contractType 	= 0;
		$shipment 		= $this->getRecord( $shipmentId );
		$deliveries 	= $this->shipment_delivery_model->getDeliveriesByShipmentId( $shipmentId );
		$allowedContractTypes = array(Contract_model::$CONTRACT_TYPE_CORE_RANGE, Contract_model::$CONTRACT_TYPE_AUI );

		foreach( $deliveries as $delivery ) {
			$contract = $this->contract_model->getRecord( $delivery['contract_id'] );
			$contractType = $contract['contract_type'];
		}

		if( $shipment['atd'] != '0000-00-00 00:00:00' && in_array( $contractType, $allowedContractTypes ) ) {
			$data = ( $shipment['import_date'] == '0000-00-00 00:00:00' ) ? array( 'approval' => 1, 'import_date' => date('Y-m-d H:i:s') ) : array( 'approval' => 1 );
			$this->updateRecord( $shipmentId, $data );
		}
	}

	/**
	 * setApprovalDate
	 */
	public function setApprovalDate( $id ) {
		$this->updateRecord( $id, array( 'approval_date' => date( 'Y-m-d H:i:s' ) ) );
	}

	/**
	 * setEdoSentClickDate
	 */
	public function setEdoSentClickDate( $id ) {
		$this->updateRecord( $id, array( 'edo_sent_click_date' => date( 'Y-m-d H:i:s' ) ) );
	}

	/**
	 * setPrintDate
	 */
	public function setPrintDate( $id ) {
		$shipment = $this->getRecord( $id );
		if( $shipment['print_date'] == '0000-00-00 00:00:00' || empty( $shipment['print_date'] ) ) {
			$this->updateRecord( $id, array( 'print_date' => date( 'Y-m-d H:i:s' ) ) );
		}
	}

	/**
	 * resetPrintDate
	 */
	public function resetPrintDate( $id ) {
		$this->updateRecord( $id, array( 'print_date' => date( '0000-00-00 00:00:00' ) ) );
	}

	/**
	 * updateHBLHAWB
	 */
	public function updateHBLHAWB( $id ) {
		$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId( $id );
		$hblHawb 	= array();
		$data		= array();
		foreach( $deliveries as $delivery ) {
			$hblHawb[] = $delivery['hbl_hawb_mm'];
		}
		$data['hbl_hawb'] = implode( ',', array_unique( $hblHawb ) );
		$this->updateRecord( $id, $data );
	}

	/**
	 * updateRecordByHBLHAWB
	 */
	public function updateRecordByHBLHAWBAndContainerNumber( $hblHawb, $containerNumber, $insertData ) {
		$shipments 	= $this->getRecordsWhere( array( 'hbl_hawb' => $hblHawb, 'container_number' => $containerNumber ,'hidden' => 0, 'deleted' => 0 ) );
		if( count($shipments) > 0 ) {
			$shipmentId = $shipments[0]['id'];
			$this->updateRecord( $shipmentId, $insertData );
		}
	}

	/**
	 * updateTelexReceived
	 */
	public function updateTelexReceived( $id ) {
		$tmp 		= array();
		$telex 		= 0;
		$records 	= $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $id, 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $records as $record ) {
			$tmp[] = $record['telex_received'];
		}
		$tmp = array_unique( $tmp );
		if( count( $tmp ) == 2 ) {
			$telex = 2;
		} else if( count( $tmp ) == 1 && $tmp[0] === '1' ) {
			$telex = 1;
		}
		$data['telex_received'] = $telex;
		$this->updateRecord( $id, $data );
	}

	/**
	 * isTelexReceived
	 */
	public function isTelexReceived( $id = 0 ){
		$out = FALSE;
		if( $id > 0 ){
			$records = $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $id, 'hidden' => 0, 'deleted' => 0 ) );
			$tmp = array();
			foreach( $records as $record ) {
					$tmp[] = $record['telex_received'];
			}
			$tmp = array_unique( $tmp );
			$out = ( count( $tmp ) == 1 && $tmp[0] === '1' ) ? TRUE : FALSE;
		}
		return $out;
	}


	/**
	 * updateOtherShipmentDeliveryRelations
	 */
	public function updateOtherShipmentDeliveryRelations( $id = 0 ){
		$deliveries 	= array();
		$records		= $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $id, 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $records as $record ) {
			$tmp		= array();
			$tmp['delivery_id'] 	= $record['delivery_id'];
			$tmp['hbl_hawb'] 		= $record['hbl_hawb'];
			$tmp['telex_received'] 	= $record['telex_received'];
			$deliveries[] 			= $tmp;
		}

		foreach( $deliveries as $delivery ) {
			if( !empty( $delivery['hbl_hawb'] ) ) {
				$records = $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id !=' => $id, 'hbl_hawb' => $delivery['hbl_hawb'], 'delivery_id' => $delivery['delivery_id'], 'hidden' => 0, 'deleted' => 0 ) );
				foreach( $records as $record ) {
					$data = array();
					$data['telex_received'] = $delivery['telex_received'];
					$this->shipment_delivery_model->updateRecord( $record['id'], $data );
				}
			}
		}

	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	private function prepareResultQueryForOverviewOutput( $query_result_array ){
		$res 		= array();
		$looOptions = $this->getTCAgetMultiselectionOptions( 'contracts_logwin_origin_office' );
		$desOptions = $this->getTCAgetMultiselectionOptions( 'shipments_destination_port' );
		foreach ( $query_result_array as $row) {
			$row['fob_date'] 						= ( $row['fob_date'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['fob_date'] ) ) : "";
			$row['aldi_au_wh_delivery_due_date'] 	= ( $row['aldi_au_wh_delivery_due_date'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $row['aldi_au_wh_delivery_due_date'] ) ) : "";
			$row['logwin_origin_office'] 			= ( $row['logwin_origin_office'] != 0 && array_key_exists($row['logwin_origin_office'], $looOptions) ) ? $looOptions[$row['logwin_origin_office']] : '';
			$row['destination'] 					= ( $row['destination'] != 0 && array_key_exists($row['destination'], $desOptions) ) ? $desOptions[$row['destination']] : '';

			$res[] = $row;
		}
		return $res;
	}

	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}
}

/* End of file shipments.php */
/* Location: ./app/models/shipments.php */
