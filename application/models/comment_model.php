<?php
require_once( APPPATH.'models/abstract_model'.EXT );

class Comment_model extends Abstract_model {

	function __construct() {
		parent::__construct();
		$this->tablename = 'comments';
		$this->commentsHistoryView = 'comment/history';
	}

	public function createNew( $insertData ) {
		$insertData['user_id'] = $this->session->userdata('userId');
    	return parent::createNew( $insertData );
    }
    
    public function getCommentHistory( $pid ) {
    	$this->db->select('comments.*, users.username, comments.id AS id');
		$this->db->from( $this->tablename );
		$this->db->join('users', 'comments.user_id = users.id', 'left');
		$this->db->where( array( $this->tablename.'.pid' => $pid, $this->tablename.'.hidden' => 0, $this->tablename.'.deleted' => 0 ) );
		$this->db->group_by( $this->tablename.'.id' );
		$this->db->order_by( $this->tablename.'.crdate ASC');
		$query = $this->db->get();
    	$comments = $query->result_array();
    	return $this->load->view( $this->commentsHistoryView, array( 'comments' => $comments) , TRUE );
    }
}

/* End of file comment_model.php */
/* Location: ./app/models/comment_model.php */
