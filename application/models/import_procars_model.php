<?php
require_once( APPPATH.'models/abstract_import_model'.EXT );

class Import_procars_model extends Abstract_import_model {

	function __construct() {
		parent::__construct();
		$this->lockSuffix = '_PROCARS';
		$this->importSubPath = '';
	}

	/**
	 * readEventFile
	 * @param $file
	 * @return boolean
	 */
	public function readEventFile( $file ) {
		$this->welogger->log( "readEventFile called with file ".$file, WELogger::$LOG_LEVEL_INFO, "Import_procars_model.readEventFile" );
		$this->log('readEventFile called with file '.$file);

		$success	= TRUE;
		$path 		= $this->config->item('import_path').$this->importSubPath;
		$shipment 	= array();

		$content 	= file_get_contents( $path.$file );
		$xml 		= new SimpleXMLElement( $content );
		$xml->registerXPathNamespace( 'n', 'http://dtd.riege.com/rsixml2/rsixml2' );

		$shipmentType = $this->getAttributeFromXML( $xml, 'n:shipment', 'type' );

		if( $shipmentType != 'subHouse' ) {

			foreach( $xml->xpath('n:shipment/n:conts/n:cont') as $container ) {
				$container->registerXPathNamespace( 'n', 'http://dtd.riege.com/rsixml2/rsixml2' );
				$containerType = $this->getAttributeFromXML( $container, 'n:contType[@qual="procars"]', 'code' );
				$containerTypeData = $this->getContainerType( $containerType );

				$shipment['container_number'] 	= (string) $container->attributes()->id;
				$shipment['container_number'] 	= str_replace( ' ', '', $shipment['container_number'] );

				$shipment['container_size']	 	= $containerTypeData['container_size'];
				$tcaNode 						= $this->tca->getColumnNodeById( 'pcshipments_container_size' );
				$options 						= $this->tca->getMultiselectionOptions( $tcaNode );
				$shipment['container_size'] 	= in_array(strtoupper($shipment['container_size']), $options) ? array_search(strtoupper($shipment['container_size']), $options) : $shipment['container_size'];

				$shipment['container_type']	 	= $containerTypeData['container_type'];
				$tcaNode 						= $this->tca->getColumnNodeById( 'pcshipments_container_type' );
				$options 						= $this->tca->getMultiselectionOptions( $tcaNode );
				$shipment['container_type'] 	= in_array(strtoupper($shipment['container_type']), $options) ? array_search(strtoupper($shipment['container_type']), $options) : $shipment['container_type'];

				$shipment['container_tare_weight'] = $containerTypeData['tare_weight'];

				$shipment['seal_number']	 	= $this->getAttributeFromXML( $container, 'n:sealNumbers', 'p1' );

				$etdCode						= $this->getEventCodeFromXML( $xml, array('ETD','IETD','TETD') );
				$etd 							= $this->getEventDateFromXML( $xml, $etdCode );
				$etaCode 						= $this->getEventCodeFromXML( $xml, array('ETA','IETA','TETA') );
				$eta 							= $this->getEventDateFromXML( $xml, $etaCode );
				$atdCode						= $this->getEventCodeFromXML( $xml, array('ATD','IATD','TATD') );
				$atd 							= $this->getEventDateFromXML( $xml, $atdCode );
				$ataCode 						= $this->getEventCodeFromXML( $xml, array('ATA','IATA','TATA') );
				$ata 							= $this->getEventDateFromXML( $xml, $ataCode );
				$cusCode 						= $this->getEventCodeFromXML( $xml, array('CUS','ICUS','TCUS') );
				$cus 							= $this->getEventDateFromXML( $xml, $cusCode );
				$aqc 							= $this->getEventDateFromXML( $xml, 'AQC' );
				$flo 							= $this->getEventDateFromXML( $xml, 'IFLO' );
				$ful 							= $this->getEventDateFromXML( $xml, 'IFUL' );
				if( !empty( $etd ) ) $shipment['etd'] = $etd;
				if( !empty( $eta ) ) $shipment['eta'] = $eta;
				if( !empty( $atd ) ) {
					$loc1 = $this->getAttributeFromXML( $xml, 'n:shipment/n:tdts/n:tdt/n:ports/n:port[@qual="departure"]', 'code' );
					$loc2 = $this->getAttributeFromXML( $xml, 'n:event[@code="'.$atdCode.'"]/n:locs/n:loc[@qual="event"]', 'code' );
					if( $loc1 == $loc2 ) $shipment['atd'] = $atd;
				}
				if( !empty( $ata ) ) {
					$loc1 = $this->getAttributeFromXML( $xml, 'n:shipment/n:tdts/n:tdt/n:ports/n:port[@qual="destination"]', 'code' );
					$loc2 = $this->getAttributeFromXML( $xml, 'n:event[@code="'.$ataCode.'"]/n:locs/n:loc[@qual="event"]', 'code' );
					if( $loc1 == $loc2 ) $shipment['ata'] = $ata;
				}
				if( !empty( $cus ) ) $shipment['cus'] = $cus;
				if( !empty( $aqc ) ) $shipment['aqc'] = $aqc;
				if( !empty( $flo ) ) $shipment['container_loaded'] = $flo;
				if( !empty( $ful ) ) $shipment['container_unloaded'] = $ful;

				$hbl 							= $this->getAttributeFromXML( $xml, 'n:shipment/n:refs/n:ref[@qual="bl"]', 'value' );
				$hawb 							= $this->getAttributeFromXML( $xml, 'n:shipment/n:refs/n:ref[@qual="awb"]', 'value' );
				$mbl 							= $this->getAttributeFromXML( $xml, 'n:shipment/n:refs/n:ref[@qual="mbl"]', 'value' );
				$mawb 							= $this->getAttributeFromXML( $xml, 'n:shipment/n:refs/n:ref[@qual="mawb"]', 'value' );

				$shipment['hbl_hawb'] 			= !empty( $hbl ) ? $hbl : $hawb;
				$shipment['mbl_mawb'] 			= !empty( $mbl ) ? $mbl : $mawb;

				$shipment['shipment_number']	= $this->getAttributeFromXML( $xml, 'n:shipment/n:refs/n:ref[@qual="shipment"]', 'value' );
				$shipment['obl']				= $this->getAttributeFromXML( $xml, 'n:shipment/n:refs/n:ref[@qual="obl"]', 'value' );
				$shipment['departure_port']		= $this->getAttributeFromXML( $xml, 'n:shipment/n:locs/n:loc[@qual="departure"]', 'code' );

				$shipment['destination_port'] 	= $this->getAttributeFromXML( $xml, 'n:shipment/n:locs/n:loc[@qual="destination"]', 'code' );
				$tcaNode 						= $this->tca->getColumnNodeById( 'pcshipments_destination_port' );
				$options 						= $this->tca->getMultiselectionOptions( $tcaNode );
				$shipment['destination_port'] 	= in_array(strtoupper($shipment['destination_port']), $options) ? array_search(strtoupper($shipment['destination_port']), $options) : $shipment['destination_port'];

				$shipment['incoterm_code'] 		= $this->getAttributeFromXML( $xml, 'n:shipment/n:incoTerms', 'code' );
				$tcaNode 						= $this->tca->getColumnNodeById( 'pcshipments_incoterm_code' );
				$options 						= $this->tca->getMultiselectionOptions( $tcaNode );
				$shipment['incoterm_code']		= in_array(strtoupper($shipment['incoterm_code']), $options) ? array_search(strtoupper($shipment['incoterm_code']), $options) : $shipment['incoterm_code'];

				$carrierCode					= $this->getValueFromXML( $xml, 'n:shipment/n:nads/n:nad[@qual="carrier"]/n:nadId' );
				$shipment['carrier']			= $this->getCarrierName( $carrierCode );
				$shipment['mother_vessel_name']	= $this->getValueFromXML( $xml, 'n:shipment/n:tdts/n:tdt/n:voyage/n:vessel' );
				$shipment['voyage_number']		= $this->getAttributeFromXML( $xml, 'n:shipment/n:tdts/n:tdt/n:voyage', 'number' );

				$role							= $this->getAttributeFromXML( $xml, 'n:shipment/n:partners/n:partner[@qual="sender"]', 'role' );
				if( $role == 'importer' ) {
					$shipment['vessel_2']			= $this->getValueFromXML( $xml, 'n:shipment/n:tdts/n:tdt/n:voyage/n:vessel' );
					$shipment['voyage_2']			= $this->getAttributeFromXML( $xml, 'n:shipment/n:tdts/n:tdt/n:voyage', 'number' );
				} else {
					$shipment['mother_vessel_name']	= $this->getValueFromXML( $xml, 'n:shipment/n:tdts/n:tdt/n:voyage/n:vessel' );
					$shipment['voyage_number']		= $this->getAttributeFromXML( $xml, 'n:shipment/n:tdts/n:tdt/n:voyage', 'number' );
				}

				$shipmentId = $this->getShipmentId( $shipment['container_number'], $shipment['hbl_hawb'] );

				$this->log('write shipment to db '.$shipment['container_number']. ' ' . $shipment['hbl_hawb']);
				$this->welogger->log( "write shipment to db ".var_export($shipment,true), WELogger::$LOG_LEVEL_INFO, "Import_procars_model.readEventFile" );
				if( $shipmentId > 0 ) {
					$previousRow = $this->pcshipment_model->getRecord( $shipmentId, false );
					$this->pcshipment_model->updateRecord( $shipmentId, $shipment, false );
				} else {
					$shipmentId = $this->pcshipment_model->createNew( $shipment, false );
				}
				$pcshipment = $this->pcshipment_model->getRecord( $shipmentId, false );
				// set ietd and ieta
				$this->setInitialDate( $pcshipment, 'ietd', 'etd', $pcshipment['etd'] );
				$this->setInitialDate( $pcshipment, 'ieta', 'eta', $pcshipment['eta'] );

				// set approval
				$this->setApproval( $pcshipment );

				// update corresponding shipment
				$this->pcshipment_model->updateRelatedShipment( $pcshipment['id'] );

				// process events dates
				$this->pcshipment_model->processEventDates( $pcshipment, $previousRow );
			}
		}

		// move file to archive
		if( ENVIRONMENT == 'production' || ENVIRONMENT == 'staging' ) $this->moveFileToArchive( $file );
		return $success;
	}

	/**
	 * getShipmentId
	 * @param containerNumber
	 * @return int
	 */
	public function getShipmentId( $containerNumber, $hblHawb )
	{
		$out = parent::getShipmentId( $containerNumber, $hblHawb );
		if( $out == 0 ) {
			$shipments = $this->pcshipment_model->getRecordsWhere( array( 'container_number'=> '', 'hbl_hawb' => $hblHawb,'hidden' => 0, 'deleted' => 0 ) );
			if( count( $shipments ) > 0 ) {
				$out = $shipments[0]['id'];
			}
		}
		return $out;
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

	/**
	 * getContainerTareWeight
	 * @param containerNumber
	 * @return int
	 */
	private function getContainerType( $containerType )
	{
		$this->load->model('Container_type_model', 'container_type_model');
		$out = array();
		$out['tare_weight'] 	= '';
		$out['container_type'] 	= '';
		$out['container_size'] 	= '';
		$rows = $this->container_type_model->getRecordsWhere( array( 'container_type_procars'=> $containerType, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $rows ) ) {
			$out = $rows[0];
		}
		return $out;
	}

	/**
	 * getCarrierName
	 * @param $code
	 * @return string
	 */
	private function getCarrierName( $code )
	{
		$this->load->model('Carrier_model', 'carrier_model');
		$out = '';
		$rows = $this->carrier_model->getRecordsWhere( array( 'code'=> $code, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $rows ) > 0 ) {
			$out = $rows[0]['name'];
		}
		return $out;
	}

	/**
	 * getAttributeFromXML
	 * @param $file
	 * @return
	 */
	private function getAttributeFromXML( $xml, $path, $attr ) {
		$data = $xml->xpath( $path );
		return isset( $data[0] ) ? (string) $data[0]->attributes()->$attr : '';
	}

	/**
	 * getAttributesFromXML
	 * @param $file
	 * @return
	 */
	private function getAttributesFromXML( $xml, $path, $attr ) {
		$out 	= array();
		$data 	= $xml->xpath( $path );
		if( isset( $data[0] ) ) {
			foreach( $data as $item ) {
				$out[] = (string) $item->attributes()->$attr;
			}
		}
		return implode( ',', $out );
	}

	/**
	 * getEventCodeFromXML
	 * @param
	 * @return
	 */
	private function getEventCodeFromXML( $xml, $events ) {
		foreach( $events as $event ) {
			$date = $this->getEventDateFromXML( $xml, $event );
			if( !empty( $date ) ) return $event;
		}
		return $events[0];
	}

	/**
	 * getEventDateFromXML
	 * @param $file
	 * @return
	 */
	private function getEventDateFromXML( $xml, $event ) {
		$data = $xml->xpath( 'n:event[@code="'.$event.'"]/n:dtms/n:dtm[@qual="event"]' );
		return  isset( $data[0] ) ? (string) $data[0]->attributes()->date : '';
	}

	/**
	 * setApproval
	 * @param $pcshipment
	 * @return
	 */
	private function setApproval( $pcshipment )
	{
		$shipments = $this->pcshipment_model->getRelatedShipmentsByPoNumber( $pcshipment['po_number'] );
		foreach( $shipments as $shipment ) {
			$this->shipment_model->setApproval( $shipment['id'] );
		}
	}

	/**
	 * setInitialDates
	 * @param $pcshipment
	 * @param $field
	 * @param $value
	 * @return
	 */
	private function setInitialDate( $pcshipment, $initialField, $defaultField, $initialValue )
	{
		// initial field must be empty
		if( empty( $pcshipment[$initialField] ) || $pcshipment[$initialField] == '0000-00-00 00:00:00') {
			// first check if hbl already exists
			$id 		= $pcshipment['id'];
			$hbl 		= $pcshipment['hbl_hawb'];
			$shipment 	= $this->getOtherShipmentByHBL( $hbl, $id );
			$data		= array();

			// if hbl exists take value of initial date field and default date field
			if( !empty( $shipment ) && $shipment[$initialField] != '0000-00-00 00:00:00') {
				// set initial value
				$initialValue = $shipment[$initialField];
				// set default value
				$data[$defaultField] = $shipment[$defaultField];
			}

			// update initial date field of record
			if( !empty( $initialValue ) ) {
				$data[$initialField] = $initialValue;
				$this->pcshipment_model->updateRecord( $id, $data, false );
			}
		}
	}

	/**
	 * getOtherShipmentByHBL
	 * @param $hbl
	 * @param $id
	 * @return array
	 */
	private function getOtherShipmentByHBL( $hbl, $id )
	{
		$out = array();
		$shipments = $this->pcshipment_model->getRecordsWhere( array( 'id !=' =>$id, 'hbl_hawb' => $hbl, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $shipments ) ) {
			$out = $shipments[0];
		}
		return $out;
	}

}
/* End of file import_model.php */
/* Location: ./app/models/import_model.php */
