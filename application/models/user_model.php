<?php
require_once ( APPPATH.'models/abstract_model'.EXT);

class User_model extends Abstract_model {

    function __construct() {
        parent::__construct();
        $this->tablename = 'users';
    }

	/**
	 * getSupplier
	 * returns supplier id
	 *
	 * @param
	 * @return number
	 */
	public function getSupplier() {
		$out 		='';
		$id 		= $this->session->userdata('userId');
		$records 	= $this->getRecordsWhere( array( 'id' => $id, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $records ) > 0 ) $out = $records[0]['supplier'];
		return $out;
	}

	/**
	 * getEmailByUsername
	 * returns email adresses.
	 *
	 * @param string - username
	 * @return string - email
	 */
	public function getEmailByUsername( $username = '' ) {
		$out 		='';
		$records 	= $this->getRecordsWhere( array( 'username' => $username, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $records ) > 0 ) $out = $records[0]['email'];
		return $out;
	}


}

/* End of file myfile.php */
/* Location: ./system/modules/mymodule/myfile.php */
