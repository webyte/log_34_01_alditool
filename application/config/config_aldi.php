<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Frontend
|--------------------------------------------------------------------------
*/
$config['items_per_page']					= 20;
$config['version']							= 'Version: 0.1 ';
$config['login_success_default_redirect']	= '/contract/contract';
$config['api_key']							= 'ElySany8qoXvHtiMABue6ij7RzGV6cyH1jKaIMY6xJMO5ZNP8IUu1KfyteNZabSP';

/*
|--------------------------------------------------------------------------
| Menu
|--------------------------------------------------------------------------
*/
$config['main_menu'] 	= array(
	array( 	'title' 		=> 'Contracts',
			'controller' 	=> 'contract/contract',
			'submenu' 		=> array(
				array( 	'title' 		=> 'Contracts',
						'controller' 	=> 'contract/contract'
				),
				array( 	'title' 		=> 'Contract upload',
						'controller' 	=> 'contract/upload'
				),
			),
	),
	array( 	'title' 		=> 'Open DC-Deliveries',
			'controller' 	=> 'delivery/delivery',
			'submenu' 		=> array(
				array( 	'title' 		=> 'Core range',
						'controller' 	=> 'delivery/delivery_core_range'
				),
				array( 	'title' 		=> 'Store equipment',
						'controller' 	=> 'delivery/delivery_store_equipment'
				),
				array( 	'title' 		=> 'Special buys',
						'controller' 	=> 'delivery/delivery_special_buys'
				),
				array( 	'title' 		=> 'One Off Specials',
						'controller' 	=> 'delivery/delivery_one_off_specials'
				),
				array( 	'title' 		=> 'eCommerce',
						'controller' 	=> 'delivery/delivery_ecommerce'
				),
				array( 	'title' 		=> 'Seasonal',
						'controller' 	=> 'delivery/delivery_seasonal'
				),
				array( 	'title' 		=> 'AUI',
						'controller' 	=> 'delivery/delivery_aui'
				),
			),
	),
	array( 	'title' 		=> 'CLPs',
			'controller' 	=> 'shipment/clps',
			'submenu' 		=> array(
			array( 	'title' 		=> 'Unconfirmed CLPs',
					'controller' 	=> 'shipment/shipment_unconfirmed'
			),
			array( 	'title' 		=> 'Confirmed CLPs',
					'controller' 	=> 'shipment/shipment_confirmed'
			),
			array( 	'title' 		=> 'All CLPs',
					'controller' 	=> 'shipment/shipment'
			),
			array( 	'title' 		=> 'Document follow up',
					'controller' 	=> 'shipment/dfu'
			),
		),
	),
	array( 	'title' 		=> 'Shipment',
			'controller' 	=> 'pcshipment/pcshipment',
	),
	array( 	'title' 		=> 'Container',
			'controller' 	=> 'container/container',
	),
	array( 	'title' 		=> 'Report',
			'controller' 	=> 'report/report_main',
            'submenu' 		=> array(
                array( 	'title' 		=> 'General report',
                        'controller' 	=> 'report/report'
                ),
                array( 	'title' 		=> 'CLP report',
                        'controller' 	=> 'report/report_clp'
                ),
                array( 	'title' 		=> 'Container Load Plan/Delivery Docket/Container Weight Declaration',
                        'controller' 	=> 'report/report_cda'
                ),
                array( 	'title' 		=> 'ALDI Summary of Order report',
                        'controller' 	=> 'report/report_order'
                ),
				array( 	'title' 		=> 'Second telex release reminder report',
						'controller' 	=> 'report/report_telex_1'
				),
				array( 	'title' 		=> 'Late telex release report',
						'controller' 	=> 'report/report_telex_2'
				),
				array( 	'title' 		=> 'Freight Forwarders (FF) Schedule',
						'controller' 	=> 'report/report_ff'
				),
				array( 	'title' 		=> 'CLP delivery',
						'controller' 	=> 'report/report_clp_delivery'
				),
				array( 	'title' 		=> 'KPI-Report',
						'controller' 	=> 'report/report_kpi'
				),
				array( 	'title' 		=> 'CoO-Report',
						'controller' 	=> 'report/report_coo'
				),
				array( 	'title' 		=> 'Vessel Schedule',
						'controller' 	=> 'report/report_vessel_schedule'
				),
				array( 	'title' 		=> 'Supplier contract ready report',
						'controller' 	=> 'report/report_supplier_contract_ready'
				),
				array( 	'title' 		=> 'Contract departure report',
						'controller' 	=> 'report/report_contract_departure'
				),
				array( 	'title' 		=> 'CLP unconfirmed report',
						'controller' 	=> 'report/report_clp_unconfirmed'
				),
				array( 	'title' 		=> 'Container Load Plan/Delivery Docket/Container Weight Declaration unconfirmed',
						'controller' 	=> 'report/report_cda_unconfirmed'
				),
				array( 	'title' 		=> 'Container Load Plan/Delivery Docket/Container Weight Declaration *NEW*',
						'controller' 	=> 'report/report_cda_new'
				),
				array( 	'title' 		=> 'Exception report',
						'controller' 	=> 'report/report_exc'
				),
				array( 	'title' 		=> 'Status Report',
						'controller' 	=> 'report/report_status'
				),
				array( 	'title' 		=> 'Status CRD versus ATD',
						'controller' 	=> 'report/report_crd_vs_atd'
				),
				array( 	'title' 		=> 'Booking Report',
						'controller' 	=> 'report/report_booking'
				),
				array( 	'title' 		=> 'Shipment Report',
						'controller' 	=> 'report/report_shipment'
				),
				array( 	'title' 		=> 'KPI Report',
						'controller' 	=> 'report/report_clp_kpi'
				),
				array( 	'title' 		=> 'Container Status Report',
						'controller' 	=> 'report/report_clp_container_status'
				),
				array( 	'title' 		=> 'Shipment Status Report',
						'controller' 	=> 'report/report_clp_shipment_status'
				),
				array( 	'title' 		=> 'Delay Report',
						'controller' 	=> 'report/report_clp_delay'
				),
				array( 	'title' 		=> 'PO Report',
						'controller' 	=> 'report/report_clp_po'
				),
				array( 	'title' 		=> 'OFC – Shipment Report',
						'controller' 	=> 'report/report_clp_shipment'
				),
				array( 	'title' 		=> 'OFC - Shipment Report (Interface)',
						'controller' 	=> 'report/report_clp_shipment_interface'
				),
				array( 	'title' 		=> 'Dimension Report',
						'controller' 	=> 'report/report_dimension'
				),
				array( 	'title' 		=> 'Contracts per On-Sale Week Report',
						'controller' 	=> 'report/report_contract_osw'
				),
			),
	),
	array( 	'title' 		=> 'Activity log',
			'controller' 	=> 'activity/activity',
	),
	array( 	'title' 		=> 'Dashboard',
			'controller' 	=> 'dashboard/dashboard',
			'submenu' 		=> array(
				array( 	'title' 		=> 'Supplier',
						'controller' 	=> 'dashboard/dashboard_supplier'
				),
				array( 	'title' 		=> 'CFS - Factory',
						'controller' 	=> 'dashboard/dashboard_cfs_factory'
				),
				array( 	'title' 		=> 'Ocean Freight',
						'controller' 	=> 'dashboard/dashboard_ocean_freight'
				),
				array( 	'title' 		=> 'Destination Management',
						'controller' 	=> 'dashboard/dashboard_destination_management'
				),
				array( 	'title' 		=> 'Entire Transportation',
						'controller' 	=> 'dashboard/dashboard_entire_transportation'
				),
				array( 	'title' 		=> 'Document Management',
						'controller' 	=> 'dashboard/dashboard_document_management'
				),
			),
	),
	array( 	'title' 		=> 'Customs',
			'controller' 	=> 'customs/customs_main',
			'submenu' 		=> array(
				array( 	'title' 		=> 'Customs',
						'controller' 	=> 'customs/customs'
				),
				array( 	'title' 		=> 'Customs Upload',
						'controller' 	=> 'customs/customs_upload'
				),
			),
	),
	array( 	'title' 		=> 'Supplier Portal',
			'controller' 	=> 'supplier/supplier_portal',
	),
	array( 	'title' 		=> 'Administration',
			'controller' 	=> 'manage/administration',
			'submenu' 		=> array(
				array( 	'title' 		=> 'Suppliers',
						'controller' 	=> 'manage/supplier',
				),
                array( 	'title' 		=> 'Products',
                        'controller'    => 'manage/product',
                ),
				array( 	'title' 		=> 'Logwin origin offices',
						'controller' 	=> 'manage/office',
				),
				array( 	'title' 		=> 'Departure ports',
						'controller' 	=> 'manage/departure',
				),
				array( 	'title' 		=> 'Aldi special weeks PIC',
						'controller' 	=> 'manage/pic',
				),
				array( 	'title' 		=> 'ALDI AU WH delivery due date',
						'controller' 	=> 'manage/delivery_due',
				),
				array( 	'title' 		=> 'Detention Free Days',
						'controller' 	=> 'manage/detention_free_day',
				),
				array( 	'title' 		=> 'Users',
						'controller' 	=> 'manage/user',
				),
				array( 	'title' 		=> 'Usergroups',
						'controller' 	=> 'manage/usergroup',
				),
			),
	),
);

/*
|--------------------------------------------------------------------------
| Permissions
|--------------------------------------------------------------------------
*/

$config['tables_for_perfieldpermissions']	= array('contracts','deliveries','shipments','pcshipments'); // tables for fields that should have ability to set permission (read-only, edit, hidden)
$config['acl_guarded_controllers']			= array(
	'activity/activity',
	'container/container',
	'contract/contract',
	'contract/ofu',
	'contract/upload',
	'dashboard/dashboard_cfs_factory',
	'dashboard/dashboard_destination_management',
	'dashboard/dashboard_document_management',
	'dashboard/dashboard_entire_transportation',
	'dashboard/dashboard_ocean_freight',
	'dashboard/dashboard_supplier',
	'delivery/delivery',
	'delivery/delivery_core_range',
	'delivery/delivery_store_equipment',
	'delivery/delivery_special_buys',
	'delivery/delivery_one_off_specials',
	'delivery/delivery_ecommerce',
	'delivery/delivery_seasonal',
	'delivery/delivery_aui',
	'manage/archive',
	'manage/contactperson',
	'manage/delivery_due',
	'manage/departure',
	'manage/migrate',
	'manage/office',
	'manage/pic',
	'manage/product',
	'manage/supplier',
	'manage/detention_free_day',
	'manage/user',
	'manage/usergroup',
	'pcshipment/pcshipment',
	'report/report',
	'report/report_booking',
	'report/report_cda',
	'report/report_cda_new',
	'report/report_cda_unconfirmed',
	'report/report_clp',
	'report/report_clp_delivery',
	'report/report_clp_unconfirmed',
	'report/report_contract_departure',
	'report/report_coo',
	'report/report_exc',
	'report/report_ff',
	'report/report_kpi',
	'report/report_order',
	'report/report_shipment',
	'report/report_status',
	'report/report_crd_vs_atd',
	'report/report_supplier_contract_ready',
	'report/report_telex_1',
	'report/report_telex_2',
	'report/report_vessel_schedule',
	'report/report_clp_container_status',
	'report/report_clp_shipment_status',
	'report/report_clp_kpi',
	'report/report_clp_delay',
	'report/report_clp_po',
	'report/report_clp_shipment',
	'report/report_clp_shipment_interface',
	'report/report_dimension',
	'report/report_contract_osw',
	'shipment/dfu',
	'shipment/shipment',
	'shipment/shipment_confirmed',
	'shipment/shipment_unconfirmed',
	'customs/customs',
	'customs/customs_upload',
	'supplier/supplier_portal',
); // names of controllers, that should have acl settings for all public methods.
$config['tables_with_crud_permissions']		= array('contracts','deliveries','shipments','pcshipments','users','user_groups','offices','suppliers','departures','contactpersons','products','pics','delivery_dues','detention_free_days','documents','supplier_comments'); // tables that should have permissions for CRUD
$config['login_free_controllers'] 			= array('login', 'forgot','change');
$config['acl_guarded_actions']				= array(
													'contract/contract_sendMail',
													'shipment/shipment_confirmed_delete',
													'shipment/shipment_unconfirmed_delete',
													'shipment/shipment_delete',
													'shipment/shipment_addDelivery',
													'shipment/shipment_removeDelivery',
													'shipment/shipment_removeHBLHAWB',
													'container/container_transmit',
													'report/report_cda_savePrintDate',
													'supplier/supplier_portal_viewDocument',
													'supplier/supplier_portal_uploadDocument',
													'supplier/supplier_portal_deleteDocument',
													'supplier/supplier_portal_confirmDocument',
													'supplier/supplier_portal_addComment',
													'supplier/supplier_portal_viewComment',
												);

/*
|--------------------------------------------------------------------------
| Paths
|--------------------------------------------------------------------------
*/

if( ENVIRONMENT == 'development_local') {
	$config['root_path']						= '/Users/oliver/Workspace/LogwinAldiTool/';
	$config['import_path']						= $config['root_path'].'import/';
	$config['export_path']						= $config['root_path'].'export/';
} else if( ENVIRONMENT == 'staging') {
	$config['root_path']						= '/var/www/alditool.thiel-logistik.loc/staging/current/';
	$config['import_path']						= $config['root_path'].'import_stage/';
	$config['export_path']						= $config['root_path'].'export_stage/';
} else if( ENVIRONMENT == 'production') {
	$config['root_path']						= '/var/www/alditool.thiel-logistik.loc/production/current/';
	$config['import_path']						= $config['root_path'].'import/';
	$config['export_path']						= $config['root_path'].'export/';
}
$config['sqlite_path']						= $config['root_path'].'db/';
$config['app_path']							= $config['root_path'].'application/';
$config['www_path']							= $config['root_path'].'public/';
$config['tca_path']							= $config['app_path'].'config/tca.xml';
$config['tmp_path']							= $config['root_path'].'tmp/';
$config['archive_path']						= $config['root_path'].'archive/';


/*
|--------------------------------------------------------------------------
| Documents
|--------------------------------------------------------------------------
*/
$config['documents_upload_path']			= $config['root_path'].'uploads/';
$config['allowed_types']					= "pdf|xls|xlsx|csv";
$config['max_size']							= 5120; // in KB

/*
|--------------------------------------------------------------------------
| Formats
|--------------------------------------------------------------------------
*/
$config['floatvalues_format'] 		= "%01.2f";
$config['doublevalues_format'] 		= "%01.3f";
$config['longdoublevalues_format'] 	= "%01.9f";
$config['datetime_format'] 			= "d/m/Y H:i:s";
$config['date_format'] 				= "d/m/Y";

/*
|--------------------------------------------------------------------------
| Mails
|--------------------------------------------------------------------------
*/
$config['from_mail']		= "noreply@logwin-logistics.com";
$config['from_name']		= "Logwin Aldi Tool";

/*
|--------------------------------------------------------------------------
| Import
|--------------------------------------------------------------------------
*/
$config['lock_file'] 				= "LOCK_ALDI";


/*
|--------------------------------------------------------------------------
| carrier mapping
|--------------------------------------------------------------------------
*/
$config['carrier_map'] = array(
	' HAMBURG' => 'HSD',
	' HAMBURG SUD' => 'HSD',
	' MOL' => 'MOL',
	'ANL' => 'ANL',
	'ANL CONTAINER LINE' => 'ANL',
	'ANL SINGAPORE PTE LTD' => 'ANL',
	'APL' => 'APL',
	'APL (CHINA)CO.' => 'LTD',
	'APL LINES (AUSTRALIA)' => 'APL',
	'APL-AMERICAN PRESIDENT LINES LTD' => 'APL',
	'cma' => 'CMA',
	'cma cgm' => 'CMA',
	'CMA CGM  AUSTRALIA PTY LTD' => 'CMA',
	'CMA CGM & ANL SECURITIES' => 'CMA',
	'CMA CGM (ANL LINE)' => 'CMA',
	'CMA CGM (CHINA)SHIPPING CO.' => 'LTD',
	'CMA CGM (HONG KONG) LIMITED' => 'CMA',
	'CMA CGM (THAILAND) LTD.' => 'CMA',
	'CMA-CGM GLOBAL (INDIA) PVT LTD' => 'CMA',
	'CMA-CGM VIETNAM AS AGENT FOR' => 'CMA',
	'COSCO CONTAINER LINE' => 'COSV',
	'COSCO CONTAINER LINE AGENCIES LTD' => 'COSV',
	'COSCO CONTAINER LINES' => 'COSV',
	'COSCO SHIPPING CONTAINER LINE' => 'COSV',
	'COSCO SHIPPING LINES (OCEANIA)' => 'COSV',
	'H-S' => 'HSD',
	'H-SUB' => 'HSD',
	'H-SUD' => 'HSD',
	'HAGAG LLOYD' => 'HLC',
	'HAGPAG-LLOYD' => 'HLC',
	'HALAG-LLOYD' => 'HLC',
	'HAM-SUD' => 'HSD',
	'HAMABURG SUD' => 'HSD',
	'HAMBRUG SUD ' => 'HSD',
	'HAMBUR SUD' => 'HSD',
	'HAMBURB SUD' => 'HSD',
	'HAMBURG' => 'HSD',
	'HAMBURG  SUD ' => 'HSD',
	'HAMBURG  SUED' => 'HSD',
	'HAMBURG SUD' => 'HSD',
	'HAMBURG SUD AUSTRALIA PTY LTD' => 'HSD',
	'HAMBURG SUD GUANGZHOU' => 'HSD',
	'HAMBURG SUD LNE' => 'HSD',
	'HAMBURG SUD SHENZHEN' => 'HSD',
	'HAMBURG SUD V.410S' => 'HSD',
	'HAMBURG SUED' => 'HSD',
	'HAMBURG USD ' => 'HSD',
	'HAMBURG-SUD' => 'HSD',
	'HAMBURGR SUD ' => 'HSD',
	'HAMBURGSUD' => 'HSD',
	'HAMNURG SUD ' => 'HSD',
	'HAPA' => 'HLC',
	'HAPA-LLOYD' => 'HLC',
	'HAPAG' => 'HLC',
	'HAPAG  LLOYD ' => 'HLC',
	'HAPAG - LLOYD (CHINA) LTD' => 'HLC',
	'HAPAG LLLOYD' => 'HLC',
	'HAPAG LLOLYD' => 'HLC',
	'HAPAG LLOYD' => 'HLC',
	'HAPAG LLOYD (ITALY) S.R.L.' => 'HLC',
	'HAPAG LLOYD (TAIWAN) LTD.' => 'HLC',
	'HAPAG LLOYD (UK) LTD' => 'HLC',
	'HAPAG LLOYD AG' => 'HLC',
	'HAPAG LLOYD SPAIN' => ' S.L.',
	'HAPAG LLOYD V.349S' => 'HLC',
	'HAPAG LLOYED' => 'HLC',
	'HAPAG LLOYED (CHINA) SHIPPING LTD.' => 'HLC',
	'HAPAG LLOYS' => 'HLC',
	'Hapag Llyod' => 'HLC',
	'Hapag Looyd' => 'HLC',
	'HAPAG-LLLOYD' => 'HLC',
	'HAPAG-LLOD' => 'HLC',
	'HAPAG-LLOY' => 'HLC',
	'HAPAG-LLOYD' => 'HLC',
	'HAPAG-LLOYD (AUSTRALIA) PTY LTD' => 'HLC',
	'HAPAG-LLOYD (CHINA) LTD' => 'HLC',
	'HAPAG-LLOYD (M) SDN BHD' => 'HLC',
	'HAPAG-LLOYD (THAILAND) LTD.' => 'HLC',
	'HAPAG-LLOYD (VIETNAM)' => 'HLC',
	'HAPAG-LLOYD AG' => 'HLC',
	'HAPAG-LLOYD AUSTRIA GESMBH' => 'HLC',
	'HAPAG-LLOYD INDIA PVT LTD' => 'HLC',
	'HAPAG-LLOYD INDONESIA' => 'HLC',
	'HAPAG-LLOYD PHILIPPINES INC.' => 'HLC',
	'HAPAG-LLOYED' => 'HLC',
	'HAPAG-LLYOD' => 'HLC',
	'HAPAGG LLOYD' => 'HLC',
	'HAPAh' => 'HLC',
	'HAPAP LLOYD' => 'HLC',
	'HAPAQ' => 'HLC',
	'HAPAY LLOYD' => 'HLC',
	'HAPG LLOYD' => 'HLC',
	'HAPPAG LLOYD ' => 'HLC',
	'HASU' => 'HSD',
	'HAUBURG SUD' => 'HSD',
	'HISUD' => 'HSD',
	'HLAG' => 'HLC',
	'HLC' => 'HLC',
	'HLCU' => 'HLC',
	'HLCUVIE150506454' => 'HLC',
	'HLCUXM1151221491' => 'HLC',
	'HLP' => 'HLC',
	'HPL' => 'HLC',
	'HS' => 'HSD',
	'HSDG' => 'HSD',
	'HSUD' => 'HSD',
	'HSUS' => 'HSD',
	'HUSD' => 'HSD',
	'K LINE' => 'KLI',
	'K-Line' => 'KLI',
	'M.O.L.' => 'MOL',
	'MAERKS' => 'MSK',
	'MAERSK' => 'MSK',
	'MAERSK (CHINA) SHIPPING CO.' => ' LTD.',
	'MAERSK AUSTRALIA PTY LTD' => 'MSK',
	'MAERSK HONG KONG LTD' => 'MSK',
	'MAERSK LINE' => 'MSK',
	'MAERSK LINE (THAILAND) CO.' => 'LTD.',
	'MAERSK SEALAND' => 'MSK',
	'MAERSK TAIWAN LTD' => 'MSK',
	'MAERSK VIETNAM LTD.' => 'MSK',
	'MEDITERRANEAN SHIPPING COMPANY' => 'MSC',
	'MEDITERRANEAN SHIPPING COMPANY S.A.' => 'MSC',
	'mitsui' => 'MOL',
	'MITSUI O S K LINES LTD' => 'MOL',
	'MITSUI O.S.K' => 'MOL',
	'MITSUI O.S.K LINE' => 'MOL',
	'MITSUI O.S.K LINEA' => 'MOL',
	'MITSUI O.S.K LINES' => 'MOL',
	'MITSUI O.S.K LINES (INDIA) PVT LTD' => 'MOL',
	'MITSUI O.S.K LINES (M) SDN BHD' => 'MOL',
	'MITSUI O.S.K LINES INDIA PVT LTD' => 'MOL',
	'MITSUI O.S.K. LINES' => 'MOL',
	'MITSUI O.S.K. LINES (AUSTRALIA) P/L' => 'MOL',
	'MITSUI O.S.K. LINES (M) SDN BHD' => 'MOL',
	'MITSUI O.S.K. LINES (THAILAND)' => 'MOL',
	'MITSUI O.S.K. LINES LTD' => 'MOL',
	'MITSUI O.S.K.LINES' => 'MOL',
	'MITSUI O.S.K.LINES (AUST) PTY LTD' => 'MOL',
	'MITSUI O.S.K.LINES (VIETNAM) LTD' => 'MOL',
	'MITSUI O.S.K.LINES LTD' => 'MOL',
	'MITSUI O.SK.LINES' => 'MOL',
	'MITSUI OSK' => 'MOL',
	'MITSUI OSK LINE' => 'MOL',
	'MITSUI OSK LINES' => 'MOL',
	'MOL' => 'MOL',
	'MOL (CHINA) CO.' => 'LTD',
	'MOL (CHINA) CO.' => 'LTD.GUANGZHOU',
	'MOL (CHINA) CO.' => 'LTD.GUANGZHOUBRANCH',
	'MOL (EUROPE) B.V.' => 'MOL',
	'MOL (EUROPE) B.V. BRANCH AUSTRIA' => 'MOL',
	'MOL (EUROPE) BV' => 'MOL',
	'MOL (SINGAPORE) PTE LTD' => 'MOL',
	'MOl Quality' => 'MOL',
	'MOL(KOREA) CO.' => ' LTD',
	'MOLU' => 'MOL',
	'MOS' => 'MOL',
	'MOSK' => 'MOL',
	'MSC ' => 'MSC',
	'MSC VIETNAM COMPANY LIMITED' => 'MSC',
	'MSK' => 'MSC',
	'MTSUI OSK LINES' => 'MOL',
	'NYK Helios' => 'NYK',
	'O.N.E' => 'ONE',
	'OCEAN NETWORK EXPRESS' => 'ONE',
	'OCEAN NETWORK EXPRESS (AUSTRALIA)' => 'ONE',
	'OCEAN NETWORK EXPRESS (CHINA) LTD' => 'ONE',
	'OCEAN NETWORK EXPRESS (CHINA) LTD.' => 'ONE',
	'OCEAN NETWORK EXPRESS (EAST ASIA)' => 'ONE',
	'OCEAN NETWORK EXPRESS (THAILAND)' => 'ONE',
	'OCEAN NETWORK EXPRESS (VIETNAM)' => 'ONE',
	'ONE LINE INDIA PVT  LTD' => 'ONE',
	'OOCL' => 'OOLV',
	'OOCL (AUSTRALIA) PTY LTD' => 'OOLV',
	'OOCL LOGISTICS (CHINA) LTD' => 'OOLV',
	'ORIENT OVERSEAS CONTAINER LINE' => 'OOLV',
	'ORIENT OVERSEAS CONTAINER LINE LTD' => 'OOLV',
	'ORIENT OVERSEAS CONTAINER LINE LTD.' => 'OOLV',
	'PACIFIC INTERNATIONAL LINES' => 'PIL',
	'PIL VIETNAM CO LTD' => 'PIL',
	'PT MITSUI O.S.K LINE INDONESIA' => 'MOL',
	'PT MITSUI O.S.K LINE LTD' => 'MOL',
	'PT. MITSUI OSK LINE INDONESIA' => 'MOL',
	'YANG MING (AUSTRALIA) PTY LTD' => 'YML',
	'YANGMING MARINE TRANSPORT CORP' => 'YML',
	'YML' => 'YML',
	'^mol' => 'MOL',
	'OOCL Orient Overseas Container Line Ltd' => 'OOLV',
	'ANL Container Line Pty Limited' => 'ANL',
	'AUSTRALIA NATIONAL LINE' => 'ANL',
	'Hamburg Sued Hong Kong Limited - Region Asia Pacific Head Office' => 'HSD',
	'Hamburg Sued Line' => 'HSD',
	'Hamburg Sued Shipping Agency Ltd.' => 'HSD',
	'Hapag-Lloyd (China) Limited' => 'HLC',
	'Hapag-Lloyd AG' => 'HLC',
	'Maersk A/S' => 'MSK',
	'Maersk Line - Ho Chi Minh City' => 'MSK',
	'Maersk Line - Hong Kong' => 'MSK',
	'MSC Mediterranean Shipping Company S.A' => 'MSC',
	'MSC Vietnam Company Limited' => 'MSC',
	'Ocean Network Express (East Asia) Ltd.' => 'ONE',
	'OCEAN NETWORK EXPRESS LINE [INDIA]' => 'ONE',
	'ONE' => 'ONE',
	'ONE Ocean Network Express Pte Ltd.' => 'ONE',
	' OOCL (PHILIPPINES) INC.' => 'OOCL',
	'Orient Overseas Container Line Limited - OOCL Hong Kong Branch' => 'OOCL',
	'Evergreen Marine Corp. (Taiwan) Ltd.' => 'EMC',
);

/* End of file config_zlabels.php */
/* Location: ./app/config/config_zlabels.php */