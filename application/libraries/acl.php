<?php
class Acl {
	public $CI;
	private $CIDirectory;
	private $CIClass;
	private $CIMethod;

	public static $crud_prefix 			= "table_crud_";
	public static $tablefield_prefix 	= "tablefield_";
	public static $action_prefix 		= "action_";
	public static $controller_prefix 	= "controller_";

	public function __construct($params=array()) {
		$this->CI =& get_instance();
		$this->CI->load->library( 'session' );
		$this->CI->load->helper('url');

		$this->init();
	}

	/**
	 * init
	 * @param
	 * @return
	 */
	public function init() {
		$this->usergroup = $this->getUsergroup();
		$this->perms = $this->getGroupPerms( $this->usergroup );
	}

	/**
	 * hasPermission
	 * returns TRUE/FALSE for permissions with boolean value 0 or 1
	 * @param $permKey string
	 * @return bool
	 */
	public function hasPermission( $permKey ) {
		$out = FALSE;
		if ( array_key_exists( $permKey, $this->perms ) ) {
			if ( $this->perms[$permKey] === '1' ) {
				$out = TRUE;
			}
		}
		return $out;
	}

	/**
	 * getPermissionValue
	 * returns value of given permission.
	 * useful for permissions with larger range. (e.g. formfields can have
	 * have 0,1 and 2 as values (edit,disable,hidden))
	 *
	 * @param $permKey string
	 * @return int
	 */
	public function getPermissionValue( $permKey ) {
		$out = FALSE;
		if ( array_key_exists( $permKey, $this->perms ) ) {
			return $this->perms[$permKey];
		}
		return $out;
	}

	/**
	 * permissionExists
	 * returns TRUE if the given permission exists in the group_perms db table
	 * @param $permKey string
	 * @return bool
	 */
	public function permissionExists( $permKey ) {
		return array_key_exists( $permKey, $this->perms );
	}

	/**
	 * isUserLoggedIn
	 * @param
	 * @return Boolean - TRUE is logged in
	 */
	public function isUserLoggedIn() {
		return $this->CI->session->userdata('isLoggedIn');

	}

	/**
	 * getGroupPerms
	 * @param $usergroup int
	 * @return array
	 */
	private function getGroupPerms( $usergroup ) {
		$out = array();
		$query = $this->CI->db->get_where( 'group_perms', array( 'group_id' => $usergroup, 'deleted' => 0 ) );
		if( $query->num_rows() > 0 ) {
			foreach( $query->result_array() as $row ){
				$out[$row['name']] 	= $row['value'];
			}
		}
		return $out;
	}

	/**
	 * getUsergroup
	 * @param
	 * @return string
	 */
	private function getUsergroup(){
		return $this->CI->session->userdata('usergroup') ? $this->CI->session->userdata('usergroup') : 0;
	}

	/**
	 * loadRouter
	 * @param
	 * @return
	 */
	private function loadRouter(){
		$router = &load_class('Router');
		$this->CIDirectory 	= $router->fetch_directory();
		$this->CIClass 		= $router->fetch_class();
		$this->CIMethod 	= $router->fetch_method();
	}

}


/* End of file acl.php */
/* Location: ./app/libraries/acl.php */