<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WELogger {
	public $CI;
	public static $LOG_LEVEL_ERROR 		= 0;
	public static $LOG_LEVEL_WARNING 	= 1;
	public static $LOG_LEVEL_INFO 		= 2;
	
	private $table;
    
    function __construct() {
		$this->CI =& get_instance();
 		$this->CI->load->helper('file');
 		$this->table = 'logs';
    }

	/**
	 * log
	 * @param $action string
	 * @param $service string
	 * @param $type string
	 * @param $pon string
	 * @param $file string
	 * @returns
	 */
    public function log( $action='', $type='', $service='' ) {
 		$data 		= array('type' 			=> $type,
							'action' 		=> $action,
							'service' 		=> $service,
							'ip' 			=> $this->CI->input->ip_address(),
							'user_id' 		=> $this->CI->session->userdata('userId'),
						 );
		if( $this->table ) $query 	= $this->CI->db->insert( $this->table, $data );
  }

	/**
	 * setLogTable
	 * @param $str string
	 * @returns
	 */
	function setLogTable( $str = '' ) {
		$this->table = $str;
	}

	/**
	 * writeLogFile
	 * @param $msg string
	 * @returns
	 */
	private function writeLogFile( $msg ) {
		$message 	= '';
		$logPath 	= $this->CI->config->item( 'log_path' );
		$filePath 	= $logPath.'log-'.date('Y-m-d').EXT;
		
		if ( !file_exists( $filePath ) ) {
			$message .= "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
		}

		$message .= date( 'Y-m-d H:i:s' ).' --> '.implode( ",", $msg )."\n";
		
		if ( write_file( $filePath, $message, FOPEN_WRITE_CREATE ) === FALSE ) {
			return FALSE;
		}
	
		@chmod( $filePath, FILE_WRITE_MODE ); 		
		return TRUE;
	}

    
	/**
	 * getAllSessionDate
	 * @returns string
	 */
    private function getAllSessionData() {
   		$sess_data 	= ( $this->CI->session->all_userdata() ) ? $this->CI->session->all_userdata() : FALSE;
    	// clean up data
	    foreach ( array( 'session_id','ip_address','user_agent','last_activity' ) as $val ) {
			unset( $sess_data[$val] );
		}
		
		$out = '';
		foreach( $sess_data as $key => $val ) {
			$out .= $key.': '.$val.', ';
		}
		$out = ( strlen( $out ) > 0 ) ? substr( $out, 0, -2 ) : '';
		return $out;
    }
    
    
}

// END Logger class

/* End of file WELogger.php */
/* Location: ./system/application/libraries/WELogger.php */