<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tca {
	public $CI;
	private $uniqueId = -1; // is used to give unique id-attributes if we have multiple form fields with the same columnid in the form.
	private $currentRecordValues; // valuesarray of the current record, a form is being generated for. 
	private $relationCounter = 0; // counter for generateDBrelation for initiating multiple models

	public function __construct( $config = array() )	{
		$this->CI =& get_instance();
		$this->CI->load->helper('url');
		$this->CI->load->helper('form');
		$this->tcaXML = simplexml_load_file( $this->CI->config->item('tca_path') );
	}

	/**
	 * getFormArray
	 * 
	 * returns complete form-html including all
	 * fields defined in given tcaxml.
	 * 
	 */
	public function getFormArray($action, $fieldValues, $submitLabel = 'Save') {
		$out = array ();
		$attr = array (
			'class' => 'form'
		);
		$out['form.start'] = form_open($action, $attr);
		if (is_array($fieldValues) && count($fieldValues) > 0) {
			// loop through all tables
			foreach ($fieldValues as $tableName => $values) {
				$tableNode = $this->tcaXML->xpath('table[@name="' . $tableName . '"]');
				$this->currentRecordValues = $values;
				foreach ($tableNode[0]->xpath('column') as $column) {
					$field = (string) $column->fieldname;
					$value = array_key_exists($field, $values) ? $values[$field] : "";
					$out['label.' . $tableName . '.' . $field] = $this->getFormLabel($column); //." (".(string)$column["id"].")\n";
					$out['input.' . $tableName . '.' . $field] = $this->getFormInput($column, $value) . "\n";
					$out['value.' . $tableName . '.' . $field] = $value;
					$out['history.' . $tableName . '.' . $field] = $this->getHistory($column);
				}

				$crudActions = array('new', 'edit', 'delete');
				foreach ( $crudActions as $action ) {
					$tmp_perm_name 			= Acl::$crud_prefix.$tableName.'_'.$action;
					$out['perms.' . $tableName . '.'.$action] = $this->CI->acl->hasPermission( $tmp_perm_name );
				}
			}
		}
		if ($action != "") $out['form.submit'] = form_submit('mysubmit', $submitLabel, 'class="btn btn-primary"');
		$out['form.close'] = form_close();
		return $out;
	}

	/**
	 * getFormInput
	 * return html input formfield for the given columnNode.
	 * 
	 * @param $columnNode - TCA column node
	 * @param $value - prefilled/selected value in the field
	 * @param $forcePermission - if set, the setting overrides permissions in ACL 1 == editable, 2 == disabled, 3 == hidden
	 * @return HTML Formfield
	 */
	private function getFormInput($columnNode, $value = '', $forcePermission = '' ) {
		$result = "";

		$display 	= $this->getParamValue('display', $columnNode, 'inputconfig/');
		$type 		= $this->getParamValue('type', $columnNode, 'inputconfig/');
		$validate 	= $this->getParamValue('validate', $columnNode, 'inputconfig/');
		$class 		= $this->getParamValue('class', $columnNode, 'inputconfig/');
		$id 		= (string) $columnNode["id"];
		
		// permission
		$perm_name = 'tablefield_'.$this->getTablenameById($id).'_'.$this->getFieldNameById($id);
		if( $forcePermission !== '' ) {
			$editable = $forcePermission;
		} else if ( $this->getParamValue('editable', $columnNode, 'inputconfig/') == 'readonly' ) {
			$editable = 2;
		} else {
			$editable = $this->CI->acl->getPermissionValue( $perm_name );  // 1 == editable, 2 == disabled, 3 == hidden
		}

		// default values
		$defaultvalue = $this->getParamValue('defaultvalue', $columnNode, 'inputconfig/');
		if ($defaultvalue != "" && $value == '') $value = $defaultvalue;

		$bondedfield = $this->getParamValue('bondedfield', $columnNode, 'inputconfig/');

		if ($editable != 3) {
			if ($display == "hidden") {
				$result .= form_hidden($id.'[]', $value);
			} else {
				$fieldParams = array (
					'name' => $id.'[]',
					'value' => $value,
					'id' => $this->uniqueId > -1 ? $id.'_'.$this->uniqueId : $id,
				);
				if( !empty( $class ) ) {
					$fieldParams["class"] = $class;
				}
				if ($editable == "2") {
					$fieldParams["readonly"] = "readonly";
					if (array_key_exists("class", $fieldParams)) {
						$fieldParams["class"] .= ' readonly';
					} else {
						$fieldParams["class"] = "readonly";
					}
				}
				if ($validate != "") {
					if (array_key_exists("class", $fieldParams)) {
						$fieldParams["class"] .= ' ' . $validate;
					} else {
						$fieldParams["class"] = $validate;
					}
				}
				
				switch ($type) {
					case "input" :
						$result .= form_input($fieldParams);
						break;

					case "password" :
						$result .= form_password($fieldParams);
						break;

					case "int" :
						$result .= form_input($fieldParams);
						break;

					case "date" :
						if ($editable != "2") {
							if (array_key_exists("class", $fieldParams)) {
								$fieldParams["class"] .= ' datepicker';
							} else {
								$fieldParams["class"] = "datepicker";
							}
						}
						if ($value == 0) {
							$fieldParams["value"] = '';
						} else {
							$tmp = '';
							//$tmp = preg_replace('#([0-9]{4})\-([0-9]{2})\-([0-9]{2}) (.*)#', '\\3.\\2.\\1', $value);
							$tmp = date( $this->CI->config->item('date_format') , strtotime( $value ) );
							$fieldParams["value"] = $tmp;
						}
						$result .= form_input($fieldParams);
						break;

					case "datetime" :
						if ($editable != "2") {
							if (array_key_exists("class", $fieldParams)) {
								$fieldParams["class"] .= ' datepicker';
							} else {
								$fieldParams["class"] = "datepicker";
							}
						}
						if ($value == 0) {
							$fieldParams["value"] = '';
						} else {
							$tmp = '';
							//$tmp = preg_replace('#([0-9]{4})\-([0-9]{2})\-([0-9]{2}) (.*)#', '\\3.\\2.\\1 \\4', $value);
							$tmp = date( $this->CI->config->item('datetime_format') , strtotime( $value ) );
							$fieldParams["value"] = $tmp;
						}
						$result .= form_input($fieldParams);
						break;

					case "time" :
						if ($editable != "2") {
							if (array_key_exists("class", $fieldParams)) {
								$fieldParams["class"] .= ' timepicker';
							} else {
								$fieldParams["class"] = "timepicker";
							}
						}
						if ($value == 0) {
							$fieldParams["value"] = '';
						} else {
							$fieldParams["value"] = $value;
						}
						$result .= form_input($fieldParams);
						break;

					case "float" :
						//$fieldParams['value'] = sprintf($this->CI->config->item('floatvalues_format'), $value);
						$fieldParams['value'] = number_format( round($value, 2), 2, '.', '');
						$result .= form_input($fieldParams);
						break;

					case "double" :
						//$fieldParams['value'] = sprintf($this->CI->config->item('doublevalues_format'), $value);
						$fieldParams['value'] = number_format( round( $value, 3 ), 3, '.', '');
						$result .= form_input($fieldParams);
						break;

					case "longdouble" :
						//$fieldParams['value'] = sprintf($this->CI->config->item('longdoublevalues_format'), $value);
						$fieldParams['value'] = number_format( round( $value, 9 ), 9, '.', '');
						$result .= form_input($fieldParams);
						break;

					case "textarea" :
						$fieldParams["rows"] = "3";
						$fieldParams["cols"] = "30";
						$fieldParams["value"] = $value;
						$result .= form_textarea($fieldParams);
						break;

					case "checkbox" :
						$extras = ($editable == "2") ? 'class="checkbox" disabled="disabled"' : 'class="checkbox"';
						$result .= ($editable == "2") ? '' : form_hidden($id, ""); // we need a hidden input field with the same id, to ensure, that the value of the unchecked checkbox is present in post params when form is sumitted
						$result .= form_checkbox($id, "1", $value == "1", $extras);
						break;

					case "radio" :
						$dataProvider = $this->getParamValue('radiodata', $columnNode, 'inputconfig/');
						if ($editable == "2")
							$fieldParams["disabled"] = "disabled";
						if ($dataProvider != "") {
							$buttons = explode(',', $dataProvider);
							$i = 0;
							foreach ($buttons as $button) {
								$parts = explode('|', $button);
								$fieldParams["id"] = $id . '_' . $i;
								$fieldParams["value"] = $parts[1];
								$fieldParams["checked"] = ($value == $parts[1]);

								if (array_key_exists("class", $fieldParams)) {
									$fieldParams["class"] .= ' radio';
								} else {
									$fieldParams["class"] = 'radio';
								}
								$result .=  '<label class="radio inline" for="' . $id . '_' . $i . '">' . $parts[0] . form_radio($fieldParams) . '</label>';
								$i++;
							}
						}
						break;
					case "select" :
						$options = $this->getMultiselectionOptions($columnNode);
						$selected = array ( $value );
						$extras = 'class="' . $validate . '" id="' . $id . '"';
						$extras .= ($editable == "2") ? ' disabled="disabled"' : '';
						$result .= form_dropdown($id.'[]', $options, $selected, $extras);
						break;
					case "checkboxgroup" :
						$options = $this->getMultiselectionOptions($columnNode);
						$values = explode(',', $value);

						foreach ($options as $key => $v) {
							$extras = ($editable == "2") ? 'class="checkbox" disabled="disabled"' : 'class="checkbox"';
							if ($key != "") {
								$result .= '<div class="checkboxgroup_item">' . form_checkbox($v, $key, in_array($key, $values), $extras) . '<span>' . $v . '</span></div>';
							}
						}
						//						$result .= form_hidden( $id, $value );
						$fieldParams["class"] .= ' invisible';
						$result .= form_input($fieldParams);
						break;

					case "relation":
						$result .= $this->generateDBrelationForms( $columnNode, $value );
						break;
						
					case "upload":
						$result .= $this->generateUploadForm( $columnNode, $value );
						break;	
					
					case "comment":
						$fieldParams["rows"] = "3";
						$fieldParams["class"] = "comment_input";
						$fieldParams["value"] = '';
						$fieldParams["placeholder"] = 'Type your comment here.';
						$result .= form_textarea($fieldParams);
						$result .= ( !empty( $value ) ) ? '<div class="comments">'.$value.'</div>' : '';
						break;			

					default :
				}
			}
		}

		if ($bondedfield != "") {
			$result .= '<script type="text/javascript">bondedFields.push( {source:"' . $id . '", target:"' . $bondedfield . '"} );</script>';
		}

		return $result;
	}

	public function getFormInputById($columnId, $value = '', $forcePermission = '') {
		$columnNode = $this->getColumnNodeById($columnId);
		return $this->getFormInput($columnNode, $value, $forcePermission);
	}

	public function getMultiselectionOptions($columnNode) {
		$options = array ();
		if ($this->getParamValue('source', $columnNode, 'inputconfig/') == 'foreign_table')
			$options = $this->getForeignTableValues($columnNode);
		if ($this->getParamValue('source', $columnNode, 'inputconfig/') == 'list') {
			$parts = explode(',', $this->getParamValue('selectvalues', $columnNode, 'inputconfig/'));
			foreach ($parts as $v) {
				$kv = explode('|', $v);
				$options[$kv[0]] = count($kv) > 1 ? $kv[1] : $kv[0];
			}
		}
		return $options;
	}

	private function generateDBrelationForms( $columnNode, $value ) {
		$res = "";
		$id_field					= $this->getParamValue('parenttable_primarykey_field', $columnNode, 'inputconfig/');
		$foreigntable_name			= $this->getParamValue('foreigntable_name', $columnNode, 'inputconfig/');
		$foreigntable_form_template	= $this->getParamValue('foreigntable_form_template', $columnNode, 'inputconfig/');
		$foreigntable_model			= $this->getParamValue('foreigntable_model', $columnNode, 'inputconfig/');
		$foreigntable_method		= $this->getParamValue('foreigntable_model_receive_records_methodname', $columnNode, 'inputconfig/');
		$model_name					= 'tmpModel'.$this->relationCounter;
		$currentRecordValues		= $this->currentRecordValues;

		$this->CI->load->model($foreigntable_model, $model_name);

		if( is_array($currentRecordValues) && array_key_exists($id_field, $currentRecordValues) ){
			$relatedRecords = call_user_func(array($this->CI->$model_name, $foreigntable_method), $currentRecordValues[$id_field] );
			foreach ($relatedRecords as $row) { // get formfields for every related record.
				$this->uniqueId++; // this triggers id names in the form (columnId+uniqueId)
				$data = array();
				$data['form'] = $this->getFormArray( "" , array( $foreigntable_name => $row ) );
				$data['deletePerms']= $this->CI->acl->hasPermission( Acl::$crud_prefix.$foreigntable_name.'_delete' );
				$data['row']= $row;

				$res .= $this->CI->load->view( $foreigntable_form_template, $data, true );
			}
			$this->uniqueId = -1; // this disables ids with number postfix
			$this->currentRecordValues = $currentRecordValues;
		}
		$this->relationCounter++;
		return $res;
	}
	
	private function generateUploadForm( $columnNode, $value ) {
		$res = "";
//		$id_field					=	$this->getParamValue('parenttable_primarykey_field', $columnNode, 'inputconfig/');
//		$foreigntable_name			=	$this->getParamValue('foreigntable_name', $columnNode, 'inputconfig/');
//		$foreigntable_form_template	=	$this->getParamValue('foreigntable_form_template', $columnNode, 'inputconfig/');
//		$foreigntable_model			=	$this->getParamValue('foreigntable_model', $columnNode, 'inputconfig/');
//		$foreigntable_method		=	$this->getParamValue('foreigntable_model_receive_records_methodname', $columnNode, 'inputconfig/');

	    $res = $this->CI->load->view( 'general/file_upload', array("error" => '' ), TRUE );
	    
	    return $res;
	}
	
	/**
	 * getFormLabel
	 * 
	 */
	private function getFormLabel($columnNode) {
		$result = "";
		$id = (string) $columnNode["id"];
		
		// permission
		$perm_name = $this->getTablenameById($id).'_'.$this->getFieldNameById($id);
		$editable = $this->CI->acl->getPermissionValue( $perm_name ); // 1 == editable, 2 == disabled, 3 == hidden

		if ($editable != "3") {
			$result .= isset( $columnNode->label ) ? $columnNode->label : "";
		}
		
		/* // history
		if( is_array($this->currentRecordValues) && array_key_exists('id', $this->currentRecordValues) ){
			$history_id = $this->getTablenameById($id).'_'.$this->currentRecordValues['id'].'-'.$this->getFieldNameById($id);
			$result .= '<i class="icon-time pull-right history_icon" id="'.$history_id.'"></i>';
		}*/

		return $result;
	}

	/**
	 * getHistory
	 *
	 */
	private function getHistory($columnNode) {
		$result = "";
		$id = (string) $columnNode["id"];

		// history
		if( is_array($this->currentRecordValues) && array_key_exists('id', $this->currentRecordValues) ){
			$history_id = $this->getTablenameById($id).'_'.$this->currentRecordValues['id'].'-'.$this->getFieldNameById($id);
			$result .= '<i class="icon-time pull-right history_icon" id="'.$history_id.'"></i>';
		}

		return $result;
	}

	public function getFormLabelById($columnId, $value = '') {
		$columnNode = $this->getColumnNodeById($columnId);
		return $this->getFormLabel($columnNode);
	}
	
	public function getFieldLabelById($columnId) {
		return $this->getFormLabelById($columnId);
	}

	/**
	 * getFieldNameById
	 * returns the fieldname of the colum with the given id
	 * 
	 */
	public function getFieldNameById($columnId) {
		$node = $this->tcaXML->xpath('//column[@id="' . $columnId . '"]');
		return is_array($node) && count($node) > 0 ? (string) $node[0]->fieldname : "";
	}


	/**
	 * getFieldTypeById
	 * returns the fieldtype of the colum with the given id
	 * 
	 */
	private function getFieldTypeById($columnId) {
		$node = $this->tcaXML->xpath('//column[@id="' . $columnId . '"]');
		return is_array($node) && count($node) > 0 ? (string) $this->getParamValue('type', $node[0], 'inputconfig/') : "";
	}

	/**
	 * getTablenameById
	 * returns table name of column with given id
	 */
	private function getTablenameById($columnId) {
		$node = $this->tcaXML->xpath('//column[@id="' . $columnId . '"]/..');
		//print_r($node[0]["name"]);
		return ( is_array($node) && count($node) > 0 ) ? (string) $node[0]["name"] : "";
	}

	/**
	 * getParamValue
	 * returns value of param-tag with given name
	 * 
	 */
	private function getParamValue($paramname, $node, $additionalXpath = "") {
		$res = array();
		if( !empty( $node ) ) {
			$res = $node->xpath($additionalXpath . 'param[@name="' . $paramname . '"]');
		}
		return is_array($res) && count($res) > 0 ? (string) $res[0] : "";
	}

	public function getIdByFieldName($fieldname, $tablename) {
		$node = $this->tcaXML->xpath('//table[@name="' . $tablename . '"]/column[fieldname="' . $fieldname . '"]');
		return is_array($node) && count($node) > 0 ? (string) $node[0]->attributes()->id : "";
	}

	public function getParamValueWithFieldId($columnId, $paramname, $paramNode = 'inputconfig/') {
		$node = $this->tcaXML->xpath('//column[@id="' . $columnId . '"]');
		return $this->getParamValue($paramname, $node[0], $paramNode);
	}

	/**
	 * getForeignTableValues
	 * returns array with value - label pairs from 
	 * foreign table as is is defined in given node.
	 */
	private function getForeignTableValues($columnNode) {
		$valueField = $this->getParamValue('valuefield', $columnNode, 'inputconfig/');
		$labelFields = $columnNode->xpath('inputconfig/param[@name="labelfield"]');
		$orderby = $columnNode->xpath('inputconfig/param[@name="orderby"]');
		$where = $this->getParamValue('where', $columnNode, 'inputconfig/');
		$tableName = $this->getParamValue('tablename', $columnNode, 'inputconfig/');
		$emptyPrefix = $this->getParamValue('emptyprefix', $columnNode, 'inputconfig/');

		return $this->getForeignTableValuesArray($tableName, $valueField, $labelFields, $orderby, $where, $emptyPrefix);
	}

	/**
	 * getForeignTableValuesArray
	 * fetch given values from table and return as array (for use in html selects)
	 */
	private function getForeignTableValuesArray($tableName, $valueField, $labelFields, $orderby, $where, $emptyPrefix ) {
		$result = array ();
		if( $emptyPrefix ) $result[""] = "please select ...";

		$where = $this->substituteSessionValues($where);
		$where = $this->substituteUserRightsValues($where);

		if ($valueField != "" && is_array($labelFields) && $tableName != "") {
			if ($orderby)
				$this->CI->db->order_by(implode(',', $orderby));
			if ($where)
				$this->CI->db->where($where, NULL, FALSE);
			$query = $this->CI->db->get($tableName);
			foreach ($query->result_array() as $row) {
				$labelfield = "";
				foreach ($labelFields as $labelkey) {
					$labelfield .= $row[(string) $labelkey] . ' - ';
				}
				$labelfield = rtrim($labelfield, " - ");
				$result[$row[$valueField]] = $labelfield;
			}
		}
		return $result;
	}


	/**
	 * getForeignTableValue
	 * returns value of field with "source = foreign_table"
	 */
	public function getForeignTableValue($fieldid, $foreignkey) {
		$node = $this->tcaXML->xpath('//column[@id="' . $fieldid . '"]');
		$list = $this->getForeignTableValues($node[0]);
		if (array_key_exists($foreignkey, $list)) {
			return $list[$foreignkey];
		} else {
			return "";
		}
	}

	/**
	 * getFieldValuesForDBquery
	 * returns array with tablenames, fieldnames and values for
	 * db inserts and updates.
	 * method takes array with the colum-ids and corresponding values
	 * like they are stored in $_POST when submitting an edit form.
	 * 
	 * the post vars are in the form array( 'columnId1' => array('value_record1', 'value_record2', ...), 'columId2' => array... )
	 * 
	 * @param colums - post vars from html-form
	 */
	public function getFieldValuesForDBquery($colums) {

		$insertData = array ();
		foreach ($colums as $key => $values) { // $values is an array! it may be that we have several records of the same table. so all values are stored in an array
			$fieldName = $this->getFieldNameById($key);
			$fieldType = $this->getFieldTypeById($key);
			if ($fieldName != "") {
				$tableName = $this->getTableNameById($key);
				if ($tableName != "") {
					$valresult = array();
					if(is_array($values)) {
						foreach ($values as $value) { 
							switch ($fieldType) {
								case 'date':
									// special case: client wants inofficial date format dd/mm/yyyy
									$transform = str_replace( '/', '.', $value );
									$tmp = !empty( $transform ) ? date( 'Y-m-d H:i:s', strtotime( $transform ) )  : '';
									$valresult[] = $tmp;
									break;
								case 'datetime':
									// special case: client wants inofficial date format dd/mm/yyyy
									$transform = str_replace( '/', '.', $value );
									$tmp = !empty( $transform ) ? date( 'Y-m-d H:i:s', strtotime( $transform ) )  : '';
									$valresult[] = $tmp;
									break;
								case 'float':
									//$valresult[] = sprintf($this->CI->config->item('floatvalues_format'), $value);
									$valresult[] = number_format( round( $value, 2 ), 2, '.', '');
									break;
								case 'double':
									//$valresult[] = sprintf($this->CI->config->item('doublevalues_format'), $value);
									$valresult[] = number_format( round( $value, 3 ) , 3, '.', '');
									break;
								default :
									$valresult[] = $value;
							}
						}	
					} else {
						$valresult[] = $values;
					}
					$insertData[$tableName][] = array (
						'name' => $fieldName,
						'value' => $valresult
					);
				}
			}
		}
		return $insertData;
	}

	/**
	 * getAllFieldnamesOfTable
	 * returns an array with all columnames of the given table
	 */
	public function getAllFieldnamesOfTable($tablename) {
		$result = array ();
		$tableNode = $this->tcaXML->xpath('table[@name="' . $tablename . '"]');
		if (is_array($tableNode)) {
			foreach ($tableNode[0]->xpath('column') as $column) {
				$result[] = (string) $column->fieldname;
			}
		}
		return $result;
	}

	private function array_in_array($needles, $haystack) {
		foreach ($needles as $needle) {
			if (in_array($needle, $haystack)) {
				return true;
			}
		}
		return false;
	}

	private function getFieldValueFromDBpreparedArray($fieldname, $fieldsArray) {
		foreach ($fieldsArray as $field) {
			if ($field['name'] == $fieldname)
				return $field['value'];
		}
		return NULL;
	}

	/**
	 * getColumnNodeById
	 * returns the fieldname of the colum with the given id
	 * 
	 */
	public function getColumnNodeById($columnId) {
		$node = $this->tcaXML->xpath('//column[@id="' . $columnId . '"]');
		return count($node) > 0 ? $node[0] : NULL;
	}

	public function getSearchParameter($paramname, $nodeId) {
		$node = $this->getColumnNodeById($nodeId);
		return $node ? $this->getParamValue($paramname, $node, 'searchconfig/') : '';
	}

	public function setUniqueId( $id ) {
		$this->uniqueId = $id;
	}

	
	///////////////////////////////////
	
	//	DEPRECATED - old stuff!
	
	///////////////////////////////////
	
	/**
	 * substituteSessionValues
	 * substitutes pattern "<session:*>" in given string with
	 * the corresponding session variable.
	 */
	private function substituteSessionValues($str) {
		return $str;
//		$CI = & get_instance();
//		$out = array ();
//		$matches = array ();
//		$pattern = '/<session:(.*?)>/';
//		preg_match($pattern, $str, $matches);
//		if (count($matches) == 2) {
//			$value = $CI->session->userdata[$matches[1]];
//			$str = str_replace($matches[0], $value, $str);
//
//		}
//		return $str;
	}

	/**
	 * substituteUserRightsValues
	 * substitutes pattern "<rights:*>" in given string with
	 * the corresponding data form the rightsmanagement.
	 */
	private function substituteUserRightsValues($str) {
		return $str;
		/*
		$CI =& get_instance();
		$CI->load->library( 'rightsmanagement' );
		$out		= array();
		$matches 	= array();
		$pattern 	= '/<rights:(.*?)>/';
		preg_match($pattern, $str, $matches);
		if( count($matches) == 2 ) {
			$value = $CI->rightsmanagement->getRightsValuesByKey($matches[1]);
			$str = str_replace($matches[0], $value, $str);
		}
		return $str;
		*/
	}
}

