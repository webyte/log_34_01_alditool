<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>

	<?=$this->load->view('dashboard/header', array( 'loggedIn' => true ), true); ?>

    <div class="container">
        <h2><?=$headline?></h2><br/>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div id="table" class="span15">
                <div class="vld-parent">
                   <loading :active.sync="isLoading"
                            :can-cancel="true"
                            :is-full-page="fullPage"></loading>
                </div>
                <b-table
                        :data="data"
                        :loading="isLoading"
                        :per-page="perPage"
                        paginated
                        current-page.sync="1"
                        pagination-position="bottom"
                        default-sort-direction="asc"
                        default-sort="contract_number"
                        sort-icon="arrow-up"
                        sort-icon-size="is-small">
                    <template slot-scope="props">
                        <b-table-column field="supplier" label="Supplier" sortable searchable>
                            {{ props.row.supplier }}
                        </b-table-column>
                        <b-table-column field="contract_number" label="Contract No" sortable numeric searchable>
                            {{ props.row.contract_number }}
                        </b-table-column>
                        <b-table-column field="product_code" label="Product Code" sortable searchable>
                            {{ props.row.product_code }}
                        </b-table-column>
                        <b-table-column field="sales_week" label="Sales Week" sortable numeric searchable>
                            {{ props.row.sales_week }}
                        </b-table-column>
                        <b-table-column field="hbl_hawb" label="HBL Number" sortable searchable>
                            {{ props.row.hbl_hawb }}
                        </b-table-column>
                        <b-table-column field="container_number" label="Container No" sortable searchable>
                            {{ props.row.container_number }}
                        </b-table-column>
                        <b-table-column field="dc" label="DC" sortable searchable>
                            {{ props.row.dc }}
                        </b-table-column>
                        <b-table-column field="destination_port" label="Destination" sortable searchable>
                            {{ props.row.destination_port }}
                        </b-table-column>
                        <b-table-column field="eta_timestamp" label="ETA" sortable>
                            {{ props.row.eta }}
                        </b-table-column>
                        <b-table-column field="commercial_invoice" label="Commercial Invoice" sortable>
                            <span v-bind:class="props.row.commercial_invoice_css">{{ props.row.commercial_invoice }}</span>
                        </b-table-column>
                        <b-table-column field="packing_list" label="Packing List" sortable>
                            <span v-bind:class="props.row.packing_list_css">{{ props.row.packing_list }}</span>
                        </b-table-column>
                        <b-table-column field="coo" label="Certificate of Origin" sortable>
                            <span v-bind:class="props.row.coo_css">{{ props.row.coo }}</span>
                        </b-table-column>
                        <b-table-column field="packing_declaration" label="Packing Declaration" sortable>
                            <span v-bind:class="props.row.packing_declaration_css">{{ props.row.packing_declaration }}</span>
                        </b-table-column>
                        <b-table-column field="telex_release" label="Telex Release" sortable>
                            <span v-bind:class="props.row.telex_release_css">{{ props.row.telex_release }}</span>
                        </b-table-column>
                        <b-table-column field="edo_sent" label="e-Delivery Order" sortable>
                            <span v-bind:class="props.row.edo_sent_css">{{ props.row.edo_sent }}</span>
                        </b-table-column>
                    </template>
                </b-table>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <a class="btn" href="dashboard/dashboard_document_management/export" target="_blank"><i class="icon-file"></i> Export</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">
                <br><a class="btn export-xls" href="#"><i class="icon-file"></i> Export view</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <small class="pull-right"><?=( $is_cached ? 'Cache: active' : 'Cache: inactive') ?></small>
            </div>
        </div>

    </div>

	<?=$this->load->view('dashboard/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=$this->load->view('dashboard/additional_jscss', '', true); ?>

    <script>
		Vue.use(VueLoading);
		Vue.component('loading', VueLoading)
        var table = new Vue({
			el: '#table',
			data: function() {
				return{
					data: [],
					currentPage: 1,
					perPage: 15,
					isLoading: true,
					fullPage: true               }
			},
			mounted: function() {
				this.loadData({});
			},
			methods: {
				loadData(params) {
					axios
						.get('dashboard/dashboard_document_management/getRecords', {
							params: params
						})
						.then(function(response) {
							table.data = response.data
							table.isLoading = false
						})
				}
			}
		})
    </script>

</body>
</html>