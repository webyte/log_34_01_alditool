<link rel="stylesheet" href="css/dashboard/materialdesignicons.min.css">
<script src="js/vendor/dashboard/es6-promise.auto.min.js"></script>
<script src="js/vendor/dashboard/vue.min.js"></script>
<script src="js/vendor/dashboard/vue-loading-overlay.js"></script>
<link rel="stylesheet" href="css/dashboard/vue-loading.css">
<script src="js/vendor/dashboard/buefy.min.js"></script>
<script src="js/vendor/dashboard/axios.min.js"></script>
<script src="js/vendor/dashboard/Chart.min.js"></script>
<script src="js/vendor/dashboard/vue-chartjs.min.js"></script>
<script src="js/vendor/sheetjs/xlsx.full.min.js"></script>
<script src="js/dashboard.js"></script>
