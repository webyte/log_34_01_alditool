<div class="navbar">
	<div class="navbar-inner">
		<div class="container">
			<div class="container">
				<a class="brand" href="<?=$this->config->item('base_url');?>"><img id="logo" src="<?=$this->config->item('base_url');?>img/logwin_logo.svg" alt="Logwin AG" width="200" /></a>
                <div class="pull-right">
                    <p style="padding: 10px; float: left; font-weight: bold">LEGACY</p>
                    <img style="vertical-align: top" id="aldi" src="<?=$this->config->item('base_url');?>img/aldi.png" alt="Aldi" width="50" height="60" />
                </div>
			</div>
			<hr/>
			<div class="nav-collapse">
				<ul class="nav">
					<?php print( $this->load->view('general/main_menu', '', true) ); ?>
				</ul>
			</div><!-- /.nav-collapse -->
		</div>
	</div><!-- /navbar-inner -->
</div>
