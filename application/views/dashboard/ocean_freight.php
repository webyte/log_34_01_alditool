<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>

	<?=$this->load->view('dashboard/header', array( 'loggedIn' => true ), true); ?>

    <div class="container">
        <h2><?=$headline?></h2><br/>
        <div class="row">
            <div class="span6 pie-container">
                <h5>Departure <a href="#departure-modal" data-toggle="modal"><i class="icon-question-sign"></i></a></h5>
                <canvas id="chart1"></canvas>
            </div>
            <div class="span6 pie-container">
                <h5>Arrival <a href="#arrival-modal" data-toggle="modal"><i class="icon-question-sign"></i></a></h5>
                <canvas id="chart2"></canvas>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <br/>
        <hr>
        <div class="row-fluid">
            <div id="table" class="span15">
                <div class="vld-parent">
                   <loading :active.sync="isLoading"
                            :can-cancel="true"
                            :is-full-page="fullPage"></loading>
                </div>
                <b-table
                        :data="data"
                        :loading="isLoading"
                        :paginated="isPaginated"
                        :per-page="perPage"
                        :current-page.sync="currentPage"
                        :pagination-simple="isPaginationSimple"
                        :pagination-position="paginationPosition"
                        :default-sort-direction="defaultSortDirection"
                        :sort-icon="sortIcon"
                        :sort-icon-size="sortIconSize">
                    <template slot-scope="props">
                        <b-table-column field="contract_number" label="Contract No" sortable numeric searchable>
                            {{ props.row.contract_number }}
                        </b-table-column>
                        <b-table-column field="sales_week" label="Sales Week" sortable searchable>
                            {{ props.row.sales_week }}
                        </b-table-column>
                        <b-table-column field="carrier" label="Carrier" sortable searchable>
                            {{ props.row.carrier }}
                        </b-table-column>
                        <b-table-column field="vessel_name" label="Vessel Name" sortable searchable>
                            {{ props.row.vessel_name }}
                        </b-table-column>
                        <b-table-column field="voyage_number" label="Voyage Number" sortable searchable>
                            {{ props.row.voyage_number }}
                        </b-table-column>
                        <b-table-column field="container_number" label="Container No" sortable searchable>
                            {{ props.row.container_number }}
                        </b-table-column>
                        <b-table-column field="departure_port" label="Departure" sortable searchable>
                            {{ props.row.departure_port }}
                        </b-table-column>
                        <b-table-column field="destination_port" label="Destination" sortable searchable>
                            {{ props.row.destination_port }}
                        </b-table-column>
                        <b-table-column field="etd_timestamp" label="ETD" sortable>
                            {{ props.row.etd }}
                        </b-table-column>
                        <b-table-column field="eta_timestamp" label="ETA" sortable>
                            {{ props.row.eta }}
                        </b-table-column>
                        <b-table-column field="atd_timestamp" label="ATD" sortable>
                            {{ props.row.atd }}
                        </b-table-column>
                        <b-table-column field="ata_timestamp" label="ATA" sortable>
                            {{ props.row.ata }}
                        </b-table-column>
                        <b-table-column field="discrepancy" label="Discrepancy" sortable>
                            <a class="btn comment-show" href="#" :data-id="props.row.contracts_id">Comments <span class="badge badge" v-if="props.row.comments_count">{{ props.row.comments_count }}</span></a>
                        </b-table-column>
                        <b-table-column field="status_departure" label="Status Departure" sortable>
                            <span v-bind:class="props.row.status_departure_css">{{ props.row.status_departure }}</span>
                        </b-table-column>
                        <b-table-column field="status_arrival" label="Status Arrival" sortable>
                            <span v-bind:class="props.row.status_arrival_css">{{ props.row.status_arrival }}</span>
                        </b-table-column>
                    </template>
                </b-table>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <a class="btn" href="dashboard/dashboard_ocean_freight/export" target="_blank"><i class="icon-file"></i> Export</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">
                <br><a class="btn export-xls" href="#"><i class="icon-file"></i> Export view</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <small class="pull-right"><?=( $is_cached ? 'Cache: active' : 'Cache: inactive') ?></small>
            </div>
        </div>

        <div id="comment-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Comments</h3>
            </div>
            <div class="modal-body">
                <form class="form-inline">
                    <input type="text" id="comment-msg" data-id="" placeholder="Type something…">
                    <button class="btn comment-add"><i class="icon-plus"></i> Add comment</button>
                </form>
                <hr>
                <div></div>
            </div>
        </div>
        <div id="departure-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Departure</h3>
            </div>
            <div class="modal-body">
                <p>
                    Shows the amount of contracts which departed on time or delayed.
                    <br><br>
                    <strong>On time:</strong><br>
                    The actual departure date is earlier or at the same day than the estimated departure date<br>
                    OR the estimated departure date is still in the future.
                    <br><br>
                    <strong>Delay:</strong><br>
                    The actual departure date is later than the estimated departure date<br>
                    OR the actual departure date is missing although the estimated departure date is in the past.
                </p>
            </div>
        </div>
        <div id="arrival-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Arrival</h3>
            </div>
            <div class="modal-body">
                <p>
                    Shows the amount of contracts which arrived on time or delayed.
                    <br><br>
                    <strong>On time:</strong><br>
                    The actual arrival date is earlier or the same day than the estimated arrival date<br>
                    OR the estimated arrival date is still in the future.
                    <br><br>
                    <strong>Delay:</strong><br>
                    The actual arrival date is later than the estimated arrival date<br>
                    OR the actual arrival date is missing although the estimated arrival date is in the past.
                </p>
            </div>
        </div>
    </div>

	<?=$this->load->view('dashboard/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=$this->load->view('dashboard/additional_jscss', '', true); ?>

    <script>
		Vue.use(VueLoading);
		Vue.component('loading', VueLoading)

        var chartOptions = {
			responsive: true,
			maintainAspectRatio: false,
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						return data.datasets[0].data[tooltipItem.index] + '%';
					}
				}
			}
		}
		var chart1 = new Vue({
			el: '#chart1',
			extends: VueChartJs.Pie,
			mounted: function() {
				axios
					.get('dashboard/dashboard_ocean_freight/getDeparturePieData')
					.then(function(response) {
						var data = {
								labels: response.data.labels,
								datasets: [{
									data: response.data.data,
									backgroundColor: [
										"#468847",
										"#b94a48"
									],
								}]
							}
						var cOptions = chartOptions;
						cOptions.onClick = chart1.graphClickHandler;
                        chart1.renderChart(data, cOptions)
                    })
			},
			methods: {
				graphClickHandler(e, a){
					if(a.length > 0 ) {
						var status = ( a[0]._index == 0 ) ? 'ontime' : 'delay';
						table.loadData({filter: {type: 'departure', value: status}});
					}
				}
			}
		})
		var chart2 = new Vue({
			el: '#chart2',
			extends: VueChartJs.Pie,
			mounted: function() {
				axios
					.get('dashboard/dashboard_ocean_freight/getArrivalPieData')
					.then(function(response) {
							var data = {
								labels: response.data.labels,
								datasets: [{
									data: response.data.data,
									backgroundColor: [
										"#468847",
										"#b94a48"
									],
								}]
							}
						var cOptions = chartOptions;
						cOptions.onClick = chart2.graphClickHandler;
                        chart2.renderChart(data, cOptions)
                    })
			},
			methods: {
				graphClickHandler(e, a){
					if(a.length > 0 ) {
						var status = ( a[0]._index == 0 ) ? 'ontime' : 'delay';
						table.loadData({filter: {type: 'arrival', value: status}});
					}
				}
			}
		})
        var table = new Vue({
			el: '#table',
			data: function() {
				return{
					data: [],
					isPaginated: true,
					isPaginationSimple: false,
					paginationPosition: 'bottom',
					defaultSortDirection: 'asc',
					sortIcon: 'arrow-up',
					sortIconSize: 'is-small',
					currentPage: 1,
					perPage: 15,
					isLoading: true,
					fullPage: true
				}
			},
			mounted: function() {
				this.loadData({});
			},
			methods: {
				loadData(params) {
					axios
						.get('dashboard/dashboard_ocean_freight/getRecords', {
							params: params
						})
						.then(function(response) {
							table.data = response.data
							table.isLoading = false
						})
				}
			}
		})
    </script>

</body>
</html>