<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>

	<?=$this->load->view('dashboard/header', array( 'loggedIn' => true ), true); ?>

    <div class="container">
        <h2><?=$headline?></h2><br/>
        <div class="row">
            <div class="span6 bar-container">
                <h5>Contract Ready in time <a href="#contract-modal" data-toggle="modal"><i class="icon-question-sign"></i></a></h5>
                <canvas id="chart1"></canvas>
            </div>
            <div class="span6 bar-container">
                <h5>Status Delivery <a href="#status-modal" data-toggle="modal"><i class="icon-question-sign"></i></a></h5>
                <canvas id="chart2"></canvas>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <br/>
        <hr>
        <div class="row-fluid">
            <div id="table" class="span15">
                <div class="vld-parent">
                   <loading :active.sync="isLoading"
                            :can-cancel="true"
                            :is-full-page="fullPage"></loading>
                </div>
                <b-table
                        :data="data"
                        :loading="isLoading"
                        :per-page="perPage"
                        paginated
                        current-page.sync="1"
                        pagination-position="bottom"
                        default-sort-direction="asc"
                        default-sort="contract_number"
                        sort-icon="arrow-up"
                        sort-icon-size="is-small">
                    <template slot-scope="props">
                        <b-table-column field="contract_number" label="Contract No" sortable numeric searchable>
                            {{ props.row.contract_number }}
                        </b-table-column>
                        <b-table-column field="sales_week" label="Sales Week" sortable searchable>
                            {{ props.row.sales_week }}
                        </b-table-column>
                        <b-table-column field="traffic_type" label="Traffic Type" sortable searchable>
                            {{ props.row.traffic_type }}
                        </b-table-column>
                        <b-table-column field="departure_port" label="Departure" sortable searchable>
                            {{ props.row.departure_port }}
                        </b-table-column>
                        <b-table-column field="dc" label="DC" sortable searchable>
                            {{ props.row.dc }}
                        </b-table-column>
                        <b-table-column field="ofu_real_timestamp" label="Booking Received Date" sortable>
                            {{ props.row.ofu_real }}
                        </b-table-column>
                        <b-table-column field="actual_contract_ready_date_timestamp" label="Actual Contract Ready Date" sortable>
                            {{ props.row.actual_contract_ready_date }}
                        </b-table-column>
                        <b-table-column field="received_in_cfs_timestamp" label="Received in CFS" sortable>
                            {{ props.row.received_in_cfs }}
                        </b-table-column>
                        <b-table-column field="container_gate_in_timestamp" label="CY Gate In" sortable>
                            {{ props.row.container_gate_in }}
                        </b-table-column>
                        <b-table-column field="etd_timestamp" label="ETD" sortable>
                            {{ props.row.etd }}
                        </b-table-column>
                        <b-table-column field="eta_timestamp" label="ETA" sortable>
                            {{ props.row.eta }}
                        </b-table-column>
                        <b-table-column field="atd_timestamp" label="ATD" sortable>
                            {{ props.row.atd }}
                        </b-table-column>
                        <b-table-column field="ata_timestamp" label="ATA" sortable>
                            {{ props.row.ata }}
                        </b-table-column>
                        <b-table-column field="cus_timestamp" label="CUS" sortable>
                            {{ props.row.cus }}
                        </b-table-column>
                        <b-table-column field="delivery_window_start_timestamp" label="Delivery Window Start" sortable>
                            {{ props.row.delivery_window_start }}
                        </b-table-column>
                        <b-table-column field="delivery_window_end_timestamp" label="Delivery Window End" sortable>
                            {{ props.row.delivery_window_end }}
                        </b-table-column>
                        <b-table-column field="status_delivery" label="Status Delivery" sortable>
                            <span v-bind:class="props.row.status_delivery_css">{{ props.row.status_delivery }}</span>
                        </b-table-column>
                    </template>
                </b-table>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <a class="btn" href="dashboard/dashboard_entire_transportation/export" target="_blank"><i class="icon-file"></i> Export</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">
                <br><a class="btn export-xls" href="#"><i class="icon-file"></i> Export view</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <small class="pull-right"><?=( $is_cached ? 'Cache: active' : 'Cache: inactive') ?></small>
            </div>
        </div>

        <div id="contract-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Contract Ready in Time</h3>
            </div>
            <div class="modal-body">
                <p>
                    Shows the three suppliers with the biggest amount of contracts with status "On Time"
                    <br><br>
                    <strong>On time:</strong><br>
                    The actual contract ready date is earlier or at the same day than the requested contract ready date<br>
                    OR the requested contract ready date is still in the future.
                    <br><br>
                    <strong>Delay:</strong><br>
                    The actual contract ready date is later than the requested contract ready date<br>
                    OR the actual contract ready date is missing but the requested contract ready date is in the past
                </p>
            </div>
        </div>
        <div id="status-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Status Delivery</h3>
            </div>
            <div class="modal-body">
                <p>
                    Shows the amount of contracts<br>
                    - who will be / were delivered on time<br>
                    - where there is a potential delay expected<br>
                    - who will be / were delivered with a delay
                    <br><br>
                    <strong>On time:</strong><br>
                    The actual arrival date is more than 3 days before the delivery window start date<br>
                    OR the goods have not arrived at the destination port yet and the delivery window start date is minimum 6 days in the future.
                    <br><br>
                    <strong>Potential delay:</strong><br>
                    The goods have not arrived at the destination port yet and the delivery window is within the next 3 to 5 days.
                    <br><br>
                    <strong>Delay:</strong><br>
                    The actual arrival date is less than 3 days before the delivery window start date<br>
                    OR the goods have not arrived at the destination port yet but the delivery window start date is less than 3 days ahead.
                </p>
            </div>
        </div>
    </div>

	<?=$this->load->view('dashboard/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=$this->load->view('dashboard/additional_jscss', '', true); ?>

    <script>
		Vue.use(VueLoading);
		Vue.component('loading', VueLoading)
		var chartOptions = {
			responsive: true,
			maintainAspectRatio: false,
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			},
			legend: {
				display: false
            },
			tooltips: {
				enabled: true
            }
		}
		var chart1 = new Vue({
			el: '#chart1',
			extends: VueChartJs.Bar,
			mounted: function() {
				axios
					.get('dashboard/dashboard_entire_transportation/getDelayedRecordsBySupplier')
					.then(function(response) {
						var data = {
								labels: response.data.labels,
								datasets: [{
									data: response.data.data,
								}]
							}
							chart1.renderChart(data, chartOptions)
						}
					)
			}
		})
		var chart2 = new Vue({
			el: '#chart2',
			extends: VueChartJs.Bar,
			mounted: function() {
				axios
					.get('dashboard/dashboard_entire_transportation/getRecordsByStatusDelivery')
					.then(function(response) {
							var data = {
								labels: response.data.labels,
								datasets: [{
									data: response.data.data,
									backgroundColor: [
										"#468847",
										"#f89406",
										"#b94a48"
									],
								}]
							}
						var cOptions = chartOptions;
						cOptions.onClick = chart2.graphClickHandler;
                        chart2.renderChart(data, cOptions)
                    })
			},
			methods: {
				graphClickHandler(e, a){
					if(a.length > 0 ) {
						var status = '';
						switch( a[0]._index ) {
							case 0:
								status = 'ontime';
								break;
							case 1:
								status = 'potentialdelay';
								break;
							case 2:
								status = 'delay';
								break;
						}
						table.loadData({filter: {type: 'status', value: status}});
					}
				}
			}
		})
        var table = new Vue({
			el: '#table',
			data: function() {
				return{
					data: [],
					currentPage: 1,
					perPage: 15,
					isLoading: true,
					fullPage: true               }
			},
			mounted: function() {
				this.loadData({});
			},
			methods: {
				loadData(params) {
					axios
						.get('dashboard/dashboard_entire_transportation/getRecords', {
							params: params
						})
						.then(function(response) {
							table.data = response.data
							table.isLoading = false
						})
				}
			}
		})
    </script>

</body>
</html>