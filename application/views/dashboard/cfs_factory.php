<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>

	<?=$this->load->view('dashboard/header', array( 'loggedIn' => true ), true); ?>

    <div class="container">
	    <h2><?=$headline?></h2><br/>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div id="table" class="span15">
                <div class="vld-parent">
                   <loading :active.sync="isLoading"
                            :can-cancel="true"
                            :is-full-page="fullPage"></loading>
                </div>
                <b-table
                        :data="data"
                        :loading="isLoading"
                        :per-page="perPage"
                        paginated
                        current-page.sync="1"
                        pagination-position="bottom"
                        default-sort-direction="asc"
                        default-sort="contract_number"
                        sort-icon="arrow-up"
                        sort-icon-size="is-small">
                    <template slot-scope="props">
                        <b-table-column field="contract_number" label="Contract No" sortable numeric searchable>
                            {{ props.row.contract_number }}
                        </b-table-column>
                        <b-table-column field="sales_week" label="Sales Week" sortable searchable>
                            {{ props.row.sales_week }}
                        </b-table-column>
                        <b-table-column field="traffic_type" label="Traffic Type" sortable searchable>
                            {{ props.row.traffic_type }}
                        </b-table-column>
                        <b-table-column field="departure_port" label="Departure" sortable searchable>
                            {{ props.row.departure_port }}
                        </b-table-column>
                        <b-table-column field="dc" label="DC" sortable searchable>
                            {{ props.row.dc }}
                        </b-table-column>
                        <b-table-column field="cbm" label="CBM" sortable>
                            {{ props.row.cbm }}
                        </b-table-column>
                        <b-table-column field="number_of_carton" label="Number of Carton" sortable>
                            {{ props.row.number_of_carton }}
                        </b-table-column>
                        <b-table-column field="received_in_cfs_timestamp" label="Received in CFS" sortable>
                            {{ props.row.received_in_cfs }}
                        </b-table-column>
                        <b-table-column field="intended_cfs_cut_off_timestamp" label="Intended CFS Cut Off" sortable>
                            {{ props.row.intended_cfs_cut_off }}
                        </b-table-column>
                        <b-table-column field="container_loaded_timestamp" label="Container Loaded" sortable>
                            {{ props.row.container_loaded }}
                        </b-table-column>
                        <b-table-column field="clp_approved_timestamp" label="CLP Approved" sortable>
                            {{ props.row.clp_approved }}
                        </b-table-column>
                        <b-table-column field="container_gate_in_at_terminal_timestamp" label="CY Gate In" sortable>
                            {{ props.row.container_gate_in_at_terminal }}
                        </b-table-column>
                        <b-table-column field="export_customs_cleared_timestamp" label="Export Customs Cleared" sortable>
                            {{ props.row.export_customs_cleared }}
                        </b-table-column>
                     </template>
                </b-table>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">
                <a class="btn" href="dashboard/dashboard_cfs_factory/export" target="_blank"><i class="icon-file"></i> Export</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">
                <br><a class="btn export-xls" href="#"><i class="icon-file"></i> Export view</a>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <small class="pull-right"><?=( $is_cached ? 'Cache: active' : 'Cache: inactive') ?></small>
        </div>
    </div>

	<?=$this->load->view('dashboard/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=$this->load->view('dashboard/additional_jscss', '', true); ?>

    <script>
		Vue.use(VueLoading);
		Vue.component('loading', VueLoading)
        var table = new Vue({
			el: '#table',
			data: function() {
				return{
					data: [],
					currentPage: 1,
					perPage: 15,
					isLoading: true,
					fullPage: true               }
			},
			mounted: function() {
				axios
					.get('dashboard/dashboard_cfs_factory/getRecords')
					.then(function(response) {
						table.data = response.data
						table.isLoading = false
                    })
			}
		})
    </script>

</body>
</html>