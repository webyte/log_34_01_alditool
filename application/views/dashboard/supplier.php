<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>

	<?=$this->load->view('dashboard/header', array( 'loggedIn' => true ), true); ?>

    <div class="container">
        <h2><?=$headline?></h2><br/>

        <div class="row">
            <div class="span6 bar-container">
                <h5>Status <a href="#status-modal" data-toggle="modal"><i class="icon-question-sign"></i></a></h5>
                <canvas id="chart1"></canvas>
            </div>
            <div class="span6 bar-container">
                <h5>Delayed Contracts by Supplier <a href="#delayed-modal" data-toggle="modal"><i class="icon-question-sign"></i></a></h5>
                <canvas id="chart2"></canvas>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <br/>
        <hr>
        <div class="row-fluid">
            <div id="table" class="span15">
                <div class="vld-parent">
                   <loading :active.sync="isLoading"
                            :can-cancel="true"
                            :is-full-page="fullPage"></loading>
                </div>
                <b-table
                        :data="data"
                        :loading="isLoading"
                        :per-page="perPage"
                        paginated
                        current-page.sync="1"
                        pagination-position="bottom"
                        default-sort-direction="asc"
                        default-sort="contract_number"
                        sort-icon="arrow-up"
                        sort-icon-size="is-small">
                    <template slot-scope="props">
                        <b-table-column field="contract_number" label="Contract No" sortable numeric searchable>
                            {{ props.row.contract_number }}
                        </b-table-column>
                        <b-table-column field="supplier" label="Supplier" sortable searchable>
                            {{ props.row.supplier }}
                        </b-table-column>
                        <b-table-column field="sales_week" label="Sales Week" sortable searchable>
                            {{ props.row.sales_week }}
                        </b-table-column>
                        <b-table-column field="traffic_type" label="Traffic Type" sortable searchable>
                            {{ props.row.traffic_type }}
                        </b-table-column>
                        <b-table-column field="departure" label="Departure" sortable searchable>
                            {{ props.row.departure }}
                        </b-table-column>
                        <b-table-column field="ofu_real_timestamp" label="Booking Received Date" sortable>
                            {{ props.row.ofu_real }}
                        </b-table-column>
                        <b-table-column field="fob_date_timestamp" label="Requested Contract Ready Date" sortable>
                            {{ props.row.fob_date }}
                        </b-table-column>
                        <b-table-column field="actual_contract_ready_date_timestamp" label="Actual Contract Ready Date" sortable>
                            {{ props.row.actual_contract_ready_date }}
                        </b-table-column>
                        <b-table-column field="delay_reason" label="Delay Reason" sortable searchable>
                            {{ props.row.delay_reason }}
                        </b-table-column>
                        <b-table-column field="discrepancy" label="Discrepancy">
                            <a class="btn comment-show" href="#" :data-id="props.row.contracts_id">Comments <span class="badge badge" v-if="props.row.comments_count">{{ props.row.comments_count }}</span></a>
                        </b-table-column>
                        <b-table-column field="status_contract" label="Status" sortable>
                            <span v-bind:class="props.row.status_contract_css">{{ props.row.status_contract }}</span>
                        </b-table-column>
                    </template>
                </b-table>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <a class="btn" href="dashboard/dashboard_supplier/export" target="_blank"><i class="icon-file"></i> Export</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">
                <br><a class="btn export-xls" href="#"><i class="icon-file"></i> Export view</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <small class="pull-right"><?=( $is_cached ? 'Cache: active' : 'Cache: inactive') ?></small>
            </div>
        </div>

        <div id="comment-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Comments</h3>
            </div>
            <div class="modal-body">
                <form class="form-inline">
                    <input type="text" id="comment-msg" data-id="" placeholder="Type something…">
                    <button class="btn comment-add"><i class="icon-plus"></i> Add comment</button>
                </form>
                <hr>
                <div></div>
            </div>
        </div>

        <div id="status-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Status</h3>
            </div>
            <div class="modal-body">
                <p>
                    Shows the amount of contracts which are on time or delayed.
                    <br><br>
                    <strong>On time:</strong><br>
                    The actual contract ready date is earlier or at the same day than the requested contract ready date<br>
                    OR the requested contract ready date is still in the future.
                    <br><br>
                    <strong>Delay:</strong><br>
                    The actual contract ready date is later than the requested contract ready date<br>
                    OR the actual contract ready date is missing but the requested contract ready date is in the past.
                </p>
            </div>
        </div>
        <div id="delayed-modal" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Delayed Contracts by Supplier</h3>
            </div>
            <div class="modal-body">
                <p>
                    Shows the five suppliers with the biggest amount of contracts with status "Delay"
                    <br><br>
                    <strong>On time:</strong><br>
                    The actual contract ready date is earlier or at the same day than the requested contract ready date<br>
                    OR the requested contract ready date is still in the future.
                    <br><br>
                    <strong>Delay:</strong><br>
                    The actual contract ready date is later than the requested contract ready date<br>
                    OR the actual contract ready date is missing but the requested contract ready date is in the past.
                </p>
            </div>
        </div>
    </div>

    <?=$this->load->view('dashboard/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=$this->load->view('dashboard/additional_jscss', '', true); ?>

    <script>
		Vue.use(VueLoading);
		Vue.component('loading', VueLoading)
		var chartOptions = {
			responsive: true,
			maintainAspectRatio: false,
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			},
			legend: {
				display: false
            },
			tooltips: {
				enabled: true
            }
		}
		var chart1 = new Vue({
			el: '#chart1',
			extends: VueChartJs.Bar,
			mounted: function() {
				axios
					.get('dashboard/dashboard_supplier/getRecordsByStatusContract')
					.then(function(response) {
                        var data = {
                            labels: response.data.labels,
                            datasets: [{
                                data: response.data.data,
                                backgroundColor: [
                                    "#468847",
                                    "#b94a48"
                                ],
                            }]
                        };
                        var cOptions = chartOptions;
                        cOptions.onClick = chart1.graphClickHandler;
                        chart1.renderChart(data, cOptions);
                    })
			},
			methods: {
				graphClickHandler(e, a){
					if(a.length > 0 ) {
						var status = ( a[0]._index == 0 ) ? 'ontime' : 'delay';
						table.loadData({filter: {type: 'status', value: status}});
                    }
				}
			}
		})
		var chart2 = new Vue({
			el: '#chart2',
			extends: VueChartJs.Bar,
			mounted: function() {
				axios
					.get('dashboard/dashboard_supplier/getDelayedRecordsBySupplier')
					.then(function(response) {
							var data = {
								labels: response.data.labels,
								datasets: [{
									data: response.data.data
								}]
							}
							chart2.renderChart(data, chartOptions)
						}
					)
			}
		})
        var table = new Vue({
			el: '#table',
			data: function() {
				return{
					data: [],
					currentPage: 1,
					perPage: 15,
					isLoading: true,
					fullPage: true
				}
			},
			mounted: function() {
				this.loadData({});
			},
			methods: {
				loadData(params) {
					axios
						.get('dashboard/dashboard_supplier/getRecords', {
							params: params
						})
						.then(function(response) {
							table.data = response.data
							table.isLoading = false
						})
				}
			}
		})
    </script>

</body>
</html>