<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
	<?= $this->load->view('general/header', array(), true); ?>
	<h1><?=$headline?></h1><br/>

	<?= $form['form.start']; ?>
	<?= $form['input.contracts.id']; ?>
	<?= $form['input.contracts.total_cases']; ?>
	<fieldset>
		<legend>Edit contract data</legend>
	</fieldset>

	<h4>Header data</h4>
	<div class="row">
		<div class="span3">
			<label><?= $form['label.contracts.contract_number']; ?></label>
			<?= $form['input.contracts.contract_number']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.traffic_type']; ?></label>
			<?= $form['input.contracts.traffic_type']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.departure']; ?></label>
			<?= $form['input.contracts.departure']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.incoterm_code']; ?></label>
			<?= $form['input.contracts.incoterm_code']; ?>
		</div>
		<div class="span3">
			<?php
			if($sendMailPerms){
				print( '<a class="pull-right inform-loo" href="#"><i class="icon-envelope"></i></a>' );
			}
			?>
			<label><?= $form['label.contracts.logwin_origin_office']; ?></label>
			<?= $form['input.contracts.logwin_origin_office']; ?>
		</div>
	</div>
	<div class="row">
		<div class="span3">
			<label><?= $form['label.contracts.volume']; ?></label>
			<?= $form['input.contracts.volume']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.gross_weight']; ?></label>
			<?= $form['input.contracts.gross_weight']; ?>
		</div>
        <div class="span3">
            <label><?= $form['label.contracts.contract_type']; ?></label>
			<?= $form['input.contracts.contract_type']; ?>
        </div>
		<div class="span3">
			<label><?= $form['label.contracts.crdate']; ?></label>
			<?= $form['input.contracts.crdate']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.modified']; ?></label>
			<?= $form['input.contracts.modified']; ?>
		</div>
	</div>
	<div class="row">
        <div class="span3">
			<?php
			if($sendMailPerms){
				print( '<a class="pull-right inform-supplier" href="#"><i class="icon-envelope"></i></a>' );
			}
			?>
            <label><?= $form['label.contracts.supplier']; ?></label>
			<?= $form['input.contracts.supplier']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.supplier_mail_sent']; ?></label>
			<?= $form['input.contracts.supplier_mail_sent']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.initial_sales_date']; ?></label>
			<?= $form['input.contracts.initial_sales_date']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.on_sale_date']; ?></label>
			<?= $form['input.contracts.on_sale_date']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.advertisement_week']; ?></label>
			<?= $form['input.contracts.advertisement_week']; ?>
        </div>

	</div>
	<div class="row">
        <div class="span3">
            <label><?= $form['label.contracts.ofu_scheduled']; ?></label>
			<?= $form['input.contracts.ofu_scheduled']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.ofu_real']; ?></label>
			<?= $form['input.contracts.ofu_real']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.no_booking_received']; ?></label>
			<?= $form['input.contracts.no_booking_received']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.aldi_au_wh_delivery_due_date']; ?></label>
			<?= $form['input.contracts.aldi_au_wh_delivery_due_date']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.aldi_au_wh_delivery_due_end_date']; ?></label>
			<?= $form['input.contracts.aldi_au_wh_delivery_due_end_date']; ?>
        </div>
	</div>
	<div class="row">
        <div class="span3">
            <label><?= $form['label.contracts.initial_crd']; ?></label>
			<?= $form['input.contracts.initial_crd']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.fob_date']; ?></label>
			<?= $form['input.contracts.fob_date']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.actual_cargo_ready_date']; ?></label>
			<?= $form['input.contracts.actual_cargo_ready_date']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.actual_contract_ready_date']; ?></label>
			<?= $form['input.contracts.actual_contract_ready_date']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.received_in_cfs']; ?></label>
			<?= $form['input.contracts.received_in_cfs']; ?>
        </div>
	</div>
    <div class="row">
        <div class="span3">
            <label><?= $form['label.contracts.delay_reason']; ?></label>
			<?= $form['input.contracts.delay_reason']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.comments']; ?></label>
			<?= $form['input.contracts.comments']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.pickup_point']; ?></label>
			<?= $form['input.contracts.pickup_point']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.freight_order_number']; ?></label>
			<?= $form['input.contracts.freight_order_number']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contracts.contact_details']; ?></label>
			<?= $form['input.contracts.contact_details']; ?>
        </div>
   </div>

	<h4>Contract information</h4>
	<div class="row">
		<div class="span3">
			<label><?= $form['label.contracts.product_code']; ?></label>
			<?= $form['input.contracts.product_code']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.product_description']; ?></label>
			<?= $form['input.contracts.product_description']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.units_per_case']; ?></label>
			<?= $form['input.contracts.units_per_case']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.case_length']; ?></label>
			<?= $form['input.contracts.case_length']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.case_width']; ?></label>
			<?= $form['input.contracts.case_width']; ?>
		</div>
	</div>
	<div class="row">
		<div class="span3">
			<label><?= $form['label.contracts.case_height']; ?></label>
			<?= $form['input.contracts.case_height']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.case_cubage']; ?></label>
			<?= $form['input.contracts.case_cubage']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.cube_qa_check']; ?></label>
			<?php if( !$isCubeQACheckDisabled ) { ?>
				<?= $form['input.contracts.cube_qa_check']; ?>
			<?php } else { ?>
				<input type="checkbox" checked="checked" disabled="disabled" readonly="readonly">
			<?php } ?>
		</div>
		<div class="span3">
			<label><?= $form['label.contracts.case_weight']; ?></label>
			<?= $form['input.contracts.case_weight']; ?>
		</div>
	</div>
	<h4>Contract details</h4>
	<?php
	if($newDeliveryPerms){
		print( '<a href="#" class="btn add-delivery"><i class="icon-plus"></i> New</a>' );
	}
	?>
	<div class="delivery-records">
		<?= $form['input.contracts.deliveries']; ?>
	</div>


	<div class="form-actions">
		<?= $form['form.submit']; ?>
		<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?= $this->config->item('base_url');?>contract/contract';"/>
	</div>
	</form>


	<!-- modal: supplier info mail -->

	<div class="modal hide" id="mailToSupplier">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3><?=$supplierMail['subject']; ?></h3>
		</div>
		<div class="modal-body">
			<?php
			if( count( $supplierRecipients ) ){
				echo '<h5>Please select contact person(s)</h5><div class="supplier-recipients">';
				foreach( $supplierRecipients as $recipient ){
					$checked = ( $recipient['default_contact'] == '1' ) ? ' checked="checked"' : '';
					echo '<label class="checkbox"><input type="checkbox" name="supplier_recipient" value="'.$recipient['id'].'"'.$checked.'> '.$recipient['name'] . '</label>';
				}
				echo '</div><hr />';
			}
			?>
			<p><textarea style="width: 510px; height: 200px;"><?=$supplierMail['body']; ?></textarea></p>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn">Cancel</a>
			<a class="btn btn-danger submit" rel="<?= $form['value.contracts.id']; ?>"><i class="icon-envelope icon-white"></i> Submit</a>
		</div>
	</div>


	<!-- modal: cube qa queck -->

	<div class="modal hide" id="cubeQACheck">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3>Notice</h3>
		</div>
		<div class="modal-body">
			<p>Have all dimensions been confirmed by CFS/Shipper?</p>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn cube-qa-check-unconfirmed">No</a>
			<a class="btn btn-success cube-qa-check-confirmed">Yes</a>
		</div>
	</div>


	<!-- modal: logwin origin office info mail -->

	<div class="modal hide" id="mailToLoo">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3><?=$looMail['subject']; ?></h3>
		</div>
		<div class="modal-body">
			<p><textarea style="width: 520px; height: 300px;"><?=$looMail['body']; ?></textarea></p>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn">Cancel</a>
			<a class="btn btn-danger submit" rel="<?= $form['value.contracts.id']; ?>"><i class="icon-envelope icon-white"></i> Submit</a>
		</div>
	</div>

	<?= $this->load->view('general/footer', '', true); ?>
	<?= $this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>