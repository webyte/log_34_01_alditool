<div class="delivery">
    <hr/>
	<?php
    if($deletePerms){
    print( '<a href="#" class="pull-right delete-delivery" rel="'.$form['value.deliveries.id'].'"><i class="icon-remove"></i></a><br/>' );
    }
	?>
	<?= $form['input.deliveries.id']; ?>
	<style>
        .delivery .row .span3 {
            width: 180px;
        }
        .delivery .row .span3 input {
            width: 166px;
        }
	</style>
    <div class="row">
		<div class="span3">
			<label><?= $form['label.deliveries.dc']; ?></label>
			<?= $form['input.deliveries.dc']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.deliveries.destination']; ?></label>
			<?= $form['input.deliveries.destination']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.deliveries.case_quantity']; ?></label>
			<?= $form['input.deliveries.case_quantity']; ?>
		</div>
        <div class="span3">
            <label><?= $form['label.deliveries.unit_quantity']; ?></label>
			<?php if( is_array( $form['value.deliveries.hbl_hawb_mm'] ) && count( $form['value.deliveries.hbl_hawb_mm'] ) > 0 ) { ?>
				<input type="text" value="<?= $form['value.deliveries.unit_quantity']; ?>" disabled="disabled" readonly="readonly">
				<input type="hidden" value="<?= $form['value.deliveries.unit_quantity']; ?>" name="deliveries_unit_quantity[]">
			<?php } else { ?>
				<?= $form['input.deliveries.unit_quantity']; ?>
			<?php } ?>
        </div>
        <div class="span3">
            <label><?= $form['label.deliveries.eta']; ?></label>
			<?= $form['input.deliveries.eta']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.deliveries.hbl_hawb_mm']; ?></label>
			<?php
				if( is_array( $form['value.deliveries.hbl_hawb_mm'] ) ) {
					foreach( $form['value.deliveries.hbl_hawb_mm'] as $item ) {
						if( $item['id'] > 0 ) {
							if( $this->acl->hasPermission( Acl::$controller_prefix.'shipment/shipment_unconfirmed' ) ) {
								print( '<a href="shipment/shipment/edit/'.$item['id'].'">'.str_replace( ',',', ',$item['hbl_hawb_mm']).'</a><br/>' );
							} else {
								if( $item['approval'] == 1 ) {
									print( '<a href="shipment/shipment_confirmed/edit/'.$item['id'].'">'.str_replace( ',',', ',$item['hbl_hawb']).'</a><br/>' );
								} else {
									print( '<a href="#" class="show-clp-unconfirmed-info" id="shipment-'.$item['id'].'">'.str_replace( ',',', ',$item['hbl_hawb']).'</a><br/>' );
								}
							}
						}
					}
				}
			?>
        </div>
	</div>
	<div class="row">
		<div class="span3">
			<label><?= $form['label.deliveries.unit_quantity_remain']; ?></label>
			<?= $form['input.deliveries.unit_quantity_remain']; ?>
		</div>
        <div class="span3">
            <label><?= $form['label.deliveries.shipment_number']; ?></label>
			<?= $form['input.deliveries.shipment_number']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.deliveries.order_number']; ?></label>
			<?= $form['input.deliveries.order_number']; ?>
        </div>
	</div>
</div>