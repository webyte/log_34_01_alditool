<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
	<?= $this->load->view('general/header', array(), true); ?>
	<h1>Upload</h1><br/>
	<?php
		if( isset( $error ) ) {
			$css = ( $error ) ? 'error' : 'success';
			print('<div class="alert alert-'.$css.'"><button class="close" data-dismiss="alert" type="button">×</button>'.$message.'</div>');
		}
	?>
    <div class="container">
		<?=form_open( 'contract/upload/do_upload', array( 'class' => 'contract-upload', 'enctype' => 'multipart/form-data' ) ); ?>
        <fieldset>
			<legend>Upload contract data</legend>
        </fieldset>

        <div class="row">
            <div class="span5">
                <input id="contract-upload-file" type="file" style="visibility:hidden;" name="contract-upload-file">
				<label>Contract</label>
				<?php
					if( $this->agent->is_browser('MSIE') ) {
				?>
				<input id="contract-upload-file" type="file" name="contract-upload-file">
				<?php
					} else {
				?>
				<div class="input-append">
					<input id="contract-upload-holder" class="input-large" type="text">
					<a class="btn" onclick="$('input[id=contract-upload-file]').click();"><i class="icon-file"></i> Browse</a>
				</div>
				<?php
					}
				?>
            </div>
        </div>
        <div class="row">
            <div class="span3">
                <label>Upload type</label>
				<?= form_dropdown( 'upload-type', array( 'regular' => 'Regular Upload', 'ecommerce' => 'eCommerce Upload') ); ?>
            </div>
            <div class="span3">
				<label>On-Sale week</label>
				<?= form_dropdown( 'contract-advertisement-week', $optionsAdvertisementWeek ); ?>
			</div>
            <div class="span3">
                <label>On-Sale year</label>
				<?= form_dropdown( 'contract-advertisement-year', $optionsAdvertisementYear, date('Y') ); ?>
            </div>
            <div class="span3">
                <label>Contract type</label>
				<?= form_dropdown( 'contract-type', $optionsContractType ); ?>
            </div>
        </div>
   	 	<div class="form-actions">
            <button type="submit" class="btn btn-primary">Upload</button>
            <input type="reset" class="btn" value="Reset">
        </div>
		<?=form_close(); ?>
    </div>

	<?= $this->load->view('general/footer', '', true); ?>
	<?= $this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>