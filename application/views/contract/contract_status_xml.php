<?php print('<?xml version="1.0" encoding="utf-8"?>') ?>
<StatusData>
    <Sender>LOGWIN</Sender>
    <EDIDateTime><?= date('Y-m-d').'T'.date('H:i:s'); ?></EDIDateTime>
    <OrderInformation>
        <PurchaseNumber><?= $contracts_contract_number; ?></PurchaseNumber>
        <ProductCode><?= $contracts_product_code; ?></ProductCode>
        <ProductDescription><?= $contracts_product_description; ?></ProductDescription>
        <SupplierCode><?= $supplier_code; ?></SupplierCode>
        <Type><?= $contracts_contract_type; ?></Type>
        <OSW><?= $contracts_advertisement_week; ?></OSW>
    </OrderInformation>
    <Status>
        <Code><?= $event_code; ?></Code>
        <Date><?= $event_date; ?></Date>
    </Status>
</StatusData>