<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
	<?=$this->load->view('general/header', array( 'loggedIn' => true ), true); ?>
	<h1><?=$headline?></h1><br/>
	<?php
		if(isset($searchform)){
			print( $searchform );
		}
	?>
	<div class="container">
		<?php
		if( $createnewlink != "" ) {
			print ( '<a href="'.$createnewlink.'" class="btn"><i class="icon-plus"></i> New</a><br/><br/>' );
		}
		?>
	</div>

	<?php if( !empty( $resulttable )) { ?>
	<ul id="pager-top" class="pager">
		<li><a href="#" class="btn btn-small first"><i class="icon-step-backward"></i></a></li>
		<li><a href="#" class="btn btn-small prev"><i class="icon-backward"></i></a></li>
		<li><input type="text" class="pagedisplay span1" /></li>
		<li><a href="#" class="btn btn-small next"><i class="icon-forward"></i></a></li>
		<li><a href="#" class="btn btn-small last"><i class="icon-step-forward"></i></a></li>
		<li>
			<select class="pagesize span1">
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
				<option selected="selected" value="100000">All</option>
			</select>
		</li>
	</ul>
	<?php } ?>

	<div class="container">
		<?=$resulttable?>
	</div>

	<?php if( !empty( $resulttable )) { ?>
	<ul id="pager-bottom" class="pager">
		<li><a href="#" class="btn btn-small first"><i class="icon-step-backward"></i></a></li>
        <li><a href="#" class="btn btn-small prev"><i class="icon-backward"></i></a></li>
        <li><input type="text" class="pagedisplay span1" /></li>
        <li><a href="#" class="btn btn-small next"><i class="icon-forward"></i></a></li>
        <li><a href="#" class="btn btn-small last"><i class="icon-step-forward"></i></a></li>
        <li>
			<select class="pagesize span1">
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
				<option selected="selected" value="100000">All</option>
			</select>
        </li>
	</ul>
	<?php } ?>

	<div class="modal hide" id="deleteWarning">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3>Delete this record</h3>
		</div>
		<div class="modal-body">
			<p>Do you really want to delete this contract?</p>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn">No</a>
			<a class="btn btn-danger delete"><i class="icon-trash icon-white"></i> Yes</a>
		</div>
	</div>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>
</body>
</html>