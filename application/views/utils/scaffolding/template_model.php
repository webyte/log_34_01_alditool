<?php print("<?php\r") ?>
require_once( APPPATH.'models/abstract_model'.EXT );

class <?=ucfirst($tablename)?>_model extends Abstract_model {

    function __construct() {
        parent::__construct();
        $this->tablename = '<?=$tablename?>';
    }

    /**
     * createNew
     * creates new record in table with the given insertdata
     * retrns the id of the new entry or -1 if something went wrong
     * @param array $insertData - associative array with data to insert into new table row
     * @return int new table-row id
     */
    function createNew( $insertData ) {
    	return  parent::createNew( $insertData );
    }

    /**
     * updateRecord
     * updates the record with the given id
     * 
     * @param int $id - row id
     * @param array $insertData - updated data
     */
    function updateRecord($id, $insertData ) {
    	parent::updateRecord($id, $insertData );
    }

    /**
     * getRecord
     * returns the db-record with the given id as array
     * 
     * @param int $id - id of record to retrieve
     * @return array - db record as array.
     */
    function getRecord( $id ) {
   		return parent::getRecord( $id );
    }
    
    /**
     * deleteRecord
     * deletes record with given id from table
     * 
     * @param int $id - row id
     * 
     */
    function deleteRecord( $id ) {
   		parent::deleteRecord( $id );
    }
   
}

/* End of file <?=$tablename; ?>.php */
/* Location: ./app/models/<?=$tablename; ?>.php */
