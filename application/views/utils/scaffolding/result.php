<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>
		<h1>Scaffolding Result</h1>
		<p class="lead"></p>
		<hr/>			
		<h3>TCA</h3>
		<?= $tca; ?>
		
		<h3>Controllers</h3>
		<?
			foreach ( $controlers as $tmpControler ) {
       			print $tmpControler;
			}
		?>
		<h3>Models</h3>
		<?
			foreach ( $models as $tmpModel ) {
       			print $tmpModel;
			}
		?>
		
		<h3>Views</h3>
		<?
			foreach ( $views as $tmpView ) {
       			print $tmpView;
			}
		?>

<?=$this->load->view('general/footer', '', true); ?>
<?=$this->load->view('general/javascript', '', true); ?>
</body>

</html>