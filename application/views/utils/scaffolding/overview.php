<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
	<?=$this->load->view('general/browser_warning', '', true); ?>
	<?=$this->load->view('general/header', array( 'loggedIn' => true ), true); ?>
	<h1>Scaffolding</h1>
	<p class="lead">Select the database tables you want to create all needed code from. Creates simple CRUD functionality and TCA settings.</p>
	<hr/>
	<?= $form['form.start']; ?>
	<?
		foreach ( $tables as $tablename => $chekbox ) {
			print '<label class="checkbox">'.$chekbox.$tablename.'</label>';
		}
	?>
	<div class="form-actions">
		<?= $form['form.submit']; ?>
		<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?=$this->config->item('base_url');?>utils/scaffolding';"/>
	</div>
	<?= $form['form.end']; ?>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
</body>

</html>