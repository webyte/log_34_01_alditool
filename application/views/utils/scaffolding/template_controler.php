<?php print("<?php\r") ?>
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class <?=ucfirst($tablename)?> extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('<?=ucfirst($tablename)?>_model', 'model');
		
		$this->tablename 			= '<?=$tablename?>';
		$this->scriptPath 			= 'manage/<?=$tablename?>/';
		$this->recordEditView 		= 'manage/<?=$tablename?>data_edit';
		$this->headline				= '<?=ucfirst($tablename)?>';
		$this->infotext				= '';
		$this->showDefaultSearchForm = FALSE;
	}

}

/* End of file <?=$tablename; ?>.php */
/* Location: ./app/controllers/<?=$tablename; ?>.php */