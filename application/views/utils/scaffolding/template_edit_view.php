<?php print("<?= $"."this->load->view('general/doctype_html', '', true); ?>"); ?>
<?php print("<?= $"."this->load->view('general/html_head', '', true); ?>"); ?>

<body>
	<?php print("<?= $"."this->load->view('general/header', array(), true); ?>\r"); ?>
	<h1><?= ucfirst($tablename); ?></h1><br/>

	<?php print("<?= $"."form['form.start']; ?>\r"); ?>
		<?php
			foreach ( $hiddenFields as $tmpHidden ) {
				print( "\t<?= $"."form['input.".$tablename.".".$tmpHidden."']; ?>\r" );
			}
		?>
		<legend>Edit <?=$tablename; ?> data</legend>

		<?php
			$res = '';
			$counter = 0;
			foreach ( $formFields as $tmpfield ) {
				if( $counter % 5 == 0 ) $res .= "\t\t\t".'<div class="row">'."\n";
				$res .= "\t\t\t\t".'<div class="span3">'."\n";
				$res .= "\t\t\t\t\t<label><?= $"."form['label.".$tablename.".".$tmpfield."']; ?></label>"."\n";
				$res .= "\t\t\t\t\t<?= $"."form['input.".$tablename.".".$tmpfield."']; ?>"."\n";
				$res .= "\t\t\t\t".'</div>'."\n";
				if( $counter % 5 == 4 ) $res .= "\t\t\t".'</div>'."\n";
				$counter++;
			}
			print($res);
		?>
		<div class="form-actions">
			<?php print("<?= $"."form['form.submit']; ?>"); ?>
			<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?php print("<?= $"."this->config->item('base_url');?>"); ?><?=$closelink?>';"/>
		</div>
	</form>

	<?php print("<?= $"."this->load->view('general/footer', '', true); ?>"); ?>
	<?php print("<?= $"."this->load->view('general/javascript', '', true); ?>"); ?>
</body>

</html>