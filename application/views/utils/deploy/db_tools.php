<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
		<?=$this->load->view('general/header', array( 'loggedIn' => true ), true); ?>
		<h1>DB Tools - Database: <?=$dbGroup?></h1>
		<p class="lead"></p>
		<hr/>			
		
		<a href="<?=$this->config->item('base_url');?>utils/deploy">back to overview</a><br/><br/>
				<?php
					echo "<h2>Restore from existing Backup</h2>";
					echo $dumpForm;
					echo "<br/><h2>Create Backup of current DB</h2>";
				?>
		<input onClick="location.href = '<?=$this->config->item('base_url');?>utils/deploy/dump_db/<?=$dbGroup?>/<?=$revision?>';" class="submit" type="button" name="compare" value="Create DB Dump from <?=$dbGroup?>  DB">

	<?= $this->load->view('general/footer', '', true); ?>
	<?= $this->load->view('general/javascript', '', true); ?>

</body>

</html>