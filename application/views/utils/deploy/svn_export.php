<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
	<?=$this->load->view('general/header', array( 'loggedIn' => true ), true); ?>
	<h1>SVN Export</h1>
	<p class="lead"></p>
	<hr/>			
	<p><?=$msg?></p>
	<textarea style="width:100%;" id ="stdout" rows="35" cols="120"></textarea>
	<a href="<?=$this->config->item('base_url');?>utils/deploy">back to overview</a>

	<?= $this->load->view('general/footer', '', true); ?>
	<?= $this->load->view('general/javascript', '', true); ?>
    <script type="text/javascript" charset="utf-8">
        $(document).ready( function() {
            setInterval( "updateLog()", 2000 );
        });
        function updateLog() {
            $.get('<?=$this->config->item('base_url');?>utils/deploy/get_log_file/<?=$headRev?>', function(data){
                $('#stdout').val(data);
                var a = $('#stdout');
                a.scrollTop( a[0].scrollHeight - a.height() );
            });
        }
    </script>
</body>

</html>