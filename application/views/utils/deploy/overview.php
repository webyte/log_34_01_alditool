<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
		<?=$this->load->view('general/header', array( 'loggedIn' => true ), true); ?>
		<h1>Deployment Manager</h1>
		<p class="lead"></p>
		<hr/>			
		
		<h2>Live</h2>
		<form id="liveVersion" method="post" action="utils/deploy/change_version/live">
		<table border="0" width="100%">
			<tr>
				<td>Links to: <strong><?= $liveLink ?></strong></td>
				<td>Change to: <?=$liveRevisionSelection?></td>
				<td><input onClick="location.href = 'utils/deploy/compare_db/production/<?=$liveLink?>';" class="submit" type="button" name="compare" value="Compare DB"></td>
				<td><input onClick="location.href = 'utils/deploy/db_tools/production/<?=$liveLink?>';" class="submit" type="button" name="compare" value="Restore/Backup DB Dump"></td>
				<td><input class="submit" type="submit" name="livesubmit" value="Change Revision" id="livesubmit"></td>
			</tr>
		</table>
		</form>	
		<br/>
		
		<h2>Staging</h2>
		<form id="liveVersion" method="post" action="utils/deploy/change_version/staging">
		<table border="0" width="100%">
			<tr>
				<td>Links to: <strong><?= $stagingLink ?></strong></td>
				<td>Change to: <?=$stagingRevisionSelection?></td>
				<td><input onClick="location.href = 'utils/deploy/compare_db/staging/<?=$stagingLink?>';" class="submit" type="button" name="compare" value="Compare DB"></td>
				<td><input onClick="location.href = 'utils/deploy/db_tools/staging/<?=$liveLink?>';" class="submit" type="button" name="compare" value="Restore/Backup DB Dump"></td>
				<td><input class="submit" type="submit" name="stagesubmit" value="Change Revision" id="stagesubmit"></td>
			</tr>
		</table>
		</form>
		<br/>
		
		<h2>Development</h2>
		<form id="liveVersion" method="post" action="utils/deploy/change_version/dev">
		<table border="0" width="100%">
			<tr>
				<td>Links to: <strong><?= $devLink ?></strong></td>
				<td>Change to: <?=$devRevisionSelection?></td>
				<td><input onClick="location.href = 'utils/deploy/compare_db/development/<?=$devLink?>';" class="submit" type="button" name="compare" value="Compare DB"></td>
				<td><input onClick="location.href = 'utils/deploy/db_tools/development/<?=$liveLink?>';" class="submit" type="button" name="compare" value="Restore/Backup DB Dump"></td>
				<td><input class="submit" type="submit" name="devsubmit" value="Change Revision" id="devsubmit"></td>
			</tr>
		</table>
		</form>
		<br/>
		<hr/>
		<br/>
		<h1>SVN Repository Download</h1>
		<form id="download" method="post" action="utils/deploy/download_latest_revision/rev_<?=$headRev?>">
			<input class="submit" type="submit" name="download" value="Download HEAD revision (<?=$headRev?>)" id="download">
		</form>


	<?= $this->load->view('general/footer', '', true); ?>
	<?= $this->load->view('general/javascript', '', true); ?>

</body>

</html>