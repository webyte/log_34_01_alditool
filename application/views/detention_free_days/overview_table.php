	<table class="table table-striped table-bordered table-condensed resulttable">
		<thead>
			<tr>
				<?php
					$fieldheaders = '';
					foreach( $fieldnames as $field ) {
						$additionalAttriputes = '';
 						if( $field['type'] == 'date' ) $additionalAttriputes = 'class="{sorter: \'germandate\'}"';
						if( $field['fieldname'] == 'contract_number' ) $additionalAttriputes = 'class="{sorter: \'text\'}"';
						$fieldheaders .= '<th '.$additionalAttriputes.'>'.$field['label'].'</th>';
					}
					print($fieldheaders);
					
					for ($index = 0; $index < count($recordactions); $index++) {
						print "<th class='{sorter: false}'>&nbsp;</th>";
					}
				?>
			</tr>	
		</thead>
		<tbody>
	<?php
	$tablerows = '';
	
	foreach( $records as $row ){
		$rowcolumns = '';
		foreach( $fieldnames as $field ){
			$rowcolumns .= '<td>'.$row[$field['fieldname']].'</td>';
		}
		foreach( $recordactions as $action ) {
			$actionControllerLink = base_url().$action['controller'].'/'.$row['id'];
			$rowcolumns .= '<td class="'.$action['title'].' span1"><a href="'.$actionControllerLink.'" title="'.$action['title'].'" class="btn btn-small"><i class="'.$action['icon'].'"></i></a></td>';
		}
		$tablerows .= '<tr>'.$rowcolumns.'</tr>';
	}
	print( $tablerows );
	?>
		</tbody>
	</table>

