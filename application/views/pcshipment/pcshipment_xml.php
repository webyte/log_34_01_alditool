<?php print('<?xml version="1.0" encoding="utf-8"?>') ?>
<StatusData>
    <Sender>LOGWIN</Sender>
    <EDIDateTime><?= date('Y-m-dH:i:s'); ?></EDIDateTime>
    <ShipmentInfo>
        <HBL><?= $pcshipments_hbl_hawb; ?></HBL>
        <Master><?= $pcshipments_mbl_mawb; ?></Master>
        <ShippingInfo>
            <FirstVessel><?= $pcshipments_mother_vessel_name; ?></FirstVessel>
            <FirstVoyage><?= $pcshipments_voyage_number; ?></FirstVoyage>
            <LastVessel><?= $pcshipments_vessel_2; ?></LastVessel>
            <LastVoyage><?= $pcshipments_voyage_2; ?></LastVoyage>
            <Carrier><?= $pcshipments_carrier; ?></Carrier>
            <Incoterm><?= $pcshipments_incoterm_code; ?></Incoterm>
            <TrafficType><?= $shipments_traffic_type; ?></TrafficType>
            <POL><?= $pcshipments_departure_port; ?></POL>
            <POD><?= $pcshipments_destination_port; ?></POD>
            <MainDC><?= $shipments_dc; ?></MainDC>
            <ContractType><?= $pcshipments_contract_type; ?></ContractType>
            <ContractNumber><?= $shipments_contract_number; ?></ContractNumber>
        </ShippingInfo>
        <ProductCollectionShipment>
        <?php foreach( $pcShipmentDeliveries as $delivery ) { ?>
            <Product>
                <PurchaseNumber><?= $delivery['contracts_contract_number']; ?></PurchaseNumber>
                <ProductCode><?= $delivery['deliveries_product_code']; ?></ProductCode>
                <ProductDescription><?= htmlspecialchars( $delivery['deliveries_product_description'] ); ?></ProductDescription>
                <SupplierName><?= $delivery['contracts_supplier']; ?></SupplierName>
                <Type><?= $delivery['contracts_contract_type']; ?></Type>
                <OSW><?= $delivery['contracts_advertisement_week']; ?></OSW>
                <QtyPack><?= $delivery['shipments_deliveries_mm_case_quantity']; ?></QtyPack>
                <DC><?= $delivery['deliveries_dc']; ?></DC>
            </Product>
			<?php } ?>
        </ProductCollectionShipment>
        <ContainerCollection>
            <Container>
                <ContainerNumber><?= $pcshipments_container_number; ?></ContainerNumber>
                <ISO><?= $container_type; ?></ISO>
                <SealNumber><?= $pcshipments_seal_number; ?></SealNumber>
                <GrossWeight>
                    <Weight><?= $gross_weight; ?></Weight>
                    <WeightUnit>KG</WeightUnit>
                </GrossWeight>
                <Temperature><?= $shipments_temperature; ?></Temperature>
                <DG><?= $shipments_dg; ?></DG>
                <EDOReceived><?= $shipments_edo_sent; ?></EDOReceived>
                <CustomsStatus><?= $shipments_cus; ?></CustomsStatus>
                <FreightOrder/>
                <ProductCollectionContainer>
					<?php foreach( $shipmentDeliveries as $delivery ) { ?>
                    <Product>
                        <PurchaseNumber><?= $delivery['contracts_contract_number']; ?></PurchaseNumber>
                        <ProductCode><?= $delivery['deliveries_product_code']; ?></ProductCode>
                        <ProductDescription><?= htmlspecialchars( $delivery['deliveries_product_description'] ); ?></ProductDescription>
                        <SupplierName><?= $delivery['contracts_supplier']; ?></SupplierName>
                        <Type><?= $delivery['contracts_contract_type']; ?></Type>
                        <OSW><?= $delivery['contracts_advertisement_week']; ?></OSW>
                        <QtyPack><?= $delivery['shipments_deliveries_mm_case_quantity']; ?></QtyPack>
                        <DC><?= $delivery['deliveries_dc']; ?></DC>
                    </Product>
					<?php } ?>
                </ProductCollectionContainer>
            </Container>
        </ContainerCollection>
        <Status>
            <Code><?= $event_code; ?></Code>
            <Date><?= $event_date; ?></Date>
        </Status>
    </ShipmentInfo>
</StatusData>