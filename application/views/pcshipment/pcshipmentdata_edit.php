<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>
<body>
<?= $this->load->view('general/header', array(), true); ?>
<h1>Shipments</h1><br/>

<?= $form['form.start']; ?>
<?= $form['input.pcshipments.id']; ?>
<legend>Edit shipments data</legend>

<div class="row">
    <div class="span3">
        <label><?= $form['label.pcshipments.hbl_hawb']; ?></label>
		<?= $form['input.pcshipments.hbl_hawb']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.mbl_mawb']; ?></label>
		<?= $form['input.pcshipments.mbl_mawb']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.obl']; ?></label>
		<?= $form['input.pcshipments.obl']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.incoterm_code']; ?></label>
		<?= $form['input.pcshipments.incoterm_code']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.carrier']; ?></label>
		<?= $form['input.pcshipments.carrier']; ?>
    </div>
</div>
<div class="row">
    <div class="span3">
        <label><?= $form['label.pcshipments.etd']; ?></label>
		<?= $form['input.pcshipments.etd']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.atd']; ?></label>
		<?= $form['input.pcshipments.atd']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.eta']; ?></label>
		<?= $form['input.pcshipments.eta']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.ata']; ?></label>
		<?= $form['input.pcshipments.ata']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.cus']; ?></label>
		<?= $form['input.pcshipments.cus']; ?>
    </div>
</div>
<div class="row">
    <div class="span3">
        <label><?= $form['label.pcshipments.aqc']; ?></label>
		<?= $form['input.pcshipments.aqc']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.container_loaded']; ?></label>
		<?= $form['input.pcshipments.container_loaded']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.container_unloaded']; ?></label>
		<?= $form['input.pcshipments.container_unloaded']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.mother_vessel_name']; ?></label>
		<?= $form['input.pcshipments.mother_vessel_name']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.voyage_number']; ?></label>
		<?= $form['input.pcshipments.voyage_number']; ?>
    </div>
</div>
<div class="row">
    <div class="span3">
        <label><?= $form['label.pcshipments.departure_port']; ?></label>
		<?= $form['input.pcshipments.departure_port']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.destination_port']; ?></label>
		<?= $form['input.pcshipments.destination_port']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.container_number']; ?></label>
		<?= $form['input.pcshipments.container_number']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.container_tare_weight']; ?></label>
		<?= $form['input.pcshipments.container_tare_weight']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.container_type']; ?></label>
		<?= $form['input.pcshipments.container_type']; ?>
    </div>
</div>
<div class="row">
    <div class="span3">
        <label><?= $form['label.pcshipments.container_size']; ?></label>
		<?= $form['input.pcshipments.container_size']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.seal_number']; ?></label>
		<?= $form['input.pcshipments.seal_number']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.po_number']; ?></label>
		<?= $form['input.pcshipments.po_number']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.certificate_of_origin_received']; ?></label>
		<?= $form['input.pcshipments.certificate_of_origin_received']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.duty_refund_completed']; ?></label>
		<?= $form['input.pcshipments.duty_refund_completed']; ?>
    </div>
</div>
<div class="row">
    <div class="span3">
        <label><?= $form['label.pcshipments.vessel_2']; ?></label>
		<?= $form['input.pcshipments.vessel_2']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.voyage_2']; ?></label>
		<?= $form['input.pcshipments.voyage_2']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.transshipment_port']; ?></label>
		<?= $form['input.pcshipments.transshipment_port']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.carrier_booking_no']; ?></label>
		<?= $form['input.pcshipments.carrier_booking_no']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.first_leg_eta']; ?></label>
		<?= $form['input.pcshipments.first_leg_eta']; ?>
    </div>
</div>
<div class="row">
    <div class="span3">
        <label><?= $form['label.pcshipments.first_leg_ata']; ?></label>
		<?= $form['input.pcshipments.first_leg_ata']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.last_leg_etd']; ?></label>
		<?= $form['input.pcshipments.last_leg_etd']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.last_leg_atd']; ?></label>
		<?= $form['input.pcshipments.last_leg_atd']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.lloyds_number_export']; ?></label>
		<?= $form['input.pcshipments.lloyds_number_export']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.lloyds_number_import']; ?></label>
		<?= $form['input.pcshipments.lloyds_number_import']; ?>
    </div>
</div>
<div class="row">
    <div class="span3">
        <label><?= $form['label.pcshipments.ietd']; ?></label>
		<?= $form['input.pcshipments.ietd']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.ieta']; ?></label>
		<?= $form['input.pcshipments.ieta']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.contract_number']; ?></label>
		<?= $form['input.pcshipments.contract_number']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.contract_type']; ?></label>
		<?= $form['input.pcshipments.contract_type']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.detention_free_days']; ?></label>
		<?= $form['input.pcshipments.detention_free_days']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.pcshipments.shipment_number']; ?></label>
		<?= $form['input.pcshipments.shipment_number']; ?>
    </div>
</div>
<br/><br/>
<div class="row">
    <div class="span9">
        <label>Link CLP</label>
        <input name="delivery-added-initially" value="0" type="hidden" id="delivery-added-initially"/>
        <table class="table table-bordered table-condensed link-clp">
            <thead>
            <tr>
                <th>Header DC</th>
                <th>Contract</th>
                <th>Case quantity (contract)</th>
                <th>Unit Quantity (contract)</th>
                <th>DC Delivery</th>
            </tr>
            </thead>
            <tbody>
			<?php
			foreach( $shipments as $shipment ) {
				//$out = '<tr><td colspan="6"><a href="shipment/shipment/edit/'.$shipment['id'].'">'.$shipment['hbl_hawb'].'</a></td></tr>';
				$out = '<tr class="row-clickable"><td>'.$shipment['dc'].'</td><td>'.$shipment['contract_numbers'].'</td><td>'.$shipment['case_quantities'].'</td><td>'.$shipment['unit_quantities'].'</td><td>'.$shipment['dcs'].'</td></tr>';
				$out .= '<tr style="display: none;"><td colspan="5"><table class="table table-bordered table-condensed">';
				$out .= '<tr><th>Header DC</th><th>Contract</th><th>Case quantity (contract)</th><th>Unit Quantity (contract)</th><th>DC Delivery</th><th>&nbsp;</th></tr>';
				foreach( $shipment['deliveries'] as $delivery ) {
					$out .= '<tr><td>'.$shipment['dc'].'</td><td>'.$delivery['contract_number'].'</td><td>'.$shipment['case_quantities'].'</td><td>'.$shipment['unit_quantities'].'</td><td>'.$delivery['dc'].'</td><td style="text-align: right">';

					if( $delivery['isLinked'] ) {
						$out .= '<button class="btn remove-delivery" type="button" data-delivery-id="'.$delivery['id'].'" data-shipment-id="'.$shipment['id'].'"><i class="icon-trash"></i></button>';
                    } else {
						$out .= '<button class="btn add-delivery" type="button" data-delivery-id="'.$delivery['id'].'" data-shipment-id="'.$shipment['id'].'"><i class="icon-plus"></i></button>';
                    }
					$out .= '</td></tr>';
				}
				$out .= '</table></td></tr>';
				print($out);
			}
			?>            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="span9">
        <label>CLP</label>
        <table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr>
                <th>Header DC</th>
                <th>Contracts</th>
                <th>Case quantity (contract)</th>
                <th>Unit Quantity (contract)</th>
                <th>DC Deliveries</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
			<?php
			foreach( $linkedShipments as $linkedShipment ) {
				$out = '
					<tr>
						<td>'.$linkedShipment['dc'].'</td>
						<td>'.$linkedShipment['contract_number'].'</td>
						<td>'.$linkedShipment['case_quantity'].'</td>
						<td>'.$linkedShipment['unit_quantity'].'</td>
						<td>'.$linkedShipment['deliveries_dc'].'</td>
						<td style="text-align: right"><button class="btn remove-delivery" type="button" data-shipment-id="'.$linkedShipment['shipment_id'].'" data-delivery-id="'.$linkedShipment['delivery_id'].'"><i class="icon-trash"></i></button></td>
					</tr>';
				print($out);
			}
			?>
            </tbody>
        </table>
    </div>
</div>
<div class="form-actions">
	<?= $form['form.submit']; ?>
    <input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?= $this->config->item('base_url');?>pcshipment/pcshipment';"/>
</div>
</form>


<!-- modal: linkCLPError -->

<div class="modal hide" id="linkCLPError">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3>Error</h3>
    </div>
    <div class="modal-body">
        <p>It is not possible to add this contract to the container. Only contracts from one CLP can be added to a container.</p>
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" class="btn">Ok</a>
    </div>
</div>


<?= $this->load->view('general/footer', '', true); ?>
<?= $this->load->view('general/javascript', '', true); ?>
<?=!empty($additionalJS) ? $additionalJS : ''; ?>
</body>

</html>