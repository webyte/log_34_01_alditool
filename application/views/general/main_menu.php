<?
$ci =& get_instance();
$ci->load->helper('file');
$ci->load->helper('url');
$ci->load->library('uri');
$ci->load->library('acl');
$loggedIn = array_key_exists('isLoggedIn', $ci->session->userdata) && $ci->session->userdata['isLoggedIn'];

$menu = '';
if( $loggedIn ) {
	$menuConf = $this->config->item('main_menu');

//    echo '<pre>';
//    var_dump($menuConf);
//    echo '</pre>';


	foreach ( $menuConf as $mainMenuItem ) {
   		if( !array_key_exists('submenu', $mainMenuItem) ) {
   			$menu .= getMenuItem($mainMenuItem, $ci); //'<li><a href="'.$this->config->item('base_url').$c.'" >'.$mainMenuItem['title'].'</a></li>';
   		} else {
			$t = $mainMenuItem['title'];
			$c = $mainMenuItem['controller'];
			if( $ci->acl->hasPermission( 'controller_'.$c ) ) {
	   			$menu .= '<li class="dropdown"><a title="'.$t.'" class="dropdown-toggle" href="#" data-toggle="dropdown" >'.$t.'<b class="caret caret-black"></b></a><ul class="dropdown-menu">';
				foreach ( $mainMenuItem['submenu'] as $subMenuItem ) {
					$menu .= getMenuItem($subMenuItem, $ci);
				}
				$menu .= '</ul></li>';
			}
   		}
	}
}

function getMenuItem($item, $ci) {
	$li = '';
	$t = $item['title'];
	$c = $item['controller'];
	if( $ci->acl->hasPermission( 'controller_'.$c ) ){
		$li =  '<li><a href="'.$ci->config->item('base_url').$c.'" >'.$t.'</a></li>';
	}

//    echo '<pre>';
//    var_dump($li);
//    echo '</pre>';


	return $li;
}

print $menu;
?>
