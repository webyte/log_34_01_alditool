<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><?=$this->lang->line('login.html.title');?></title>

	<meta name="description" content="<?=$this->lang->line('meta.description');?>">
    <meta name="keywords" content="<?=$this->lang->line('meta.keywords');?>">
    <meta name="author" content="<?=$this->lang->line('meta.author');?>">
    <meta name="viewport" content="width=device-width">

    <!--[if IE]><base href="<?=$this->config->item('base_url');?>"></base><![endif]-->
    <!--[if !IE]>--><base href="<?=$this->config->item('base_url');?>" /><!--<![endif]-->

    <link rel="shortcut icon" type="image/x-icon" href="<?=$this->config->item('base_url');?>favicon.ico">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->item('base_url');?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$this->config->item('base_url');?>css/main.css">
    <script src="js/vendor/modernizr/modernizr-2.6.1.min.js"></script>

</head>
