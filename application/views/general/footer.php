<?php
$ci =& get_instance();
$ci->load->helper('file');
$ci->load->helper('url');
$ci->load->library('uri');
$rev = read_file('../rev.txt');
$rev = $rev === FALSE ? "" : $rev;
$loggedIn = array_key_exists('isLoggedIn', $ci->session->userdata) && $ci->session->userdata['isLoggedIn'];
$versionInfo = $this->config->item('version').' r'.$rev;
if( $loggedIn ) {
	$versionInfo = '<small>logged in as '.$ci->session->userdata['username'].' <a href="login/logout">('.$this->lang->line( 'list.logout' ).')</a> '.$versionInfo.'</small>';
}
?>

    <hr/>
	<footer>
        <a href="assets/Logwin_AU_Trading_Terms.pdf" target="_blank" class="pull-right">Logwin AU Trading Terms</a>
        <p>Copyright &copy; <?php print( date( 'Y' ) );?> Logwin AG<br/>
            <?=$versionInfo?></p>
	</footer>
</div>
