<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
	<?= $this->load->view('general/header', array(), true); ?>
	<h1>Upload</h1><br/>
	<?php
		if( isset( $error ) ) {
			$css = ( $error ) ? 'error' : 'success';
			print('<div class="alert alert-'.$css.'"><button class="close" data-dismiss="alert" type="button">×</button>'.$message.'</div>');
		}
	?>
    <div class="container">
		<?=form_open( 'customs/customs_upload/do_upload', array( 'class' => 'customs-upload', 'enctype' => 'multipart/form-data' ) ); ?>
        <fieldset>
			<legend>Upload customs data</legend>
        </fieldset>

        <div class="row">
            <div class="span5">
                <input id="customs-upload-file" type="file" style="visibility:hidden;" name="customs-upload-file">
				<?php
					if( $this->agent->is_browser('MSIE') ) {
				?>
				<input id="customs-upload-file" type="file" name="customs-upload-file">
				<?php
					} else {
				?>
				<div class="input-append">
					<input id="customs-upload-holder" class="input-large" type="text">
					<a class="btn" onclick="$('input[id=customs-upload-file]').click();"><i class="icon-file"></i> Browse</a>
				</div>
				<?php
					}
				?>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-primary">Upload</button>
            <input type="reset" class="btn" value="Reset">
        </div>
		<?=form_close(); ?>
    </div>

	<?= $this->load->view('general/footer', '', true); ?>
	<?= $this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>