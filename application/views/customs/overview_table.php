<table class="table table-striped table-bordered table-condensed resulttable">
    <thead>
    <tr>
		<?php
		$fieldheaders = '';
		foreach( $fieldnames as $field ) {
			$additionalAttriputes = '';
			//if( $field['type'] == 'date' ) $additionalAttriputes = 'class="{sorter: \'germandate\'}"';
			$additionalAttriputes = 'class="{sorter: false}"';
			$fieldheaders .= '<th '.$additionalAttriputes.'>'.$field['label'].'</th>';
		}
		print($fieldheaders);

		for ($index = 0; $index < count($recordactions); $index++) {
			print "<th class='{sorter: false}'>&nbsp;</th>";
		}
		?>
    </tr>
    </thead>
    <tbody>
	<?php
	$tablerows = '';

	foreach( $records as $row ){
		$rowcolumns = '';
		foreach( $fieldnames as $field ){

            if( ( $field['fieldname'] == 'customs_product_specs' || $field['fieldname'] == 'customs_requirements' )
                && !empty( $row[$field['fieldname']] )
                && strlen( $row[$field['fieldname']] ) > 50
            ) {
				$rowcolumns .= '<td><div data-toggle="tooltip" data-title="'.htmlspecialchars(nl2br($row[$field['fieldname']])).'">'.substr( $row[$field['fieldname']], 0, 50 ).' ...</div></td>';
            } else {
			    $rowcolumns .= '<td>'.$row[$field['fieldname']].'</td>';
            }
		}
		foreach( $recordactions as $action ) {
			$actionControllerLink = base_url().$action['controller'].'/'.$row['id'];
			$rowcolumns .= '<td class="'.$action['title'].' span1"><a href="'.$actionControllerLink.'" title="'.$action['title'].'" class="btn btn-small"><i class="'.$action['icon'].'"></i></a></td>';
		}
		$tablerows .= '<tr>'.$rowcolumns.'</tr>';
	}
	print( $tablerows );
	?>
    </tbody>
</table>

