<table class="table table-striped table-bordered table-condensed resulttable">
    <thead>
    <tr>
		<?php
		$out = '<th class="{sorter: false}"><input type="checkbox" value="" name="container-select-all" id="container-select-all"></th>';
		$fieldheaders = '';
		foreach( $fieldnames as $field ) {
			$additionalAttriputes = '';
			if( $field['type'] == 'date' ) $additionalAttriputes = 'class="{sorter: \'germandate\'}"';
			$fieldheaders .= '<th '.$additionalAttriputes.'>'.$field['label'].'</th>';
		}
		$out .= $fieldheaders;

		print($out);
		?>
    </tr>
    </thead>
    <tbody>
	<?php
	$tablerows = '';
	foreach( $records as $row ){
		$rowcolumns = '<td><input type="checkbox" value="'.$row['id'].'" name="containerid[]" id="container-checkbox-'.$row['id'].'" class="container-checkbox"></td>';
		foreach( $fieldnames as  $field  ){
		    if( $field['fieldname'] == 'hbl' ) {
				$deliveries = $row['deliveries'];
				$rowcolumns .= '<td><table class="table table-striped table-bordered table-condensed resulttable"><tr><th></th><th>Telex released</th><th>CUS / AQC</th></tr>';
				$hblHawbs = array();
				foreach ($deliveries as $delivery) {

					if (!empty($delivery['hbl_hawb']) && !in_array($delivery['hbl_hawb'], $hblHawbs)) {
						$rowcolumns .= '<tr>';
						$rowcolumns .= '<td>' . $delivery['hbl_hawb'] . '</td>';
						$rowcolumns .= '<td>' . $delivery['telex_received'] . '</td>';
						$rowcolumns .= '<td>' . $row['cus'] . '</td>';
						$rowcolumns .= '</tr>';
						$hblHawbs[] = $delivery['hbl_hawb'];
					}
				}
				$rowcolumns .= '</table></td>';
			} else if( $field['fieldname'] == 'edo_sent' ) {
				$rowcolumns .= '<td><input type="checkbox" value="'.$row['id'].'" name="edo-sent" id="edo-checkbox-'.$row['id'].'" class="edo-checkbox" '.( $row['edo_sent'] == 1 ? 'checked="checked"' : '' ).'></td>';
			} else if( $field['fieldname'] == 'file_transmission'  ) {
		        if( $this->session->userdata('usergroup') == 1 && !empty($row['file_transmission']) ) {
					$rowcolumns .= '<td><a href="container/container/get_file/'.$row['id'].'" class="btn btn-small" target="_blank"><i class="icon-download"></i></a></td>';
                } else {
					$rowcolumns .= '<td>&nbsp;</td>';
                }
		    } else {
				$rowcolumns .= '<td>'.$row[$field['fieldname']].'</td>';
            }
		}
		$tablerows .= '<tr>'.$rowcolumns.'</tr>';
	}
	print( $tablerows );
	?>
    </tbody>
</table>

