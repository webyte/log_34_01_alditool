<?php print('<?xml version="1.0" encoding="utf-8"?>') ?>
<Booking xmlns="http://freighttracker.com.au/Schemas/ImportBooking" >
    <CustomerCode><?= $contract_type_aufre ?></CustomerCode>
    <Sender>LOG</Sender>
    <BookingReference><?= $shipments_id ?></BookingReference>
    <EDIDateTime><?= date('Y-m-d').'T'.date('H:i:s') ?></EDIDateTime>
    <JobType>Import</JobType>
    <VesselName><?= $shipments_vessel_2 ?></VesselName>
    <VoyageNumber><?= $shipments_voyage_2 ?></VoyageNumber>
    <LloydsNo><?= $pcshipments_lloyds_number_import ?></LloydsNo>
    <OceanBL><?= $pcshipments_mbl_mawb ?></OceanBL>
    <ShippingLine><?= $shipments_carrier ?></ShippingLine>
    <POL><?= $shipments_departure_port ?></POL>
    <POD><?= $shipments_destination_port ?></POD>
    <ETA><?= $shipments_eta ?></ETA>
    <AddressCollection>
        <Address>
            <AddressType>Consignee</AddressType>
            <AddressName><?= $consignee['dc_long'] ?></AddressName>
            <AddressCustomerCode><?= $consignee['truckercode'] ?></AddressCustomerCode>
            <AddressLine1><?= $consignee['street'] ?></AddressLine1>
            <AddressLine2></AddressLine2>
            <AddressSuburb><?= $consignee['suburb'] ?></AddressSuburb>
            <AddressPostCode><?= $consignee['postcode'] ?></AddressPostCode>
            <AddressState><?= $consignee['state'] ?></AddressState>
            <EquipmentAtAddress>N/A</EquipmentAtAddress>
            <ContainerLink>1</ContainerLink>
        </Address>
    </AddressCollection>
    <ContainerCollection>
        <Container>
            <LinkNumber>1</LinkNumber>
            <ContainerNumber><?= $shipments_container_number ?></ContainerNumber>
            <ISO><?= $shipments_container_size.$shipments_container_type ?></ISO>
            <SealNumber><?= $shipments_seal_number ?></SealNumber>
            <GrossWeight><?= $shipments_gross_weight ?></GrossWeight>
            <RequiredDate><?= $contracts_aldi_au_wh_delivery_due_date ?></RequiredDate>
            <DropMode></DropMode>
            <CargoType><?= $shipments_cargo_type ?></CargoType>
            <Temperature><?= $shipments_temperature ?></Temperature>
            <DG><?= $shipments_dg ?></DG>
            <UNNumber><?= $shipments_dg_class ?></UNNumber>
            <EDOReceived><?= $shipments_edo_sent ?></EDOReceived>
            <CustomsStatus><?= $shipments_customs_status ?></CustomsStatus>
            <AQISStatus><?= $shipments_customs_status ?></AQISStatus>
            <Notes></Notes>
            <ProductCollection>
                <?php
                foreach( $deliveries as $delivery ) {
                ?>
                <Product>
                    <ProductCode><?= $delivery['product_code'] ?></ProductCode>
                    <ProductDescription><![CDATA[<?= $delivery['product_description'] ?>]]></ProductDescription>
                    <PurchaseNumber><?= $delivery['contract_number'] ?></PurchaseNumber>
                    <SupplierName><![CDATA[<?= $delivery['supplier'] ?>]]></SupplierName>
                    <RangeType><?= $delivery['contract_type'] ?></RangeType>
                    <Week><?= $delivery['advertisement_week'] ?></Week>
                    <OnSaleDate><?= $contracts_on_sale_date ?></OnSaleDate>
                    <PackType>CASE</PackType>
                    <QtyPackType><?= $delivery['case_quantity'] ?></QtyPackType>
                    <DeliveryLocationCode><?= $delivery['truckercode'] ?></DeliveryLocationCode>
                </Product>
				<?php
				}
				?>
            </ProductCollection>
        </Container>
    </ContainerCollection>
</Booking>