<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
	<?=$this->load->view('general/header', array( 'loggedIn' => true ), true); ?>
	<h1><?=$headline?></h1><br/>
	<?php
		if(isset($searchform)){
			print( $searchform );
		}
	?>
	<div class="container">
		<br/>
	</div>

	<?php if( $transmitSuccess ) { ?>
        <div class="container">
            <h5>Data was transmitted successfully.</h5>
        </div>
	<?php } ?>


	<?php if( !empty( $resulttable )) { ?>
	<ul id="pager-top" class="pager">
		<li><a href="#" class="btn btn-small first"><i class="icon-step-backward"></i></a></li>
		<li><a href="#" class="btn btn-small prev"><i class="icon-backward"></i></a></li>
		<li><input type="text" class="pagedisplay span1" /></li>
		<li><a href="#" class="btn btn-small next"><i class="icon-forward"></i></a></li>
		<li><a href="#" class="btn btn-small last"><i class="icon-step-forward"></i></a></li>
		<li>
			<select class="pagesize span1">
				<option value="10">10</option>
				<option selected="selected" value="20">20</option>
				<option value="50">50</option>
				<option  value="100">100</option>
			</select>
		</li>
	</ul>

	<div class="container">
        <div style="overflow-x: scroll;">
			<?=$resulttable?>
        </div>

		<?php
        if( $this->acl->hasPermission( Acl::$action_prefix.'container/container_transmit' ) ) {
		?>
            <?= form_open($transmitlink, array('class' => 'transmit-container')); ?>
            <input name="container-ids" value="" type="hidden" id="container-ids"/>
            <button type="submit" class="btn transmit-container-button"><i class="icon-file"></i> Transmit data</button>

            <?= form_close(); ?>
        <?php
		}
        ?>

	</div>

	<ul id="pager-bottom" class="pager">
		<li><a href="#" class="btn btn-small first"><i class="icon-step-backward"></i></a></li>
        <li><a href="#" class="btn btn-small prev"><i class="icon-backward"></i></a></li>
        <li><input type="text" class="pagedisplay span1" /></li>
        <li><a href="#" class="btn btn-small next"><i class="icon-forward"></i></a></li>
        <li><a href="#" class="btn btn-small last"><i class="icon-step-forward"></i></a></li>
        <li>
			<select class="pagesize span1">
				<option value="10">10</option>
				<option selected="selected" value="20">20</option>
				<option value="50">50</option>
				<option  value="100">100</option>
			</select>
        </li>
	</ul>
    <?php } ?>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>
</body>
</html>