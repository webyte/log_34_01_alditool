<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>

	<br/><br/><br/>
	<h1><?php print( $this->lang->line( 'login.headline' ) ); ?></h1>
	<p><?php print( $this->lang->line( 'login.body' ) ); ?></p>
	<br/>

	<form id="loginform" action="login" method="post">
		<input name="action" value="login" type="hidden"/>
		<input name="redirect" value="<?=$redirect; ?>" type="hidden"/>
		<?php
		if( $error ) {
			print( '<div class="alert alert-error">'.$this->lang->line( 'login.message.2' ).'</div>' );
		}
		?>
		<fieldset>
			<legend><?= $this->lang->line( 'login.message.1' ) ?></legend>
			<label><?php print( $this->lang->line( 'login.field.user' ) ); ?></label>
			<input <?php if( $error ) print('class="error"') ?> name="username" value="" type="text" placeholder="Enter your username"/>
			<label><?php print( $this->lang->line( 'login.field.pwd' ) ); ?></label>
			<input <?php if( $error ) print('class="error"') ?> name="password" value="" type="password" placeholder="Enter your password"/>
			<div class="form-actions">
				<input class="btn btn-primary" name="submit" value="<?php print( $this->lang->line( 'login.field.submit' ) ); ?>" type="submit"/>
				<input class="btn" name="reset" value="<?php print( $this->lang->line( 'login.field.cancel' ) ); ?>" type="reset"/>
			</div>
		</fieldset>
	</form>

	<p>
		<br /><br />
		<?=$this->lang->line( 'login.forgot' ); ?>
		<?=$this->lang->line( 'login.change' ); ?>
	</p>

<?=$this->load->view('general/footer', '', true); ?>
<?=$this->load->view('general/javascript', '', true); ?>
</body>
</html>