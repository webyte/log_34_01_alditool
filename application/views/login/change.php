<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', false); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>

	<br/><br/><br/>
	<h1>Change your Password</h1>
	<p><?php print( $this->lang->line( 'login.change.body' ) ); ?></p>
	<br/>

	<form id="pwchangeform" action="" method="post" class="form">
		<input name="action" value="change" type="hidden"/>
	<?php
		if( $error ) {
			print( '<div class="alert alert-error">'.$message.'</div>' );
		} else {
			if($message != "") print( '<div class="alert alert-success">'.$message.'</div>' );
		}
	?>
	<?php
		if($pwchange == FALSE){
	?>

		<fieldset>
			<legend><?= $this->lang->line( 'login.change.message.1' ) ?></legend>
			<label>Username:</label>
			<input <?php if( $error ) print('class="error"') ?> name="username" id="username" value="" type="text" class="validate[required]" placeholder="Enter your username"/><br />

			<label>E-Mail:</label>
			<input <?php if( $error ) print('class="error"') ?> name="email" id="email" value="" type="text" class="validate[required]" placeholder="Enter your e-mail"/><br />
			<br />
			<label>Old password:</label>
			<input <?php if( $error ) print('class="error"') ?> name="oldpassword" id="oldpassword" value="" type="password" class="validate[required]" placeholder="Enter your old password"/><br />

			<label>New password:</label>
			<input <?php if( $error ) print('class="error"') ?> name="password" id="password" value="" type="password" class="validate[required,minSize[5]]" placeholder="Enter your new password"/><br />

			<label>Retype new password</label>
			<input <?php if( $error ) print('class="error"') ?> name="password2" id="password2" value="" type="password" class="validate[required,equals[password],minSize[5]]" placeholder="Enter your new password"/>

			<div class="form-actions">
				<input class="btn btn-primary" name="submit" value="<?php print( $this->lang->line( 'forgot.field.submit' ) ); ?>" type="submit"/>
				<input class="btn" name="reset" value="<?php print( $this->lang->line( 'login.field.cancel' ) ); ?>" type="reset"/>
			</div>

		</fieldset>
		<?php
		}
		?>

	</form>


	<p><br /><br /><a href="<?=$this->config->item('base_url');?>">Back to login form</a></p>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
</body>
</html>