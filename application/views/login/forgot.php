<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
	<?=$this->load->view('general/browser_warning', '', true); ?>
	<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>


	<br/><br/><br/>
	<h1><?php print( $this->lang->line( 'forgot.headline' ) ); ?></h1>
	<p><?php print( $this->lang->line( 'forgot.body' ) ); ?></p>
	<br/>

	<form id="loginform" action="forgot" method="post">
		<input name="action" value="forgot" type="hidden"/>
		<?php
		if( $error ) {
			print( '<div class="alert alert-error">'.$message.'</div>' );
		} else {
			if($message != "")	print( '<div class="alert alert-success">'.$message.'</div>' );
		}
		?>
		<fieldset>
			<legend><?= $this->lang->line( 'forgot.message.1' ) ?></legend>
			<label><?php print( $this->lang->line( 'forgot.field.mail' ) ); ?></label>
			<input <?php if( $error ) print('class="error"') ?> name="email" value="" type="text" placeholder="Enter your mail"/>
			<div class="form-actions">
				<input class="btn btn-primary" name="submit" value="<?php print( $this->lang->line( 'forgot.field.submit' ) ); ?>" type="submit"/>
				<input class="btn" name="reset" value="<?php print( $this->lang->line( 'login.field.cancel' ) ); ?>" type="reset"/>
			</div>
		</fieldset>
	</form>

	<p><br /><br /><a href="<?=$this->config->item('base_url');?>">Back to login form</a></p>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
</body>
</html>