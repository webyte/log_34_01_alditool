<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
	<?= $this->load->view('general/header', array(), true); ?>
	<h1><?=$headline?></h1><br/>

	<?= $form['form.start']; ?>
		<h4>Header</h4>
		<div class="row">
			<div class="span2">
				<label><?= $form['label.deliveries.contract_number']; ?></label>
				<p class="muted"><?= $record['deliveries_contract_number']; ?></p>
			</div>
			<div class="span2">
				<label><?= $form['label.deliveries.dc']; ?></label>
                <p class="muted"><?= $record['deliveries_dc']; ?></p>
			</div>
			<div class="span2">
				<label><?= $form['label.deliveries.product_code']; ?></label>
                <p class="muted"><?= $record['deliveries_product_code']; ?></p>
			</div>
			<div class="span2">
				<label>Pieces</label>
                <p class="muted"><?= $record['shipments_deliveries_mm_case_quantity']; ?></p>
			</div>
            <div class="span2">
                <label><?= $form['label.shipments.container_number']; ?></label>
                <p class="muted"><?= $record['shipments_container_number']; ?></p>
            </div>
            <div class="span2">
                <label><?= $form['label.pcshipments.hbl_hawb']; ?></label>
                <p class="muted"><?= $record['pcshipments_hbl_hawb']; ?></p>
            </div>
            <div class="span2">
                <label><?= $form['label.shipments.destination_port']; ?></label>
                <p class="muted"><?= $record['shipments_destination_port']; ?></p>
            </div>
		</div>
		<div class="row">
            <div class="span2">
                <label><?= $form['label.shipments.etd']; ?></label>
                <p class="muted"><?= $record['shipments_etd']; ?></p>
            </div>
            <div class="span2">
                <label><?= $form['label.shipments.eta']; ?></label>
                <p class="muted"><?= $record['shipments_eta']; ?></p>
            </div>
            <div class="span2">
                <label><?= $form['label.shipments.atd']; ?></label>
                <p class="muted"><?= $record['shipments_atd']; ?></p>
            </div>
            <div class="span2">
                <label><?= $form['label.shipments.ata']; ?></label>
                <p class="muted"><?= $record['shipments_ata']; ?></p>
            </div>
		</div>
        <hr/>
        <br/><br/>
        <h4>Documents</h4>
        <table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr>
                <th>Document</th>
                <th>Mandatory</th>
                <th>Uploaded</th>
                <th>Status</th>
                <th>Confirmed</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
        <?php foreach( $documents as $key=>$document ) { ?>
            <tr>
                <td><?= $document['document_type_title'] ?></td>
                <td><?= form_dropdown('mandatory', array( 0 => 'No', 1 => 'Yes', 2 => 'N/R' ), $document['mandatory'], 'class="span2" data-record-id="'.$record['id'].'" data-document-id="'.$document['id'].'"' ); ?></td>
                <td><?= $document['uploaded'] ?></td>
                <td>
                    <?php
                        switch( $document['status'] ) {
                            case Document_model::$STATUS_DOCUMENT_MISSING:
                                $css = 'badge badge-important';
                                break;
							case Document_model::$STATUS_DOCUMENT_UPLOADED:
								$css = 'badge badge-warning';
								break;
                            case Document_model::$STATUS_DOCUMENT_CONFIRMED:
								$css = 'badge badge-success';
								break;
                        }
                        print('<span class="'.$css.'"></span>');
                    ?>
                </td>
            <?php if( !empty( $document['file'] ) ) { ?>
				<?php if( $this->acl->hasPermission( 'action_supplier/supplier_portal_confirmDocument' ) ) { ?>
                <td><?= form_checkbox( array( 'name' => 'confirmed', 'value' => '1', 'checked' => $document['confirmed'], 'data-record-id' => $record['id'], 'data-document-id' => $document['id'], 'class' => 'supplier-document-confirmed' ) ) ?></td>
				<?php } else { ?>
                <td>&nbsp;</td>
				<?php } ?>
				<?php if( $this->acl->hasPermission( 'action_supplier/supplier_portal_viewDocument' ) ) { ?>
                <td class="span1"><a href="supplier/supplier_portal/viewDocument/<?= $document['id'] ?>" title="view" class="btn btn-small" target="_blank"><i class="icon-eye-open"></i></a></td>
				<?php } else { ?>
                <td>&nbsp;</td>
				<?php } ?>
				<?php if( $this->acl->hasPermission( 'action_supplier/supplier_portal_deleteDocument' ) ) { ?>
                <td class="span1"><a href="supplier/supplier_portal/deleteDocument/<?= $record['id'].'/'.$document['id'] ?>" title="delete" class="btn btn-small"><i class="icon-trash"></i></a></td>
				<?php } else { ?>
                <td>&nbsp;</td>
				<?php } ?>
            <?php } else { ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            <?php } ?>
            </tr>
        <?php } ?>
            </tbody>
        </table>
    </form>
	<?php if( $this->acl->hasPermission( 'action_supplier/supplier_portal_uploadDocument' ) ) { ?>
    <hr/>
    <?=form_open( 'supplier/supplier_portal/uploadDocument/'.$record['id'], array( 'class' => 'supplier-upload form', 'enctype' => 'multipart/form-data' ) ); ?>
        <div class="row">
            <div class="span3">
                <label>Document type</label>
				<?= form_dropdown( 'document-type', $documentTypes ); ?>
            </div>
        </div>
        <div class="row">
           <div class="span5">
               <input id="supplier-upload-file" type="file" style="visibility:hidden;" name="supplier-upload-file">
               <label>Document</label>

               <div class="well" id="supplier-upload-holder">
                   <p style="text-align: center; overflow: hidden; height: 65px;">Drag and Drop file here<br/>or<br/>Click to select file</p>
               </div>

           </div>
        </div>
        <div class="row">
            <div class="span3">
                 <button type="submit" class="btn btn-primary">Upload</button>
            </div>
        </div>
    <?=form_close(); ?>
	<?php } ?>
	<?php if( $this->acl->hasPermission( 'action_supplier/supplier_portal_viewComment' ) ) { ?>
    <hr/>
    <br/><br/>
	<?=form_open( 'supplier/supplier_portal/addComment/'.$record['id'], array( 'class' => 'form') ); ?>
        <h4>Comments</h4>
        <table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr>
                <th>Date</th>
                <th>Comment</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach( $comments as $comment ) { ?>
                <tr>
                    <td><?= $comment['crdate'] ?></td>
                    <td><?= $comment['msg'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
	    <?php if( $this->acl->hasPermission( 'action_supplier/supplier_portal_addComment' ) ) { ?>
        <hr/>
        <div class="row">
            <div class="span6">
                <label>New Comment</label>
                <?= form_textarea(array('name' => 'msg', 'class' => 'span6')) ?>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
		<?php } ?>
    <?=form_close(); ?>
	<?php } ?>

    <?= $this->load->view('general/footer', '', true); ?>
	<?= $this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>