<table class="table table-striped table-bordered table-condensed resulttable">
    <thead>
    <tr>
		<?php
		$fieldheaders = '';
		foreach( $fieldnames as $field ) {
			$additionalAttriputes = '';
			if( $field['type'] == 'date' ) $additionalAttriputes = 'class="{sorter: \'germandate\'}"';
			$fieldheaders .= '<th '.$additionalAttriputes.'>'.$field['label'].'</th>';
		}
		print($fieldheaders);

		foreach( $recordactions as $action ) {
			if( $action['title'] == 'edit' ) print "<th class='{sorter: false}'>&nbsp;</th>";
		}
		?>
    </tr>
    </thead>
    <tbody>
	<?php
	$tablerows = '';

	foreach( $records as $row ){
		$rowcolumns = '';
		foreach( $fieldnames as $field ){
			if( $field['fieldname'] == 'shipments_deliveries_mm_documents_status' ) {
				switch ($row[$field['fieldname']]) {
					case Document_model::$STATUS_DOCUMENT_MISSING:
						$css = 'badge badge-important';
						break;
					case Document_model::$STATUS_DOCUMENT_UPLOADED:
						$css = 'badge badge-warning';
						break;
					case Document_model::$STATUS_DOCUMENT_CONFIRMED:
						$css = 'badge badge-success';
						break;
				}
				$rowcolumns .= '<td><span class="' . $css . '"></span></td>';
			} else {
				$rowcolumns .= '<td>'.$row[$field['fieldname']].'</td>';
			}
		}
		foreach( $recordactions as $action ) {
            if( $action['title'] == 'edit' ) {
				$actionControllerLink = base_url().$action['controller'].'/'.$row['id'];
				$rowcolumns .= '<td class="'.$action['title'].' span1"><a href="'.$actionControllerLink.'" title="'.$action['title'].'" class="btn btn-small"><i class="'.$action['icon'].'"></i></a></td>';
            }
		}
		$tablerows .= '<tr>'.$rowcolumns.'</tr>';
	}
	print( $tablerows );
	?>
    </tbody>
</table>

