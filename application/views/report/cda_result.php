<?= form_open( $action, array('class' => 'search' ) ) ?>

<ul id="pager-top" class="pager">
	<li><a href="#" class="btn btn-small first"><i class="icon-step-backward"></i></a></li>
	<li><a href="#" class="btn btn-small prev"><i class="icon-backward"></i></a></li>
	<li><input type="text" class="pagedisplay span1" /></li>
	<li><a href="#" class="btn btn-small next"><i class="icon-forward"></i></a></li>
	<li><a href="#" class="btn btn-small last"><i class="icon-step-forward"></i></a></li>
	<li>
		<select class="pagesize span1">
			<option value="10">10</option>
			<option selected="selected" value="20">20</option>
			<option value="50">50</option>
			<option  value="100">100</option>
		</select>
	</li>
</ul>

<table class="table table-striped table-bordered table-condensed reportresulttable" style="table-layout:fixed;">
	<thead>
    <tr>
		<th class='{sorter: false}' width="20"><input type="checkbox" value="" name="cda-select-all" id="cda-select-all"></th>
        <th class='{sorter: false}'>HBL / HAWB</th>
        <th class='{sorter: false}'>Container number</th>
        <th class='{sorter: false}'>Contract/PO number</th>
        <th class='{sorter: false}'>On-Sale week</th>
		<th>Date confirmed</th>
		<th>Aldi user printed</th>
		<th class='{sorter: false}'>Telex received</th>
	</tr>
    </thead>
    <tbody>
	<?php
		$tableRows = '';
		foreach( $records as $row ){
			$rowColumns = '<td><input type="checkbox" value="'.$row['shipments_id'].'" name="shipmentid[]"></td>';
			$rowColumns .= '<td style="word-wrap:break-word;">'.$row['shipments_hbl_hawb'].'</td>';
			$rowColumns .= '<td style="word-wrap:break-word;">'.$row['shipments_container_number'].'</td>';
			$rowColumns .= '<td style="word-wrap:break-word;">'.$row['contracts_contract_number'].'</td>';
			$rowColumns .= '<td style="word-wrap:break-word;">'.$row['contracts_advertisement_week'].'</td>';
			$rowColumns .= '<td style="word-wrap:break-word;">'.$row['shipments_approval_date'].'</td>';
			$rowColumns .= '<td style="word-wrap:break-word;">'.$row['shipments_print_date'].'</td>';
			$rowColumns .= '<td style="word-wrap:break-word;">'.( $row['telex_received'] ? '<i class="icon-ok"></i>' : '' ).'</td>';
			$tableRows .= '<tr>'.$rowColumns.'</tr>';
		}
		print( $tableRows );
	?>
    </tbody>
</table>

<ul id="pager-bottom" class="pager">
	<li><a href="#" class="btn btn-small first"><i class="icon-step-backward"></i></a></li>
	<li><a href="#" class="btn btn-small prev"><i class="icon-backward"></i></a></li>
	<li><input type="text" class="pagedisplay span1" /></li>
	<li><a href="#" class="btn btn-small next"><i class="icon-forward"></i></a></li>
	<li><a href="#" class="btn btn-small last"><i class="icon-step-forward"></i></a></li>
	<li>
		<select class="pagesize span1">
			<option value="10">10</option>
			<option selected="selected" value="20">20</option>
			<option value="50">50</option>
			<option  value="100">100</option>
		</select>
	</li>
</ul>


<?= form_close() ?>
<div class="form-actions">
    <a href="#" class="btn btn-primary generate-cda">Generate</a>
</div>

