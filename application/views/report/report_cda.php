<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
<?= $this->load->view('general/header', array(), true); ?>
<h1><?=$cdaHeadline; ?></h1><br/>
<?=$cdaForm; ?>
<div class="container">
    <?=$cdaResult; ?>
</div>

<?= $this->load->view('general/footer', '', true); ?>
<?= $this->load->view('general/javascript', '', true); ?>
<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>