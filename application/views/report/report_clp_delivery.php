<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
<style>
    .search .btn {
        display: none;
    }
</style>

<?= $this->load->view('general/header', array(), true); ?>
<h1><?=$reportHeadline; ?></h1><br/>
<?=$reportForm; ?>
<div class="container">
    <div class="form-actions">
        <a href="#" class="btn btn-primary generate-clp-delivery">Generate</a>
    </div>
</div>

<?= $this->load->view('general/footer', '', true); ?>
<?= $this->load->view('general/javascript', '', true); ?>
<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>