<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
<style>
    .search .btn {
        display: none;
    }
</style>

<?= $this->load->view('general/header', array(), true); ?>
<h1><?=$reportHeadline; ?></h1><br/>
<?=$reportForm; ?>
<div class="container">
    <div class="form-actions">
        <a href="#" class="btn btn-primary generate-crd-vs-atd">Generate</a>
    </div>
</div>
<div class="container">
    <div class="form-actions">
        <a href="/report/report_crd_vs_atd/generateReportAndSendByMail" class="btn btn-primary generate-and-send-crd-vs-atd">Generate report and send by mail</a>
    </div>
</div>

<?= $this->load->view('general/footer', '', true); ?>
<?= $this->load->view('general/javascript', '', true); ?>
<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>