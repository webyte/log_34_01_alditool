<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>

<style>
    h4 {
        color: #333333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    }
    .table-bordered th {
        border-bottom: 1px solid #DDDDDD;
    }
</style>
<?php
$out = '';
foreach( $records as $shipments ) {
	$out .= '<table class="table table-bordered table-condensed">
					<thead>
						<tr>
							<th>POD</th>
							<th>Contract no</th>
							<th>Product code</th>
							<th>DC</th>
							<th>Carton</th>
							<th>Gross weight</th>
							<th>Volume</th>
							<th>Container size</th>
							<th>Container type</th>
						</tr>
					</thead>
					<tbody>';
	$containerInfo = array();
	$destinationPort = '';
	foreach( $shipments as $shipment ) {
		$i = 0;
		$rowspan = count( $shipment['deliveries'] );
		foreach( $shipment['deliveries'] as $delivery ) {
			$out .= '<tr>';
//			if( $i == 0 ) $out .= '<td rowspan="'.$rowspan.'" class="center">'.$shipment['destination_port'].'</td>';
			if( $i == 0 ) $out .= '<td class="center">'.$shipment['destination_port'].'</td>';
			else $out .= '<td class="center" style="border-bottom: 0; border-top: 0">&nbsp;</td>';
			$out .= '<td class="center">'.$delivery['contract_number'].'</td>';
			$out .= '<td class="center">'.$delivery['product_code'].'</td>';
			$out .= '<td class="right">'.$delivery['dc'].'</td>';
			$out .= '<td class="right">'.$delivery['case_quantity'].'</td>';
			$out .= '<td class="right">'.$delivery['gross_weight'].'</td>';
			$out .= '<td class="right">'.$delivery['volume'].'</td>';
//			if( $i == 0 ) $out .= '<td rowspan="'.($rowspan+1).'" class="center">'.$shipment['container_size'].'</td>';
//			if( $i == 0 ) $out .= '<td rowspan="'.($rowspan+1).'" class="center">'.$shipment['container_type'].'</td>';
			if( $i == 0 ) $out .= '<td class="center">'.$shipment['container_size'].'</td>';
			else $out .= '<td class="center" style="border-bottom: 0; border-top: 0">&nbsp;</td>';
			if( $i == 0 ) $out .= '<td class="center">'.$shipment['container_type'].'</td>';
			else $out .= '<td class="center" style="border-bottom: 0; border-top: 0">&nbsp;</td>';
			$out .= '</tr>';
			$i++;
		}
		$out .= '<tr>
							<td><strong>Total</strong></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td class="right"><strong>'.$shipment['case_quantity_total'].'</strong></td>
							<td class="right"><strong>'.$shipment['gross_weight_total'].'</strong></td>
							<td class="right"><strong>'.$shipment['volume_total'].'</strong></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="9">&nbsp;</td>
						</tr>';

		$containerInfo[] = '1 * '.$shipment['container_size']."'".$shipment['container_type'];
		$destinationPort = $shipment['destination_port'];
	}
	$out .= '</tbody></table>';
	$out .= '<p style="text-align: right;">'.implode( ' + ', $containerInfo ).' to '.$destinationPort.'</p><br/><br/>';
}
$out .= '';
print( $out );
?>

<?= $this->load->view('general/javascript', '', true); ?>

</body>

</html>