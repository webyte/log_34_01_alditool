<html>
<head>
	<meta charset="utf-8">
    <style>
        * {
            font-size: 11px;
        }
        table {
            width: 600px;
        }
    </style>
</head>

<body>
	<table>
		<tbody>
			<tr>
				<td>
                    <img width="50" height="60" alt="Aldi" src="http://alditool.logwin-logistics.com/img/aldi.png" id="aldi">
					<h4>Container Load Plan/Delivery Docket/Container Weight Declaration</h4>
					<h4>Delivery To: ALDI Stores<br/><?=$record['shipments_dc']; ?></h4>
				</td>
			</tr>
		</tbody>
	</table>
	<table>
		<tbody>
			<tr>
				<td>ALDI Purchase Order No/Contract No:</td>
				<td><?=$record['contracts_contract_number']; ?></td>
			</tr>
			<tr>
				<td>Creation Date:</td>
				<td><?=$record['shipments_etd']; ?>&nbsp;</td>
			</tr>
			<tr>
				<td>Week:</td>
				<td><?=$record['contracts_advertisement_week']; ?></td>
			</tr>
			<tr>
				<td>Supplier Job No:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Shipping Terms:</td>
				<td><?=$record['shipments_incoterm_code']; ?></td>
			</tr>
			<tr>
				<td>Confirmed Delivery Date/Time/Dock:</td>
				<td></td>
			</tr>
			<tr>
				<td>Vessel Name/Voyage:</td>
				<td><?=$record['shipments_mother_vessel_name']; ?></td>
			</tr>
			<tr>
				<td>Port of Loading/ETD: <?=$record['shipments_departure_port']; ?>/<?=$record['shipments_etd']; ?></td>
				<td>Port of Discharge/ETA: <?=$record['shipments_destination_port']; ?>/<?=$record['shipments_eta']; ?></td>
			</tr>
			<tr>
				<td>Factory or CFS Loading:</td>
				<td><?=$record['contracts_traffic_type']; ?></td>
			</tr>
			<tr>
				<td>Date of Last Free Delivery Day from Port:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Transport Co. Name:</td>
				<td></td>
			</tr>
			<tr>
				<td>Container Type:</td>
				<td><?=$record['shipments_container_size']; ?>'<?=$record['shipments_container_type']; ?></td>
			</tr>
			<tr>
				<td>Container No:</td>
				<td><?=$record['shipments_container_number']; ?></td>
			</tr>
			<tr>
				<td>Seal No:</td>
				<td><?=$record['shipments_seal_number']; ?></td>
			</tr>
			<tr>
				<td>Cargo Gross Weight Kgs:</td>
				<td><?=$record['deliveries_sum_gross_weight']; ?></td>
			</tr>
			<tr>
				<td>Approximate Container Weight:</td>
				<td><?=$record['shipments_container_tare_weight']; ?></td>
			</tr>
			<tr>
				<td>Gross Weight (Incl Container Tare) Kgs:</td>
				<td><?= ($record['deliveries_sum_gross_weight']+$record['shipments_container_tare_weight']); ?></td>
			</tr>
		</tbody>
	</table>
	<br/><br/>
	<table>
		<tbody>
			<tr>
				<td>Start time of De Stuffing:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>End Time of De Stuffing:</td>
				<td></td>
			</tr>
			<tr>
				<td>Container Departure Time:</td>
				<td>&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<br/>
	<h4>Product Information</h4>
	<table class="table table-bordered table-condensed">
		<thead>
			<tr>
				<th>Supplier</th>
				<th>ALDI Product Code</th>
				<th>Contract No.</th>
				<th>Units Per Case</th>
				<th>Product Description</th>
				<th>Total Number of Cases</th>
				<th>CBM</th>
				<th>Case Dimensions LxWxH</th>
				<th>Regions</th>
			</tr>
		</thead>
		<tbody>
		<?php
			foreach( $record['deliveries'] as $delivery ) {
				$out = '
			<tr>
				<td>'.$delivery['contracts_supplier'].'</td>
				<td>'.$delivery['deliveries_product_code'].'</td>
				<td>'.$delivery['deliveries_contract_number'].'</td>
				<td>'.$delivery['contracts_units_per_case'].'</td>
				<td>'.$delivery['deliveries_product_description'].'</td>
				<td>'.$delivery['deliveries_case_quantity'].'</td>
				<td>'.$delivery['deliveries_volume'].'</td>
				<td>'.$delivery['contracts_case_length'].'x'.$delivery['contracts_case_width'].'x'.$delivery['contracts_case_height'].'</td>
				<td>'.$delivery['deliveries_dc'].'</td>
			</tr>';
				print($out);
			}
		?>
		</tbody>
	</table>
	<p>** Photo to be taken if stock is damaged<br/>
	<br/>
	<br/>
	<br/>
	Packing:<?=$record['shipments_loading_type']; ?><br/>
	<br/>
	AQIS:<br/>
	<br/>
	ALDI Receipt Stamp:</p>
</body>

</html>