<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
	<?=$this->load->view('general/header', array( 'loggedIn' => true ), true); ?>
	<h1><?=$headline?></h1><br/>
	<?php
		if(isset($searchform)){
			print( $searchform );
		}
	?>
	<div class="container">
		<?php
		if( $createnewlink != "" ) {
			print ( '<a href="'.$createnewlink.'" class="btn"><i class="icon-plus"></i> New</a><br/><br/>' );
		}
		?>
	</div>
    <div class="container">
        <?= form_open( '/activity/activity/csv_report_export', array('class' => 'search-csv', 'target' => '_blank' ) ); ?>
        <button type="submit" class="btn csv-report-export"><i class="icon-file"></i> Download</button><br>
        <?= form_close(); ?>
    </div>
	<div class="container">
		<?=$resulttable?>
	</div>

	<div class="modal hide" id="deleteWarning">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3>Really delete this record?</h3>
		</div>
		<div class="modal-body">
			<p>Are you sure, you want to delete this record. This action can not be undone!</p>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn">Cancel</a>
			<a class="btn btn-danger delete"><i class="icon-trash icon-white"></i> Delete</a>
		</div>
	</div>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>
</body>
</html>