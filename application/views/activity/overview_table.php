<?php
foreach( $records as $key=>$val ) {
?>
	<table class="table table-striped table-bordered table-condensed">
		<thead>
		<tr>
			<th class='{sorter: false}' width="20%"><?= $val[0]['record_type_label']; ?> / #<?= $val[0]['contract_number']; ?><?= $val[0]['container_number']; ?></th>
			<th class='{sorter: false}' width="20%">Old value</th>
			<th class='{sorter: false}' width="20%">New value</th>
			<th class='{sorter: false}' width="20%">Action type</th>
			<th class='{sorter: false}' width="20%">Username</th>
			<th class='{sorter: false}' width="20%">Date</th>
		</tr>
		</thead>
		<tbody>
<?php
	foreach( $val as $row ) {
?>
			<tr>
				<td><?= $row['record_field_label']; ?></td>
				<td><?= $row['record_data_old']; ?></td>
				<td><?= $row['record_data_new']; ?></td>
				<td><?= $row['action_type']; ?></td>
				<td><?= $row['username']; ?></td>
				<td><?= $row['crdate']; ?></td>
			</tr>
<?php
	}
?>
		</tbody>
	</table>
<?php
}
?>
