<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>

	<h1>Suppliers</h1><br/>
	<?= $form['form.start']; ?>
		<?= $form['input.suppliers.id']; ?>
		<legend>Edit supplier data</legend>
		<div class="row">
			<div class="span3">
				<label><?= $form['label.suppliers.name']; ?></label>
				<?= $form['input.suppliers.name']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.suppliers.street']; ?></label>
				<?= $form['input.suppliers.street']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.suppliers.city']; ?></label>
				<?= $form['input.suppliers.city']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.suppliers.contact_phone']; ?></label>
				<?= $form['input.suppliers.contact_phone']; ?>
			</div>
            <div class="span3">
                <label><?= $form['label.suppliers.code']; ?></label>
				<?= $form['input.suppliers.code']; ?>
            </div>
		</div>

		<h4>Contact persons</h4>

        <?php
        if($newContactPersonPerms){
	        print( '<a href="#" class="btn add-contactperson"><i class="icon-plus"></i> New</a>' );
        }
        ?>
        <div class="contactperson-records">
        	<?= $form['input.suppliers.contactpersons']; ?>
        </div>

		<div class="form-actions">
			<?= $form['form.submit']; ?>
			<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?=$this->config->item('base_url');?>manage/supplier';"/>
		</div>
	</form>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
    <?=!empty($additionalJS) ? $additionalJS : ''; ?>
</body>

</html>