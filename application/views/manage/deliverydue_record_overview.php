<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
	<?=$this->load->view('general/header', array( 'loggedIn' => true ), true); ?>
	<h1><?=$headline?></h1><br/>
	<?php
	if( isset( $error ) ) {
		$css = ( $error ) ? 'error' : 'success';
		print('<div class="alert alert-'.$css.'"><button class="close" data-dismiss="alert" type="button">×</button>'.$message.'</div>');
	}
	?>
	<?=form_open( 'manage/delivery_due/do_upload', array( 'class' => 'delivery-due-upload', 'enctype' => 'multipart/form-data' ) ); ?>
    <div class="row">
        <div class="span5">
            <input id="delivery-due-upload-file" type="file" style="visibility:hidden;" name="delivery-due-upload-file">
            <label>Upload data</label>
			<?php
			if( $this->agent->is_browser('MSIE') ) {
				?>
                <input id="delivery-due-upload-file" type="file" name="delivery-due-upload-file">
				<?php
			} else {
				?>
                <div class="input-append">
                    <input id="delivery-due-upload-holder" class="input-large" type="text">
                    <a class="btn" onclick="$('input[id=delivery-due-upload-file]').click();"><i class="icon-file"></i> Browse</a>
                </div>
				<?php
			}
			?>
        </div>
    </div>
    <div class="row">
        <div class="span3">
            <label>Sales year</label>
			<?= form_dropdown( 'sales-year', $optionsSalesYear, date('Y') ); ?>
        </div>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn btn-primary">Upload</button>
        <input type="reset" class="btn" value="Reset">
    </div>
	<?=form_close(); ?>

    <br><br>

	<div class="container">
		<?=$resulttable?>
	</div>

	<?php if( !empty( $resulttable )) { ?>
	<ul id="pager-bottom" class="pager">
		<li><a href="#" class="btn btn-small first"><i class="icon-step-backward"></i></a></li>
        <li><a href="#" class="btn btn-small prev"><i class="icon-backward"></i></a></li>
        <li><input type="text" class="pagedisplay span1" /></li>
        <li><a href="#" class="btn btn-small next"><i class="icon-forward"></i></a></li>
        <li><a href="#" class="btn btn-small last"><i class="icon-step-forward"></i></a></li>
        <li>
			<select class="pagesize span1">
				<option value="10">10</option>
				<option selected="selected" value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
				<option value="100000">All</option>
			</select>
        </li>
	</ul>
	<?php } ?>

    <div class="modal hide" id="deleteWarning">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">×</a>
            <h3>Really delete this record?</h3>
        </div>
        <div class="modal-body">
            <p>Are you sure, you want to delete this record. This action can not be undone!</p>
        </div>
        <div class="modal-footer">
            <a data-dismiss="modal" class="btn">Cancel</a>
            <a class="btn btn-danger delete"><i class="icon-trash icon-white"></i> Delete</a>
        </div>
    </div>

    <?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>
</body>
</html>