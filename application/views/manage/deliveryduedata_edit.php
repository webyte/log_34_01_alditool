<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>

<h1>ALDI AU WH delivery due date</h1><br/>
<?= $form['form.start']; ?>
<?= $form['input.delivery_dues.id']; ?>
<legend>Edit ALDI AU WH delivery due date data</legend>
<?php
if( !empty($infotext) ) print( '<div class="alert alert-danger">'.$infotext.'</div>' );
?>
<div class="row">
    <div class="span3">
        <label><?= $form['label.delivery_dues.sales_week']; ?></label>
		<?= $form['input.delivery_dues.sales_week']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.delivery_dues.sales_year']; ?></label>
		<?= $form['input.delivery_dues.sales_year']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.delivery_dues.delivery_due_date']; ?></label>
		<?= $form['input.delivery_dues.delivery_due_date']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.delivery_dues.delivery_due_end_date']; ?></label>
		<?= $form['input.delivery_dues.delivery_due_end_date']; ?>
    </div>
</div>

<div class="form-actions">
	<?= $form['form.submit']; ?>
    <input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?=$this->config->item('base_url');?>manage/delivery_due';"/>
</div>
</form>

<?=$this->load->view('general/footer', '', true); ?>
<?=$this->load->view('general/javascript', '', true); ?>

</body>

</html>