<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>

<h1>Detention free days</h1><br/>
<?= $form['form.start']; ?>
<?= $form['input.detention_free_days.id']; ?>
<legend>Edit detention free days data </legend>
<?php
if( !empty($infotext) ) print( '<div class="alert alert-danger">'.$infotext.'</div>' );
?>
<div class="row">
    <div class="span3">
        <label><?= $form['label.detention_free_days.contract_number']; ?></label>
		<?= $form['input.detention_free_days.contract_number']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.detention_free_days.container_type']; ?></label>
		<?= $form['input.detention_free_days.container_type']; ?>
    </div>
    <div class="span3">
        <label><?= $form['label.detention_free_days.free_days']; ?></label>
		<?= $form['input.detention_free_days.free_days']; ?>
    </div>
</div>

<div class="form-actions">
	<?= $form['form.submit']; ?>
    <input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?=$this->config->item('base_url');?>manage/detention_free_day';"/>
</div>
</form>

<?=$this->load->view('general/footer', '', true); ?>
<?=$this->load->view('general/javascript', '', true); ?>

</body>

</html>