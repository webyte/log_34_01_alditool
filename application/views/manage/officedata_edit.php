<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>

	<h1>Logwin origin offices</h1><br/>
	<?= $form['form.start']; ?>
		<?= $form['input.offices.id']; ?>
		<legend>Edit office data</legend>
		<div class="row">
			<div class="span3">
				<label><?= $form['label.offices.name']; ?></label>
				<?= $form['input.offices.name']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.offices.street']; ?></label>
				<?= $form['input.offices.street']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.offices.city']; ?></label>
				<?= $form['input.offices.city']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.offices.contact_name']; ?></label>
				<?= $form['input.offices.contact_name']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.offices.contact_mail']; ?></label>
				<?= $form['input.offices.contact_mail']; ?>
			</div>
		</div>
		<div class="row">
			<div class="span3">
				<label><?= $form['label.offices.contact_phone']; ?></label>
				<?= $form['input.offices.contact_phone']; ?>
			</div>
            <div class="span3">
                <label><?= $form['label.offices.telex_mail']; ?></label>
				<?= $form['input.offices.telex_mail']; ?>
            </div>
            <div class="span3">
                <label><?= $form['label.offices.ports']; ?></label>
				<?= $form['input.offices.ports']; ?>
            </div>
		</div>

		<div class="form-actions">
			<?= $form['form.submit']; ?>
			<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?=$this->config->item('base_url');?>manage/office';"/>
		</div>
	</form>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>

</body>

</html>