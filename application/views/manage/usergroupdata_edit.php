<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>
	<h1>Usergroup</h1><br/>
	<?= $form['form.start']; ?>
		<?= $form['input.user_groups.id']; ?>
		<legend>Edit usergroup data</legend>

		<div class="row">
			<div class="span3">
				<label><?= $form['label.user_groups.title']; ?></label>
				<?= $form['input.user_groups.title']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.user_groups.hidden']; ?></label>
				<?= $form['input.user_groups.hidden']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.user_groups.login_redirect']; ?></label>
				<?= $form['input.user_groups.login_redirect']; ?>
			</div>
		</div>
		<br/><br/>
		<legend>Permissions</legend>
		<div class="row">
			<div class="span6">
				<h5>Modules</h5>
				<table class="table table-striped table-condensed span5">
					<thead>
					<tr>
						<th>Module</th>
						<th>allowed</th>
					</tr>
					</thead>
					<tbody>
					<?
					foreach ( $module_permissions as $mod_perm ) {
						$tmpRow = '<tr>';
						$tmpRow .= '<td>'.$mod_perm['label'].'</td>';
						$tmpRow .= '<td class="span1">'.$mod_perm['allowed'].'</td>';
						$tmpRow .= '</tr>';
						print( $tmpRow );
					}
					?>
					</tbody>
				</table>
				<br/><br/><br/>
				<h5>Module Actions</h5>
				<table class="table table-striped table-condensed span5">
                    <thead>
					<tr>
						<th>Action</th>
						<th>allowed</th>
					</tr>
					</thead>
					<tbody>
					<?
					foreach ( $action_permissions as $controllername => $controller_perms ) {
						print( '<tr><td colspan="2"><strong><br/>Controller: '.$controllername.'</strong></td></tr>' );
						foreach ( $controller_perms as $action ) {
							$tmpRow = '<tr>';
							$tmpRow .= '<td>'.$action['label'].'</td>';
							$tmpRow .= '<td class="span1">'.$action['allowed'].'</td>';
							$tmpRow .= '</tr>';
							print( $tmpRow );
						}
					}
					?>
					</tbody>
				</table>

				<br/><br/><br/>
				<h5>Table CRUD functions</h5>
				<table class="table table-striped table-condensed span5">
					<thead>
					<tr>
						<th>Action</th>
						<th>allowed</th>
					</tr>
					</thead>
					<tbody>
					<?
					foreach ( $tables_crud_permissions as $tablename => $table_perms ) {
						print( "<tr><td colspan='2'><strong><br/>Table: $tablename</strong></td></tr>" );
						foreach ( $table_perms as $action ) {
							$tmpRow = '<tr>';
							$tmpRow .= '<td>'.$action['label'].'</td>';
							$tmpRow .= '<td class="span1">'.$action['allowed'].'</td>';
							$tmpRow .= '</tr>';
							print( $tmpRow );
						}
					}
					?>
					</tbody>
				</table>

			</div>
			<div class="span6">
				<h5>Fields</h5>
				<table class="table table-striped table-condensed span5">
					<thead>
					<tr style="border-bottom:1px solid #eee;">
						<th>Field</th>
						<th>editable</th>
						<th>disabled (readonly)</th>
						<th>hidden</th>
					</tr>
					</thead>
					<tbody>
					<?
					foreach ( $field_permissions as $tablename => $table_perms ) {
						print( "<tr><td colspan='4'><strong><br/>Table: $tablename</strong></td></tr>" );
						foreach ( $table_perms as $perm ) {
							$tmpRow = '<tr>';
							$tmpRow .= '<td>'.$perm['label'].'</td>';
							$tmpRow .= '<td class="span2">'.$perm['edit'].'</td>';
							$tmpRow .= '<td class="span2">'.$perm['disabled'].'</td>';
							$tmpRow .= '<td>'.$perm['hidden'].'</td>';
							$tmpRow .= '</tr>';
							print( $tmpRow );
						}
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="form-actions">
			<?= $form['form.submit']; ?>
			<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?=$this->config->item('base_url');?>manage/usergroup';"/>
		</div>
	</form>
<?=$this->load->view('general/footer', '', true); ?>
<?=$this->load->view('general/javascript', '', true); ?>

</body>

</html>