<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>

	<h1>Users</h1><br/>
	<?= $form['form.start']; ?>
		<?= $form['input.users.id']; ?>
		<legend>Edit user data</legend>
		<div class="row">
            <div class="span3">
				<label><?= $form['label.users.username']; ?></label>
				<?= $form['input.users.username']; ?>
            </div>
            <div class="span3">
                <label><?= $form['label.users.password']; ?></label>
				<?= $form['input.users.password']; ?>
            </div>
            <div class="span3">
				<label><?= $form['label.users.usergroup']; ?></label>
				<?= $form['input.users.usergroup']; ?>
            </div>
            <div class="span3">
				<label><?= $form['label.users.hidden']; ?></label>
				<?= $form['input.users.hidden']; ?>
            </div>
		</div>

		<div class="row">
			<div class="span3">
                <label><?= $form['label.users.firstname']; ?></label>
				<?= $form['input.users.firstname']; ?>
            </div>
            <div class="span3">
				<label><?= $form['label.users.lastname']; ?></label>
				<?= $form['input.users.lastname']; ?>
            </div>
            <div class="span3">
				<label><?= $form['label.users.email']; ?></label>
				<?= $form['input.users.email']; ?>
			</div>
            <div class="span3">
                <label><?= $form['label.users.logwin_origin_office']; ?></label>
				<?= $form['input.users.logwin_origin_office']; ?>
            </div>
            <div class="span3">
                <label><?= $form['label.users.supplier']; ?></label>
				<?= $form['input.users.supplier']; ?>
            </div>
		</div>

		<div class="form-actions">
			<?= $form['form.submit']; ?>
			<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?=$this->config->item('base_url');?>manage/user';"/>
		</div>
	</form>
<?=$this->load->view('general/footer', '', true); ?>
<?=$this->load->view('general/javascript', '', true); ?>

</body>

</html>