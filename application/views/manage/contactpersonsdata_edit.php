<div class="contactperson">
    <hr/>
    <?php
    if($deletePerms){
        print( '<a href="#" class="pull-right delete-contactperson" rel="'.$form['value.contactpersons.id'].'"><i class="icon-remove"></i></a><br/>' );
    }
    ?>
    <?= $form['input.contactpersons.id']; ?>
    <div class="row">
        <div class="span3">
            <label><?= $form['label.contactpersons.name']; ?></label>
            <?= $form['input.contactpersons.name']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contactpersons.email']; ?></label>
            <?= $form['input.contactpersons.email']; ?>
        </div>
        <div class="span3">
            <label><?= $form['label.contactpersons.default_contact']; ?></label>
            <?= $form['input.contactpersons.default_contact']; ?>
        </div>
    </div>
</div>
