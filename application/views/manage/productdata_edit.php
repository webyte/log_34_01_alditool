<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>

<h1>Products</h1><br/>
<?= $form['form.start']; ?>
<?= $form['input.products.id']; ?>
<legend>Edit product data</legend>
<div class="row">
	<div class="span3">
		<label><?= $form['label.products.article_number']; ?></label>
		<?= $form['input.products.article_number']; ?>
	</div>
	<div class="span3">
		<label><?= $form['label.products.article_description']; ?></label>
		<?= $form['input.products.article_description']; ?>
	</div>
</div>

<div class="form-actions">
	<?= $form['form.submit']; ?>
	<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?=$this->config->item('base_url');?>manage/product';"/>
</div>
</form>
<?=$this->load->view('general/footer', '', true); ?>
<?=$this->load->view('general/javascript', '', true); ?>

</body>

</html>