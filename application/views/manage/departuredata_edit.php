<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>

	<h1>Departures</h1><br/>
	<?= $form['form.start']; ?>
		<?= $form['input.departures.id']; ?>
		<legend>Edit departure data</legend>
		<div class="row">
			<div class="span3">
				<label><?= $form['label.departures.name']; ?></label>
				<?= $form['input.departures.name']; ?>
			</div>
             <div class="span3">
                <label><?= $form['label.departures.port_name']; ?></label>
				<?= $form['input.departures.port_name']; ?>
            </div>
            <div class="span3">
                <label><?= $form['label.departures.port_country']; ?></label>
				<?= $form['input.departures.port_country']; ?>
            </div>
            <div class="span3">
                <label><?= $form['label.departures.cut_off_time']; ?></label>
				<?= $form['input.departures.cut_off_time']; ?>
            </div>
		</div>

		<div class="form-actions">
			<?= $form['form.submit']; ?>
			<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?=$this->config->item('base_url');?>manage/office';"/>
		</div>
	</form>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>

</body>

</html>