<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
<?=$this->load->view('general/browser_warning', '', true); ?>
<?=$this->load->view('general/header', array( 'loggedIn' => false ), true); ?>

	<h1>Aldi special week PIC</h1><br/>
	<?= $form['form.start']; ?>
	<?= $form['input.pics.id']; ?>
	<?= $form['input.pics.username']; ?>
	<legend>Edit PIC data</legend>
	<div class="row">
		<div class="span3">
			<label><?= $form['label.pics.user_id']; ?></label>
			<?= $form['input.pics.user_id']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.pics.email']; ?></label>
			<?= $form['input.pics.email']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.pics.on_sale_week']; ?></label>
			<?= $form['input.pics.on_sale_week']; ?>
		</div>
	</div>

	<div class="form-actions">
		<?= $form['form.submit']; ?>
		<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?=$this->config->item('base_url');?>manage/pic';"/>
	</div>
	</form>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>