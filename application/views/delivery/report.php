<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>

<style>
    h4 {
        color: #333333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    }
    .table-bordered th {
        border-bottom: 1px solid #DDDDDD;
    }
</style>
<?php
$out = '';
foreach( $records as $key=>$value ) {
	$out .= '<table class="table table-bordered table-condensed">
					<thead>
						<tr>
							<th>POD</th>
							<th>Contract no</th>
							<th>Product code</th>
							<th>DC</th>
							<th>Carton</th>
							<th>Gross weight</th>
							<th>Volume</th>
							<th>Container size</th>
							<th>Container type</th>
						</tr>
					</thead>
					<tbody>';
//	$containerInfo = array();
//	$destinationPort = '';
	$i = 0;
	$rowspan = count( $value['deliveries'] );

	foreach( $value['deliveries'] as $delivery ) {
		$out .= '<tr>';

		if( $i == 0 ) $out .= '<td class="center">'.$key.'</td>';
		else $out .= '<td class="center" style="border-bottom: 0; border-top: 0">&nbsp;</td>';

		$out .= '<td class="center">'.$delivery['deliveries_contract_number'].'</td>';
		$out .= '<td class="center">'.$delivery['deliveries_product_code'].'</td>';
		$out .= '<td class="right">'.$delivery['deliveries_dc'].'</td>';
		$out .= '<td class="right">'.$delivery['deliveries_unit_quantity'].'</td>';
		$out .= '<td class="right">'.$delivery['deliveries_gross_weight'].'</td>';
		$out .= '<td class="right">'.$delivery['deliveries_volume'].'</td>';

		if( $i == 0 ) $out .= '<td class="center">&nbsp;</td>';
		else $out .= '<td class="center" style="border-bottom: 0; border-top: 0">&nbsp;</td>';

		if( $i == 0 ) $out .= '<td class="center">&nbsp;</td>';
		else $out .= '<td class="center" style="border-bottom: 0; border-top: 0">&nbsp;</td>';

		$out .= '</tr>';

		$i++;
	}
	$out .= '<tr>
							<td><strong>Total</strong></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td class="right"><strong>'.$value['unit_quantity_total'].'</strong></td>
							<td class="right"><strong>'.$value['gross_weight_total'].'</strong></td>
							<td class="right"><strong>'.$value['volume_total'].'</strong></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>';
	$out .= '</tbody></table>';
}
$out .= '';
print( $out );
?>

<?= $this->load->view('general/javascript', '', true); ?>

</body>

</html>