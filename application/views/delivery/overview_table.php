<table class="table table-striped table-bordered table-condensed resulttable">
    <thead>
    <tr>
		<?php
		$out = '<th class="{sorter: false}"><input type="checkbox" value="" name="delivery-select-all" id="delivery-select-all"></th>';
		$fieldheaders = '';
		foreach( $fieldnames as $field ) {
			$additionalAttriputes = '';
			if( $field['type'] == 'date' ) $additionalAttriputes = 'class="{sorter: \'germandate\'}"';
			$fieldheaders .= '<th '.$additionalAttriputes.'>'.$field['label'].'</th>';
		}
		$out .= $fieldheaders;

		for ($index = 0; $index < count($recordactions); $index++) {
			$out .= '<th>&nbsp;</th>';
		}
		print($out);
		?>
    </tr>
    </thead>
    <tbody>
	<?php
	$tablerows = '';
	foreach( $records as $row ){
		$rowcolumns = '<td><input type="checkbox" value="'.$row['id'].'_'.$row['destination'].'" name="deliveryid[]" id="delivery-checkbox-'.$row['id'].'" class="delivery-checkbox"></td>';
		foreach( $fieldnames as $field ){
			$rowcolumns .= '<td>'.$row[$field['fieldname']].'</td>';
		}
		foreach( $recordactions as $action ) {
			$actionControllerLink = base_url().$action['controller'].'/'.$row['id'];
			$rowcolumns .= '<td class="'.$action['title'].' span1"><a href="'.$actionControllerLink.'" title="'.$action['title'].'" class="btn btn-small"><i class="'.$action['icon'].'"></i></a></td>';
		}
		$tablerows .= '<tr>'.$rowcolumns.'</tr>';
	}
	print( $tablerows );
	?>
    </tbody>
</table>

