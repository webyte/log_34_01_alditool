<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
	<?= $this->load->view('general/header', array(), true); ?>
	<h1><?=$headline?></h1><br/>

	<?= $form['form.start']; ?>
		<?= $form['input.deliveries.id']; ?>
		<fieldset>
			<legend>Edit deliveries data</legend>
		</fieldset>

		<h4>Header</h4>
		<div class="row">
			<div class="span3">
				<label><?= $form['label.deliveries.contract_id']; ?></label>
				<?= $form['input.deliveries.contract_id']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.deliveries.shipment_id']; ?></label>
				<?= $form['input.deliveries.shipment_id']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.deliveries.dc']; ?></label>
				<?= $form['input.deliveries.dc']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.deliveries.destination']; ?></label>
				<?= $form['input.deliveries.destination']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.deliveries.case_quantity']; ?></label>
				<?= $form['input.deliveries.case_quantity']; ?>
			</div>
		</div>
		<div class="row">
			<div class="span3">
				<label><?= $form['label.deliveries.unit_quantity']; ?></label>
				<?= $form['input.deliveries.unit_quantity']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.deliveries.eta']; ?></label>
				<?= $form['input.deliveries.eta']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.deliveries.edl']; ?></label>
				<?= $form['input.deliveries.edl']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.deliveries.adl']; ?></label>
				<?= $form['input.deliveries.adl']; ?>
			</div>
			<div class="span3">
				<label><?= $form['label.deliveries.hbl_hawb']; ?></label>
				<?= $form['input.deliveries.hbl_hawb']; ?>
			</div>
		</div>

		<div class="form-actions">
			<?= $form['form.submit']; ?>
			<input type="reset" class="btn" value="Cancel" onClick="window.location.href='<?= $this->config->item('base_url');?>delivery/delivery';"/>
    	</div>
    </form>
    

	<?= $this->load->view('general/footer', '', true); ?>
	<?= $this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>