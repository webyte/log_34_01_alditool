<?=$this->load->view('general/doctype_html', '', true); ?>
<?=$this->load->view('general/html_head', '', true); ?>

<body>
	<?=$this->load->view('general/header', array( 'loggedIn' => true ), true); ?>
	<h1><?=$headline?></h1><br/>
	<?php
		if(isset($searchform)){
			print( $searchform );
		}
	?>
	<div class="container">
		<br/>
	</div>

	<ul id="pager-top" class="pager">
		<li><a href="#" class="btn btn-small first"><i class="icon-step-backward"></i></a></li>
		<li><a href="#" class="btn btn-small prev"><i class="icon-backward"></i></a></li>
		<li><input type="text" class="pagedisplay span1" /></li>
		<li><a href="#" class="btn btn-small next"><i class="icon-forward"></i></a></li>
		<li><a href="#" class="btn btn-small last"><i class="icon-step-forward"></i></a></li>
		<li>
			<select class="pagesize span1">
				<option value="10">10</option>
				<option selected="selected" value="20">20</option>
				<option value="50">50</option>
				<option  value="100">100</option>
			</select>
		</li>
	</ul>
	
	<div class="container">
		<?=$resulttable?>

		<?=form_open( $reportlink, array( 'class' => 'report-delivery', 'target' => '_blank' ) ); ?>
			<input name="report-delivery-ids" value="" type="hidden" id="report-delivery-ids" />
			<button type="submit" class="btn report-delivery-button"><i class="icon-file"></i> Generate report</button>
		<?=form_close(); ?>

		<?=form_open( $consolidatelink, array( 'class' => 'consolidate-delivery' ) ); ?>
        <input name="consolidate-delivery-ids" value="" type="hidden" id="consolidate-delivery-ids" />
		<?php
		if( $consolidatelink != "" ) {
			print ( '<button type="submit" class="btn consolidate-delivery-button"><i class="icon-briefcase"></i> Consolidate</button><div class="message"><br/></div>' );
		}
		?>
		<?=form_close(); ?>
	</div>

	<ul id="pager-bottom" class="pager">
		<li><a href="#" class="btn btn-small first"><i class="icon-step-backward"></i></a></li>
        <li><a href="#" class="btn btn-small prev"><i class="icon-backward"></i></a></li>
        <li><input type="text" class="pagedisplay span1" /></li>
        <li><a href="#" class="btn btn-small next"><i class="icon-forward"></i></a></li>
        <li><a href="#" class="btn btn-small last"><i class="icon-step-forward"></i></a></li>
        <li>
			<select class="pagesize span1">
				<option value="10">10</option>
				<option selected="selected" value="20">20</option>
				<option value="50">50</option>
				<option  value="100">100</option>
			</select>
        </li>
	</ul>

	<div class="modal hide" id="deleteWarning">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3>Really delete this record?</h3>
		</div>
		<div class="modal-body">
			<p>Are you sure, you want to delete this record. This action can not be undone!</p>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn">Cancel</a>
			<a class="btn btn-danger delete"><i class="icon-trash icon-white"></i> Delete</a>
		</div>
	</div>

	<div class="modal hide" id="consolidateDifferentDCs">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3>Really consolidate these records?</h3>
		</div>
		<div class="modal-body">
			<p>Do you really want to add deliveries to [DC] into one CLP?</p>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn btn-danger"><i class="icon-remove icon-white"></i> No</a>
			<a class="btn btn-success submit"><i class="icon-ok icon-white"></i> Yes</a>
		</div>
	</div>

	<?=$this->load->view('general/footer', '', true); ?>
	<?=$this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>
</body>
</html>