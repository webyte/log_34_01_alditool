<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
	<?= $this->load->view('general/header', array(), true); ?>
	<h1><?=$headline?></h1><br/>

	<?= $form['form.start']; ?>
		<?= $form['input.shipments.id']; ?>
		<fieldset>
			<legend>Edit CLP data</legend>
		</fieldset>

		<ul class="nav nav-tabs" id="shipment-tab">
			<li class="active"><a href="#export">Export</a></li>
			<li><a href="#import">Import</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="export">
				<h4>Header data</h4>
				<div class="row">
					<div class="span3">
						<label><?= $form['label.shipments.traffic_type']; ?></label>
						<?= $form['input.shipments.traffic_type']; ?>
					</div>
					<div class="span3">
						<?= $form['label.shipments.hbl_hawb']; ?>
						<table class="table table-bordered table-hover table-condensed hblhawb-table">
							<tbody>

							</tbody>
						</table>
					</div>
					<div class="span3">
						<label><?= $form['label.shipments.carrier']; ?></label>
						<?= $form['input.shipments.carrier']; ?>
					</div>
					<div class="span3">
						<label><?= $form['label.shipments.incoterm_code']; ?></label>
						<?php if( $form['value.shipments.approval'] === '0' || $form['value.shipments.approval'] === '2' ) { ?>
						<?= $form['input.shipments.incoterm_code']; ?>
						<?php } else {
							$tcaNode 		= $this->tca->getColumnNodeById( 'shipments_incoterm_code' );
							$options 		= $this->tca->getMultiselectionOptions( $tcaNode );
							$incotermCode 	= $form['value.shipments.incoterm_code'];
							$out 			= ( $incotermCode != 0 && array_key_exists($incotermCode, $options) ) ? $options[$incotermCode] : '';
							print($out);
						} ?>
					</div>
				</div>
				<div class="row">
					<div class="span3">
						<label><?= $form['label.shipments.departure_port']; ?></label>
						<?= $form['input.shipments.departure_port']; ?>
					</div>
					<div class="span3">
						<label><?= $form['label.shipments.destination_port']; ?></label>
						<?= $form['input.shipments.destination_port']; ?>
					</div>
					<div class="span3">
						<label><?= $form['label.shipments.feeder_vessel_name']; ?></label>
						<?= $form['input.shipments.feeder_vessel_name']; ?>
					</div>
					<div class="span3">
						<label><?= $form['label.shipments.mother_vessel_name']; ?></label>
						<?= $form['input.shipments.mother_vessel_name']; ?>
					</div>
					<div class="span3">
						<label><?= $form['label.shipments.voyage_number']; ?></label>
						<?= $form['input.shipments.voyage_number']; ?>
					</div>
				</div>
				<div class="row">
					<div class="span3">
						<label><?= $form['label.shipments.flight1']; ?></label>
						<?= $form['input.shipments.flight1']; ?>
					</div>
					<div class="span3">
						<label><?= $form['label.shipments.flight2']; ?></label>
						<?= $form['input.shipments.flight2']; ?>
					</div>
					<div class="span3">
						<label><?= $form['label.shipments.flight3']; ?></label>
						<?= $form['input.shipments.flight3']; ?>
					</div>
                    <div class="span3">
                        <label><?= $form['label.shipments.dc']; ?></label>
						<?= $form['input.shipments.dc']; ?>
                    </div>
					<div class="span3">
						<label><?= $form['label.shipments.contents_secured']; ?></label>
						<?= $form['input.shipments.contents_secured']; ?>
					</div>
				</div>
				<div class="row">
                    <div class="span3">
                        <label><?= $form['label.shipments.edo_sent']; ?></label>
						<?= $form['input.shipments.edo_sent']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.vessel_2']; ?></label>
						<?= $form['input.shipments.vessel_2']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.voyage_2']; ?></label>
						<?= $form['input.shipments.voyage_2']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.delay_reason_group']; ?></label>
						<?= $form['input.shipments.delay_reason_group']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.delay_reason']; ?></label>
						<?= $form['input.shipments.delay_reason']; ?>
                    </div>
				</div>
                <div class="row">
                    <div class="span3">
                        <label><?= $form['label.shipments.contract_number']; ?></label>
						<?= $form['input.shipments.contract_number']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.contract_type']; ?></label>
						<?= $form['input.shipments.contract_type']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.online_rate']; ?></label>
						<?= $form['input.shipments.online_rate']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.carrier_booking_number']; ?></label>
						<?= $form['input.shipments.carrier_booking_number']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.shipment_number']; ?></label>
						<?= $form['input.shipments.shipment_number']; ?>
                    </div>
                </div>
                <h4>Event data</h4>
                <div class="row">
                    <div class="span3">
                        <label><?= $form['label.shipments.so_released_date']; ?></label>
						<?= $form['input.shipments.so_released_date']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.so_received']; ?></label>
						<?= $form['input.shipments.so_received']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.container_stuffed']; ?></label>
						<?= $form['input.shipments.container_stuffed']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.container_loaded']; ?></label>
						<?= $form['input.shipments.container_loaded']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.container_unloaded']; ?></label>
						<?= $form['input.shipments.container_unloaded']; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="span3">
                        <label><?= $form['label.shipments.export_customs_cleared']; ?></label>
						<?= $form['input.shipments.export_customs_cleared']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.container_gate_in']; ?></label>
						<?= $form['input.shipments.container_gate_in']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.cy_cut_off']; ?></label>
						<?= $form['input.shipments.cy_cut_off']; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="span3">
                        <label><?= $form['label.shipments.etd']; ?></label>
						<?= $form['input.shipments.etd']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.atd']; ?></label>
						<?= $form['input.shipments.atd']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.eta']; ?></label>
						<?= $form['input.shipments.eta']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.ata']; ?></label>
						<?= $form['input.shipments.ata']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.cus']; ?></label>
						<?= $form['input.shipments.cus']; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="span3">
                        <label><?= $form['label.shipments.pick_up_at_wharf']; ?></label>
						<?= $form['input.shipments.pick_up_at_wharf']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.delivered_to_dc']; ?></label>
						<?= $form['input.shipments.delivered_to_dc']; ?>
                    </div>
                </div>
				<h4>Container data</h4>
				<div class="row">
					<div class="span3">
						<label><?= $form['label.shipments.edor']; ?></label>
						<?= $form['input.shipments.edor']; ?>
					</div>
                    <div class="span3">
                        <label><?= $form['label.shipments.container_type']; ?></label>
						<?= $form['input.shipments.container_type']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.container_size']; ?></label>
						<?= $form['input.shipments.container_size']; ?>
                    </div>
					<div class="span3">
						<label><?= $form['label.shipments.container_number']; ?></label>
						<?= $form['input.shipments.container_number']; ?>
					</div>
                    <div class="span3">
                        <label><?= $form['label.shipments.container_tare_weight']; ?></label>
						<?= $form['input.shipments.container_tare_weight']; ?>
                    </div>
				</div>
                <div class="row">
					<div class="span3">
						<label><?= $form['label.shipments.seal_number']; ?></label>
						<?= $form['input.shipments.seal_number']; ?>
					</div>
					<div class="span3">
                        <label><?= $form['label.shipments.loading_type']; ?></label>
						<?= $form['input.shipments.loading_type']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.temperature']; ?></label>
						<?= $form['input.shipments.temperature']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.dg']; ?></label>
						<?= $form['input.shipments.dg']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.dg_class']; ?></label>
						<?= $form['input.shipments.dg_class']; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="span3">
                        <label><?= $form['label.shipments.last_transmission']; ?></label>
						<?= $form['input.shipments.last_transmission']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.num_of_transmission']; ?></label>
						<?= $form['input.shipments.num_of_transmission']; ?>
                    </div>
                    <div class="span3">
                        <label><?= $form['label.shipments.detention_free_days']; ?></label>
						<?= $form['input.shipments.detention_free_days']; ?>
                    </div>
                </div>
                <h4>DC Deliveries</h4>
				<?php if( $addDeliveryPerms && ( $form['value.shipments.approval'] === '0' || $form['value.shipments.approval'] === '2' )) { ?>
                <div class="input-append">
                    <input class="span3 add-contract-number contract-number-typeahead" type="text"><a href="#" class="btn add-contract" type="button"><i class="icon-plus"></i> Add Contract/PO number</a>
                </div>
				<div class="delivery-holder">
				</div>
				<?php } ?>
                <div class="delivery-records">
					<?= $form['input.shipments.deliveries']; ?>
                </div>
            </div>
            <div class="tab-pane" id="import">
				<div class="row">
					<div class="span3">
                        <label><?= $form['label.shipments.approval']; ?></label>
                        <?= $form['input.shipments.approval']; ?>
					</div>
					<div class="span3">
						<label><?= $form['label.shipments.import_date']; ?></label>
						<?= $form['input.shipments.import_date']; ?>
					</div>
				</div>
                <div class="row">
                    <div class="span12">
                        <label><?= $form['label.shipments.comment']; ?></label>
						<?= $form['input.shipments.comment']; ?>
                    </div>
                </div>
            </div>
		</div>
    	<div class="form-actions">
			<?= $form['form.submit']; ?>
			<?php
				if( $deleteInsteadOfCancel ) {
            		print('<a href="shipment/shipment/delete/'.$form['value.shipments.id'].'" class="btn">Cancel</a>');
				} else {
					print('<input type="reset" class="btn" value="Cancel" onClick="javascript:history.back(1)"/>');
				}
			?>
		</div>
    </form>

	<div class="modal hide" id="consolidateDifferentDCs">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3>Really consolidate these records?</h3>
		</div>
		<div class="modal-body">
			<p>Do you really want to add deliveries to [DC] into one CLP?</p>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn btn-danger cancel"><i class="icon-remove icon-white"></i> No</a>
			<a class="btn btn-success submit"><i class="icon-ok icon-white"></i> Yes</a>
		</div>
	</div>

	<div class="modal hide" id="notMatchingDCs">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3>Header DC not matching delivery DCs!</h3>
		</div>
		<div class="modal-body">
			<p></p>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn btn-danger cancel"><i class="icon-remove icon-white"></i> No</a>
			<a class="btn btn-success submit"><i class="icon-ok icon-white"></i> Yes</a>
		</div>
	</div>

	<div class="modal hide" id="higherVolumeDCs">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">×</a>
			<h3>Header DC with higher volume available!</h3>
		</div>
		<div class="modal-body">
			<p></p>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn btn-danger cancel"><i class="icon-remove icon-white"></i> No</a>
			<a class="btn btn-success submit"><i class="icon-ok icon-white"></i> Yes</a>
		</div>
	</div>

	<?= $this->load->view('general/footer', '', true); ?>
	<?= $this->load->view('general/javascript', '', true); ?>
	<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>