<div class="delivery">
	<hr/>
	<?php if( $this->acl->hasPermission( Acl::$action_prefix.'shipment/shipment_removeDelivery' ) && $row['approval'] !== '1' ) { ?>
		<a href="#" class="pull-right delete-delivery" rel="<?= $form['value.deliveries.id']; ?>"><i class="icon-remove"></i></a><br/>
	<?php } ?>
	<?= $form['input.deliveries.id']; ?>
	<div class="row">
		<div class="span3">
			<label><?= $form['label.deliveries.contract_number']; ?></label>
			<?= $form['input.deliveries.contract_number']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.deliveries.product_code']; ?></label>
			<?= $form['input.deliveries.product_code']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.deliveries.case_quantity']; ?></label>
			<?php
			$contractType = $form['value.deliveries.contract_type'];
			if( $row['approval'] === '0' || $row['approval'] === '2' ) {
			    print( $form['input.deliveries.case_quantity_mm'] );
			} else {
				print( $form['value.deliveries.case_quantity_mm'] );
			}
			?>
		</div>
		<div class="span3">
			<label><?= $form['label.deliveries.unit_quantity']; ?></label>
			<?php
				print( $form['input.deliveries.unit_quantity_mm'] );
			?>
			<span id="unit-quantity-remain" class="hide"><?= $form['value.deliveries.unit_quantity_remain']; ?></span>
			<span id="unit-quantity-total" class="hide"><?= $form['value.deliveries.unit_quantity']; ?></span>
			<span id="unit-quantity-actual" class="hide"><?= $form['value.deliveries.unit_quantity_mm']; ?></span>
			<span id="telex_received" class="hide"><?= $form['input.deliveries.telex_received_mm']; ?></span>
		</div>
		<div class="span3">
			<label><?= $form['label.deliveries.volume']; ?></label>
			<?php
				print( $form['input.deliveries.volume_mm'] );
			?>
		</div>
	</div>
	<div class="row">
		<div class="span3">
			<label><?= $form['label.deliveries.gross_weight']; ?></label>
			<?php
				print( $form['input.deliveries.gross_weight_mm'] );
			?>
		</div>
		<div class="span3">
			<label><?= $form['label.deliveries.dc']; ?></label>
			<?= $form['input.deliveries.dc']; ?>
		</div>
		<div class="span3">
			<label><?= $form['label.deliveries.hbl_hawb_mm']; ?></label>

			<?php if( $row['approval'] !== '3' ) { ?>
				<input type="hidden" name="deliveries_hbl_hawb_mm[]" value="<?= $form['value.deliveries.hbl_hawb_mm']; ?>">
				<style>
					.input-append input, .uneditable-input {
						width: 163px;
					}
				</style>
				<?php
				$hblHawbs = explode( ',', $form['value.deliveries.hbl_hawb_mm'] );
				foreach( $hblHawbs as $hblHawb ) {

				    if( $this->acl->hasPermission( Acl::$action_prefix.'shipment/shipment_removeHBLHAWB' ) ) {
    					print('<div class="input-append"><span class="input uneditable-input">'.$hblHawb.'</span><button class="btn remove-hblhawb" type="button" data-delivery-id="'.$form['value.deliveries.id'].'"><i class="icon-remove"></i></button></div>');
				    } else {
						print('<div class="input-append"><span class="input uneditable-input">'.$hblHawb.'</span></div>');
                    }
				}
				?>
				<div class="input-append" class="span3">
					<input type="text" class="hblhawb-typeahead input-hblhawb" data-disable="false">
					<button class="btn add-hblhawb" type="button" data-delivery-id="<?= $form['value.deliveries.id']; ?>"><i class="icon-plus"></i></button>
				</div>
			<?php } else { ?>
				<?= $form['input.deliveries.hbl_hawb_mm']; ?>
			<?php } ?>
		</div>
		<div class="span3 hide">
			<label><?= $form['label.deliveries.destination']; ?></label>
			<?= $form['input.deliveries.destination']; ?>
		</div>
	</div>
</div>