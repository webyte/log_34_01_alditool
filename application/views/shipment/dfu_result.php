<?= form_open( 'shipment/document_follow_up/send', array( 'class' => 'dfu-search-form' ) ) ?>
<table class="table table-striped table-bordered table-condensed dfuresulttable">
	<thead>
    <tr>
		<th class='{sorter: false}'><input type="checkbox" value="" name="dfu-select-all" id="dfu-select-all"></th>
        <th class='{sorter: false}'>HBL / HAWB</th>
        <th class='{sorter: false}'>Container number</th>
        <th class='{sorter: false}'>Contract/PO number</th>
        <th class='{sorter: false}'>On-Sale week</th>
	</tr>
    </thead>
    <tbody>
	<?php
		$tableRows = '';
		foreach( $records as $row ){
			$rowColumns = '<td><input type="checkbox" value="'.$row['shipments_id'].'" name="shipmentid[]" class="validate[required]" data-contract-number="'.$row['contracts_contract_number'].'"></td>';
			$rowColumns .= '<td>'.$row['shipments_hbl_hawb'].'</td>';
			$rowColumns .= '<td>'.$row['shipments_container_number'].'</td>';
			$rowColumns .= '<td>'.$row['contracts_contract_number'].'</td>';
			$rowColumns .= '<td>'.$row['contracts_advertisement_week'].'</td>';
			$tableRows .= '<tr>'.$rowColumns.'</tr>';
		}
		print( $tableRows );
	?>
    </tbody>
</table>
<div class="form-actions">
	<p><strong>Select document type and send email</strong></p>
	<label class="checkbox inline">
		<input type="checkbox" value="Invoice" name="documents[]" class="validate[required]">
		Invoice
	</label>
	<label class="checkbox inline">
		<input type="checkbox" value="B/L" name="documents[]" class="validate[required]">
		B/L
	</label>
	<label class="checkbox inline">
		<input type="checkbox" value="Packing List" name="documents[]" class="validate[required]">
		Packing List
	</label>
	<label class="checkbox inline">
		<input type="checkbox" value="Treatment Certificate" name="documents[]" class="validate[required]">
		Treatment Certificate
	</label>
	<label class="checkbox inline">
		<input type="checkbox" value="Packing Declaration" name="documents[]" class="validate[required]">
		Packing Declaration
	</label>
	<label class="checkbox inline">
		<input type="checkbox" value="Certificate of Origin" name="documents[]" class="validate[required]">
		Certificate of Origin
	</label>
	<br/><br/>
    <button type="submit" class="btn btn-primary" id="send-dfu"><i class="icon-envelope icon-white"></i> Send</button>
</div>
<?= form_close() ?>

