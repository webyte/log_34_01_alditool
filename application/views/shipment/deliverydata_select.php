<table class="table table-striped table-bordered table-condensed">
	<thead>
		<tr>
            <th>Product code</th>
			<th>DC</th>
            <th>Destination</th>
            <th>Case quantity</th>
            <th>Unit quantity</th>
            <th>Volume</th>
            <th>Gross weight</th>
            <th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
			<?php
			foreach( $deliveries as $delivery ) {
				$out = '
				<tr>
					<td>'.$delivery['product_code'].'</td>
					<td>'.$delivery['dc'].'</td>
					<td>'.$delivery['destination'].'</td>
					<td>'.$delivery['case_quantity'].'</td>
					<td>'.$delivery['unit_quantity'].'</td>
					<td>'.$delivery['volume'].'</td>
					<td>'.$delivery['gross_weight'].'</td>
					<td class="span1"><button class="btn add-delivery" rel="'.$delivery['id'].'"><i class="icon-plus"></i></button></td>
				</tr>';
				print($out);
			}
			?>
	</tbody>
</table>
