<?= $this->load->view('general/doctype_html', '', true); ?>
<?= $this->load->view('general/html_head', '', true); ?>

<body>
<?= $this->load->view('general/header', array(), true); ?>
<h1><?=$headline; ?></h1><br/>
<?=$form; ?>
<div class="container">
    <?=$result; ?>
</div>

<!-- modal: document follow up mail -->
<div class="modal hide" id="modal-dfu">
	<div class="modal-header">
		<a class="close" data-dismiss="modal">×</a>
		<h3><?=$dfuMail['subject']; ?></h3>
	</div>
	<div class="modal-body">
		<?php
		if( count( $dfuMail['recipients'] ) > 0 ){
			echo '<h5>Please select contact person(s)</h5><div class="dfu-recipients">';
			foreach( $dfuMail['recipients'] as $recipient ){
				$checked = ( $recipient['default_contact'] == '1' ) ? ' checked="checked"' : '';
				echo '<label class="checkbox"><input type="checkbox" name="dfu_recipient" value="'.$recipient['id'].'"'.$checked.'> '.$recipient['name'].' - '.$recipient['supplier_name'].'</label>';
			}
			echo '</div><hr />';
		}
		?>
		<p><textarea style="width: 510px; height: 200px;" id="dfu-body"><?=$dfuMail['body']; ?></textarea></p>
		<input type="hidden" value="<?= $dfuMail['advertisement_week']; ?>" name="dfu-advertisement-week" id="dfu-advertisement-week" />
	</div>
	<div class="modal-footer">
		<a data-dismiss="modal" class="btn">Cancel</a>
		<a class="btn btn-danger submit"><i class="icon-envelope icon-white"></i> Submit</a>
	</div>
</div>

<?= $this->load->view('general/footer', '', true); ?>
<?= $this->load->view('general/javascript', '', true); ?>
<?=!empty($additionalJS) ? $additionalJS : ''; ?>

</body>

</html>