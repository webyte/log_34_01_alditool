<?php
class Reminder_cron extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Shipment_model', 'model');
		//$this->output->enable_profiler(TRUE);
	}

	public function init() {

		// get shipments with num_of_transmissions = 0 and send mail depending on destination port (3 days before ETA)
		$shipments = $this->getShipmentRecords();
		$this->generateMail( $shipments );
	}

	/**
	 * getShipmentRecordsByOffsetAndNoTransmissions
	 * @return
	 */
	private function getShipmentRecords() {
		$start 	= date( 'Y-m-d', strtotime( '-5 days', time() ) ).' 00:00:00';
		$stop 	= date( 'Y-m-d', strtotime( '+3 days', time() ) ).' 23:59:59';

		$shipments = $this->model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0, 'eta >=' => $start, 'eta <=' => $stop, 'num_of_transmission' => 0 ) );
		return $shipments;
	}

	/**
	 * generateMail
	 * @return
	 */
	private function generateMail( $shipments ) {
		foreach( $shipments as $shipment ) {

			$this->load->helper( 'str_helper' );
			$mailData						= array();
			$mailData['CONTAINER_NUMBER'] 	= $shipment['container_number'];
			$mailData['ETA'] 				= $shipment['eta'];

			$data 				= array();
			$data['subject'] 	= replacePattern( $this->lang->line( 'container.reminder.mail.subject' ), $mailData );
			$data['body'] 		= replacePattern( $this->lang->line( 'container.reminder.mail.body' ), $mailData );


			switch( $shipment['destination_port'] ) {
				case 1:
					$data['recipient'] = 'Aldi-Imports-Syd@logwin-logistics.com';
					break;
				case 2:
					$data['recipient'] = 'Aldi-Imports-Bri@logwin-logistics.com';
					break;
				case 3:
					$data['recipient'] = 'Aldi-Imports-Per@logwin-logistics.com';
					break;
				case 4:
					$data['recipient'] = 'Aldi-Imports-Mel@logwin-logistics.com';
					break;
				case 5:
					$data['recipient'] = 'Aldi-Imports-Mel@logwin-logistics.com';
					break;
			}

			$data['cc'] = array('Stefanie.Stahl@logwin-logistics.com');
			if( ENVIRONMENT != 'production' ) $data['recipient'] = array('or@webyte.org','Stefanie.Stahl@logwin-logistics.com');
			if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
		}
	}


		/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$cc			= ( isset( $data['cc'] ) ) ? $data['cc'] : '';
		$subject 	= $data['subject'];
		$body 		= $data['body'];

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		if( !empty( $cc ) ) $this->email->cc( $cc );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Reminder_cron.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Reminder_cron.sendMail' );
			return true;
		}
		$this->email->clear(TRUE);
	}

}

/* End of file reminder_cron.php */
/* Location: ./app/controllers/reminder_cron.php */