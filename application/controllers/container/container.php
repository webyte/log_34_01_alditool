<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Container extends Abstract_controller {

	public static $PROVIDER_ACFS 	= 'ACFS';
	public static $PROVIDER_NEWLINE = 'NEWLINE';

	function __construct() {
		parent::__construct();
		$this->load->model('Container_model', 'model');
		$this->load->model('Contract_model', 'contract_model');

		$this->tablename 			= 'shipments';
		$this->scriptPath 			= 'container/container/';
		$this->recordEditView 		= 'container/containerdata_edit';
		$this->headline				= 'Containers';
		$this->infotext				= '';
		$this->showDefaultSearchForm = FALSE;
		$this->overviewFields 	= array(
			array('label' => 'Container', 			'fieldname' => 'container_number', 'type' => ''),
			array('label' => 'EDO sent', 			'fieldname' => 'edo_sent', 'type' => ''),
			array('label' => 'Destination', 		'fieldname' => 'destination_port', 'type' => ''),
			array('label' => 'Vessel', 				'fieldname' => 'vessel_2', 'type' => ''),
			array('label' => 'Voyage', 				'fieldname' => 'voyage_2', 'type' => ''),
			array('label' => 'Shipping line', 		'fieldname' => 'carrier', 'type' => ''),
			array('label' => 'Sales Week', 			'fieldname' => 'advertisement_week', 'type' => ''),
			array('label' => 'ETA', 				'fieldname' => 'eta', 'type' => ''),
			array('label' => 'DC due date', 		'fieldname' => 'aldi_au_wh_delivery_due_date', 'type' => ''),
			array('label' => 'HBL', 				'fieldname' => 'hbl', 'type' => ''),
			array('label' => 'Contracts', 			'fieldname' => 'contract_numbers', 	'type' => ''),
			array('label' => 'No transmission', 	'fieldname' => 'num_of_transmission', 'type' => ''),
			array('label' => 'DG', 					'fieldname' => 'dg', 'type' => ''),
			array('label' => 'DG Class', 			'fieldname' => 'dg_class', 'type' => ''),
			array('label' => 'Last transmission', 	'fieldname' => 'last_transmission', 'type' => ''),
			array('label' => 'Temperature', 		'fieldname' => 'temperature', 'type' => ''),
			array('label' => 'File transmission', 	'fieldname' => 'file_transmission', 'type' => ''),
		);

		//$this->output->enable_profiler(TRUE);
	}


	/**
	 * recordList
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE, $transmitSuccess = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']		= generateCustomSearchForm( $this->tablename, array(), 'container/container/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']	= '';
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['transmitlink'] 	= $this->scriptPath.'transmit';
		$data['additionalJS'] 	= '<script src="'.base_url().'js/container.js"></script>';
		$data['transmitSuccess'] = $transmitSuccess;
		$this->load->view('container/record_overview', $data );
	}


	/**
	 * result
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function result( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->recordListSearchParams['shipments_modified'] = array( date( 'Y-m-d 00:00:00', strtotime( '-183 days', time() ) ) );
		$this->records					= $this->model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']		= generateCustomSearchForm( $this->tablename, array(), 'container/container/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']	= generateCustomOverview( $this->overviewFields, $this->records, array(), 'container/overview_table' );
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['transmitlink'] 	= $this->scriptPath.'transmit';
		$data['additionalJS'] 	= '<script src="'.base_url().'js/container.js"></script>';
		$data['transmitSuccess'] = FALSE;
		$this->load->view('container/record_overview', $data );
	}

	/**
	 * transmit
	 * generates xml report
	 * @param
	 */
	public function transmit(){
		$tmp 		= $this->input->post('container-ids');
		$shipments 	= json_decode( $tmp );

		foreach( $shipments as $id ) {
			$this->generateXML( $id );
		}

		$this->recordList( array(), TRUE, TRUE );
	}

	/**
	 * get_file
	 * returns file
	 * @param
	 */
	public function get_file( $id=0 ){
		if( $id > 0 ) {
			$container = $this->model->getRecord($id);
			$file = $container['file_transmission'];
			if( !empty( $file ) ) {
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false);
				header("Content-type: application/force-download");
				header("Content-Disposition: attachment; filename=\"".basename( $file )."\";" );
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".@filesize( $file ) );
				readfile( $this->config->item('archive_path').$file );

			}
		}
	}

	/**
	 * setEDOSent
	 * @param
	 */
	public function setEDOSent(){
		$id 		= $this->input->post('id');
		$edoSent 	= $this->input->post('edoSent');
		$this->model->updateRecord( $id, array('edo_sent' => ( $edoSent == 'false' ? 0 : 1 ) ) );
		if( $edoSent != 'false' ) {
			$this->load->model('Shipment_model', 'shipment_model');
			$this->shipment_model->setEdoSentClickDate( $id );
		}

	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * generateXML
	 * @return string
	 */
	private function generateXML( $id ) {
		$archivePath 	= $this->config->item('archive_path');
		$data 			= $this->model->getFreightTrackerRecord( $id );
		$file 			= 'freighttracker-'.$id.'-'.random_string('alnum', 16).'.xml';

		//shipments_dc
		// DC SIR, JKT, AUIBNAM, AUIBNAC, AUISYAM, AUISYAC, AUIMEAM oder AUIMEAC => ACFS
		// DCs (STP, MIN, PRE und BRE) => Newline/AUFRE
		// DC RGY und ETA > 28.05.2023 => ACFS, < 28.05.2023 => Newline
		// DC DER und ETA > 18.06.2023 => ACFS, < 18.06.2023 => Newline
		// DC DAN und ETA > 09.07.2023 => ACFS, < 09.07.2023 => Newline

		$shipment	= $this->model->getRecord( $id );
		$dc 		= $this->getDC( $shipment['dc'] );
		$provider	= '';
		$acfsDCs 	= array('SIR', 'JKT', 'AUIBNAM', 'AUIBNAC', 'AUISYAM', 'AUISYAC', 'AUIMEAM', 'AUIMEAC');
		$newlineDCs = array('STP', 'MIN', 'PRE', 'BRE');
		$eta		= $shipment['eta'];

		if(  in_array( $dc, $acfsDCs ) ) {
			$provider = self::$PROVIDER_ACFS;
		} else if( in_array( $dc, $newlineDCs ) ) {
			$provider = self::$PROVIDER_NEWLINE;
		} else if( $dc == 'RGY') {
			$provider = ( strtotime( $eta ) > strtotime( '2023-05-28' ) ) ? self::$PROVIDER_ACFS : self::$PROVIDER_NEWLINE;
		} else if( $dc == 'DER') {
			$provider = ( strtotime( $eta ) > strtotime( '2023-06-17' ) ) ? self::$PROVIDER_ACFS : self::$PROVIDER_NEWLINE;
		} else if( $dc == 'DAN') {
			$provider = ( strtotime( $eta ) > strtotime( '2023-07-09' ) ) ? self::$PROVIDER_ACFS : self::$PROVIDER_NEWLINE;
		}

		if( empty( $provider ) ) {
			$this->welogger->log( "no provider, id: ".$id, WELogger::$LOG_LEVEL_INFO, "container.generateAndTransmitXML" );
			return;
		}

		if( $provider == self::$PROVIDER_ACFS ) {
			$xml 		= $this->load->view('container/freighttracker_aufre_xml', $data, true);
			$this->welogger->log( "generateXML - load view - ACFS - DC: ".$dc.", ETA: ".$eta, WELogger::$LOG_LEVEL_INFO, "container.generateAndTransmitXML" );
		} else if( $provider == self::$PROVIDER_NEWLINE ) {
			$xml 		= $this->load->view('container/freighttracker_xml', $data, true);
			$this->welogger->log( "generateXML - load view - NEWLINE - DC: ".$dc.", ETA: ".$eta, WELogger::$LOG_LEVEL_INFO, "container.generateAndTransmitXML" );
		}

		$this->load->helper('file');
		write_file($archivePath.$file, $xml);
		$this->welogger->log( "xml generated - shipmentId: ".$id.", file: ".$file, WELogger::$LOG_LEVEL_INFO, "container.generateAndTransmitXML" );

		if( $provider == self::$PROVIDER_ACFS ) {
			$exportPath = $this->config->item('export_path').'acfs/';
			copy( $archivePath.$file, $exportPath.$file );
			$this->welogger->log( "generateXML - copy file - ACFS - DC: ".$dc.", ETA: ".$eta, WELogger::$LOG_LEVEL_INFO, "container.generateAndTransmitXML" );
		} else if( $provider == self::$PROVIDER_NEWLINE ) {
			$this->transmitXMLPerSFTP( $file, $archivePath, $id );
			$this->welogger->log( "generateXML - transmitXMLPerSFTP - NEWLINE - DC: ".$dc.", ETA: ".$eta, WELogger::$LOG_LEVEL_INFO, "container.generateAndTransmitXML" );
		}

		if( ENVIRONMENT != 'production' ) {
			$data = array();
			$data['container_number'] = $shipment['container_number'];
			$data['dc'] = $dc;
			$data['eta'] = $eta;
			$data['provider'] = $provider;
			$this->sendMail( $data, $archivePath.$file );
		}

		$this->model->updateRecord( $id, array( 'file_transmission' => $file,'last_transmission' => date('Y-m-d H:i:s' ), 'num_of_transmission' => ( intval($data['shipments_num_of_transmission']) + 1 ) ) );
		return $file;
	}

	/**
	 * transmitXMLPerSFTP
	 * @return
	 */
	private function transmitXMLPerSFTP( $file, $path, $shipmentId ) {
		if( ENVIRONMENT == 'production' ) {
			$this->load->library('sftp'); // initialize library
			$this->sftp->connect();

			if( !$this->sftp->put($path.$file,'logwin/from_logwin/'.$file) ) {
				$this->welogger->log( "ftp upload error - shipmentId: ".$shipmentId.", file: ".$file, WELogger::$LOG_LEVEL_INFO, "container.generateAndTransmitXML" );
			}
		}
	}

	/**
	 * getDC
	 * returns dc name
	 *
	 * @param int $id - id of record to retrieve
	 * @return string - dc name
	 */
	private function getDC( $id='' ) {
		$this->load->model('Dc_model', 'dc_model');
		$row = $this->dc_model->getRecord( $id );
		return ( isset( $row['dc'] ) > 0 ) ? $row['dc'] : '';
	}
	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {

		// ist nur ne quick&dirty Lösung!
		// muss man mal ordentlich machen...

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = $this->tca->getFormInputById('shipments_container_number', $this->getSearchFormValue('shipments_container_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Destination";
		$tmp['input'] = $this->tca->getFormInputById('shipments_destination_port', $this->getSearchFormValue('shipments_destination_port'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Vessel";
		$tmp['input'] = $this->tca->getFormInputById('shipments_vessel_2', $this->getSearchFormValue('shipments_vessel_2'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Voyage";
		$tmp['input'] = $this->tca->getFormInputById('shipments_voyage_2', $this->getSearchFormValue('shipments_voyage_2'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Shipping line";
		$tmp['input'] = $this->tca->getFormInputById('shipments_carrier', $this->getSearchFormValue('shipments_carrier'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "HBL/HAWB";
		$tmp['input'] = '<input type="text" id="shipments_hbl_hawb" value="'.$this->getSearchFormValue('shipments_hbl_hawb').'" name="shipments_hbl_hawb[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'containers_event[]', array( "" => "please select ...", "shipments.eta" => "ETA", "contracts.aldi_au_wh_delivery_due_date" => "DC due date", "shipments.last_transmission" => "Last transmission" ), $this->getSearchFormValue('containers_event') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="containers_from" value="'.$this->getSearchFormValue('containers_from').'" name="containers_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="containers_to" value="'.$this->getSearchFormValue('containers_to').'" name="containers_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Telex released";
		$tmp['input'] = form_dropdown('shipments_telex_received[]', array( "" => "please select ...", "1" => "Yes", "0" => "No" ), $this->getSearchFormValue('shipments_telex_received') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "CUS / AQC";
		$tmp['input'] = form_dropdown('containers_cus[]', array( "" => "please select ...", "1" => "Yes", "0" => "No" ), $this->getSearchFormValue('containers_cus') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "No transmissions";
		$tmp['input'] = '<input type="checkbox" class="checkbox" id="shipments_num_of_transmission" value="no" '.($this->getSearchFormValue('shipments_num_of_transmission') == 'no' ? 'checked' : '').' name="shipments_num_of_transmission[]">';
		$res[] = $tmp;

		return $res;
	}

	/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data, $file ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
		$subject 	= 'Container data transmitted to '.$data['provider'].' with Container '.$data['container_number'].', DC '.$data['dc'].', ETA '.$data['eta'];
		$body 		= '';

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		$this->email->attach( $file );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Container.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Container.sendMail' );
			return true;
		}
		$this->email->clear(TRUE);
	}

}

/* End of file pcshipment.php */
/* Location: ./app/controllers/pcshipment.php */