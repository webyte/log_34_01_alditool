<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Supplier_portal extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model('Document_model', 'document_model');
		$this->load->model('Supplier_comment_model', 'supplier_comment_model');
		$this->load->model('Supplier_portal_model', 'supplier_portal_model');

		$this->tablename 			= 'deliveries';
		$this->scriptPath 			= 'supplier/supplier_portal/';
		$this->recordEditView 		= 'supplier/supplierdata_edit';
		$this->headline				= 'Supplier Portal';
		$this->infotext				= '';
		$this->showDefaultSearchForm = FALSE;
		$this->overviewFields 	= array(
			array('label' => 'Contract Number', 	'fieldname' => 'deliveries_contract_number', 	'type' => ''),
			array('label' => 'DC', 					'fieldname' => 'deliveries_dc', 				'type' => ''),
			array('label' => 'Product Code', 		'fieldname' => 'deliveries_product_code', 		'type' => ''),
			array('label' => 'Pieces', 				'fieldname' => 'shipments_deliveries_mm_case_quantity', 'type' => ''),
			array('label' => 'Documents', 			'fieldname' => 'shipments_deliveries_mm_documents_status', 'type' => ''),
			array('label' => 'Container Number', 	'fieldname' => 'shipments_container_number', 	'type' => ''),
			array('label' => 'HBL', 				'fieldname' => 'pcshipments_hbl_hawb', 			'type' => ''),
			array('label' => 'Destination port', 	'fieldname' => 'shipments_destination_port', 	'type' => ''),
			array('label' => 'ETD', 				'fieldname' => 'shipments_etd', 				'type' => 'date'),
			array('label' => 'ETA', 				'fieldname' => 'shipments_eta', 				'type' => 'date'),
			array('label' => 'ATD', 				'fieldname' => 'shipments_atd', 				'type' => 'date'),
			array('label' => 'ATA', 				'fieldname' => 'shipments_ata', 				'type' => 'date'),
		);

		if( ENVIRONMENT == 'development_local' ) $this->output->enable_profiler(TRUE);
	}


	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {

		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']			= generateCustomSearchForm( $this->tablename, array(), 'supplier/supplier_portal/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']		= '';
		$data['headline'] 			= $this->headline;
		$data['infotext'] 			= $this->infotext;
		$data['createnewlink'] 		= '';
		$data['additionalJS'] 		= '';
		$this->load->view('supplier/record_overview', $data );
	}

	/**
	 * @Override
	 */
	public function result( $searchParams = array(), $ignoreSearchParams = FALSE ) {

		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= $this->supplier_portal_model->getOverviewRecords( $this->recordListSearchParams );
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']			= generateCustomSearchForm( $this->tablename, array(), 'supplier/supplier_portal/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']		= generateCustomOverview( $this->overviewFields, $this->records, $this->getRecordactions(), 'supplier/overview_table' );
		$data['headline'] 			= $this->headline;
		$data['infotext'] 			= $this->infotext;
		$data['createnewlink'] 		= '';
		$data['additionalJS'] 		= '';

		$this->load->view('supplier/record_overview', $data );
	}

	/**
	 * edit
	 * default edit implementation.
	 *
	 * @param int $id - id of record to edit. If id is 0 a new record is created.
	 */
	public function edit( $id = 0 ) {
		$records = $this->supplier_portal_model->getOverviewRecords( array('shipments_deliveries_mm_id' => array($id) ) );

		if (count( $records ) > 0){
			$recorddata = $records[0];
			$formaction = $this->scriptPath."updateRecord/".$id;
		}

		$data['form'] 			= $this->tca->getFormArray( $formaction, array( 'deliveries' => array(), 'shipments' => array(), 'pcshipments' => array() ) );
		$data['record'] 		= $recorddata;
		$data['error'] 			= FALSE;
		$data['message'] 		= '';
		$data['documents'] 		= $this->document_model->getDocumentsByPid( $id );
		$data['comments'] 		= $this->supplier_comment_model->getCommentsByPid( $id );
		$data['documentTypes']	= $this->document_model->getDocumentTypeTitles();
		$data['additionalJS'] 	= '<script src="'.base_url().'js/supplierportal.js"></script>';

		$this->load->view($this->recordEditView, $data );
	}

	/**
	 * uploadDocument
	 * @param $id
	 * @return
	 */
	public function uploadDocument( $id = 0 ) {
		$upload = $this->saveUpload( 'supplier-upload-file' );

		if( !empty( $upload ) ) {
			$documentType 	= $this->input->post('document-type');
			$mandatory 		= $this->document_model->getMandatoryByDocumentType( $documentType, $id );
			$data = array(
				'shipments_deliveries_mm_id' => $id,
				'document_type' => $documentType,
				'mandatory' => $mandatory,
				'uploaded' => date('Y-m-d H:i:s' ),
				'status' => Document_model::$STATUS_DOCUMENT_UPLOADED,
				'file' => $upload,
			);
			$this->document_model->createNew( $data );
			$this->supplier_portal_model->updateMandatoryDocumentsStatus( $id );
		}

		print( json_encode( array( 'success' => 'true' ) ) );
		//redirect( 'supplier/supplier_portal/edit/'.$id );
	}

	/**
	 * viewDocument
	 * returns file
	 * @param $id
	 */
	public function viewDocument( $id=0 ){
		if( $id > 0 ) {
			$row = $this->document_model->getRecord($id);
			$file = $row['file'];
			if( !empty( $file ) ) {
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false);
				header("Content-type: application/force-download");
				header("Content-Disposition: attachment; filename=\"".basename( $file )."\";" );
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".@filesize( $file ) );
				readfile( $this->config->item('documents_upload_path').$file );
			}
		}
	}

	/**
	 * deleteDocument
	 * return
	 * @param $recordId, $documentId
	 */
	public function deleteDocument( $recordId=0, $documentId=0 )
	{
		if( $recordId != 0 && $documentId != 0 ) {
			$this->document_model->updateRecord( $documentId, array( 'deleted' => 1 ) );
			$this->supplier_portal_model->updateMandatoryDocumentsStatus( $recordId );
			redirect( 'supplier/supplier_portal/edit/'.$recordId );
		}
	}

	/**
	 * confirmDocument
	 * return
	 * @param $recordId, $documentId
	 */
	public function confirmDocument( $recordId=0, $documentId=0, $isChecked=0 )
	{
		if( $recordId != 0 && $documentId != 0 ) {
			$confirmed 	= $isChecked ? 1 : 0;
			$status 	= $confirmed ? Document_model::$STATUS_DOCUMENT_CONFIRMED : Document_model::$STATUS_DOCUMENT_UPLOADED;
			$this->document_model->updateRecord( $documentId, array( 'confirmed' => $confirmed, 'status' => $status ) );
			if( $confirmed ) $this->document_model->export( $recordId, $documentId );
			$this->supplier_portal_model->updateMandatoryDocumentsStatus( $recordId );
			redirect( 'supplier/supplier_portal/edit/'.$recordId );
		}
	}

	/**
	 * setMandatory
	 * return
	 * @param $recordId, $documentId
	 */
	public function setMandatory( $recordId=0, $documentId=0, $mandatory=0 )
	{
		if( $recordId != 0 && $documentId != 0 ) {
			$this->document_model->updateRecord( $documentId, array( 'mandatory' => $mandatory ) );
			redirect( 'supplier/supplier_portal/edit/'.$recordId );
		}
	}

	/**
	 * addComment
	 * @param $id
	 * @return
	 */
	public function addComment( $recordId = 0 )
	{
		$msg = $this->input->post('msg');
		$this->supplier_comment_model->createNew( array( 'pid' => $recordId, 'msg' => $msg ) );
		$this->generateCommentMail( $recordId, $msg );
		redirect( 'supplier/supplier_portal/edit/'.$recordId );
	}

	/**
	 * viewComment
	 * returns file
	 * @param $id
	 */
	public function viewComment( $id=0 )
	{
	}

	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {
		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract Number";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_contract_number', $this->getSearchFormValue('deliveries_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product Code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "DC";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_dc', $this->getSearchFormValue('deliveries_dc'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On Sale Week";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_advertisement_week', $this->getSearchFormValue('deliveries_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container Number";
		$tmp['input'] = $this->tca->getFormInputById('shipments_container_number', $this->getSearchFormValue('shipments_container_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Destination port";
		$tmp['input'] = $this->tca->getFormInputById('shipments_destination_port', $this->getSearchFormValue('shipments_destination_port'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'event[]', array( "" => "please select ...", "etd" => "ETD", "eta" => "ETA", "atd" => "ATD", "ata" => "ATA" ), $this->getSearchFormValue('event') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_from" value="'.$this->getSearchFormValue('shipments_from').'" name="shipments_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_to" value="'.$this->getSearchFormValue('shipments_to').'" name="shipments_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Documents available";
		$tmp['input'] = form_dropdown( 'documents_available[]', array(
			"" => "please select ...",
			"" => "all",
			Document_model::$STATUS_DOCUMENT_UPLOADED => "available",
			Document_model::$STATUS_DOCUMENT_MISSING => "missing",
			Document_model::$STATUS_DOCUMENT_CONFIRMED => "vetted" ), $this->getSearchFormValue('documents_available') );
		$res[] = $tmp;

		return $res;
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * generateCommentMail
	 * @return
	 */
	private function generateCommentMail( $id, $msg ) {

		$records = $this->supplier_portal_model->getOverviewRecords( array('shipments_deliveries_mm_id' => array( $id ) ) );
		if( count( $records ) > 0 ) {
			$this->load->helper( 'str_helper' );
			$record 						= $records[0];
			$mailData						= array();
			$mailData['CONTRACT_NUMBER'] 	= $record['contracts_contract_number'];
			$mailData['DC'] 				= $record['deliveries_dc'];
			$mailData['HBL_HAWB'] 			= $record['pcshipments_hbl_hawb'];
			$mailData['SHIPMENT_NUMBER'] 	= $record['shipments_shipment_number'];;
			$mailData['COMMENT'] 			= $msg;

			$this->load->model('Contactperson_model', 'contactperson_model');
			$recipients = $this->contactperson_model->getDefaultEMailsForSupplier( $record['contracts_supplier'] );

			$data 				= array();
			$data['subject'] 	= replacePattern( $this->lang->line( 'supplier_portal.comment.mail.subject' ), $mailData );
			$data['body'] 		= replacePattern( $this->lang->line( 'supplier_portal.comment.mail.body' ), $mailData );
			$data['recipient'] 	= ( ENVIRONMENT == 'production' ) ? $recipients : array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
			$data['cc'] 		= array();

			if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
		}
	}

	/**
	 * saveUpload
	 * @param $field
	 * @return string
	 */
	private function saveUpload( $field ) {
		$config['upload_path'] 		= $this->config->item('documents_upload_path');
		$config['allowed_types'] 	= '*';
		$config['max_size']			= $this->config->item('max_size');
		$config['encrypt_name'] 	= TRUE;
		$destFile					= '';

		$this->load->library( 'upload', $config );

		if ( !$this->upload->do_upload( $field ) ) {
			$data 	= $this->upload->data();
			$error 	= array('error' => $this->upload->display_errors());

			$this->welogger->log( 'Upload failure, file: '.$data['full_path'].', error: '.$error['error'].', image: '.serialize( $this->upload->data() ), WELogger::$LOG_LEVEL_ERROR, 'Supplier_portal.saveUpload' );
		} else {
			$data 		= $this->upload->data();
			$source 	= $data['full_path'];
			$destFile	= $data['file_name'];

			$this->welogger->log( 'Upload success, file: '.$source, WELogger::$LOG_LEVEL_INFO, 'Supplier_portal.saveUpload' );

		}
		return $destFile;
	}

	/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$cc			= ( isset( $data['cc'] ) ) ? $data['cc'] : '';
		$subject 	= $data['subject'];
		$body 		= $data['body'];

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );

		if( !empty( $cc ) ) $this->email->cc( $cc );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Supplier_portal.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Supplier_portal.sendMail' );
			return true;
		}
		$this->email->clear(TRUE);
	}

}

/* End of file customs.php */
/* Location: ./app/controllers/customs/customs.php */