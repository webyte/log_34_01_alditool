<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class Detention_free_day extends Abstract_controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('detention_free_day_model', 'model');
        $this->tablename 		= 'detention_free_days';
        $this->scriptPath 		= 'manage/detention_free_day/';
        $this->recordEditView 	= 'manage/detention_free_days_edit';
        $this->headline			= 'Detention Free Days';
        $this->infotext			= '';
        $this->showDefaultSearchForm = FALSE;

		//$this->output->enable_profiler(TRUE);
    }

	/**
	 * recordList
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function recordList() {
		$this->recordListSearchParams = $this->input->post() ? $this->input->post() : array();

		$data = array();
		$data['resulttable'] 	= generateTableOverview( $this->tablename, $this->getRecordactions(), $this->recordListSearchParams, $this->recordListWhere, 'detention_free_days/overview_table' );
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['createnewlink'] 	= $this->scriptPath.'edit';
		$data['additionalJS'] 	= '';
		if( $this->showDefaultSearchForm ) $data['searchform'] = generateDefaultSearchForm( $this->tablename, $this->scriptPath.'recordList', 'start search', $this->recordListSearchParams );
		$this->load->view('general/record_overview', $data );
	}

	/**
	 * edit
	 */
	function edit( $id = 0 ) {
		$query 	= $this->db->get_where( $this->tablename, array('id' => $id) );
		if ($query->num_rows() > 0){
			$recorddata = $query->first_row('array');
			$formaction = $this->scriptPath."updateRecord/".$id;
		} else {
			$recorddata = array();
			$formaction = $this->scriptPath."createNew";
		}
		$data['infotext'] 	= $this->infotext;
		$data['form'] 		= $this->tca->getFormArray( $formaction, array( $this->tablename => $recorddata ) );
		$this->load->view($this->recordEditView, $data );
	}

	/**
	 * createNew
	 */
	function createNew() {
		$this->saveUserInput();
	}

	/**
	 * updateRecord
	 */
	function updateRecord( $id = 0 ) {
		$this->saveUserInput( $id );
	}

	/**
	 * saveUserInput
	 */
	private function saveUserInput( $id = 0 )
	{
		$insertArray 	= $this->getInsertArrayFromPostVars();
		$contractNumber = $insertArray[0]['contract_number'];
		$containerType 	= $insertArray[0]['container_type'];
		if( !empty($contractNumber) || !empty($containerType) ) {
			if( $this->model->isUnique( $contractNumber, $containerType, $id ) ) {
				if ($id == 0) $this->model->createNew( $insertArray[0] );
				else $this->model->updateRecord($id, $insertArray[0]);
				$this->recordList();
			} else {
				$this->infotext = '<strong>Error:</strong> Combination of Contract number and Container type already exists!';
				$this->edit( $id );
			}

		}
	}

}

/* End of file supplier.php */
/* Location: ./app/controllers/supplier.php */