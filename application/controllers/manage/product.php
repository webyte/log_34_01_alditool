<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class Product extends Abstract_controller {

    function __construct()
    {
		parent::__construct();
		$this->load->model('product_model', 'model');
		$this->tablename 		= 'products';
		$this->scriptPath 		= 'manage/product/';
		$this->recordEditView 	= 'manage/productdata_edit';
		$this->headline			= 'Products';
		$this->infotext			= '';
		$this->showDefaultSearchForm = FALSE;
    }

	/**
	 * recordList
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function recordList() {
		$this->recordListSearchParams = $this->input->post() ? $this->input->post() : array();

		$data = array();
		$data['resulttable'] 	= generateTableOverview( $this->tablename, $this->getRecordactions(), $this->recordListSearchParams, $this->recordListWhere );
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['createnewlink'] 	= $this->acl->hasPermission( Acl::$crud_prefix.$this->tablename.'_new' ) ? $this->scriptPath.'edit' : '';
		$data['additionalJS'] 	= '';
		if( $this->showDefaultSearchForm ) $data['searchform'] = generateDefaultSearchForm( $this->tablename, $this->scriptPath.'recordList', 'start search', $this->recordListSearchParams );
		$this->load->view('general/record_overview', $data );
	}


}