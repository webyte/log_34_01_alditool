<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class User extends Abstract_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model', 'model');
		$this->tablename 		= 'users';
		$this->scriptPath 		= 'manage/user/';
		$this->recordEditView 	= 'manage/userdata_edit';
		$this->headline			= 'Users';
		$this->infotext			= '';
		$this->showDefaultSearchForm = FALSE;
	}

}

/* End of file user.php */
/* Location: ./app/controllers/user.php */