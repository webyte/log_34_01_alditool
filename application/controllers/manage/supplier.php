<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class Supplier extends Abstract_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('supplier_model', 'model');
        $this->load->model('contactperson_model', 'contactperson_model');
		$this->tablename 		= 'suppliers';
		$this->scriptPath 		= 'manage/supplier/';
		$this->recordEditView 	= 'manage/supplierdata_edit';
		$this->headline			= 'Suppliers';
		$this->infotext			= '';
		$this->showDefaultSearchForm = TRUE;
		$this->overviewFields 	= array(
			array('label' => 'Name', 			'fieldname' => 'name', 			'type' => ''),
			array('label' => 'Street', 			'fieldname' => 'street', 		'type' => ''),
			array('label' => 'City', 			'fieldname' => 'city', 			'type' => ''),
			array('label' => 'Contact name', 	'fieldname' => 'contact_name', 	'type' => ''),
			array('label' => 'Contact mail', 	'fieldname' => 'contact_mail', 	'type' => ''),
			array('label' => 'Contact phone', 	'fieldname' => 'contact_phone', 'type' => ''),
			array('label' => 'Code', 			'fieldname' => 'code', 			'type' => ''),
		);
	}

	/**
	 * @Override
	 */
	public function recordList() {
		$this->recordListSearchParams 	= $this->input->post() ? $this->input->post() : array();
		$this->records					= $this->model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']		= generateCustomSearchForm( $this->tablename, array(), 'manage/supplier/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']	= generateCustomOverview( $this->overviewFields, $this->records, $this->getRecordactions() );
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['createnewlink'] 	= $this->acl->hasPermission( Acl::$crud_prefix.$this->tablename.'_new' ) ? $this->scriptPath.'edit' : '';
        $data['additionalJS'] 		= '<script src="'.base_url().'js/supplier.js"></script>';
		$this->load->view('general/record_overview', $data );
	}


	/**
	 * result
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function result( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordList( $searchParams, $ignoreSearchParams );
	}

	/**
     * @Override
     */
    public function edit( $id = 0 ) {
        $query 	= $this->db->get_where( $this->tablename, array('id' => $id) );
        if ($query->num_rows() > 0){
            $recorddata = $query->first_row('array');
            $formaction = $this->scriptPath."updateRecord/".$id;
        } else {
            $recorddata = array();
            $formaction = $this->scriptPath."createNew";
        }
        $data['newContactPersonPerms']	= $this->acl->hasPermission( Acl::$crud_prefix.'contactpersons_new' );
        $data['additionalJS'] 		= '<script src="'.base_url().'js/supplier.js"></script>';
        $data['form']               = $this->tca->getFormArray( $formaction, array( $this->tablename => $recorddata ) );
        $this->load->view($this->recordEditView, $data );
    }

    /**
     * getNewContactPersonForm
     * returns a new html-form for a delivery record to be inserted
     * in the frontend dynamically.
     */
    public function getNewContactPersonForm() {
        $data 			= array();
        $data['form'] 	= $this->tca->getFormArray( "" , array( 'contactpersons' => array() ) );
        $data['deletePerms']= $this->acl->hasPermission( Acl::$crud_prefix.'contactpersons_delete' );
        $res 			= $this->load->view('manage/contactpersonsdata_edit', $data, true );
        print( $res );
    }

	/**
	 * removeContactPerson
	 */
	public function removeContactPerson($id=0) {
		if( $id > 0 ) {
			$this->contactperson_model->deleteRecord( $id );
		}
	}

    /**
     * @Override
     */
    public function createNew() {
        $id = $this->saveUserInput();
        //$this->recordList( array(), TRUE );
        redirect( $this->scriptPath.'edit/'.$id );
    }

    /**
     * @Override
     */
    public function updateRecord( $id = 0 ) {
        $this->saveUserInput( $id );
        //$this->recordList( array(), TRUE );
        redirect( $this->scriptPath.'edit/'.$id );
    }

    ////////////////////////////////////////////////////////////////////////////

    //		private

    ////////////////////////////////////////////////////////////////////////////

    /**
     * saveUserInput
     * general function for saving user input
     * @param int $id - id of record to edit. If id is 0 a new record is created.
     */
    private function saveUserInput( $id = 0 ) {
          // shipment record
        $insertArray 	= $this->getInsertArrayFromPostVars();
        $firstSupplier 	= $insertArray[0];

        if( $id == 0 ) $id = $this->model->createNew( $firstSupplier );
        else $this->model->updateRecord( $id, $firstSupplier );

        // contactpersons record
        $contactPersonRecordsInsertArray = $this->getInsertArrayFromPostVars('contactpersons');
        foreach ( $contactPersonRecordsInsertArray as $contactPersonInsertArray ) {
            $contactPersonId = $contactPersonInsertArray['id'];

            $contactPersonInsertArray['supplier_id'] = $id;
            // save contactpersons
            if( !empty( $contactPersonId ) ){
                unset( $contactPersonInsertArray['id'] );
                $this->contactperson_model->updateRecord( $contactPersonId, $contactPersonInsertArray );
            } else {
                $contactPersonId = $this->contactperson_model->createNew( $contactPersonInsertArray );
            }
        }

        return $id;
    }
	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {
		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Name";
		$tmp['input'] = $this->tca->getFormInputById('suppliers_name', $this->getSearchFormValue('suppliers_name'), '1');
		$res[] = $tmp;


		return $res;
	}

}

/* End of file supplier.php */
/* Location: ./app/controllers/supplier.php */