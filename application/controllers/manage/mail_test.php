<?php

class Mail_test extends CI_controller {

	function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
		ini_set( 'display_errors', 1 );
		error_reporting( E_ALL );
	}

	/**
	 * send_with_attachment
	 */
	public function send_with_attachment() {
		$recipient 	= array('or@webyte.org');
		$fileName 	= $this->config->item('tmp_path').'logwin_aldi_clp_po_report_6C1yrkgnbhj03RYr5093X3MOnVZW2dzg.zip';
		$name 		= 'PO Report - Test mail with attachment';
		$this->generateMail( $fileName, $name, $recipient );
	}

	/**
	 * send_without_attachment
	 */
	public function send_without_attachment() {
		$recipient 	= array('or@webyte.org');
		$fileName 	= '';
		$name 		= 'PO Report - Test mail without attachment';
		$this->generateMail( $fileName, $name, $recipient );
	}

	/**
	 * generateMail
	 * @return
	 */
	private function generateMail( $file, $name, $recipient ) {
		$data 				= array();
		$data['subject'] 	= $this->lang->line( 'report_clp.mail.subject' ).$name;
		$data['body'] 		= $this->lang->line( 'report_clp.mail.body' ).$name;
		$data['file']		= $file;
		$data['recipient'] 	= $recipient;

		if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
	}

	/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$subject 	= $data['subject'];
		$body 		= $data['body'];
		$file 		= $data['file'];

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		if( !empty( $file ) ) $this->email->attach( $file );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			print($this->email->print_debugger());
			//$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Mail_test.sendMail' );
			return false;
		} else {
			print($this->email->print_debugger());
			//$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Mail_test.sendMail' );
			return true;
		}
		$this->email->clear(TRUE);
	}
}
/* End of file Mail_test.php */
/* Location: ./app/controllers/manage/Mail_test.php */