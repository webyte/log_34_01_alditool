<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class Delivery_due extends Abstract_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model('Delivery_due_model', 'model');
		$this->tablename 		= 'delivery_dues';
		$this->scriptPath 		= 'manage/delivery_due/';
		$this->recordEditView 	= 'manage/deliveryduedata_edit';
		$this->headline			= 'ALDI AU WH delivery due date';
		$this->infotext			= '';
		$this->showDefaultSearchForm = FALSE;
		$this->overviewFields 	= array(
			array('label' => 'Sales week', 'fieldname' => 'sales_week', 'type' => ''),
			array('label' => 'Sales year', 'fieldname' => 'sales_year', 'type' => ''),
			array('label' => 'Delivery due date', 'fieldname' => 'delivery_due_date', 'type' => ''),
			array('label' => 'Delivery due end date', 'fieldname' => 'delivery_due_end_date', 'type' => ''),
		);
	}

	/**
	 * recordList
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function recordList( $msg=array() ) {
		$this->recordListSearchParams = $this->input->post() ? $this->input->post() : array();
		$this->records			= $this->model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );

		$data = array();
		$data['resulttable']	= generateCustomOverview( $this->overviewFields, $this->records, $this->getRecordactions() );
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['optionsSalesYear'] = $this->getCalendarYears();
		if( isset( $msg['error'] ) ) {
			$data['error'] 		= $msg['error'];
			$data['message'] 	= $msg['message'];
		}
		$data['additionalJS'] 	= '<script src="'.base_url().'js/deliverydue.js"></script>';
		$this->load->view('manage/deliverydue_record_overview.php', $data );
	}

	/**
	 * edit
	 */
	function edit( $id = 0 ) {
		$query 	= $this->db->get_where( $this->tablename, array('id' => $id) );
		if ($query->num_rows() > 0){
			$recorddata = $query->first_row('array');
			$formaction = $this->scriptPath."updateRecord/".$id;
		} else {
			$recorddata = array();
			$formaction = $this->scriptPath."createNew";
		}
		$data['infotext'] 	= $this->infotext;
		$data['form'] 		= $this->tca->getFormArray( $formaction, array( $this->tablename => $recorddata ) );
		$this->load->view($this->recordEditView, $data );
	}

	/**
	 * upload
	 * @param
	 * @return
	 */
	public function do_upload() {
		$upload = $this->saveUpload( 'delivery-due-upload-file' );
		$data = array();
		if( !empty( $upload ) ) {
			$errors = $this->importUpload( $upload );
			if( count( $errors ) > 0 ) {
				$data['error'] 		= TRUE;
				$data['message'] 	= '<h4>Error!</h4>Following Suppliers are unknown:<br/><br/>'.implode( '<br/>', $errors );
			} else {
				$data['error'] 		= FALSE;
				$data['message'] 	= '<h4>Success!</h4>File was successfully imported.';
			}

		} else {
			$data['error'] 		= TRUE;
			$data['message'] 	= '<h4>Error!</h4>Upload failure, please try again.';
		}

		$this->recordList($data);
	}

	/**
	 * importOrderUpload
	 * @param $file
	 * @param $supplier
	 * @return array
	 */
	private function importUpload( $file ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$i			= 1;
		$src 		= $this->config->item('documents_upload_path').$file;
		$errors		= array();

		$objPHPExcel 	= IOFactory::load( $src );
		$objWorksheet 	= $objPHPExcel->getActiveSheet();
		$salesYear 		= $this->input->post('sales-year');
		foreach ($objWorksheet->getRowIterator() as $row) {
			if( $i > 4 && $i % 2 == 1 ) {
				$salesWeek 		= (string) $objWorksheet->getCellByColumnAndRow(1, $i)->getValue();
				$deliveryDue 	= (string) $objWorksheet->getCellByColumnAndRow(3, $i)->getFormattedValue();
				$deliveryDueEnd = (string) $objWorksheet->getCellByColumnAndRow(4, $i)->getFormattedValue();

				$data 							= array();
				$data['sales_week'] 			= $salesWeek;
				$data['sales_year'] 			= $salesYear;
				$data['delivery_due_date'] 		= date('Y-m-d H:i:s', strtotime($deliveryDue));
				$data['delivery_due_end_date'] 	= date('Y-m-d H:i:s', strtotime($deliveryDueEnd));
				$data['hidden'] 				= 0;
				$data['deleted'] 				= 0;

				$rows = $this->model->getRecordsWhere( $data );
				if( count( $rows ) > 0 ) {
					$id = isset( $rows[0]['id'] ) ? $rows[0]['id'] : 0;
					if( $id > 0 ) $this->model->updateRecord( $id, $data );
				} else {
					$this->model->createNew( $data );
				}
			}
			$i++;
		}
		return $errors;
	}

	/**
	 * saveUpload
	 * @param $field
	 * @return string
	 */
	private function saveUpload( $field ) {
		$config['upload_path'] 		= $this->config->item('documents_upload_path');
		$config['allowed_types'] 	= $this->config->item('allowed_types');
		$config['max_size']			= $this->config->item('max_size');
		$config['encrypt_name'] 	= TRUE;
		$destFile					= '';

		$this->load->library( 'upload', $config );

		if ( !$this->upload->do_upload( $field ) ) {
			$data 	= $this->upload->data();
			$error 	= array('error' => $this->upload->display_errors());

			$this->welogger->log( 'Upload failure, file: '.$data['full_path'].', error: '.$error['error'].', image: '.serialize( $this->upload->data() ), WELogger::$LOG_LEVEL_ERROR, 'Edit.do_upload' );
		} else {
			$data 		= $this->upload->data();
			$source 	= $data['full_path'];
			$destFile	= $data['file_name'];

			$this->welogger->log( 'Upload success, file: '.$source, WELogger::$LOG_LEVEL_INFO, 'Edit.do_upload' );

		}
		return $destFile;
	}

	/**
	 * getCalendarYears
	 * @return array
	 */
	private function getCalendarYears() {
		$out = array();
		$years = range( date("Y", strtotime("-2 year")), date("Y", strtotime("+2 year")));
		$out = array_combine( $years, $years );
		return $out;
	}

}
