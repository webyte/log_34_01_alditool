<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class Contactperson extends Abstract_controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('contactperson_model', 'model');
        $this->tablename 		= 'contactpersons';
        $this->scriptPath 		= 'manage/contactpersons/';
        $this->recordEditView 	= 'manage/contactpersonsdata_edit';
        $this->headline			= 'Contact Persons';
        $this->infotext			= '';
        $this->showDefaultSearchForm = FALSE;
    }


}

/* End of file supplier.php */
/* Location: ./app/controllers/supplier.php */