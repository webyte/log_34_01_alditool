<?php

class Sqlite_checker extends CI_controller {

	function __construct()
	{
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * init
	 */
	public function init() {
		$sqLitePath = $this->config->item('sqlite_path');
		$sqLiteFile = $sqLitePath.'record_histories_'.ENVIRONMENT.'_'.date('Y-m', strtotime('+1 month')).'.sqlite';
		if( !file_exists( $sqLiteFile ) ) {
			copy( $sqLitePath.'record_histories.sqlite', $sqLiteFile );
			chmod ( $sqLiteFile, 0777);
		}
		if( filesize($sqLiteFile) <= 0 ) unlink($sqLiteFile);
	}

}
/* End of file Sqlite_checker.php */
/* Location: ./app/controllers/Sqlite_checker.php */