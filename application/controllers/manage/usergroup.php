<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class Usergroup extends Abstract_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('usergroup_model', 'model');
		$this->load->model('groupperm_model','group_perms');
		$this->tablename 		= 'user_groups';
		$this->scriptPath 		= 'manage/usergroup/';
		$this->recordEditView 	= 'manage/usergroupdata_edit';
		$this->headline			= 'Usergroups';
		$this->infotext			= '';
		$this->showDefaultSearchForm = FALSE;
		//$this->permissionTables = array('orders','articles','bookings','shipments');
		//$this->output->enable_profiler(TRUE);
	}

	public function edit( $id = 0 ) {
		$query 	= $this->db->get_where( $this->tablename, array('id' => $id) );
		if ($query->num_rows() > 0){
			$recorddata = $query->first_row('array');
			$formaction = $this->scriptPath."updateRecord/".$id;
		} else {
			$recorddata = array();
			$formaction = $this->scriptPath."createNew";
		}	
		$data['form'] 						= $this->tca->getFormArray( $formaction, array( $this->tablename => $recorddata ) );
		$data['field_permissions'] 			= $this->getTableFieldPermissionFormFields( $this->config->item('tables_for_perfieldpermissions'), $id );
		$data['module_permissions'] 		= $this->getModulePermissionFormFields( $id );
		$data['action_permissions'] 		= $this->getActionPermissionFormFields( $this->config->item('acl_guarded_controllers'), $this->config->item('acl_guarded_actions'), $id );
		$data['tables_crud_permissions'] 	= $this->getCrudPermissionFormFields( $this->config->item('tables_with_crud_permissions'), $id );

		$this->load->view( $this->recordEditView, $data );
	}

	public function createNew( ) {
		$insertArray = $this->getInsertArrayFromPostVars();
		$newId = $this->model->createNew( $insertArray[0] );
		$this->saveGroupPermissions( $newId );
		$this->recordList();
	}

	public function updateRecord( $id = 0 ) {
		if( $id > 0 ) $this->saveGroupPermissions( $id );
		parent::updateRecord( $id );
	}

	private function saveGroupPermissions( $groupId ) {
		foreach ( $_POST as $key => $formValue ) {
			if (strpos($key, Acl::$crud_prefix ) > -1 || strpos($key,Acl::$tablefield_prefix) > -1 || strpos($key,Acl::$action_prefix) > -1 || strpos($key,Acl::$controller_prefix) > -1 ) {
				$this->model->group_perms->savePermission($key, $formValue, $groupId);
			}
		}
	}

	private function getTableFieldPermissionFormFields( $tables, $groupId ) {
		$formfields = array();
		foreach( $tables as $tableName ) {
			$tmpTableFieldPerms = $this->model->group_perms->getFieldPermissionsForTable( $tableName, $groupId );
			$tmpFormFields = array();
			foreach ( $tmpTableFieldPerms as $fieldPerms ) {
				$tmpFormFields[] = $this->getTablefieldRadioGroup( $fieldPerms['label'], $fieldPerms['perm_name'], (string)$fieldPerms['perm_value'] );
			}
			$formfields[$tableName] = $tmpFormFields;
		}
		return $formfields;
	}

	private function getModulePermissionFormFields( $groupId ) {
		$formfields = array();
		$moduleperms = $this->model->group_perms->getModulePermissions( $groupId );
		foreach ( $moduleperms as $m ) {
			$formfields[] = $this->getModulePermsCheckbox($m['title'], $m['controller'], $m['perm_value']);
		}
		return $formfields;
	}

	private function getActionPermissionFormFields( $controllers, $actions, $groupId ) {
		$formfields = array();
		foreach( $controllers as $controller ) {
			$actionperms = $this->model->group_perms->getActionPermissionsForController( $controller, $actions, $groupId );
			$tmpFormFields = array();
			foreach ( $actionperms as $a ) {
				$tmpFormFields[] = $this->getModulePermsCheckbox($a['label'], $a['perm_name'], $a['perm_value']);
			}
			if( count( $tmpFormFields ) > 0 ) $formfields[$controller] = $tmpFormFields;
		}
		return $formfields;
	}

	private function getCrudPermissionFormFields( $tables, $groupId ) {
		$formfields = array();
		foreach( $tables as $tableName ) {
			$tmpTableFieldPerms = $this->model->group_perms->getCrudPermissionsForTable( $tableName, $groupId );
			$tmpFormFields = array();
			foreach ( $tmpTableFieldPerms as $crudPerms ) {
				$tmpFormFields[] = $this->getModulePermsCheckbox($crudPerms['label'], $crudPerms['perm_name'], $crudPerms['perm_value']);
			}
			$formfields[$tableName] = $tmpFormFields;
		}
		return $formfields;
	}

	private function getTablefieldRadioGroup( $label, $perm_name, $value ) {
		if(empty($value)) $value = "2";
		$tmpField = array();
		$tmpField['label'] 		= $label;
		$tmpField['edit'] 		= form_radio($perm_name, "1", $value == "1");
		$tmpField['disabled'] 	= form_radio($perm_name, "2", $value == "2");
		$tmpField['hidden'] 	= form_radio($perm_name, "3", $value == "3");
		return $tmpField;
	}

	private function getModulePermsCheckbox($title, $controller, $permvalue) {
		$tmpField 				= array();
		$tmpField['label'] 		= $title.' ('.( strlen( $controller ) > 35 ? substr( $controller, 0, 35 ).' ...' : $controller ).')';
		$tmpField['allowed'] 	= form_hidden($controller, "").form_checkbox($controller, "1", $permvalue == "1");
		return $tmpField;
	}
}

/* End of file usergroup.php */
/* Location: ./app/controllers/usergroup.php */
