<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class Pic extends Abstract_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Pic_model', 'model');
		$this->load->model('User_model', 'user_model');
		$this->tablename 		= 'pics';
		$this->scriptPath 		= 'manage/pic/';
		$this->recordEditView 	= 'manage/picdata_edit';
		$this->headline			= 'Aldi special week PIC';
		$this->infotext			= '';
		$this->showDefaultSearchForm = FALSE;
	}

	/**
	 * @Override
	 */
	public function edit( $id = 0 ) {
		$query 	= $this->db->get_where( $this->tablename, array('id' => $id) );
		if ($query->num_rows() > 0){
			$recorddata = $query->first_row('array');
			$formaction = $this->scriptPath."updateRecord/".$id;
		} else {
			$recorddata = array();
			$formaction = $this->scriptPath."createNew";
		}
		$data['form'] 			= $this->tca->getFormArray( $formaction, array( $this->tablename => $recorddata ) );
		$data['additionalJS'] 	= '<script src="'.base_url().'js/pic.js"></script>';
		$this->load->view($this->recordEditView, $data );
	}


	/**
	 * getUserData
	 * prints json object with user data
	 * @param int - id
	 * @return void
	 */
	public function getUserData( $id = 0 ) {
		$record 	= $this->user_model->getRecord( $id );
		$email 		= ( isset( $record['email'] ) ) ? $record['email'] : '';
		$username 	= ( isset( $record['username'] ) ) ? $record['username'] : '';
		print( json_encode( array( 'email' => $email, 'username' => $username ) ) );
	}
}

/* End of file pic.php */
/* Location: ./app/controllers/pic.php */