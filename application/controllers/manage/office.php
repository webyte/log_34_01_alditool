<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class Office extends Abstract_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('office_model', 'model');
		$this->tablename 		= 'offices';
		$this->scriptPath 		= 'manage/office/';
		$this->recordEditView 	= 'manage/officedata_edit';
		$this->headline			= 'Offices';
		$this->infotext			= '';
		$this->showDefaultSearchForm = FALSE;
	}

	/**
	 * @Override
	 */
	public function recordList() {
		$this->recordListSearchParams = $this->input->post() ? $this->input->post() : array();
		$data = array();
		$data['resulttable'] 	= generateTableOverview( $this->tablename, $this->getRecordactions(), $this->recordListSearchParams, $this->recordListWhere );
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['createnewlink'] 	= $this->acl->hasPermission( Acl::$crud_prefix.$this->tablename.'_new' ) ? $this->scriptPath.'edit' : '';
		$data['additionalJS'] 	= '';
		if( $this->showDefaultSearchForm ) $data['searchform']		= generateDefaultSearchForm( $this->tablename, $this->scriptPath.'recordList', 'start search', $this->recordListSearchParams );
		$this->load->view('general/record_overview', $data );
	}

}

/* End of file office.php */
/* Location: ./app/controllers/office.php */