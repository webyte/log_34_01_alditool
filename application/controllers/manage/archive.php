<?php

class Archive extends CI_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->model('Shipment_model', 'shipment_model');
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * init
	 */
	public function init() {
		//$this->archiveRecords();
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * archiveRecords
	 */
	private function archiveRecords() {

		$contractCsv = '';
		$deliveryCsv = '';
		$shipmentDeliveryCsv = '';
		$shipmentCsv = '';

		$archiveDate 	= date( 'Y-m-d', strtotime( '-183 days', time() ) );
		// contracts
		$contracts 		= $this->contract_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ), 'id ASC' );

		foreach( $contracts as $contract ) {
			$crdateWeek = date( 'W', strtotime( $contract['crdate'] ) );
			$advWeek	= $contract['advertisement_week'];

			if( is_numeric( $advWeek ) ) {

				$tmpAdvWeek = ( strlen( $advWeek ) > 1 ) ? $advWeek : '0'.$advWeek;

				if( $crdateWeek > $advWeek ) {
					$refYear = date( 'Y', strtotime( $contract['crdate'] ) ) + 1;
				} else {
					$refYear = date( 'Y', strtotime( $contract['crdate'] ) );
				}

				$refDate = date( 'Y-m-d', strtotime( $refYear.'W'.$tmpAdvWeek ) );

				if( strtotime( $refDate ) <  strtotime( $archiveDate ) ) {

					$contractCsv .= $this->getCsv( $contract );
					$this->contract_model->updateRecord( $contract['id'], array( 'hidden' => 1, 'deleted'=> 1 ) );

					// deliveries
					$deliveries = $this->delivery_model->getRecordsWhere( array( 'contract_id' => $contract['id'], 'hidden' => 0, 'deleted' => 0 ) );
					foreach( $deliveries as $delivery ) {
						$deliveryCsv .= $this->getCsv( $delivery );
						$this->delivery_model->updateRecord( $delivery['id'], array( 'hidden' => 1, 'deleted'=> 1 ) );

						// shipmentDeliveries
						$shipmentDeliveries = $this->shipment_delivery_model->getRecordsWhere( array( 'delivery_id' => $delivery['id'], 'hidden' => 0, 'deleted' => 0 ) );
						foreach( $shipmentDeliveries as $shipmentDelivery ) {
							$shipmentDeliveryCsv .= $this->getCsv( $shipmentDelivery );
							$this->shipment_delivery_model->updateRecord( $shipmentDelivery['id'], array( 'hidden' => 1, 'deleted'=> 1 ) );

							// shipments
							$shipments = $this->shipment_model->getRecordsWhere( array( 'id' => $shipmentDelivery['shipment_id'], 'hidden' => 0, 'deleted' => 0 ) );
							foreach( $shipments as $shipment ) {
								$shipmentCsv .= $this->getCsv( $shipment );
								$this->shipment_model->updateRecord( $shipment['id'], array( 'hidden' => 1, 'deleted'=> 1 ) );
							}

						}

					}

				}
			}
		}

		if( !empty( $contractCsv ) ) {
			$this->generateArchiveFile( $contractCsv, date('Y-m-d_His').'_contracts.csv' );
			$this->generateArchiveFile( $deliveryCsv, date('Y-m-d_His').'_deliveries.csv' );
			$this->generateArchiveFile( $shipmentDeliveryCsv, date('Y-m-d_His').'_shipments_deliveries.csv' );
			$this->generateArchiveFile( $shipmentCsv, date('Y-m-d_His').'_shipments.csv' );

//			print('<h2>Contracts</h2><br/>');
//			print(nl2br($contractCsv));
//			print('<br/><br/>');
//
//			print('<h2>Deliveries</h2><br/>');
//			print(nl2br($deliveryCsv));
//			print('<br/><br/>');
//
//			print('<h2>Shipment Delivery Relations</h2><br/>');
//			print(nl2br($shipmentDeliveryCsv));
//			print('<br/><br/>');
//
//			print('<h2>Shipments</h2><br/>');
//			print(nl2br($shipmentCsv));
//			print('<br/><br/>');

		}



	}

	private function generateArchiveFile( $data, $file ) {
		$this->load->helper('file');
		write_file( $this->config->item('archive_path').$file, $data );
	}

	private function getCsv( $row ) {
		$tmp = array();
		foreach ( $row as $field ) {
			if( preg_match( '/\\r|\\n|,|"/', $field ) ) {
				$tmp[] = '"'.str_replace( '"', '""', $field ).'"';
			} else {
				$tmp[] = $field;
			}
		}
		return implode(',',$tmp)."\r\n";

	}
}
/* End of file Archive.php */
/* Location: ./app/controllers/Archive.php */