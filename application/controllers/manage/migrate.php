<?php

class Migrate extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(TRUE);
	}

	/**
	 * index
	 */
	public function index() {
	}

	/**
	 * migrate
	 */
	public function do_migrate() {
		$this->migrateSupplier();
//		$this->migrateContainerTareWeight();
//		$this->migrateCaseQuantity();
//		$this->migrateContractType();
//		$this->migrateProductCodeAndDescription();
//		$this->migrateTelexSecondReminderDate();
//		$this->migrateTelexSetDate();
//		$this->migrateActivitiesLabel();
//		$this->migrateSupplierAddress();
//		$this->recalculateContractVolume();
//		$this->recalculateCaseQuantity();
//		$this->migrateShipments();
	}

	private function migrateSupplier()
	{
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Supplier_model', 'supplier_model');
		$path 	= $this->config->item('import_path');
		$handle = fopen( $path.'supplier.csv', "r" );
		$i 		= 0;
		$offset = 1;
		while ( ( $data = fgetcsv( $handle, 0, ";", '"' ) ) !== FALSE) {
			if( $i >= $offset && !empty( $data ) ) {
				$productCode = $data[0];
				$supplierCode = $data[7];
				$records = $this->contract_model->getRecordsWhere( array( 'product_code' => $productCode, 'hidden' => 0, 'deleted' => 0 ) );
				if( count( $records ) > 0 ) {
					$supplierId = $records[0]['supplier'];
					$this->supplier_model->updateRecord($supplierId, array( 'code' =>  $supplierCode ) );
				}
			}
			$i++;
		}
	}

	private function migrateContainerTareWeight()
	{
		$this->load->model('Shipment_model', 'shipment_model');
		$this->load->model('Container_type_model', 'container_type_model');
		$records = $this->shipment_model->getRecordsWhere( array( 'container_type >' => 0, 'container_size >' => 0, 'container_tare_weight >' => '', 'hidden' => 0, 'deleted' => 0 ) );

		foreach( $records as $record ) {
			$containerType = $record['container_type'];
			$containerSize = $record['container_size'];
			$options 	= $this->getTCAgetMultiselectionOptions('pcshipments_container_type');
			$cType 		= ( array_key_exists($containerType, $options) ) ? $options[$containerType] : '';
			$options 	= $this->getTCAgetMultiselectionOptions('pcshipments_container_size');
			$cSize		= ( array_key_exists($containerSize, $options) ) ? $options[$containerSize] : '';
			$rows 		= $this->container_type_model->getRecordsWhere( array( 'container_type'=> $cType, 'container_size'=> $cSize, 'hidden' => 0, 'deleted' => 0 ) );

			if( count( $rows ) ) {
				$tareWeight = $rows[0]['tare_weight'];
				$this->shipment_model->updateRecord($record['id'], array( 'container_tare_weight' =>  $tareWeight ) );
			}
		}
	}

	private function migrateCaseQuantity()
	{

		$records = $this->delivery_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $records as $record ) {
			$caseQuantity = round( $record['case_quantity'] );
			$this->delivery_model->updateRecord($record['id'], array( 'case_quantity' =>  $caseQuantity ) );
		}
	}

	private function migrateShipments() {
		$records = $this->shipment_delivery_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $records as $record ) {

			$shipments = $this->shipment_model->getRecordsWhere( array( 'id' => $record['shipment_id'], 'hidden' => 0, 'deleted' => 0 ) );
			if( count( $shipments )  > 0 ) {
				$shipment = $shipments[0];
				$pcshipments = $this->pcshipment_model->getRecordsWhere( array( 'hbl_hawb' => $record['hbl_hawb'], 'hidden' => 0, 'deleted' => 0 ) );
				if( count( $pcshipments ) <= 0 ) {
					$containerNumber = !empty( $shipment['container_number'] ) ? $shipment['container_number']: '';
					$pcShipmentId = $this->pcshipment_model->createNew(array('hbl_hawb' => $record['hbl_hawb'],'container_number' => $containerNumber));
					$data = array(
						'pcshipment_id' => $pcShipmentId,
						'delivery_id' => $record['delivery_id'],
						'shipment_id' => $record['shipment_id'],
						'hbl_hawb' => $record['hbl_hawb'],
						'container_number' => $containerNumber,
					);
					$this->pcshipment_delivery_model->createNew($data);
				} else {

					$deliveryId 		= $record['delivery_id'];
					$pcShipmentId 		= $pcshipments[0]['id'];
					$shipmentId 		= $record['shipment_id'];
					$containerNumber 	= $pcshipments[0]['container_number'];

					$pcshipmentDeliveries = $this->pcshipment_delivery_model->getRecordsWhere( array( 'delivery_id' => $deliveryId, 'pcshipment_id' => $pcShipmentId, 'shipment_id' => $shipmentId, 'hidden' => 0, 'deleted' => 0 ) );
					if( count( $pcshipmentDeliveries ) <= 0 ) {
						$data = array(
							'pcshipment_id' => $pcShipmentId,
							'delivery_id' => $record['delivery_id'],
							'shipment_id' => $record['shipment_id'],
							'hbl_hawb' => $record['hbl_hawb'],
							'container_number' => $containerNumber,
						);
						$this->pcshipment_delivery_model->createNew($data);
					}
				}
			}
		}

	}

	private function restoreArchivedData() {
		$contracts = $this->contract_model->getRecordsWhere( array( 'contract_type' => 1, 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $contracts as $contract ) {
			print('contract: '.$contract['id'].'<br/>');
			$deliveries 	= $this->delivery_model->getRecordsWhere( array( 'contract_id' => $contract['id'], 'hidden' => 1, 'deleted' => 1 ) );
			foreach( $deliveries as $delivery ) {
				print('delievry: '.$delivery['id'].'<br/>');
				$this->delivery_model->updateRecord( $delivery['id'], array( 'hidden' =>  0, 'deleted' =>  0 ) );
				$shipmentsDeliveries = $this->shipment_delivery_model->getRecordsWhere( array( 'delivery_id' => $delivery['id'], 'hidden' => 1, 'deleted' => 1 ) );
				foreach( $shipmentsDeliveries as $shipmentDelivery ) {
					print('shipmentDelivery: '.$shipmentDelivery['id'].'<br/>');
					$this->shipment_delivery_model->updateRecord($shipmentDelivery['id'], array( 'hidden' =>  0, 'deleted' =>  0 ) );
					$shipments = $this->shipment_model->getRecordsWhere( array( 'id' => $shipmentDelivery['shipment_id'], 'hidden' => 1, 'deleted' => 1 ) );
					foreach( $shipments as $shipment ) {
						print('shipment: '.$shipment['id'].'<br/>');
						$this->shipment_model->updateRecord($shipment['id'], array( 'hidden' =>  0, 'deleted' =>  0 ) );
					}
				}
			}
		}
	}

	private function recalculateContractVolume() {
		$contracts = $this->contract_model->getRecordsWhere( array( 'modified >' => '2014-10-02 00:00:00', 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $contracts as $contract ) {
			$totalCases		= $contract['total_cases'];
			$caseLength		= $contract['case_length'];
			$caseWidth 		= $contract['case_width'];
			$caseHeight		= $contract['case_height'];
			if( $caseLength < 10 ) {
				$caseVolume		= round( $caseLength * $caseWidth * $caseHeight, 6 );
			} else {
				$caseVolume		= round( ( $caseLength / 1000 ) * ( $caseWidth / 1000 ) * ( $caseHeight / 1000 ), 6 ); // convert millimeter in meter
			}
			$volume 		= $caseVolume * $totalCases;

			$data['volume'] 		= $volume;
			$data['case_cubage']	= $caseVolume;
			$this->contract_model->updateRecord( $contract['id'], $data );
			print($contract['id'].', '.$contract['contract_number'].', '.$caseVolume.'<br/>');
		}
	}

	private function recalculateCaseQuantity() {
		$records = $this->shipment_delivery_model->getRecordsWhere( array( 'modified >' => '2014-10-02 00:00:00', 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $records as $record ) {
			$cq = $this->shipment_delivery_model->updateCaseQuantity( $record['shipment_id'], $record['delivery_id'], $record['unit_quantity'] );
			print($record['id'].', '.$record['hbl_hawb'].', '.$cq.'<br/>');
		}
	}

	private function migrateContractType() {
		// UPDATE contracts SET contract_type = 3

		$contracts = $this->contract_model->getRecordsWhere( array( 'contract_type !=' => Contract_model::$CONTRACT_TYPE_SPECIAL_BUYS, 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $contracts as $contract ) {
			$this->contract_model->updateRecord( $contract['id'], array( 'contract_type' =>  Contract_model::$CONTRACT_TYPE_SPECIAL_BUYS ) );
		}
	}

	private function migrateProductCodeAndDescription() {

		// UPDATE deliveries SET deliveries.product_description = (SELECT product_description FROM contracts WHERE contracts.id=deliveries.contract_id)
		// UPDATE deliveries SET deliveries.product_code = (SELECT product_code FROM contracts WHERE contracts.id=deliveries.contract_id)

		$contracts = $this->contract_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $contracts as $contract ) {
			$contractId 	= $contract['id'];
			$productCode 	= $contract['product_code'];
			$productDesc 	= $contract['product_description'];
			$deliveries 	= $this->delivery_model->getRecordsWhere( array( 'contract_id' => $contractId, 'product_code' => '','hidden' => 0, 'deleted' => 0 ) );
			foreach( $deliveries as $delivery ) {
				$this->delivery_model->updateRecord( $delivery['id'], array( 'product_code' =>  $productCode, 'product_description' =>  $productDesc ) );
			}
		}
	}

	private function migrateTelexSecondReminderDate() {
		$this->db->select( '*' );
		$this->db->from( 'activities' );
		$this->db->like( 'record_data_new', substr( $this->lang->line( 'supplier.reminder2.mail.subject' ), 0, 41 ) );
		$this->db->where( array( 'record_type' => 'shipments', 'deleted' => 0, 'hidden' => 0 ) );
		$query = $this->db->get();

		foreach( $query->result_array() as $row ) {
			$shipmentId = $row['record_id'];
			$recordDate = $row['crdate'];
			$shipmentDeliveries = $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'telex_second_reminder_sent' => '0000-00-00 00:00:00', 'hidden' => 0, 'deleted' => 0 ) );

			foreach( $shipmentDeliveries as $shipmentDelivery ) {
				$this->shipment_delivery_model->updateRecord( $shipmentDelivery['id'], array( 'telex_second_reminder_sent' => $recordDate ) );
				print('Activity: '.$row['id'].'<br/>Shipment: '.$shipmentId.'<br/>Date: '.$recordDate.'<br/><br/>');
			}

		}
	}

	private function migrateActivitiesLabel() {
		$this->db->query( 'UPDATE activities SET record_field_label = "Contract/PO number" WHERE record_field_label = "Contract number"' );
	}

	private function migrateSupplierAddress() {

		$this->load->model('Supplier_model', 'supplier_model');
		$this->load->model('Contactperson_model', 'contactperson_model');
		$suppliers = $this->supplier_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $suppliers as $supplier ) {
			$this->contactperson_model->createNew( array( 'supplier_id' => $supplier['id'], 'name' => $supplier['contact_name'], 'email' => $supplier['contact_mail'] ) );
		}
	}

	private function migrateTelexSetDate() {

		$this->db->select( '*' );
		$this->db->from( 'activities' );
		$this->db->where( array( 'record_field' => 'telex_received', 'record_data_old' => 0, 'record_data_new' => 1, 'record_type' => 'shipments', 'deleted' => 0, 'hidden' => 0 ) );
		$query = $this->db->get();

		foreach( $query->result_array() as $row ) {
			$shipmentId = $row['record_id'];
			$recordDate = $row['crdate'];
			$shipmentDeliveries = $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $shipmentId, 'telex_set_date' => '0000-00-00 00:00:00', 'hidden' => 0, 'deleted' => 0 ) );

			foreach( $shipmentDeliveries as $shipmentDelivery ) {
				$this->shipment_delivery_model->updateRecord( $shipmentDelivery['id'], array( 'telex_set_date' => $recordDate ) );
				print('Activity: '.$row['id'].'<br/>Shipment: '.$shipmentId.'<br/>Date: '.$recordDate.'<br/><br/>');
			}
		}
	}

	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}

}
/* End of file migrate.php */
/* Location: ./app/controllers/migrate.php */