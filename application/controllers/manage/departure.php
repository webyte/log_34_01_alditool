<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class Departure extends Abstract_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Departure_model', 'model');
		$this->tablename 		= 'departures';
		$this->scriptPath 		= 'manage/departure/';
		$this->recordEditView 	= 'manage/departuredata_edit';
		$this->headline			= 'Departures';
		$this->infotext			= '';
		$this->showDefaultSearchForm = TRUE;
		$this->overviewFields 	= array(
			array('label' => 'Port Code', 		'fieldname' => 'name', 'type' => ''),
			array('label' => 'Port Name', 		'fieldname' => 'port_name', 'type' => ''),
			array('label' => 'Port Country', 	'fieldname' => 'port_country', 'type' => ''),
			array('label' => 'Cut Off Time', 	'fieldname' => 'cut_off_time', 'type' => ''),
		);

		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * result
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function result( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordList( $searchParams, $ignoreSearchParams );
	}

	/**
	 * @Override
	 */
	public function recordList($searchParams = array(), $ignoreSearchParams = FALSE) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= $this->model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']		= generateCustomSearchForm( $this->tablename, array(), 'manage/departure/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']	= generateCustomOverview( $this->overviewFields, $this->records, $this->getRecordactions() );
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['createnewlink'] 	= $this->acl->hasPermission( Acl::$crud_prefix.$this->tablename.'_new' ) ? $this->scriptPath.'edit' : '';
		$data['additionalJS'] 	= '';

		$this->load->view('general/record_overview', $data );
	}

	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {
		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Name";
		$tmp['input'] = $this->tca->getFormInputById('departures_name', $this->getSearchFormValue('departures_name'), '1');
		$res[] = $tmp;


		return $res;
	}

}

/* End of file departure.php */
/* Location: ./app/controllers/departure.php */