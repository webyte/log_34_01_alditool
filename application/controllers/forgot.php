<?php
class Forgot extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper( 'url' );
	}

	public function index() {
		$email		= (	isset( $_REQUEST["email"] ) ) ? 	$_REQUEST["email"] 	: "";
		$action		= ( isset( $_REQUEST["action"] ) ) ? 	$_REQUEST["action"] 	: "";

		switch ( $action ) {
			case "forgot":
				$this->checkEmail( $email );			
				break;
			
			default:
				$this->load->view( 'login/forgot', array( 'error' => false, 'message' => "" ) );
				break;
		}		
	}

	/**
	 * checkEmail
	 * @param $email
	 * @return
	 */
	private function checkEmail( $email ){           						
		if( $email == '' ) {
			$isError = true;
			$message = $this->lang->line( 'forgot.message.2' );
		} else {
			$query = $this->db->query( "SELECT id, username, password, email FROM users WHERE email='".$this->db->escape_str( $email )."'" );
	
			if( $query->num_rows() > 0 ) {
				$this->welogger->log( "Mail ".$email." exists", WELogger::$LOG_LEVEL_INFO, "Forgot.checkEmail" );
	
				foreach( $query->result_array() as $row ){
					$id 		= $row['id'];
					$username 	= $row['username'];
					$password 	= $row['password'];
					$mail 		= $row['email'];
				}
				
				if( $this->sendMail( $mail, $username, $password ) ){
					$isError = false;
					$message = $this->lang->line( 'forgot.message.3' );		
				} else {
					$isError = true;
					$message = $this->lang->line( 'forgot.message.4' );	
				}

				// reset pw_set_date
				$this->db->query( "UPDATE users SET pw_set_date = '0000-00-00 00:00:00' WHERE id=".$id );

			} else {
				$this->welogger->log( "Mail ".$email." does not exist", WELogger::$LOG_LEVEL_WARNING, "Forgot.checkEmail" );
	
				$isError = true;
				$message = $this->lang->line( 'forgot.message.5' );
			}		
		}
		
		$this->load->view( 'login/forgot', array( 'error' => $isError, 'message' => $message ) );

	}

	/**
	 * sendMail
	 * @param $mail
	 * @param $username
	 * @param $password
	 * @return boolean
	 */
	private function sendMail( $mail, $username, $password ) {		
		$this->welogger->log( "sendMail called with mail: ".$mail.", username: ".$username, WELogger::$LOG_LEVEL_INFO, "Forgot.sendMail" );
		
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$mailData['USERNAME'] 		= $username;
		$mailData['PASSWORD'] 		= $password;
		$fromMail 					= $this->config->item('from_mail');
		$fromName 					= $this->config->item('from_name' );
		$subject 					= $this->lang->line( 'forgot.mail.subject' );
		$mailText 					= $this->lang->line( 'forgot.mail.body' );
		$toMail 					= $mail;
		$body						= replacePattern( $mailText, $mailData );

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( "Sending Mail to ".$toMail." failed, error: ".$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, "Forgot.sendMail" );
			return false;
		} else {
			$this->welogger->log( "Mail was sent to ".$toMail." \nwith subject: ".$subject." \nand message: ".$body, WELogger::$LOG_LEVEL_INFO, "Forgot.sendMail" );
			return true;
		}
		$this->email->clear(TRUE);
	}

}

/* End of file forgot.php */
/* Location: ./app/controllers/forgot.php */