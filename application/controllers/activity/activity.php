<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Activity extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Activity_model', 'model');
		$this->tablename 			= 'recorddata_history';
		$this->scriptPath 			= 'activity/activity/';
		$this->recordEditView 		= '';
		$this->headline				= 'Activity log';
		$this->infotext				= '';
		$this->showDefaultSearchForm = FALSE;
		$this->overviewFields 	= array(
			array('label' => 'Contract/PO number', 	'fieldname' => 'contract_number', 	'type' => ''),
			array('label' => 'Action', 				'fieldname' => 'action', 			'type' => ''),
			array('label' => 'User name', 			'fieldname' => 'username', 			'type' => ''),
			array('label' => 'Date', 				'fieldname' => 'crdate', 			'type' => ''),
			array('label' => 'Action type', 		'fieldname' => 'action_type', 		'type' => ''),
		);
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {

		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']			= generateCustomSearchForm( $this->tablename, array(), 'activity/activity/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']		= '';
		$data['headline'] 			= $this->headline;
		$data['infotext'] 			= $this->infotext;
		$data['createnewlink'] 		= '';
		$data['additionalJS'] 		= '';
		$this->load->view('general/record_overview', $data );
	}

	/**
	 * @Override
	 */
	public function result( $searchParams = array(), $ignoreSearchParams = FALSE ) {

        $this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
        $this->records					= $this->model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );
        $searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']			= generateCustomSearchForm( $this->tablename, array(), 'activity/activity/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']		= generateCustomOverview( $this->overviewFields, $this->records, array(), 'activity/overview_table' );
		$data['headline'] 			= $this->headline;
		$data['infotext'] 			= $this->infotext;
		$data['createnewlink'] 		= '';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/activity.js"></script>';

		$this->load->view('activity/record_overview', $data );
	}

	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {
		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = '<input type="text" id="shipments_container_number" value="'.$this->getSearchFormValue('shipments_container_number').'" name="shipments_container_number[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="activities_from" value="'.$this->getSearchFormValue('activities_from').'" name="activities_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="activities_to" value="'.$this->getSearchFormValue('activities_to').'" name="activities_to[]">';
		$res[] = $tmp;

		return $res;
	}


	/**
	 * csv_report_export
	 * @return
	 */
	public function csv_report_export(){
		$recordListSearchParams 	= $this->input->post();
		$records					= $this->model->getOverviewRecords( $recordListSearchParams );

		$data = array(
			'record_data_old' => 'Old value',
			'record_data_new' => 'New value',
			'action_type' => 'Action type',
			'username' => 'User name',
			'crdate' => 'Date',
		);

		$out 		= "";
		$delimiter	= ";";
		$enclosure	= '"';
		$newline 	= "\r\n";


		foreach ( $records as $rows ) {
			// header
			$out .= $enclosure . $rows[0]['record_type_label'].' / # '.$rows[0]['contract_number'].' '.$rows[0]['container_number'] . $enclosure . $delimiter;
			foreach ($data as $key => $val) {
				$out .= $enclosure. $val . $enclosure . $delimiter;
			}
			$out .= $newline;

			// body
			foreach ($rows as $row) {
				$out .= $enclosure . $row['record_field_label'] . $enclosure . $delimiter;
				foreach ($data as $key => $val) {
					$out .= $enclosure . $row[$key] . $enclosure . $delimiter;
				}
				$out .= $newline;
			}
			$out .= '"";"";"";"";"";"";';

		}
		$this->output->set_header('Content-Type: text/x-csv');
		$this->output->set_header('Content-Disposition: attachment; filename=logwin_aldi_activity.csv');
		$this->output->set_header('Cache-Control: max-age=0');
		$this->output->set_output( utf8_decode( $out ) );
//		var_dump($out);
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}

/* End of file report.php */
/* Location: ./app/controllers/report.php */