<?php
require_once( APPPATH.'controllers/abstract_aldi_controller'.EXT );

/**
 * abstract_admin
 * 
 * base class for table administration.
 * provides generic methods for overview of records, edit, update
 * and delete options.
 * With simple tables it is enough to subclass this class, set
 * propper class-member values to get overview-table + edit, delete options.
 * It is recommended to have a seperate "model"-class for the table
 * this controller belongs to. To make better separation of controller 
 * functionality and data-handling.
 * 
 */
class Abstract_controller extends Abstract_aldi_controller {
	
	protected $tablename;
	protected $scriptPath;
	protected $recordEditView;
	protected $headline;
	protected $infotext;
	protected $recordListWhere;
	protected $recordListSearchParams;
	protected $showDefaultSearchForm;
	
	function __construct() {
		parent::__construct();
		$this->load->helper( 'tableoverview_helper' );
		$this->load->helper( 'url' );
		$this->load->library( 'tca' );

		$this->recordListWhere 	= "";
		$this->recordListSearchParams = array();
		$this->showDefaultSearchForm = TRUE;
	}

	/**
	 * index
	 * 
	 */
	function index() {
		$this->recordList();
	}

	/**
	 * recordList
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 * 
	 */
	public function recordList() {
		$this->recordListSearchParams = $this->input->post() ? $this->input->post() : array();

		$data = array();
		$data['resulttable'] 	= generateTableOverview( $this->tablename, $this->getRecordactions(), $this->recordListSearchParams, $this->recordListWhere );
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['createnewlink'] 	= $this->scriptPath.'edit';
		$data['additionalJS'] 	= '';
		if( $this->showDefaultSearchForm ) $data['searchform'] = generateDefaultSearchForm( $this->tablename, $this->scriptPath.'recordList', 'start search', $this->recordListSearchParams );
		$this->load->view('general/record_overview', $data );
	}

	/**
	 * edit
	 * default edit implementation. 
	 * 
	 * @param int $id - id of record to edit. If id is 0 a new record is created.
	 */
	function edit( $id = 0 ) {
		$query 	= $this->db->get_where( $this->tablename, array('id' => $id) );
		if ($query->num_rows() > 0){
			$recorddata = $query->first_row('array');
			$formaction = $this->scriptPath."updateRecord/".$id;
		} else {
			$recorddata = array();
			$formaction = $this->scriptPath."createNew";
		}	
		$data['form'] = $this->tca->getFormArray( $formaction, array( $this->tablename => $recorddata ) );
		$this->load->view($this->recordEditView, $data );
	}

	/**
	 * delete
	 * default delete implementation. simply deletes the record
	 * with the given id from db-table.
	 * 
	 * @param int $id - id of record to delete
	 */
	function delete( $id ) {
		$this->model->deleteRecord( $id );
		$this->recordList();
	}

	/**
	 * createNew
	 * default createnew implementation. simply creates new record
	 * with the given post-data from edit-form.
	 * 
	 */
	function createNew( ) {
		$insertArray 	= $this->getInsertArrayFromPostVars();
		$this->model->createNew( $insertArray[0] ); // by default we only take the first pack of values (it is possible, that insertArray contains more than one record)
		$this->recordList();
	}

	/**
	 * updateRecord
	 * default update-record implementation. simply updates the record
	 * with the given id with the post-data from edit-form.
	 * 
	 * @param int $id - id of record to update
	 */
	function updateRecord( $id = 0 ) {
		$insertArray 	= $this->getInsertArrayFromPostVars();
		//var_dump($insertArray);
		if( $id == 0 ) $this->createNew();
		else $this->model->updateRecord( $id, $insertArray[0] ); // by default we only take the firs pack of values (it is possible, that insertArray contains more than one record)
		$this->recordList();
	}

	/**
	 * getRecordactions
	 * returns array with all actions the user is allowed to perform 
	 * with the record in overview. a record action is an associative array
	 * with 3 values:
	 * controller : the controller call to the specific action
	 * icon : css class for the icon to display
	 * title : html-title
	 * 
	 * @return array recordactions
	 */
	protected function getRecordactions( $table = '' ) {
		$recordactions 		= array();
		$table				= $table == '' ? $this->tablename : $table;
		$edit_perm_name 	= Acl::$crud_prefix.$table.'_edit';
		$delete_perm_name 	= Acl::$crud_prefix.$table.'_delete';

		if( $this->acl->hasPermission( $edit_perm_name ) ) 		$recordactions[] 	= array('controller' => $this->scriptPath.'edit', 'icon' => 'icon-pencil', 'title' => "edit" );
		if( $this->acl->hasPermission( $delete_perm_name ) ) 	$recordactions[] 	= array('controller' => $this->scriptPath.'delete', 'icon' => 'icon-trash', 'title' => "delete" );
		return $recordactions;
	}

	/**
	 * getInsertArrayFromPostVars
	 * prepare values from edit-form for use in model.
	 * The values from the edit-form get cleaned up, (eg. date and number values are formated the right way)
	 * column_ids from the form are resolved to the real table fieldnames and an associative array is generated 
	 * in the form array( 'dbtable_fieldname' => 'value', ... ) for all fields that belong to the models table.
	 * 
	 * @param tableName - optional parameter, to get the formvalues of another table, then the one, this controller belongs to
	 * @return array associative array with key=>value pairs for db actions
	 */
	protected function getInsertArrayFromPostVars( $tableName = '' ) {
		$table 			= !empty($tableName) ? $tableName : $this->tablename;
		$insertdata 	= $this->tca->getFieldValuesForDBquery( $_POST );
		$recorddata 	= isset( $insertdata[ $table ] ) ? $insertdata[ $table ] : array();
		$insertArray 	= array();
		foreach ( $recorddata as $field ) {
			$values = $field['value'];
			for ( $index = 0, $max_count = sizeof( $values ); $index < $max_count; $index++ ) {
				if( count($insertArray) < $index+1 ) $insertArray[] = array();
				$tmpArray = &$insertArray[$index];
				if( $table != $this->tablename ) {
					$tmpArray[ $field['name'] ] = $values[ $index ];
				} else {
					if( $field['name'] != 'id' ) $tmpArray[ $field['name'] ] = $values[ $index ]; // only leave out 'id' when we retrieve the values for the "native" tabel.	
				}
			}
       		
		}
		return $insertArray;
	}

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */