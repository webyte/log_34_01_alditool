<?php
class Combine_contract_special_cron extends CI_Controller {

	public $importSubPath;
	function __construct() {
		parent::__construct();
		$this->importSubPath = 'contract/special_raw/';
		$this->exportSubPath = 'contract/special/';
		//$this->output->enable_profiler(TRUE);
	}

	public function init() {
		$this->log("init called, environment ".ENVIRONMENT);
		$this->checkForNewFiles();
	}

	private function checkForNewFiles() {
		$path 		= $this->config->item('import_path').$this->importSubPath;
		$excludes 	= array( ".", "..", ".DS_Store", ".svn", ".gitkeep");
		$lines		= array();
		$headline	= '';

		if ( is_dir( $path ) ) {
			if ( $handle = opendir( $path ) ) {
				while ( ( $file = readdir( $handle ) ) !== false ) {
					if ( !in_array( $file, $excludes ) ) {
						$headline 	= $this->readLine( 0, $file );
						$line 		= $this->readLine( 1, $file );
						$lines[] 	= $line;

						// move file to archive
						if( ENVIRONMENT == 'production' || ENVIRONMENT == 'staging' ) $this->moveFileToArchive( $file );
					}
				}
				closedir( $handle );
				if( count( $lines ) > 0 ) {
					array_unshift($lines , $headline);
					$this->export( $lines );
				}
			} else {
				$this->welogger->log( "Directory ".$path." can not be opened", WELogger::$LOG_LEVEL_ERROR, "Combine_contract_special_cron.checkForNewEventFiles" );
			}
		} else {
			$this->welogger->log( "Directory ".$path." does not exist", WELogger::$LOG_LEVEL_ERROR, "Combine_contract_special_cron.checkForNewEventFiles" );
		}


	}

	/**
	 * readLine
	 * @param $lineNum
	 * @param $file
	 * @return
	 */
	private function readLine( $lineNum, $file ) {
		$this->welogger->log( "readLine called with file ".$file, WELogger::$LOG_LEVEL_INFO, "Combine_contract_special_cron.readEventFile" );
		$this->log('readLine called with file '.$file);

		$path 		= $this->config->item('import_path').$this->importSubPath;
		$lines 		= file($path.$file );
		$out 		= trim( $lines[$lineNum] );

		return $out;
	}

	/**
	 * export
	 * @param $lineNum
	 * @param $file
	 * @return
	 */
	private function export( $lines ) {
		$path = $this->config->item('import_path').$this->exportSubPath;
		$file = $path.'contract_special_'.date( 'YmdHis' ).'.csv';
		$this->log('export called with file '.$file);
		file_put_contents( $file, implode( "\r\n", $lines ) );
	}

	/**
	 * moveFileToArchive
	 * @param $file
	 * @return
	 */
	private function moveFileToArchive( $file ) {
		$this->welogger->log( "moveFileToArchive called with file ".$file, WELogger::$LOG_LEVEL_INFO, "Combine_contract_special_cron.moveFileToArchive" );
		$this->log('moveFileToArchive called with file '.$file);

		$uploadPath 	= $this->config->item('import_path').$this->importSubPath;
		$archivePath 	= $this->config->item('archive_path').date('Y-m-d').'/';

		if( !file_exists( $archivePath ) ) mkdir( $archivePath, 0777 );

		// get new filename if alreday exist in archive
		$fileName = $this->getFileName( $file, $archivePath );
		// change file rights
		chmod ( $uploadPath.$fileName, 0777);
		// copy to archive folder
		copy( $uploadPath.$file, $archivePath.$fileName ) or $this->welogger->log( "Couldn't copy file ".$file." to archive ".$archivePath, WELogger::$LOG_LEVEL_ERROR, "Combine_contract_special_cron.moveFileToArchive" );
		// change file rights
		chmod ( $archivePath.$fileName, 0777);
		// delete original file
		unlink( $uploadPath.$file ) or $this->welogger->log( "Couldn't delete file ".$file." from upload ".$uploadPath, WELogger::$LOG_LEVEL_ERROR, "Combine_contract_special_cron.moveFileToArchive" );
	}

	/**
	 * log
	 * @param $msg
	 * @return
	 */
	private function log( $msg ) {
		print( date('Y-m-d H:i:s').': '.$msg."\n");
	}

	/**
	 * getFileName
	 * @param $file
	 * @return string
	 */
	private function getFileName( $file, $archivePath ) {
		// check if file already exists in archive
		if( file_exists( $archivePath.$file ) ) {
			$str = basename( $file,".xml" ).'_'.time().'.xml';
			$this->welogger->log( "file already exists new name is ".$str, WELogger::$LOG_LEVEL_INFO, "Event_import.getFileName" );
			return $str;
		} else {
			return $file;
		}
	}
}

/* End of file Combine_contract_special_cron.php */
/* Location: ./app/controllers/Combine_contract_special_cron.php */