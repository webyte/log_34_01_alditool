<?php
class Booking_received_cron extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Office_model', 'office_model');
		$this->load->helper( 'str_helper' );

		//$this->output->enable_profiler(TRUE);
	}

	public function init() {
		$today 		= date( 'Y-m-d' );
		$fobDate 	= date( 'Y-m-d', strtotime( '22 days', time() ) );
		$contracts 	= $this->contract_model->getRecordsWhere( array( 'fob_date <=' => $fobDate, 'fob_date >=' => $today, 'hidden' => 0, 'deleted' => 0 ), 'id ASC' );
		foreach( $contracts as $contract ) {

			if( $contract['ofu_real'] == '0000-00-00 00:00:00' && $contract['no_booking_received'] == 0) {

				$mailData['CONTRACT_NUMBER'] 	= $contract['contract_number'];
				$mailData['ON_SALE_WEEK'] 		= $contract['advertisement_week'];
				$data 				= array();
				$data['subject'] 	= replacePattern( $this->lang->line( 'booking_received.reminder.mail.subject' ), $mailData );
				$data['body'] 		= replacePattern( $this->lang->line( 'booking_received.reminder.mail.body' ), $mailData );;
				$data['recipient'] 	= ( ENVIRONMENT == 'production' ) ? $this->getLooMail( $contract['logwin_origin_office'] ) : array('or@webyte.org','Stefanie.Stahl@logwin-logistics.com');
				$data['cc'] 		= array();//array('or@webyte.org');
				if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
			}
		}
	}

	/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$cc			= ( isset( $data['cc'] ) ) ? $data['cc'] : '';
		$subject 	= $data['subject'];
		$body 		= $data['body'];

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		if( !empty( $cc ) ) $this->email->cc( $cc );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Booking_received_cron.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Booking_received_cron.sendMail' );
			return true;
		}
		$this->email->clear(TRUE);
	}

	private function getLooMail( $id = 0 ) {
		$out = '';
		$record = $this->office_model->getRecord( $id );
		if( count( $record ) > 0 ) {
			if( !empty( $record['contact_mail'] ) ) $out = $record['contact_mail'];
		}
		return $out;
	}

}

/* End of file Booking_received_cron.php */
/* Location: ./app/controllers/booking_received_cron.php */