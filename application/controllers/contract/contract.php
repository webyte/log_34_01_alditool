<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Contract extends Abstract_controller {

    public $contactperson_model;


	function __construct() {
		parent::__construct();
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->model('Contract_model', 'model');


		$this->load->model('Activity_model', 'activity_model');
		$this->load->helper( 'str_helper' );

		$this->tablename 			= 'contracts';
		$this->scriptPath 			= 'contract/contract/';
		$this->recordEditView 		= 'contract/contractdata_edit';
		$this->headline				= 'Contracts';
		$this->infotext				= '';
		$this->showDefaultSearchForm = FALSE;
		$this->overviewFields 	= array(
			array('label' => 'Traffic type', 		'fieldname' => 'traffic_type', 			'type' => ''),
			array('label' => 'Contract/PO number', 	'fieldname' => 'contract_number', 		'type' => ''),
			array('label' => 'Product code', 		'fieldname' => 'product_code', 			'type' => ''),
			array('label' => 'Supplier', 			'fieldname' => 'supplier', 				'type' => ''),
			array('label' => 'Logwin origin office', 'fieldname' => 'logwin_origin_office', 'type' => ''),
			array('label' => 'Departure', 			'fieldname' => 'departure', 			'type' => ''),
			array('label' => 'On-Sale week', 		'fieldname' => 'advertisement_week', 	'type' => ''),
			array('label' => 'FOB date', 			'fieldname' => 'fob_date', 				'type' => 'date'),
			array('label' => 'ALDI AU WH delivery due date', 'fieldname' => 'aldi_au_wh_delivery_due_date', 'type' => 'date'),
			array('label' => 'Cube QA check', 		'fieldname' => 'cube_qa_check', 		'type' => ''),
			array('label' => 'Freight order no', 	'fieldname' => 'freight_order_number', 	'type' => ''),
			array('label' => 'Pickup point', 	    'fieldname' => 'pickup_point', 			'type' => ''),
			array('label' => 'Contract type', 	    'fieldname' => 'contract_type', 		'type' => ''),
		);

		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function edit( $id = 0 ) {

		$record 		= $this->model->getRecord( $id );
		if( count( $record ) > 0 ){
			$recorddata = $record;
			$formaction = $this->scriptPath."updateRecord/".$id;
		} else {
			$recorddata = array();
			$recorddata['crdate'] = date( 'Y-m-d H:i:s' );
			$formaction = $this->scriptPath."createNew";
		}

		$data['form'] 				= $this->tca->getFormArray( $formaction, array( $this->tablename => $recorddata ) );
		$data['headline'] 			= $this->headline;
		$data['newDeliveryPerms']	= $this->acl->hasPermission( Acl::$crud_prefix.'deliveries_new' );
		$data['sendMailPerms']		= $this->acl->hasPermission( Acl::$action_prefix.'contract/contract_sendMail' );
		$data['isCubeQACheckDisabled'] = $this->isCubeQACheckDisabled( $id );
		$data['additionalJS'] 		= '<script src="'.base_url().'js/contract.js"></script>';
		$data['supplierMail']		= array();
		$data['looMail']			= array();
		$data['supplierRecipients']	= array();

		if( isset( $recorddata['supplier'] ) ) {
			$this->load->model('Contactperson_model', 'contactperson_model');
			$contactPersons = $this->contactperson_model->getRecordsWhere( array( 'supplier_id' => $recorddata['supplier'], 'hidden' => 0, 'deleted' => 0 ) );
			$data['supplierRecipients']	= $contactPersons;
			$data['supplierMail']		= $this->getSupplierInformMailData( $recorddata );
			$data['looMail']			= $this->getLooInformMailData( $recorddata );
		}

		// load view for imported contracts
		$this->load->view( $this->recordEditView, $data );
	}

	/**
	 * recordList
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data 					= array();
		$data['searchform']		= generateCustomSearchForm( $this->tablename, array(), 'contract/contract/result', $searchFormFields, 'Go', $this->recordListSearchParams );
        $data['resulttable']	= '';
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['createnewlink'] 	= $this->acl->hasPermission( Acl::$crud_prefix.$this->tablename.'_new' ) ? $this->scriptPath.'edit' : '';
		$data['additionalJS'] 	= '<script src="'.base_url().'js/contract.js"></script>';
		$this->load->view('contract/record_overview', $data );
	}

	/**
	 * recordList
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function result( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->recordListSearchParams['contracts_modified'] = array( date( 'Y-m-d 00:00:00', strtotime( '-183 days', time() ) ) );
		$this->records					= $this->model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data 					= array();
		$data['searchform']		= generateCustomSearchForm( $this->tablename, array(), 'contract/contract/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']	= generateCustomOverview( $this->overviewFields, $this->records, $this->getRecordactions() );
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['createnewlink'] 	= $this->acl->hasPermission( Acl::$crud_prefix.$this->tablename.'_new' ) ? $this->scriptPath.'edit' : '';
		$data['additionalJS'] 	= '<script src="'.base_url().'js/contract.js"></script>';
		$this->load->view('contract/record_overview', $data );

	}

	/**
	 * @Override
	 */
	public function createNew() {
		$id = $this->saveUserInput();
		redirect( $this->scriptPath.'edit/'.$id );
		//redirect( $this->scriptPath );
	}

	/**
	 * @Override
	 */
	public function updateRecord( $id = 0 ) {
		if( $id == 0 ) {
			$id = $this->saveUserInput( $id );
			redirect( $this->scriptPath.'edit/'.$id );
		} else {
			$id = $this->saveUserInput( $id );
			redirect( $this->scriptPath.'edit/'.$id );
		}
	}

	/**
	 * @Override
	 */
	public function delete( $id ) {
		$contract = $this->model->getRecord( $id );

		$this->model->deleteRecord( $id );
		$this->delivery_model->deleteRecordsByParentId( $id, 'contract_id' );

		$logData['id'] 			= $contract['id'];
		$logData['warning'] 	= 'Contract was deleted!';
		$this->activity_model->logChanges( $logData, array(), $contract['contract_number'], Activity_model::$DATA_TYPE_CONTRACT, Activity_model::$ACTION_CONTRACT_DELETED );

		$this->recordList();
	}

	/**
	 * getNewDeliveryForm
	 * returns a new html-form for a delivery record to be inserted
	 * in the frontend dynamically.
	 */
	public function getNewDeliveryForm() {
		$data 			= array();
		$data['form'] 	= $this->tca->getFormArray( "" , array( 'deliveries' => array() ) );
		$data['deletePerms'] = $this->acl->hasPermission( Acl::$crud_prefix.'deliveries_delete' );
		$res = $this->load->view('contract/deliverydata_edit', $data, true );
		print( $res );
	}

	/**
	 * sendMail
	 * @return void
	 */
	public function sendMail() {
		$id 		= $this->input->post('id');
		$type 		= $this->input->post('type');
		$subject 	= utf8_encode( $this->input->post('subject') );
		$body 		= utf8_encode( $this->input->post('body') );
        $recipients = $this->input->post('recipients');

		$logData	= array();
		if( $id > 0 && !empty( $type ) ) {
			$contract = $this->model->getRecord( $id );
			if( $type == '1' ) {
				$this->load->model('Supplier_model', 'supplier_model');
				$this->load->model('Office_model', 'office_model');
				$this->load->model('Contactperson_model', 'contactperson_model');
				$this->load->helper( 'str_helper' );
				$office 	= $this->office_model->getRecord( $contract['logwin_origin_office'] );
				$looMail 	= $office['contact_mail'];
				$emails		= array();

				foreach( $recipients as $recipientId ) {
					$contactPerson 	= $this->contactperson_model->getRecord( $recipientId );
					$emails[] 	= $contactPerson['email'];
				}

				$data 				= array();
				$data['subject'] 	= $subject;
				$data['recipient'] 	= $emails;
				$data['cc'] 		= $looMail;
				$data['body'] 		= $body;
				$this->doSendMail( $data );

				// log data
				$logData['id'] 			= $contract['id'];
				$logData['subject'] 	= $subject;
				$logData['recipient'] 	= implode(', ',$emails);
				$logData['mail_type'] 	= 'Supplier email';
				$this->activity_model->logChanges( $logData, array(), $contract['contract_number'], Activity_model::$DATA_TYPE_CONTRACT );

				$this->model->updateRecord( $id, array( 'supplier_mail_sent' => 1 ) );
			}
		}
	}

	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {
		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Departure Port";
		$tmp['input'] = $this->tca->getFormInputById('contracts_departure', $this->getSearchFormValue('contracts_departure'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Traffic type";
		$tmp['input'] = form_dropdown( 'contracts_traffic_type[]', array( "" => "please select ...", "1" => "Factory", "5" => "Factory/CFS", "2" => "CFS", "3" => "AIR", "4" => "PTS Loading" ), $this->getSearchFormValue('contracts_traffic_type') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-Sale week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="contracts_from" value="'.$this->getSearchFormValue('contracts_from').'" name="contracts_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="contracts_to" value="'.$this->getSearchFormValue('contracts_to').'" name="contracts_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Logwin origin office";
		$tmp['input'] = $this->tca->getFormInputById('contracts_logwin_origin_office', $this->getSearchFormValue('contracts_logwin_origin_office'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Supplier";
		$tmp['input'] = $this->tca->getFormInputById('contracts_supplier', $this->getSearchFormValue('contracts_supplier'), '1');
		$res[] = $tmp;

        $tmp['prefield_markup'] = "";
        $tmp['label'] = "Freight order no";
        $tmp['input'] = $this->tca->getFormInputById('contracts_freight_order_number', $this->getSearchFormValue('contracts_freight_order_number'), '1');
        $res[] = $tmp;

        $tmp['prefield_markup'] = "";
        $tmp['label'] = "Pickup point";
        $tmp['input'] = $this->tca->getFormInputById('contracts_pickup_point', $this->getSearchFormValue('contracts_pickup_point'), '1');
        $res[] = $tmp;

        $tmp['prefield_markup'] = "";
        $tmp['label'] = "Contract type";
        $tmp['input'] = $this->tca->getFormInputById('contracts_contract_type', $this->getSearchFormValue('contracts_contract_type'), '1');
        $res[] = $tmp;

		return $res;
	}

	/**
	 * getProductDescription
	 * returns the product description depending on the product code
	 */
	public function getProductDescription( $productCode = '' ) {
		if( count( $productCode ) > 0 ) {
			$this->load->model('Product_model', 'product_model');
			$desc = $this->product_model->getProductDescriptionByProductCode( $productCode );
			print( json_encode( array( 'desc' => $desc ) ) );
		}
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * saveUserInput
	 * general function for saving user input
	 * @param int $id - id of record to edit. If id is 0 a new record is created.
	 */
	private function saveUserInput( $id = 0 ) {
		$id = $this->saveUserInputForImportedContracts( $id );
		return $id;
	}

	/**
	 * saveUserInputForImportedContracts
	 * general function for saving user input
	 * @param int $id - id of record to edit. If id is 0 a new record is created.
	 */
	private function saveUserInputForImportedContracts( $id = 0 ) {
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');

		// previous contract
		$previousContract = array();
		if( $id > 0 ) $previousContract = $this->model->getRecord( $id );

		// contract record
		$insertArray 	= $this->getInsertArrayFromPostVars();
		$firstContract 	= $insertArray[0];

		// recalculate
		$totalCases		= $firstContract['total_cases'];
		$caseLength		= $firstContract['case_length'];
		$caseWidth 		= $firstContract['case_width'];
		$caseHeight		= $firstContract['case_height'];
		$caseVolume		= ( $caseLength / 1000 ) * ( $caseWidth / 1000 ) * ( $caseHeight / 1000 ); // convert millimeter in meter
//		if( strtotime( $firstContract['crdate'] ) > strtotime( '2014-10-02 00:00:00' ) ) {
//			$caseVolume		= ( $caseLength / 1000 ) * ( $caseWidth / 1000 ) * ( $caseHeight / 1000 ); // convert millimeter in meter
//		} else {
//			$caseVolume		= $caseLength * $caseWidth * $caseHeight;
//		}
		$caseWeight		= $firstContract['case_weight'];
		$volume 		= $caseVolume * $totalCases;
		$grossWeight 	= $caseWeight * $totalCases;
		$unitsPerCase	= $firstContract['units_per_case'];

		$firstContract['volume'] 		= $volume;
		$firstContract['gross_weight'] 	= $grossWeight;
		$firstContract['case_cubage']	= $caseVolume;

		if( empty( $firstContract['actual_cargo_ready_date'] ) ) $firstContract['actual_cargo_ready_date'] = $firstContract['actual_contract_ready_date'];

		// check if logwin origin office has changed and send mail
		if( isset( $firstContract['logwin_origin_office'] ) ) $this->checkIfLooHasChanged( $id, $firstContract );

		if( $id == 0 ) {
			$id = $this->model->createNew( $firstContract );
		} else {
			$this->model->updateRecord( $id, $firstContract );
		}
		$firstContract['id'] = $id;

		// process events
		$latestContract = $this->model->getRecord( $id );
		$this->model->processEventDates( $latestContract, $previousContract );

		// delivery record
		$deliveryRecordsInsertArray = $this->getInsertArrayFromPostVars('deliveries');
		foreach ( $deliveryRecordsInsertArray as $deliveryInsertArray ) {
			$deliveryId 								= $deliveryInsertArray['id'];

			// previous delivery
			$previousDelivery = array();
			if( $deliveryId > 0 ) $previousDelivery 	= $this->delivery_model->getRecord( $deliveryId );

			// calculate and set data
			if( isset( $deliveryInsertArray['unit_quantity'] ) ) {
				$caseQuantity 							= ceil( $deliveryInsertArray['unit_quantity'] / $unitsPerCase );
				$deliveryInsertArray['case_quantity'] 	= $caseQuantity;
				$deliveryInsertArray['gross_weight'] 	= $caseQuantity * $caseWeight;
				$deliveryInsertArray['volume'] 			= $caseQuantity * $caseVolume;
			}
			$deliveryInsertArray['contract_id'] 		= $id;
			$deliveryInsertArray['advertisement_week'] 	= $firstContract['advertisement_week'];
			$deliveryInsertArray['contract_number'] 	= $firstContract['contract_number'];
			if( isset( $firstContract['contract_type'] ) ) $deliveryInsertArray['contract_type'] = $firstContract['contract_type'];
			$deliveryInsertArray['product_code'] 		= $firstContract['product_code'];
			$deliveryInsertArray['product_description'] = $firstContract['product_description'];
			// save delivery
			if( !empty( $deliveryId ) ){
				unset( $deliveryInsertArray['id'] );
				$this->delivery_model->updateRecord( $deliveryId, $deliveryInsertArray );
			} else {
				$deliveryId = $this->delivery_model->createNew( $deliveryInsertArray );
			}
			$deliveryInsertArray['id'] 					= $deliveryId;

			// set remaining unit quantity
			$remainQuantity = $this->shipment_delivery_model->getRemainingUnitQuantityByDeliveryId( $deliveryId );
			$this->delivery_model->updateRecord( $deliveryId, array( 'unit_quantity_remain' => $remainQuantity ) );

			// set remaining case quantity
			$remainQuantity = $this->shipment_delivery_model->getRemainingCaseQuantityByDeliveryId( $deliveryId );
			$this->delivery_model->updateRecord( $deliveryId, array( 'case_quantity_remain' => $remainQuantity ) );

			// log delivery changes
			$this->activity_model->logChanges( $deliveryInsertArray, $previousDelivery, $firstContract['contract_number'], Activity_model::$DATA_TYPE_DELIVERY );
		}

		// no_booking_received
		if( $latestContract['no_booking_received'] == 1 && $previousContract['no_booking_received'] == 0 ) $this->model->processStatusXMLExport( $id, 'no_booking_received' );

		// log contract changes
		$this->activity_model->logChanges( $firstContract, $previousContract, $firstContract['contract_number'], Activity_model::$DATA_TYPE_CONTRACT );

		return $id;
	}

	/**
	 * doSendMail
	 * @return boolean
	 */
	private function doSendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= ( ENVIRONMENT == 'production' ) ? $data['recipient'] : array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
		$cc			= ( ENVIRONMENT == 'production' ) ? $data['cc'] : '';

		$subject 	= $data['subject'];
		$body 		= nl2br( $data['body'] );

		$this->email->set_mailtype( 'html' );
		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		if( !empty( $cc ) ) $this->email->cc( $cc );
		$this->email->subject( $subject );
		$this->email->message( $body );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( "Sending Mail to ".$toMail." failed, error: ".$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, "Contract.doSendMail" );
			return false;
		} else {
			$this->welogger->log( "Mail was sent to ".$toMail." \nwith subject: ".$subject." \nand message: ".$body, WELogger::$LOG_LEVEL_INFO, "Contract.doSendMail" );
			return true;
		}
		$this->email->clear(TRUE);
	}

	private function checkIfLooHasChanged( $id, $record ) {
		if( $id >= 0 ) {
			$contract = $this->model->getRecord( $id );
			if( !empty( $contract['logwin_origin_office'] ) && $contract['logwin_origin_office'] != $record['logwin_origin_office'] ) {
				// inform logwin australia
				$data = $this->getLooChangeMailData( $record );
				$this->doSendMail( $data );

				// inform new office
				$this->load->model('Office_model', 'office_model');
				$data 		= $this->getLooInformMailData( $record );
				$office 	= $this->office_model->getRecord( $record['logwin_origin_office'] );
				$recipient 	= $office['contact_mail'];
				$data['recipient'] 	= $recipient;
				$this->doSendMail( $data );
			}
		}
	}

	private function getLooChangeMailData( $recorddata ) {
		$mailData['CONTRACT_NUMBER'] 	= $recorddata['contract_number'];
		$mailData['LOO_NAME'] 			= $this->getLooName( $recorddata['logwin_origin_office'] );
		$mailData['LOO_ADDRESS'] 		= $this->getLooAddress( $recorddata['logwin_origin_office'] );
		$subject 						= replacePattern( $this->lang->line( 'loo.change.mail.subject' ), $mailData );
		$body							= replacePattern( $this->lang->line( 'loo.change.mail.body' ), $mailData );
		$recipient						= $this->lang->line( 'loo.change.mail.recipient' );
		return array( 'subject' => $subject, 'body' => $body, 'recipient' => $recipient );
	}


    /**
     * @param $recorddata
     * @return array
     */
    private function getSupplierInformMailData( $recorddata ) {
		$loo                        = $this->getLooAddress( $recorddata['logwin_origin_office'] );
		$mailData['RECIPIENT']		= 'User';
		$mailData['FOB_DATE'] 		= date( $this->config->item('date_format'), strtotime( $recorddata['fob_date'] ) );
		$mailData['OFU_SCHEDULED'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['ofu_scheduled'] ) );
		$mailData['DEPARTURE'] 		= $recorddata['departure'];
		$mailData['LOO_NAME'] 		= $loo['name'];
		$mailData['LOO_STREET'] 	= $loo['street'];
		$mailData['LOO_CITY'] 	    = $loo['city'];
		$mailData['LOO_PHONE'] 	    = $loo['contact_phone'];
		$mailData['LOO_MAIL'] 	    = $loo['contact_mail'];
		$mailData['INFO_DATE']      = date( 'd/m/Y', strtotime( date( 'Y-m-d H:i:s', strtotime( $recorddata['fob_date'] ) ).' -35 days' ) );
		$mailData['CONTACT_NAME'] 	= $this->getSupplierContactName( $recorddata['supplier'] );
		$mailData['CONTRACT_NUMBER']= $recorddata['contract_number'];
		$mailData['DESCRIPTION'] 	= $recorddata['product_description'];
		$subject 					= replacePattern( $this->lang->line( 'supplier.inform.mail.subject' ), $mailData );
		$body						= replacePattern( $this->lang->line( 'supplier.inform.mail.body' ), $mailData );

		return array( 'subject' => $subject, 'body' => $body );
	}

	private function getLooInformMailData( $recorddata ) {
        # abfrage nach type
		$mailData['CONTRACT_NUMBER'] 				= $recorddata['contract_number'];
		$mailData['ADVERTISEMENT_WEEK'] 			= $recorddata['advertisement_week'];
		$mailData['ALDI_AU_WH_DELIVERY_DUE_DATE'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['aldi_au_wh_delivery_due_date'] ) );
		$mailData['ALDI_AU_WH_DELIVERY_DUE_DATE_MINUS_SEVEN_DAYS'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['aldi_au_wh_delivery_due_date'].' -7 Days' ) );
		$mailData['ALDI_AU_WH_DELIVERY_DUE_DATE_MINUS_TEN_DAYS'] 	= date( $this->config->item('date_format'), strtotime( $recorddata['aldi_au_wh_delivery_due_date'].' -10 Days' ) );
		$subject 					= replacePattern( $this->lang->line( 'loo.inform.mail.subject' ), $mailData );
        if($recorddata['contract_type'] == Contract_model::$CONTRACT_TYPE_CORE_RANGE ){
            $body						= replacePattern( $this->lang->line( 'loo.inform.mail.body_core_range' ), $mailData );
        }else{
		    $body						= replacePattern( $this->lang->line( 'loo.inform.mail.body' ), $mailData );
        }
		return array( 'subject' => $subject, 'body' => $body );
	}

	private function getSupplierAddress( $id = 0 ) {
		$out = '';
		$this->load->model('Supplier_model', 'supplier_model');
		$record = $this->supplier_model->getRecord( $id );
		if( count( $record ) > 0 ) {
			if( !empty( $record['street'] ) ) 			$out .= $record['street'].', ';
			if( !empty( $record['city'] ) ) 			$out .= $record['city'].', ';
			if( !empty( $record['contact_mail'] ) ) 	$out .= $record['contact_mail'].', ';
			if( !empty( $record['contact_phone'] ) ) 	$out .= $record['contact_phone'];
		}
		return $out;
	}

	private function getLooName( $id = 0 ) {
		$out = '';
		$this->load->model('Office_model', 'office_model');
		$record = $this->office_model->getRecord( $id );
		if( count( $record ) > 0 ) {
			if( !empty( $record['name'] ) ) $out = $record['name'];
		}
		return $out;
	}

	private function getLooAddress( $id = 0 ) {
		$this->load->model('Office_model', 'office_model');
		$record = $this->office_model->getRecord( $id );
		return $record;
	}

	private function getSupplierContactName( $id = 0 ) {
		$out = '';
		$this->load->model('Supplier_model', 'supplier_model');
		$record = $this->supplier_model->getRecord( $id );
		if( count( $record ) > 0 ) $out = $record['contact_name'];
		return $out;
	}

	private function isCubeQACheckDisabled( $id = 0 ) {
		$out = FALSE;
		$contracts = $this->delivery_model->getRecordsForContract( $id );
		foreach( $contracts as $contract ) {
			if( !empty( $contract['hbl_hawb'] ) ) $out = TRUE;
		}
		return $out;
	}

}

/* End of file contract.php */
/* Location: ./app/controllers/contract.php */