<?php
require_once ( APPPATH.'controllers/abstract_controller'.EXT );

class Ofu extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('contract_model', 'model');
		$this->tablename = 'contracts';
		$this->load->helper( 'str_helper' );
	}

	/**
	 * check
	 */
	public function index() {

	}

	/**
	 * check
	 */
	public function check() {
		$records = $this->model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $records as $record ) {
			$OFUScheduled 	= $record['ofu_scheduled'];
			$OFUReal 		= $record['ofu_real'];

			if( $OFUReal == '0000-00-00 00:00:00' && time() >= strtotime( $OFUScheduled ) ) {
				$data = $this->getOFUAlertMailData( $record );
				$this->doSendMail( $data );
			}

		}
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * doSendMail
	 * @return boolean
	 */
	private function doSendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$subject 	= $data['subject'];
		$body 		= $data['body'];

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( "Sending Mail to ".$toMail." failed, error: ".$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, "Ofu.doSendMail" );
			return false;
		} else {
			$this->welogger->log( "Mail was sent to ".$toMail." \nwith subject: ".$subject." \nand message: ".$body, WELogger::$LOG_LEVEL_INFO, "Ofu.doSendMail" );
			return true;
		}
		$this->email->clear(TRUE);
	}

	private function getOFUAlertMailData( $record ) {
		$mailData['CONTRACT_NUMBER'] 	= $record['contract_number'];
		$subject 						= replacePattern( $this->lang->line( 'ofu.alert.mail.subject' ), $mailData );
		$body							= replacePattern( $this->lang->line( 'ofu.alert.mail.body' ), $mailData );
		$recipient						= $this->lang->line( 'ofu.alert.mail.recipient' );
		return array( 'subject' => $subject, 'body' => $body, 'recipient' => $recipient );
	}

}

/* End of file ofu.php */
/* Location: ./app/controllers/ofu.php */