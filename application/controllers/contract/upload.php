<?php
class Upload extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('user_agent');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->model('Supplier_model', 'supplier_model');
		$this->load->model('Delivery_due_model', 'delivery_due_model');
	}

	public function index() {
		$data = array();
		$data['message'] 					= '';
		$data['additionalJS'] 				= '<script src="'.base_url().'js/upload.js"></script>';
		$data['optionsAdvertisementWeek']	= $this->getCalendarWeeks();
		$data['optionsAdvertisementYear']	= $this->getCalendarYears();
		$data['optionsContractType']		= $this->getContractTypes();
		$this->load->view('contract/upload_overview', $data );
	}

	/**
	 * upload
	 * @param
	 * @return
	 */
	public function do_upload() {
		$upload = $this->saveUpload( 'contract-upload-file' );
		if( !empty( $upload ) ) {
			$errors = $this->importOrderUpload( $upload );
			if( count( $errors ) > 0 ) {
				$data['error'] 		= TRUE;
				$data['message'] 	= '<h4>Error!</h4>Following Suppliers are unknown:<br/><br/>'.implode( '<br/>', $errors );
			} else {
				$data['error'] 		= FALSE;
				$data['message'] 	= '<h4>Success!</h4>File was successfully imported.';
			}

		} else {
			$data['error'] 		= TRUE;
			$data['message'] 	= '<h4>Error!</h4>Upload failure, please try again.';
		}
		$data['additionalJS'] 	= '<script src="'.base_url().'js/upload.js"></script>';
		$data['optionsAdvertisementWeek']	= $this->getCalendarWeeks();
		$data['optionsAdvertisementYear']	= $this->getCalendarYears();
		$data['optionsContractType']		= $this->getContractTypes();

		$this->load->view('contract/upload_overview', $data );
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * importOrderUpload
	 * @param $file
	 * @param $supplier
	 * @return array
	 */
	private function importOrderUpload( $file ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$src 			= $this->config->item('documents_upload_path').$file;
		$errors			= array();
		$uploadType 	= $this->input->post('upload-type');
		$contracts		= array();
		$objPHPExcel 	= IOFactory::load( $src );
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		switch( $uploadType ) {
			case 'regular':
				$contracts = $this->getRegularContracts( $objWorksheet );
				break;
			case 'ecommerce':
				$contracts = $this->getEcommerceContracts( $objWorksheet );
				break;
		}

		$this->saveToDB( $contracts );
		return $errors;
	}


	/**
	 * getRegularContracts
	 * @param $objWorksheet
	 * @return array
	 */
	private function getRegularContracts( $objWorksheet ) {

		$i			= 1;
		$contracts	= array();

		foreach ($objWorksheet->getRowIterator() as $row) {
			if( $i > 3 ) {
				$contractNumber = (string) $objWorksheet->getCellByColumnAndRow(2, $i)->getValue();
				if( !empty( $contractNumber ) ) {

					if( !isset( $orders[$contractNumber] ) ) {
						$advertisementWeek 	= $this->input->post('contract-advertisement-week');
						$advertisementYear 	= $this->input->post('contract-advertisement-year');
						$contractType 		= $this->input->post('contract-type');
						$productCode 		= (string) $objWorksheet->getCellByColumnAndRow(0, $i)->getValue();
						$productDescription = (string) $objWorksheet->getCellByColumnAndRow(1, $i)->getValue();
						$supplier 			= $this->getSupplierId( (string) $objWorksheet->getCellByColumnAndRow(14, $i)->getValue() );
						if( empty( $supplier ) ) $errors[] = '<strong>Contract '.$contractNumber.':</strong> Supplier <i>'.(string) $objWorksheet->getCellByColumnAndRow(14, $i)->getValue().'</i> unknown.';

						$caseLength		= (float) $objWorksheet->getCellByColumnAndRow(4, $i)->getValue();
						$caseWidth 		= (float) $objWorksheet->getCellByColumnAndRow(5, $i)->getValue();
						$caseHeight		= (float) $objWorksheet->getCellByColumnAndRow(6, $i)->getValue();
						$caseWeight		= (float) $objWorksheet->getCellByColumnAndRow(3, $i)->getValue();
						$caseQuantity1 	= (float) $objWorksheet->getCellByColumnAndRow(20, $i)->getCalculatedValue();
						$caseQuantity2 	= (float) $objWorksheet->getCellByColumnAndRow(21, $i)->getCalculatedValue();
						$caseQuantity3 	= (float) $objWorksheet->getCellByColumnAndRow(23, $i)->getCalculatedValue();
						$caseQuantity4 	= (float) $objWorksheet->getCellByColumnAndRow(24, $i)->getCalculatedValue();
						$caseQuantity5 	= (float) $objWorksheet->getCellByColumnAndRow(25, $i)->getCalculatedValue();
						$caseQuantity6 	= (float) $objWorksheet->getCellByColumnAndRow(26, $i)->getCalculatedValue();
						$caseQuantity7 	= (float) $objWorksheet->getCellByColumnAndRow(27, $i)->getCalculatedValue();
						$caseQuantity8 	= (float) $objWorksheet->getCellByColumnAndRow(28, $i)->getCalculatedValue();
						$caseQuantity9 	= (float) $objWorksheet->getCellByColumnAndRow(22, $i)->getCalculatedValue();
						$totalCases 	= $caseQuantity1 + $caseQuantity2 + $caseQuantity3 + $caseQuantity4 + $caseQuantity5 + $caseQuantity6 + $caseQuantity7 + $caseQuantity8 + $caseQuantity9;
						$unitsPerCase	= (string) $objWorksheet->getCellByColumnAndRow(8, $i)->getValue();
						$caseVolume		= ( $caseLength / 1000 ) * ( $caseWidth / 1000 ) * ( $caseHeight / 1000 ); // convert millimeter in meter

						$volume 		= $caseVolume * $totalCases;
						$grossWeight 	= $caseWeight * $totalCases;

						$contracts[$contractNumber] = array(
							'contract_number' 	=> $contractNumber,
							'departure' 		=> (string) $objWorksheet->getCellByColumnAndRow(12, $i)->getValue(),
							'volume'			=> $volume,
							'gross_weight'		=> $grossWeight,
							'total_cases'		=> $totalCases,
							'ofu_scheduled' 	=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(9, $i)->getValue(), ' -21 days' ),
							'on_sale_date' 		=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(18, $i)->getValue() ),
							'fob_date' 			=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(9, $i)->getValue() ),
							'initial_crd' 		=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(9, $i)->getValue() ),
							'initial_sales_date'=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(18, $i)->getValue() ),
							'aldi_au_wh_delivery_due_date' => $this->delivery_due_model->getDeliveryDueDate( $advertisementWeek, $advertisementYear ),
							'aldi_au_wh_delivery_due_end_date' => $this->delivery_due_model->getDeliveryDueDate( $advertisementWeek, $advertisementYear, true ),
							'advertisement_week' => $advertisementWeek,
							'advertisement_year' => $advertisementYear,
							'supplier' 			=> $supplier,
							'units_per_case' 	=> $unitsPerCase,
							'case_length' 		=> $caseLength,
							'case_width' 		=> $caseWidth,
							'case_height' 		=> $caseHeight,
							'case_weight' 		=> $caseWeight,
							'case_cubage'		=> $caseVolume,
							'contract_type'		=> $contractType,
							'deliveries' 		=> array(),
						);
					}
					$contracts[$contractNumber]['deliveries'][] = array(
						'dc'			=> 'MIN',
						'destination' 	=> 'SYD',
						'case_quantity' => $caseQuantity1,
						'unit_quantity' => $caseQuantity1 * $unitsPerCase,
						'volume'		=> $caseQuantity1 * $caseVolume,
						'gross_weight'	=> $caseQuantity1 * $caseWeight,
						'contract_number' => $contractNumber,
						'product_code' 			=> $productCode,
						'product_description' 	=> $productDescription,
						'advertisement_week' 	=> $advertisementWeek,
					);
					$contracts[$contractNumber]['deliveries'][] = array(
						'dc' 			=> 'DER',
						'destination' 	=> 'MEL',
						'case_quantity' => $caseQuantity2,
						'unit_quantity' => $caseQuantity2 * $unitsPerCase,
						'volume'		=> $caseQuantity2 * $caseVolume,
						'gross_weight'	=> $caseQuantity2 * $caseWeight,
						'contract_number' => $contractNumber,
						'product_code' 			=> $productCode,
						'product_description' 	=> $productDescription,
						'advertisement_week' 	=> $advertisementWeek,
					);
					$contracts[$contractNumber]['deliveries'][] = array(
						'dc' 			=> 'STP',
						'destination' 	=> 'BNE',
						'case_quantity' => $caseQuantity3,
						'unit_quantity' => $caseQuantity3 * $unitsPerCase,
						'volume'		=> $caseQuantity3 * $caseVolume,
						'gross_weight'	=> $caseQuantity3 * $caseWeight,
						'contract_number' => $contractNumber,
						'product_code' 			=> $productCode,
						'product_description' 	=> $productDescription,
						'advertisement_week' 	=> $advertisementWeek,
					);
					$contracts[$contractNumber]['deliveries'][] = array(
						'dc' 			=> 'PRE',
						'destination' 	=> 'SYD',
						'case_quantity' => $caseQuantity4,
						'unit_quantity' => $caseQuantity4 * $unitsPerCase,
						'volume'		=> $caseQuantity4 * $caseVolume,
						'gross_weight'	=> $caseQuantity4 * $caseWeight,
						'contract_number' => $contractNumber,
						'product_code' 			=> $productCode,
						'product_description' 	=> $productDescription,
						'advertisement_week' 	=> $advertisementWeek,
					);
					$contracts[$contractNumber]['deliveries'][] = array(
						'dc' 			=> 'DAN',
						'destination' 	=> 'MEL',
						'case_quantity' => $caseQuantity5,
						'unit_quantity' => $caseQuantity5 * $unitsPerCase,
						'volume'		=> $caseQuantity5 * $caseVolume,
						'gross_weight'	=> $caseQuantity5 * $caseWeight,
						'contract_number' => $contractNumber,
						'product_code' 			=> $productCode,
						'product_description' 	=> $productDescription,
						'advertisement_week' 	=> $advertisementWeek,
					);
					$contracts[$contractNumber]['deliveries'][] = array(
						'dc' 			=> 'BRE',
						'destination' 	=> 'BNE',
						'case_quantity' => $caseQuantity6,
						'unit_quantity' => $caseQuantity6 * $unitsPerCase,
						'volume'		=> $caseQuantity6 * $caseVolume,
						'gross_weight'	=> $caseQuantity6 * $caseWeight,
						'contract_number' => $contractNumber,
						'product_code' 			=> $productCode,
						'product_description' 	=> $productDescription,
						'advertisement_week' 	=> $advertisementWeek,
					);
					$contracts[$contractNumber]['deliveries'][] = array(
						'dc' 			=> 'JKT',
						'destination' 	=> 'FRE',
						'case_quantity' => $caseQuantity7,
						'unit_quantity' => $caseQuantity7 * $unitsPerCase,
						'volume'		=> $caseQuantity7 * $caseVolume,
						'gross_weight'	=> $caseQuantity7 * $caseWeight,
						'contract_number' => $contractNumber,
						'product_code' 			=> $productCode,
						'product_description' 	=> $productDescription,
						'advertisement_week' => $advertisementWeek,
					);
					$contracts[$contractNumber]['deliveries'][] = array(
						'dc' 			=> 'RGY',
						'destination' 	=> 'ADL',
						'case_quantity' => $caseQuantity8,
						'unit_quantity' => $caseQuantity8 * $unitsPerCase,
						'volume'		=> $caseQuantity8 * $caseVolume,
						'gross_weight'	=> $caseQuantity8 * $caseWeight,
						'contract_number' => $contractNumber,
						'product_code' 			=> $productCode,
						'product_description' 	=> $productDescription,
						'advertisement_week' => $advertisementWeek,
					);
					$contracts[$contractNumber]['deliveries'][] = array(
						'dc'			=> 'TRU',
						'destination' 	=> 'AUMEL',
						'case_quantity' => $caseQuantity9,
						'unit_quantity' => $caseQuantity9 * $unitsPerCase,
						'volume'		=> $caseQuantity9 * $caseVolume,
						'gross_weight'	=> $caseQuantity9 * $caseWeight,
						'contract_number' => $contractNumber,
						'product_code' 			=> $productCode,
						'product_description' 	=> $productDescription,
						'advertisement_week' 	=> $advertisementWeek,
					);
				}
			}
			$i++;
		}
		return $contracts;
	}

	/**
	 * getEcommerceContracts
	 * @param $objWorksheet
	 * @return array
	 */
	private function getEcommerceContracts( $objWorksheet ) {

		$i			= 1;
		$contracts	= array();

		foreach ($objWorksheet->getRowIterator() as $row) {
			if( $i > 3 ) {
				$contractNumber = (string) $objWorksheet->getCellByColumnAndRow(2, $i)->getValue();
				if( !empty( $contractNumber ) ) {

					if (!isset($orders[$contractNumber])) {
						$advertisementWeek = $this->input->post('contract-advertisement-week');
						$advertisementYear = $this->input->post('contract-advertisement-year');
						$contractType = $this->input->post('contract-type');
						$productCode = (string)$objWorksheet->getCellByColumnAndRow(0, $i)->getValue();
						$productDescription = (string)$objWorksheet->getCellByColumnAndRow(1, $i)->getValue();
						$supplier = $this->getSupplierId((string)$objWorksheet->getCellByColumnAndRow(14, $i)->getValue());
						if (empty($supplier)) $errors[] = '<strong>Contract ' . $contractNumber . ':</strong> Supplier <i>' . (string)$objWorksheet->getCellByColumnAndRow(14, $i)->getValue() . '</i> unknown.';

						$caseLength = (float)$objWorksheet->getCellByColumnAndRow(4, $i)->getValue();
						$caseWidth = (float)$objWorksheet->getCellByColumnAndRow(5, $i)->getValue();
						$caseHeight = (float)$objWorksheet->getCellByColumnAndRow(6, $i)->getValue();
						$caseWeight = (float)$objWorksheet->getCellByColumnAndRow(3, $i)->getValue();
						$caseQuantity = (float)$objWorksheet->getCellByColumnAndRow(20, $i)->getCalculatedValue();
						$totalCases = $caseQuantity;
						$unitsPerCase = (string)$objWorksheet->getCellByColumnAndRow(8, $i)->getValue();
						$caseVolume = ($caseLength / 1000) * ($caseWidth / 1000) * ($caseHeight / 1000); // convert millimeter in meter

						$volume = $caseVolume * $totalCases;
						$grossWeight = $caseWeight * $totalCases;

						$contracts[$contractNumber] = array(
							'contract_number' => $contractNumber,
							'departure' => (string)$objWorksheet->getCellByColumnAndRow(12, $i)->getValue(),
							'volume' => $volume,
							'gross_weight' => $grossWeight,
							'total_cases' => $totalCases,
							'ofu_scheduled' => $this->formatDate($objWorksheet->getCellByColumnAndRow(9, $i)->getValue(), ' -21 days'),
							'on_sale_date' => $this->formatDate($objWorksheet->getCellByColumnAndRow(19, $i)->getValue()),
							'fob_date' => $this->formatDate($objWorksheet->getCellByColumnAndRow(9, $i)->getValue()),
							'initial_crd' 		=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(9, $i)->getValue() ),
							'initial_sales_date'=> $this->formatDate( $objWorksheet->getCellByColumnAndRow(19, $i)->getValue() ),
							'aldi_au_wh_delivery_due_date' => $this->delivery_due_model->getDeliveryDueDate($advertisementWeek, $advertisementYear),
							'aldi_au_wh_delivery_due_end_date' => $this->delivery_due_model->getDeliveryDueDate($advertisementWeek, $advertisementYear, true),
							'advertisement_week' => $advertisementWeek,
							'advertisement_year' => $advertisementYear,
							'supplier' => $supplier,
							'units_per_case' => $unitsPerCase,
							'case_length' => $caseLength,
							'case_width' => $caseWidth,
							'case_height' => $caseHeight,
							'case_weight' => $caseWeight,
							'case_cubage' => $caseVolume,
							'contract_type' => $contractType,
							'deliveries' => array(),
						);
					}
					$contracts[$contractNumber]['deliveries'][] = array(
						'dc' => 'NSW',
						'destination' => 'SYD',
						'case_quantity' => $caseQuantity,
						'unit_quantity' => $caseQuantity * $unitsPerCase,
						'volume' => $caseQuantity * $caseVolume,
						'gross_weight' => $caseQuantity * $caseWeight,
						'contract_number' => $contractNumber,
						'product_code' => $productCode,
						'product_description' => $productDescription,
						'advertisement_week' => $advertisementWeek,
					);
				}
			}
			$i++;
		}
		return $contracts;
	}

	/**
	 * getSupplierId
	 * @param str
	 * @return str
	 */
	private function getSupplierId( $name ){
		$out = '';
		$suppliers = $this->supplier_model->getRecordsWhere( array( 'name' => $name, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $suppliers ) > 0 ) {
			$out = $suppliers[0]['id'];
		}
		return $out;
	}

	/**
	 * saveToDB
	 * @param
	 * @return array
	 */
	private function saveToDB( $data ){

		foreach( $data as $value ) {
			// save contract
			$contractId = $this->contract_model->getIdByContractNumber( $value['contract_number'] );

			// previous contract
			$previousContract = array();
			if( $contractId > 0 ) $previousContract = $this->contract_model->getRecord( $contractId );

			$contract 	= $value;
			unset( $contract['deliveries'] );

			if( $contractId > 0 ) {
				if( $previousContract['initial_crd'] != '0000-00-00 00:00:00' ) unset( $contract['initial_crd'] );
				if( $previousContract['initial_sales_date'] != '0000-00-00 00:00:00' ) unset( $contract['initial_sales_date'] );
			}

			if( $contractId > 0 ) {
				$this->contract_model->updateRecord( $contractId, $contract );
			} else {
				$contractId = $this->contract_model->createNew( $contract );
			}

			// save deliveries
			$deliveries = $value['deliveries'];
			foreach( $deliveries as $delivery ) {
				$deliveryId = $this->delivery_model->getIdByDCAndContractId( $delivery['dc'], $contractId );
				$delivery['contract_id'] = $contractId;

				if( $deliveryId > 0 ) {
					$this->delivery_model->updateRecord( $deliveryId, $delivery );
				} else {
					$articleId = $this->delivery_model->createNew( $delivery );
				}
			}

			// process events
			$latestContract = $this->contract_model->getRecord( $contractId );
			$this->contract_model->processEventDates( $latestContract, $previousContract );

		}
	}

	/**
	 * formatDate
	 * @param $date
	 * @return string
	 */
	private function formatDate( $date, $offset='' ) {
		$out = '';
		if( !empty( $date ) && stristr( $date, '.' ) === FALSE ) {
			$out = gmdate( 'Y-m-d H:i:s', strtotime( date( 'Y-m-d H:i:s', PHPExcel_Shared_Date::ExcelToPHP( $date ) ).$offset ) );
		} else {
			$out = $date;
		}
		return $out;
	}

	/**
	 * saveUpload
	 * @param $field
	 * @return string
	 */
	private function saveUpload( $field ) {
		$config['upload_path'] 		= $this->config->item('documents_upload_path');
		$config['allowed_types'] 	= $this->config->item('allowed_types');
		$config['max_size']			= $this->config->item('max_size');
		$config['encrypt_name'] 	= TRUE;
		$destFile					= '';

		$this->load->library( 'upload', $config );

		if ( !$this->upload->do_upload( $field ) ) {
			$data 	= $this->upload->data();
			$error 	= array('error' => $this->upload->display_errors());

			$this->welogger->log( 'Upload failure, file: '.$data['full_path'].', error: '.$error['error'].', image: '.serialize( $this->upload->data() ), WELogger::$LOG_LEVEL_ERROR, 'Edit.do_upload' );
		} else {
			$data 		= $this->upload->data();
			$source 	= $data['full_path'];
			$destFile	= $data['file_name'];

			$this->welogger->log( 'Upload success, file: '.$source, WELogger::$LOG_LEVEL_INFO, 'Edit.do_upload' );

		}
		return $destFile;
	}

	/**
	 * getCalendarWeeks
	 * @return array
	 */
	private function getCalendarWeeks() {
		$out = array();
		for( $i=1; $i<=53; $i++ ) {
			$num = ( $i < 10 ) ? '0'.$i : $i;
			$out[$num] = $num;
		}
		$out['EA']	= 'EA';
		$out['EA6']	= 'EA6';
		$out['XM']	= 'XM';
		return $out;
	}

	/**
	 * getCalendarYears
	 * @return array
	 */
	private function getCalendarYears() {
		$out = array();
		$years = range( date("Y", strtotime("-2 year")), date("Y", strtotime("+2 year")));
		$out = array_combine( $years, $years );
		return $out;
	}

	/**
	 * getContractTypes
	 * @return array
	 */
	private function getContractTypes() {
		$tcaNode = $this->tca->getColumnNodeById( 'contracts_contract_type' );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}
}

/* End of file upload.php */
/* Location: ./app/controllers/upload.php */