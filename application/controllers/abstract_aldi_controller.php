<?php

/**
 * Abstract_zlabels_controller
 * 
 * base class for all controllers of the app. 
 * Basically it provides functions for controlling acces to the 
 * concrete controller.
 */
class Abstract_aldi_controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->library('acl');
        $this->load->library('session');

		$dir 	= $this->router->fetch_directory();
		$ctrl 	= $this->router->fetch_class();
		$method = $this->router->fetch_method();

		if (defined('CRON')) {
			// allow access to all controllers via CRON
		} else {

			if( !in_array( $ctrl, $this->config->item('login_free_controllers') ) ){
				$controller_perm_name 	= 'controller_'.$dir.$ctrl;
				$action_perm_name 		= 'action_'.$ctrl.'_'.$method;

				/*var_dump($controller_perm_name);
				var_dump($dir.$ctrl);
				var_dump($this->acl->hasPermission( $controller_perm_name ));
				var_dump(in_array( $dir.$ctrl, $this->config->item('acl_guarded_controllers') ));*/

				// HTTP calls must be checked against ACL
				if( in_array( $dir.$ctrl, $this->config->item('acl_guarded_controllers') ) &&  !$this->acl->hasPermission( $controller_perm_name ) ) redirect( '/login/logout?back_url='.urlencode( uri_string() ), 'location' );//show_404($dir.$ctrl);
				if( $this->acl->permissionExists( $action_perm_name ) && !$this->acl->hasPermission( $action_perm_name ) ) show_404($dir.$ctrl);
			}
		}
	}

	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {

		// ist nur ne quick&dirty Lösung!
		// muss man mal ordentlich machen...

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "HBL/HAWB";
		$tmp['input'] = '<input type="text" id="shipments_hbl_hawb" value="'.$this->getSearchFormValue('shipments_hbl_hawb').'" name="shipments_hbl_hawb[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = '<input type="text" id="shipments_container_number" value="'.$this->getSearchFormValue('shipments_container_number').'" name="shipments_container_number[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Departure";
		$tmp['input'] = $this->tca->getFormInputById('contracts_departure', $this->getSearchFormValue('contracts_departure'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Destination";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_destination', $this->getSearchFormValue('deliveries_destination'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Traffic type";
		$tmp['input'] = form_dropdown( 'contracts_traffic_type[]', array( "" => "please select ...", "1" => "Factory", "5" => "Factory/CFS", "2" => "CFS", "3" => "AIR", "4" => "PTS Loading" ), $this->getSearchFormValue('contracts_traffic_type') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'event[]', array( "" => "please select ...", "etd" => "ETD", "eta" => "ETA", "cus" => "CUS", "atd" => "ATD", "ata" => "ATA" ), $this->getSearchFormValue('event') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_from" value="'.$this->getSearchFormValue('shipments_from').'" name="shipments_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_to" value="'.$this->getSearchFormValue('shipments_to').'" name="shipments_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-Sale week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Logwin origin office";
		$tmp['input'] = $this->tca->getFormInputById('contracts_logwin_origin_office', $this->getSearchFormValue('contracts_logwin_origin_office'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Supplier";
		$tmp['input'] = $this->tca->getFormInputById('contracts_supplier', $this->getSearchFormValue('contracts_supplier'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		return $res;
	}

	/**
	 * getSearchFormValue
	 * returns transmitted search form value
	 * @param str $key
	 */
	public function getSearchFormValue( $key ){
		$v = !empty($this->recordListSearchParams[ $key ]) ? $this->recordListSearchParams[ $key ] : "";
		if( is_array($v)) $v = $v[0];
		return $v;
	}

    /**
     * @param $postData
     * @return mixed
     */
    public function clearEmptyPostVars($postData){
        $returnArray = array();
        if(is_array($postData)){
            foreach($postData as $key => $val){
                if(!empty($val['0'])){
                    $returnArray[$key] = $val['0'];
                }
            }

        }
        return $returnArray;
    }




}

/* End of file zlabels_controller.php */
/* Location: ./system/application/core/zlabels_controller.php */