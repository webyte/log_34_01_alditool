<?php
require_once( APPPATH.'third_party/spout/src/Spout/Autoloader/autoload.php' );

class Report_clp_cron extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Abstract_report_clp_model', 'abstract_model');
	}

	/**
	 * excel_report_mail
	 * @return
	 */
	public function excel_report_mail( $reportType=0, $userId=0 ){

		if( $reportType <= 0 ) return;
        $writerType = \Box\Spout\Common\Type::XLSX;
        $suffix     = 'xlsx';

		switch( $reportType ) {
			case Abstract_report_clp_model::$REPORT_TYPE_CONTAINER_STATUS:
				$name 		= 'Container Status Report';
				$filename 	= 'logwin_aldi_clp_container_status_report_'.random_string('alnum', 32);
				$this->load->model('Report_clp_container_status_model', 'model');
				break;
			case Abstract_report_clp_model::$REPORT_TYPE_DELAY:
				$name 		= 'Delay Report';
				$filename 	= 'logwin_aldi_clp_delay_report_'.random_string('alnum', 32);
				$this->load->model('Report_clp_delay_model', 'model');
				break;
			case Abstract_report_clp_model::$REPORT_TYPE_KPI:
				$name 		= 'KPI Report';
				$filename 	= 'logwin_aldi_clp_kpi_report_'.random_string('alnum', 32);
				$this->load->model('Report_clp_kpi_model', 'model');
				break;
			case Abstract_report_clp_model::$REPORT_TYPE_SHIPMENT_STATUS:
				$name 		= 'Shipment Status Report';
				$filename 	= 'logwin_aldi_clp_shipment_status_report_'.random_string('alnum', 32);
				$this->load->model('Report_clp_shipment_status_model', 'model');
				break;
			case Abstract_report_clp_model::$REPORT_TYPE_PO:
				$name 		= 'PO Report';
				$filename 	= 'logwin_aldi_clp_po_report_'.random_string('alnum', 32);
				$this->load->model('Report_clp_po_model', 'model');
				break;
			case Abstract_report_clp_model::$REPORT_TYPE_SHIPMENT:
				$name 		= 'Shipment Report';
				$filename 	= 'logwin_aldi_clp_shipment_report_'.random_string('alnum', 32);
				$this->load->model('Report_clp_shipment_model', 'model');
				break;
			case Abstract_report_clp_model::$REPORT_TYPE_SHIPMENT_INTERFACE:
				$name 		= 'Shipment Report (Interface)';
				$filename 	= 'AU_LIBU_FFShipmentInbound_ALDI_Logwin_'.date('Ymd').'_'.random_string('alnum', 32);
                $writerType = \Box\Spout\Common\Type::CSV;
                $suffix     = 'csv.ready';
				$this->load->model('Report_clp_shipment_interface_model', 'model');
				break;
		}

		$fileName 				= $this->config->item('tmp_path').$filename;
		$cells					= $this->model->getCellDefintions();
		$records				= $this->model->getRecords();
		$rows					= array();
		$labels 				= array();
		foreach( $cells as $cell ) {
			$labels[] = $cell['label'];
		}
		$rows[] 				= $labels;

		foreach( $records as $record ) {
			$data = array();
			foreach( $cells as $cell ) {
				$data[] = !empty( $cell['field'] ) ? $record[$cell['field']] : '';
			}
			$rows[] = $data;
		}

		$writer = \Box\Spout\Writer\WriterFactory::create( $writerType );
		$writer->openToFile( $fileName.'.'.$suffix );
		$writer->addRows($rows);
		$writer->close();

		$this->load->library('zip');
		$this->zip->read_file( $fileName.'.'.$suffix );
		$this->zip->archive( $fileName.'.zip' );

		$recipient = $this->getRecipient( $userId );
		$this->generateMail( $fileName.'.zip', $name, $recipient );
	}

	/**
	 * excel_report_file
	 * @return
	 */
	public function excel_report_file( $reportType=0, $userId=0 )
	{
		if( $reportType <= 0 ) return;

		switch( $reportType ) {
			case Abstract_report_clp_model::$REPORT_TYPE_SHIPMENT_INTERFACE:
				$name 		= 'Shipment Report (Interface)';
                $filename 	= 'AU_LIBU_FFShipmentInbound_ALDI_Logwin_'.date('Ymd').'_'.random_string('alnum', 32);
				$this->load->model('Report_clp_shipment_interface_model', 'model');
				break;
		}

		$fileName 				= $this->config->item('export_path').'reports/shipment/'.$filename;
		$cells					= $this->model->getCellDefintions();
		$records				= $this->model->getRecords();
		$rows					= array();
		$labels 				= array();
		foreach( $cells as $cell ) {
			$labels[] = $cell['label'];
		}
		$rows[] 				= $labels;

		foreach( $records as $record ) {
			$data = array();
			foreach( $cells as $cell ) {
				$data[] = !empty( $cell['field'] ) ? $record[$cell['field']] : '';
			}
			$rows[] = $data;
		}

		$writer = \Box\Spout\Writer\WriterFactory::create(\Box\Spout\Common\Type::CSV);
		$writer->openToFile( $fileName.'.csv.ready' );
		$writer->addRows($rows);
		$writer->close();
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams = array() ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$cells			= $this->model->getCellDefintions();
		$records		= $this->model->getRecords( $recordListSearchParams );
		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		$labels = array();
		foreach( $cells as $cell ) {
			$labels[] = $cell['label'];
		}
		$objWorksheet->fromArray( $labels, NULL, 'A1' );

		$row = 2;
		foreach( $records as $record ) {
			$data = array();
			foreach( $cells as $cell ) {
				$data[] = $record[$cell['field']];
			}
			$objWorksheet->fromArray( $data, NULL, 'A'.$row );
			$row++;
		}

		return $objPHPExcel;
	}

	/**
	 * generateMail
	 * @return
	 */
	private function generateMail( $file, $name, $recipient ) {
		$data 				= array();
		$data['subject'] 	= $this->lang->line( 'report_clp.mail.subject' ).$name;
		$data['body'] 		= $this->lang->line( 'report_clp.mail.body' ).$name;
		$data['file']		= $file;
		$data['recipient'] 	= $recipient;
		$data['cc'] 		= array('lots@logwin-logistics.com','or@webyte.org');

		if( ENVIRONMENT != 'production' ) $data['recipient'] = array('lots@logwin-logistics.com','or@webyte.org');
		if( !empty( $data['recipient'] ) ) $this->sendMail( $data );

	}

	/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$cc			= ( isset( $data['cc'] ) ) ? $data['cc'] : '';
		$subject 	= $data['subject'];
		$body 		= $data['body'];
		$file 		= $data['file'];

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		$this->email->attach( $file );

		if( !empty( $cc ) ) $this->email->cc( $cc );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Report_clp_kpi.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Report_clp_kpi.sendMail' );
			return true;
		}
		$this->email->clear(TRUE);
	}

	private function getRecipient( $userId ) {
		$this->load->model('User_model', 'user_model');
		$out 		='';
		$records 	= $this->user_model->getRecordsWhere( array( 'id' => $userId, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $records ) > 0 ) $out = $records[0]['email'];
		return $out;

	}

}

/* End of file report.php */
/* Location: ./app/controllers/report.php */
