<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );
//require_once( APPPATH.'third_party/spout/src/Spout/Autoloader/autoload.php' );

class Report_shipment extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$data['cdaResult'] = '';
		$this->load->view('report/report_overview', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 		= $this->getCustomSearchFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_shipment/excel_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Shipment report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}

	/**
	 * excel_report_export
	 * @return
	 */
	public function excel_report_export(){

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams 	= $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=logwin_aldi_shipment_report.xls');
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}

	/**
	 * csv_report_export
	 * @return
	 */
	public function csv_report_export(){
		$this->recordListSearchParams 	= $this->input->post();
		$this->records					= $this->model->getShipmentReportRecords( $this->recordListSearchParams, array() );

		$data = array(
			'hbl_hawb_dc' => 'HAWB/HBL + DC',
			'shipments_hbl_hawb' => 'HAWB/HBL',
			'line_item_id' => 'Line Item ID',
			'contracts_contract_number' => 'Purchase Order',
			'deliveries_product_code' => 'Product code',
			'shipments_carrier' => 'Carrier',
			'shipments_mother_vessel_name' => 'Mother vessel',
			'shipments_voyage_number' => 'Voyage',
			'container_type' => 'Container Type',
			'shipments_container_number' => 'Container number',
			'shipments_seal_number' => 'Seal',
			'deliveries_case_quantity' => 'Case quantity',
			'deliveries_unit_quantity' => 'Unit quantity',
			'deliveries_volume' => 'Case cubage',
			'deliveries_gross_weight' => 'Case weight',
			'shipments_departure_port' => 'Departure Port',
			'shipments_destination_port' => 'Destination Port',
			'shipments_etd' => 'ETD',
			'shipments_eta' => 'ETA',
			'cy_delivery' => 'CY Delivery',
			'shipments_ata' => 'Arrival Date',
			'shipments_cus' => 'Customs Cleared Date',
			'mode' => 'Mode',
			'contracts_traffic_type' => 'Traffic Type',
			'contracts_ofu_real' => 'OFU actual',
		);

		$out 		= "";
		$delimiter 	= "|";
		$newline 	= "\r\n";

		// header
		foreach( $data as $key=>$val ) {
			$out .= $val.$delimiter;
		}
		$out .= $newline;

		// body
		foreach ( $this->records as $row ) {
			foreach( $data as $key=>$val ) {
				$out .= $row[$key].$delimiter;
			}
			$out .= $newline;
		}

		$this->output->set_header('Content-Type: text/x-csv');
		$this->output->set_header('Content-Disposition: attachment; filename=logwin_aldi_shipment_report.csv');
		$this->output->set_header('Cache-Control: max-age=0');
		$this->output->set_output( utf8_decode( $out ) );
		//var_dump($out);
	}


	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {

		// ist nur ne quick&dirty Lösung!
		// muss man mal ordentlich machen...

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "HBL/HAWB";
		$tmp['input'] = '<input type="text" id="shipments_hbl_hawb" value="'.$this->getSearchFormValue('shipments_hbl_hawb').'" name="shipments_hbl_hawb[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = '<input type="text" id="shipments_container_number" value="'.$this->getSearchFormValue('shipments_container_number').'" name="shipments_container_number[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Departure";
		$tmp['input'] = $this->tca->getFormInputById('contracts_departure', $this->getSearchFormValue('contracts_departure'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Destination";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_destination', $this->getSearchFormValue('deliveries_destination'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Traffic type";
		$tmp['input'] = form_dropdown( 'contracts_traffic_type[]', array( "" => "please select ...", "1" => "Factory", "2" => "CFS", "3" => "AIR", "4" => "PTS Loading", "5" => "Factory/CFS" ), $this->getSearchFormValue('contracts_traffic_type') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'event[]', array( "" => "please select ...", "etd" => "ETD", "eta" => "ETA", "cus" => "CUS", "atd" => "ATD", "ata" => "ATA" ), $this->getSearchFormValue('event') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_from" value="'.$this->getSearchFormValue('shipments_from').'" name="shipments_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_to" value="'.$this->getSearchFormValue('shipments_to').'" name="shipments_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-Sale week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Logwin origin office";
		$tmp['input'] = $this->tca->getFormInputById('contracts_logwin_origin_office', $this->getSearchFormValue('contracts_logwin_origin_office'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Supplier";
		$tmp['input'] = $this->tca->getFormInputById('contracts_supplier', $this->getSearchFormValue('contracts_supplier'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Incoterm";
		$tmp['input'] = form_dropdown( 'shipments_incoterm_code[]', array( "" => "please select ...", "1" => "EXW", "2" => "FOB", "3" => "DDP" ), $this->getSearchFormValue('shipments_incoterm_code') );
		$res[] = $tmp;

		return $res;
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$cells = array(
			'hbl_hawb_dc' => 'HAWB/HBL + DC',
			'shipments_deliveries_mm_hbl_hawb' => 'HAWB/HBL',
			'line_item_id' => 'Line Item ID',
			'contracts_contract_number' => 'Purchase Order',
			'deliveries_product_code' => 'Product code',
			'shipments_carrier' => 'Carrier',
			'shipments_mother_vessel_name' => 'Mother vessel',
			'shipments_voyage_number' => 'Voyage',
			'container_type' => 'Container Type',
			'shipments_container_number' => 'Container number',
			'shipments_seal_number' => 'Seal',
			'deliveries_case_quantity' => 'Case quantity',
			'deliveries_unit_quantity' => 'Unit quantity',
			'deliveries_volume' => 'Case cubage',
			'deliveries_gross_weight' => 'Case weight',
			'shipments_departure_port' => 'Departure Port',
			'shipments_destination_port' => 'Destination Port',
			'shipments_etd' => 'ETD',
			'shipments_eta' => 'ETA',
			'cy_delivery' => 'CY Delivery',
			'shipments_ata' => 'Arrival Date',
			'shipments_cus' => 'Customs Cleared Date',
			'mode' => 'Mode',
			'contracts_traffic_type' => 'Traffic Type',
			'contracts_ofu_real' => 'OFU actual',
		);
		$alphas = range('A', 'Z');

		$this->records	= $this->model->getShipmentReportRecords( $recordListSearchParams, array() );
		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		$cell = 0;
		foreach( $cells as $key=>$val ) {
			$objWorksheet->getCell( $alphas[$cell].'1')->setValue( $val );
			$cell++;
		}

		$row = 2;
		foreach( $this->records as $data ) {
			$cell = 0;
			foreach( $cells as $key=>$val ) {
				if(
					$key == 'shipments_etd' ||
					$key == 'shipments_eta'
				) {
					$d = ( $key == 'shipments_etd' ) ? $data['excel_etd'] : $data['excel_eta'];
					if( !empty($d) && $d != '0000-00-00 00:00:00') {
						$dt = new DateTime( $d );
						$objWorksheet->getCell($alphas[$cell].$row)->setValue( PHPExcel_Shared_Date::PHPToExcel( $dt ) );
						$objWorksheet->getStyle( $alphas[$cell].$row)->getNumberFormat()->setFormatCode('dd/mm/yyyy' );
					}
				} else if(
					$key == 'deliveries_case_quantity' ||
					$key == 'deliveries_unit_quantity' ||
					$key == 'deliveries_volume' ||
					$key == 'deliveries_gross_weight'
				) {
					$objWorksheet->getCell($alphas[$cell].$row)->setValue( $data[$key] );
					$objWorksheet->getStyle( $alphas[$cell].$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL );
				} else {
					$objWorksheet->getCell($alphas[$cell].$row)->setValue( $data[$key] );
				}
				$cell++;
			}
			$row++;
		}

		return $objPHPExcel;
	}

}

/* End of file report.php */
/* Location: ./app/controllers/report.php */