<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_vessel_schedule extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= 'Reporting';
//		$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_vessel_schedule', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 			= $this->getCustomVesselScheduleFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_vessel_schedule/vessel_schedule_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Vessel Schedule';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * order_report_export
	 * @return
	 */
	public function vessel_schedule_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams = $this->input->post();
		$recordListSearchParams['shipments_crdate'] = array( date( 'Y-m-d 00:00:00', strtotime( '-183 days', time() ) ) );
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel2007' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'vessel_schedule_report.xlsx' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}



	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////
	

	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$this->load->model('Dc_model', 'dc_model');

		$this->records 	= $this->model->getVesselScheduleRecords( $recordListSearchParams, array() );

		$week				= $recordListSearchParams['contracts_advertisement_week'][0];
		$activeSheetIndex	= 0;
		$template 			= $this->config->item('app_path').'views/report/vessel_schedule_template.xlsx';
		$objReader 			= new PHPExcel_Reader_Excel2007();
		$objPHPExcel 		= $objReader->load( $template );

		$objPHPExcel->setActiveSheetIndex($activeSheetIndex);
		$sheetTemplate		= clone $objPHPExcel->getActiveSheet();

		foreach( $this->records as $key=>$value ) {
			if( $activeSheetIndex == 0 ) {
				$objPHPExcel->setActiveSheetIndex($activeSheetIndex);
			} else {
				$newSheet = clone $sheetTemplate;
				$newSheetIndex = $activeSheetIndex;
				$objPHPExcel->addSheet( $newSheet, $newSheetIndex );
				$objPHPExcel->setActiveSheetIndex( $activeSheetIndex );
			}

			$dcs = $this->dc_model->getRecordsWhere( array( 'dc' => $key ) );

			$objWorksheet = $objPHPExcel->getActiveSheet();
			$objWorksheet->setTitle( $key );
			$objWorksheet->getCell('A3')->setValue( 'Week '.$week.' '.date('Y').' Delivery Schedule Update' );
			$objWorksheet->getCell('B5')->setValue( date('m/d/Y') );
			$objWorksheet->getCell('B6')->setValue( $dcs[0]['suburb'] );

			$i = 10;
			foreach( $value as $row ) {

				$styleRows = ( $i%2 ) ? '11': '10';

				$objWorksheet->getCell('A'.$i)->setValue( $row['contracts_sum_supplier'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'A'.$styleRows ), 'A'.$i);

				$objWorksheet->getCell('B'.$i)->setValue( $row['deliveries_sum_product_code'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'B'.$styleRows ), 'B'.$i);

				$objWorksheet->getCell('C'.$i)->setValue( $row['deliveries_sum_product_description'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'C'.$styleRows ), 'C'.$i);

				$objWorksheet->getCell('D'.$i)->setValue( $row['contracts_sum_on_sale_date'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'D'.$styleRows ), 'D'.$i);

				$objWorksheet->getCell('E'.$i)->setValue( $row['shipments_container_number'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'E'.$styleRows ), 'E'.$i);

				$objWorksheet->getCell('F'.$i)->setValue( $row['shipments_carrier'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'F'.$styleRows ), 'F'.$i);

				$objWorksheet->getCell('G'.$i)->setValue( $row['shipments_contract_type'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'G'.$styleRows ), 'G'.$i);

				$objWorksheet->getCell('H'.$i)->setValue( $row['shipments_mother_vessel_name'].', '.$row['shipments_voyage_number'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'H'.$styleRows ), 'H'.$i);

				$objWorksheet->getCell('I'.$i)->setValue( $row['shipments_etd'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'I'.$styleRows ), 'I'.$i);

				$objWorksheet->getCell('J'.$i)->setValue( $row['shipments_eta'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'J'.$styleRows ), 'J'.$i);

				$objWorksheet->getCell('K'.$i)->setValue( $row['shipments_detention_free_days'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'K'.$styleRows ), 'K'.$i);

				$objWorksheet->getCell('L'.$i)->setValue( $row['shipments_container_size'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'K'.$styleRows ), 'K'.$i);

				$objWorksheet->getCell('M'.$i)->setValue( '' );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'L'.$styleRows ), 'L'.$i);

				$objWorksheet->getCell('N'.$i)->setValue( $row['shipments_destination_port'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'M'.$styleRows ), 'M'.$i);

				$objWorksheet->getCell('O'.$i)->setValue( $row['shipments_loading_type'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'N'.$styleRows ), 'N'.$i);

				$objWorksheet->getCell('P'.$i)->setValue( $row['deliveries_sum_case_quantity'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'O'.$styleRows ), 'O'.$i);

				$objWorksheet->getCell('Q'.$i)->setValue( $row['deliveries_sum_case_quantity_other'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'P'.$styleRows ), 'P'.$i);

				$objWorksheet->getCell('R'.$i)->setValue( $row['deliveries_sum_case_quantity_MIN'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'Q'.$styleRows ), 'Q'.$i);

				$objWorksheet->getCell('S'.$i)->setValue( $row['deliveries_sum_case_quantity_DER'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'R'.$styleRows ), 'R'.$i);

				$objWorksheet->getCell('T'.$i)->setValue( $row['deliveries_sum_case_quantity_STP'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'S'.$styleRows ), 'S'.$i);

				$objWorksheet->getCell('U'.$i)->setValue( $row['deliveries_sum_case_quantity_PRE'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'T'.$styleRows ), 'T'.$i);

				$objWorksheet->getCell('V'.$i)->setValue( $row['deliveries_sum_case_quantity_DAN'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'U'.$styleRows ), 'U'.$i);

				$objWorksheet->getCell('W'.$i)->setValue( $row['deliveries_sum_case_quantity_BRE'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'V'.$styleRows ), 'V'.$i);

				$objWorksheet->getCell('X'.$i)->setValue( $row['deliveries_sum_case_quantity_RGY'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'W'.$styleRows ), 'W'.$i);

				$objWorksheet->getCell('Y'.$i)->setValue( $row['deliveries_sum_case_quantity_JKT'] );
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'X'.$styleRows ), 'X'.$i);

				$objWorksheet->getCell('Z'.$i)->setValue(implode( ",\n",$row['deliveries_sum_comment'] ) );
				$objWorksheet->getStyle('H5')->getAlignment()->setWrapText(true);
				$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'Y'.$styleRows ), 'Y'.$i);

				$objWorksheet->getRowDimension($i)->setRowHeight(-1);

				$i++;
			}

			$objWorksheet->getCell('K'.$i)->setValue( 'Total Containers:' );
			$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'K11' ), 'K'.$i);
			$objWorksheet->getCell('L'.$i)->setValue( $i-10 );
			$objWorksheet->duplicateStyle( $objWorksheet->getStyle( 'L11' ), 'L'.$i);

			$activeSheetIndex++;
		}

		return $objPHPExcel;
	}


	/**
	 * getFormattedCommentText
	 */
	private function getFormattedCommentText( $comments ){
		$regions = array();
		$i = 0;
		foreach( $comments as $comment ) {
			$region = substr($comment, 0, 3);
			$regions[] = $region;
		}
		$bold = ( count( array_unique( $regions ) ) > 1 ) ? true : false;

		$objRichText = new PHPExcel_RichText();
		foreach( $comments as $comment ) {
			$dc		= substr( $comment, 0, 3 );
			$color 	= $this->getCommentColor( $dc );
			$br		= ($i < count( $comments ) - 1) ? ",\n" : "";
			$run 	= $objRichText->createTextRun( $comment);
			$run->getFont()->getColor()->setRGB( $color );
			$run->getFont()->setBold( $bold );
			$objRichText->createText($br);
			$i++;
		}

		return $objRichText;
	}

	/**
	 * getCommentColor
	 */
	private function getCommentColor( $dc ) {
		$color = '000000';
		switch( $dc ) {
			case 'MIN';
				$color = '3bb057';
				break;
			case 'PRE';
				$color = '3E3E3E';
				break;
			case 'DER';
				$color = 'FF0000';
				break;
			case 'DAN';
				$color = 'FF0000';
				break;
			case 'RGY';
				$color = '1e66d3';
				break;
			case 'STP';
				$color = 'ff672b';
				break;
			case 'BRE';
				$color = 'ff672b';
				break;
			case 'JKT';
				$color = '802caa';
				break;
		}
		return $color;
	}

	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomVesselScheduleFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-Sale week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */