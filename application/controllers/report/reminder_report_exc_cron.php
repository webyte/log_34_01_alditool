<?php
class Reminder_report_exc_cron extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		//$this->output->enable_profiler(TRUE);
	}

	public function init() {

		$file 			= $this->config->item('archive_path').'Aldi_exception_report_'.random_string('alnum', 16).'.xlsx';

		$objPHPExcel 	= $this->generateObjPHPExcel();
		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel2007' );
		$objWriter->save( $file );

		$this->generateMail( $file );
	}

	/**
	 * generateExcelDoc
	 */
	private function generateObjPHPExcel() {

		$start 	= date( 'd/m/Y', strtotime( '-21 days', time() ) );
		$stop 	= date( 'd/m/Y', strtotime( '+7 days', time() ) );

		$recordListSearchParams = array(
  			'event' => array('eta'),
			'shipments_from' => array( $start ),
			'shipments_to' => array( $stop ),
		);

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$records 	= $this->model->getExcRecords( $recordListSearchParams, array() );
		$template 		= $this->config->item('app_path').'views/report/exc_template.xlsx';

		$objReader 		= new PHPExcel_Reader_Excel2007();
		$objPHPExcel 	= $objReader->load( $template );

		$objWorksheet 	= $objPHPExcel->getActiveSheet();
		$objWorksheet->getCell('D6')->setValue( date('d/m/Y') );
		$objWorksheet->getCell('D7')->setValue( date('H:i:s') );

		$offset = 10;
		$i = $offset;

		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)
		);

		foreach( $records as $record ) {
			$objWorksheet->getCell('A'.$i)->setValue( $record['contracts_advertisement_week'] );
			$objWorksheet->getCell('B'.$i)->setValue( $record['shipments_container_number'] );
			$objWorksheet->getCell('C'.$i)->setValue( $record['contracts_contract_number'] );
			$objWorksheet->getCell('D'.$i)->setValue( $record['contracts_fob_date'] );
			$objWorksheet->getCell('E'.$i)->setValue( $record['contracts_actual_contract_ready_date'] );
			$objWorksheet->getCell('F'.$i)->setValue( $record['shipments_etd'] );
			$objWorksheet->getCell('G'.$i)->setValue( $record['shipments_atd'] );
			$objWorksheet->getCell('H'.$i)->setValue( $record['shipments_eta'] );
			$objWorksheet->getCell('I'.$i)->setValue( $record['shipments_ata'] );
			$objWorksheet->getCell('J'.$i)->setValue( $record['shipments_invoice_click_date'] );
			$objWorksheet->getCell('K'.$i)->setValue( $record['shipments_packing_list_click_date'] );
			$objWorksheet->getCell('L'.$i)->setValue( $record['shipments_packing_declaration_click_date'] );
			$objWorksheet->getCell('M'.$i)->setValue( $record['shipments_treatment_certificate_click_date'] );
			$objWorksheet->getCell('N'.$i)->setValue( $record['shipments_deliveries_mm_telex_first_reminder_sent'] );
			$objWorksheet->getCell('O'.$i)->setValue( $record['shipments_deliveries_mm_telex_second_reminder_sent'] );
			$objWorksheet->getCell('P'.$i)->setValue( $record['shipments_bl_click_date'] );
			$objWorksheet->getCell('Q'.$i)->setValue( $record['shipments_certificate_of_origin_click_date'] );
			$objWorksheet->getCell('R'.$i)->setValue( '' );
			$objWorksheet->getCell('S'.$i)->setValue( '' );

			$objWorksheet->getStyle('A'.$i.':S'.$i)->applyFromArray($styleArray);

			$i++;
		}

		return $objPHPExcel;

	}

	/**
	 * generateMail
	 * @return
	 */
	private function generateMail( $file ) {
		$this->load->helper( 'str_helper' );
		$mailData						= array();
		$mailData['CONTAINER_NUMBER'] 	= '';
		$mailData['ETA'] 				= '';

		$data 				= array();
		$data['subject'] 	= replacePattern( $this->lang->line( 'report_exc.reminder.mail.subject' ), $mailData );
		$data['body'] 		= replacePattern( $this->lang->line( 'report_exc.reminder.mail.body' ), $mailData );
		$data['file']		= $file;
		$data['recipient'] 	= array();
		$data['cc'] 		= array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');

		if( ENVIRONMENT != 'production' ) $data['recipient'] = array('Stefanie.Stahl@logwin-logistics.com','or@webyte.org');
		if( !empty( $data['recipient'] ) ) $this->sendMail( $data );

	}


	/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$cc			= ( isset( $data['cc'] ) ) ? $data['cc'] : '';
		$subject 	= $data['subject'];
		$body 		= $data['body'];
		$file 		= $data['file'];

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		$this->email->attach( $file );

		if( !empty( $cc ) ) $this->email->cc( $cc );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Reminder_report_exc_cron.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Reminder_report_exc_cron.sendMail' );
			return true;
		}
		$this->email->clear(TRUE);
	}

}

/* End of file reminder_cron.php */
/* Location: ./app/controllers/reminder_cron.php */