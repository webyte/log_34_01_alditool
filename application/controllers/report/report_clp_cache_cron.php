<?php
class Report_clp_cache_cron extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_clp_po_model', 'report_clp_po_model');
		//$this->output->enable_profiler(TRUE);
	}

	public function init() {
		$this->report_clp_po_model->generateCache();
	}


}

/* End of file reminder_cron.php */
/* Location: ./app/controllers/dashboard_cron.php */