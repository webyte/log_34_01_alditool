<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_crd_vs_atd extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_crd_vs_atd', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 		= $this->getCustomStatusFormFields();
		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_crd_vs_atd/status_crd_vs_atd_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Status CRD versus ATD';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * generateReportAndSendByMail
	 * @return
	 */
	public function generateReportAndSendByMail( $contractType ) {
		$userId = $this->session->userdata('userId');
		$wwwPath = $this->config->item('www_path');
		$cmd = "/usr/bin/php5.6 ".$wwwPath."index.php report/report_crd_vs_atd_cron excel_report_mail ".$userId." ".$contractType." > /dev/null 2>/dev/null &";

		$this->welogger->log( $cmd, WELogger::$LOG_LEVEL_INFO, 'Report_crd_vs_atd.generateReportAndSendByMail' );

		shell_exec( $cmd );
		$this->index();
	}

	/**
	 * order_report_export
	 * @return
	 */
	public function status_crd_vs_atd_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams 	= $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel2007' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'Logwin_CRD_vs_ATD_Report.xlsx' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}



	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->removeSheetByIndex( 0 );

		// Worksheet "Pending"
		$searchParams = $recordListSearchParams;
		$searchParams['contracts_aldi_au_wh_delivery_due_date_from'] = array(  date( 'Y-m-d H:i:s' ) );
		$searchParams['shipments_id'] = array( NULL );
		$searchParams['deliveries_empty_case_quantity'] = array( true );
		$this->generateWorksheet( $objPHPExcel, 0, 'Pending', $searchParams);

		// Worksheet "Shipping"
		$searchParams = $recordListSearchParams;
		$searchParams['shipments_ata'] = array( '0000-00-00 00:00:00' );
		$searchParams['shipments_crdate_from'] = array( '2019-01-01 00:00:00' );
		$this->generateWorksheet( $objPHPExcel, 1, 'Shipping', $searchParams);

		// Worksheet "Arrived"
		$searchParams = $recordListSearchParams;
		$searchParams['shipments_ata_from'] = array(  date( 'Y-m-01 00:00:00', strtotime( '-3 months', time() ) ) );
		$searchParams['shipments_ata_to'] = array( date('Y-m-t 23:59:59' ) );
		$this->generateWorksheet( $objPHPExcel, 2, 'Arrived', $searchParams);

		$objPHPExcel->setActiveSheetIndex( 0 );

		return $objPHPExcel;
	}



	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomStatusFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract type";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_type', $this->getSearchFormValue('contracts_contract_type'), '1');
		$res[] = $tmp;

		return $res;
	}


	/**
	 * generateWorksheet
	 */
	private function generateWorksheet( &$objPHPExcel, $activeSheetIndex, $title, $searchParams ) {

		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex( $activeSheetIndex );
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$objWorksheet->setTitle( $title );
		$cols = array();

		// pending
		if(
			$activeSheetIndex == 0
		) {
			$cols[0] = array( 'row' => 'A', 'title' => 'Supplier', 										'field' => 'contracts_supplier' );
			$cols[1] = array( 'row' => 'B', 'title' => 'Region', 										'field' => 'deliveries_dc' );
			$cols[2] = array( 'row' => 'C', 'title' => 'ALDI Purchase Order-No', 						'field' => 'contracts_contract_number' );
			$cols[3] = array( 'row' => 'D', 'title' => 'Date Received Copy of Fax Order from ALDI Region','field' => 'contracts_crdate' );
			$cols[4] = array( 'row' => 'E', 'title' => 'Traffic Type', 									'field' => 'contracts_traffic_type' );
			$cols[5] = array( 'row' => 'F', 'title' => 'Required Delivery Date into ALDI Region', 		'field' => 'contracts_aldi_au_wh_delivery_due_date' );
			$cols[6] = array( 'row' => 'G', 'title' => 'Actual RIS Date (column E + 1 working day rule)', 'field' => 'contracts_ris_date' );
			$cols[7] = array( 'row' => 'H', 'title' => 'Intended PickUp Date / Cargo Ready Date', 		'field' => 'contracts_fob_date' );
			$cols[8] = array( 'row' => 'I', 'title' => 'Actual Cargo Ready Date (Initial)', 			'field' => 'contracts_actual_cargo_ready_date' );
			$cols[9] = array( 'row' => 'J', 'title' => 'Actual PickUp Date / Cargo Ready Date', 		'field' => 'contracts_actual_contract_ready_date' );
			$cols[10] = array( 'row' => 'K', 'title' => 'Supplier Early/Late By', 						'field' => 'contracts_supplier_kpi' );
			$cols[11] = array( 'row' => 'L', 'title' => 'Received in CFS', 								'field' => 'contracts_received_in_cfs' );
			$cols[12] = array( 'row' => 'M', 'title' => 'Product Description', 							'field' => 'deliveries_product_description' );
			$cols[13] = array( 'row' => 'N', 'title' => 'Product Code', 								'field' => 'deliveries_product_code' );
			$cols[14] = array( 'row' => 'O', 'title' => 'No of Cases', 									'field' => 'deliveries_case_quantity' );
			$cols[15] = array( 'row' => 'P', 'title' => 'Vessel Name', 									'field' => 'shipments_mother_vessel_name' );
			$cols[16] = array( 'row' => 'Q', 'title' => 'Port of Loading', 								'field' => 'shipments_departure_port' );
			$cols[17] = array( 'row' => 'R', 'title' => 'Initial ETD', 									'field' => 'pcshipments_ietd' );
			$cols[18] = array( 'row' => 'S', 'title' => 'Estimated Vessel Sailing Date', 				'field' => 'shipments_etd' );
			$cols[19] = array( 'row' => 'T', 'title' => 'Actual Vessel Sailing Date', 					'field' => 'shipments_atd' );
			$cols[20] = array( 'row' => 'U', 'title' => 'Initial ETA', 									'field' => 'pcshipments_ieta' );
			$cols[21] = array( 'row' => 'V', 'title' => 'ETA Required Australian Port', 				'field' => 'shipments_eta' );
			$cols[22] = array( 'row' => 'W', 'title' => 'Estimated Delivery Date into Region', 			'field' => 'shipments_delivery_date' );
			$cols[23] = array( 'row' => 'X', 'title' => 'Early/Late By', 								'field' => 'shipments_early_late' );
			$cols[24] = array( 'row' => 'Y', 'title' => 'Container Type', 								'field' => 'shipments_container_size_weight' );
			$cols[25] = array( 'row' => 'Z', 'title' => 'Container No', 								'field' => 'shipments_container_number' );
			$cols[26] = array( 'row' => 'AA', 'title' => 'Remarks', 									'field' => 'shipments_remarks' );

			// https://redmine.webyte.org/issues/20157
			// Anpassungen für die Abfrage (contract type) nach Special Buy:
			// - Spalte E(Required Delivery Date into ALDI Region) durch "delivery due date" (entspricht "ALDI AU WH delivery due date" im Frontend) auf allen 3 Reitern ersetzen
			// - Spalte F(Actual RIS Date (column E + 1 working day rule)) durch "delivery due date end" (entspricht "ALDI AU WH delivery due end date" im Frontend) auf allen 3 Reitern ersetzen
			// - in der letzten Spalte (nach Remarks) noch "on- sale week" auf allen 3 Reitern setzen
			if(
				$searchParams['contracts_contract_type'][0] == Contract_model::$CONTRACT_TYPE_SPECIAL_BUYS
			) {
				$cols[5] = array( 'row' => 'F', 'title' => 'Delivery Due Date', 'field' => 'contracts_aldi_au_wh_delivery_due_date' );
				$cols[6] = array( 'row' => 'G', 'title' => 'Delivery Due Date End', 'field' => 'contracts_aldi_au_wh_delivery_due_end_date' );
				$cols[27] = array( 'row' => 'AB', 'title' => 'On-Sale Week', 'field' => 'contracts_advertisement_week' );
			}

		}

		// shipping
		if(
			$activeSheetIndex == 1
		) {
			$cols[0] = array( 'row' => 'A', 'title' => 'Supplier', 										'field' => 'contracts_supplier' );
			$cols[1] = array( 'row' => 'B', 'title' => 'Region', 										'field' => 'deliveries_dc' );
			$cols[2] = array( 'row' => 'C', 'title' => 'ALDI Purchase Order-No', 						'field' => 'contracts_contract_number' );
			$cols[3] = array( 'row' => 'D', 'title' => 'Date Received Copy of Fax Order from ALDI Region','field' => 'contracts_crdate' );
			$cols[4] = array( 'row' => 'E', 'title' => 'Traffic Type', 									'field' => 'contracts_traffic_type' );
			$cols[5] = array( 'row' => 'F', 'title' => 'Required Delivery Date into ALDI Region', 		'field' => 'contracts_aldi_au_wh_delivery_due_date' );
			$cols[6] = array( 'row' => 'G', 'title' => 'Actual RIS Date (column E + 1 working day rule)', 'field' => 'contracts_ris_date' );
			$cols[7] = array( 'row' => 'H', 'title' => 'Intended PickUp Date / Cargo Ready Date', 		'field' => 'contracts_fob_date' );
			$cols[8] = array( 'row' => 'I', 'title' => 'Actual Cargo Ready Date (Initial)', 			'field' => 'contracts_actual_cargo_ready_date' );
			$cols[9] = array( 'row' => 'J', 'title' => 'Actual PickUp Date / Cargo Ready Date', 		'field' => 'contracts_actual_contract_ready_date' );
			$cols[10] = array( 'row' => 'K', 'title' => 'CFSO Opening Date', 							'field' => 'contracts_cfso_opening_date' );
			$cols[11] = array( 'row' => 'L', 'title' => 'Supplier Early/Late By', 						'field' => 'contracts_supplier_kpi' );
			$cols[12] = array( 'row' => 'M', 'title' => 'Received in CFS', 								'field' => 'contracts_received_in_cfs' );
			$cols[13] = array( 'row' => 'N', 'title' => 'CFS Out', 										'field' => 'shipments_container_gate_in' );
			$cols[14] = array( 'row' => 'O', 'title' => 'Product Description', 							'field' => 'deliveries_product_description' );
			$cols[15] = array( 'row' => 'P', 'title' => 'Product Code', 								'field' => 'deliveries_product_code' );
			$cols[16] = array( 'row' => 'Q', 'title' => 'No of Cases', 									'field' => 'deliveries_case_quantity' );
			$cols[17] = array( 'row' => 'R', 'title' => 'Vessel Name', 									'field' => 'shipments_mother_vessel_name' );
			$cols[18] = array( 'row' => 'S', 'title' => 'Port of Loading', 								'field' => 'shipments_departure_port' );
			$cols[19] = array( 'row' => 'T', 'title' => 'Initial ETD', 									'field' => 'pcshipments_ietd' );
			$cols[20] = array( 'row' => 'U', 'title' => 'Estimated Vessel Sailing Date', 				'field' => 'shipments_etd' );
			$cols[21] = array( 'row' => 'V', 'title' => 'Actual Vessel Sailing Date', 					'field' => 'shipments_atd' );
			$cols[22] = array( 'row' => 'W', 'title' => 'Initial ETA', 									'field' => 'pcshipments_ieta' );
			$cols[23] = array( 'row' => 'X', 'title' => 'ETA Required Australian Port', 				'field' => 'shipments_eta' );
			$cols[24] = array( 'row' => 'Y', 'title' => 'Estimated Delivery Date into Region', 			'field' => 'shipments_delivery_date' );
			$cols[25] = array( 'row' => 'Z', 'title' => 'Early/Late By', 								'field' => 'shipments_early_late' );
			$cols[26] = array( 'row' => 'AA', 'title' => 'Container Type', 								'field' => 'shipments_container_size_weight' );
			$cols[27] = array( 'row' => 'AB', 'title' => 'Container No', 								'field' => 'shipments_container_number' );
			$cols[28] = array( 'row' => 'AC', 'title' => 'Remarks', 										'field' => 'shipments_remarks' );

			// https://redmine.webyte.org/issues/20157
			// Anpassungen für die Abfrage (contract type) nach Special Buy:
			// - Spalte E(Required Delivery Date into ALDI Region) durch "delivery due date" (entspricht "ALDI AU WH delivery due date" im Frontend) auf allen 3 Reitern ersetzen
			// - Spalte F(Actual RIS Date (column E + 1 working day rule)) durch "delivery due date end" (entspricht "ALDI AU WH delivery due end date" im Frontend) auf allen 3 Reitern ersetzen
			// - in der letzten Spalte (nach Remarks) noch "on- sale week" auf allen 3 Reitern setzen
			if(
				$searchParams['contracts_contract_type'][0] == Contract_model::$CONTRACT_TYPE_SPECIAL_BUYS
			) {
				$cols[5] = array( 'row' => 'F', 'title' => 'Delivery Due Date', 'field' => 'contracts_aldi_au_wh_delivery_due_date' );
				$cols[6] = array( 'row' => 'G', 'title' => 'Delivery Due Date End', 'field' => 'contracts_aldi_au_wh_delivery_due_end_date' );
				$cols[29] = array( 'row' => 'AD', 'title' => 'On-Sale Week', 'field' => 'contracts_advertisement_week' );
			}

			// https://redmine.webyte.org/issues/20157
			// Anpassungen für die Abfrage (contract type) nach Core Range
			// Spalte "early /late by" aus dem Reiter "Shipping" muss entfernt werden
			if(
				$searchParams['contracts_contract_type'][0] == Contract_model::$CONTRACT_TYPE_CORE_RANGE
			) {
				$cols[26] = array( 'row' => 'AA', 'title' => 'Container Type', 	'field' => 'shipments_container_size_weight' );
				$cols[27] = array( 'row' => 'AB', 'title' => 'Container No', 	'field' => 'shipments_container_number' );
				$cols[28] = array( 'row' => 'AC', 'title' => 'Remarks', 			'field' => 'shipments_remarks' );
				$cols[29] = array( 'row' => 'AD', 'title' => 'On-Sale Week', 'field' => 'contracts_advertisement_week' );
			}
		}

		// arrived
		if(
			$activeSheetIndex == 2
		) {
			$cols[0] = array( 'row' => 'A', 'title' => 'Supplier', 										'field' => 'contracts_supplier' );
			$cols[1] = array( 'row' => 'B', 'title' => 'Region', 										'field' => 'deliveries_dc' );
			$cols[2] = array( 'row' => 'C', 'title' => 'ALDI Purchase Order-No', 						'field' => 'contracts_contract_number' );
			$cols[3] = array( 'row' => 'D', 'title' => 'Date Received Copy of Fax Order from ALDI Region','field' => 'contracts_crdate' );
			$cols[4] = array( 'row' => 'E', 'title' => 'Traffic Type', 									'field' => 'contracts_traffic_type' );
			$cols[5] = array( 'row' => 'F', 'title' => 'Required Delivery Date into ALDI Region', 		'field' => 'contracts_aldi_au_wh_delivery_due_date' );
			$cols[6] = array( 'row' => 'G', 'title' => 'Actual RIS Date (column E + 1 working day rule)', 'field' => 'contracts_ris_date' );
			$cols[7] = array( 'row' => 'H', 'title' => 'Intended PickUp Date / Cargo Ready Date', 		'field' => 'contracts_fob_date' );
			$cols[8] = array( 'row' => 'I', 'title' => 'Actual Cargo Ready Date (Initial)', 			'field' => 'contracts_actual_cargo_ready_date' );
			$cols[9] = array( 'row' => 'J', 'title' => 'Actual PickUp Date / Cargo Ready Date', 		'field' => 'contracts_actual_contract_ready_date' );
			$cols[10] = array( 'row' => 'K', 'title' => 'Supplier Early/Late By', 						'field' => 'contracts_supplier_kpi' );
			$cols[11] = array( 'row' => 'L', 'title' => 'Received in CFS', 								'field' => 'contracts_received_in_cfs' );
			$cols[12] = array( 'row' => 'M', 'title' => 'CFS Out', 										'field' => 'shipments_container_gate_in' );
			$cols[13] = array( 'row' => 'N', 'title' => 'Product Description', 							'field' => 'deliveries_product_description' );
			$cols[14] = array( 'row' => 'O', 'title' => 'Product Code', 								'field' => 'deliveries_product_code' );
			$cols[15] = array( 'row' => 'P', 'title' => 'No of Cases', 									'field' => 'deliveries_case_quantity' );
			$cols[16] = array( 'row' => 'Q', 'title' => 'Vessel Name', 									'field' => 'shipments_mother_vessel_name' );
			$cols[17] = array( 'row' => 'R', 'title' => 'Port of Loading', 								'field' => 'shipments_departure_port' );
			$cols[18] = array( 'row' => 'S', 'title' => 'Initial ETD', 									'field' => 'pcshipments_ietd' );
			$cols[19] = array( 'row' => 'T', 'title' => 'Estimated Vessel Sailing Date', 				'field' => 'shipments_etd' );
			$cols[20] = array( 'row' => 'U', 'title' => 'Actual Vessel Sailing Date', 					'field' => 'shipments_atd' );
			$cols[21] = array( 'row' => 'V', 'title' => 'ETA Required Australian Port', 				'field' => 'shipments_eta' );
			$cols[22] = array( 'row' => 'W', 'title' => 'ATA Australian Port', 							'field' => 'shipments_ata' );
			$cols[23] = array( 'row' => 'X', 'title' => 'Initial ETA', 									'field' => 'pcshipments_ieta' );
			$cols[24] = array( 'row' => 'Y', 'title' => 'Estimated Delivery Date into Region', 			'field' => 'shipments_delivery_date' );
			$cols[25] = array( 'row' => 'Z', 'title' => 'Early/Late By', 								'field' => 'shipments_early_late' );
			$cols[26] = array( 'row' => 'AA', 'title' => 'Container Type', 								'field' => 'shipments_container_size_weight' );
			$cols[27] = array( 'row' => 'AB', 'title' => 'Container No', 								'field' => 'shipments_container_number' );
			$cols[28] = array( 'row' => 'AC', 'title' => 'Remarks', 										'field' => 'shipments_remarks' );

			// https://redmine.webyte.org/issues/20157
			// Anpassungen für die Abfrage (contract type) nach Special Buy:
			// - Spalte E(Required Delivery Date into ALDI Region) durch "delivery due date" (entspricht "ALDI AU WH delivery due date" im Frontend) auf allen 3 Reitern ersetzen
			// - Spalte F(Actual RIS Date (column E + 1 working day rule)) durch "delivery due date end" (entspricht "ALDI AU WH delivery due end date" im Frontend) auf allen 3 Reitern ersetzen
			// - in der letzten Spalte (nach Remarks) noch "on- sale week" auf allen 3 Reitern setzen
			if(
				$searchParams['contracts_contract_type'][0] == Contract_model::$CONTRACT_TYPE_SPECIAL_BUYS
			) {
				$cols[5] = array( 'row' => 'F', 'title' => 'Delivery Due Date', 'field' => 'contracts_aldi_au_wh_delivery_due_date' );
				$cols[6] = array( 'row' => 'G', 'title' => 'Delivery Due Date End', 'field' => 'contracts_aldi_au_wh_delivery_due_end_date' );
				$cols[29] = array( 'row' => 'AD', 'title' => 'On-Sale Week', 'field' => 'contracts_advertisement_week' );
			}
		}

		foreach( $cols as $col ) {
			$objWorksheet->getCell($col['row'].'1')->setValue( $col['title'] );
		}

		if( $title == 'Pending' ) {
			$records = $this->model->getStatusRecords( $searchParams, array() );
		} else {
			$records = $this->model->getCRDvsATDRecords( $searchParams, array() );
		}

		$i = 2;

		foreach( $records as $row ) {
			foreach( $cols as $col ) {
				$objWorksheet->getCell($col['row'].$i)->setValue($row[$col['field']]);
			}
			$i++;
		}

		foreach( $cols as $col ) {
			$objWorksheet->getColumnDimension($col['row'])->setAutoSize(true);
		}

		return $objWorksheet;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */