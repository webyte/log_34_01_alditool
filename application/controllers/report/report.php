<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );
//require_once( BASEPATH.'../../phpdocx_pro/classes/CreateDocx.inc' );
class Report extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$data['cdaResult'] = '';
		$this->load->view('report/report_overview', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 		= $this->getCustomSearchFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report/csv_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'General report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}

	/**
	 * csv_report_export
	 * @return
	 */
	public function csv_report_export(){
		$this->recordListSearchParams 	= $this->input->post();
		$this->records					= $this->model->getReportRecords( $this->recordListSearchParams, array() );

		$data = array(
			'contracts_contract_number' => 'Contract number/Purchase order number',
			'deliveries_product_code' => 'Product code',
			'deliveries_product_description' => 'Description',
			'contracts_departure' => 'Departure Port',
			'shipments_destination_port' => 'Destination Port',
			'shipments_container_number' => 'Container number',
			'shipments_container_size' => 'Container size',
			'shipments_container_type' => 'Container type',
			'shipments_loading_type' => 'Loading type',
			'shipments_deliveries_mm_hbl_hawb' => 'HAWB/HBL',
			'shipments_feeder_vessel_name' => 'Feeder vessel',
			'shipments_mother_vessel_name' => 'Mother vessel',
			'shipments_voyage_number' => 'Voyage',
			'deliveries_case_quantity' => 'Case quantity',
			'deliveries_unit_quantity' => 'Unit quantity',
			'contracts_units_per_case' => 'Units per case',
			'deliveries_volume' => 'Case cubage',
			'deliveries_gross_weight' => 'Case weight',
			'deliveries_dc' => 'DC',
			'contracts_supplier' => 'Supplier',
			'contracts_logwin_origin_office' => 'Logwin origin office',
			'contracts_traffic_type' => 'Traffic Type',
			'shipments_incoterm_code' => 'Incoterm Code',
			'contracts_on_sale_date' => 'On sale date',
			'contracts_fob_date' => 'FOB date',
			'contracts_aldi_au_wh_delivery_due_date' => 'Aldi AU WH delivery due date',
			'contracts_ofu_scheduled' => 'OFU scheduled',
			'contracts_ofu_real' => 'OFU actual',
			'pcshipments_ietd' => 'IETD',
			'shipments_etd' => 'ETD',
			'pcshipments_ieta' => 'IETA',
			'shipments_eta' => 'ETA',
			'shipments_atd' => 'ATD',
			'shipments_ata' => 'ATA',
			'shipments_cus' => 'CUS',
			'shipments_deliveries_mm_edl' => 'EDL',
			'shipments_deliveries_mm_adl' => 'ADL',
			'contracts_actual_contract_ready_date' => 'Actual contract ready date',
		);

		$out 		= "";
		$delimiter 	= "|";
		$newline 	= "\r\n";

		// header
		foreach( $data as $key=>$val ) {
			$out .= $val.$delimiter;
		}
		$out .= $newline;

		// body
		foreach ( $this->records as $row ) {
			foreach( $data as $key=>$val ) {
				$out .= $row[$key].$delimiter;
			}
			$out .= $newline;
		}

		$this->output->set_header('Content-Type: text/x-csv');
		$this->output->set_header('Content-Disposition: attachment; filename=logwin_aldi_report.csv');
		$this->output->set_header('Cache-Control: max-age=0');
		$this->output->set_output( utf8_decode( $out ) );
		//var_dump($out);
	}


	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {

		// ist nur ne quick&dirty Lösung!
		// muss man mal ordentlich machen...

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "HBL/HAWB";
		$tmp['input'] = '<input type="text" id="shipments_hbl_hawb" value="'.$this->getSearchFormValue('shipments_hbl_hawb').'" name="shipments_hbl_hawb[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = '<input type="text" id="shipments_container_number" value="'.$this->getSearchFormValue('shipments_container_number').'" name="shipments_container_number[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Departure";
		$tmp['input'] = $this->tca->getFormInputById('contracts_departure', $this->getSearchFormValue('contracts_departure'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Destination";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_destination', $this->getSearchFormValue('deliveries_destination'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Traffic type";
		$tmp['input'] = form_dropdown( 'contracts_traffic_type[]', array( "" => "please select ...", "1" => "Factory", "2" => "CFS", "3" => "AIR", "4" => "PTS Loading", "5" => "Factory/CFS" ), $this->getSearchFormValue('contracts_traffic_type') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'event[]', array( "" => "please select ...", "etd" => "ETD", "eta" => "ETA", "cus" => "CUS", "atd" => "ATD", "ata" => "ATA", "fob_date" => "FOB Date" ), $this->getSearchFormValue('event') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_from" value="'.$this->getSearchFormValue('shipments_from').'" name="shipments_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_to" value="'.$this->getSearchFormValue('shipments_to').'" name="shipments_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-Sale week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Logwin origin office";
		$tmp['input'] = $this->tca->getFormInputById('contracts_logwin_origin_office', $this->getSearchFormValue('contracts_logwin_origin_office'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Supplier";
		$tmp['input'] = $this->tca->getFormInputById('contracts_supplier', $this->getSearchFormValue('contracts_supplier'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Incoterm";
		$tmp['input'] = form_dropdown( 'shipments_incoterm_code[]', array( "" => "please select ...", "1" => "EXW", "2" => "FOB", "3" => "DDP" ), $this->getSearchFormValue('shipments_incoterm_code') );
		$res[] = $tmp;

		return $res;
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


}

/* End of file report.php */
/* Location: ./app/controllers/report.php */