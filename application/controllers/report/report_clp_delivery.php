<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_clp_delivery extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= 'CLP Delivery';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_clp_delivery', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 			= $this->getCustomOrderFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_clp_delivery/clp_delivery_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'CLP delivery report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * clp_delivery_report_export
	 * @return
	 */
	public function clp_delivery_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams = $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );
		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel2007' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'clp_delivery_report.xlsx' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}


	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$this->records 	= $this->model->getCLPDeliveryRecords( $recordListSearchParams, array() );

		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		$objWorksheet->getCell('A1')->setValue( 'Container number' );
		$objWorksheet->getCell('B1')->setValue( 'Contract number' );
		$objWorksheet->getCell('C1')->setValue( 'Product number' );
		$objWorksheet->getCell('D1')->setValue( 'DC' );
		$objWorksheet->getCell('E1')->setValue( 'HeaderData-DC' );
		$objWorksheet->getCell('F1')->setValue( 'On-sales week' );
		$objWorksheet->getCell('G1')->setValue( 'Information' );

		$i = 2;
		foreach( $this->records as $row ) {
			$objWorksheet->getCell('A'.$i)->setValue( $row['pcshipments_container_number'] );
			$objWorksheet->getCell('B'.$i)->setValue( $row['contracts_contract_number'] );
			$objWorksheet->getCell('C'.$i)->setValue( $row['deliveries_product_code'] );
			$objWorksheet->getCell('D'.$i)->setValue( $row['deliveries_dcs'] );
			$objWorksheet->getCell('E'.$i)->setValue( $row['shipments_dc'] );
			$objWorksheet->getCell('F'.$i)->setValue( $row['deliveries_advertisement_weeks'] );
			$objWorksheet->getCell('G'.$i)->setValue( $row['information'] );
			$i++;
		}

		return $objPHPExcel;
	}


	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomOrderFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = $this->tca->getFormInputById('pcshipments_container_number', $this->getSearchFormValue('pcshipments_container_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('contracts_product_code', $this->getSearchFormValue('contracts_product_code'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "HeaderData-DC";
		$tmp['input'] = $this->tca->getFormInputById('shipments_dc', $this->getSearchFormValue('shipments_dc'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-sales week";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_advertisement_week', $this->getSearchFormValue('deliveries_advertisement_week'), '1');
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */