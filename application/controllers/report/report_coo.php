<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_coo extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->headline	= 'CoO-Report';
//		$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_kpi', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 		= $this->getCustomOrderFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_coo/coo_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'CoO-Report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * coo_report_export
	 * @return
	 */
	public function coo_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams = $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );
		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'coo-report.xls' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}


	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$this->records 	= $this->model->getCOORecords( $recordListSearchParams, array() );

		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();




		$objWorksheet->getCell('A1')->setValue( 'Contract Number' );
		$objWorksheet->getCell('B1')->setValue( 'Product Code' );
		$objWorksheet->getCell('C1')->setValue( 'Product Description' );
		$objWorksheet->getCell('D1')->setValue( 'Supplier' );
		$objWorksheet->getCell('E1')->setValue( 'Country' );
		$objWorksheet->getCell('F1')->setValue( 'CoO/DoO received' );
		$objWorksheet->getCell('G1')->setValue( 'Duty Refund' );

		$i = 2;
		foreach( $this->records as $row ) {
			$objWorksheet->getCell('A'.$i)->setValue( $row['contracts_contract_number'] );
			$objWorksheet->getCell('B'.$i)->setValue( $row['contracts_product_code'] );
			$objWorksheet->getCell('C'.$i)->setValue( $row['contracts_product_description'] );
			$objWorksheet->getCell('D'.$i)->setValue( $row['contracts_supplier'] );
			$objWorksheet->getCell('E'.$i)->setValue( $row['pcshipments_departure_port'] );
			$objWorksheet->getCell('F'.$i)->setValue( $row['pcshipments_certificate_of_origin_received'] );
			$objWorksheet->getCell('G'.$i)->setValue( $row['pcshipments_duty_refund_completed'] );
			$i++;
		}

		return $objPHPExcel;
	}


	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomOrderFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'pcevent[]', array( "" => "please select ...", "etd" => "ETD", "eta" => "ETA" ), $this->getSearchFormValue('pcevent') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="pcshipments_from" value="'.$this->getSearchFormValue('pcshipments_from').'" name="pcshipments_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="pcshipments_to" value="'.$this->getSearchFormValue('pcshipments_to').'" name="pcshipments_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-sales week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract no.";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */