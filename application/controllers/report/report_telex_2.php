<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_telex_2 extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_telex_2', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 		= $this->getCustomTelexFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_telex_2/telex_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Late telex release report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * order_report_export
	 * @return
	 */
	public function telex_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams = $this->input->post();
		$objPHPExcel 			= $this->generateObjPHPExcel( $recordListSearchParams );
		$objWriter 				= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'Late_telex_release_report.xls' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}


	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$this->records 		= $this->model->getTelex2Records( $recordListSearchParams, array() );
		$objPHPExcel 		= new PHPExcel();
		$objWorksheet 		= $objPHPExcel->getActiveSheet();
		$i					= 2;
		$objWorksheet->getCell('A1')->setValue( 'Supplier' );
		$objWorksheet->getCell('B1')->setValue( 'Contract/PO number' );
		$objWorksheet->getCell('C1')->setValue( 'ETA' );
		$objWorksheet->getCell('D1')->setValue( 'Change Date' );

		foreach( $this->records as $row ) {
			$objWorksheet->getCell('A'.$i)->setValue( $row['supplier'] );
			$objWorksheet->getCell('B'.$i)->setValue( $row['contract_number'] );
			$objWorksheet->getCell('C'.$i)->setValue( $row['eta'] );
			$objWorksheet->getCell('D'.$i)->setValue( $row['crdate'] );
			$i++;
		}

		return $objPHPExcel;
	}

	/**
	 * getCustomTelexFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomTelexFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="telex_from" value="'.$this->getSearchFormValue('telex_from').'" name="telex_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="telex_to" value="'.$this->getSearchFormValue('telex_to').'" name="telex_to[]">';
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */