<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_dimension extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= '';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_dimension', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 			= $this->getCustomOrderFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_dimension/dimension_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Dimension Report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * dimension_report_export
	 * @return
	 */
	public function dimension_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams 	= $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'Logwin_Aldi_Dimension_Report.xls' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}



	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$cells = array(
			'activities_contract_number' => 'Contract #',
			'activities_record_field_label' => 'Dimension change',
			'activities_record_data_old' => 'Old value',
			'activities_record_data_new' => 'New Value',
			'activities_action_type' => 'Action Type',
			'activities_crdate' => 'Date',
		);
		$alphas = range('A', 'Z');

		$this->records	= $this->model->getDimensionRecords( $recordListSearchParams, array() );
		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		$cell = 0;
		foreach( $cells as $key=>$val ) {
			$objWorksheet->getCell( $alphas[$cell].'1')->setValue( $val );
			$objWorksheet->getColumnDimension( $alphas[$cell] )->setAutoSize(true);
			$cell++;
		}

		$row = 2;
		foreach( $this->records as $data ) {
			$cell = 0;
			foreach( $cells as $key=>$val ) {
				$objWorksheet->getCell($alphas[$cell].$row)->setValue( $data[$key] );
				$cell++;
			}
			$row++;
		}

		return $objPHPExcel;
	}


	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomOrderFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Date of change from";
		$tmp['input'] = '<input type="text" class="datepicker" id="crdate_from" value="'.$this->getSearchFormValue('crdate_from').'" name="crdate_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Date of change to";
		$tmp['input'] = '<input type="text" class="datepicker" id="crdate_to" value="'.$this->getSearchFormValue('crdate_to').'" name="crdate_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Dimension change";
		$tmp['input'] = form_dropdown( 'dimension_type[]', array( "" => "All", "case_length" => "Case length", "case_height" => "Case height", "case_width" => "Case width" ), $this->getSearchFormValue('dimension_type') );
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */