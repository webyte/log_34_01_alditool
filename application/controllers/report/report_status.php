<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_status extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_status', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 			= $this->getCustomStatusFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_status/status_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Status report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * order_report_export
	 * @return
	 */
	public function status_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams 	= $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel2007' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'Logwin_Status_Report.xlsx' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}



	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->removeSheetByIndex( 0 );

		// Worksheet "Pending"
		$searchParams = $recordListSearchParams;
		$searchParams['contracts_aldi_au_wh_delivery_due_date_from'] = array(  date( 'Y-m-d H:i:s' ) );
		$searchParams['shipments_id'] = array( NULL );
		$searchParams['deliveries_empty_case_quantity'] = array( true );
		$this->generateWorksheet( $objPHPExcel, 0, 'Pending', $searchParams);

		// Worksheet "Shipping"
		$searchParams = $recordListSearchParams;
		$searchParams['shipments_ata'] = array( '0000-00-00 00:00:00' );
		$searchParams['shipments_crdate_from'] = array( '2019-01-01 00:00:00' );
		$this->generateWorksheet( $objPHPExcel, 1, 'Shipping', $searchParams);

		// Worksheet "Arrived"
		$searchParams = $recordListSearchParams;
		$searchParams['shipments_ata_from'] = array(  date( 'Y-m-01 00:00:00', strtotime( '-1 months', time() ) ) );
		$searchParams['shipments_ata_to'] = array( date('Y-m-t 23:59:59' ) );
		$this->generateWorksheet( $objPHPExcel, 2, 'Arrived', $searchParams);

		$objPHPExcel->setActiveSheetIndex( 0 );

		return $objPHPExcel;
	}



	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomStatusFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract type";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_type', $this->getSearchFormValue('contracts_contract_type'), '1');
		$res[] = $tmp;

		return $res;
	}


	/**
	 * generateWorksheet
	 */
	private function generateWorksheet( &$objPHPExcel, $activeSheetIndex, $title, $searchParams ) {
		$objPHPExcel->createSheet();
		$objPHPExcel->setActiveSheetIndex( $activeSheetIndex );
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$objWorksheet->setTitle( $title );
		$cols = array(
			array( 'row' => 'A', 'title' => 'Supplier', 										'field' => 'contracts_supplier' ),
			array( 'row' => 'B', 'title' => 'Region', 											'field' => 'deliveries_dc' ),
			array( 'row' => 'C', 'title' => 'ALDI Purchase Order-No', 							'field' => 'contracts_contract_number' ),
			array( 'row' => 'D', 'title' => 'Date Received Copy of Fax Order from ALDI Region', 'field' => 'contracts_crdate' ),
			array( 'row' => 'E', 'title' => 'Required Delivery Date into ALDI Region', 			'field' => 'contracts_aldi_au_wh_delivery_due_date' ),
			array( 'row' => 'F', 'title' => 'Actual RIS Date (column E + 1 working day rule)', 	'field' => 'contracts_ris_date' ),
			array( 'row' => 'G', 'title' => 'Intended PickUp Date / Cargo Ready Date', 			'field' => 'contracts_fob_date' ),
			array( 'row' => 'H', 'title' => 'Actual PickUp Date / Cargo Ready Date', 			'field' => 'contracts_actual_contract_ready_date' ),
			array( 'row' => 'I', 'title' => 'Supplier Early/Late By', 							'field' => 'contracts_supplier_kpi' ),
			array( 'row' => 'J', 'title' => 'Product Description', 								'field' => 'deliveries_product_description' ),
			array( 'row' => 'K', 'title' => 'Product Code', 									'field' => 'deliveries_product_code' ),
			array( 'row' => 'L', 'title' => 'No of Cases', 										'field' => 'deliveries_case_quantity' ),
			array( 'row' => 'M', 'title' => 'Vessel Name', 										'field' => 'shipments_mother_vessel_name' ),
			array( 'row' => 'N', 'title' => 'Port of Loading', 									'field' => 'shipments_departure_port' ),
			array( 'row' => 'O', 'title' => 'Estimated Vessel Sailing Date', 					'field' => 'shipments_etd' ),
			array( 'row' => 'P', 'title' => 'Actual Vessel Sailing Date', 						'field' => 'shipments_atd' ),
			array( 'row' => 'Q', 'title' => 'ETA Required Australian Port', 					'field' => 'shipments_eta' ),
			array( 'row' => 'R', 'title' => 'ATA Australian Port', 								'field' => 'shipments_ata' ),
			array( 'row' => 'S', 'title' => 'Estimated Delivery Date into Region', 				'field' => 'shipments_delivery_date' ),
			array( 'row' => 'T', 'title' => 'Early/Late By', 									'field' => 'shipments_early_late' ),
			array( 'row' => 'U', 'title' => 'Container Type', 									'field' => 'shipments_container_size_weight' ),
			array( 'row' => 'V', 'title' => 'Container No', 									'field' => 'shipments_container_number' ),
			array( 'row' => 'W', 'title' => 'Remarks', 											'field' => 'shipments_remarks' ),
		);
		foreach( $cols as $col ) {
			$objWorksheet->getCell($col['row'].'1')->setValue( $col['title'] );
		}

		$records = $this->model->getStatusRecords( $searchParams, array() );
		$i = 2;

		foreach( $records as $row ) {
			foreach( $cols as $col ) {
				$objWorksheet->getCell($col['row'].$i)->setValue($row[$col['field']]);
			}
			$i++;
		}

		foreach( $cols as $col ) {
			$objWorksheet->getColumnDimension($col['row'])->setAutoSize(true);
		}

		return $objWorksheet;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */