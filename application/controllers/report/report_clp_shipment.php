<?php
require_once( APPPATH.'controllers/report/abstract_report_clp.php' );

class Report_clp_shipment extends Abstract_report_clp {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_clp_shipment_model', 'model');
		$this->headline		= 'OFC – Shipment Report';
		$this->filename 	= 'logwin_aldi_clp_shipment_report.xlsx';
		$this->scriptPath 	= 'report/report_clp_shipment/';
		$this->reportType	= Abstract_report_clp_model::$REPORT_TYPE_SHIPMENT;
		//$this->output->enable_profiler(TRUE);
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}

/* End of file report.php */
/* Location: ./app/controllers/report.php */
