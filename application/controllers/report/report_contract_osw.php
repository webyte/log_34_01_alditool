<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_contract_osw extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_contract_departure', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 			= $this->getCustomFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_contract_osw/contract_osw_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Contracts per On-Sale Week';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * contract_osw_report_export
	 * @return
	 */
	public function contract_osw_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams 	= $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'contract_osw_report.xls' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}



	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$recordListSearchParams['contracts_crdate'] = array( date( 'Y-m-d 00:00:00', strtotime( '-183 days', time() ) ) );

		$this->records 	= $this->model->getContractOSWRecords( $recordListSearchParams, array() );
		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		$objWorksheet->getCell('A1')->setValue( 'Week '.$recordListSearchParams['contracts_advertisement_week'][0]);
		$objWorksheet->getCell('A3')->setValue( 'Number of contracts:' );
		$objWorksheet->getCell('B3')->setValue( $this->records[0]['total_contracts'] );
		$objWorksheet->getCell('A4')->setValue( 'Suppliers:' );
		$objWorksheet->getCell('B4')->setValue( $this->records[0]['total_suppliers'] );


		$objWorksheet->getCell('A7')->setValue( 'Contract' );
		$objWorksheet->getCell('B7')->setValue( 'Product Code' );
		$objWorksheet->getCell('C7')->setValue( 'Product Description' );
		$objWorksheet->getCell('D7')->setValue( 'Supplier' );
		$objWorksheet->getCell('E7')->setValue( 'Type' );
		$objWorksheet->getCell('F7')->setValue( 'Departure Port' );
		$objWorksheet->getCell('G7')->setValue( 'Destination Port' );
		$objWorksheet->getCell('H7')->setValue( 'Export Office' );
		$objWorksheet->getCell('I7')->setValue( 'CRD' );
		$objWorksheet->getCell('J7')->setValue( 'Units per case' );
		$objWorksheet->getCell('K7')->setValue( 'Case Weight' );
		$objWorksheet->getCell('L7')->setValue( 'Length' );
		$objWorksheet->getCell('M7')->setValue( 'Width' );
		$objWorksheet->getCell('N7')->setValue( 'Height' );
		$objWorksheet->getCell('O7')->setValue( 'Case Cubage' );
		$objWorksheet->getCell('P7')->setValue( 'DC' );
		$objWorksheet->getCell('Q7')->setValue( 'Cases' );
		$objWorksheet->getCell('R7')->setValue( 'Units' );
		$objWorksheet->getCell('S7')->setValue( 'Volume' );

		$i = 8;
		foreach( $this->records as $row ) {
			$objWorksheet->getCell('A'.$i)->setValue( $row['contracts_contract_number'] );
			$objWorksheet->getCell('B'.$i)->setValue( $row['contracts_product_code'] );
			$objWorksheet->getCell('C'.$i)->setValue( $row['contracts_product_description'] );
			$objWorksheet->getCell('D'.$i)->setValue( $row['contracts_supplier'] );
			$objWorksheet->getCell('E'.$i)->setValue( $row['contracts_traffic_type'] );
			$objWorksheet->getCell('F'.$i)->setValue( $row['contracts_departure'] );
			$objWorksheet->getCell('G'.$i)->setValue( $row['deliveries_destination'] );
			$objWorksheet->getCell('H'.$i)->setValue( $row['contracts_logwin_origin_office'] );
			$objWorksheet->getCell('I'.$i)->setValue( $row['contracts_fob_date'] );
			$objWorksheet->getCell('J'.$i)->setValue( $row['contracts_units_per_case'] );
			$objWorksheet->getCell('K'.$i)->setValue( $row['contracts_case_weight'] );
			$objWorksheet->getCell('L'.$i)->setValue( $row['contracts_case_length'] );
			$objWorksheet->getCell('M'.$i)->setValue( $row['contracts_case_width'] );
			$objWorksheet->getCell('N'.$i)->setValue( $row['contracts_case_height'] );
			$objWorksheet->getCell('O'.$i)->setValue( $row['contracts_case_cubage'] );
			$objWorksheet->getCell('P'.$i)->setValue( $row['deliveries_dc'] );
			$objWorksheet->getCell('Q'.$i)->setValue( $row['deliveries_case_quantity'] );
			$objWorksheet->getCell('R'.$i)->setValue( $row['deliveries_unit_quantity'] );
			$objWorksheet->getCell('S'.$i)->setValue( $row['volume'] );


			$i++;
		}

		return $objPHPExcel;
	}


	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-sales week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "DC";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_dc', $this->getSearchFormValue('deliveries_dc'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Logwin Origin Office";
		$tmp['input'] = $this->tca->getFormInputById('contracts_logwin_origin_office', $this->getSearchFormValue('contracts_logwin_origin_office'), '1');
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */