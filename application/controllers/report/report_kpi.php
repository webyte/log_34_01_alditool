<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_kpi extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->headline	= 'KPI-Report';
//		$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_kpi', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 		= $this->getCustomOrderFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_kpi/kpi_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'KPI-Report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * kpi_report_export
	 * @return
	 */
	public function kpi_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams = $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );
		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'kpi-report.xls' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}


	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$this->records 	= $this->model->getKPIRecords( $recordListSearchParams, array() );

		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();



		$objWorksheet->getCell('A1')->setValue( 'Container No' );
		$objWorksheet->getCell('B1')->setValue( 'Container Size' );
		$objWorksheet->getCell('C1')->setValue( 'Container Type' );
		$objWorksheet->getCell('D1')->setValue( 'Port Origin' );
		$objWorksheet->getCell('E1')->setValue( 'Country of Origin' );
		$objWorksheet->getCell('F1')->setValue( 'Port of Discharge' );
		$objWorksheet->getCell('G1')->setValue( 'DC' );
		$objWorksheet->getCell('H1')->setValue( 'Vessel Name' );
		$objWorksheet->getCell('I1')->setValue( 'Shipping Line' );
		$objWorksheet->getCell('J1')->setValue( 'Initial Estimated Departure Date' );
		$objWorksheet->getCell('K1')->setValue( 'Estimated Departure Date' );
		$objWorksheet->getCell('L1')->setValue( 'Actual Departure Date' );
		$objWorksheet->getCell('M1')->setValue( 'Initial Estimated Arrival Date' );
		$objWorksheet->getCell('N1')->setValue( 'Estimated Arrival Date' );
		$objWorksheet->getCell('O1')->setValue( 'Actual Arrival Date' );
		$objWorksheet->getCell('P1')->setValue( 'Product Code' );
		$objWorksheet->getCell('Q1')->setValue( 'Purchase Order' );
		$objWorksheet->getCell('R1')->setValue( 'Supplier' );
		$objWorksheet->getCell('S1')->setValue( 'On-sale date' );
		$objWorksheet->getCell('T1')->setValue( 'Product Range' );
		$objWorksheet->getCell('U1')->setValue( 'Product CBM' );
		$objWorksheet->getCell('V1')->setValue( 'Product Weight' );
		$objWorksheet->getCell('W1')->setValue( 'CFS / Direct' );
		$objWorksheet->getCell('X1')->setValue( 'Incoterm Code' );
		$objWorksheet->getCell('Y1')->setValue( 'Loading Type' );



		$i = 2;
		foreach( $this->records as $row ) {
			$objWorksheet->getCell('A'.$i)->setValue( $row['shipments_container_number'] );
			$objWorksheet->getCell('B'.$i)->setValue( $row['shipments_container_size'] );
			$objWorksheet->getCell('C'.$i)->setValue( $row['shipments_container_type'] );
			$objWorksheet->getCell('D'.$i)->setValue( $row['shipments_departure_port'] );
			$objWorksheet->getCell('E'.$i)->setValue( substr( $row['shipments_departure_port'], 0, 2 ) );
			$objWorksheet->getCell('F'.$i)->setValue( $row['shipments_destination_port'] );
			$objWorksheet->getCell('G'.$i)->setValue( $row['shipments_dc'] );
			$objWorksheet->getCell('H'.$i)->setValue( $row['shipments_mother_vessel_name'] );
			$objWorksheet->getCell('I'.$i)->setValue( $row['shipments_carrier'] );
			$objWorksheet->getCell('J'.$i)->setValue( $row['pcshipments_ietd'] );
			$objWorksheet->getCell('K'.$i)->setValue( $row['shipments_etd'] );
			$objWorksheet->getCell('L'.$i)->setValue( $row['shipments_atd'] );
			$objWorksheet->getCell('M'.$i)->setValue( $row['pcshipments_ieta'] );
			$objWorksheet->getCell('N'.$i)->setValue( $row['shipments_eta'] );
			$objWorksheet->getCell('O'.$i)->setValue( $row['shipments_ata'] );
			$objWorksheet->getCell('P'.$i)->setValue( implode( ', ', array_unique( $row['deliveries_product_code'] ) ) );
			$objWorksheet->getCell('Q'.$i)->setValue( implode( ', ', array_unique( $row['contracts_contract_number'] ) ) );
			$objWorksheet->getCell('R'.$i)->setValue( implode( ', ', array_unique( $row['contracts_supplier'] ) ) );
			$objWorksheet->getCell('S'.$i)->setValue( $row['contracts_on_sale_date'] );
			$objWorksheet->getCell('T'.$i)->setValue( implode( ', ', array_unique( $row['contracts_contract_type'] ) ) );
			$objWorksheet->getCell('U'.$i)->setValueExplicit( sprintf($this->config->item('doublevalues_format'), array_sum( $row['deliveries_volume'] ) ) );
			$objWorksheet->getCell('V'.$i)->setValueExplicit( sprintf($this->config->item('floatvalues_format'), array_sum( $row['deliveries_gross_weight'] ) ) );
			$objWorksheet->getCell('W'.$i)->setValue( $row['contracts_traffic_type'] );
			$objWorksheet->getCell('X'.$i)->setValue( $row['shipments_incoterm_code'] );
			$objWorksheet->getCell('Y'.$i)->setValue( $row['shipments_loading_type'] );

			$i++;
		}

		return $objPHPExcel;
	}


	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomOrderFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'event[]', array( "" => "please select ...", "etd" => "ETD", "eta" => "ETA" ), $this->getSearchFormValue('event') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_from" value="'.$this->getSearchFormValue('shipments_from').'" name="shipments_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_to" value="'.$this->getSearchFormValue('shipments_to').'" name="shipments_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-sales week";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_advertisement_week', $this->getSearchFormValue('deliveries_advertisement_week'), '1');
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */