<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_contract_departure extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= 'Reporting';
//		$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_contract_departure', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 			= $this->getCustomFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_contract_departure/contract_departure_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Contract departure report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * order_report_export
	 * @return
	 */
	public function contract_departure_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams 	= $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'contract_departure_report.xls' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}



	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$this->records 	= $this->model->getContractDepartureRecords( $recordListSearchParams, array() );

		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		
		$objWorksheet->getCell('A1')->setValue( 'Contract number' );
		$objWorksheet->getCell('B1')->setValue( 'Import date' );
		$objWorksheet->getCell('C1')->setValue( 'Creation date' );
		$objWorksheet->getCell('D1')->setValue( 'Print date' );
		$objWorksheet->getCell('E1')->setValue( 'Departure port' );
		$objWorksheet->getCell('F1')->setValue( 'Mother vessel name' );
		$objWorksheet->getCell('G1')->setValue( 'ETD' );

		$i = 2;
		foreach( $this->records as $row ) {
			$objWorksheet->getCell('A'.$i)->setValue( $row['shipments_contract_numbers'] );
			$objWorksheet->getCell('B'.$i)->setValue( $row['shipments_import_date'] );
			$objWorksheet->getCell('C'.$i)->setValue( $row['contracts_crdate'] );
			$objWorksheet->getCell('D'.$i)->setValue( $row['shipments_print_date'] );
			$objWorksheet->getCell('E'.$i)->setValue( $row['shipments_departure_port'] );
			$objWorksheet->getCell('F'.$i)->setValue( $row['shipments_mother_vessel_name'] );
			$objWorksheet->getCell('G'.$i)->setValue( $row['shipments_etd'] );
			$i++;
		}

		return $objPHPExcel;
	}


	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-sales week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "ETD";
		$tmp['input'] = $this->tca->getFormInputById('shipments_etd', $this->getSearchFormValue('shipments_etd'), '1');
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */