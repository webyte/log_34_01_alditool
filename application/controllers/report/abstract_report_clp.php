<?php
require_once( APPPATH.'controllers/abstract_controller.php' );
require_once( APPPATH.'third_party/spout/src/Spout/Autoloader/autoload.php' );


class Abstract_report_clp extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->headline		= '';
		$this->filename 	= '';
		$this->scriptPath 	= '';
		$this->reportType	= '';
		$this->writerType = \Box\Spout\Common\Type::XLSX;
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$data['cdaResult'] = '';
		$this->load->view('report/report_overview_clp', $data );
	}

	/**
	 * @Override
	 */
	public function getViewData() {
		$searchFormFields	 		= $this->getCustomSearchFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), $this->scriptPath.'excel_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= $this->headline;
		$data['exportMailUrl']		= $this->scriptPath.'generateReportAndSendByMail/'.$this->reportType;
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}

	/**
	 * excel_report_export
	 * @return
	 */
	public function excel_report_export(){
		$recordListSearchParams = $this->input->post();
		$cells					= $this->model->getCellDefintions();
		$records				= $this->model->getRecords( $recordListSearchParams );
		$rows					= array();
		$labels 				= array();
		foreach( $cells as $cell ) {
			$labels[] = $cell['label'];
		}
		$rows[] 				= $labels;

		foreach( $records as $record ) {
			$data = array();
			foreach( $cells as $cell ) {
				$data[] = !empty( $cell['field'] ) ? $record[$cell['field']] : '';
			}
			$rows[] = $data;
		}

		$writer = \Box\Spout\Writer\WriterFactory::create( $this->writerType );
		$writer->openToBrowser( $this->filename );
		$writer->addRows($rows);
		$writer->close();
	}

	/**
	 * generateReportAndSendByMail
	 * @return
	 */
	public function generateReportAndSendByMail( $reportType ){
		$userId = $this->session->userdata('userId');
		$wwwPath = $this->config->item('www_path');
		$cmd = "/usr/bin/php5.6 ".$wwwPath."index.php report/report_clp_cron excel_report_mail ".$reportType." ".$userId." > /dev/null 2>/dev/null &";

		$this->welogger->log( $cmd, WELogger::$LOG_LEVEL_INFO, 'Abstract_report_clp.generateReportAndSendByMail' );

		shell_exec( $cmd );
		$this->index();
	}

	/**
	 * generateCache
	 *
	 */
	public function generateCache()
	{
		$this->model->generateCache();
	}

	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "HBL/HAWB";
		$tmp['input'] = '<input type="text" id="shipments_hbl_hawb" value="'.$this->getSearchFormValue('shipments_hbl_hawb').'" name="shipments_hbl_hawb[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = '<input type="text" id="shipments_container_number" value="'.$this->getSearchFormValue('shipments_container_number').'" name="shipments_container_number[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Departure";
		$tmp['input'] = $this->tca->getFormInputById('contracts_departure', $this->getSearchFormValue('contracts_departure'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Destination";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_destination', $this->getSearchFormValue('deliveries_destination'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Traffic type";
		$tmp['input'] = form_dropdown( 'contracts_traffic_type[]', array( "" => "please select ...", "1" => "Factory", "2" => "CFS", "3" => "AIR", "4" => "PTS Loading", "5" => "Factory/CFS" ), $this->getSearchFormValue('contracts_traffic_type') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'event[]', array( "" => "please select ...", "etd" => "ETD", "eta" => "ETA", "cus" => "CUS", "atd" => "ATD", "ata" => "ATA" ), $this->getSearchFormValue('event') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_from" value="'.$this->getSearchFormValue('shipments_from').'" name="shipments_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_to" value="'.$this->getSearchFormValue('shipments_to').'" name="shipments_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-Sale week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Logwin origin office";
		$tmp['input'] = $this->tca->getFormInputById('contracts_logwin_origin_office', $this->getSearchFormValue('contracts_logwin_origin_office'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Supplier";
		$tmp['input'] = $this->tca->getFormInputById('contracts_supplier', $this->getSearchFormValue('contracts_supplier'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Incoterm";
		$tmp['input'] = form_dropdown( 'shipments_incoterm_code[]', array( "" => "please select ...", "1" => "EXW", "2" => "FOB", "3" => "DDP" ), $this->getSearchFormValue('shipments_incoterm_code') );
		$res[] = $tmp;

		return $res;
	}

	/**
	 * generateObjPHPExcel
	 */
	public function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$cells			= $this->model->getCellDefintions();
		$records		= $this->model->getRecords( $recordListSearchParams );
		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		$labels = array();
		foreach( $cells as $cell ) {
			$labels[] = $cell['label'];
		}
		$objWorksheet->fromArray( $labels, NULL, 'A1' );

		$row = 2;
		foreach( $records as $record ) {
			$data = array();
			foreach( $cells as $cell ) {
				$data[] = !empty( $cell['field'] ) ? $record[$cell['field']] : '';
			}
			$objWorksheet->fromArray( $data, NULL, 'A'.$row );
			$row++;
		}

		return $objPHPExcel;
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


}

/* End of file report.php */
/* Location: ./app/controllers/report.php */
