<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_ff extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_ff', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 		= $this->getCustomTelexFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_ff/ff_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Freight Forwarders (FF) Schedule';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * order_report_export
	 * @return
	 */
	public function ff_report_export(){
		$this->load->library('zip');
		$dcsPerState 			= array( 1 => array( 'MIN', 'PRE' ),  2 => array( 'DAN', 'DER' ),  3 => array( 'STP', 'BRE' ), 4 => array( 'RGY' ), 5 => array( 'JKT' ) );
		$stateTitles			= array( 1 => 'New South Wales', 2 => 'Victoria', 3 => 'Queensland', 4 => 'South Australia', 5 => 'Western Australia' );
		$recordListSearchParams = $this->input->post();
		$states 				= $recordListSearchParams['ff_states'];
		foreach( $states as $state ) {
			$dcs 	= $dcsPerState[$state];
			$file 	= $this->generateExcelDoc( $recordListSearchParams, $dcs, $stateTitles[$state] );
			$this->zip->read_file( $file );
		}
		$this->zip->download('ff_report.zip');
	}


	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * generateExcelDoc
	 */
	private function generateExcelDoc( $recordListSearchParams, $dcs, $state ) {
		$this->load->helper('string');
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$file					= $this->config->item('tmp_path').'ff_report_'.$state.'_'.random_string('alnum', 32).'.xls';
		$objPHPExcel 			= $this->generateObjPHPExcel( $recordListSearchParams, $dcs, $state );
		$objWriter 				= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		$objWriter->save( $file );
		return $file;

//		header('Content-Type: application/vnd.ms-excel');
//		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'ff_report.xls' ));
//		header('Cache-Control: max-age=0');
//		$objWriter->save('php://output');
	}

	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams, $dcs, $state ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$this->records 		= $this->model->getFFRecords( $recordListSearchParams, array() );
		//var_dump($this->records);
		$activeSheetIndex	= 0;
		$template 			= $this->config->item('app_path').'views/report/ff_template.xls';
		$objReader 			= new PHPExcel_Reader_Excel5();
		$objPHPExcel 		= $objReader->load( $template );

		$objPHPExcel->setActiveSheetIndex($activeSheetIndex);
		$sheetTemplate		= clone $objPHPExcel->getActiveSheet();

		foreach( $dcs as $dc ) {
			if( isset( $this->records[$dc] ) ) {
				$value = $this->records[$dc];
				if( $activeSheetIndex == 0 ) {
					$objPHPExcel->setActiveSheetIndex($activeSheetIndex);
				} else {
					$newSheet = clone $sheetTemplate;
					$newSheetIndex = $activeSheetIndex;
					$objPHPExcel->addSheet( $newSheet, $newSheetIndex );
					$objPHPExcel->setActiveSheetIndex( $activeSheetIndex );
				}

				$objWorksheet = $objPHPExcel->getActiveSheet();
				$objWorksheet->setTitle( $dc );
				$objWorksheet->getCell('A1')->setValue( 'Container Arrivals - '.$dc.' ('.$state.')' );

				$i = 4;
				foreach( $value as $row ) {
					$objWorksheet->getCell('B'.$i)->setValue( $row['date_aldi'] );
					$objWorksheet->getCell('F'.$i)->setValue( $row['shipments_eta'] );
					$objWorksheet->getCell('G'.$i)->setValue( $row['shipments_container_number'] );
					$objWorksheet->getCell('H'.$i)->setValue( $row['shipments_container_size'] );
					$objWorksheet->getCell('I'.$i)->setValue( $row['shipments_container_type'] );
					$objWorksheet->getCell('J'.$i)->setValue( $row['shipments_seal_number'] );
					$objWorksheet->getCell('K'.$i)->setValue( $row['shipments_container_tare_weight'] + $row['deliveries_sum_gross_weight'] );
					$objWorksheet->getCell('L'.$i)->setValue( $row['shipments_mother_vessel_name'] );
					$objWorksheet->getCell('M'.$i)->setValue( $row['shipments_voyage_number'] );
					$objWorksheet->getCell('N'.$i)->setValue( $row['shipments_carrier'] );
					$objWorksheet->getCell('O'.$i)->setValue( $row['shipments_yn_edor'] );
					$objWorksheet->getCell('P'.$i)->setValue( $row['shipments_yn_telex_received'] );
					$objWorksheet->getCell('Q'.$i)->setValue( $row['shipments_yn_cus'] );
					$objWorksheet->getCell('R'.$i)->setValue( $row['shipments_yn_cus'] );
					$objWorksheet->getCell('W'.$i)->setValue( $row['contracts_sum_contract_number'] );
					$objWorksheet->getCell('X'.$i)->setValue( $row['contracts_sum_supplier'] );
					$objWorksheet->getCell('Y'.$i)->setValue( $row['contracts_contract_type'] );
					$objWorksheet->getCell('Z'.$i)->setValue( $row['contracts_aldi_au_wh_delivery_due_date'] );
					$objWorksheet->getCell('AA'.$i)->setValue( $row['deliveries_sum_product_code'] );
					if( $row['shipments_telex_received'] == '0' ) {
						$styleArray = array(
							'font'  => array(
								'color' => array('rgb' => 'FF0000'),
							)
						);

						$objWorksheet->getStyle('AA'.$i)->applyFromArray($styleArray);
					}
					$objWorksheet->getCell('AB'.$i)->setValue( $row['deliveries_sum_product_description'] );
					$objWorksheet->getCell('AC'.$i)->setValue( $row['deliveries_sum_case_quantity'] );
					$objWorksheet->getCell('AD'.$i)->setValue( $row['shipments_loading_type'] );
					$objWorksheet->getCell('AE'.$i)->setValue( $row['region'] );
					$i++;
				}
				$activeSheetIndex++;
			}
		}

		return $objPHPExcel;
	}

	/**
	 * getCustomTelexFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomTelexFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="ff_from" value="'.$this->getSearchFormValue('ff_from').'" name="ff_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="ff_to" value="'.$this->getSearchFormValue('ff_to').'" name="ff_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "States";
		$tmp['input'] = '<label class="checkbox"><input type="checkbox" id="ff_states_1" value="1" name="ff_states[]"> New South Wales</label>
<label class="checkbox"><input type="checkbox" id="ff_states_2" value="2" name="ff_states[]"> Victoria</label>
<label class="checkbox"><input type="checkbox" id="ff_states_3" value="3" name="ff_states[]"> Queensland</label>
<label class="checkbox"><input type="checkbox" id="ff_states_4" value="4" name="ff_states[]"> South Australia</label>
<label class="checkbox"><input type="checkbox" id="ff_states_5" value="5" name="ff_states[]"> Western Australia</label>';
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */