<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_order extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_order', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 			= $this->getCustomOrderFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_order/order_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'ALDI summary of order report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * order_report_export
	 * @return
	 */
	public function order_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams 	= $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'ALDI_Summary_of_Orders_Report.xls' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}


	/**
	 * generate_order_report_mail
	 * @return
	 */
	public function generate_order_report_mail(){
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $this->lang->line('report.order.mail.recipient');
		$subject 	= $this->lang->line('report.order.mail.subject');
		$body 		= $this->lang->line('report.order.mail.body');

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );

		$this->load->model('Supplier_model', 'supplier_model');
		$suppliers = $this->supplier_model->getRecordsWhere( array('deleted' => 0, 'hidden' => 0) );

		foreach( $suppliers as $supplier ) {
			$file1 = $this->createOrderReportXLSFile( Contract_model::$CONTRACT_TYPE_CORE_RANGE, $supplier['id'] );
			$file2 = $this->createOrderReportXLSFile( Contract_model::$CONTRACT_TYPE_STORE_EQUIPMENT, $supplier['id'] );

			if( !empty( $file1 ) ) $this->email->attach( $file1 );
			if( !empty( $file2 ) ) $this->email->attach( $file2 );
		}

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( "Sending Mail to ".$toMail." failed, error: ".$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, "Report_order.generateOrderReportMail" );
			return false;
		} else {
			$this->welogger->log( "Mail was sent to ".$toMail." \nwith subject: ".$subject." \nand message: ".$body, WELogger::$LOG_LEVEL_INFO, "Report_order.generateOrderReportMail" );
			return true;
		}
		$this->email->clear(TRUE);

	}



	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * createOrderReportXLSFile
	 * @return boolean
	 */
	private function createOrderReportXLSFile( $contractType, $supplier ) {
		$this->load->helper('string');
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$contactETAFrom = date( 'd/m/Y', strtotime( '-30 days' ) );
		$contactETATo 	= date( 'd/m/Y', strtotime( '+10 days' ) );
		$archivePath 	= $this->config->item('archive_path');

		$recordListSearchParams = array (
			'contracts_contract_type' 	=> array( $contractType ),
			'contracts_supplier' 		=> array( $supplier ),
			'contact_eta_from' 			=> array( $contactETAFrom ),
			'contact_eta_to' 			=> array( $contactETATo )
		);

		$file = '';
		$records = $this->model->getOrderRecords( $recordListSearchParams, array() );
		if( count( $records ) > 0 ) {

			$this->load->model('Supplier_model', 'supplier_model');
			$supplierName 	= $this->getSupplierName( $supplier );
			$contractTypeName = $this->getContractTypeName( $contractType );

			$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );
			$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
			$file 			= $archivePath.'ALDI_Summary_of_Orders_Report-'.$contractTypeName.'-'.$supplierName.'-'.random_string('alnum', 16).'.xls';

			$objWriter->save( $file );
		}

		return $file;
	}


	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$this->records 	= $this->model->getOrderRecords( $recordListSearchParams, array() );

		$filename			= '';
		$activeSheetIndex	= 0;
		$template 			= $this->config->item('app_path').'views/report/order_template.xls';
		$objReader 			= new PHPExcel_Reader_Excel5();
		$objPHPExcel 		= $objReader->load( $template );

		$dcs = array('PRE','MIN','STP','DER','DAN','BRE','JKT','RGY');

		for( $j=0; $j<8; $j++) {
			if( isset( $this->records[$dcs[$j]] ) ) {
				$value = $this->records[$dcs[$j]];
				$objPHPExcel->setActiveSheetIndex($activeSheetIndex);
				$objWorksheet = $objPHPExcel->getActiveSheet();

				$i = 8;
				foreach( $value as $row ) {
					$objWorksheet->getCell('B'.$i)->setValue( $i-1 );
					$objWorksheet->getCell('C'.$i)->setValue( $row['contracts_contract_number'] );
					$objWorksheet->getCell('D'.$i)->setValue( $row['contracts_crdate'] );
					$objWorksheet->getCell('E'.$i)->setValue( $row['deliveries_dc'] );
					$objWorksheet->getCell('F'.$i)->setValue( $row['contracts_aldi_au_wh_delivery_due_date'] );
					$objWorksheet->getCell('G'.$i)->setValue( '' );
					$objWorksheet->getCell('H'.$i)->setValue( $row['contracts_departure'] );
					$objWorksheet->getCell('I'.$i)->setValue( $row['contracts_fob_date'] );
					$objWorksheet->getCell('J'.$i)->setValue( $row['contracts_pickup_point'] );
					$objWorksheet->getCell('K'.$i)->setValue( $row['deliveries_product_description'] );
					$objWorksheet->getCell('L'.$i)->setValue( $row['deliveries_product_code'] );
					$objWorksheet->getCell('M'.$i)->setValue( '1' );
					$objWorksheet->getCell('N'.$i)->setValue( $row['deliveries_case_quantity'] );
					$objWorksheet->getCell('O'.$i)->setValue( $row['shipments_mother_vessel_name'] );
					$objWorksheet->getCell('P'.$i)->setValue( $row['shipments_atd'] );
					$objWorksheet->getCell('Q'.$i)->setValue( $row['shipments_container_number'] );
					$objWorksheet->getCell('R'.$i)->setValue( $this->getDateDifference( $row['shipments_eta'], $row['shipments_etd']) );
					$objWorksheet->getCell('S'.$i)->setValue( $row['shipments_eta'] );
					$objWorksheet->getCell('T'.$i)->setValue( $this->getContentsSecured( $row['shipments_contents_secured'] ) );
					$objWorksheet->getCell('U'.$i)->setValue( $this->getIfETAisGreaterThanDeliveryDueDate( $row['shipments_eta'], $row['contracts_aldi_au_wh_delivery_due_date']) );
					$objWorksheet->getCell('V'.$i)->setValue( '' );
					$objWorksheet->getCell('W'.$i)->setValue( $row['contracts_comments'] );
					$i++;
				}
			}
			$activeSheetIndex++;
		}

		return $objPHPExcel;
	}

	/**
	 * getContentsSecured
	 */
	private function getContentsSecured( $val ) {
		return $val == 1 ? 'Yes' : 'No';

	}

	/**
	 * getSupplierName
	 */
	private function getSupplierName( $id ) {
		$this->load->model('Supplier_model', 'supplier_model');
		$supplier 		= $this->supplier_model->getRecord( $id );
		return $supplier['name'];
	}

	/**
	 * getContractTypeName
	 */
	private function getContractTypeName( $id ) {
		$conOptions = $this->getTCAgetMultiselectionOptions( 'contracts_contract_type' );
		return array_key_exists($id, $conOptions) ? $conOptions[$id] : $id;

	}

	private function getTCAgetMultiselectionOptions( $str ){
		$tcaNode = $this->tca->getColumnNodeById( $str );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		return $options;
	}

	/**
	 * getIfETAisGreaterThanDeliveryDueDate
	 */
	private function getIfETAisGreaterThanDeliveryDueDate( $eta, $deliveryDueDate ) {
		$date1 = strtotime( $eta );
		$date2 = strtotime( $deliveryDueDate );
		return ($date1 > $date2 ) ? 'N' : 'Y';
	}

	/**
	 * getDateDifference
	 */
	private function getDateDifference( $date1, $date2 ) {
		$dStart = new DateTime( $date1 );
		$dEnd  	= new DateTime( $date2 );
		$dDiff 	= $dStart->diff($dEnd);
		return $dDiff->days;
	}

	/**
	 * getOrderHeaderRow
	 */
	private function getNewOrderReportWorksheet( &$objPHPExcel, $dc, $activeSheetIndex ) {
		$objPHPExcel->createSheet();
		$objWorksheet = $objPHPExcel->setActiveSheetIndex( $activeSheetIndex );
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$objWorksheet->setTitle('Summary of Orders Report '.$dc);
		// set header row
		$headers = array(
			'A' => '',
			'B' => 'ALDI Purchase Order-No:',
			'C' => 'Date Received Copy of Fax Order from ALDI Region',
			'D' => 'ALDI Region',
			'E' => 'Required Delivery Date into ADLi Region',
			'F' => 'Date Contacted Overseas Supplier',
			'G' => 'Country',
			'H' => 'Pickup Date - Ex Works',
			'I' => 'Pickup Point',
			'J' => 'Product',
			'K' => 'Product Code',
			'L' => 'No of Containers',
			'M' => 'No of Units',
			'N' => 'Vessel Name ',
			'O' => 'Vessel Sailing Date',
			'P' => 'Container number',
			'Q' => 'Ocean Freight Timeframe - Days',
			'R' => 'Date when Container arrives at required Port in Australia',
			'S' => 'Contents Insured - Yes/No',
			'T' => 'Delivery on required day expected into ALDI - Yes/No, if no please comment',
			'U' => 'Delivered into Warehouse',
			'V' => 'Comments'
		);
		foreach( $headers as $key=>$value ) {
			$objWorksheet->getCell($key.'1')->setValue( $value );
		}
		return $objWorksheet;
	}

	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomOrderFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract type";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_type', $this->getSearchFormValue('contracts_contract_type'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Supplier";
		$tmp['input'] = $this->tca->getFormInputById('contracts_supplier', $this->getSearchFormValue('contracts_supplier'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "ETA from";
		$tmp['input'] = '<input type="text" class="datepicker" id="contracts_eta_from" value="'.$this->getSearchFormValue('contracts_eta_from').'" name="contracts_eta_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "ETA to";
		$tmp['input'] = '<input type="text" class="datepicker" id="contracts_eta_to" value="'.$this->getSearchFormValue('contracts_eta_to').'" name="contracts_eta_to[]">';
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */