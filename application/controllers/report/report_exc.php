<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );
//require_once( BASEPATH.'../../phpdocx_pro/classes/CreateDocx.inc' );
class Report_exc extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$data['cdaResult'] = '';
		$this->load->view('report/report_overview', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 		= $this->getCustomSearchFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_exc/exc_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Exception report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}

	/**
	 * csv_report_export
	 * @return
	 */
	public function exc_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams 	= $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel2007' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=Aldi_exception_report.xlsx' );
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}


	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {

		// ist nur ne quick&dirty Lösung!
		// muss man mal ordentlich machen...

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "HBL/HAWB";
		$tmp['input'] = '<input type="text" id="shipments_hbl_hawb" value="'.$this->getSearchFormValue('shipments_hbl_hawb').'" name="shipments_hbl_hawb[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = '<input type="text" id="shipments_container_number" value="'.$this->getSearchFormValue('shipments_container_number').'" name="shipments_container_number[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Departure";
		$tmp['input'] = $this->tca->getFormInputById('contracts_departure', $this->getSearchFormValue('contracts_departure'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Destination";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_destination', $this->getSearchFormValue('deliveries_destination'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Traffic type";
		$tmp['input'] = form_dropdown( 'contracts_traffic_type[]', array( "" => "please select ...", "1" => "Factory", "2" => "CFS", "3" => "AIR", "4" => "PTS Loading", "5" => "Factory/CFS" ), $this->getSearchFormValue('contracts_traffic_type') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'event[]', array( "" => "please select ...", "etd" => "ETD", "eta" => "ETA", "cus" => "CUS", "atd" => "ATD", "ata" => "ATA" ), $this->getSearchFormValue('event') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_from" value="'.$this->getSearchFormValue('shipments_from').'" name="shipments_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_to" value="'.$this->getSearchFormValue('shipments_to').'" name="shipments_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-Sale week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Logwin origin office";
		$tmp['input'] = $this->tca->getFormInputById('contracts_logwin_origin_office', $this->getSearchFormValue('contracts_logwin_origin_office'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Supplier";
		$tmp['input'] = $this->tca->getFormInputById('contracts_supplier', $this->getSearchFormValue('contracts_supplier'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Incoterm";
		$tmp['input'] = form_dropdown( 'shipments_incoterm_code[]', array( "" => "please select ...", "1" => "EXW", "2" => "FOB", "3" => "DDP" ), $this->getSearchFormValue('shipments_incoterm_code') );
		$res[] = $tmp;

		return $res;
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * generateExcelDoc
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$records 		= $this->model->getExcRecords( $recordListSearchParams, array() );
		$template 		= $this->config->item('app_path').'views/report/exc_template.xlsx';

		$objReader 		= new PHPExcel_Reader_Excel2007();
		$objPHPExcel 	= $objReader->load( $template );

		$objWorksheet 	= $objPHPExcel->getActiveSheet();
		$objWorksheet->getCell('D6')->setValue( date('d/m/Y') );
		$objWorksheet->getCell('D7')->setValue( date('H:i:s') );

		$offset = 10;
		$i = $offset;

		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)
		);

		foreach( $records as $record ) {
			$objWorksheet->getCell('A'.$i)->setValue( $record['contracts_advertisement_week'] );
			$objWorksheet->getCell('B'.$i)->setValue( $record['shipments_container_number'] );
			$objWorksheet->getCell('C'.$i)->setValue( $record['contracts_contract_number'] );
			$objWorksheet->getCell('D'.$i)->setValue( $record['contracts_supplier'] );
			$objWorksheet->getCell('E'.$i)->setValue( $record['contracts_fob_date'] );
			$objWorksheet->getCell('F'.$i)->setValue( $record['contracts_actual_contract_ready_date'] );
			$objWorksheet->getCell('G'.$i)->setValue( $record['contracts_day_variances'] );
			$objWorksheet->getCell('H'.$i)->setValue( $record['shipments_etd'] );
			$objWorksheet->getCell('I'.$i)->setValue( $record['shipments_atd'] );
			$objWorksheet->getCell('J'.$i)->setValue( $record['shipments_eta'] );
			$objWorksheet->getCell('K'.$i)->setValue( $record['shipments_ata'] );
			$objWorksheet->getCell('L'.$i)->setValue( $record['contracts_aldi_au_wh_delivery_due_date'] );
			$objWorksheet->getCell('M'.$i)->setValue( $record['contracts_aldi_au_wh_delivery_due_end_date'] );
			$objWorksheet->getCell('N'.$i)->setValue( $record['shipments_invoice_click_date'] );
			$objWorksheet->getCell('O'.$i)->setValue( $record['shipments_packing_list_click_date'] );
			$objWorksheet->getCell('P'.$i)->setValue( $record['shipments_packing_declaration_click_date'] );
			$objWorksheet->getCell('Q'.$i)->setValue( $record['shipments_treatment_certificate_click_date'] );
			$objWorksheet->getCell('R'.$i)->setValue( $record['shipments_deliveries_mm_telex_first_reminder_sent'] );
			$objWorksheet->getCell('S'.$i)->setValue( $record['shipments_deliveries_mm_telex_second_reminder_sent'] );
			$objWorksheet->getCell('T'.$i)->setValue( $record['shipments_deliveries_mm_telex_set_date'] );
			$objWorksheet->getCell('U'.$i)->setValue( $record['shipments_cus'] );
			$objWorksheet->getCell('V'.$i)->setValue( $record['shipments_certificate_of_origin_click_date'] );

			$objWorksheet->getStyle('A'.$i.':V'.$i)->applyFromArray($styleArray);

			$i++;
		}

		return $objPHPExcel;

	}

}

/* End of file report.php */
/* Location: ./app/controllers/report.php */