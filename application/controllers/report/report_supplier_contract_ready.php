<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Report_supplier_contract_ready extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$this->load->view('report/report_supplier_contract_ready', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 			= $this->getCustomFormFields();

		$data = array();
		$data['reportForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_supplier_contract_ready/supplier_contract_ready_report_export', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['reportHeadline'] 	= 'Supplier contract ready report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}


	/**
	 * order_report_export
	 * @return
	 */
	public function supplier_contract_ready_report_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$recordListSearchParams 	= $this->input->post();
		$objPHPExcel 	= $this->generateObjPHPExcel( $recordListSearchParams );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'supplier_contract_ready_report.xls' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}



	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $recordListSearchParams ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$this->records 	= $this->model->getSupplierContractReadyRecords( $recordListSearchParams, array() );

		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		$objWorksheet->getColumnDimension('A')->setAutoSize(true);
		$objWorksheet->getColumnDimension('B')->setAutoSize(true);
		$objWorksheet->getColumnDimension('C')->setAutoSize(true);
		$objWorksheet->getColumnDimension('D')->setAutoSize(true);
		$objWorksheet->getColumnDimension('E')->setAutoSize(true);
		$objWorksheet->getColumnDimension('F')->setAutoSize(true);
		$objWorksheet->getColumnDimension('G')->setAutoSize(true);
		$objWorksheet->getColumnDimension('H')->setAutoSize(true);

		$objWorksheet->getCell('A1')->setValue( 'Contract No' );
		$objWorksheet->getCell('B1')->setValue( 'Product Code' );
		$objWorksheet->getCell('C1')->setValue( 'On Sale Week' );
		$objWorksheet->getCell('D1')->setValue( 'Traffic Type' );
		$objWorksheet->getCell('E1')->setValue( 'Supplier Name' );
		$objWorksheet->getCell('F1')->setValue( 'Requested contract ready date (FOB date)' );
		$objWorksheet->getCell('G1')->setValue( 'Actual contract ready date' );
		$objWorksheet->getCell('H1')->setValue( 'Comment' );


		$i = 2;
		foreach( $this->records as $row ) {
			$objWorksheet->getCell('A'.$i)->setValue( $row['contracts_contract_number'] );
			$objWorksheet->getCell('B'.$i)->setValue( $row['contracts_product_code'] );
			$objWorksheet->getCell('C'.$i)->setValue( $row['contracts_advertisement_week'] );
			$objWorksheet->getCell('D'.$i)->setValue( $row['contracts_traffic_type'] );
			$objWorksheet->getCell('E'.$i)->setValue( $row['contracts_supplier'] );
			$objWorksheet->getCell('F'.$i)->setValue( $row['contracts_fob_date'] );
			$objWorksheet->getCell('G'.$i)->setValue( $row['contracts_actual_contract_ready_date'] );
			$objWorksheet->getCell('H'.$i)->setValue( $row['contracts_comments'] );
			$i++;
		}

		return $objPHPExcel;
	}


	/**
	 * getCustomOrderFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-sales week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Supplier";
		$tmp['input'] = $this->tca->getFormInputById('contracts_supplier', $this->getSearchFormValue('contracts_supplier'), '1');
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */