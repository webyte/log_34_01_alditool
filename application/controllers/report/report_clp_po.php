<?php
require_once( APPPATH.'controllers/report/abstract_report_clp.php' );

class Report_clp_po extends Abstract_report_clp {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_clp_po_model', 'model');
		$this->headline		= 'PO Report';
		$this->filename 	= 'logwin_aldi_clp_po_report.xlsx';
		$this->scriptPath 	= 'report/report_clp_po/';
		$this->reportType	= Abstract_report_clp_model::$REPORT_TYPE_PO;
		//$this->output->enable_profiler(TRUE);
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}

/* End of file report.php */
/* Location: ./app/controllers/report.php */
