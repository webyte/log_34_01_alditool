<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );
//require_once( BASEPATH.'../../phpdocx_pro/classes/CreateDocx.inc' );
class Report_clp extends Abstract_controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Report_model', 'model');
        $this->headline	= 'Reporting';
        //$this->output->enable_profiler(TRUE);
    }

    /**
     * @Override
     */
    public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
        $this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
        $this->records					= array();

        $data = $this->getViewData();
        $data['cdaResult'] = '';
        $this->load->view('report/report_clp', $data );
    }

    /**
     * @Override
     */
    private function getViewData() {
        $clpFormFields	 				= $this->getCustomCLPFormFields();

        $data = array();
        $data['clpForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_clp/clp_report_export', $clpFormFields, 'Go', $this->recordListSearchParams );
        $data['clpHeadline'] 		= 'CLP report';
        $data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
        return $data;
    }


    /**
     * clp_report_export
     * @return
     */
    public function clp_report_export(){
        $this->load->library('dompdf_gen');

        $this->recordListSearchParams 	= $this->input->post();
        $this->records					= $this->model->getCLPRecords( $this->recordListSearchParams, array() );
        $data['records']				= $this->records;
        $html = $this->load->view('report/clp_report', $data, true );
//        print($html);
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream('logwin_aldi_clp_report.pdf');
    }

    ////////////////////////////////////////////////////////////////////////////

    //		private

    ////////////////////////////////////////////////////////////////////////////


    /**
     * getCustomCLPFormFields
     * creates custom formfields for searchform.
     */
    private function getCustomCLPFormFields() {
        $res = array();
        $tmp = array();

        $tmp['prefield_markup'] = "";
        $tmp['label'] = "On-Sale week";
        $tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
        $res[] = $tmp;

        return $res;
    }

}

/* End of file report.php */
/* Location: ./app/controllers/report.php */