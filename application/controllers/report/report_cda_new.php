<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );
//require_once( BASEPATH.'../../phpdocx_pro/classes/CreateDocx.inc' );

class Report_cda_new extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_model', 'model');
		$this->headline	= 'Reporting';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();

		$data = $this->getViewData();
		$data['cdaResult'] = '';
		$this->load->view('report/report_cda', $data );
	}

	/**
	 * cdaRecordList
	 * @return
	 */
	public function cdaRecordList(){
		$this->recordListSearchParams 	= $this->input->post();
		$this->recordListSearchParams['shipments_crdate'] = array( date( 'Y-m-d 00:00:00', strtotime( '-183 days', time() ) ) );
		$this->records					= $this->model->getCDARecords( $this->recordListSearchParams, array() );

		$data = $this->getViewData();
		$data['cdaResult'] = ( count( $this->records ) > 0 ) ?  $this->load->view('report/cda_result', array( 'records' => $this->records, 'action' => 'report/report_cda_new/cda_report_export' ), TRUE ) : '';
		$this->load->view('report/report_cda', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$cdaFormFields	 			= $this->getCustomCDAFormFields();

		$data = array();
		$data['cdaForm']			= generateCustomSearchForm( $this->tablename, array(), 'report/report_cda_new/cdaRecordList', $cdaFormFields, 'Go', $this->recordListSearchParams );
		$data['cdaHeadline'] 		= 'Container Load Plan/Delivery Docket/Container Weight Declaration *NEW*';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/report.js"></script>';
		return $data;
	}

	/**
	 * cda_report_export
	 * @return
	 */
	public function cda_report_export(){
		$this->load->library('zip');
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$counter			= 1;
		$out 				= new PHPExcel();
		$out->removeSheetByIndex(0);
		$shipments 			= $this->input->post('shipmentid');
		$filename			= '';
		foreach( $shipments as $shipment ) {
			$file = $this->generateExcelDoc( $shipment );
			$this->zip->read_file( $file );
		}

		$this->zip->download('clp_report.zip');
	}

	/**
	 * savePrintDate
	 */
	public function savePrintDate( $id ) {
		$this->load->model('Shipment_model', 'shipment_model');
		$this->load->model('Activity_model', 'activity_model');
		$previousShipment = $this->shipment_model->getRecord( $id );

		$this->shipment_model->setPrintDate( $id );

		$latestShipment = $this->shipment_model->getRecord( $id );
		$this->activity_model->logChanges( $latestShipment, $previousShipment, $latestShipment['container_number'], Activity_model::$DATA_TYPE_SHIPMENT );
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * generateExcelDoc
	 */
	private function generateExcelDoc( $shipment ) {

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$this->records 	= $this->model->getCDARecords( array( 'shipments_id' => array( $shipment ) ), array() );
		$record			= $this->records[0];
		$template 		= $this->config->item('app_path').'views/report/cda_template_new.xlsx';

		$objReader 		= new PHPExcel_Reader_Excel2007();
		$objPHPExcel 	= $objReader->load( $template );
		$file			= $this->config->item('archive_path').$record['shipments_container_number'].'.xlsx';

		$objWorksheet 	= $objPHPExcel->getActiveSheet();
		$objWorksheet->setTitle($record['shipments_container_number']);
		$objWorksheet->getPageSetup()->setFitToPage( TRUE );
		$objWorksheet->getCell('B8')->setValue( 'Delivery To: ALDI Stores'."\n".$record['shipments_dc_address'] );
		$objWorksheet->getCell('F11')->setValue( $record['shipments_region'] );
		$objWorksheet->getCell('F12')->setValue( $record['contracts_contract_number'] );
		$objWorksheet->getCell('F13')->setValue( $record['shipments_etd'] );
		$objWorksheet->getCell('F14')->setValue( $record['contracts_advertisement_week'] );
		$objWorksheet->getCell('F16')->setValue( $record['shipments_incoterm_code'] );
		$objWorksheet->getCell('F18')->setValue( $record['shipments_mother_vessel_name'].', '.$record['shipments_voyage_number']);
		$objWorksheet->getCell('F19')->setValue( $record['shipments_departure_port'] );
		$objWorksheet->getCell('F20')->setValue( $record['shipments_etd'] );
		$objWorksheet->getCell('F21')->setValue( $record['shipments_destination_port'] );
		$objWorksheet->getCell('F22')->setValue( $record['shipments_eta'] );


		$objWorksheet->getCell('F23')->setValue( $record['shipments_traffic_type'] );
		$objWorksheet->getCell('F26')->setValue( $record['shipments_container_size_weight'] );
		$objWorksheet->getCell('F27')->setValue( $record['shipments_container_number'] );
		$objWorksheet->getCell('F28')->setValue( $record['shipments_seal_number'] );
		$objWorksheet->getCell('F29')->setValue( $record['deliveries_sum_gross_weight'] );
		$objWorksheet->getCell('F30')->setValue( $record['shipments_container_tare_weight'] );
		$objWorksheet->getCell('F31')->setValue( $record['deliveries_sum_gross_weight']+$record['shipments_container_tare_weight'] );

		$offset = 39;
		$i = $offset;
		$totalNumberOfCases = 0;
		$totalCbm = 0;

		foreach( $record['deliveries'] as $delivery ) {
			$objWorksheet->getCell('A'.$i)->setValue( $delivery['contracts_supplier'] );
			$objWorksheet->getCell('C'.$i)->setValue( $delivery['contracts_on_sale_date'] );
			$objWorksheet->getCell('D'.$i)->setValue( $delivery['deliveries_product_code'] );
			$objWorksheet->getCell('E'.$i)->setValue( $delivery['deliveries_contract_number'] );
			$objWorksheet->getCell('F'.$i)->setValue( $delivery['contracts_units_per_case'] );
			$objWorksheet->getCell('G'.$i)->setValue( $delivery['deliveries_product_description'] );
			$objWorksheet->getCell('H'.$i)->setValue( $delivery['shipments_deliveries_mm_case_quantity'] );
			$objWorksheet->getCell('I'.$i)->setValue( $delivery['shipments_deliveries_mm_unit_quantity'] );
			$objWorksheet->getCell('J'.$i)->setValueExplicit( $delivery['shipments_deliveries_mm_volume'], PHPExcel_Cell_DataType::TYPE_STRING );
			$l = str_replace('.000', '', $delivery['contracts_case_length'] );
			$w = str_replace('.000', '', $delivery['contracts_case_width'] );
			$h = str_replace('.000', '', $delivery['contracts_case_height'] );
			$objWorksheet->getCell('K'.$i)->setValue( $l.'x'.$w.'x'.$h );
			$objWorksheet->getCell('L'.$i)->setValue( $delivery['deliveries_dc'] );

			if( ( $i - $offset + 1 ) < count( $record['deliveries'] ) ) {
				$objPHPExcel->getActiveSheet()->insertNewRowBefore( $i+1, 1 );
				$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+1).':B'.($i+1));
			}

			$totalNumberOfCases += $delivery['shipments_deliveries_mm_case_quantity'];
			$totalCbm += $delivery['shipments_deliveries_mm_volume'];
			$i++;
		}

		$objWorksheet->getCell('H'.$i)->setValue( $totalNumberOfCases );
		$objWorksheet->getCell('J'.$i)->setValueExplicit( $totalCbm, PHPExcel_Cell_DataType::TYPE_STRING );

		$objWorksheet->getCell('B'.($i+2))->setValue( 'Packing: '.$record['shipments_loading_type'] );

		$objWorksheet->getCell('J'.($i+2))->setValue( 'Carrier: ' );
		$objWorksheet->getCell('K'.($i+2))->setValue( $record['shipments_carrier'] );
		$objWorksheet->getCell('J'.($i+3))->setValue( 'DG: '.$record['shipments_dg'] );
		$objWorksheet->getCell('K'.($i+3))->setValue( 'DG Class: '.$record['shipments_dg_class'] );
		$objWorksheet->getCell('J'.($i+4))->setValue( 'Contract Type: '.$record['shipments_contract_type'] );
		$objWorksheet->getCell('L'.($i+4))->setValue( $record['shipments_online_rate'] );

		$detentionFreeDays = ( !empty( $record['shipments_detention_free_days'] ) ) ? 'Detention Free Days: '.$record['shipments_detention_free_days'] : 'Check with Carrier for Detention Free Days';
		$objWorksheet->getCell('J'.($i+5))->setValue( $detentionFreeDays );

		$objWriter 				= IOFactory::createWriter( $objPHPExcel, 'Excel2007' );
		$objWriter->save( $file );

		// set shipment print date
		if( $this->acl->hasPermission( Acl::$action_prefix.'report/report_cda_savePrintDate' ) ) $this->savePrintDate( $shipment );

		return $file;

	}

	/**
	 * getCustomCDAFormFields
	 * creates custom formfields for searchform.
	 */
	private function getCustomCDAFormFields() {

		$res = array();
		$tmp = array();

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-Sale week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = $this->tca->getFormInputById('shipments_container_number', $this->getSearchFormValue('shipments_container_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Telex Rel./Orig. B/L received";
		$tmp['input'] = $this->tca->getFormInputById('shipments_telex_received', $this->getSearchFormValue('shipments_telex_received'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Destination DC";
		$tmp['input'] = $this->tca->getFormInputById('shipments_dc', $this->getSearchFormValue('shipments_dc'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "HBL/HAWB";
		$tmp['input'] = '<input type="text" id="shipments_hbl_hawb" value="'.$this->getSearchFormValue('shipments_hbl_hawb').'" name="shipments_hbl_hawb[]">';
		$res[] = $tmp;

		return $res;
	}
}

/* End of file report.php */
/* Location: ./app/controllers/report.php */