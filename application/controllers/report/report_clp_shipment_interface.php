<?php
require_once( APPPATH.'controllers/report/abstract_report_clp.php' );

class Report_clp_shipment_interface extends Abstract_report_clp {

	function __construct() {
		parent::__construct();
		$this->load->model('Report_clp_shipment_interface_model', 'model');
		$this->headline		= 'OFC - Shipment Report (Interface)';
        $this->filename 	= 'AU_LIBU_FFShipmentInbound_ALDI_Logwin_'.date('Ymd').'_'.random_string('alnum', 32).'.csv.ready';
		$this->scriptPath 	= 'report/report_clp_shipment_interface/';
		$this->reportType	= Abstract_report_clp_model::$REPORT_TYPE_SHIPMENT_INTERFACE;
		$this->writerType 	= \Box\Spout\Common\Type::CSV;
		//$this->output->enable_profiler(TRUE);
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}

/* End of file report.php */
/* Location: ./app/controllers/report.php */
