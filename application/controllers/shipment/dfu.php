<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Dfu extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Dfu_model', 'model');
		$this->load->model('Supplier_model', 'supplier_model');
		$this->load->model('Contactperson_model', 'contactperson_model');
		$this->load->model('Activity_model', 'activity_model');
		$this->load->helper( 'str_helper' );
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= ( count( $this->recordListSearchParams ) > 0 ) ? $this->model->getRecords( $this->recordListSearchParams, array() ) : array();

		$data = $this->getViewData();
		$this->load->view('shipment/dfu_overview', $data );
	}

	/**
	 * @Override
	 */
	private function getViewData() {
		$searchFormFields	 	= $this->getCustomSearchFormFields();
		$data = array();
		$data['form']			= generateCustomSearchForm( $this->tablename, array(), 'shipment/dfu/recordList', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['headline'] 		= 'Document follow up';
		$data['result'] 		= ( count( $this->records ) > 0 ) ?  $this->load->view('shipment/dfu_result', array( 'records' => $this->records ), TRUE ) : '';
		$data['dfuMail']		= $this->getMailData( $this->records );
		$data['additionalJS'] 	= '<script src="'.base_url().'js/dfu.js"></script>';
		return $data;
	}

	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		return $res;
	}

	/**
	 * sendMail
	 * @return void
	 */
	public function sendMail() {
		$subject 			= utf8_encode( $this->input->post('subject') );
		$body 				= utf8_encode( $this->input->post('body') );
		$recipients 		= $this->input->post('recipients');
		$contractNumbers 	= $this->input->post('contractNumbers');
		$docTypes 			= $this->input->post('docTypes');
		$shipmentIds 		= $this->input->post('shipmentIds');
		$contractNumber 	= count( $contractNumbers ) > 0 ? $contractNumbers[0] : '';
		$contractId			= $this->contract_model->getIdByContractNumber( $contractNumber );
		$advertisementWeek 	= $this->input->post('advertisementWeek');
		$ccRecipients		= 'aldicustoms@logwin-logistics.com';//$this->model->getCCRecipientsByOnSaleWeek( $advertisementWeek );

		$this->saveClickDate( $shipmentIds, $docTypes );

		$mailData	= array();
		foreach( $recipients as $recipient ) {
			$supplier				= $this->contactperson_model->getRecord( $recipient );
			$mailData['RECIPIENT'] 	= $supplier['name'];
			$bodyProcessed			= replacePattern( $body, $mailData );

			$data 				= array();
			$data['subject'] 	= $subject;
			$data['recipient'] 	= $supplier['email'];
			$data['cc'] 		= $ccRecipients;
			$data['body'] 		= $bodyProcessed;
			$this->doSendMail( $data );

			// log data
			$logData['id'] 			= $contractId;
			$logData['recipient'] 	= $supplier['email'];
			$logData['cc'] 			= $ccRecipients;
			$logData['subject'] 	= $subject;
			$logData['body'] 		= $bodyProcessed;
			$logData['mail_type'] 	= 'Document follow up email';
			$this->activity_model->logChanges( $logData, array(), $contractNumber, Activity_model::$DATA_TYPE_CONTRACT );

		}
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	private function saveClickDate( $shipmentIds, $docTypes ) {
		$this->load->model('Shipment_model', 'shipment_model');
		$map = array(
			'invoice_click_date' 				=> 'Invoice',
			'bl_click_date' 					=> 'B/L',
			'packing_list_click_date' 			=> 'Packing List',
			'treatment_certificate_click_date' 	=> 'Treatment Certificate',
			'packing_declaration_click_date' 	=> 'Packing Declaration',
			'certificate_of_origin_click_date' 	=> 'Certificate of Origin',
		);
		foreach( $shipmentIds as $shipmentId ) {
			foreach( $docTypes as $docType ){
				$field = array_search($docType, $map);
				if( $field !== FALSE ) $this->shipment_model->updateRecord( $shipmentId, array( $field => date( 'Y-m-d H:i:s' ) ) );
			}
		}

	}


	private function getMailData( $recorddata ) {
		$mailData 			= array();
		$recipients 		= '';
		$advertisementWeek 	= '';
		if( count( $recorddata ) > 0 ) {
			$mailData['CONTRACT_NUMBER'] 	= $recorddata[0]['contracts_contract_number'];
			$mailData['PRODUCT_CODE'] 		= $recorddata[0]['deliveries_product_code'];
			$recipients						= $this->model->getRecipients( $recorddata );
			$advertisementWeek				= $recorddata[0]['contracts_advertisement_week'];
			switch( $recorddata[0]['contracts_contract_type'] ) {
				case 1: //core range
					$mailData['MORE'] = 'Core Range';
					break;
				case 2: //store equipment
					$mailData['MORE'] = 'Store Equipment';
					break;
				case 3: //special buys
					$mailData['MORE'] = 'On-Sale Week '.$advertisementWeek;
					break;
				default:
					$mailData['MORE'] = '';
			}
		}
		$subject 	= replacePattern( $this->lang->line( 'dfu.mail.subject' ), $mailData );
		$body		= replacePattern( $this->lang->line( 'dfu.mail.body' ), $mailData );
		return array( 'subject' => $subject, 'body' => $body, 'recipients' => $recipients, 'advertisement_week' => $advertisementWeek );
	}

	/**
	 * doSendMail
	 * @return boolean
	 */
	private function doSendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$subject 	= $data['subject'];
		$body 		= $data['body'];
		$cc 		= $data['cc'];

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->cc( $cc );
		$this->email->subject( $subject );
		$this->email->message( $body );
		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( "Sending Mail to ".$toMail." failed, error: ".$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, "Document_follow_up.doSendMail" );
			return false;
		} else {
			$this->welogger->log( "Mail was sent to ".$toMail." \nwith subject: ".$subject." \nand message: ".$body, WELogger::$LOG_LEVEL_INFO, "Document_follow_up.doSendMail" );
			return true;
		}
		$this->email->clear(TRUE);
	}

}
/* End of file dfu.php */
/* Location: ./app/controllers/dfu.php */