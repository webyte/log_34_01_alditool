<?php
require_once( APPPATH.'controllers/shipment/shipment'.EXT );

class Shipment_confirmed extends Shipment {

	function __construct() {
		parent::__construct();
		$this->scriptPath 			= 'shipment/shipment_confirmed/';
		$this->headline				= 'Confirmed CLPs';
		$this->approvalFilter		= TRUE;
		$this->approvalValue		= 1;
		//$this->output->enable_profiler(TRUE);
	}


	/**
	 * getCustomRecordActions
	 * @return array recordactions
	 */
	public function getCustomRecordActions( $table = '' ) {
		$recordActions 		= array();
		$table				= $table == '' ? $this->tablename : $table;
		$edit_perm_name 	= Acl::$crud_prefix.$table.'_edit';
		//$delete_perm_name 	= Acl::$crud_prefix.$table.'_delete';
		$delete_perm_name 	= Acl::$action_prefix.'shipment/shipment_confirmed_delete';

		if( $this->acl->hasPermission( $edit_perm_name ) ) 		$recordActions[] 	= array('controller' => $this->scriptPath.'edit', 'icon' => 'icon-pencil', 'title' => "edit" );
		if( $this->acl->hasPermission( $delete_perm_name ) ) 	$recordActions[] 	= array('controller' => $this->scriptPath.'delete', 'icon' => 'icon-trash', 'title' => "delete" );
		return $recordActions;
	}


	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}
/* End of file shipment_unconfirmed.php */
/* Location: ./app/controllers/shipment_unconfirmed.php */