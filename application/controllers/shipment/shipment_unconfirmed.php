<?php
require_once( APPPATH.'controllers/shipment/shipment'.EXT );

class Shipment_unconfirmed extends Shipment {

	function __construct() {
		parent::__construct();
		$this->scriptPath 			= 'shipment/shipment_unconfirmed/';
		$this->headline				= 'Unconfirmed CLPs';
		$this->approvalFilter		= TRUE;
		$this->approvalValue		= 0;
		//$this->output->enable_profiler(TRUE);
	}


	/**
	 * getCustomRecordActions
	 * @return array recordactions
	 */
	public function getCustomRecordActions( $table = '' ) {
		$recordActions 		= array();
		$table				= $table == '' ? $this->tablename : $table;
		$edit_perm_name 	= Acl::$crud_prefix.$table.'_edit';
		//$delete_perm_name 	= Acl::$crud_prefix.$table.'_delete';
		$delete_perm_name 	= Acl::$action_prefix.'shipment/shipment_unconfirmed_delete';

		if( $this->acl->hasPermission( $edit_perm_name ) ) 		$recordActions[] 	= array('controller' => $this->scriptPath.'edit', 'icon' => 'icon-pencil', 'title' => "edit" );
		if( $this->acl->hasPermission( $delete_perm_name ) ) 	$recordActions[] 	= array('controller' => $this->scriptPath.'delete', 'icon' => 'icon-trash', 'title' => "delete" );

////        $this->session->set_userdata( array('cpl_formData'=> $postData) );
//        $userData = $this->session->all_userdata();
//
//


//
//        $postData = $post = $this->input->post();
//
//
////        $this->session->unset_userdata( $postData );
//
//        $userData2 = $this->session->all_userdata();
//
//        echo '<pre>';
//        var_dump($userData2);
//        echo '</pre>';

        return $recordActions;
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}
/* End of file shipment_unconfirmed.php */
/* Location: ./app/controllers/shipment_unconfirmed.php */