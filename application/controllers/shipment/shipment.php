<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Shipment extends Abstract_controller {

	private $alreadySentHblHawbs = array();

	function __construct() {
		parent::__construct();
		$this->load->model('Shipment_model', 'model');
		$this->load->model('Delivery_model', 'delivery_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Comment_model', 'comment_model');
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Activity_model', 'activity_model');

		$this->tablename 			= 'shipments';
		$this->scriptPath 			= 'shipment/shipment/';
		$this->recordEditView 		= 'shipment/shipmentdata_edit';
		$this->headline				= 'All CLPs';
		$this->infotext				= '';
		$this->showDefaultSearchForm = FALSE;
		$this->approvalFilter		= FALSE;
		$this->approvalValue		= 0;

		$this->overviewFields 	= array(
			array('label' => 'HBL/HAWB', 			'fieldname' => 'hbl_hawb', 				'type' => ''),
			array('label' => 'Container number', 	'fieldname' => 'container_number', 		'type' => ''),
			array('label' => 'Logwin origin office', 'fieldname' => 'logwin_origin_office', 'type' => ''),
			array('label' => 'Contract/PO number', 	'fieldname' => 'contract_number', 		'type' => ''),
			array('label' => 'DC', 					'fieldname' => 'dc', 					'type' => ''),
			array('label' => 'On-Sale week', 		'fieldname' => 'advertisement_week', 	'type' => ''),
			array('label' => 'FOB date', 			'fieldname' => 'fob_date', 				'type' => 'date'),
			array('label' => 'ALDI AU WH delivery due date', 'fieldname' => 'aldi_au_wh_delivery_due_date', 'type' => 'date'),
			array('label' => 'Departure', 			'fieldname' => 'departure', 			'type' => ''),
			array('label' => 'Destination', 		'fieldname' => 'destination', 			'type' => ''),
		);
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * @Override
	 */
	public function unconfirmed( $id = 0 ) {
		$this->recordList();
	}

	/**
	 * @Override
	 */
	public function confirmed( $id = 0 ) {
		$this->recordList();
	}

	/**
	 * @Override
	 */
	public function all( $id = 0 ) {
		$this->recordList();
	}

	/**
	 * @Override
	 */
	public function edit( $id = 0, $source = '' ) {
		$this->welogger->log( "edit called with id: ".$id, WELogger::$LOG_LEVEL_INFO, "Shipment.edit" );
		$records 		= $this->model->getRecord( $id );
		if ( count( $records ) > 0){
			$recorddata = $records;
			$formaction = $this->scriptPath."updateRecord/".$id;
		} else {
			$recorddata = array();
			$formaction = $this->scriptPath."createNew";
		}

		$data['form'] 				= $this->tca->getFormArray( $formaction, array( $this->tablename => $recorddata ) );
		$data['headline'] 			= $this->headline;
		$data['addDeliveryPerms']	= $this->acl->hasPermission( Acl::$action_prefix.'shipment/shipment_addDelivery' );
		$data['additionalJS'] 		= '<script src="'.base_url().'js/shipment.js"></script>';
		$data['deleteInsteadOfCancel'] = ( $source == 'consolidate' ) ? TRUE : FALSE;
		$this->load->view($this->recordEditView, $data );
	}

	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		//$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;

//		$shipments = $this->model->getRecordsWhere( array( 'crdate >=' => '2018-01-01 17:03:16', 'deleted'=>0 ) );
//		foreach( $shipments as $shipment ) {
//
//			$containers = $this->model->getRecordsWhere( array( 'container_number' => $shipment['container_number'], 'deleted' => 0 ) );
//			$tmp = array();
//			foreach( $containers as $container ) {
//				$tmp[] = $container['hbl_hawb'];
//			}
//			$tmp = array_unique( $tmp );
//			if( count( $tmp ) > 1 ) {
//				var_dump($shipment['container_number']);
//				var_dump($tmp);
//			}
//		}

		if( $this->input->post() && !$ignoreSearchParams ) {
			$this->recordListSearchParams = $this->input->post();
			$this->session->set_userdata( array( 'searchFormData' => $this->recordListSearchParams ) );
		} else {
			if( count( $this->session->userdata('searchFormData') ) > 0 ) {
				$this->recordListSearchParams = $this->session->userdata('searchFormData');
			} else {
				$this->recordListSearchParams = $searchParams;
			}
		}

		if( $this->approvalFilter ) {
			$this->recordListSearchParams['shipments_approval'] = array( $this->approvalValue );
		}

		//		$this->records					= $this->model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );
		$this->records					= array();
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']			= generateCustomSearchForm( $this->tablename, array(), $this->scriptPath.'result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']		= generateCustomOverview( $this->overviewFields, $this->records, $this->getCustomRecordActions() );
		$data['headline'] 			= $this->headline;
		$data['infotext'] 			= $this->infotext;
		$data['createnewlink'] 		= $this->acl->hasPermission( Acl::$crud_prefix.$this->tablename.'_new' ) ? $this->scriptPath.'edit' : '';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/shipment.js"></script>';

		$this->load->view('general/record_overview', $data );
	}

	/**
	 * @Override
	 */
	public function result( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		//$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		if( $this->input->post() && !$ignoreSearchParams ) {
			$this->recordListSearchParams = $this->input->post();
			$this->session->set_userdata( array( 'searchFormData' => $this->recordListSearchParams ) );
		} else {
			if( count( $this->session->userdata('searchFormData') ) > 0 ) {
				$this->recordListSearchParams = $this->session->userdata('searchFormData');
			} else {
				$this->recordListSearchParams = $searchParams;
			}
		}

		if( $this->approvalFilter ) {
			$this->recordListSearchParams['shipments_approval'] = array( $this->approvalValue );
		}
		$this->recordListSearchParams['shipments_modified'] = array( date( 'Y-m-d 00:00:00', strtotime( '-183 days', time() ) ) );
		$this->records					= $this->model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']			= generateCustomSearchForm( $this->tablename, array(), $this->scriptPath.'result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']		= generateCustomOverview( $this->overviewFields, $this->records, $this->getCustomRecordActions() );
		$data['headline'] 			= $this->headline;
		$data['infotext'] 			= $this->infotext;
		$data['createnewlink'] 		= $this->acl->hasPermission( Acl::$crud_prefix.$this->tablename.'_new' ) ? $this->scriptPath.'edit' : '';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/shipment.js"></script>';

		$this->load->view('general/record_overview', $data );
	}

	/**
	 * @Override
	 */
	public function createNew() {
		$this->welogger->log( "createNew called", WELogger::$LOG_LEVEL_INFO, "Shipment.createNew" );
		$this->saveUserInput();
		//this->recordList( array(), TRUE );
		redirect( $this->scriptPath );
	}

	/**
	 * @Override
	 */
	public function updateRecord( $id = 0 ) {
		$this->welogger->log( "updateRecord called with id: ".$id, WELogger::$LOG_LEVEL_INFO, "Shipment.updateRecord" );
		$this->saveUserInput( $id );
		//$this->recordList( array(), TRUE );
		redirect( $this->scriptPath );
	}

	/**
	 * @Override
	 */
	public function delete( $id ) {
		$this->welogger->log( "delete called with id: ".$id, WELogger::$LOG_LEVEL_INFO, "Shipment.delete" );
		$this->model->deleteRecord( $id );
		$deliveries = $this->shipment_delivery_model->getDeliveriesByShipmentId( $id );
		foreach( $deliveries as $delivery ) {
			$this->welogger->log( "delivery with id: ".$id." deleted", WELogger::$LOG_LEVEL_INFO, "Shipment.delete" );
			$this->shipment_delivery_model->deleteRecord( $id, $delivery['id'] );
		}

		$this->load->model('Pcshipment_model', 'pcshipment_model');
		$pcshipments = $this->pcshipment_model->getRecordsWhere( array( 'shipment_id' => $id, 'hidden' => 0, 'deleted' => 0 ) );
		foreach( $pcshipments as $pcshipment ) {
			$this->pcshipment_model->updateRecord( $pcshipment['id'], array( 'shipment_id' => 0 )  );
		}

		$this->recordList();
	}

	/**
	 * addDelivery
	 * returns a html-form for a delivery record to be inserted
	 * in the frontend dynamically.
	 */
	public function addDelivery( $deliveryId, $shipmentId ) {
		$this->welogger->log( "addDelivery called with deliveryId: ".$deliveryId." and shipmentId: ".$shipmentId, WELogger::$LOG_LEVEL_INFO, "Shipment.addDelivery" );

		$delivery 						= $this->delivery_model->getRecord( $deliveryId );
		$totalQuantity					= $delivery['unit_quantity'];
		$actualUnitQuantiy				= $this->shipment_delivery_model->getActualUnitQuantityByDeliveryId( $deliveryId );
		$remainQuantity					= $totalQuantity - $actualUnitQuantiy;
		$delivery['unit_quantity_mm'] 	= $remainQuantity;
		$delivery['unit_quantity_remain'] = $remainQuantity;
		$data 							= array();
		$data['row']['approval'] 		= 2;
		$data['form'] 					= $this->tca->getFormArray( "" , array( 'deliveries' => $delivery ) );
		$res 							= $this->load->view('shipment/deliverydata_edit', $data, true );

		$this->shipment_delivery_model->createNew( array( 'shipment_id' => $shipmentId, 'delivery_id' => $deliveryId ) );

		print( $res );
	}

	/**
	 * removeDelivery
	 * removes delivery from shipment
	 * in the frontend dynamically.
	 */
	public function removeDelivery( $shipmentId=0, $deliveryId=0 ) {
		$this->welogger->log( "removeDelivery called with deliveryId: ".$deliveryId." and shipmentId: ".$shipmentId, WELogger::$LOG_LEVEL_INFO, "Shipment.removeDelivery" );
		if( $shipmentId > 0 && $deliveryId > 0 ) {
			$this->shipment_delivery_model->deleteRecord( $shipmentId, $deliveryId );
		}
	}

	/**
	 * sendTelexReleaseMail
	 * submits mail
	 */
	public function sendTelexReleaseMail( $shipmentId=0, $hblHawb='' ) {
		$this->welogger->log( "sendTelexReleaseMail called with shipmentId: ".$shipmentId.", hblHawb: ".$hblHawb, WELogger::$LOG_LEVEL_INFO, "Shipment.sendTelexReleaseMail" );
		$this->load->helper( 'str_helper' );

		$shipments = $this->shipment_delivery_model->getRecordsWhere( array( 'hbl_hawb LIKE' => '%'.$hblHawb.'%', 'telex_received >=' => 1 ) );


		$this->welogger->log( "sendTelexReleaseMail num of shipments: ".count( $shipments ), WELogger::$LOG_LEVEL_INFO, "Shipment.sendTelexReleaseMail" );
		if( count( $shipments ) < 1 ) {
			$shipment = $this->model->getRecord( $shipmentId );

			$containerNumbers = $this->model->getContainerNumbersByHblHawb( $hblHawb );

			$mailData						= array();
			$mailData['HBL_HAWB'] 			= $hblHawb;
			$mailData['DEPARTURE_PORT'] 	= $shipment['departure_port'];
			$mailData['MOTHER_VESSEL_NAME'] = $shipment['mother_vessel_name'];
			$mailData['CONTAINER_NUMBER'] 	= implode(', ', $containerNumbers);

			$data 				= array();
			$data['subject'] 	= replacePattern( $this->lang->line( 'clp.telex.mail.subject' ), $mailData );
			$data['body'] 		= replacePattern( $this->lang->line( 'clp.telex.mail.body' ), $mailData );

			//5|AUADL,2|AUBNE,3|AUFRE,4|AUMEL,1|AUSYD

			//1 AUSYD: Aldi-Imports-Syd@logwin-logistics.com
			//2 AUBNE: Aldi-Imports-Bri@logwin-logistics.com
			//3 AUFRE: Aldi-Imports-Per@logwin-logistics.com
			//4 AUMEL: Aldi-Imports-Mel@logwin-logistics.com
			//5 AUADL: Aldi-Imports-Mel@logwin-logistics.com

			switch( $shipment['destination_port'] ) {
				case 1:
					$data['recipient'] = 'Aldi-Imports-Syd@logwin-logistics.com';
					break;
				case 2:
					$data['recipient'] = 'Aldi-Imports-Bri@logwin-logistics.com';
					break;
				case 3:
					$data['recipient'] = 'Aldi-Imports-Per@logwin-logistics.com';
					break;
				case 4:
					$data['recipient'] = 'Aldi-Imports-Mel@logwin-logistics.com';
					break;
				case 5:
					$data['recipient'] = 'Aldi-Imports-Mel@logwin-logistics.com';
					break;
			}


			$data['cc'] = array('Stefanie.Stahl@logwin-logistics.com');
			$LOOTelexMail = $this->model->getLOOTelexMail( $shipmentId );
			if( !empty($LOOTelexMail) ) $data['cc'][] = $LOOTelexMail;

			$this->welogger->log( "sendTelexReleaseMail destination port: ".$shipment['destination_port'].', recipient: '.$data['recipient'], WELogger::$LOG_LEVEL_INFO, "Shipment.sendTelexReleaseMail" );

			if(!in_array( $hblHawb, $this->alreadySentHblHawbs)) {
				if( ENVIRONMENT != 'production' ) $data['recipient'] = array('or@webyte.org','Stefanie.Stahl@logwin-logistics.com');
				if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
				$this->alreadySentHblHawbs[] = $hblHawb;
			}
		}
	}

	/**
	 * getContractNumbers
	 * returns a json encoded array with associated po numbers
	 * in the frontend dynamically.
	 */
	public function getContractNumbers() {
		$query = $this->input->post('query');
		$result = $this->contract_model->getContractNumbers( $query );
		$tmp = array();
		foreach( $result as $row ) {
			$tmp[] = $row['contract_number'];
		}
		print( json_encode( array( 'options' => $tmp ) ) );
	}

	/**
	 * getDelayReasons
	 * returns a json encoded array with associated po numbers
	 * in the frontend dynamically.
	 */
	public function getDelayReasons() {
		$this->load->model('Delay_reason_model', 'delay_reason_model');
		$group = $this->input->post('group');
		$result = $this->delay_reason_model->getRecordsWhere( array( 'group'=> $group, 'crdate >=' => '2023-01-01', 'hidden' => 0, 'deleted' => 0 ), 'title' );
		print( json_encode( $result ) );
	}

	/**
	 * getContainerTareWeight
	 * returns a json encoded array with associated po numbers
	 * in the frontend dynamically.
	 */
	public function getContainerTareWeight() {
		$this->load->model('Container_type_model', 'container_type_model');
		$out = '0';
		$containerType = $this->input->post('containerType');
		$containerSize = $this->input->post('containerSize');
		$rows = $this->container_type_model->getRecordsWhere( array( 'container_type'=> $containerType, 'container_size'=> $containerSize, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $rows ) ) {
			$out = $rows[0]['tare_weight'];
		}
		print( json_encode( array( 'tare_weight' => $out ) ) );
	}

	/**
	 * getDeparturepPorts
	 * returns a json encoded array with associated departure ports
	 * in the frontend dynamically.
	 */
	public function getDeparturePorts() {
		$this->load->model('Departure_model', 'departure_model');
		$query = $this->input->post('query');
		$result = $this->departure_model->getDeparturePorts( $query );
		$tmp = array();
		foreach( $result as $row ) {
			$tmp[] = $row['name'];
		}
		print( json_encode( array( 'options' => $tmp ) ) );
	}

	/**
	 * checkDeparturePort
	 */
	public function checkDeparturePort( $str = '' ) {
		if( !empty( $str ) ) {
			$this->load->model('Departure_model', 'departure_model');
			$result = $this->departure_model->getRecordsWhere( array( 'name' => $str, 'hidden' => 0, 'deleted' => 0 ) );
			$out = ( count( $result ) > 0 ) ? TRUE : FALSE;
		} else {
			$out = TRUE;
		}
		print( json_encode( array( 'result' => $out ) ) );
	}

	/**
	 * getDeliveries
	 * returns a json encoded array with associated deliveries
	 * in the frontend dynamically.
	 * @param str $contractNumber
	 * @param str $destination
	 */
	public function getDeliveries( $contractNumber, $destination='' ) {
		$contracts = $this->contract_model->getRecordsWhere( array( 'contract_number' => $contractNumber, 'hidden' => 0, 'deleted' => 0 ) );
		if( count( $contracts ) <= 0 ) {
			print( 'error' );
			return FALSE;
		}
		$contractId = $contracts[0]['id'];

		//$data 				= ( !empty( $destination ) ) ? array( 'contract_id' => $contractId, 'destination' => $destination, 'hidden' => 0, 'deleted' => 0 ) : array( 'contract_id' => $contractId, 'hidden' => 0, 'deleted' => 0 );
		$data 				= array( 'contract_id' => $contractId, 'hidden' => 0, 'deleted' => 0 );
		$deliveries 		= $this->delivery_model->getRecordsWhere( $data );
		$tmp				= array();
		foreach( $deliveries as $delivery ) {
			if( count( $this->shipment_delivery_model->getShipmentsByDeliveryId( $delivery['id'] ) ) <= 0 ) $tmp[] = $delivery;
		}
		$data['deliveries'] = $tmp;
		$html 				= $this->load->view( 'shipment/deliverydata_select', $data, true );
		print( $html );
	}

	/**
	 * getCustomRecordActions
	 * @return array recordactions
	 */
	public function getCustomRecordActions( $table = '' ) {
		$recordActions 		= array();
		$table				= $table == '' ? $this->tablename : $table;
		$edit_perm_name 	= Acl::$crud_prefix.$table.'_edit';
		//$delete_perm_name 	= Acl::$crud_prefix.$table.'_delete';
		$delete_perm_name 	= Acl::$action_prefix.'shipment/shipment_delete';

		if( $this->acl->hasPermission( $edit_perm_name ) ) 		$recordActions[] 	= array('controller' => $this->scriptPath.'edit', 'icon' => 'icon-pencil', 'title' => "edit" );
		if( $this->acl->hasPermission( $delete_perm_name ) ) 	$recordActions[] 	= array('controller' => $this->scriptPath.'delete', 'icon' => 'icon-trash', 'title' => "delete" );
		return $recordActions;
	}

	/**
	 * getHBLHAWB
	 * returns a json encoded array with associated hbl/hawb
	 * in the frontend dynamically.
	 */
	public function getHBLHAWB() {
		$this->load->model('Pcshipment_model', 'pcshipment_model');
		$query 	= $this->input->post('query');
		$result = $this->pcshipment_model->getHBLHAWB( $query );
		$tmp = array();
		foreach( $result as $row ) {
			$tmp[] = $row['hbl_hawb'];
		}
		print( json_encode( array( 'options' => $tmp ) ) );
	}

	/**
	 * getContainerNumber
	 * returns a json encoded array with associated container number
	 * in the frontend dynamically.
	 */
	public function getContainerNumber() {
		$this->load->model('Pcshipment_model', 'pcshipment_model');
		$hblHawb 	= $this->input->post('hblhawb');
		$result 	= $this->pcshipment_model->getContainerNumber( $hblHawb );
		$tmp = array();
		foreach( $result as $row ) {
			$tmp[] = $row['container_number'];
		}
		print( json_encode( array( 'options' => $tmp ) ) );
	}

	/**
	 * addHBLHAWB
	 * add HBL/HAWB record to pcshipments
	 * in the frontend dynamically.
	 */
	public function addHBLHAWB() {
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');
		$this->load->model('Pcshipment_model', 'pcshipment_model');
		$success	= FALSE;
		$pcShipment = array();
		$deliveryId = $this->input->post('deliveryId');
		$shipmentId = $this->input->post('shipmentId');
		$hblHawb 	= $this->input->post('hblHawb');
		$containerNumber = $this->input->post('containerNumber');
//		$res 		= $this->pcshipment_model->getRecordsWhere( array( 'hbl_hawb' => $hblHawb, 'container_number' => $containerNumber, 'hidden' => 0, 'deleted' => 0 ) );
//
//		if( count( $res ) > 0 ) {
//			$pcRelationId 	= $this->pcshipment_delivery_model->addRelation( $shipmentId, $deliveryId, $hblHawb, $containerNumber );
//			$pcRelation 	= $this->pcshipment_delivery_model->getRecord( $pcRelationId );
//			$pcShipment		= $this->pcshipment_model->getRecord( $pcRelation['pcshipment_id'] );
//			if( count( $pcShipment ) > 0 ) {
//				unset( $pcShipment['id'] );
//				unset( $pcShipment['hbl_hawb'] );
//				unset( $pcShipment['mbl_mawb'] );
//				unset( $pcShipment['obl'] );
//				unset( $pcShipment['po_number'] );
//				unset( $pcShipment['deliveries'] );
//				unset( $pcShipment['deliveryRelations'] );
//				unset( $pcShipment['crdate'] );
//				unset( $pcShipment['modified'] );
//				unset( $pcShipment['hidden'] );
//				unset( $pcShipment['deleted'] );
//				$this->model->updateRecord( $pcRelation['shipment_id'], $pcShipment );
//				$success 	= TRUE;
//				$pcShipment = $this->pcshipment_model->getPreparedRecord( $pcRelation['pcshipment_id'] );
//			}
//		}
		$success = TRUE;
		print( json_encode( array( 'success' => $success, 'data' => $pcShipment ) ) );

	}

	/**
	 * removeHBLHAWB
	 * add HBL/HAWB record to pcshipments
	 * in the frontend dynamically.
	 */
	public function removeHBLHAWB() {
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');
		$deliveryId = $this->input->post('deliveryId');
		$shipmentId = $this->input->post('shipmentId');
		$hblHawb 	= $this->input->post('hblHawb');
		$this->pcshipment_delivery_model->removeRelation( $shipmentId, $deliveryId, $hblHawb );

	}

	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {

		// ist nur ne quick&dirty Lösung!
		// muss man mal ordentlich machen...

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "HBL/HAWB";
		$tmp['input'] = '<input type="text" id="shipments_hbl_hawb" value="'.$this->getSearchFormValue('shipments_hbl_hawb').'" name="shipments_hbl_hawb[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('contracts_contract_number', $this->getSearchFormValue('contracts_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = '<input type="text" id="shipments_container_number" value="'.$this->getSearchFormValue('shipments_container_number').'" name="shipments_container_number[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Departure";
		$tmp['input'] = $this->tca->getFormInputById('shipments_departure_port', $this->getSearchFormValue('shipments_departure_port'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Destination";
		$tmp['input'] = form_dropdown( 'shipments_destination_port[]', array( "" => "please select ...", "1" => "AUSYD", "2" => "AUBNE", "3" => "AUFRE", "4" => "AUMEL", "5" => "AUADL" ), $this->getSearchFormValue('shipments_destination_port') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Traffic type";
		$tmp['input'] = form_dropdown( 'contracts_traffic_type[]', array( "" => "please select ...", "1" => "Factory", "2" => "CFS", "3" => "AIR", "4" => "PTS Loading", "5" => "Factory/CFS" ), $this->getSearchFormValue('contracts_traffic_type') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'event[]', array( "" => "please select ...", "etd" => "ETD", "eta" => "ETA", "cus" => "CUS", "atd" => "ATD", "ata" => "ATA" ), $this->getSearchFormValue('event') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_from" value="'.$this->getSearchFormValue('shipments_from').'" name="shipments_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="shipments_to" value="'.$this->getSearchFormValue('shipments_to').'" name="shipments_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-Sale week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Logwin origin office";
		$tmp['input'] = $this->tca->getFormInputById('contracts_logwin_origin_office', $this->getSearchFormValue('contracts_logwin_origin_office'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Supplier";
		$tmp['input'] = $this->tca->getFormInputById('contracts_supplier', $this->getSearchFormValue('contracts_supplier'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		return $res;
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////


	/**
	 * saveUserInput
	 * general function for saving user input
	 * @param int $id - id of record to edit. If id is 0 a new record is created.
	 */
	private function saveUserInput( $id = 0 ) {
		$this->welogger->log( "saveUserInput called with id: ".$id, WELogger::$LOG_LEVEL_INFO, "Shipment.saveUserInput" );
		// previousShipment
		$previousShipment = array();
		if( $id > 0 ) $previousShipment = $this->model->getRecord( $id );

		// shipment record
		$insertArray = $this->getInsertArrayFromPostVars();
		$firstShipment = $insertArray[0];

		$comment = "";
		if( array_key_exists('comment', $firstShipment) ){
			$comment = $firstShipment['comment'];
			unset( $firstShipment['comment'] );
		}

		if( $id == 0 ) $id = $this->model->createNew( $firstShipment );
		else $this->model->updateRecord( $id, $firstShipment );

		// delivery record
		$deliveryRecordsInsertArray = $this->getInsertArrayFromPostVars('deliveries');
		foreach ( $deliveryRecordsInsertArray as $deliveryInsertArray ) {
			$unitQuantity 	= isset( $deliveryInsertArray['unit_quantity_mm'] ) ? $deliveryInsertArray['unit_quantity_mm'] : 0;
			$caseQuantity 	= isset( $deliveryInsertArray['case_quantity_mm'] ) ? $deliveryInsertArray['case_quantity_mm'] : 0;
			$hblHawb 		= isset( $deliveryInsertArray['hbl_hawb_mm'] ) ? $deliveryInsertArray['hbl_hawb_mm'] : '';
			$telexReceived 	= isset( $deliveryInsertArray['telex_received_mm'] ) ? $deliveryInsertArray['telex_received_mm'] : '';

			unset( $deliveryInsertArray['unit_quantity_mm'] );
			unset( $deliveryInsertArray['case_quantity_mm'] );
			unset( $deliveryInsertArray['volume_mm'] );
			unset( $deliveryInsertArray['gross_weight_mm'] );
			unset( $deliveryInsertArray['unit_quantity_remain'] );
			unset( $deliveryInsertArray['telex_received_mm'] );
			unset( $deliveryInsertArray['hbl_hawb_mm'] );
			$deliveryId 						= $deliveryInsertArray['id'];
			$deliveryInsertArray['eta'] 		= $firstShipment['eta'];

			// previous delivery
			$previousDelivery = array();
			if( $deliveryId > 0 ) $previousDelivery = $this->delivery_model->getRecordForActivityLog( $deliveryId, $id );

			// previous delivery/shipment relation
			$previousDeliveryShipment = array();
			if( $deliveryId > 0 ) $previousDeliveryShipment = $this->shipment_delivery_model->getRecordsWhere( array( 'delivery_id' => $deliveryId, 'shipment_id' => $id, 'hidden' => 0, 'deleted' => 0 ) );

			// send telex mail
			$this->welogger->log( "sendTelexReleaseMail deliveryId: ".$deliveryId.", telexReceived: ".$telexReceived.", previousDelivery_telex_received: ".$previousDelivery['telex_received'], WELogger::$LOG_LEVEL_INFO, "Shipment.saveUserInput" );
			if( $deliveryId > 0 ) {
				if( $telexReceived === '1' && $previousDelivery['telex_received'] === '0' ) $this->sendTelexReleaseMail( $id, $hblHawb );
			}

			$this->welogger->log( "saveUserInput: save delivery called with id: ".$deliveryId, WELogger::$LOG_LEVEL_INFO, "Shipment.saveUserInput" );
			if( !empty($deliveryId) ){
				unset( $deliveryInsertArray['id'] );
				$this->delivery_model->updateRecord( $deliveryId, $deliveryInsertArray );
			} else {
				$deliveryId = $this->delivery_model->createNew( $deliveryInsertArray );
				$this->shipment_delivery_model->createNew( array( 'shipment_id' => $id, 'delivery_id' => $deliveryId ) );
			}

			if( $unitQuantity > 0 ) {
				$this->shipment_delivery_model->updateUnitQuantity( $id, $deliveryId, $unitQuantity );
			}

			if( isset( $deliveryInsertArray['edl'] ) ) $data['edl'] = $deliveryInsertArray['edl'];
			if( isset( $deliveryInsertArray['adl'] ) ) $data['adl'] = $deliveryInsertArray['adl'];

			if( !empty( $hblHawb ) ) $data['hbl_hawb'] = $hblHawb;
			else $data['hbl_hawb'] = '';
			if( $telexReceived != '' ) {
				$data['telex_received'] = $telexReceived;
				if( count( $previousDeliveryShipment ) > 0 && $previousDeliveryShipment[0]['telex_received'] != $telexReceived ) {
					$data['telex_set_date'] = date( 'Y-m-d H:i:s' );
				}
			}

			$this->shipment_delivery_model->updateRecordMM( $id, $deliveryId, $data );

			// log delivery changes
			$latestDelivery = $this->delivery_model->getRecordForActivityLog( $deliveryId, $id );
			$this->activity_model->logChanges( $latestDelivery, $previousDelivery, $latestDelivery['contract_number'], Activity_model::$DATA_TYPE_DELIVERY );
		}

		// comments
		if( !empty( $comment ) ){
			$this->comment_model->createNew( array( 'msg' => $comment, 'pid' => $id ) );
		}

		// update contract number and dc values
		$this->model->updateContractNumbersAndDCs( $id );

		// update hawb
		$this->model->updateHBLHAWB( $id );

		// update telex received
		$this->model->updateTelexReceived( $id );

		// latestShipment
		$latestShipment = $this->model->getRecord( $id );

		// set approval date
		if( $latestShipment['approval'] == '1' && $previousShipment['approval'] != $latestShipment['approval'] ) $this->model->setApprovalDate( $id );

		// set edo sent click date
		if( $latestShipment['edo_sent'] == '1' && $previousShipment['edo_sent'] != $latestShipment['edo_sent'] ) $this->model->setEdoSentClickDate( $id );

		// update other shipment/delivery relations with the same delivery id
		$this->model->updateOtherShipmentDeliveryRelations( $id );

		// if shipment was disapproved, reset "print date"
		if( $latestShipment['approval'] == '0' && $previousShipment['approval'] = '1' ) $this->model->resetPrintDate( $id );

		// set approval
		if( $latestShipment['atd'] != '0000-00-00 00:00:00' ) $this->model->setApproval( $id );

		$this->activity_model->logChanges( $latestShipment, $previousShipment, $latestShipment['container_number'], Activity_model::$DATA_TYPE_SHIPMENT );
		$this->generateMails( $latestShipment, $previousShipment );

	}

	/**
	 * generateMails
	 * @return
	 */
	private function generateMails( $latestShipment, $previousShipment ) {
		$this->load->helper( 'str_helper' );

		if( count( $previousShipment ) <= 0 ) $previousShipment['approval'] = 0;
		$looContactMail = $this->model->getLOOContactMail( $latestShipment['id'] );

		// submit atd info mail
		if( $latestShipment['approval'] == '1' && !empty( $latestShipment['atd'] ) && $latestShipment['atd'] != '0000-00-00 00:00:00' && $latestShipment['atd_info_mail_sent'] == '0' ) {
			var_dump($latestShipment);
			$mailData						= array();
			$mailData['ADVERTISEMENT_WEEK'] = $this->model->getAdvertisementWeek( $latestShipment['id'] );
			$mailData['CONTAINER_NUMBER'] 	= $latestShipment['container_number'];

			$data 				= array();
			$data['subject'] 	= replacePattern( $this->lang->line( 'atd.inform.mail.subject' ), $mailData );
			$data['body'] 		= replacePattern( $this->lang->line( 'atd.inform.mail.body' ), $mailData );
			$data['recipient'] 	= ( ENVIRONMENT == 'production' ) ? $this->lang->line( 'atd.inform.mail.recipient' ) : '';
			$data['cc'] 		= ( ENVIRONMENT == 'production' ) ? $this->lang->line( 'atd.inform.mail.cc' ) : '';
			if( !empty( $data['recipient'] ) ) $this->sendMail( $data );

			$this->model->updateRecord( $latestShipment['id'], array( 'atd_info_mail_sent' => 1 ) );

			$logData['mail_type'] = 'ATD info mail';
		}

		// submit clp confirmed mail
		if( ( $previousShipment['approval'] == '0' && $latestShipment['approval'] == '1' ) || ( $previousShipment['approval'] == '2' && $latestShipment['approval'] == '1' ) ) {
			$mailData						= array();
			$mailData['HBL_HAWB'] 			= $latestShipment['hbl_hawb'];
			$mailData['CONTAINER_NUMBER'] 	= $latestShipment['container_number'];

			$data 				= array();
			$data['subject'] 	= replacePattern( $this->lang->line( 'clp.confirm.mail.subject' ), $mailData );
			$data['body'] 		= replacePattern( $this->lang->line( 'clp.confirm.mail.body' ), $mailData );
			$data['recipient'] 	= $looContactMail;
			if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
			$logData['mail_type'] = 'CLP confirmed mail';
		}

		// submit clp change mail
		if( $previousShipment['approval'] == '1' && $latestShipment['approval'] == '0' ) {
			$mailData						= array();
			$mailData['HBL_HAWB'] 			= $latestShipment['hbl_hawb'];
			$mailData['CONTAINER_NUMBER'] 	= $latestShipment['container_number'];

			$data 				= array();
			$data['subject'] 	= replacePattern( $this->lang->line( 'clp.change.mail.subject' ), $mailData );
			$data['body'] 		= replacePattern( $this->lang->line( 'clp.change.mail.body' ), $mailData );
			$data['recipient'] 	= $looContactMail;
			if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
			$logData['mail_type'] = 'CLP changed mail';
		}

		// submit clp change mail (to ALDI)
		if( $previousShipment['approval'] == '1' && $latestShipment['approval'] == '0' ) {
			$this->load->model('Pic_model', 'pic_model');

			$mailData						= array();
			$mailData['ADVERTISEMENT_WEEK'] = $this->model->getAdvertisementWeek( $latestShipment['id'] );
			$mailData['CONTAINER_NUMBER'] 	= $latestShipment['container_number'];
			$mailData['CONTRACT_NUMBER'] 	= $latestShipment['contract_numbers'];

			$data 				= array();
			$data['subject'] 	= replacePattern( $this->lang->line( 'clp.change2.mail.subject' ), $mailData );
			$data['body'] 		= replacePattern( $this->lang->line( 'clp.change2.mail.body' ), $mailData );
			$data['recipient'] 	= $this->pic_model->getMailByOnSaleWeek( $mailData['ADVERTISEMENT_WEEK'] );
			if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
			$logData['mail_type'] = 'CLP changed mail (to ALDI)';
		}

		// submit clp deny mail
		if( $previousShipment['approval'] == '2' && $latestShipment['approval'] == '0' ) {
			$mailData						= array();
			$mailData['HBL_HAWB'] 			= $latestShipment['hbl_hawb'];
			$mailData['CONTAINER_NUMBER'] 	= $latestShipment['container_number'];

			$data 				= array();
			$data['subject'] 	= replacePattern( $this->lang->line( 'clp.deny.mail.subject' ), $mailData );
			$data['body'] 		= replacePattern( $this->lang->line( 'clp.deny.mail.body' ), $mailData );
			$data['recipient'] 	= $looContactMail;
			if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
			$logData['mail_type'] = 'CLP denied mail';
		}

		// log data
		$logData['id'] 			= $latestShipment['id'];
		$logData['subject'] 	= isset( $data['subject'] ) ? $data['subject'] : '';
		$logData['recipient'] 	= isset( $data['recipient'] ) ? $data['recipient'] : '';
		$this->activity_model->logChanges( $logData, array(), $latestShipment['hbl_hawb'], Activity_model::$DATA_TYPE_SHIPMENT );
	}

	/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$cc 		= isset( $data['cc'] ) ? $data['cc'] : '';
		$subject 	= $data['subject'];
		$body 		= $data['body'];

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		if( !empty( $cc ) ) $this->email->cc( $cc );
		$this->email->subject( $subject );
		$this->email->message( $body );

		$sendSuccess = $this->email->send();
		if( !$sendSuccess ) {
			$this->welogger->log( "Sending Mail to ".$toMail." failed, error: ".$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, "Shipment.sendMail" );
			return false;
		} else {
			$this->welogger->log( "Mail was sent to ".$toMail." \nwith subject: ".$subject." \nand message: ".$body, WELogger::$LOG_LEVEL_INFO, "Shipment.sendMail" );
			return true;
		}
		$this->email->clear(TRUE);

	}
}
/* End of file shipment_unconfirmed.php */
/* Location: ./app/controllers/shipment_unconfirmed.php */