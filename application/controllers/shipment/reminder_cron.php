<?php
class Reminder_cron extends CI_Controller {

	public static $OFFICE_REMINDER_MAIL_5DAYS 		= 1;
	public static $SUPPLIER_REMINDER_MAIL_5DAYS 	= 2;
	public static $SUPPLIER_REMINDER_MAIL_2DAYS 	= 3;

	function __construct() {
		parent::__construct();
		$this->load->model('Shipment_model', 'model');
		$this->load->model('Office_model', 'office_model');
		$this->load->model('Supplier_model', 'supplier_model');
		$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Activity_model', 'activity_model');
		$this->tablename = 'shipments';
		//$this->output->enable_profiler(TRUE);
	}

	public function init() {

		// get shipments and send mail to office (5 days before ETA)
		$shipments = $this->getShipmentRecordsByOffset( '+5 days' );
		$this->checkIfTelexReceivedIsSet( $shipments, Reminder_cron::$OFFICE_REMINDER_MAIL_5DAYS );

		// get shipments and send mail to supplier (5 days before ETA)
		$shipments = $this->getShipmentRecordsByOffset( '+5 days' );
		$this->checkIfTelexReceivedIsSet( $shipments, Reminder_cron::$SUPPLIER_REMINDER_MAIL_5DAYS );

		// get shipments and send mail to supplier (2 days before ETA)
		$shipments = $this->getShipmentRecordsByOffset( '+2 days' );
		$this->checkIfTelexReceivedIsSet( $shipments, Reminder_cron::$SUPPLIER_REMINDER_MAIL_2DAYS );

	}

	/**
	 * getShipmentRecordsByOffset
	 * @return
	 */
	private function getShipmentRecordsByOffset( $offset ) {

		// get shipments and send mail to office
		$now 	= strtotime( $offset, time() );
		$start 	= date( 'Y-m-d', $now ).' 00:00:00';
		$stop 	= date( 'Y-m-d', $now ).' 23:59:59';

		$shipments = $this->model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0, 'eta >=' => $start, 'eta <=' => $stop ) );
		return $shipments;
	}

	/**
	 * checkIfTelexReceivedIsSet
	 * @return
	 */
	private function checkIfTelexReceivedIsSet( $shipments, $type ) {
		$tmp = array();
		foreach( $shipments as $shipment ) {
			$records = $this->shipment_delivery_model->getRecordsWhere( array( 'shipment_id' => $shipment['id'], 'hidden' => 0, 'deleted' => 0 ) );
			foreach( $records as $record ) {
				if( $record['telex_received'] == 0 ) {
					if( !in_array( $record, $tmp ) ) $tmp[] = $record;
				}
			}
		}

		foreach( $tmp as $item ) {
			$deliveries = $this->delivery_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0, 'id' => $item['delivery_id'] ) );
			$shipment = $this->getShipment( $item['shipment_id'] );
			foreach( $deliveries as $delivery ) {
				$contracts 	= $this->contract_model->getRecordsWhere( array( 'hidden' => 0, 'deleted' => 0, 'id' => $delivery['contract_id'] ) );
				$contract 	= $contracts[0];
				$data 		= array(
									'shipment_id'			=> $shipment['id'],
									'container_number'		=> $shipment['container_number'],
									'logwin_origin_office' 	=> $contract['logwin_origin_office'],
									'supplier' 				=> $contract['supplier'],
									'contract_number' 		=> $contract['contract_number'],
									'product_code' 			=> $delivery['product_code'],
									'advertisement_week' 	=> $contract['advertisement_week'],
									'hbl_hawb'				=> $item['hbl_hawb'],
									'destination_port'		=> $shipment['destination_port'],
									'eta'					=> $shipment['eta'],
								);
				$this->generateMail( $data, $type );

				// save second reminder delivery date
				if( $type == Reminder_cron::$SUPPLIER_REMINDER_MAIL_2DAYS ) {
					$this->shipment_delivery_model->updateRecordMM( $shipment['id'], $delivery['id'], array( 'telex_second_reminder_sent' => date('Y-m-d H:i:s') ) );
				}

				// save first reminder delivery date
				if( $type == Reminder_cron::$SUPPLIER_REMINDER_MAIL_5DAYS ) {
					$this->shipment_delivery_model->updateRecordMM( $shipment['id'], $delivery['id'], array( 'telex_first_reminder_sent' => date('Y-m-d H:i:s') ) );
				}
			}
		}
	}

	/**
	 * generateMail
	 * @return
	 */
	private function generateMail( $data, $type = 0 ) {

		$this->load->helper( 'str_helper' );
		$office 						= $this->office_model->getRecord( $data['logwin_origin_office'] );
		$supplier						= $this->supplier_model->getRecord( $data['supplier'] );
		$containerNumber				= $data['container_number'];
		$eta							= $data['eta'];
		$shipmentId						= $data['shipment_id'];
		$logData						= array();
		$mailData						= array();
		$mailData['CONTRACT_NUMBER'] 	= $data['contract_number'];
		$mailData['PRODUCT_CODE'] 		= $data['product_code'];
		$mailData['ADVERTISEMENT_WEEK'] = $data['advertisement_week'];
		$mailData['HBL_HAWB'] 			= $data['hbl_hawb'];
		$mailData['DESTINATION_PORT'] 	= $data['destination_port'];
		$mailData['ETA'] 				= $data['eta'];

		switch( $type ) {
			case Reminder_cron::$OFFICE_REMINDER_MAIL_5DAYS:

				$cc					= '';
				$data 				= array();
				$data['subject'] 	= replacePattern( $this->lang->line( 'office.reminder.mail.subject' ), $mailData );
				$data['body'] 		= replacePattern( $this->lang->line( 'office.reminder.mail.body' ), $mailData );;
				$data['recipient'] 	= $office['contact_mail'];
				if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
				$logData['mail_type'] 	= 'Telex release reminder (Office, 5 days before ETA)';
				break;

			case Reminder_cron::$SUPPLIER_REMINDER_MAIL_5DAYS:

				$cc					= ( ENVIRONMENT == 'production' ) ? $this->lang->line( 'supplier.reminder.mail.cc' ) : '';
				$data 				= array();
				$data['subject'] 	= replacePattern( $this->lang->line( 'supplier.reminder.mail.subject' ), $mailData );
				$data['body'] 		= replacePattern( $this->lang->line( 'supplier.reminder.mail.body' ), $mailData );;
				$data['recipient'] 	= $this->supplier_model->getContactMailsBySupplierId($supplier['id']);
				$data['cc'] 		= $cc;
				if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
				$logData['mail_type'] 	= 'Telex release reminder (Supplier, 5 days before ETA)';
				break;

			case Reminder_cron::$SUPPLIER_REMINDER_MAIL_2DAYS:

				$this->load->model('Pic_model', 'pic_model');
				$cc					= $this->pic_model->getMailByOnSaleWeek( $data['advertisement_week'] );
				$data 				= array();
				$data['subject'] 	= replacePattern( $this->lang->line( 'supplier.reminder2.mail.subject' ), $mailData );
				$data['body'] 		= replacePattern( $this->lang->line( 'supplier.reminder2.mail.body' ), $mailData );;
				$data['recipient'] 	= $this->supplier_model->getContactMailsBySupplierId($supplier['id']);
				$data['cc'] 		= ( !empty( $cc ) ) ? $cc : ( ( ENVIRONMENT == 'production' ) ? $this->lang->line( 'supplier.reminder2.mail.cc' ) : '' );
				if( !empty( $data['recipient'] ) ) $this->sendMail( $data );
				$logData['mail_type'] 	= 'Telex release alert  (Supplier and PIC, 2 days before ETA)';
				break;
		}

		// log data
		$logData['id'] 			= $shipmentId;
		$logData['subject'] 	= $data['subject'];
		$logData['recipient'] 	= $data['recipient'];
		if( isset( $data['cc'] ) ) $logData['cc'] = $data['cc'];

//		print('----------------------------------------------------------------'.'</br>');
//		print('New Mail, type: '.$logData['mail_type'].'</br>');
//		print('Shipment: '.$containerNumber.', ETA: '.$eta.', <a href="/shipment/edit/'.$logData['id'].'" target="_blank">Link</a></br>');
//		print('Subject: '.$logData['subject'].'</br>');
//		print('Recipient: '.$logData['recipient'].'</br>');
//		print('CC: '.$cc.'</br>');

		$this->activity_model->logChanges( $logData, array(), $containerNumber, Activity_model::$DATA_TYPE_SHIPMENT );
	}

	/**
	 * getShipment
	 * @return array
	 */
	private function getShipment( $id ) {
		$shipment = $this->model->getRecord( $id );

		$tcaNode = $this->tca->getColumnNodeById( 'shipments_destination_port' );
		$options = $this->tca->getMultiselectionOptions( $tcaNode );
		$shipment['destination_port'] 	= ( $shipment['destination_port'] != 0 && array_key_exists($shipment['destination_port'], $options) ) ? $options[$shipment['destination_port']] : '';
		$shipment['eta'] 				= ( $shipment['eta'] != "0000-00-00 00:00:00" ) ? date( $this->config->item('date_format'), strtotime( $shipment['eta'] ) ) : "";
		return $shipment;
	}

		/**
	 * sendMail
	 * @return boolean
	 */
	private function sendMail( $data ) {
		$this->load->library('email');
		$this->load->helper( 'str_helper' );

		// create mail data
		$fromMail 	= $this->config->item('from_mail');
		$fromName 	= $this->config->item('from_name' );
		$toMail 	= $data['recipient'];
		$cc			= ( isset( $data['cc'] ) ) ? $data['cc'] : '';
		$subject 	= $data['subject'];
		$body 		= $data['body'];

		$this->email->from( $fromMail, $fromName );
		$this->email->to( $toMail );
		$this->email->subject( $subject );
		$this->email->message( $body );
		if( !empty( $cc ) ) $this->email->cc( $cc );
		//$this->email->bcc( 'Benjamin.Haas@logwin-logistics.com' );

		$sendSuccess = ( ENVIRONMENT == 'production' ) ? $this->email->send() : TRUE;
		if( !$sendSuccess ) {
			$this->welogger->log( 'Sending Mail to '.$toMail.' failed, error: '.$this->email->print_debugger(), WELogger::$LOG_LEVEL_ERROR, 'Reminder_cron.sendMail' );
			return false;
		} else {
			$this->welogger->log( 'Mail was sent to '.$toMail.' \nwith subject:  '.$subject.' \nand message: '.$body, WELogger::$LOG_LEVEL_INFO, 'Reminder_cron.sendMail' );
			return true;
		}
		$this->email->clear(TRUE);
	}

}

/* End of file reminder_cron.php */
/* Location: ./app/controllers/reminder_cron.php */