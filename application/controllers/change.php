<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change extends CI_Controller {

    public $userData;

	public function __construct() {
		parent::__construct();
		$this->load->model('User_model', 'model');
		$this->load->helper( 'url' );
		//$this->output->enable_profiler(TRUE);
	}

    /**
     * change
     */
    public function index(){
        $data 				= array();
		$data['error'] 		= '';
		$data['message'] 	= '';
		$data['pwchange'] 	= FALSE;
		$post 				= $this->input->post();

        if( !empty( $post ) ) {

			$query = $this->db->query( "SELECT * FROM users WHERE username='".$this->db->escape_str( $post['username'] )."' AND password='".$this->db->escape_str( $post['oldpassword'] )."' AND email='".$this->db->escape_str( $post['email'] )."' AND hidden=0 AND deleted=0" );
			// check if user exists
			if( $query->num_rows() > 0 ) {
				$userData = $query->row_array();
				// check if passwords match
				if( $post['password'] == $post['password2'] ) {
					// check password history
					if( $this->checkPasswordHistory( $userData, $post ) == TRUE ){
						$this->saveNewPasswordData( $userData, $post );
						$data['message'] = 'You successfully changed your password! Please login with your new credentials';
						$data['pwchange'] = TRUE;
					}else{
						$data['error'] = TRUE;
						$data['message'] = 'For security reasons, you can´t use an old password again! You need to set a new one, which you have never been used';
					}
				}else{
					$data['error'] = TRUE;
					$data['message'] = 'Your new passwords do not match. Please type again!';
				}
			} else {
				$data['error'] = TRUE;
				$data['message'] = 'User does not exist!';
			}

        } else {
            $this->session->set_userdata( array( 'isLoggedIn' => FALSE ) );
        }

        $this->load->view( 'login/change', $data );
    }



    /**
     * @param $userData
     * @param $postData
     * @return bool
     * @throws Exception
     */
    public function saveNewPasswordData( $userData, $postData ){

		$oldPwds 			= json_decode( $userData['pw_history'] );
		$oldPwds[] 			= $postData['password2'];

        $data = array(
        	'password'      => $postData['password2'],
            'pw_set_date'   => date( 'Y-m-d H:i:s',time() ),
            'pw_history'    => json_encode( $oldPwds )
        );

		$this->model->updateRecord( $userData['id'], $data );
    }

    ////////////////////////////////////////////////////////////////////////////

    //		private

    ////////////////////////////////////////////////////////////////////////////

	/**
	 * checkPasswordHistory
	 *
	 * @param $post
	 * @param $userData
	 */
	private function checkPasswordHistory( $userData, $post ){
		$newPassword 	= $post['password2'];
		$oldHistory 	= json_decode( $userData['pw_history'] );

		if( !empty( $oldHistory ) && in_array( $newPassword, $oldHistory ) ) {
			return FALSE;
		}else{
			return TRUE;
		}
	}

}

/* End of file login.php */
/* Location: ./app/controllers/login.php */