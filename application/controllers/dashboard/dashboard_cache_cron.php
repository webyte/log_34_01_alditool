<?php
class Dashboard_cache_cron extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Dashboard_cfs_factory_model', 'dashboard_cfs_factory_model');
		$this->load->model('Dashboard_destination_management_model', 'dashboard_destination_management_model');
		$this->load->model('Dashboard_document_management_model', 'dashboard_document_management_model');
		$this->load->model('Dashboard_entire_transportation_model', 'dashboard_entire_transportation_model');
		$this->load->model('Dashboard_ocean_freight_model', 'dashboard_ocean_freight_model');
		$this->load->model('Dashboard_supplier_model', 'dashboard_supplier_model');
		//$this->output->enable_profiler(TRUE);
	}

	public function init() {
		$this->dashboard_cfs_factory_model->generateCache();
		$this->dashboard_destination_management_model->generateCache();
		$this->dashboard_document_management_model->generateCache();
		$this->dashboard_entire_transportation_model->generateCache();
		$this->dashboard_ocean_freight_model->generateCache();
		$this->dashboard_supplier_model->generateCache();
	}


}

/* End of file reminder_cron.php */
/* Location: ./app/controllers/dashboard_cron.php */