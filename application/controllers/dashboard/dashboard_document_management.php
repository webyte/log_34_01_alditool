<?php
require_once( APPPATH.'controllers/dashboard/abstract_dashboard.php' );

class Dashboard_document_management extends Abstract_dashboard {

	function __construct() {
		parent::__construct();
		$this->load->model('Dashboard_document_management_model', 'model');
		$this->headline		= 'Document management';
		$this->viewTemplate = 'dashboard/document_management';
		$this->exportFilename = 'logwin_document_destination_management.xlsx';
		$this->fields = array(
			array(
				'label' => 'Supplier',
				'fieldname' => 'contracts_supplier',
				'jsonname' => 'supplier',
				'export' => true,
			),
			array(
				'label' => 'Contract No',
				'fieldname' => 'contracts_contract_number',
				'jsonname' => 'contract_number',
				'export' => true,
			),
			array(
				'label' => 'Product Code',
				'fieldname' => 'contracts_product_code',
				'jsonname' => 'product_code',
				'export' => true,
			),
			array(
				'label' => 'Sales Week',
				'fieldname' => 'contracts_advertisement_week',
				'jsonname' => 'sales_week',
				'export' => true,
			),
			array(
				'label' => 'HBL Number',
				'fieldname' => 'shipments_deliveries_mm_hbl_hawb',
				'jsonname' => 'hbl_hawb',
				'export' => true,
			),
			array(
				'label' => 'Container No',
				'fieldname' => 'shipments_container_number',
				'jsonname' => 'container_number',
				'export' => true,
			),
			array(
				'label' => 'DC',
				'fieldname' => 'deliveries_dc',
				'jsonname' => 'dc',
				'export' => true,
			),
			array(
				'label' => 'Destination',
				'fieldname' => 'shipments_destination_port',
				'jsonname' => 'destination_port',
				'export' => true,
			),
			array(
				'label' => 'ETA',
				'fieldname' => 'shipments_eta',
				'jsonname' => 'eta',
				'export' => true,
			),
			array(
				'label' => 'ETA',
				'fieldname' => 'shipments_eta_timestamp',
				'jsonname' => 'eta_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Commercial Invoice',
				'fieldname' => 'commercial_invoice',
				'jsonname' => 'commercial_invoice',
				'export' => true,
			),
			array(
				'label' => 'Commercial Invoice',
				'fieldname' => 'commercial_invoice_css',
				'jsonname' => 'commercial_invoice_css',
				'export' => false,
			),
			array(
				'label' => 'Packing List',
				'fieldname' => 'packing_list',
				'jsonname' => 'packing_list',
				'export' => true,
			),
			array(
				'label' => 'Packing List',
				'fieldname' => 'packing_list_css',
				'jsonname' => 'packing_list_css',
				'export' => false,
			),
			array(
				'label' => 'Certificate of Origin',
				'fieldname' => 'coo',
				'jsonname' => 'coo',
				'export' => true,
			),
			array(
				'label' => 'Certificate of Origin',
				'fieldname' => 'coo_css',
				'jsonname' => 'coo_css',
				'export' => false,
			),
			array(
				'label' => 'Packing Declaration',
				'fieldname' => 'packing_declaration',
				'jsonname' => 'packing_declaration',
				'export' => true,
			),
			array(
				'label' => 'Packing Declaration',
				'fieldname' => 'packing_declaration_css',
				'jsonname' => 'packing_declaration_css',
				'export' => false,
			),
			array(
				'label' => 'Telex Release',
				'fieldname' => 'telex_release',
				'jsonname' => 'telex_release',
				'export' => true,
			),
			array(
				'label' => 'Telex Release',
				'fieldname' => 'telex_release_css',
				'jsonname' => 'telex_release_css',
				'export' => false,
			),
			array(
				'label' => 'e-Delivery Order',
				'fieldname' => 'edo_sent',
				'jsonname' => 'edo_sent',
				'export' => true,
			),
			array(
				'label' => 'e-Delivery Order',
				'fieldname' => 'edo_sent_css',
				'jsonname' => 'edo_sent_css',
				'export' => false,
			),
		);

		//$this->output->enable_profiler(TRUE);
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

}

