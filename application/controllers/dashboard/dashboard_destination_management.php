<?php
require_once( APPPATH.'controllers/dashboard/abstract_dashboard.php' );

class Dashboard_destination_management extends Abstract_dashboard {

	function __construct() {
		parent::__construct();
		$this->load->model('Dashboard_destination_management_model', 'model');
		$this->headline		= 'Destination management';
		$this->viewTemplate = 'dashboard/destination_management';
		$this->exportFilename = 'logwin_dashboard_destination_management.xlsx';
		$this->fields = array(
			array(
				'label' => 'Contract No',
				'fieldname' => 'contracts_contract_number',
				'jsonname' => 'contract_number',
				'export' => true,
			),
			array(
				'label' => 'Sales Week',
				'fieldname' => 'contracts_advertisement_week',
				'jsonname' => 'sales_week',
				'export' => true,
			),
			array(
				'label' => 'Traffic Type',
				'fieldname' => 'contracts_traffic_type',
				'jsonname' => 'traffic_type',
				'export' => true,
			),
			array(
				'label' => 'Destination',
				'fieldname' => 'shipments_destination_port',
				'jsonname' => 'destination_port',
				'export' => true,
			),
			array(
				'label' => 'Container No',
				'fieldname' => 'shipments_container_number',
				'jsonname' => 'container_number',
				'export' => true,
			),
			array(
				'label' => 'ETA',
				'fieldname' => 'shipments_eta',
				'jsonname' => 'eta',
				'export' => true,
			),
			array(
				'label' => 'ETA',
				'fieldname' => 'shipments_eta_timestamp',
				'jsonname' => 'eta_timestamp',
				'export' => false,
			),
			array(
				'label' => 'ATA',
				'fieldname' => 'shipments_ata',
				'jsonname' => 'ata',
				'export' => true,
			),
			array(
				'label' => 'ATA',
				'fieldname' => 'shipments_ata_timestamp',
				'jsonname' => 'ata_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Customs Clearance',
				'fieldname' => 'shipments_cus',
				'jsonname' => 'customs_clearance',
				'export' => true,
			),
			array(
				'label' => 'Customs Clearance',
				'fieldname' => 'shipments_cus_timestamp',
				'jsonname' => 'customs_clearance_timestamp',
				'export' => false,
			),
			array(
				'label' => 'E-Delivery Order',
				'fieldname' => 'shipments_edo_sent_click_date',
				'jsonname' => 'edo',
				'export' => true,
			),
			array(
				'label' => 'E-Delivery Order',
				'fieldname' => 'shipments_edo_sent_click_date_timestamp',
				'jsonname' => 'edo_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Delivery Window Start',
				'fieldname' => 'contracts_aldi_au_wh_delivery_due_date',
				'jsonname' => 'delivery_window_start',
				'export' => true,
			),
			array(
				'label' => 'Delivery Window Start',
				'fieldname' => 'contracts_aldi_au_wh_delivery_due_date_timestamp',
				'jsonname' => 'delivery_window_start_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Delivery Window End',
				'fieldname' => 'contracts_aldi_au_wh_delivery_due_end_date',
				'jsonname' => 'delivery_window_end',
				'export' => true,
			),
			array(
				'label' => 'Delivery Window End',
				'fieldname' => 'contracts_aldi_au_wh_delivery_due_end_date_timestamp',
				'jsonname' => 'delivery_window_end_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Status Delivery',
				'fieldname' => 'status_delivery',
				'jsonname' => 'status_delivery',
				'export' => true,
			),
			array(
				'label' => 'Status Delivery',
				'fieldname' => 'status_delivery_css',
				'jsonname' => 'status_delivery_css',
				'export' => false,
			),
			array(
				'label' => 'Pick Up at Wharf',
				'fieldname' => 'shipments_pick_up_at_wharf',
				'jsonname' => 'pick_up_at_wharf',
				'export' => true,
			),
			array(
				'label' => 'Pick Up at Wharf',
				'fieldname' => 'shipments_pick_up_at_wharf_timestamp',
				'jsonname' => 'pick_up_at_wharf_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Delivered to DC',
				'fieldname' => 'shipments_delivered_to_dc',
				'jsonname' => 'delivered_to_dc',
				'export' => true,
			),
			array(
				'label' => 'Delivered to DC',
				'fieldname' => 'shipments_delivered_to_dc_timestamp',
				'jsonname' => 'delivered_to_dc_timestamp',
				'export' => false,
			),
		);

		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * getRecordsByStatusDelivery
	 *
	 */
	public function getRecordsByStatusDelivery() {
		$rows = $this->model->getRecordsByStatusDelivery();
		header('Content-Type: application/json');
		print( json_encode( $rows ) );
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

}

