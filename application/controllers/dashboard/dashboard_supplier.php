<?php
require_once( APPPATH.'controllers/dashboard/abstract_dashboard.php' );

class Dashboard_supplier extends Abstract_dashboard {

	function __construct() {
		parent::__construct();
		$this->load->model('Dashboard_supplier_model', 'model');
		$this->headline		= 'Supplier';
		$this->viewTemplate = 'dashboard/supplier';
		$this->exportFilename = 'logwin_dashboard_supplier.xlsx';
		$this->fields = array(
			array(
				'label' => 'Contract No',
				'fieldname' => 'contracts_contract_number',
				'jsonname' => 'contract_number',
				'export' => true,
			),
			array(
				'label' => 'Supplier',
				'fieldname' => 'contracts_supplier',
				'jsonname' => 'supplier',
				'export' => true,
			),
			array(
				'label' => 'Sales Week',
				'fieldname' => 'contracts_advertisement_week',
				'jsonname' => 'sales_week',
				'export' => true,
			),
			array(
				'label' => 'Traffic Type',
				'fieldname' => 'contracts_traffic_type',
				'jsonname' => 'traffic_type',
				'export' => true,
			),
			array(
				'label' => 'Departure',
				'fieldname' => 'contracts_departure',
				'jsonname' => 'departure',
				'export' => true,
			),
			array(
				'label' => 'Booking Received Date',
				'fieldname' => 'contracts_ofu_real',
				'jsonname' => 'ofu_real',
				'export' => true,
			),
			array(
				'label' => 'Booking Received Date',
				'fieldname' => 'contracts_ofu_real_timestamp',
				'jsonname' => 'ofu_real_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Requested Contract Ready Date',
				'fieldname' => 'contracts_fob_date',
				'jsonname' => 'fob_date',
				'export' => true,
			),
			array(
				'label' => 'Requested Contract Ready Date',
				'fieldname' => 'contracts_fob_date_timestamp',
				'jsonname' => 'fob_date_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Actual Contract Ready Date',
				'fieldname' => 'contracts_actual_contract_ready_date',
				'jsonname' => 'actual_contract_ready_date',
				'export' => true,
			),
			array(
				'label' => 'Actual Contract Ready Date',
				'fieldname' => 'contracts_actual_contract_ready_date_timestamp',
				'jsonname' => 'actual_contract_ready_date_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Delay Reason',
				'fieldname' => 'contracts_delay_reason',
				'jsonname' => 'delay_reason',
				'export' => true,
			),
			array(
				'label' => 'Discrepancy',
				'fieldname' => '',
				'jsonname' => 'discrepancy',
				'export' => false,
			),
			array(
				'label' => 'Status',
				'fieldname' => 'status_contract',
				'jsonname' => 'status_contract',
				'export' => true,
			),
			array(
				'label' => 'Status',
				'fieldname' => 'status_contract_css',
				'jsonname' => 'status_contract_css',
				'export' => false,
			),
			array(
				'label' => '',
				'fieldname' => 'comments_count',
				'jsonname' => 'comments_count',
				'export' => false,
			),
			array(
				'label' => '',
				'fieldname' => 'contracts_id',
				'jsonname' => 'contracts_id',
				'export' => false,
			),
			array(
				'label' => 'Discrepancy',
				'fieldname' => 'discrepancy',
				'jsonname' => 'discrepancy',
				'export' => true,
			),
		);


		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * getDelayedRecordsBySupplier
	 *
	 */
	public function getDelayedRecordsBySupplier() {
		$rows = $this->model->getDelayedRecordsBySupplier();
		header('Content-Type: application/json');
		print( json_encode( $rows ) );
	}

	/**
	 * getRecordsByStatusContract
	 *
	 */
	public function getRecordsByStatusContract() {
		$rows = $this->model->getRecordsByStatusContract();
		header('Content-Type: application/json');
		print( json_encode( $rows ) );
	}


	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

}

