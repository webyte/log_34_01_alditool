<?php
require_once( APPPATH.'controllers/dashboard/abstract_dashboard.php' );

class Dashboard_entire_transportation extends Abstract_dashboard {

	function __construct() {
		parent::__construct();
		$this->load->model('Dashboard_entire_transportation_model', 'model');
		$this->headline		= 'Entire Transportation';
		$this->viewTemplate = 'dashboard/entire_transportation';
		$this->exportFilename = 'logwin_dashboard_entire_transportation.xlsx';
		$this->fields = array(
			array(
				'label' => 'Contract No',
				'fieldname' => 'contracts_contract_number',
				'jsonname' => 'contract_number',
				'export' => true,
			),
			array(
				'label' => 'Sales Week',
				'fieldname' => 'contracts_advertisement_week',
				'jsonname' => 'sales_week',
				'export' => true,
			),
			array(
				'label' => 'Traffic Type',
				'fieldname' => 'contracts_traffic_type',
				'jsonname' => 'traffic_type',
				'export' => true,
			),
			array(
				'label' => 'Departure',
				'fieldname' => 'shipments_departure_port',
				'jsonname' => 'departure_port',
				'export' => true,
			),
			array(
				'label' => 'DC',
				'fieldname' => 'deliveries_dc',
				'jsonname' => 'dc',
				'export' => true,
			),
			array(
				'label' => 'Booking Received Date',
				'fieldname' => 'contracts_ofu_real',
				'jsonname' => 'ofu_real',
				'export' => true,
			),
			array(
				'label' => 'Booking Received Date',
				'fieldname' => 'contracts_ofu_real_timestamp',
				'jsonname' => 'ofu_real_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Actual Contract Ready Date',
				'fieldname' => 'contracts_actual_contract_ready_date',
				'jsonname' => 'actual_contract_ready_date',
				'export' => true,
			),
			array(
				'label' => 'Actual Contract Ready Date',
				'fieldname' => 'contracts_actual_contract_ready_date_timestamp',
				'jsonname' => 'actual_contract_ready_date_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Received in CFS',
				'fieldname' => 'contracts_received_in_cfs',
				'jsonname' => 'received_in_cfs',
				'export' => true,
			),
			array(
				'label' => 'Received in CFS',
				'fieldname' => 'contracts_received_in_cfs_timestamp',
				'jsonname' => 'received_in_cfs_timestamp',
				'export' => false,
			),
			array(
				'label' => 'CY Gate In',
				'fieldname' => 'shipments_container_gate_in',
				'jsonname' => 'container_gate_in',
				'export' => true,
			),
			array(
				'label' => 'CY Gate In',
				'fieldname' => 'shipments_container_gate_in_timestamp',
				'jsonname' => 'container_gate_in_timestamp',
				'export' => false,
			),
			array(
				'label' => 'ETD',
				'fieldname' => 'shipments_etd',
				'jsonname' => 'etd',
				'export' => true,
			),
			array(
				'label' => 'ETD',
				'fieldname' => 'shipments_etd_timestamp',
				'jsonname' => 'etd_timestamp',
				'export' => false,
			),
			array(
				'label' => 'ETA',
				'fieldname' => 'shipments_eta',
				'jsonname' => 'eta',
				'export' => true,
			),
			array(
				'label' => 'ETA',
				'fieldname' => 'shipments_eta_timestamp',
				'jsonname' => 'eta_timestamp',
				'export' => false,
			),
			array(
				'label' => 'ATD',
				'fieldname' => 'shipments_atd',
				'jsonname' => 'atd',
				'export' => true,
			),
			array(
				'label' => 'ATD',
				'fieldname' => 'shipments_atd_timestamp',
				'jsonname' => 'atd_timestamp',
				'export' => false,
			),
			array(
				'label' => 'ATA',
				'fieldname' => 'shipments_ata',
				'jsonname' => 'ata',
				'export' => true,
			),
			array(
				'label' => 'ATA',
				'fieldname' => 'shipments_ata_timestamp',
				'jsonname' => 'ata_timestamp',
				'export' => false,
			),
			array(
				'label' => 'CUS',
				'fieldname' => 'shipments_cus',
				'jsonname' => 'cus',
				'export' => true,
			),
			array(
				'label' => 'CUS',
				'fieldname' => 'shipments_cus_timestamp',
				'jsonname' => 'cus_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Delivery Window Start',
				'fieldname' => 'contracts_aldi_au_wh_delivery_due_date',
				'jsonname' => 'delivery_window_start',
				'export' => true,
			),
			array(
				'label' => 'Delivery Window Start',
				'fieldname' => 'contracts_aldi_au_wh_delivery_due_date_timestamp',
				'jsonname' => 'delivery_window_start_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Delivery Window End',
				'fieldname' => 'contracts_aldi_au_wh_delivery_due_end_date',
				'jsonname' => 'delivery_window_end',
				'export' => true,
			),
			array(
				'label' => 'Delivery Window End',
				'fieldname' => 'contracts_aldi_au_wh_delivery_due_end_date_timestamp',
				'jsonname' => 'delivery_window_end_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Status Delivery',
				'fieldname' => 'status_delivery',
				'jsonname' => 'status_delivery',
				'export' => true,
			),
			array(
				'label' => 'Status Delivery',
				'fieldname' => 'status_delivery_css',
				'jsonname' => 'status_delivery_css',
				'export' => false,
			),
		);

		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * getDelayedRecordsBySupplier
	 *
	 */
	public function getDelayedRecordsBySupplier() {
		$rows = $this->model->getDelayedRecordsBySupplier();
		header('Content-Type: application/json');
		print( json_encode( $rows ) );
	}

	/**
	 * getRecordsByStatusDelivery
	 *
	 */
	public function getRecordsByStatusDelivery() {
		$rows = $this->model->getRecordsByStatusDelivery();
		header('Content-Type: application/json');
		print( json_encode( $rows ) );
	}


	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

}

