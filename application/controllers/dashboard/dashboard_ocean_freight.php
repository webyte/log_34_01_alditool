<?php
require_once( APPPATH.'controllers/dashboard/abstract_dashboard.php' );

class Dashboard_ocean_freight extends Abstract_dashboard {

	function __construct() {
		parent::__construct();
		$this->load->model('Dashboard_ocean_freight_model', 'model');
		$this->headline		= 'Ocean Freight';
		$this->viewTemplate = 'dashboard/ocean_freight';
		$this->exportFilename = 'logwin_dashboard_ocean_freight.xlsx';
		$this->fields = array(
			array(
				'label' => 'Contract No',
				'fieldname' => 'contracts_contract_number',
				'jsonname' => 'contract_number',
				'export' => true,
			),
			array(
				'label' => 'Sales Week',
				'fieldname' => 'contracts_advertisement_week',
				'jsonname' => 'sales_week',
				'export' => true,
			),
			array(
				'label' => 'Carrier',
				'fieldname' => 'shipments_carrier',
				'jsonname' => 'carrier',
				'export' => true,
			),
			array(
				'label' => 'Vessel Name',
				'fieldname' => 'shipments_mother_vessel_name',
				'jsonname' => 'vessel_name',
				'export' => true,
			),
			array(
				'label' => 'Voyage Number',
				'fieldname' => 'shipments_voyage_number',
				'jsonname' => 'voyage_number',
				'export' => true,
			),
			array(
				'label' => 'Container No',
				'fieldname' => 'shipments_container_number',
				'jsonname' => 'container_number',
				'export' => true,
			),
			array(
				'label' => 'Departure',
				'fieldname' => 'shipments_departure_port',
				'jsonname' => 'departure_port',
				'export' => true,
			),
			array(
				'label' => 'Destination',
				'fieldname' => 'shipments_destination_port',
				'jsonname' => 'destination_port',
				'export' => true,
			),
			array(
				'label' => 'ETD',
				'fieldname' => 'shipments_etd',
				'jsonname' => 'etd',
				'export' => true,
			),
			array(
				'label' => 'ETD',
				'fieldname' => 'shipments_etd_timestamp',
				'jsonname' => 'etd_timestamp',
				'export' => false,
			),
			array(
				'label' => 'ETA',
				'fieldname' => 'shipments_eta',
				'jsonname' => 'eta',
				'export' => true,
			),
			array(
				'label' => 'ETA',
				'fieldname' => 'shipments_eta_timestamp',
				'jsonname' => 'eta_timestamp',
				'export' => false,
			),
			array(
				'label' => 'ATD',
				'fieldname' => 'shipments_atd',
				'jsonname' => 'atd',
				'export' => true,
			),
			array(
				'label' => 'ATD',
				'fieldname' => 'shipments_atd_timestamp',
				'jsonname' => 'atd_timestamp',
				'export' => false,
			),
			array(
				'label' => 'ATA',
				'fieldname' => 'shipments_ata',
				'jsonname' => 'ata',
				'export' => true,
			),
			array(
				'label' => 'ATA',
				'fieldname' => 'shipments_ata_timestamp',
				'jsonname' => 'ata_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Status Departure',
				'fieldname' => 'status_departure',
				'jsonname' => 'status_departure',
				'export' => true,
			),
			array(
				'label' => 'Status Departure',
				'fieldname' => 'status_departure_css',
				'jsonname' => 'status_departure_css',
				'export' => false,
			),
			array(
				'label' => 'Status Arrival',
				'fieldname' => 'status_arrival',
				'jsonname' => 'status_arrival',
				'export' => true,
			),
			array(
				'label' => 'Status Arrival',
				'fieldname' => 'status_arrival_css',
				'jsonname' => 'status_arrival_css',
				'export' => false,
			),
			array(
				'label' => 'Discrepancy',
				'fieldname' => 'comments_count',
				'jsonname' => 'comments_count',
				'export' => false,
			),
			array(
				'label' => 'Discrepancy',
				'fieldname' => 'contracts_id',
				'jsonname' => 'contracts_id',
				'export' => false,
			),
			array(
				'label' => 'Discrepancy',
				'fieldname' => 'discrepancy',
				'jsonname' => 'discrepancy',
				'export' => true,
			),
		);

		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * getDeparturePieData
	 *
	 */
	public function getDeparturePieData() {
		$rows = $this->model->getDeparturePieData();
		header('Content-Type: application/json');
		print( json_encode( $rows ) );
	}

	/**
	 * getArrivalPieData
	 *
	 */
	public function getArrivalPieData() {
		$rows = $this->model->getArrivalPieData();
		header('Content-Type: application/json');
		print( json_encode( $rows ) );
	}


	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

}

