<?php
require_once( APPPATH.'controllers/dashboard/abstract_dashboard.php' );

class Dashboard_cfs_factory extends Abstract_dashboard {

	function __construct() {
		parent::__construct();
		$this->load->model('Dashboard_cfs_factory_model', 'model');
		$this->headline		= 'CFS - Factory';
		$this->viewTemplate = 'dashboard/cfs_factory';
		$this->exportFilename = 'logwin_dashboard_cfs_factory.xlsx';
		$this->fields = array(
			array(
				'label' => 'Contract No',
				'fieldname' => 'contracts_contract_number',
				'jsonname' => 'contract_number',
				'export' => true,
			),
			array(
				'label' => 'Sales Week',
				'fieldname' => 'contracts_advertisement_week',
				'jsonname' => 'sales_week',
				'export' => true,
			),
			array(
				'label' => 'Traffic Type',
				'fieldname' => 'contracts_traffic_type',
				'jsonname' => 'traffic_type',
				'export' => true,
			),
			array(
				'label' => 'Departure',
				'fieldname' => 'shipments_departure_port',
				'jsonname' => 'departure_port',
				'export' => true,
			),
			array(
				'label' => 'DC',
				'fieldname' => 'deliveries_dc',
				'jsonname' => 'dc',
				'export' => true,
			),
			array(
				'label' => 'CBM',
				'fieldname' => 'deliveries_volume',
				'jsonname' => 'cbm',
				'export' => true,
			),
			array(
				'label' => 'Number of Carton',
				'fieldname' => 'deliveries_case_quantity',
				'jsonname' => 'number_of_carton',
				'export' => true,
			),
			array(
				'label' => 'Received in CFS',
				'fieldname' => 'contracts_received_in_cfs',
				'jsonname' => 'received_in_cfs',
				'export' => true,
			),
			array(
				'label' => 'Received in CFS',
				'fieldname' => 'contracts_received_in_cfs_timestamp',
				'jsonname' => 'received_in_cfs_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Intended CFS Cut Off',
				'fieldname' => 'shipments_intended_cfs_cut_off',
				'jsonname' => 'intended_cfs_cut_off',
				'export' => true,
			),
			array(
				'label' => 'Intended CFS Cut Off',
				'fieldname' => 'shipments_intended_cfs_cut_off_timestamp',
				'jsonname' => 'intended_cfs_cut_off_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Container Loaded',
				'fieldname' => 'shipments_container_loaded',
				'jsonname' => 'container_loaded',
				'export' => true,
			),
			array(
				'label' => 'Container Loaded',
				'fieldname' => 'shipments_container_loaded_timestamp',
				'jsonname' => 'container_loaded_timestamp',
				'export' => false,
			),
			array(
				'label' => 'CLP Approved',
				'fieldname' => 'shipments_approval_date',
				'jsonname' => 'clp_approved',
				'export' => true,
			),
			array(
				'label' => 'CLP Approved',
				'fieldname' => 'shipments_approval_date_timestamp',
				'jsonname' => 'clp_approved_timestamp',
				'export' => false,
			),
			array(
				'label' => 'CY Gate In',
				'fieldname' => 'shipments_container_gate_in',
				'jsonname' => 'container_gate_in_at_terminal',
				'export' => true,
			),
			array(
				'label' => 'CY Gate In',
				'fieldname' => 'shipments_container_gate_in_timestamp',
				'jsonname' => 'container_gate_in_at_terminal_timestamp',
				'export' => false,
			),
			array(
				'label' => 'Export Customs Cleared',
				'fieldname' => 'shipments_export_customs_cleared',
				'jsonname' => 'export_customs_cleared',
				'export' => true,
			),
			array(
				'label' => 'Export Customs Cleared',
				'fieldname' => 'shipments_export_customs_cleared_timestamp',
				'jsonname' => 'export_customs_cleared_timestamp',
				'export' => false,
			),
		);

		//$this->output->enable_profiler(TRUE);
	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

}

