<?php
require_once( APPPATH.'controllers/abstract_controller.php' );

class Abstract_dashboard extends Abstract_controller {

	public $headline;
	public $viewTemplate;
	public $fields;
	public $exportFilename;

	function __construct() {
		parent::__construct();
		$this->load->model('Discrepancy_model', 'discrepancy_model');
		$this->headline		= '';
		$this->viewTemplate = '';
		$this->fields = '';
		$this->exportFilename = 'logwin_dashboard.xlsx';
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * index
	 *
	 */
	public function index() {
		$data['headline'] 		= $this->headline;
		$data['is_cached'] 		= $this->model->isCached();
		$this->load->view($this->viewTemplate, $data );
	}

	/**
	 * getRecords
	 *
	 */
	public function getRecords() {
		header('Content-Type: application/json');
		print( json_encode( $this->getOutputData() ) );
	}

	/**
	 * getComments
	 *
	 */
	public function getComments() {
		$id 		= $this->input->post('id');
		$comments 	= $this->discrepancy_model->getComments( $id );
		header('Content-Type: application/json');
		print( json_encode( $comments ) );
	}

	/**
	 * addComment
	 *
	 */
	public function addComment() {
		$id 		= $this->input->post('id');
		$msg 		= $this->input->post('msg');
		$this->discrepancy_model->createNew( array( 'pid' => $id, 'msg' => $msg ) );
		$comments 	= $this->discrepancy_model->getComments( $id );
		header('Content-Type: application/json');
		print( json_encode( $comments ) );
	}

	/**
	 * getOutputData
	 *
	 */
	public function getOutputData() {
		$out = array();
		$rows = $this->model->getRecords();
		foreach( $rows as $row ) {
			$tmp = array();

			foreach( $this->fields as $field ) {
				$tmp[$field['jsonname']] = !empty( $field['fieldname'] ) ? $row[$field['fieldname']] : '';
			}

			$out[] = $tmp;
		}
		return $out;
	}

	/**
	 * export
	 *
	 */
	public function export()
	{
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		// labels
		$colCounter = 0;
		foreach( $this->fields as $field ) {
			if( $field['export'] ) {
				$objWorksheet->setCellValueByColumnAndRow( $colCounter,1, $field['label'] );
				$colCounter++;
			}
		}

		// content
		$rows = $this->getOutputData();
		$rowCounter = 2;
		foreach ( $rows as $data ) {
			$colCounter = 0;
			foreach( $this->fields as $field ) {
				if( $field['export'] ) {
					$objWorksheet->setCellValueByColumnAndRow( $colCounter,$rowCounter, $data[$field['jsonname']] );
					$colCounter++;
				}
			}
			$rowCounter++;
		}

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel2007' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.$this->exportFilename );
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');

	}

	////////////////////////////////////////////////////////////////////////////

	//		PRIVATE

	////////////////////////////////////////////////////////////////////////////

}

