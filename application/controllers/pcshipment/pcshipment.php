<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Pcshipment extends Abstract_controller {


	function __construct() {
		parent::__construct();
		$this->load->model('Pcshipment_model', 'model');

		$this->tablename 			= 'pcshipments';
		$this->scriptPath 			= 'pcshipment/pcshipment/';
		$this->recordEditView 		= 'pcshipment/pcshipmentdata_edit';
		$this->headline				= 'Shipments';
		$this->infotext				= '';
		$this->showDefaultSearchForm = FALSE;
		$this->overviewFields 	= array(
			array('label' => 'HBL/HAWB', 			'fieldname' => 'hbl_hawb', 			'type' => ''),
			array('label' => 'Container number', 	'fieldname' => 'container_number', 	'type' => ''),
			array('label' => 'Contract/PO number', 	'fieldname' => 'po_number', 		'type' => ''),
			array('label' => 'Departure', 			'fieldname' => 'departure_port', 	'type' => ''),
			array('label' => 'Destination', 		'fieldname' => 'destination_port', 	'type' => ''),
		);

		//$this->output->enable_profiler(TRUE);
	}


	/**
	 * recordList
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']		= generateCustomSearchForm( $this->tablename, array(), 'pcshipment/pcshipment/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']	= '';
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['createnewlink'] 	= $this->acl->hasPermission( Acl::$crud_prefix.$this->tablename.'_new' ) ? $this->scriptPath.'edit' : '';
		$data['additionalJS'] 	= '';
		$this->load->view('general/record_overview', $data );
	}


	/**
	 * result
	 * default overview action. outputs generic recordoverview with headline, infotext and recordactions.
	 * also also creates default search form if $this->showDefaultSearchForm is true.
	 *
	 */
	public function result( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->recordListSearchParams['pcshipments_crdate'] = array( date( 'Y-m-d 00:00:00', strtotime( '-183 days', time() ) ) );
		$this->records					= $this->model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']		= generateCustomSearchForm( $this->tablename, array(), 'pcshipment/pcshipment/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']	= generateCustomOverview( $this->overviewFields, $this->records, $this->getRecordactions() );
		$data['headline'] 		= $this->headline;
		$data['infotext'] 		= $this->infotext;
		$data['createnewlink'] 	= $this->acl->hasPermission( Acl::$crud_prefix.$this->tablename.'_new' ) ? $this->scriptPath.'edit' : '';
		$data['additionalJS'] 	= '';
		$this->load->view('general/record_overview', $data );
	}

	/**
	 * edit
	 * default edit implementation.
	 *
	 * @param int $id - id of record to edit. If id is 0 a new record is created.
	 */
	function edit( $id = 0 ) {
		$records = $this->model->getRecord( $id );

		if ( count( $records ) > 0){
			$recorddata = $records;
			$formaction = $this->scriptPath."updateRecord/".$id;
		} else {
			$recorddata = array();
			$formaction = $this->scriptPath."createNew";
		}
		$data['form'] 				= $this->tca->getFormArray( $formaction, array( $this->tablename => $recorddata ) );
		$data['shipments'] 			= $recorddata['shipments'];
		$data['linkedShipments'] 	= $recorddata['linkedShipments'];
		$data['additionalJS'] 		= '<script src="'.base_url().'js/pcshipment.js"></script>';
		$this->load->view($this->recordEditView, $data );
	}


	/**
	 * @Override
	 */
	public function updateRecord( $id = 0 ) {
		$insertArray 	= $this->getInsertArrayFromPostVars();
		if( $id == 0 ) {
			$this->createNew();
		} else {
			$previousRow = $this->model->getRecord( $id );
			$this->model->updateRecord( $id, $insertArray[0] );
			$this->model->updateRelatedShipment( $id );
			$latestRow = $this->model->getRecord( $id );

			$isInitiallyAdded = $this->input->post()['delivery-added-initially'];

			if( $isInitiallyAdded ) {
				$this->model->processEventDatesInitially( $latestRow );
			} else {
				$this->model->processEventDates( $latestRow, $previousRow );
			}

		}
		redirect( $this->scriptPath.'edit/'.$id );
	}

	/**
	 * addDelivery
	 * add deliveries record to pcshipments
	 * in the frontend dynamically.
	 */
	public function addDelivery() {
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');
		$success			= FALSE;
		$deliveryId 		= $this->input->post('deliveryId');
		$pcshipmentId 		= $this->input->post('pcshipmentId');
		$shipmentId 		= $this->input->post('shipmentId');

		$this->welogger->log( "addDelivery - deliveryId: ".$deliveryId.", pcshipmentId: ".$pcshipmentId.", shipmentId: ".$shipmentId, WELogger::$LOG_LEVEL_WARNING, "pcshipment.addDelivery" );

		// create relation
		if( $deliveryId > 0 ) {

			$this->pcshipment_delivery_model->addRelation($shipmentId, $pcshipmentId, $deliveryId);

			$pcShipment = $this->model->getRecord( $pcshipmentId );

			// update hbl/hawb in shipment
			$this->model->addHBLHAWBToShipment( $shipmentId, $pcShipment['hbl_hawb'] );

			// update hbl/hawb in shipment delivery relation
			$this->model->addHBLHAWBToShipmentDeliveryByDeliveryId( $deliveryId, $shipmentId, $pcShipment['hbl_hawb'] );

			// update corresponding shipment
			$this->model->updateRelatedShipment( $pcshipmentId, $deliveryId );

			$success = TRUE;
		}
		print( json_encode( array( 'success' => $success ) ) );

	}

	/**
	 * removeDelivery
	 * add HBL/HAWB record to pcshipments
	 * in the frontend dynamically.
	 */
	public function removeDelivery() {
		$this->load->model('Pcshipment_delivery_model', 'pcshipment_delivery_model');

		$success 		= TRUE;
		$deliveryId 		= $this->input->post('deliveryId');
		$pcshipmentId 		= $this->input->post('pcshipmentId');
		$shipmentId 		= $this->input->post('shipmentId');

		$this->welogger->log( "removeDelivery - deliveryId: ".$deliveryId.", pcshipmentId: ".$pcshipmentId.", shipmentId: ".$shipmentId, WELogger::$LOG_LEVEL_WARNING, "pcshipment.addDelivery" );

		$this->pcshipment_delivery_model->removeRelation($shipmentId, $pcshipmentId, $deliveryId);

		//$pcshipment = $this->model->getRecord( $pcshipmentId );
		//$this->model->removeHBLHAWBFromShipment( $pcshipmentId, $shipmentId );
		//$this->model->removeHBLHAWBFromShipmentDelivery( $shipmentId, $pcshipment['hbl_hawb'] );

		print( json_encode( array( 'success' => $success ) ) );
	}


	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {

		// ist nur ne quick&dirty Lösung!
		// muss man mal ordentlich machen...

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "HBL/HAWB";
		$tmp['input'] = '<input type="text" id="pcshipments_hbl_hawb" value="'.$this->getSearchFormValue('pcshipments_hbl_hawb').'" name="pcshipments_hbl_hawb[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract/PO number";
		$tmp['input'] = $this->tca->getFormInputById('pcshipments_po_number', $this->getSearchFormValue('pcshipments_po_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Container number";
		$tmp['input'] = '<input type="text" id="pcshipments_container_number" value="'.$this->getSearchFormValue('pcshipments_container_number').'" name="pcshipments_container_number[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Departure";
		$tmp['input'] = $this->tca->getFormInputById('pcshipments_departure_port', $this->getSearchFormValue('pcshipments_departure_port'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Destination";
		$tmp['input'] = $this->tca->getFormInputById('pcshipments_destination_port', $this->getSearchFormValue('pcshipments_destination_port'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Traffic type";
		$tmp['input'] = form_dropdown( 'contracts_traffic_type[]', array( "" => "please select ...", "1" => "Factory", "2" => "CFS", "3" => "AIR", "4" => "PTS Loading", "5" => "Factory/CFS" ), $this->getSearchFormValue('contracts_traffic_type') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Event";
		$tmp['input'] = form_dropdown( 'pcevent[]', array( "" => "please select ...", "etd" => "ETD", "eta" => "ETA", "cus" => "CUS", "atd" => "ATD", "ata" => "ATA" ), $this->getSearchFormValue('pcevent') );
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "From";
		$tmp['input'] = '<input type="text" class="datepicker" id="pcshipments_from" value="'.$this->getSearchFormValue('pcshipments_from').'" name="pcshipments_from[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "To";
		$tmp['input'] = '<input type="text" class="datepicker" id="pcshipments_to" value="'.$this->getSearchFormValue('pcshipments_to').'" name="pcshipments_to[]">';
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On-Sale week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Logwin origin office";
		$tmp['input'] = $this->tca->getFormInputById('contracts_logwin_origin_office', $this->getSearchFormValue('contracts_logwin_origin_office'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Supplier";
		$tmp['input'] = $this->tca->getFormInputById('contracts_supplier', $this->getSearchFormValue('contracts_supplier'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product code";
		$tmp['input'] = $this->tca->getFormInputById('deliveries_product_code', $this->getSearchFormValue('deliveries_product_code'), '1');
		$res[] = $tmp;

		return $res;
	}

}

/* End of file pcshipment.php */
/* Location: ./app/controllers/pcshipment.php */