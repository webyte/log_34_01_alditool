<?php
class Import_cron extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Import_procars_model', 'import_procars_model');
		$this->load->model('Import_tms_model', 'import_tms_model');
		$this->load->model('Import_contract_model', 'import_contract_model');
		$this->load->model('Import_contract_special_model', 'import_contract_special_model');
		//$this->output->enable_profiler(TRUE);
	}

	public function init() {
		$this->import_procars_model->log('import procars start');
		$this->import_procars_model->import();
		$this->import_procars_model->log('import procars end');

		$this->import_tms_model->log('import tms start');
		$this->import_tms_model->import();
		$this->import_tms_model->log('import tms end');

		$this->import_contract_model->log('import core contract start');
		$this->import_contract_model->setImportSubPath( 'contract/core/' );
		$this->import_contract_model->setContractType( Import_contract_model::$CONTRACT_TYPE_CORE_RANGE );
		$this->import_contract_model->import();
		$this->import_contract_model->log('import core contract end');

		$this->import_contract_model->log('import aui contract start');
		$this->import_contract_model->setImportSubPath( 'contract/aui/' );
		$this->import_contract_model->setContractType( Import_contract_model::$CONTRACT_TYPE_AUI );
		$this->import_contract_model->import();
		$this->import_contract_model->log('import aui contract end');

		$this->import_contract_special_model->log('import special contract start');
		$this->import_contract_special_model->setImportSubPath( 'contract/special/' );
		$this->import_contract_special_model->setContractType( import_contract_special_model::$CONTRACT_TYPE_SPECIAL_BUYS );
		$this->import_contract_special_model->import();
		$this->import_contract_special_model->log('import special contract end');
	}

}

/* End of file import_cron.php */
/* Location: ./app/controllers/import_cron.php */