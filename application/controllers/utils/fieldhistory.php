<?php

class Fieldhistory extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	////////////////////////////////////////////////////////////////////////////

	//	AJAX - Documents Handling

	////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * getHistory
	 * returns the former values of a given db field
	 * 
	 * @param string $recordId - id of the record as it is stored in recorddata_history
	 * @param string $fieldName - name of the field
	 */
	public function getHistory( $recordId, $fieldName, $fieldValue='' ) {
//		if (IS_AJAX) {
			$versions = $this->getVersions( $recordId );
			$res = $this->getFieldHistory( $fieldName, urldecode( $fieldValue ), $versions );
			echo $res;
//		}
	}
	
	private function getVersions( $recordId ) {
		$this->db->select('recorddata_history.*, users.username, recorddata_history.id AS id');
		$this->db->from( 'recorddata_history' );
		$this->db->join('users', 'recorddata_history.user_id = users.id', 'left');
		$this->db->where( array('record_identifier_hash' => $recordId) );
		$this->db->group_by( 'recorddata_history.id' );
		$this->db->order_by( 'recorddata_history.version ASC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	private function getFieldHistory( $fieldName, $fieldValue, $versions ) {
		$res 		= "";
		$lastVal 	= "";
		$counter 	= 0;
		foreach ( $versions as $version ) {
       		$tmpData 	= json_decode( $version['record_data'], true );
       		$val 		= isset( $tmpData[$fieldName] ) ? $tmpData[$fieldName] : '';
       		if( $lastVal != $val ) {
       			$res .= "<tr><td>".$val."</td><td><i>".$version['crdate']."</i></td><td><i>".$version['username']."</i></td></tr>";
       			$counter++;
       		}
			$lastVal = $val;
		}
		return $counter > 0 ? '<table class="field-history">'.$res.'</table>' : '';
	}
}

/* End of file order.php */
/* Location: ./app/controllers/order.php */