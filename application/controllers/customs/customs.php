<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Customs extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Customs_model', 'customs_model');

		$this->tablename 			= 'customs';
		$this->scriptPath 			= 'customs/customs/';
		$this->recordEditView 		= '';
		$this->headline				= 'Customs';
		$this->infotext				= '';
		$this->showDefaultSearchForm = FALSE;
		$this->overviewFields 	= array(
			array('label' => 'Product Code', 		'fieldname' => 'customs_product_code', 			'type' => ''),
			array('label' => 'Contract Number', 	'fieldname' => 'customs_contract_number', 		'type' => ''),
			array('label' => 'Description', 		'fieldname' => 'contracts_product_description', 'type' => ''),
			array('label' => 'Country', 			'fieldname' => 'customs_country', 				'type' => ''),
			array('label' => 'Supplier', 			'fieldname' => 'contracts_supplier', 			'type' => ''),
			array('label' => 'Product Specs', 		'fieldname' => 'customs_product_specs', 		'type' => ''),
			array('label' => 'Customs Duty Rate Excl. FTA', 'fieldname' => 'customs_customs_duty_rate_excl_fta', 'type' => ''),
			array('label' => 'Customs Duty Rate Incl. FTA', 'fieldname' => 'customs_customs_duty_rate_incl_fta', 'type' => 'date'),
			array('label' => 'Tariff Classification', 'fieldname' => 'customs_tariff_classification', 'type' => 'date'),
			array('label' => 'Tariff Concession', 	'fieldname' => 'customs_tariff_concession', 	'type' => ''),
			array('label' => 'Requirements', 		'fieldname' => 'customs_requirements', 			'type' => ''),
			array('label' => 'CoO/DoO Required', 	'fieldname' => 'customs_coo_doo_received', 		'type' => ''),
			array('label' => 'Additional Notes', 	'fieldname' => 'customs_additional_notes', 		'type' => ''),
		);

		//$this->output->enable_profiler(TRUE);
	}


	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {

		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= array();
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']			= generateCustomSearchForm( $this->tablename, array(), 'customs/customs/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']		= '';
		$data['headline'] 			= $this->headline;
		$data['infotext'] 			= $this->infotext;
		$data['createnewlink'] 		= '';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/customs.js"></script>';
		$this->load->view('customs/record_overview', $data );
	}

	/**
	 * @Override
	 */
	public function result( $searchParams = array(), $ignoreSearchParams = FALSE ) {

		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->records					= $this->customs_model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']			= generateCustomSearchForm( $this->tablename, array(), 'customs/customs/result', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']		= generateCustomOverview( $this->overviewFields, $this->records, array(), 'customs/overview_table' );
		$data['headline'] 			= $this->headline;
		$data['infotext'] 			= $this->infotext;
		$data['createnewlink'] 		= '';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/customs.js"></script>';

		$this->load->view('customs/record_overview', $data );
	}

	/**
	 * excel_export
	 * @return
	 */
	public function excel_export(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$objPHPExcel 	= new PHPExcel();
		$objWorksheet 	= $objPHPExcel->getActiveSheet();

		// labels
		$colCounter = 0;
		foreach( $this->overviewFields as $field ) {
			$objWorksheet->setCellValueByColumnAndRow( $colCounter,1, $field['label'] );
			$colCounter++;
		}

		// content
		$recordListSearchParams = $this->input->post();
		$rows					= $this->customs_model->getOverviewRecords( $recordListSearchParams );
		$rowCounter = 2;
		foreach ( $rows as $data ) {
			$colCounter = 0;
			foreach( $this->overviewFields as $field ) {
				$objWorksheet->setCellValueByColumnAndRow( $colCounter, $rowCounter, $data[$field['fieldname']] );
				$colCounter++;
			}
			$rowCounter++;
		}

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel2007' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=logwin_customs.xlsx' );
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}

	/**
	 * getCustomSearchFormFields
	 * creates custom formfields for searchform.
	 */
	public function getCustomSearchFormFields() {
		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Contract Number";
		$tmp['input'] = $this->tca->getFormInputById('customs_contract_number', $this->getSearchFormValue('customs_contract_number'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Product Code";
		$tmp['input'] = $this->tca->getFormInputById('customs_product_code', $this->getSearchFormValue('customs_product_code'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "On Sale Week";
		$tmp['input'] = $this->tca->getFormInputById('contracts_advertisement_week', $this->getSearchFormValue('contracts_advertisement_week'), '1');
		$res[] = $tmp;

		$tmp['prefield_markup'] = "";
		$tmp['label'] = "Tariff Classification";
		$tmp['input'] = $this->tca->getFormInputById('customs_tariff_classification', $this->getSearchFormValue('customs_tariff_classification'), '1');
		$res[] = $tmp;

		return $res;
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////



}

/* End of file customs.php */
/* Location: ./app/controllers/customs/customs.php */