<?php
class Customs_upload extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('user_agent');
		$this->load->model('Customs_model', 'customs_model');
		$this->load->model('Contract_model', 'contract_model');
	}

	public function index() {
		$data = array();
		$data['message'] 		= '';
		$data['additionalJS'] 	= '<script src="'.base_url().'js/upload.js"></script>';
		$this->load->view('customs/upload_overview', $data );
	}

	/**
	 * do_upload
	 * @param
	 * @return
	 */
	public function do_upload() {
		$upload = $this->saveUpload( 'customs-upload-file' );
		if( !empty( $upload ) ) {
			$this->importUpload( $upload );
			$data['error'] 		= FALSE;
			$data['message'] 	= '<h4>Success!</h4>File was successfully imported.';
		} else {
			$data['error'] 		= TRUE;
			$data['message'] 	= '<h4>Error!</h4>Upload failure, please try again.';
		}
		$data['additionalJS'] 	= '<script src="'.base_url().'js/upload.js"></script>';

		$this->load->view('customs/upload_overview', $data );
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * importUpload
	 * @param $file
	 * @param $supplier
	 * @return array
	 */
	private function importUpload( $file ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$src 			= $this->config->item('documents_upload_path').$file;
		$objPHPExcel 	= IOFactory::load( $src );
		$objWorksheet 	= $objPHPExcel->getActiveSheet();
		$this->importAndSaveToDB( $objWorksheet );
	}


	/**
	 * importAndSaveToDB
	 * @param $objWorksheet
	 * @return
	 */
	private function importAndSaveToDB( $objWorksheet ) {
		$i = 1;
		foreach ($objWorksheet->getRowIterator() as $row) {
			if( $i > 1 ) {
				$data								= array();
				$data['product_code'] 				= (string) $objWorksheet->getCellByColumnAndRow(0, $i)->getValue();
				$data['contract_number'] 			= (string) $objWorksheet->getCellByColumnAndRow(1, $i)->getValue();
				$data['country'] 					= (string) $objWorksheet->getCellByColumnAndRow(3, $i)->getValue();
				$data['product_specs'] 				= (string) $objWorksheet->getCellByColumnAndRow(5, $i)->getValue();
				$data['customs_duty_rate_excl_fta'] = (string) $objWorksheet->getCellByColumnAndRow(6, $i)->getValue();
				$data['customs_duty_rate_incl_fta'] = (string) $objWorksheet->getCellByColumnAndRow(7, $i)->getValue();
				$data['tariff_classification'] 		= (string) $objWorksheet->getCellByColumnAndRow(8, $i)->getValue();
				$data['tariff_concession'] 			= (string) $objWorksheet->getCellByColumnAndRow(9, $i)->getValue();
				$data['requirements'] 				= (string) $objWorksheet->getCellByColumnAndRow(10, $i)->getValue();
				$data['additional_notes'] 			= (string) $objWorksheet->getCellByColumnAndRow(12, $i)->getValue();
				$data['contract_id'] 				= $this->contract_model->getIdByContractNumber( $data['contract_number'] );

				if( !empty( $data['customs_duty_rate_incl_fta'] ) ) 				$data['coo_doo_received'] = 'yes';
				if( strtolower( $data['customs_duty_rate_excl_fta'] ) == 'free' ) 	$data['coo_doo_received'] = 'no';

				// previous customs
				$id = $this->customs_model->getIdByContractNumber( $data['contract_number'] );
				if( $id > 0 ) {
					$this->customs_model->updateRecord( $id, $data );
				} else {
					$id = $this->customs_model->createNew( $data );
				}
			}
			$i++;
		}
	}

	/**
	 * saveUpload
	 * @param $field
	 * @return string
	 */
	private function saveUpload( $field ) {
		$config['upload_path'] 		= $this->config->item('documents_upload_path');
		$config['allowed_types'] 	= $this->config->item('allowed_types');
		$config['max_size']			= $this->config->item('max_size');
		$config['encrypt_name'] 	= TRUE;
		$destFile					= '';

		$this->load->library( 'upload', $config );

		if ( !$this->upload->do_upload( $field ) ) {
			$data 	= $this->upload->data();
			$error 	= array('error' => $this->upload->display_errors());

			$this->welogger->log( 'Upload failure, file: '.$data['full_path'].', error: '.$error['error'].', image: '.serialize( $this->upload->data() ), WELogger::$LOG_LEVEL_ERROR, 'Edit.do_upload' );
		} else {
			$data 		= $this->upload->data();
			$source 	= $data['full_path'];
			$destFile	= $data['file_name'];

			$this->welogger->log( 'Upload success, file: '.$source, WELogger::$LOG_LEVEL_INFO, 'Edit.do_upload' );

		}
		return $destFile;
	}

}

/* End of file upload.php */
/* Location: ./app/controllers/customs/upload.php */