<?php
require_once( APPPATH.'controllers/delivery/delivery'.EXT );

class Delivery_special_buys extends Delivery {

	function __construct() {
		parent::__construct();
		$this->headline				= 'Deliveries - Special buys';
		$this->scriptPath 			= 'delivery/delivery_special_buys/';
		$this->contractType			= Contract_model::$CONTRACT_TYPE_SPECIAL_BUYS;
		//$this->output->enable_profiler(TRUE);
	}
	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}

/* End of file delivery.php */
/* Location: ./app/controllers/delivery_special_buys.php */