<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Api extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Delivery_model', 'model');
		//$this->output->enable_profiler(TRUE);
	}


	public function deliveries() {
		$key = $this->input->get('key');
		if( $key == $this->config->item('api_key') ) {
			$records = $this->model->getApiRecords();
			print( json_encode( $records ) );
		}
	}


}

/* End of file delivery.php */
/* Location: ./app/controllers/delivery.php */