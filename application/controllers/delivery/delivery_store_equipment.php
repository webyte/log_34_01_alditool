<?php
require_once( APPPATH.'controllers/delivery/delivery'.EXT );

class Delivery_store_equipment extends Delivery {

	function __construct() {
		parent::__construct();
		$this->headline				= 'Deliveries - Store equipment';
		$this->scriptPath 			= 'delivery/delivery_store_equipment/';
		$this->contractType			= Contract_model::$CONTRACT_TYPE_STORE_EQUIPMENT;
		//$this->output->enable_profiler(TRUE);
	}
	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}

/* End of file delivery.php */
/* Location: ./app/controllers/delivery_core_range.php */