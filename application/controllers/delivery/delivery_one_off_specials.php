<?php
require_once( APPPATH.'controllers/delivery/delivery'.EXT );

class Delivery_one_off_specials extends Delivery {

	function __construct() {
		parent::__construct();
		$this->headline				= 'Deliveries - One Off Specials';
		$this->scriptPath 			= 'delivery/delivery_one_off_specials/';
		$this->contractType			= Contract_model::$CONTRACT_TYPE_ONE_OFF_SPECIALS;
		//$this->output->enable_profiler(TRUE);
	}
	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}

/* End of file delivery.php */
/* Location: ./app/controllers/delivery_core_range.php */