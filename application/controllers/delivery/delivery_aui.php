<?php
require_once( APPPATH.'controllers/delivery/delivery'.EXT );

class Delivery_aui extends Delivery {

	function __construct() {
		parent::__construct();
		$this->headline				= 'Deliveries - AUI';
		$this->scriptPath 			= 'delivery/delivery_aui/';
		$this->contractType			= Contract_model::$CONTRACT_TYPE_AUI;
		//$this->output->enable_profiler(TRUE);
	}
	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}

/* End of file delivery.php */
/* Location: ./app/controllers/delivery_core_range.php */