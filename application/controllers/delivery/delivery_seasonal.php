<?php
require_once( APPPATH.'controllers/delivery/delivery'.EXT );

class Delivery_seasonal extends Delivery {

	function __construct() {
		parent::__construct();
		$this->headline				= 'Deliveries - eCommerce';
		$this->scriptPath 			= 'delivery/delivery_ecommerce/';
		$this->contractType			= Contract_model::$CONTRACT_TYPE_SEASONAL;
		//$this->output->enable_profiler(TRUE);
	}
	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}

/* End of file delivery.php */
/* Location: ./app/controllers/delivery_core_range.php */