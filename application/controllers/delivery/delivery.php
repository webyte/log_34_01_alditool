<?php
require_once( APPPATH.'controllers/abstract_controller'.EXT );

class Delivery extends Abstract_controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Delivery_model', 'model');
		$this->load->model('Contract_model', 'contract_model');
		$this->load->model('Shipment_model', 'shipment_model');

		$this->tablename 			= 'deliveries';
		$this->scriptPath 			= 'delivery/delivery/';
		$this->recordEditView 		= 'delivery/deliverydata_edit';
		$this->headline				= 'Deliveries';
		$this->infotext				= '';
		$this->showDefaultSearchForm = FALSE;
		$this->contractType			= 0;

		$this->overviewFields 	= array(
			array('label' => 'Contract/PO number', 	'fieldname' => 'contract_number', 		'type' => ''),
			array('label' => 'Product code', 		'fieldname' => 'product_code', 			'type' => ''),
			array('label' => 'DC', 					'fieldname' => 'dc', 					'type' => ''),
			array('label' => 'On-Sale week', 		'fieldname' => 'advertisement_week', 	'type' => ''),
			array('label' => 'FOB date', 			'fieldname' => 'fob_date', 				'type' => 'date'),
			array('label' => 'ALDI AU WH delivery due date', 'fieldname' => 'aldi_au_wh_delivery_due_date', 'type' => 'date'),
			array('label' => 'Departure', 			'fieldname' => 'departure', 			'type' => ''),
			array('label' => 'Destination', 		'fieldname' => 'destination', 			'type' => ''),
			array('label' => 'Case quantity', 		'fieldname' => 'case_quantity', 		'type' => ''),
			array('label' => 'Remaining case quantity', 	'fieldname' => 'case_quantity_remain', 	'type' => ''),
			array('label' => 'Unit quantity', 		'fieldname' => 'unit_quantity', 		'type' => ''),
			array('label' => 'Remaining unit quantity', 	'fieldname' => 'unit_quantity_remain', 	'type' => ''),
		);
		//$this->output->enable_profiler(TRUE);
	}


	/**
	 * @Override
	 */
	public function recordList( $searchParams = array(), $ignoreSearchParams = FALSE ) {
		$this->recordListSearchParams 	= $this->input->post() && !$ignoreSearchParams ? $this->input->post() : $searchParams;
		$this->recordListSearchParams['deliveries_modified'] = array( date( 'Y-m-d 00:00:00', strtotime( '-183 days', time() ) ) );
		if( $this->contractType > 0 ) {
			$this->recordListSearchParams['contracts_contract_type'] = array( $this->contractType );
		}
		$this->records					= $this->model->getOverviewRecords( $this->recordListSearchParams, $this->overviewFields );
		$searchFormFields	 			= $this->getCustomSearchFormFields();

		$data = array();
		$data['searchform']			= generateCustomSearchForm( $this->tablename, array(), $this->scriptPath.'recordList', $searchFormFields, 'Go', $this->recordListSearchParams );
		$data['resulttable']		= generateCustomOverview( $this->overviewFields, $this->records, array(), 'delivery/overview_table' );
		$data['headline'] 			= $this->headline;
		$data['infotext'] 			= $this->infotext;
		$data['consolidatelink'] 	= $this->scriptPath.'consolidate';
		$data['reportlink'] 		= $this->scriptPath.'report';
		$data['additionalJS'] 		= '<script src="'.base_url().'js/delivery.js"></script>';

		$this->load->view('delivery/record_overview', $data );
	}

	/**
	 * consolidate
	 * generates new shipment with the selected bookings
	 * @param
	 */
	public function consolidate() {
		$tmp 		= $this->input->post('consolidate-delivery-ids');
		$deliveries = json_decode( $tmp );

		if( $tmp && is_array( $deliveries ) ) {
			$this->load->model('Shipment_delivery_model', 'shipment_delivery_model');
			$shipmentId 	= $this->shipment_model->createNew( array() );
			foreach( $deliveries as $delivery ) {
				$record = $this->model->getRecord( $delivery->id );
				$this->shipment_delivery_model->createNew( array( 'delivery_id' => $record['id'], 'shipment_id' => $shipmentId ) );

				$remainUnitQuantity = $this->shipment_delivery_model->getRemainingUnitQuantityByDeliveryId( $record['id'] );
				$this->shipment_delivery_model->updateUnitQuantity( $shipmentId, $record['id'], $remainUnitQuantity );
			}
			redirect( '/shipment/shipment/edit/'.$shipmentId.'/consolidate' );
		} else {
			$this->recordList( array(), TRUE );
		}
	}

	/**
	 * report
	 * generates pdf report
	 * @param
	 */
	public function report(){
		$tmp 		= $this->input->post('report-delivery-ids');
		$deliveries = json_decode( $tmp );
		$data 		= array();
		foreach( $deliveries as $delivery ) {
			$data[] = $delivery->id;
		}

		$records 		= $this->model->getReportRecords( $data );
		$objPHPExcel 	= $this->generateObjPHPExcel( $records );

		$objWriter 		= IOFactory::createWriter( $objPHPExcel, 'Excel5' );
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename='.( !empty( $filename ) ? $filename : 'logwin_aldi_delivery_report.xls' ));
		header('Cache-Control: max-age=0');
		$objWriter->save('php://output');
	}

	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

	/**
	 * generateObjPHPExcel
	 */
	private function generateObjPHPExcel( $records ) {
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$objPHPExcel 		= new PHPExcel();
		$objWorksheet 		= $objPHPExcel->getActiveSheet();
		$i					= 1;

		$normalStyle = array(
			'font'  => array(
				'bold'  => false,
				'name'  => 'Arial'
			)
		);
		$normalRightStyle = array(
			'font'  => array(
				'bold'  => false,
				'name'  => 'Arial'
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			)
		);
		$boldStyle = array(
			'font'  => array(
				'bold'  => true,
				'name'  => 'Arial'
			)
		);
		$boldRightStyle = array(
			'font'  => array(
				'bold'  => true,
				'name'  => 'Arial'
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			)
		);

		$objWorksheet->getColumnDimension('A')->setAutoSize(true);
		$objWorksheet->getColumnDimension('B')->setAutoSize(true);
		$objWorksheet->getColumnDimension('C')->setAutoSize(true);
		$objWorksheet->getColumnDimension('D')->setAutoSize(true);
		$objWorksheet->getColumnDimension('E')->setAutoSize(true);
		$objWorksheet->getColumnDimension('F')->setAutoSize(true);
		$objWorksheet->getColumnDimension('G')->setAutoSize(true);
		$objWorksheet->getColumnDimension('H')->setAutoSize(true);
		$objWorksheet->getColumnDimension('I')->setAutoSize(true);
		$objWorksheet->getColumnDimension('J')->setAutoSize(true);
		$objWorksheet->getColumnDimension('K')->setAutoSize(true);
		$objWorksheet->getColumnDimension('L')->setAutoSize(true);
		$objWorksheet->getColumnDimension('M')->setAutoSize(true);
		$objWorksheet->getColumnDimension('N')->setAutoSize(true);
		$objWorksheet->getColumnDimension('O')->setAutoSize(true);
		$objWorksheet->getColumnDimension('P')->setAutoSize(true);
		$objWorksheet->getColumnDimension('Q')->setAutoSize(true);

		foreach( $records as $key=>$value ) {
			$objWorksheet->getCell('A'.$i)->setValue( 'POD' );
			$objWorksheet->getStyle('A'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('B'.$i)->setValue( 'Contract no' );
			$objWorksheet->getStyle('B'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('C'.$i)->setValue( 'Product code' );
			$objWorksheet->getStyle('C'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('D'.$i)->setValue( 'DC' );
			$objWorksheet->getStyle('D'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('E'.$i)->setValue( 'Remaining case qty ' );
			$objWorksheet->getStyle('E'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('F'.$i)->setValue( 'Remaining unit qty ' );
			$objWorksheet->getStyle('F'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('G'.$i)->setValue( 'Gross weight' );
			$objWorksheet->getStyle('G'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('H'.$i)->setValue( 'Volume' );
			$objWorksheet->getStyle('H'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('I'.$i)->setValue( 'Container size' );
			$objWorksheet->getStyle('I'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('J'.$i)->setValue( 'Container type' );
			$objWorksheet->getStyle('J'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('K'.$i)->setValue( 'Cargo ready date' );
			$objWorksheet->getStyle('K'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('L'.$i)->setValue( 'Carton weight' );
			$objWorksheet->getStyle('L'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('M'.$i)->setValue( 'Carton vol' );
			$objWorksheet->getStyle('M'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('N'.$i)->setValue( 'CFS remark' );
			$objWorksheet->getStyle('N'.$i)->applyFromArray( $boldStyle );

			$objWorksheet->getCell('O'.$i)->setValue( 'Length' );
			$objWorksheet->getStyle('O'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('P'.$i)->setValue( 'Width' );
			$objWorksheet->getStyle('P'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('Q'.$i)->setValue( 'Height' );
			$objWorksheet->getStyle('Q'.$i)->applyFromArray( $boldStyle );

			$j = 1;
			foreach( $value['deliveries'] as $delivery ) {
				if( $j == 1 ) {
					$objWorksheet->getCell('A'.($i+$j))->setValue( $key );
					$objWorksheet->getStyle('A'.($i+$j))->applyFromArray( $normalStyle );
				}
				$objWorksheet->getCell('B'.($i+$j))->setValue( $delivery['deliveries_contract_number'] );
				$objWorksheet->getStyle('B'.($i+$j))->applyFromArray( $normalStyle );
				$objWorksheet->getCell('C'.($i+$j))->setValue( $delivery['deliveries_product_code'] );
				$objWorksheet->getStyle('C'.($i+$j))->applyFromArray( $normalStyle );
				$objWorksheet->getCell('D'.($i+$j))->setValue( $delivery['deliveries_dc'] );
				$objWorksheet->getStyle('D'.($i+$j))->applyFromArray( $normalStyle );
				$objWorksheet->getCell('E'.($i+$j))->setValue( $delivery['deliveries_case_quantity_remain'] );
				$objWorksheet->getStyle('E'.($i+$j))->applyFromArray( $normalStyle );
				$objWorksheet->getCell('F'.($i+$j))->setValue( $delivery['deliveries_unit_quantity_remain'] );
				$objWorksheet->getStyle('F'.($i+$j))->applyFromArray( $normalStyle );
				$objWorksheet->getCell('G'.($i+$j))->setValueExplicit( $delivery['deliveries_gross_weight'] );
				$objWorksheet->getStyle('G'.($i+$j))->applyFromArray( $normalRightStyle );
				$objWorksheet->getCell('H'.($i+$j))->setValueExplicit( $delivery['deliveries_volume'] );
				$objWorksheet->getStyle('H'.($i+$j))->applyFromArray( $normalRightStyle );
				$objWorksheet->getCell('K'.($i+$j))->setValueExplicit( $delivery['contracts_actual_contract_ready_date'] );
				$objWorksheet->getStyle('K'.($i+$j))->applyFromArray( $normalRightStyle );
				$objWorksheet->getCell('L'.($i+$j))->setValueExplicit( $delivery['contracts_case_weight'] );
				$objWorksheet->getStyle('L'.($i+$j))->applyFromArray( $normalRightStyle );
				$objWorksheet->getCell('M'.($i+$j))->setValueExplicit( $delivery['contracts_case_cubage'] );
				$objWorksheet->getStyle('M'.($i+$j))->applyFromArray( $normalRightStyle );


				$objWorksheet->getCell('O'.($i+$j))->setValueExplicit( $delivery['contracts_case_length'] );
				$objWorksheet->getStyle('O'.($i+$j))->applyFromArray( $normalRightStyle );
				$objWorksheet->getCell('P'.($i+$j))->setValueExplicit( $delivery['contracts_case_width'] );
				$objWorksheet->getStyle('P'.($i+$j))->applyFromArray( $normalRightStyle );
				$objWorksheet->getCell('Q'.($i+$j))->setValueExplicit( $delivery['contracts_case_height'] );
				$objWorksheet->getStyle('Q'.($i+$j))->applyFromArray( $normalRightStyle );
				$j++;
			}
			$i = $i+$j;

			$objWorksheet->getCell('A'.$i)->setValue( 'Total' );
			$objWorksheet->getStyle('A'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('E'.$i)->setValue( $value['case_quantity_remain_total'] );
			$objWorksheet->getStyle('E'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('F'.$i)->setValue( $value['unit_quantity_remain_total'] );
			$objWorksheet->getStyle('F'.$i)->applyFromArray( $boldStyle );
			$objWorksheet->getCell('G'.$i)->setValueExplicit( $value['gross_weight_total'] );
			$objWorksheet->getStyle('G'.$i)->applyFromArray( $boldRightStyle );
			$objWorksheet->getCell('H'.$i)->setValueExplicit( $value['volume_total'] );
			$objWorksheet->getStyle('H'.$i)->applyFromArray( $boldRightStyle );

			$i = $i+2;
		}

		return $objPHPExcel;
	}


}

/* End of file delivery.php */
/* Location: ./app/controllers/delivery.php */