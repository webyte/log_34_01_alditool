<?php
require_once( APPPATH.'controllers/delivery/delivery'.EXT );

class Delivery_core_range extends Delivery {

	function __construct() {
		parent::__construct();
		$this->headline				= 'Deliveries - Core range';
		$this->scriptPath 			= 'delivery/delivery_core_range/';
		$this->contractType			= Contract_model::$CONTRACT_TYPE_CORE_RANGE;
		//$this->output->enable_profiler(TRUE);
	}
	////////////////////////////////////////////////////////////////////////////

	//		private

	////////////////////////////////////////////////////////////////////////////

}

/* End of file delivery.php */
/* Location: ./app/controllers/delivery_core_range.php */