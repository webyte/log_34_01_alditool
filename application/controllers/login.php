<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    public $userData;

	public function __construct() {
		parent::__construct();
		$this->load->model('User_model', 'model');
		$this->load->helper( 'url' );
		//$this->output->enable_profiler(TRUE);
	}

	public function index() {
		$username			= (	$this->input->post( "username" ) ) ? $this->input->post( "username" ) : "";
		$password			= (	$this->input->post( "password" ) ) ? $this->input->post( "password" ) : "";
		$action				= ( $this->input->post( "action" ) ) ? 	 $this->input->post( "action" )   : "";
		$redirect			= ( $this->input->post( "redirect" ) ) ? $this->input->post( "redirect" ) : "";
		switch ( $action ) {
			case "login":
				$this->do_login( $username, $password, $redirect );
				break;

			case "logout":
				$this->logout();
				break;

			default:
				if( $this->session->userdata('isLoggedIn')  ) redirect( $this->config->item('login_success_default_redirect'), 'location' );
				else $this->load->view( 'login/login', array( 'error' => false, 'redirect' => '' ) );
				break;
		}
	}

	/**
	 * login
	 * @param $username
	 * @param $password
	 * @param $redirect
	 * @return
	 */
	private function do_login( $username, $password, $redirect ){
		$this->welogger->log( "login called with username ".$username, WELogger::$LOG_LEVEL_INFO, "Login.do_login" );
		$query = $this->db->query( "SELECT * FROM users WHERE username='".$this->db->escape_str( $username )."' AND password='".$this->db->escape_str( $password )."' AND hidden=0 AND deleted=0" );

        if( $query->num_rows() > 0 && isset($username) ) {
			$this->welogger->log( "successful login with username ".$username, WELogger::$LOG_LEVEL_INFO, "Login.do_login" );

			//destroy all existing session data
			$tmp = array();
			$fields = $this->db->list_fields('users');
			foreach( $fields as $field ) {
				$tmp[$field] = '';
			}
			$tmp['isLoggedIn'] 			= '';
			$tmp['userId'] 				= '';
			$tmp['loginRedirect'] 		= '';

			$this->session->unset_userdata( $tmp );

			//create session control var
			$tmp = $query->row_array();
			$tmp['isLoggedIn'] 		= true;
			$tmp['userId'] 			= $tmp['id'];
			$tmp['loginRedirect'] 	= $this->getRedirectTarget( $tmp['usergroup'] );


			$this->session->set_userdata( $tmp );
            $this->setLastLoginTime( $tmp ); // Updates timestampfield last_login
            $this->checkLastPasswordSetAndRedirect( $tmp, $redirect );

		} else {
			$this->welogger->log( "login failed with username ".$username, WELogger::$LOG_LEVEL_WARNING, "Login.do_login" );
			$this->load->view( 'login/login', array( 'error' => true, 'redirect' => '' ) );
		}
	}

    /**
     * Logout
     */
    public function logout(){
		$url = ( $this->input->get( 'back_url' ) ) ? urlencode( $this->input->get( 'back_url' ) ) : '';
		$this->session->sess_destroy();
		$this->load->view( 'login/login', array( 'error' => false, 'redirect' => $url ) );
	}


    ////////////////////////////////////////////////////////////////////////////

    //		private

    ////////////////////////////////////////////////////////////////////////////

	/**
	 * getRedirectTarget
	 */
	private function getRedirectTarget( $userGroupId ){
		// get redirect target
		$this->load->model('Usergroup_model', 'usergroup_model');
		$usergroup = $this->usergroup_model->getRecord( $userGroupId );
		return isset( $usergroup['login_redirect'] ) ? $usergroup['login_redirect'] : '';
	}

	/**
	 * Sets the time, when a user Logs in
	 */
	private function setLastLoginTime( $userData ){
		$ts = date( "Y-m-d H:i:s", time() );
		$id = $userData['id'];
		$this->model->updateRecord( $id, array( 'last_login' => $ts ) );
	}

	/**
	 *
	 * Checks DB PW Dates for PW validation
	 *
	 * @param $userData
	 */
	private function checkLastPasswordSetAndRedirect( $userData, $redirect ){
		if( $userData['pw_set_date'] == '0000-00-00 00:00:00' || strtotime( $userData['pw_set_date'] ) < strtotime( '-3 months' ) ) {
			redirect( 'change', 'location' );
		} else {
			if ( !$this->session->userdata('loginRedirect') ) {
				$url = ( !empty( $redirect ) ) ? urldecode( $redirect ) : $this->config->item('login_success_default_redirect');
			} else {
				$url = $this->session->userdata('loginRedirect');
			}
			redirect( $url, 'location' );
		}
	}




}

/* End of file login.php */
/* Location: ./app/controllers/login.php */