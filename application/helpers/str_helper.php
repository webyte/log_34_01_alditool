<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/**
	 * trimArrayValues
	 * @param $array
	 * @param $wrapper
	 * @return array
	 */
	function trimArrayValues( $array, $wrapper='' ) {
		$tmp = array();
		foreach( $array as $value ) {
			$tmp[] = trim( $value, $wrapper );
		}
		return $tmp;
	}

	/**
	 * replacePattern
	 * 
	 * @param $content
	 * @param $varArray
	 * @return String
	 */
	function replacePattern( $content, $varArray ) {
		if ( count( $varArray ) > 0 ) {
			foreach( $varArray as $key_val => $value ) {
				$pattern = "#\[".$key_val."\]#";
				$content = preg_replace( $pattern, $value, $content );
			}
		}
		return $content;
	}

/* End of file str_helper.php */
/* Location: ./app/helpers/str_helper.php */