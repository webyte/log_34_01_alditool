<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['meta.keywords'] = 'Logwin AG - Aldi Portal';
$lang['meta.description'] = 'Logwin AG - Aldi Portal';
$lang['meta.author'] = 'Logwin AG';
$lang['login.html.title'] = 'Logwin AG - Aldi Portal';
$lang['login.headline'] = 'Logwin Aldi Portal';
$lang['login.body'] = 'If you require any information or you want to get login details for the Logwin Aldi Portal,<br/>please contact <a href="mailto:lots@logwin-logistics.com" class="mailto">lots@logwin-logistics.com</a>.';
$lang['login.message.1'] = 'login';
$lang['login.message.2'] = 'Please insert correct login details!';
$lang['login.field.user'] = 'Username:';
$lang['login.field.pwd'] = 'Password:';
$lang['login.field.submit'] = 'login';
$lang['login.field.cancel'] = 'Cancel';
$lang['login.forgot'] = 'Forgot your password? Please click here <a href="forgot" class="internal">Forgot your password?</a>';
$lang['login.change'] = '<br/>Please click here to <a href="change" class="internal">Change your password?</a>';
$lang['list.logout'] = 'Logout';
$lang['login.change.body'] = 'For security reasons, you need to change your password every 3 month.';
$lang['login.change.message.1'] = 'Please enter your data.';

$lang['forgot.headline'] = 'Logwin Aldi Portal';
$lang['forgot.body'] = 'Forgot your password? Please register your E-Mail. Your password will be sent by mail.<br/>For further questions do not hesitate to contact <a href="mailto:lots@logwin-logistics.com" class="mailto">lots@logwin-logistics.com</a>.';
$lang['forgot.field.mail'] = 'E-Mail address:';
$lang['forgot.field.submit'] = 'Send';
$lang['forgot.message.1'] = 'Please enter your E-Mail address.';
$lang['forgot.message.2'] = 'Please enter your E-Mail address!';
$lang['forgot.message.3'] = 'The password was sent via E-Mail!';
$lang['forgot.message.4'] = 'Password was not sent, please try again!';
$lang['forgot.message.5'] = 'This E-Mail address does not exist.';
$lang['forgot.mail.subject'] = 'Logwin Aldi Portal - Forgot E-Mail';
$lang['forgot.mail.body'] = 'Dear Sir or Madam,
Your Password is:
[PASSWORD]

Kind regards,
Your Logwin AG Support';

$lang['supplier.inform.mail.subject'] = 'ALDI Stores - Target Cargo Ready Date: [FOB_DATE] [DEPARTURE], Contract/PO number: [CONTRACT_NUMBER]';
$lang['supplier.inform.mail.body'] = 'Dear [RECIPIENT],

Contract/PO number: [CONTRACT_NUMBER]
Target Cargo Ready Date (ALDI contract CRD): [FOB_DATE]
Port of Loading: [DEPARTURE]
Product Description: [DESCRIPTION]
Booking deadline: [OFU_SCHEDULED]      

We have been informed by ALDI that you will have shipment(s) from the above mentioned origin(s) to Australia.
The purpose of this e-mail is to establish contact with you to ensure a smooth Supply Chain operation with Logwin, who is the appointed CMFF to handle the freight forwarding activities on behalf of ALDI.

[LOO_NAME]
[LOO_STREET]
[LOO_CITY]
Phone: [LOO_PHONE]
Email: [LOO_MAIL]

To enable a better shipment follow up arrangement, pls provide below information by email on/before [INFO_DATE]

i) Your delegated manufacturer name and contact detail 
ii) Forecast Cargo Ready Date (i.e. forecast date for laden containers returned to CY or handover date to CFS) 
 
Some key notes below:
<ul> 
<li>A Certificate of Origin is required for all shipments from China, Vietnam, Thailand, Cambodia, Malaysia, Indonesia, Philippines, India and Bangladesh. Please send a copy of this certificate (if applicable) and the customs clearance documents pertaining to the above mentioned shipment to ALDIcustoms@logwin-logistics.com seven working days prior to vessel arrival.</li>
<li>Container Weight - Cargo weight per container must not exceed 21,500 kilos</li>
<li>Fumigation if necessary, please take note of the 21 day validity period and below information:</li>
</ul>
Fumigation Certification should have at least one consignment link

Fumigation Certification is require the information including POD: Sydney / Melbourne / Brisbane / Adelaide / Fremantle next to the consignment link even if one copy of fumigation can be used and please make sure ALDI is listed as the consignee.

From 2nd August 2021 the following declaration about plastic wrapping, impervious surface and timber thickness should be included to the fumigation certificate:

- A statement confirming that the target of the fumigation conforms to the plastic wrapping, impervious surface and timber thickness requirements at the time of fumigation, as per Appendix 3 of the Methyl Bromide Fumigation Methodology.
<ul>
<li>Illegal logging form to be filled for all timber products shipped to Australia. Please see the link for details Illegal logging - DAFF (agriculture.gov.au) </li>
<li>This email also serves to alert you that additional shipping lines charges will be levied from your account for any amendment or cancellation made by the supplier on the shipment booking after ALDI shipment booking deadline (i.e. 21 days before ALDI\'s Targeted Cargo Ready Date). In situation where deadfreight or late booking cancellation fee incur due to failure of supplier\'s timely action as per the stated deadline, ALDI reserves the right to charge back any unused space and cancellation fees to the supplier / manufacturer.</li>
<li>Australia Import Documents - please submit your documents including HBL + Telex Release, Certificate of Origin/FTA, Commercial Invoice, Packing List, Packing Declaration,  and Manufacturer Declaration no later than 7 days before ETA.</li>
<li>Please refer to ALDI Supplier Onboarding Guideline (NFSB) for further information.</li>
</ul>
Looking forward to cooperate with you in arranging On Time In Full shipments for Aldi!';

$lang['loo.inform.mail.subject'] = 'New contract [CONTRACT_NUMBER] in Aldi Tool';
$lang['loo.inform.mail.body'] = 'Dear colleagues,

a new contract is available in the Aldi Tool. Please log in to the Aldi tool for details.
Contract No.: [CONTRACT_NUMBER]
On-Sale week: [ADVERTISEMENT_WEEK]
Latest date of arrival at Aldi AU WH: [ALDI_AU_WH_DELIVERY_DUE_DATE]
Latest date of arrival at Port of Destination: [ALDI_AU_WH_DELIVERY_DUE_DATE_MINUS_SEVEN_DAYS]

*Please do not answer this automatic generated e-mail!*!';

$lang['loo.inform.mail.body_core_range'] = 'Dear colleagues,

a new Core Range Purchase Order is available in the Aldi Tool. Please log in to the Aldi tool for details.
Purchase Order No.: [CONTRACT_NUMBER]
Latest date of arrival at Aldi AU WH: [ALDI_AU_WH_DELIVERY_DUE_DATE]
Latest date of arrival at Port of Destination: [ALDI_AU_WH_DELIVERY_DUE_DATE_MINUS_TEN_DAYS]

Please do not reply to this automatic generated e-mail!';

$lang['loo.change.mail.recipient'] = 'lao.scm@logwin-logistics.com,Carol.Headington@logwin-logistics.com';
$lang['loo.change.mail.subject'] = 'Change of Logwin Overseas Office - contract [CONTRACT_NUMBER] in Aldi Tool';
$lang['loo.change.mail.body'] = 'Dear colleagues,

Please be informed that the Logwin Overseas Office for the above named contract has been changed in the Aldi Tool to [LOO_NAME]

[LOO_ADDRESS]

*Please do not answer this automatic generated e-mail!*';

$lang['ofu.alert.mail.recipient'] = 'lao.scm@logwin-logistics.com';
$lang['ofu.alert.mail.subject'] = 'OFU  not yet registered missing - contract [CONTRACT_NUMBER] in Aldi Tool';
$lang['ofu.alert.mail.body'] = 'Dear colleagues,

Please be informed that the Logwin Overseas Office has not yet registered the OFU
for the above named contract in the Aldi Tool

*Please do not answer this automatic generated e-mail!*';

$lang['atd.inform.mail.recipient'] = 'au.bots.aldi@logwin-logistics.com';
$lang['atd.inform.mail.cc'] = 'logistics@aldi.com.au';
$lang['atd.inform.mail.subject'] = 'On-Sale Week: [ADVERTISEMENT_WEEK], CLP for Container [CONTAINER_NUMBER]';
$lang['atd.inform.mail.body'] = 'Please note that captioned CLP is now available in the ALDI tool for your download.

Kind regards

Logwin Air + Ocean Australia';

$lang['clp.confirm.mail.recipient'] = '';
$lang['clp.confirm.mail.subject'] = 'CLP confirmation';
$lang['clp.confirm.mail.body'] = 'The CLP has been confirmed for:
HBL / HAWB: [HBL_HAWB]
Container number: [CONTAINER_NUMBER]';

$lang['clp.change.mail.recipient'] = '';
$lang['clp.change.mail.subject'] = 'Change of CLP status';
$lang['clp.change.mail.body'] = 'The status of the CLP has been changed to unconfirmed:
HBL / HAWB: [HBL_HAWB]
Container number: [CONTAINER_NUMBER]
Please check if all necessary information are available, correct the data of this CLP and save the CLP afterwards.';

$lang['clp.change2.mail.recipient'] = '';
$lang['clp.change2.mail.subject'] = 'Change of CLP – On Sale Week: [ADVERTISEMENT_WEEK], Container no. [CONTAINER_NUMBER], Contract no. [CONTRACT_NUMBER]';
$lang['clp.change2.mail.body'] = 'The captioned CLP has been unconfirmed and might have been updated. Please make sure to re-print this CLP.';

$lang['clp.deny.mail.recipient'] = '';
$lang['clp.deny.mail.subject'] = 'CLP has not been approved';
$lang['clp.deny.mail.body'] = 'The CLP has not been approved from the "Logwin Australia".
Please check the comments and get in contact with "Logwin Australia" if necessary.
HBL / HAWB: [HBL_HAWB]
Container number: [CONTAINER_NUMBER]';

$lang['office.reminder.mail.recipient'] = '';
$lang['office.reminder.mail.subject'] = 'ALDI Stores Telex Release - Contract No: [CONTRACT_NUMBER], Product: [PRODUCT_CODE], On sale week: [ADVERTISEMENT_WEEK]';
$lang['office.reminder.mail.body'] = '*Please do not answer this automatic generated e-mail!*

Please advice Logwin AU destination office of Telex Release status';

$lang['supplier.reminder.mail.recipient'] = '';
//$lang['supplier.reminder.mail.cc'] = 'Logistics@aldi.com.au';
$lang['supplier.reminder.mail.subject'] = 'ALDI Stores Telex Release - Contract No: [CONTRACT_NUMBER], Product: [PRODUCT_CODE], On sale week: [ADVERTISEMENT_WEEK]';
$lang['supplier.reminder.mail.body'] = '*Please do not answer this automatic generated e-mail!*

House Bill: [HBL_HAWB]
Port of Destination: [DESTINATION_PORT]

This email is to alert you that the captioned product is due to arrive in Australia with Vessel ETA: [ETA].
To date no Telex Release respectively Bill of Lading has been received by Logwin.

Please email a copy of the Telex Release to ALDIcustoms@logwin-logistics.com';

$lang['supplier.reminder2.mail.recipient'] = '';
//$lang['supplier.reminder2.mail.cc'] = 'Logistics@aldi.com.au';
$lang['supplier.reminder2.mail.subject'] = 'ALDI Stores Telex Release – 2nd request, Contract No: [CONTRACT_NUMBER], Product: [PRODUCT_CODE], On sale week: [ADVERTISEMENT_WEEK]';
$lang['supplier.reminder2.mail.body'] = '*Please do not answer this automatic generated e-mail!*

House Bill: [HBL_HAWB]
Port of Destination: [DESTINATION_PORT]

This email is to alert you that the captioned product is due to arrive in Australia with Vessel ETA: [ETA].
To date no Telex Release respectively Bill of Lading has been received by Logwin.

Please email a copy of the Telex Release to ALDIcustoms@logwin-logistics.com';

$lang['report.order.mail.recipient'] = 'or@webyte.org';
$lang['report.order.mail.subject'] = 'ALDI summary of order report';
$lang['report.order.mail.body'] = '';

$lang['dfu.mail.recipient'] = 'or@webyte.org';
$lang['dfu.mail.subject'] = 'ALDI Stores - Contract [CONTRACT_NUMBER], Product [PRODUCT_CODE], [MORE] - Request to submit documents';
$lang['dfu.mail.body'] = 'Dear [RECIPIENT]
It has been noted that the following documents for the captioned shipment have not been received yet. Please send the following documents to us urgently:
[DOCUMENTS]

Please forward all documents to: ALDIcustoms@logwin-logistics.com. In accordance with ALDI\'s instructions all documents must be received seven working days prior to vessel ETA.

Please also make sure that Telex release is arranged prior to ETA

*Please do not answer this automatic generated e-mail!*';

$lang['clp.telex.mail.recipient'] = 'or@webyte.org';
$lang['clp.telex.mail.subject'] = 'HBL [HBL_HAWB] Telex released';
$lang['clp.telex.mail.body'] = 'Telex release has been arranged for
HBL: [HBL_HAWB]
Loading port: [DEPARTURE_PORT]
Vessel: [MOTHER_VESSEL_NAME]
Container: [CONTAINER_NUMBER]
 
Corresponding document is available in Lots. 
For details please check below link:

https://scm.logwin-logistics.com/scm/autologin.jsp?loginkey=71R9srzudz9TtjZVmxULZP%2Bjzkt1ZL76YM5cjhjP6no%3D&menu=tracktrace&page=globalTrackShipments&q_HAWBBL=[HBL_HAWB]';

$lang['container.reminder.mail.recipient'] = '';
$lang['container.reminder.mail.subject'] = '[CONTAINER_NUMBER] – reminder of data transmission to Newline';
$lang['container.reminder.mail.body'] = 'Dear User,
[CONTAINER_NUMBER] will arrive on [ETA].
The container data has not been transmitted to Newline yet.
Please go to the container function in the Aldi tool and transmit the data asap.';


$lang['report_exc.reminder.mail.recipient'] = '';
$lang['report_exc.reminder.mail.subject'] = 'Aldi exception report';
$lang['report_exc.reminder.mail.body'] = 'Aldi exception report';

$lang['report_clp.mail.recipient'] = '';
$lang['report_clp.mail.subject'] = 'Logwin Aldi Tool - ';
$lang['report_clp.mail.body'] = 'Logwin Aldi Tool - ';

$lang['booking_received.reminder.mail.subject'] = 'Contract [CONTRACT_NUMBER] / OSW: [ON_SALE_WEEK] - booking date missing';
$lang['booking_received.reminder.mail.body'] = 'Dear colleagues,

No booking received date has been entered for contract [CONTRACT_NUMBER] in the ALDI tool.
Please note that ALDI needs to know the booking date latest 21 days prior CRD.
Please enter the date in the ALDI tool.
If you haven\'t received the booking from the shipper yet, please tick "No booking received" in the contract.

Please do not answer this automatic generated e-mail';

$lang['supplier_portal.comment.mail.subject'] = 'URGENT ACTION REQUIRED [CONTRACT_NUMBER], [DC], [HBL_HAWB], [SHIPMENT_NUMBER]';
$lang['supplier_portal.comment.mail.body'] = 'Dear Recipient,
Please refer to below comment regarding outstanding documentation required for Customs clearance of this shipment:

[COMMENT]

Disclaimer: All documents need to be supplied 7 days prior to vessel ETA per your contract conditions with ALDI Stores. Neither Logwin nor ALDI stores shall be held responsible for any delays or fees associated with late or unacceptable provision of documentation.
';

/* End of file local_lang.php */
/* Location: ./app/language/english/local_lang.php */