<?php
namespace Deployer;

require 'recipe/codeigniter.php';

// Your Git repository
set('repository', 'https://x-token-auth:ATCTT3xFfGN0zMz9Ug59NjpPBVpdIYVPrI56iTQkSNigf7SaIjxlr2rH9G-njRDiGVgoNkAv7aF_wJcXTaYlBHDtj4VeQZHMF-W39ljxIAzy5sgHNusXEW3_a-YZziFD0XvuQPTRkINIxDD1vVogUcTbXJHwgn4fdEhgoFyEkdV6zokp9ZJ8yJ4=14B0ABCF@bitbucket.org/webyte/log_34_01_alditool.git');

// disable sudo
set('writable_use_sudo', false);

// php binary
set('bin/php', '/usr/bin/php');
//set('bin/php', '/opt/lampp/bin/php');


// deactivate statistics
set('allow_anonymous_stats', false);

// deactivate statistics
set('keep_releases', 3);

// writable mode
set('writable_mode', 'chmod');

// CodeIgniter shared dirs
set('shared_dirs', array('application/cache', 'application/logs', 'tmp', 'import', 'export', 'import_stage', 'export_stage', 'uploads', 'archive', 'db'));

// composer options
set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader --no-suggest');

// staging
host('staging')
//	->hostname('luxweb.thiel-logistik.loc')
	->hostname('127.0.0.1')
	->port( 2766 )
	->set('branch', 'development')
	->user('webytealdi')
	->set('deploy_path', '/clu/srv/www/alditool.thiel-logistik.loc/staging')
	->set('env', [
		'http_proxy' => '172.16.200.50:8080',
		'https_proxy' => '172.16.200.50:8080'
	]);


// production
host('production')
//	->hostname('luxweb.thiel-logistik.loc')
	->hostname('127.0.0.1')
	->port( 2766 )
	->set('branch', 'master')
	->user('webytealdi')
	->set('deploy_path', ' /clu/srv/www/alditool.thiel-logistik.loc/production')
	->set('env', [
		'http_proxy' => '172.16.200.50:8080',
		'https_proxy' => '172.16.200.50:8080'
	]);


task('deploy:vendors-proxy', function () {
	run("cd {{release_path}} && curl -sS https://getcomposer.org/installer | {{bin/php}}");
	$composer = '{{bin/php}} composer.phar {{composer_options}}';
    run('export {http,https,ftp}_proxy="http://172.16.200.50:8080" && cd {{release_path}} && '.$composer);
})->desc('Installing vendors with proxy');

//task('deploy:phpunit', function () {
//    try {
//        run("cd {{release_path}}/application/tests && php ../../vendor/bin/phpunit");
//    } catch (\RuntimeException $e) {
//        // test fails
//        run("cd {{deploy_path}} && if [ ! -d releases-failed ]; then mkdir releases-failed; fi");
//        run("mv {{release_path}} {{deploy_path}}/releases-failed");
//        run("cd {{deploy_path}} && if [ -h release ]; then rm release; fi");
//
//        throw $e;
//    }
//})->desc('Run PHPUnit');

//task('deploy:security-checker', function () {
//     run("cd {{release_path}} && vendor/bin/security-checker security:check composer.lock");
//})->desc('Run SensioLabs Security Checker');

task('deploy:file-rights', function () {
	//run("cd {{release_path}} && chmod -R 777 cron application/cache application/logs export export_stage tmp uploads db");
	run("cd {{release_path}} && chmod -R 777 application/cache application/logs");
})->desc('Run chmod');

task('deploy:revision-file', function () {
	$git = get('bin/git');
	$server = basename(get('current_path'));
	run("cd {{release_path}} && $git rev-parse --short HEAD > revision");
	run("cd {{release_path}} && echo $server > release");
})->desc('Create revision file');

task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:symlink',
	'deploy:revision-file',
	'deploy:file-rights',
    'cleanup',
])->desc('Deploy your project');
